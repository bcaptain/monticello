# Copyright 2012-2013 Brandon Captain.

# this script is for blender
# pack()'s format characters and endian-ness: http://docs.python.org/2/library/struct.html#format-characters
# this script exports multi-byte values as little-endian

bm1_version = 3
is_animated = 0

bl_info = {
	"name": "BM1",
	"description": "Export BM1 (.bm1 format)",
	"author": "Brandon Captain",
	"version": (1,0),
	"blender": (2, 6, 3),
	"location": "File > Export > BM1 ",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": "Import-Export"
}

import traceback
import bpy, bmesh
from bpy_extras.io_utils import ExportHelper
from mathutils import *
from math import *
from itertools import *
from bpy.props import BoolProperty, StringProperty, FloatVectorProperty
from bpy.types import Operator, AddonPreferences, PropertyGroup
from struct import *
		
def update_export_mesh_settings(self, context, origin ):
	if not getattr(self,origin): #prevents callback loop
		return
	for prop_name in self.export_mesh_setting_names:
		if prop_name != origin:
			setattr(self, prop_name, False )

class BM1_Scene(bpy.types.PropertyGroup):
	uniformScale_enabled = bpy.props.BoolProperty(
		name="Uniform scaling",
		description = "If Marked, model will export with a uniform scale defined by exportBM1_uniformScale",
		default = True
	)
	uniformScale= bpy.props.FloatProperty(
		name="Scale",
		description="On import into the game engine, the mesh vertices and bones will be scaled uniformly by this factor",
		default=True
	)
	scaleVec = bpy.props.FloatVectorProperty(
		name="Scale",
		description="On import into the game engine, the mesh vertices will be scaled by these dimensions",
		default=(1.0,1.0,1.0),
		precision=4,
		subtype='XYZ',
		size=3
	)
	export_main_mesh = bpy.props.BoolProperty(
		name = "Export Main Mesh",
		description = "Export Main Mesh",
		default = True,
		update= lambda self, context : update_export_mesh_settings(self, context, 'export_main_mesh')
	)
	export_accessory_mesh = bpy.props.BoolProperty(
		name = "Export Accessory mesh",
		description = "Export 'Accessory' mesh",
		default = False,
		update= lambda self, context : update_export_mesh_settings(self, context, 'export_accessory_mesh')
	)
	export_collision_mesh = bpy.props.BoolProperty(
		name = "Export Collision mesh",
		description = "Export 'Collision' mesh",
		default = False,
		update= lambda self, context : update_export_mesh_settings(self, context, 'export_collision_mesh')
	)
	export_mesh_setting_names = ['export_collision_mesh','export_accessory_mesh','export_main_mesh']
	

def all_equal( iterable ):
	g = groupby(iterable)
	return next(g, True) and not next(g, False)
	
class ExportMyFormat(bpy.types.Operator, ExportHelper):
	bl_idname		= "export_format.bm1"
	bl_label		= "Export BM1"

	filename_ext	= ".bm1"
	
	exporting_limb_only = bpy.props.BoolProperty(
		default = False
		)
	export_accessory_mesh_from_BM1Anim_export = bpy.props.BoolProperty(
		default = False
	)
	filePath_from_BM1Anim_export = bpy.props.StringProperty(
		name='filePath_from_BM1Anim_export',
		description='filePath passed from BM1Anim_export, used when exporting armatures and meshes simultaneously',
		options={'HIDDEN'},
		subtype='FILE_PATH'
	)
	scaleVec_from_BM1Anim_export = bpy.props.FloatVectorProperty(
		name='scale_from_BM1Anim_export',
		description='scale passed from BM1Anim_export, used when exporting armatures and meshes simultaneously',
		options={'HIDDEN'},
		subtype='XYZ',
		size=3
	)

	def execute(self, context):
		scene = bpy.context.scene
		bm1 = scene.BM1
		if not self.filePath_from_BM1Anim_export:
			FilePath = bpy.path.ensure_ext(self.filepath, ".bm1")
			if bm1.export_collision_mesh:
				if FilePath.find("_cm") < 0:
					index = FilePath.find(".bm1")
					newFilePath = FilePath[:index] + "_cm" + FilePath[index:]
					FilePath = newFilePath
		else:
			FilePath = bpy.path.ensure_ext(self.filePath_from_BM1Anim_export, ".bm1")
			if all_equal(self.scaleVec_from_BM1Anim_export):
				bm1.uniformScale_enabled = True
				bm1.uniformScale = self.scaleVec_from_BM1Anim_export.x
			else:
				bm1.uniformScale_enabled = False
				bm1.scaleVec = self.scaleVec_from_BM1Anim_export
			bm1.export_accessory_mesh = self.export_accessory_mesh_from_BM1Anim_export
			if not bm1.export_accessory_mesh or not self.exporting_limb_only:
				bm1.export_main_mesh = True
		try:
			write_bm1(self, FilePath);
		except ExportError as e:
			self.report({'ERROR'}, "BM1 Export Error Found..see Console")
			print(traceback.format_exc(4))
			print(' '.join(e.args)) #error arguments have to be strings
		return {'FINISHED'}
		
	def draw(self, context):
		scene = context.scene
		bm1 = scene.BM1
		layout = self.layout
		box = layout.box()
		row = box.row()
		row.label("Export Scale")
		col = box.column()
		col.prop( bm1, "uniformScale_enabled")
		row = col.row()
		if bm1.uniformScale_enabled:
			row.prop( bm1, "uniformScale")
		else:
			row.prop( bm1, "scaleVec")
		row = layout.row()
		row.prop( bm1, "export_main_mesh" )
		row = layout.row()
		row.prop( bm1, "export_accessory_mesh" )
		row = layout.row()
		row.prop( bm1, "export_collision_mesh" )
		
def write_vec3f( out_files, vec ):
	out_files['bm1'].write( pack('<fff', vec.x, vec.y, vec.z ) )

def write_uchar( out_files, uchr ):
	out_files['bm1'].write( pack('<B', uchr ) )

def write_uint( out_files, uval ):
	out_files['bm1'].write( pack('<I', uval ) )

def write_ushort( out_files , uval ):
	out_files['bm1anim'].write( pack('<H', uval ) )

def write_int( out_files, ival ):
	out_files['bm1'].write( pack('<i', ival ) )

def write_float( out_files, flt ):
	out_files['bm1'].write( pack('<f', flt ) )

def write_short_string( out_files, str ):
	write_uchar( out_files, len( str ) )
	out_files['bm1'].write( bytes( str, 'ASCII') )
	
def write_header( out_files ):
	global is_animated
	out_files['bm1'].write( bytes( "BM1", 'ASCII') )
	write_uchar( out_files, bm1_version )
	if (  NumExportMeshArmatureModifiers() > 0 ):
		is_animated = 1
	write_uchar( out_files, is_animated ) # marked as unused by the engine, since .bm1anim extention indicates this is an animated model.
	
def write_export_scale( self, out_files ):
	scene = bpy.context.scene
	bm1 = scene.BM1
	if bm1.uniformScale_enabled:
		uniform_val = bm1.uniformScale
		uniform_scalevec = Vector([uniform_val,uniform_val,uniform_val])
		write_vec3f( out_files, uniform_scalevec)
		out_files['exp_log'].write("Scale (uniform): {0.x},{0.y},{0.z}\n".format( uniform_scalevec ))
	else:
		export_precision = bpy.types.bm1.scaleVec[1]['precision']
		write_vec3f( out_files, bm1.scaleVec )
		out_files['exp_log'].write("Scale : {0.x:.{1}f},{0.y:.{1}f},{0.z:.{1}f}\n".format( bm1.scaleVec, export_precision ))
	
def define_valid_mesh_names():
	global valid_meshName
	valid_meshName = []
	bm1 = bpy.context.scene.BM1
	
	if bm1.export_main_mesh:
		valid_meshName = ['Mesh','mesh']
	elif bm1.export_accessory_mesh:
		valid_meshName = ['Accessory','accessory']
	elif bm1.export_collision_mesh:
		valid_meshName = ['Collision','collision','cm','CM']
	else:
		mesh_name_error_msg = "No valid meshnames detected for export settings, valid names are : "
		if len(valid_meshName):
			armature_name_error_msg += ' ,'.join(valid_meshName)
			raise RuntimeError(mesh_name_error_msg)
		else:
			raise RuntimeError("BM1 Error: No valid export settings detected.")

def define_valid_armatures( self ):
	global valid_body_armatures, armature_body_set, armature_dict, export_armature_set
	valid_body_armatures = ['Legs', 'Torso','legs','torso']
	valid_accessory_armatures = ['Accessory','accessory']
	armature_body_set = set(valid_body_armatures)
	armature_accessory_set = set(valid_accessory_armatures)
	all_armatures_set = set(valid_body_armatures + valid_accessory_armatures )
	armature_dict = dict(legs = 0, Legs = 0, Torso = 1, torso = 1, Accessory = 1, accessory = 1)
	reverse_armature_dict = { value : key for key, value in armature_dict.items() }
	armature_dict.update( reverse_armature_dict )
	
	bm1 = bpy.context.scene.BM1
	if bm1.export_main_mesh or self.exporting_limb_only:
		export_armature_set = armature_body_set
	elif bm1.export_accessory_mesh:
		export_armature_set = armature_accessory_set
	elif bm1.export_collision_mesh:
		return
	else:
		raise RuntimeError("No valid export settings detected, must select 'Main' object or 'Accessory'")
	
def get_valid_mesh_objects( objects, exportMesh_objs ):
	bm1 = bpy.context.scene.BM1

	for ob in objects:
		if ob.type == 'MESH' and ob.data.name in valid_meshName:
			if not ob.proxy:
				exportMesh_objs.append(ob)
	if len(exportMesh_objs) < 1:
		raise ExportError("Valid Mesh Not Found")
			
def get_valid_limb_mesh_ob( mesh_objects ):
	#find bone marked for export
	marked_bone = ''
	keeplooping=True
	while keeplooping:
		for arm_object in bpy.context.scene.objects:
			if not keeplooping: break
			if arm_object.data.name in armature_body_set:
				for bone in arm_object.pose.bones:
					if bone.bm1anim_export_enabled:
						marked_bone = bone
						keeplooping = False
	
	if marked_bone == '':
		raise ExportError("No bone marked for limb export")
		
	for ob in mesh_objects:
		for modifier in ob.modifers:
			if modifier.object.type == 'ARMATURE':
				for bone in modifier.object.pose.bones:
					if bone == marked_bone:
						return modifer.object
						
def get_valid_limb_mesh_ob( mesh_objects ):
	#return object of first bone marked for export
	marked_bone = ''
	keeplooping=True
	while keeplooping:
		for arm_object in bpy.context.scene.objects:
			if not keeplooping: break
			if arm_object.data.name in armature_body_set:
				for bone in arm_object.pose.bones:
					if not keeplooping: break
					if bone.bm1anim_export_enabled:
						marked_bone = bone
						keeplooping = False
	if marked_bone == '':
		raise ExportError("No bone marked for limb export")
	for ob in mesh_objects:
		for modifier in ob.modifiers:
			if modifier.object.type == 'ARMATURE':
				for bone in modifier.object.pose.bones:
					if bone == marked_bone:
						return ob
	
def write_vert_boneID_and_weight( vert_local, out_files):
	write_uchar( out_files, len(vert_local.groups))
	out_files['exp_log'].write("  #G: " + str(len(vert_local.groups)) + ", ")
	if len( vert_local.groups ) > 2:
		raise ExportError('Vert:'+str(vert_local.index)+' is assigned to more than 2 weight groups')
	for counter, vert_group in enumerate(vert_local.groups):
		if counter > 0:
			out_files['exp_log'].write( ', ' )
		write_uchar( out_files, vert_group.group)
		out_files['exp_log'].write(str(vert_group.group) + ":")
		write_float( out_files, vert_group.weight)
		out_files['exp_log'].write('{: 5.3f}'.format(vert_group.weight) )
	out_files['exp_log'].write("\n")

def write_vertices( vertices, out_files, transformation_matrix ):
#vertices = bpy.context.scene.objects['Mesh'].data.vertices
	write_uint( out_files, len(vertices) )
	out_files['exp_log'].write("Vert Listing :	[Normal(x,y,z), #G:Num of Groups, G1:G1_wgt, G2:G2_wgt,Pos(x,y,z)) \n")
	for vert_local in vertices:
		write_vec3f( out_files, vert_local.co * transformation_matrix ) # Goes from local to world space, if the origin is at (0,0,0) then this will be an identity matrix
		out_files['exp_log'].write(str(vert_local.index) + " : ")
		write_vec3f( out_files, vert_local.normal)
		out_files['exp_log'].write("Normal:(" + '{: 5.3f}'.format(vert_local.normal.x) + ",")
		out_files['exp_log'].write('{: 5.3f}'.format(vert_local.normal.y) + ",")
		out_files['exp_log'].write('{: 5.3f}'.format(vert_local.normal.z) + ")")
		if is_animated:
			write_vert_boneID_and_weight( vert_local, out_files)
		out_files['exp_log'].write(", Pos:(" + '{:5.3f}'.format((vert_local.co * transformation_matrix).x) + ",")
		out_files['exp_log'].write('{:5.3f}'.format((vert_local.co * transformation_matrix).y) + ",")
		out_files['exp_log'].write('{:5.3f}'.format((vert_local.co * transformation_matrix).z) + ")")
		out_files['exp_log'].write("\n")
		

def print_face_normal( tri ):
	a = tri[1] - tri[0]
	b = tri[2] - tri[0]
	c = a.cross( b )
	c.normalize()
	print(c)

def write_face_vertice_indexes( face_vertice_indexes, mesh, faceIndex, out_files ):
	vertex_count = len(face_vertice_indexes)
	write_uchar( out_files, vertex_count )
	tri = list()
	#for vertIndex in range(vertex_count): # normal winding
	for vertIndex in range(vertex_count - 1,-1,-1): # reverses the winding on the vertices
		write_uint( out_files, face_vertice_indexes[vertIndex] )
		vert = mesh.vertices[vertIndex]
		#tri.append( mesh.vertices[vertIndex].co ) #todo: we only need three
		write_vec3f( out_files, vert.normal ) # vertex normal, local space
		mesh.update(calc_tessface=True)
		if mesh.tessface_uv_textures.active == None:
			write_uchar( out_files, 0 ) # uv count is zero
		else:
			vertex_uvs = mesh.tessface_uv_textures.active.data[faceIndex].uv[vertIndex]
			write_uchar( out_files, len(vertex_uvs) )
			for vertex_uv in vertex_uvs:
				write_float( out_files, vertex_uv )
	#print_face_normal( tri )

def write_faces( mesh, out_files ):
	write_uint( out_files, len(mesh.polygons) )
	for faceIndex in range(len(mesh.polygons)):
		face = mesh.polygons[faceIndex]
		write_vec3f( out_files, face.normal )
		write_uchar( out_files, face.material_index ) #todo: remove this
		write_face_vertice_indexes( face.vertices, mesh, faceIndex, out_files )
		if len(face.vertices) > 3:
			raise ExportError('Mesh contains quads, engine only supports triangle meshes')

def write_mesh( mesh, out_files, transformation_matrix ):
	write_vertices( mesh.vertices, out_files, transformation_matrix )
	write_faces( mesh, out_files )

def write_meshes( self, objects, out_files ):
	exportMesh_objs = []
	get_valid_mesh_objects( objects, exportMesh_objs )
	write_uchar(out_files, len(exportMesh_objs))
	out_files['exp_log'].write("Mesh Count = " + str(len(exportMesh_objs)) + '\n')

	if not self.exporting_limb_only:
		mesh_number = 0
		for mesh_ob in exportMesh_objs:
			mesh_number += 1
			out_files['exp_log'].write("Begin Mesh : " + str(mesh_number) + '\n')
			write_mesh( mesh_ob.data, out_files, mesh_ob.matrix_world )
	else:
		limb_mesh_ob = get_valid_limb_mesh_ob( objects )
		if limb_mesh_ob == '':
			raise ExportError("Valid Limb Mesh Not Found")
		vert_indices = []
		limb_mesh = create_limb_mesh( limb_mesh_ob, vert_indices )
		obj = bpy.data.objects.new('Limb', limb_mesh)

		bpy.context.scene.objects.link(obj) #debug only, will add mesh object created for export to the scene

		ensure_object_in_mode( limb_mesh_ob, 'OBJECT')
		copy_uv_data( limb_mesh_ob.data, limb_mesh, vert_indices ) #this needs to be called before copy vert weights because of bmesh usage
		copy_vert_weights( limb_mesh_ob.data, limb_mesh, vert_indices )
		write_mesh( obj.data, out_files, obj.matrix_world )
		
		bpy.context.scene.objects.unlink(obj) #debug only, removes the created mesh object from the scene
		
		bpy.data.objects.remove(obj) #cleanup
		bpy.data.meshes.remove(limb_mesh) #cleanup

def create_limb_mesh( mesh_ob, vert_indices ):
	ensure_object_in_mode( mesh_ob, 'EDIT' ) #careful here, have to use object mode enums only

	mesh_data = mesh_ob.data

	edge_indices = []
	face_indices = []
	new_verts = get_marked_vertices( mesh_data.vertices , vert_indices )
	new_edges = get_marked_edges( mesh_data.edges, vert_indices, edge_indices )
	new_faces = get_marked_faces( mesh_data.polygons, vert_indices, face_indices )
	re_indexed_edges = get_reindexed_edges( new_edges, vert_indices )
	re_indexed_faces = get_reindexed_faces( new_faces, vert_indices )
	print('VertLen ', len(new_verts), '\n')
	print('EdgeLen ', len(re_indexed_edges), '\n')
	print('FaceLen ', len(re_indexed_faces), '\n')
	
	return generate_new_mesh( new_verts, re_indexed_edges, re_indexed_faces )

def generate_new_mesh( verts, edges, faces ):
	me_data = bpy.data.meshes.new('Limb')
	me_data.from_pydata( verts, edges, faces )
	me_data.update()
	return me_data

def copy_uv_data( orig_mesh, new_mesh, vert_indices ):
	bm = bmesh.new()
	bm.from_mesh( new_mesh )

	uv_layer = bm.loops.layers.uv.verify()
	bm.faces.layers.tex.verify()
	
	sorted_vert_list = sorted( vert_indices )
	
	for f in bm.faces:
		new_face_verts = []
		for vert in f.verts:
			new_face_verts.append(sorted_vert_list[vert.index])
		face_loop_idx = []
		for old_poly in orig_mesh.polygons:
			face_loop_idx = list(old_poly.loop_indices)
			old_face_verts = []
			for old_vert in old_poly.vertices:
				old_face_verts.append(old_vert)	
			if old_face_verts == new_face_verts:
				for cnt, l in enumerate(f.loops):
					luv = l[uv_layer]
					luv.uv = orig_mesh.uv_layers.active.data[face_loop_idx[cnt]].uv

	bm.to_mesh(new_mesh) #applies the bmesh to the new mesh, effectively replacing its data

def get_marked_vertices( mesh_vertices, vert_indices ):
	new_vertices = []
	for vert in mesh_vertices:
		vertex_marked = True
		if len(vert.groups):
			for vertex_group in vert.groups:
				boneName = bpy.context.scene.objects['Mesh'].vertex_groups[vertex_group.group].name
				for armature in bpy.data.armatures:
					for bone in armature.bones:
						if bone.name == boneName:
							bone_from_vert = bpy.context.scene.objects[armature.name].pose.bones[boneName]
							if not bone_from_vert.bm1anim_export_enabled:
								vertex_marked = False
		else:
			raise ExportError('Vert:',str(vert.index),'has no group assigned')
		if vertex_marked:
			if not vert.index in vert_indices:
				vert_indices.append(vert.index)
	for vert in vert_indices:
		new_vertices.append(mesh_vertices[vert].co[:])
	return new_vertices

def get_marked_edges( mesh_edges, vert_indices, edge_indices ):
	new_edges = []
	for edge in mesh_edges:
		for vert in edge.vertices:
			for vertex in vert_indices:
				if vertex == vert:
					if not edge.index in edge_indices:
						edge_indices.append(edge.index)
	for edge in edge_indices:
		new_edges.append(mesh_edges[edge].vertices[:])
	return new_edges

def get_marked_faces( mesh_faces, vert_indices, face_indices ):
	new_faces = []
	for face in mesh_faces:
		for vert in face.vertices:
			for vertex in vert_indices:
				if vertex == vert:
					if not face.index in face_indices:
						face_indices.append(face.index)
	for face in face_indices:
		new_faces.append(mesh_faces[face].vertices[:])
	return new_faces

def get_reindexed_edges( edges, vert_indices ):
	tmp_list = []
	sorted_vert_list = sorted(vert_indices)

	re_indexed_edges = []
	for edge in edges:
		for element in edge:
			if element in sorted_vert_list:
				tmp_list.append(sorted_vert_list.index(element))
		re_indexed_edges.append(tuple(tmp_list))
		tmp_list = []
		
	return re_indexed_edges
	
def get_reindexed_faces( faces, vert_indices ):
	tmp_list = []
	sorted_vert_list = sorted(vert_indices)

	re_indexed_faces = []
	for face in faces:
		for element in face:
			if element in sorted_vert_list:
				tmp_list.append(sorted_vert_list.index(element))
		re_indexed_faces.append(tuple(tmp_list))
		tmp_list = []

	return re_indexed_faces

def copy_vert_weights( from_mesh, to_mesh, vert_indices ):
	assign_limb_vertex_groups( from_mesh, to_mesh)
	
	sorted_vert_list = sorted(vert_indices)
	from_obj = get_obj_from_mesh( from_mesh )
	to_obj = get_obj_from_mesh( to_mesh )
	
	for vert in sorted_vert_list:
		orig_vertex_groups = from_mesh.vertices[vert].groups
		for group in orig_vertex_groups:
			group_name = from_obj.vertex_groups[group.group].name
			vert_index = sorted_vert_list.index(vert)
			to_obj.vertex_groups[group_name].add( [vert_index], group.weight, "ADD")
			
def get_obj_from_mesh( mesh ):
	ob_matches = []
	for ob in bpy.data.objects:
		if ob.data == mesh:
			ob_matches.append( ob )
			
	if len(ob_matches)  > 1:
		errMsg = "Multiple objects found with same mesh data names, see objects : "
		for ob in ob_matches:
			errMsg += ob.name + ", "
		raise RuntimeError(errMsg + '\n')
	
	return ob_matches[0]

def assign_limb_vertex_groups( from_mesh, to_mesh):
	from_obj = get_obj_from_mesh( from_mesh )
	to_obj = get_obj_from_mesh( to_mesh )
	arm_obj = ""
	
	for modifier in from_obj.modifiers:
		for bone in modifier.object.pose.bones:
			if bone.bm1anim_export_enabled:
				arm_obj = modifier.object
				break

	for bone in arm_obj.pose.bones:
		if bone.bm1anim_export_enabled:
			if not bone.parent:
				copy_limb_vertex_groups( arm_obj, to_obj, bone )
			else:
				if not bone.parent.bm1anim_export_enabled:
					copy_limb_vertex_groups( arm_obj, to_obj, bone )
				
def copy_limb_vertex_groups( arm_obj, to_obj, bone):
	to_obj.vertex_groups.new( bone.name )
	for child in arm_obj.pose.bones[bone.name].children:
		if child.bm1anim_export_enabled:
			copy_limb_vertex_groups( arm_obj, to_obj, child )

def getRootBone( armature ):
	for bone in armature.bones:
		if not bone.parent:
			return bone
			
def verify_proper_attachment( bone ):
	if bone.attached_to_armature is not None:
		if bone.attached_to_bone:
			return True
		else:
			raise RuntimeError("No bone target for attachment on bone : " + bone.name + ", select an armature and bone or disable the bone attachment")	
	
	return False

def GetArmatureChannel( armature ):
	#If exporting accessory, we match the channel of the armature it is attached to
	channelName = armature.name
	if ( armature.name in ['Accessory','accessory'] ):
		rootBone = getRootBone( armature )
		hasGoodAttachment = verify_proper_attachment( rootBone )
		if ( hasGoodAttachment ):
			channelName = rootBone.attached_to_armature.name
			
	return armature_dict[channelName]

def write_channel_ids(out_files, arm_channels_exported):
	for channel in arm_channels_exported:
		write_uint(out_files, channel)
		out_files['exp_log'].write('Armature {0}: channel = {1} \n'.format(armature_dict[channel],channel))

def write_armature_count(self, out_files):
	arm_channels_exported = []
	armature_count = 0
		
	if not self.exporting_limb_only: #determined by bm1Anim export settings
		for arm in bpy.data.armatures:
			if arm.name in export_armature_set:
				armature_count += 1
				arm_channels_exported.append( GetArmatureChannel( arm ) )
	else:
		armature_flag = False
		for arm_object in bpy.context.scene.objects:
			if arm_object.data.name in export_armature_set:
				for bone in arm_object.pose.bones:
					if bone.bm1anim_export_enabled:
						armature_flag = True
				if armature_flag:
					armature_count += 1
					arm_channels_exported.append(armature_dict[arm_object.data.name])
					armature_flag = False
	write_uint(out_files, armature_count)
	arm_channels_exported.sort()
	write_channel_ids(out_files, arm_channels_exported)
	out_files['exp_log'].write('Armature Count = {}'.format(armature_count) + '\n')

def write_bone_count(self, out_files):
	bone_count = 0

	if not self.exporting_limb_only:
		for arm in bpy.data.armatures:
			if arm.name in export_armature_set:
				bone_count += len(arm.bones)
	else:
		for arm_object in bpy.context.scene.objects:
			if arm_object.data.name in export_armature_set:
				for bone in arm_object.pose.bones:
					if bone.bm1anim_export_enabled:
						bone_count += 1;
	out_files['exp_log'].write('Total Bone Count = {}'.format(bone_count) + '\n')
	write_uint(out_files,bone_count)

def write_bm1(self, filepath):
	define_valid_mesh_names()
	define_valid_armatures( self )
	if preCheck_ErrorsFound():
		return
	cm_flag = "_cm" if bpy.context.scene.BM1.export_collision_mesh else ""
	export_log = open(bpy.path.abspath("//") + bpy.path.basename(bpy.context.blend_data.filepath).split('.')[0] + cm_flag + "_mesh_log.txt", "w")
	out=open(filepath,"w+b")
	out_files = dict(bm1 = out, exp_log = export_log)
	write_header( out_files )
	write_export_scale( self, out_files )
	if ( NumExportMeshArmatureModifiers() > 0 ):
		write_armature_count(self, out_files) 
		write_bone_count(self, out_files)
	write_meshes( self, bpy.context.scene.objects, out_files ) #1
	out_files['bm1'].close()
	out_files['exp_log'].close()

def ensure_object_in_mode( mesh_obj, set_mode ):
	for obj in bpy.context.scene.objects:
		obj.select = False
	mesh_obj.select = True
	bpy.context.scene.objects.active = mesh_obj
	if not mesh_obj.mode == set_mode:
		bpy.ops.object.mode_set(mode=set_mode)
		
def NumExportMeshArmatureModifiers():
	numArmatures = 0
	for ob in bpy.data.objects:
		if ob.data.name in valid_meshName:
			for mod in ob.modifiers:
				if mod.type == "ARMATURE":
					numArmatures += 1
	
	return numArmatures
	
def check_meshNames_NoErrors():
	validMeshNames = ','.join( valid_meshName )
	errorFound = False
	validMeshNamesFound = []
	objectsWithValidMeshNames = []
	for mesh in bpy.data.meshes:
		if mesh.name in valid_meshName:
			validMeshNamesFound.append( mesh.name )
			objectsWithValidMeshNames.append( mesh.name )

	if ( len( validMeshNamesFound ) < 1 ):
		errorFound = True
		raise ExportError("No valid mesh found for export, must be one of [" + validMeshNames + "].")
		
	if ( len( validMeshNamesFound ) > 1 ):
		errorFound = True
		objects = ','.join( objectsWithValidMeshNames )
		raise ExportError("Error: Too many valid mesh data names found.  See objects [" + objects + "].\n" + "    Valid mesh data names are [" + validMeshNames + "].")
		
	return errorFound
		
	
def preCheck_ErrorsFound(): 
	errorFound = check_meshNames_NoErrors()
	return errorFound

class ExportError(Exception):
	pass

def menu_func(self, context):
	self.layout.operator(ExportMyFormat.bl_idname, text="BM1 Format(.bm1)");

def register():
	bpy.utils.register_module(__name__);
	bpy.types.INFO_MT_file_export.append(menu_func);
	bpy.types.Scene.BM1 = bpy.props.PointerProperty(type = BM1_Scene)

def unregister():
	bpy.utils.unregister_module(__name__);
	bpy.types.INFO_MT_file_export.remove(menu_func);

if __name__ == "__main__":
	register()
