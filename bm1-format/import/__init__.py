# Copyright 2012-2013 Brandon Captain.

# this script is for blender
# pack()'s format characters and endian-ness: http://docs.python.org/2/library/struct.html#format-characters
# this script imports multi-byte values as little-endian

bm1_version = 1
is_animated = 0

bl_info = {
	"name": "BM1",
	"description": "Import BM1 (.bm1 format)",
	"author": "Brandon Captain",
	"version": (1,0),
	"blender": (2, 6, 3),
	"location": "File > Import > BM1 ",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": "Import-Export"
}

import bpy
from bpy_extras.io_utils import ImportHelper
from struct import *

class ImportMyFormat(bpy.types.Operator, ImportHelper):
	bl_idname       = "import_format.bm1";
	bl_label        = "Import BM1";
	bl_options      = {'PRESET'};

	filename_ext    = ".bm1";

	def execute(self, context):
		FilePath = bpy.path.ensure_ext(self.filepath, ".bm1");
		read_bm1(FilePath);
		return {'FINISHED'};

def read_uchar( bm1 ):
	return unpack('<B', bm1.read(1) )[0]

def read_uint( bm1 ):
	return unpack('<I', bm1.read(4) )[0]

def read_int( bm1 ):
	return unpack('<i', bm1.read(4) )[0]

def read_float( bm1 ):
	return unpack('<f', bm1.read(4) )[0]

def read_vec3f( bm1 ):
	return ( read_float(bm1), read_float(bm1), read_float(bm1) );

def read_short_string( bm1 ):
	bm1=open("/t","r+b")
	strlen=unpack("<B", bm1.read(1) )[0]
	return bm1.read(strlen).decode('ASCII')

def read_header( bm1 ):
	global is_animated
	header_type = bm1.read( 3 );
	if ( header_type is not b'BM1' ):
		print( "Not a BM1 file, header is " + header_type.decode('ASCII') )
	bm1_version = read_uchar( bm1 )
	is_animated = read_uchar( bm1 )

def read_vert_boneID_and_weight( bm1, verts_list, vert_index ):
	print('reading bone ids and weights')
	num_groups = read_uchar( bm1 )
	for vert_group in range( num_groups ):
		vert_group.group.append( read_uchar( bm1 ) )
		vert_group.weight.append( read_float( bm1 ) )

def read_vertices( bm1 ):
	num_verts = read_uint( bm1 )
	print( "reading", num_verts, "vertices" );
	verts=[]
	for v in range( num_verts ):
		verts.append( read_vec3f( bm1 ) )
		if is_animated != 0:
			read_vert_boneID_and_weight( bm1, verts, v ) # we pass the whole list because we need to mutate it (not pass by value)
	return verts;

def read_face_vertice_indexes( bm1, face_indices, face_uvs ):
	vertex_count = read_uchar( bm1 )
	# print( "reading", vertex_count, "vert indices" );
	indices=[]
	uvs=[]
	for vertIndex in range(vertex_count):
		indices.append( read_uint( bm1 ) )
		read_vec3f( bm1 ) # todo: vertex normal
		uv_count = read_uchar( bm1 ) # this should always be 2
		if ( uv_count == 2 ):
			u = read_float( bm1 )
			v = read_float( bm1 )
			uvs.append( (u,v) )
		elif ( uv_count != 0 ):
			print("Error: found vertex with", uv_count, "UVs")
	face_indices.append(indices)
	face_uvs.append(uvs)

def read_faces( bm1, face_indices, face_uvs ):
	num_faces = read_uint( bm1 )
	print( "reading", num_faces, "faces" );
	for faceIndex in range(num_faces):
		read_vec3f( bm1 ) # todo_nextversion: normal (remove)
		mat = read_uchar( bm1 ) # todo_nextversion: material index ( remove this )
		read_face_vertice_indexes( bm1, face_indices, face_uvs )

def deselect_all():
	for o in bpy.context.scene.objects:
		o.select = False

def deselect_all():
	for o in bpy.context.scene.objects:
		o.select = False

def load_mesh( bm1 ):
	verts = read_vertices( bm1 ) # each unique vertice in the mesh
	face_indices=[]
	face_uvs=[]
	read_faces( bm1, face_indices, face_uvs ) # referencing vertices by index
	name='newmesh'
	m = bpy.data.meshes.new(name)
	o = bpy.data.objects.new(name, m)
	bpy.context.scene.objects.link(o)
	bpy.context.scene.update()
	m.tessface_uv_textures.new() # generate UVs for all vertices
	m.vertices.add(len(verts))
	for i in range( len( m.vertices ) ):
		m.vertices[i].co = verts[i]
	# put all the vertices in each face into the MeshLoop
	loop=0
	for f in range( len(face_indices) ):
		for v in range( len(face_indices[f]) ):
			m.loops.add( 1 )
			m.loops[loop].vertex_index=face_indices[f][v]
			loop += 1
	# assign the vertex indices from the loops to the faces
	m.polygons.add(len(face_indices))
	start = 0
	for i in range( len(face_indices) ):
		m.polygons[i].loop_start=start
		m.polygons[i].loop_total=len( face_indices[i] )
		start += m.polygons[i].loop_total
	m.update(calc_tessface=True,calc_edges=True)
	#deselect_all()
	#o.select = True
	#bpy.context.scene.objects.active = o # must be active object to go into edit mode
	#bpy.ops.object.mode_set(mode='EDIT')
	for f in range( len(face_indices) ):
		for v in range( len(face_indices[f]) ):
			m.tessface_uv_textures.active.data[f].uv[0] = (4,4)
			m.tessface_uv_textures.active.data[f].uv[1] = (4,4)
			#m.tessface_uv_textures.active.data[f].uv = face_uvs[f]
	#bpy.ops.object.mode_set(mode='OBJECT')
	m.update()
	for f in range( len(face_indices) ):
		for v in range( len(face_indices[f]) ):
			print( "uv:", m.tessface_uv_textures.active.data[f].uv[0][0], m.tessface_uv_textures.active.data[f].uv[0][1], m.tessface_uv_textures.active.data[f].uv[1][0], m.tessface_uv_textures.active.data[f].uv[1][1] )

def read_mesh_count( bm1 ):
	return read_uchar( bm1 )

def read_meshes( bm1 ):
	mesh_count = read_mesh_count( bm1 )
	print( "reading", mesh_count, "meshes" );
	for ob in range( mesh_count ):
		load_mesh( bm1 )

def read_bm1(filepath):
	bm1=open(filepath,"r+b")
	read_header( bm1 )
	read_meshes( bm1 )
	bm1.close()

def menu_func(self, context):
	self.layout.operator(ImportMyFormat.bl_idname, text="BM1 Format(.bm1)");

def register():
	bpy.utils.register_module(__name__);
	bpy.types.INFO_MT_file_import.append(menu_func);

def unregister():
	bpy.utils.unregister_module(__name__);
	bpy.types.INFO_MT_file_import.remove(menu_func);

if __name__ == "__main__":
	register()
