
if (( $# != 1 )) ; then
	echo "Usage: profile.sh [outname]"
	echo "runs ./monticello and saves results as outname.gprof and outname.png"
	echo ""
	echo "Note: requires ./gprof2dot.py to generate png image."
	exit
fi

echo "running program"
LD_LIBRARY_PATH=../lib/lin/:$LD_LIBRARY_PATH ./monticello
echo "waiting for program to exit and generate gmon.out"
sleep 5 # wait for gprof to do it's thing
echo "running gprof to generate $2.gprof"
gprof "$1" gmon.out > "$2".gprof
echo "running gprof2dot.py to generate $2-prof.png"
cat "$2".gprof | ./gprof2dot.py | dot -Tpng -o "$2-prof".png
echo "done"

