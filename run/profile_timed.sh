
if (( $# != 3 )) ; then
	echo "Usage: [program] [seconds] [outname]"
	echo "runs [program] for [seconds] seconds and saves results as outname.gprof and outname.png"
	echo ""
	echo "Note: requires gprof2dot.py to generate png image."
	exit
fi

$1 &
sleep $2
kill $!
sleep 1 # wait for gprof to do it's thing
gprof "$1" > "$3".gprof
cat "$3".gprof | gprof2dot.py | dot -Tpng -o "$3".png
