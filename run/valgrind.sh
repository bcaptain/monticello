#!/bin/bash
rm val.log
# to show ALL memory leaks: --show-reachable=yes

opts="-v --leak-check=full --read-var-info=yes --show-reachable=yes --track-origins=yes $*"

LD_LIBRARY_PATH=../lib/lin/:$LD_LIBRARY_PATH /opt/valgrind-3.15.0/bin/valgrind $opts ./monticello_debug 2>>val.log 1>>val.log 

