


if (( $# != 2 )) ; then
	echo "Usage: profile.sh [program] [outname]"
	echo "runs [program] and saves results as outname.gprof and outname.png"
	echo ""
	echo "Note: requires gprof2dot.py to generate png image."
	exit
fi

valgrind --tool=callgrind --callgrind-out-file="$2.cg" "$1"
sleep 2 # wait for valgrind to do it's thing
cat "$2.cg" | gprof2dot.py -f callgrind | dot -Tpng -o "$2.png"

