This is the game data director. In the future, mod support will expect other data directories paralell to this one.

Each mod will have one config file in it's root names config.cfg

Assets and asset packages go here. Each package is a separate sub directory.

Each directory (including the main package directory) is sorted alphanumerically before loading. This is how files will be overridden in sequels/mods/etc.

GOLDEN RULES:

Do not use underscores in filenames unless specified to do so in instructions from another folder.txt. Doing so can cause file name parsers to screw up. If you use more than one word in a file, use camelCase.
