# set the state of the moving Entity at various times since activation (in milliseconds)
# todo: periods for accel, linear, and decel.

0 1000 move_local 0,-12,0
## move (absolute)
## depart at 0ms, arrive at 0ms at 10,10,0
#0 1000 move 10,10,0
#2000 3000 move 10,-10,0
#4000 5000 move -10,-10,0
#6000 7000 move -10,10,0
#8000 8000 reset 0 # reset script

## move (local)
## depart at 0ms, arrive at 0ms at 10,10,0
#0 1000 move_local 10,0,0
#1000 2000 move_local 0,-10,0
#2000 3000 move_local -10,0,0
#3000 4000 move_local 0,10,0
#4000 4000 reset 0 # reset script

## begin rotate at 0ms, finish at 0ms: rotate 10 degrees on the z axis around entities own origin
#0 1000 rotate_local 360,0,0 # turn a full revolution over 0.5 seconds
#1000 1000 reset 0 # and repeat

# begin rotate at 0ms, finish at 0ms: rotate 10 degrees on the z axis around origin 1,2,0
#0 0 rotate 0,0,10 1,2,0
