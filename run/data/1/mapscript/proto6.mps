# *** SYNTAX DOCUMENTATION ***

# ACTIONS
# "atTime" takes two arguments. the time in ms since the map has started (integer), and a SUBACTON
# "ifKilled", takes two parameters, a string entity name, and a SUBACTION.

# SUBACTIONS
# "spawnEnemy" takes two arguments: the entity type (string), and a DESCRIPTOR.

# DESCRIPTORS:
# "withName" takes one string parameter, the name to set the entity to
# "atPos" takes one vector parameter in the form "0,1,2"

atTime 5000 spawnEnemy grenadier withName bob atPos 0,0,10
ifKilled bob spawnEnemy grenadier withName sandy atPos 0,1,10
ifKilled bob spawnEnemy grenadier withName sandy atPos 1,0,10
ifKilled sandy spawnEnemy bob withName sandy atPos 0,0,10

