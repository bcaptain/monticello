#version 120

uniform sampler2D tex0;
varying vec2 fUV;

void main()
{
    gl_FragColor = texture2D(tex0,fUV).rgba;
}
