#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec3 vPos;
attribute vec2 vUV;
attribute vec4 vAlphas;
varying vec2 fUV;
varying vec4 fAlphas;

void main()
{    
    gl_Position = P * (M * vec4(vPos,1.0));

    fUV = vUV;
    fAlphas = vAlphas;
}
