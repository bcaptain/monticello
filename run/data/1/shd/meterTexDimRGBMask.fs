#version 120
#extension GL_EXT_gpu_shader4 : enable

uniform sampler2D tex0;
uniform sampler2D mask;
varying vec2 fUV;
uniform float percentage;
uniform float subPercentage;

void main()
{
	
	float highDimmer = 0.2;
	float lowDimmer = 0.05;
	
	float highDimRange = percentage + subPercentage;
	
	float grad_value = texture2D(mask,fUV).r;
	
	gl_FragColor = texture2D(tex0,fUV);
	
	if ( grad_value > highDimRange )
		gl_FragColor.rgb -= highDimmer;
		
	if ( grad_value > percentage && grad_value < highDimRange )
		gl_FragColor.rgb -= lowDimmer;
		
}
