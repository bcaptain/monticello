#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec3 vPos;
attribute vec2 vUV;
varying vec2 fUV;

uniform float life_frac; // zero to one
uniform float life_span_ms; // in miliseconds
uniform vec3 origin;

uniform float fWeight;
uniform vec3 vColor;
varying vec3 fColor;

uniform float fWeightTo;
uniform vec3 vColorTo;
varying vec3 fColorTo;

uniform float scale_from;
uniform float scale_to;

uniform float rot_ang_initial;

uniform bool stuntRotate;
uniform float rot_ang_per_sec;

uniform bool stuntDest;
uniform vec3 dest;

uniform bool stuntDest2;
uniform vec3 dest2;

#define PI ( 3.1415926 )
#define DEG_TO_RAD ( PI / 180 )
#define RAD_TO_DEG ( 180 / PI )

void main()
{
    if ( life_frac > 1.0f ) {
        return;
    }

    vec3 fVert = vPos;

    float z = 0.0;

    z = rot_ang_initial * DEG_TO_RAD;

    if ( stuntRotate ) {
        z += ( life_frac * ( life_span_ms / 1000 ) ) * (rot_ang_per_sec * DEG_TO_RAD);
    }

    if ( z != 0.0 ) {
        // rotate around the Z axis
        mat3 rot = mat3(
            cos(z),-sin(z),0,
            sin(z),cos(z),0,
            0,0,0
        );
        fVert *= rot;
    }

    if ( scale_to != 1.0f || scale_from != 1.0f ) {
        float scale = scale_from * (1-life_frac);
        scale += scale_to * life_frac;
        fVert *= scale;
    }

    if ( stuntDest ) {
        vec3 A = dest;
        A -= origin;
        A *= life_frac;
        A += origin;

        if ( stuntDest2 ) {
            // dest2 is relative to dest1, otherwise the particles will just scatter
            vec3 B = dest;
            B += dest2;

            B -= dest;
            B *= life_frac;
            B += dest;

            vec3 C = B;
            C -= A;
            C *= life_frac;
            C += A;

            fVert += C;
        } else {
            fVert += A;
        }
    }

    fVert += origin;

    gl_Position = P * (M * vec4(fVert,1.0));
    fUV = vUV;

    if ( fWeight > 0.0 ) {
        fColor = vColor;
    }

    if ( fWeightTo > 0.0 ) {
        fColorTo = vColorTo;
    }
}
