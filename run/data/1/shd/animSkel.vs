#version 120
#extension GL_EXT_gpu_shader4 : enable
//Shader for animating a model

uniform mat4 M;  //Modelview Matrix
uniform mat4 P;  //Projection Matrix
uniform vec3 vLocalRot;
uniform vec3 vLocalRotOffset;

const int MAX_BONES_IN_SKELETON = 40;
uniform mat2x4 Bone_DQ[MAX_BONES_IN_SKELETON]; //Dualquaternions for each bone

//input variable from host
/* todo : vPos was made vec4 for drawing skeletons
    = Vec3(x,y,z),(float)BoneID
    (float)BoneID was so I could draw the skeleton, using the last slot as a boneID so the vertex
    would know what bone it belonged to.
*/

uniform int BoneChannels[MAX_BONES_IN_SKELETON]; //Dictionary matching boneIDs to their channels [ boneID, channel ]
uniform mat4 ChannelOffset[2]; //Array of matrices representing channel offset positions per frame
attribute vec4 vPos;
varying vec4 fColor;

#include debug.glsl
#include multiplyDualQuat.glsl

void main()
{
    float boneColor = fract(vPos.w / 3.0f);

    if (boneColor < 0.005f || boneColor == 1.0f) {
        fColor = RED;
    } else if (boneColor < 0.4f) {
        fColor = YELLOW;
    } else {
        fColor = CYAN;
    }

    vec3 tmp = MultiplyDualQuat();

    gl_Position = vec4(tmp,1.0);
    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;
}
