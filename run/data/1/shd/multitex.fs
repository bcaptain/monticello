#version 120

uniform sampler2D tex1;
uniform sampler2D tex0;
varying vec2 fUV;
varying vec4 fAlphas;

void main()
{
    vec4 t0 = texture2D(tex0, fUV).rgba;
    vec4 t1 = texture2D(tex1, fUV).rgba;

    t0.a *= fAlphas.r;
        
    gl_FragColor = mix(t0, t1, fAlphas.g);

}

