#version 120
#extension GL_EXT_gpu_shader4 : enable

#define MAX_POINT_LIGHTS 10
#define MAX_DIR_LIGHTS 2
#define alpha_threshold 0.95

struct DirLight {
    vec3 direction;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular; //TODO
};

struct PointLight {
    vec3 position;
    float constant;
    float linear;
    float quadratic;
    //float radius; //used in alternative attenuation calculation below
    vec3 ambient;
    vec3 diffuse;
    vec3 specular; //TODO
};

uniform sampler2D tex0;
uniform float fWeight;
uniform vec4 vColor;

uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform DirLight dirLights[MAX_DIR_LIGHTS];
uniform int numPointLights;
uniform int numDirLights;

varying vec3 vNormal;
varying vec2 fUV;
varying vec3 fragPos;

#include light.glsl

void main() {
    
    vec3 result = vec3(0.0f,0.0f,0.0f);
    
    vec3 normV = normalize( vNormal );
    
    // directional lighting
    for(int i = 0; i < numDirLights; i++)
        result = CalcDirLight( dirLights[i], vNormal );
    //vec3 result = CalcDirLight(dirLights[i], vNormal, viewDir);  //TODO : viewDir is for specular
    
    // pointLights
    for(int i = 0; i < numPointLights; i++)
        result += CalcPointLight( pointLights[i], normV, fragPos );

        //result += CalcPointLight(pointLights[i], vNormal , FragPos, viewDir); // TODO: viewDir is for specular
    
    //ambient light
    result += 0.05f;
    
    gl_FragColor = vec4( result, 1.0f );    
    
    // subtract the portion we will replace with our color
    gl_FragColor -= texture2D(tex0,fUV) * fWeight;

    // add our color based on weight
    gl_FragColor += vColor * fWeight;

    // the original alpha from the tex0, applied to our color's alpha
    gl_FragColor.a = texture2D(tex0,fUV).a * vColor.a;
    
    if ( gl_FragColor.a < alpha_threshold ) //prevents transparency problems from texture sampling
        discard;
}
