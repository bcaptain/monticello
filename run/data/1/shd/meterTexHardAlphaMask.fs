#version 120
#extension GL_EXT_gpu_shader4 : enable

uniform sampler2D tex0;
uniform sampler2D mask;
varying vec2 fUV;
uniform float percentage;

void main()
{	
	if ( (1 - percentage ) > texture2D(mask,fUV).r )
		discard;
	
	gl_FragColor = texture2D(tex0,fUV);
}
