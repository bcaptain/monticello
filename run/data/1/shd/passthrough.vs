#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec3 vPos;
varying vec2 fUV;

void main()
{
    gl_Position = P * (M * vec4(vPos,1.0));
    fUV = gl_Position.xy;
}
