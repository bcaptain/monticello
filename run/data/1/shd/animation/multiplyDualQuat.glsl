
#ifndef shd_animation_MultiplyDualQuat
#define shd_animation_MultiplyDualQuat

//Applying dualquat to a vector
vec3 MultiplyDualQuat()
{
    //**Bone_DQ[Bone Index][0 = qReal, 1 = qDual]
    vec4 qr = Bone_DQ[int(vPos.w)][0];
    vec4 qd = Bone_DQ[int(vPos.w)][1];

    vec3 translate;

    translate.x = 2 * (-qd.w*qr.x + qd.x*qr.w - qd.y*qr.z + qd.z*qr.y);
    translate.y = 2 * (-qd.w*qr.y + qd.x*qr.z + qd.y*qr.w - qd.z*qr.x);
    translate.z = 2 * (-qd.w*qr.z - qd.x*qr.y + qd.y*qr.x + qd.z*qr.w);

    mat4x3 Tx;

    float   two_ySq = 2 * (qr.y*qr.y),
            two_xSq = 2 * (qr.x*qr.x),
            two_zSq = 2 * (qr.z*qr.z),
            two_xy = 2 * (qr.x*qr.y),
            two_wz = 2 * (qr.w*qr.z),
            two_xz = 2 * (qr.x*qr.z),
            two_yz = 2 * (qr.y*qr.z),
            two_wx = 2 * (qr.x*qr.w),
            two_wy = 2 * (qr.w*qr.y);

    Tx[0][0] = 1 - two_ySq - two_zSq;
    Tx[0][1] = two_xy + two_wz;
    Tx[0][2] = two_xz - two_wy;
    Tx[1][0] = two_xy - two_wz;
    Tx[1][1] = 1 - two_xSq - two_zSq;
    Tx[1][2] = two_yz + two_wx;
    Tx[2][0] = two_xz + two_wy;
    Tx[2][1] = two_yz - two_wx;
    Tx[2][2] = 1 - two_xSq - two_ySq;

    Tx[3] = translate;

    return Tx * vec4(vPos.xyz,1.0f);
}

#endif
