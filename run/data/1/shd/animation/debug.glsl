#ifndef shd_animation_debug
#define shd_animation_debug

//Debug Variables
const vec4 RED = vec4(1.0,0.0,0.0,1.0);
const vec4 BLUE = vec4(0.0,0.0,1.0,1.0);
const vec4 GREEN = vec4(0.0,1.0,0.0,1.0);
const vec4 YELLOW = vec4(1.0,1.0,0.0,1.0);
const vec4 PURPLE = vec4(1.0,0.0,1.0,1.0);
const vec4 WHITE = vec4(1.0f,1.0f,1.0f,1.0f);
const vec4 CYAN = vec4(0.0f,1.0f,1.0f,1.0f);

#endif
