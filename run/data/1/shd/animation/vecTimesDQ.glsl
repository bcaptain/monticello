
#ifndef shd_animation_vecTimesDQ
#define shd_animation_vecTimesDQ

vec3 vecTimesDQ( vec3 myVec ) {
    //**Bone_DQ[Bone Index][0 = qReal, 1 = qDual]

    // DualQuat with Weights

    mat2x4 dq = CalcWeightedDQ();

    vec3 translate;
    translate.x = 2 * (-dq[1].w*dq[0].x + dq[1].x*dq[0].w - dq[1].y*dq[0].z + dq[1].z*dq[0].y);
    translate.y = 2 * (-dq[1].w*dq[0].y + dq[1].x*dq[0].z + dq[1].y*dq[0].w - dq[1].z*dq[0].x);
    translate.z = 2 * (-dq[1].w*dq[0].z - dq[1].x*dq[0].y + dq[1].y*dq[0].x + dq[1].z*dq[0].w);

    mat4x3 Tx;

    vec4    twoSq = 2 * (dq[0] * dq[0]);
    float   two_xy = 2 * (dq[0].x*dq[0].y),
            two_wz = 2 * (dq[0].w*dq[0].z),
            two_xz = 2 * (dq[0].x*dq[0].z),
            two_yz = 2 * (dq[0].y*dq[0].z),
            two_wx = 2 * (dq[0].x*dq[0].w),
            two_wy = 2 * (dq[0].w*dq[0].y);

    Tx[0][0] = 1 - twoSq.y - twoSq.z;
    Tx[0][1] = two_xy + two_wz;
    Tx[0][2] = two_xz - two_wy;
    Tx[1][0] = two_xy - two_wz;
    Tx[1][1] = 1 - twoSq.x - twoSq.z;
    Tx[1][2] = two_yz + two_wx;
    Tx[2][0] = two_xz + two_wy;
    Tx[2][1] = two_yz - two_wx;
    Tx[2][2] = 1 - twoSq.x - twoSq.y;

    Tx[3] = translate;

    return Tx * vec4(myVec,1.0f);
}

#endif
