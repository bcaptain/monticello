#ifndef shd_animation_CalcWeightedDQ
#define shd_animation_CalcWeightedDQ

mat2x4 CalcWeightedDQ( ) {
    vec4 dqA_real = Bone_DQ[int(Bone_ID.x)][0];
    vec4 dqA_dual = Bone_DQ[int(Bone_ID.x)][1];

    vec4 dqB_real = Bone_DQ[int(Bone_ID.y)][0];
    vec4 dqB_dual = Bone_DQ[int(Bone_ID.y)][1];

    // Apply Weights
    dqA_real *= Bone_WGT.x;
    dqA_dual *= Bone_WGT.x;
    dqB_real *= Bone_WGT.y;
    dqB_dual *= Bone_WGT.y;

    mat2x4 dq;

    dq[0] = dqA_real + dqB_real;
    dq[1] = dqA_dual + dqB_dual;

    float normDQ = length(dq[0]);

    return dq / normDQ;
}

#endif
