#ifndef shd_animation_CalcAnimationTransformMatrix
#define shd_animation_CalcAnimationTransformMatrix

mat4x3 CalcAnimationTransformMatrix() {
    //**Bone_DQ[Bone Index][0 = qReal, 1 = qDual]
    vec4 qr = Bone_DQ[int(Bone_ID.x)][0];
    vec4 qd = Bone_DQ[int(Bone_ID.x)][1];

    // DualQuat with Weights

    mat2x4 tmpDQ = CalcWeightedDQ();

    vec3 translate;
    translate.x = 2 * (-tmpDQ[1].w*tmpDQ[0].x + tmpDQ[1].x*tmpDQ[0].w - tmpDQ[1].y*tmpDQ[0].z + tmpDQ[1].z*tmpDQ[0].y);
    translate.y = 2 * (-tmpDQ[1].w*tmpDQ[0].y + tmpDQ[1].x*tmpDQ[0].z + tmpDQ[1].y*tmpDQ[0].w - tmpDQ[1].z*tmpDQ[0].x);
    translate.z = 2 * (-tmpDQ[1].w*tmpDQ[0].z - tmpDQ[1].x*tmpDQ[0].y + tmpDQ[1].y*tmpDQ[0].x + tmpDQ[1].z*tmpDQ[0].w);

    mat4x3 Tx;

    vec4    twoSq = 2 * (tmpDQ[0] * tmpDQ[0]);
    float   two_xy = 2 * (tmpDQ[0].x*tmpDQ[0].y),
            two_wz = 2 * (tmpDQ[0].w*tmpDQ[0].z),
            two_xz = 2 * (tmpDQ[0].x*tmpDQ[0].z),
            two_yz = 2 * (tmpDQ[0].y*tmpDQ[0].z),
            two_wx = 2 * (tmpDQ[0].x*tmpDQ[0].w),
            two_wy = 2 * (tmpDQ[0].w*tmpDQ[0].y);

    Tx[0][0] = 1 - twoSq.y - twoSq.z;
    Tx[0][1] = two_xy + two_wz;
    Tx[0][2] = two_xz - two_wy;
    Tx[1][0] = two_xy - two_wz;
    Tx[1][1] = 1 - twoSq.x - twoSq.z;
    Tx[1][2] = two_yz + two_wx;
    Tx[2][0] = two_xz + two_wy;
    Tx[2][1] = two_yz - two_wx;
    Tx[2][2] = 1 - twoSq.x - twoSq.y;

    Tx[3] = translate;

    return Tx;
}

#endif
