#version 120
#extension GL_EXT_gpu_shader4 : enable

uniform sampler2D tex0;
uniform float fWeight;
varying vec2 fUV;
varying vec4 fColor;

void main()
{
// start with the color from the tex0
    gl_FragColor.rgb = texture2D(tex0,fUV).rgb;

    // subtract the portion we will replace with our color
    gl_FragColor.rgb -= texture2D(tex0,fUV).rgb * fWeight;

    // add our color based on weight
    gl_FragColor.rgb += fColor.rgb * fWeight;

    // the original alpha from the tex0, applied to our color's alpha
    gl_FragColor.a = texture2D(tex0,fUV).a * fColor.a;
}
