#version 120

uniform mat4 M;
uniform mat4 P;
uniform vec3 vColor;
attribute vec3 vPos;
attribute vec2 vCoord;
varying vec3 fColor;
varying vec2 fCoord;

void main()
{
    gl_Position = P * (M * vec4(vPos,1.0));
    fColor = vColor;
    fCoord = vCoord;
}
