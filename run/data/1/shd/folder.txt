Each glprog lists the name of a vertex shader file, and a fragment shader file.

Refer to src/rendering/shaders.cpp for itemized descriptions of each

PLEASE USE THIS UNIVERSAL VARIABLE NAMING SCHEME

* all variables that are passed to the vertex shaders are to be prepended with v, such as in vColor (except M and P, as shown below)
* all variables that are passed to the fragment shaders are to be prepended with f, such as fColor

M - modelview matrix
P - projection matrix
tex0 - first texture
tex1 - second texture, etc
vColor - vertex color
vPos - vertex
vUV - uv coordinate
fColor - a fragment color


