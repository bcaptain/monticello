#version 120

varying vec3 fColor;
varying vec2 fCoord;
uniform float fIntensity;

void main()
{
    // coordinates are -1 to 1 up and down the quad, so if we take the absolute value and subtract it from 1, we get a laser-like effect
    float absy = abs( fCoord.y );
    float alpha = ( 1 - absy ) * fCoord.x * fIntensity;

    vec3 color = fColor;

    // the non-glow part of the laser should be a constant high alpha with a high concentration of white
    if ( alpha > 0.5f ) {
        float x = 0.5f - absy;
        color.x += x;
        color.y += x;
        color.z += x;

        alpha = 0.8f;
    } else {
        // this makes the glowy part a little less intense
        alpha /= 1 + (absy*0.5f);
    }

    gl_FragColor = vec4(color, alpha);
}

