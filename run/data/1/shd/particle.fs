#version 120

uniform sampler2D tex0;
uniform float life_frac; // zero to one
uniform float fWeight;
uniform float fWeightTo;
uniform bool stuntOpacity;
uniform float opacity_from;
uniform float opacity_mid;
uniform float opacity_to;
varying vec2 fUV;
varying vec3 fColor;
varying vec3 fColorTo;

void main()
{
    gl_FragColor = texture2D(tex0,fUV); // start with the color from the texture

    if ( fWeight > 0.0 ) {
        gl_FragColor.rgb -= gl_FragColor.rgb * fWeight; // subtract the portion we will replace with our color
        gl_FragColor.rgb += fColor.rgb * fWeight; // add our color based on weight
    }

    if ( fWeightTo > 0.0 ) {
        gl_FragColor.rgb -= gl_FragColor.rgb * fWeightTo * life_frac; // subtract the portion we will replace with our color
        gl_FragColor.rgb += fColorTo.rgb * fWeightTo * life_frac; // add our color based on weight
    }

    if ( stuntOpacity ) {
        float from;
        float to;
        float percent_life;
        if ( life_frac < 0.5 ) {
            from = opacity_from;
            to = opacity_mid;
            percent_life = life_frac / 0.5;
        } else {
            from = opacity_mid;
            to = opacity_to;
            percent_life = ( life_frac - 0.5 ) / 0.5;
        }

        float diff = to - from;
        float opacity = from + ( diff * percent_life );

        gl_FragColor.a *= opacity;
    }
}
