#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec2 vPos;
attribute vec2 vUV;
varying vec2 fUV;

void main()
{
    gl_Position = P * (M * vec4(vPos,0.0,1.0));
    fUV = vUV;
}
