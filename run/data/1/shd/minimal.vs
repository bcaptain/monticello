#version 120

uniform mat4 M;
uniform mat4 P;
uniform vec4 vColor;
attribute vec3 vPos;
varying vec4 fColor;

void main()
{
    gl_Position = vec4(vPos,1.0);

    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;

    fColor = vColor;
}
