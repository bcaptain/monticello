
#ifndef shd_animation_light
#define shd_animation_light

//vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) //TODO : viewDir is for specular
vec3 CalcDirLight(DirLight light, vec3 normal);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos);

//vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) //TODO : viewDir is for specular
vec3 CalcDirLight(DirLight light, vec3 normal)
{
    vec3 lightDir = normalize(-light.direction);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0f);
    
    //TODO : specular
    // specular shading
    //vec3 reflectDir = reflect(-lightDir, normal);
    //float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // combine results
    vec3 ambient = light.ambient * vec3(texture2D(tex0, fUV));
    vec3 diffuse = light.diffuse * diff * vec3(texture2D(tex0, fUV));
    //vec3 specular = light.specular * spec * vec3(texture2D(material.specular, fUV)); //TODO : specular, specular material
    
    //return (ambient + diffuse + specular); //TODO : specular
    return (ambient + diffuse );
}


//vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir) //TODO : viewDir is for specular
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0f);
    
    //TODO : specular
    // specular shading
    //vec3 reflectDir = reflect(-lightDir, normal);
    //float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    //float attenuation = clamp(1.0 - distance/light.radius, 0.0, 1.0); //alternative attenuation calculation
    attenuation *= attenuation;
 
    // combine results   
    vec3 ambient = light.ambient * vec3(texture2D(tex0, fUV));
    vec3 diffuse = light.diffuse * diff * vec3(texture2D(tex0, fUV));
    
    //vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords)); //TODO : specular
    
    ambient *= attenuation;
      diffuse *= attenuation;
        
    //specular *= attenuation; //TODO : specular
    //return (ambient + diffuse + specular); //TODO : specular
    return ( ambient + diffuse );
}

#endif
