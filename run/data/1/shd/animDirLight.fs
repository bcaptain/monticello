#version 120
#extension GL_EXT_gpu_shader4 : enable

varying vec4 vNormal;
uniform vec3 lDir;
uniform vec4 lightColor;

void main()
{
    float intensity = max(dot(vec3(vNormal.xyz),-lDir), 0.0f);  //lDir is negated to get a vector pointing "towards" the light source
    
    gl_FragColor = vec4( vec3(lightColor.xyz) * intensity, 1.0f );//target

}
