#version 120
#extension GL_EXT_gpu_shader4 : enable


uniform vec3 lDir;
uniform vec4 lightColor;
uniform sampler2D tex0;
uniform float fWeight;
uniform vec4 vColor;

varying vec4 vNormal;
varying vec2 fUV;

void main()
{
    float intensity;
    
    float ambientStrength = 0.3f;
    vec4 ambient, diffuse;
    
    // start with the color from the tex0
    ambient = texture2D(tex0,fUV) * ambientStrength;
    
    intensity = max(dot(vec3(vNormal.xyz),-lDir), 0.0f);  //lDir is negated to get a vector pointing "towards" the light source
    diffuse =  intensity * lightColor;
    
    gl_FragColor = ambient + diffuse;

    // subtract the portion we will replace with our color
    gl_FragColor.rgb -= texture2D(tex0,fUV).rgb * fWeight;

    // add our color based on weight
    gl_FragColor.rgb += vColor.rgb * fWeight;

    // the original alpha from the tex0, applied to our color's alpha
    gl_FragColor.a = texture2D(tex0,fUV).a * vColor.a;
    
    //gl_FragColor = vec4( vec3(lightColor.xyz) * intensity, 1.0f );//target
}
