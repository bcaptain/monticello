#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec3 vPos;
attribute vec2 vUV;
varying vec2 fUV;

void main()
{
    gl_Position = vec4(vPos,1.0);

    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;
    fUV = vUV;
}
