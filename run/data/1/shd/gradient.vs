#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec3 vPos;
attribute vec4 vColor;
varying vec4 fColor;

void main()
{
    gl_Position = P * (M * vec4(vPos,1.0));
    fColor = vColor;
}
