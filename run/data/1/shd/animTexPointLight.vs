#version 120
#extension GL_EXT_gpu_shader4 : enable
//Basic shader for testing light

const int MAX_BONES_IN_SKELETON = 40;

uniform mat4 M;  //Modelview Matrix
uniform mat4 P;  //Projection Matrix
uniform mat3 N; //Normal Matrix, for lighting only
uniform mat2x4 Bone_DQ[MAX_BONES_IN_SKELETON]; //Dualquaternions for each bone
attribute vec3 vPos;
attribute vec3 vNorm;
attribute vec2 vUV;
attribute vec2 Bone_ID;  // Assumes each vert weighted to at most 2 bones.
attribute vec2 Bone_WGT;
varying vec3 vNormal;
varying vec2 fUV;
varying vec3 fragPos;

//input variable from host
/* todo : vPos was made vec4 for drawing skeletons
    = Vec3(x,y,z),(float)BoneID
    (float)BoneID was so I could draw the skeleton, using the last slot as a boneID so the vertex
    would know what bone it belonged to.
*/

#include debug.glsl
#include calcWeightedDQ.glsl
#include calcAnimationTransformMatrix.glsl

void main() {

    float eps = 0.000001;
    float PI_F = 3.141593f;
    float DEG_TO_RAD_F = PI_F / 180.0f;
    
    mat4x3 animTx = CalcAnimationTransformMatrix();
    mat3 animTx_Rot = mat3( animTx ); //For transforming normals only

    vNormal = vNorm;
    vec4 vPosition = vec4(vPos, 1.0f);
    
    if (Bone_WGT.x > eps) {
        gl_Position = vec4( animTx * vPosition ,1.0f ) ;
        fragPos = animTx * vPosition;
        vNormal = animTx_Rot * vNorm;
    } else {
        gl_Position = vPosition;
        fragPos = vPosition.xyz;
    }
    
    fragPos = vec4( M * vec4( fragPos, 1.0f)).xyz;
    
    vNormal = normalize( N * vNormal );
    
    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;
    
    fUV = vUV;
}
