#version 120
#extension GL_EXT_gpu_shader4 : enable

struct Light {
    vec3 position;
    vec4 ambient;
    vec4 diffuse; //i.e. the light's color
    float constant;
    float linear;
    float quadratic;
};

uniform sampler2D tex0;
uniform float fWeight;
uniform vec4 vColor;
uniform Light light;

varying vec4 vNormal;
varying vec2 fUV;
varying vec3 fragPos;

void main() {
    
    float intensity, attenuation, distance;
    
    vec4 ambient, diffuse;
    
    distance = length( light.position - fragPos );
    
    vec3 lDir = normalize( light.position - fragPos );
    
    //ambient
    ambient = light.ambient * texture2D(tex0,fUV);
    
    //diffuse
    intensity = max(dot(vec3(vNormal.xyz),lDir), 0.0f);
    diffuse = light.diffuse *  intensity * texture2D(tex0,fUV);
    
    //apply attenuation
    attenuation = 1.0f / (light.constant + (light.linear * distance) + ( light.quadratic * (distance * distance)) );
    
    ambient *= attenuation; //ambient
    diffuse *= attenuation;
    
    gl_FragColor = ambient + diffuse; //target
    
    // subtract the portion we will replace with our color
    gl_FragColor.rgb -= texture2D(tex0,fUV).rgb * fWeight;

    // add our color based on weight
    gl_FragColor.rgb += vColor.rgb * fWeight;

    // the original alpha from the tex0, applied to our color's alpha
    gl_FragColor.a = texture2D(tex0,fUV).a * vColor.a;
    
    //gl_FragColor = vec4( vec3(lightColor.xyz) * intensity, 1.0f );//target
}
