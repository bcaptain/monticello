#version 120

uniform mat4 M;
uniform mat4 P;
uniform vec4 vColor;
uniform vec3 vLocalRot;
uniform vec3 vLocalRotOffset;
attribute vec3 vPos;
attribute vec2 vUV;
varying vec2 fUV;
varying vec4 fColor;

#include rotationMatrix.glsl

void main()
{
    float PI_F = 3.141593f;
    float DEG_TO_RAD_F = PI_F / 180.0f;

    gl_Position = vec4(vPos,1.0);
    
    gl_Position += vec4( vLocalRotOffset, 1 );

    if ( vLocalRot.x != 0.0 )
        gl_Position = rotationMatrix( vec3(-vLocalRot.x,0,0), vLocalRot.x*DEG_TO_RAD_F ) * gl_Position;

    if ( vLocalRot.y != 0.0 )
        gl_Position = rotationMatrix( vec3(0,-vLocalRot.y,0), vLocalRot.y*DEG_TO_RAD_F ) * gl_Position;

    if ( vLocalRot.z != 0.0 )
        gl_Position = rotationMatrix( vec3(0,0,-vLocalRot.z), vLocalRot.z*DEG_TO_RAD_F ) * gl_Position;

    gl_Position -= vec4( vLocalRotOffset, 1 );

    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;
    fUV = vUV;
    fColor = vColor;
}
