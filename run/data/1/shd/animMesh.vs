#version 120
#extension GL_EXT_gpu_shader4 : enable

//Shader for animating a static model..not true?..It uses bone data, how is it static?

const int MAX_BONES_IN_SKELETON = 40;

uniform mat4 M;
uniform mat4 P;
uniform mat2x4 Bone_DQ[MAX_BONES_IN_SKELETON]; // Dualquaternions for each bone
uniform vec4 vColor;
uniform vec3 vLocalRot;
uniform vec3 vLocalRotOffset;
attribute vec3 vPos;
attribute vec2 vUV;
attribute vec2 Bone_ID;  // Assumes each vert weighted to at most 2 bones.
attribute vec2 Bone_WGT;
varying vec2 fUV;
varying vec4 fColor;

#include debug.glsl
#include rotationMatrix.glsl
#include calcWeightedDQ.glsl
#include vecTimesDQ.glsl

void main() {

    float eps = 0.000001;

    float PI_F = 3.141593f;
    float DEG_TO_RAD_F = PI_F / 180.0f;

//JESSE TODO : find a way to get rid of this if statement for vertices
// that have groups assigned to them (i.e. the door frame on the slide door
    
    if (Bone_WGT.x > eps) {
        gl_Position = vec4(vecTimesDQ(vPos),1.0);
    } else {
        gl_Position = vec4(vPos,1.0);
    }

    gl_Position += vec4( vLocalRotOffset, 1 );
    
    if ( vLocalRot.x != 0.0 )
        gl_Position = rotationMatrix( vec3(-vLocalRot.x,0,0), vLocalRot.x*DEG_TO_RAD_F ) * gl_Position;

    if ( vLocalRot.y != 0.0 )
        gl_Position = rotationMatrix( vec3(0,-vLocalRot.y,0), vLocalRot.y*DEG_TO_RAD_F ) * gl_Position;

    if ( vLocalRot.z != 0.0 )
        gl_Position = rotationMatrix( vec3(0,0,-vLocalRot.z), vLocalRot.z*DEG_TO_RAD_F ) * gl_Position;

    gl_Position -= vec4( vLocalRotOffset, 1 );

    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;

    fUV = vUV;
    fColor = vColor;
}
