#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec3 vPos;
attribute vec4 vColor;
attribute float vSize;
varying vec4 fColor;

void main()
{
    gl_PointSize = vSize;
    gl_Position = P * (M * vec4(vPos,1.0));
    fColor = vColor;
}
