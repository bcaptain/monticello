#version 120

uniform mat4 M;
uniform mat4 P;
attribute vec4 vColor;
attribute vec2 vPos;
attribute vec2 vUV;
varying vec4 fColor;
varying vec2 fUV;

void main()
{
    gl_Position = P * (M * vec4(vPos,0.0,1.0));
    fColor = vColor;
    fUV = vUV;
}
