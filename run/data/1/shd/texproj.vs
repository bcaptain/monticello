#version 120

uniform mat4 M;
uniform mat4 P;
uniform mat4 texMat;
uniform mat4 invViewMat;
attribute vec3 vPos;
attribute vec2 vUV;
varying vec2 fUV;
varying vec4 textureCoordProj;

void main()
{
    vec4 posWorld = invViewMat * M * vec4(vPos, 1.0);
    textureCoordProj = texMat * posWorld;
    
    gl_Position = vec4(vPos,1.0);

    gl_Position = M * gl_Position;
    gl_Position = P * gl_Position;
    fUV = vUV;
}
