#version 120

uniform sampler2D tex0; // Base Texture
uniform sampler2D tex1; // Projected Texture
uniform float opacity;
varying vec2 fUV;
varying vec4 textureCoordProj;

void main()
{
    vec2 finalCoord = textureCoordProj.st / textureCoordProj.q;
    vec4 texColor = texture2D( tex0, fUV ).rgba;
    
    vec4 textureColorProj;
    
    if ( textureCoordProj.w > 0 ) {
        if ( textureCoordProj.s > 0 && textureCoordProj.t > 0 && finalCoord.s < 1 && finalCoord.t < 1 ) { //Manual clamping of texture to avoid repeating
            textureColorProj = texture2D( tex1, finalCoord ).rgba * opacity;
            gl_FragColor = mix(texColor, textureColorProj, textureColorProj.a); //Alpha blending
        } else { 
            gl_FragColor = texColor;
        }
    } else {
        gl_FragColor = texColor;
    }
}
