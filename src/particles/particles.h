// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_PARTICLES_PARTICLES_H_
#define SRC_PARTICLES_PARTICLES_H_

#include "../base/main.h"
#include "../rendering/material.h"

extern const uint PARTICLE_MIN_CEASE;
extern const uint PARTICLE_MAX_CEASE;
extern const uint PARTICLE_MIN_DELAY;
extern const uint PARTICLE_MAX_DELAY;
extern const uint PARTICLE_MIN_QTY;
extern const uint PARTICLE_MAX_QTY;
extern const float PARTICLE_ROTATE_MIN_ANG_PER_SEC;
extern const float PARTICLE_ROTATE_MAX_ANG_PER_SEC;
extern const float PARTICLE_MIN_OPACITY;
extern const float PARTICLE_MAX_OPACITY;
extern const float PARTICLE_MIN_SCALE;
extern const float PARTICLE_MAX_SCALE;
extern const uint PARTICLE_MIN_LIFESPAN;
extern const uint PARTICLE_MAX_LIFESPAN;
extern const std::string PARTICLE_DEFAULT_MATERIAL;

typedef Uint8 EffectStateT_BaseType;
enum class EffectStateT : EffectStateT_BaseType;

class Effect;
template< typename TYPE >
class VBO;

typedef Uint8 ParticleStuntsT_BaseType;
enum class ParticleStuntsT : ParticleStuntsT_BaseType {
    None = 0,
    RandomAngle = 1,
    Dest = 1<<1,
    Dest2 = 1<<2
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( ParticleStuntsT, ParticleStuntsT_BaseType )

class Particle {
private:
    struct ParticleBehavior {
        ParticleBehavior( void );
        ~ParticleBehavior( void ) = default;
        ParticleBehavior( const ParticleBehavior& other ) = default;
        ParticleBehavior& operator=( const ParticleBehavior& other ) = default;

        uint life_span_ms;
        uint quantity;
        ParticleStuntsT stunts;
        Vec3f color;
        float color_weight;
        Vec3f color_to;
        float color_to_weight;
        float scale_to;
        float scale_from;
        float rot_dps_min;
        float rot_dps_max;
        float bunching_frac;
        float opacity_from;
        float opacity_mid;
        float opacity_to;
        Vec3f origin;
        float origin_distrib_xy;
        float origin_distrib_z;
        Vec3f dest;
        float dest_distrib_xy;
        float dest_distrib_z;
        Vec3f dest2;
        float dest2_distrib_xy;
        float dest2_distrib_z;
        std::shared_ptr< const Material > material;
        bool hide;
        bool face_camera;
    };

    struct ParticleState {
        ParticleState( void );

        Vec3f origin;
        Vec3f dest;
        Vec3f dest2;

        uint birth_time;
        float rot_ang_initial;
        float rot_ang_per_sec;
    };

public:
    Particle( void );
    ~Particle( void );
    Particle( const Particle& other );

    static void Init( void );
    static void Deinit( void );

    bool Draw( const EffectStateT effectState, const Uint32 curTime );
    void Setup( const EffectStateT effectState, const Uint32 curTime );
    void Reset( const Uint32 curTime );

public:
    Particle& operator=( const Particle& other );

public:
    void Save( ParserBinary& parser, File& opened_fx_file );
    void Load( ParserBinary& parser, File& opened_fx_file );

    void SetMaterial( const std::shared_ptr< const Material >& to );
    std::shared_ptr< const Material > GetMaterial( void ) const;
    std::string GetMaterialName( void ) const;

    void UnsetColorize( void );
    void SetColorize( const Vec3f& newColor );
    void SetColorize( const float colorX, const float colorY, const float colorZ );
    Vec3f GetColorize( void ) const;
    void SetColorizeWeight( const float weight_percent );
    float GetColorizeWeight( void ) const;

    void UnsetColorizeTo( void );
    void SetColorizeTo( const Vec3f& newColorTo );
    void SetColorizeTo( const float colorToX, const float colorToY, const float colorToZ );
    Vec3f GetColorizeTo( void ) const;
    void SetColorizeToWeight( const float weight_percent );
    float GetColorizeToWeight( void ) const;

    void SetScaleFrom( const float scaleFrom );
    void SetScaleTo( const float scaleTo );

    float GetScaleFrom( void ) const;
    float GetScaleTo( void ) const;

    void SetLifeSpan( const uint lifeMS );
    uint GetLifeSpan( void ) const;

    void SetBunching( const float bunchingFrac );
    float GetBunching( void ) const;

    void SetQuantity( const uint qty );
    uint GetQuantity( void ) const;

    void SetOpacityFrom( const float fromFrac ); //!< domain: zero to one
    void SetOpacityMid( const float midFrac ); //!< domain: zero to one
    void SetOpacityTo( const float toFrac ); //!< domain: zero to one

    float GetOpacityFrom( void ) const;
    float GetOpacityMid( void ) const;
    float GetOpacityTo( void ) const;

    void SetRandomInitialAngle( void );
    void UnsetRandomInitialAngle( void );
    bool GetRandomInitialAngle( void ) const;

    void SetRotationFrom( const float speedFrom );
    void SetRotationTo( const float speedTo );

    float GetRotationFrom( void ) const;
    float GetRotationTo( void ) const;

    void SetOrigin( const Vec3f& to );
    void SetOrigin( const float x, const float y, const float z );
    Vec3f GetOrigin( void ) const;

    void SetOriginDistribXYRadius( const float radius );
    void UnsetOriginDistribXYRadius( void );
    float GetOriginDistribXYRadius( void ) const;

    void SetOriginDistribZRadius( const float radius );
    void UnsetOriginDistribZRadius( void );
    float GetOriginDistribZRadius( void ) const;

    void SetDest( const Vec3f& to );
    void SetDest( const float x, const float y, const float z );
    void UnsetDest( void );
    Vec3f GetDest( void ) const;
    bool IsDestSet( void ) const;

    void SetDest2( const Vec3f& to );
    void SetDest2( const float x, const float y, const float z );
    void UnsetDest2( void );
    Vec3f GetDest2( void ) const;
    bool IsDest2Set( void ) const;
    
    void SetFaceCamera( const bool whether );
    bool GetFaceCamera( void ) const;

    void SetDestDistribXYRadius( const float radius );
    void UnsetDestDistribXYRadius( void );
    float GetDestDistribXYRadius( void ) const;

    void SetDestDistribZRadius( const float radius );
    void UnsetDestDistribZRadius( void );
    float GetDestDistribZRadius( void ) const;

    void SetDest2DistribXYRadius( const float radius );
    void UnsetDest2DistribXYRadius( void );
    float GetDest2DistribXYRadius( void ) const;

    void SetDest2DistribZRadius( const float radius );
    void UnsetDest2DistribZRadius( void );
    float GetDest2DistribZRadius( void ) const;
    
    void Hide( void );
    void Show( void );
    bool IsHidden( void ) const;

    void SetDelay( const uint ms );
    uint GetDelay( void ) const;

    void SetCease( const uint ms );
    uint GetCease( void ) const;

private:
    void DeleteValues( void );

private:
    std::shared_ptr< ParticleBehavior > behavior; //!< behaviorset, only one copy in memory for all particles of each kind
    ParticleState **values; //!< array of pointers to one object each( for fast sorting via pointer swapping ).
    std::size_t allocated;
    uint delay; //!< delay for the whole particle to start
    uint startTime; //!< keeping track of when the particle started, for use with 'cease'
    uint cease; //!< time after the start of the particle (plus delay) that the whole thing should stop emitting

    static VBO<float>* vbo;
    static VBO<float>* vbo_uv;
};

#endif  // SRC_PARTICLES_PARTICLES_H_
