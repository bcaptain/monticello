// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./effect.h"
#include "../entities/emitter.h"
#include "./particles.h"
#include "../game.h"
#include "../sprites/spriteList.h"
#include "../rendering/renderer.h"

const uint EFFECT_DEFAULT_DURATION = 1000;
const uint EFFECT_MIN_DURATION = 0;
const uint EFFECT_MAX_DURATION = 5000;

Effect::Effect( void )
    : particles()
    , name()
    , startTime(0)
    , origin()
    , duration( EFFECT_DEFAULT_DURATION )
    , owner()
    {
}

Effect::Effect( const Effect& other )
    : particles( other.particles )
    , name( other.name )
    , startTime( other.startTime )
    , origin( other.origin )
    , duration( other.duration )
    , owner( other.owner )
    {
}

Effect::~Effect( void ) {
}

#ifdef MONTICELLO_EDITOR
    void Effect::DrawPreview( const EffectStateT effectState, const Uint32 curTime ) {
        bool facing_camera = false;
        if ( Link<Particle>* particlelink = particles.GetFirst() ) {
            if ( particlelink != nullptr ) {
                if ( particlelink->Data().GetFaceCamera() ) {
                    facing_camera = true;
                    renderer->PushMatrixMV();
                    renderer->ModelView().Translate( origin );
                    Vec3f translation( renderer->ModelView().GetTranslation() );
                    renderer->ModelView().LoadIdentity();
                    renderer->ModelView().SetTranslation( translation );
                }
            }
        }
        
        renderer->ModelView().Translate( origin );
        
        for ( auto& particle : particles )
            particle.Draw( effectState, curTime );

        renderer->ModelView().Translate( -origin );
        
        if ( facing_camera )
            renderer->PopMatrixMV();
    }
#endif // MONTICELLO_EDITOR

bool Effect::Draw( const Uint32 curTime ) {
    
    std::shared_ptr< Entity > owner_sptr = owner.lock();
    if ( owner_sptr )
        origin = owner_sptr->GetOrigin();
    
    bool facing_camera = false;
    if ( Link<Particle>* particlelink = particles.GetFirst() ) {
        if ( particlelink != nullptr ) {
            if ( particlelink->Data().GetFaceCamera() ) {
                facing_camera = true;
                renderer->PushMatrixMV();
                renderer->ModelView().LoadIdentity();
            }
        }
    }
    
    renderer->ModelView().Translate( origin );
        
    bool drawn = false;

    EffectStateT effectState;

    if ( ( duration != LOOP_EFFECT_FOREVER ) && ( curTime >= (startTime+duration) ) ) {
        effectState = EffectStateT::Dying;
    } else {
        effectState = EffectStateT::Alive;
    }

    for ( auto& particle : particles ) {
        drawn = particle.Draw( effectState, curTime ) || drawn;
    }

    renderer->ModelView().Translate( -origin );
    
    if ( facing_camera )
        renderer->PopMatrixMV();

    return drawn;
}

void Effect::Reset( const Uint32 curTime ) {
    for ( auto& particle : particles )
        particle.Reset( curTime );
}

Effect& Effect::operator=( const Effect& other ) {
    particles = other.particles;
    name = other.name;
    startTime = other.startTime;
    origin = other.origin;
    duration = other.duration;
    return *this;
}

LinkList<Particle>& Effect::GetParticlesListRef( void ) {
    return particles;
}

const LinkList<Particle>& Effect::GetParticlesListRef( void ) const {
    return particles;
}

uint Effect::GetDuration( void ) const {
    return duration;
}

void Effect::SetDuration( const uint duration_ms ) {
    duration = duration_ms;
}

void Effect::SetStartTime( const uint to ) {
    startTime = to;
}

void Effect::SetOrigin( const Vec3f& to ) {
    origin = to;
}

uint Effect::GetStartTime( void ) const {
    return startTime;
}

Vec3f Effect::GetOrigin( void ) const {
    return origin;
}

Link<Particle>* Effect::AddParticle( void ) {
    return particles.Append();
}

void Effect::SetName( const char *n ) {
    name = n;
}

void Effect::SetName( const std::string& n ) {
    name = n;
}

std::string Effect::GetName( void ) const {
    return name;
}

void Effect::SetOwner( const std::weak_ptr< Entity > ent ) {
    owner = ent;
}
