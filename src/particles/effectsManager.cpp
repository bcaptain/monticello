// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./effectsManager.h"
#include "../game.h"

EffectsManager effectsManager;

bool EffectsManager::instantiated(false); // effects that are rawn

EffectsManager::EffectsManager( void )
    : AssetManager( "Effect" )
    , drawn()
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

void EffectsManager::ProcessAndDraw( void ) {
    Link< std::shared_ptr< Effect > > *lnk = drawn.GetFirst();
    std::shared_ptr< Effect > fx;

    const Uint32 curTime = game->GetGameTime();

    while ( lnk != nullptr ) {
        fx = lnk->Data();

        if ( ! fx->Draw( curTime ) ) {
            //Debug message: CMSG("Effect expired. Number of effects in world: %u\n", drawn.Num() );
            lnk = lnk->Del();
        } else {
            lnk = lnk->GetNext();
        }
    }
}

std::shared_ptr< Effect > EffectsManager::GetNonconst( const char* effect_name ) {
    if ( ! EnsureLoaded( effect_name ) )
        return nullptr;

    AssetData< Effect >* sptr = manifest.Get( effect_name );
    if ( !sptr )
        return nullptr;

    return sptr->data;
}

std::weak_ptr< Effect > EffectsManager::Spawn( const char* effect_name, const Vec3f& origin ) {
    const std::shared_ptr< const Effect > fx = Get( effect_name );

    if ( !fx ) {
        ERR("Couldn't spawn effect, doesn't exist: %s.\n", effect_name);
        return std::shared_ptr<Effect>();
    }

    return Spawn( *fx, origin );
}

std::weak_ptr< Effect > EffectsManager::Spawn( const Effect& fx, const Vec3f& origin ) {
    std::shared_ptr< Effect > spawned_fx = drawn.Append( std::make_shared< Effect >( fx ) )->Data();
    spawned_fx->SetStartTime( game->GetGameTime() );
    spawned_fx->SetOrigin( origin );
    return spawned_fx;
}

std::shared_ptr< Effect > EffectsManager::Create( const char* effect_name ) {
    if ( effect_name == nullptr || effect_name[0] == '\0' )
        return nullptr;

    AssetData< Effect >* sptr = manifest.Get( effect_name );
    if ( !sptr ) {
        RegisterName( effect_name );
        sptr = manifest.Get( effect_name );
    }

    ASSERT( sptr );

    if ( sptr->data ) {
        WARN("Not creating effect, another one already exists by that name: %s.\n", effect_name );
    } else {
        sptr->data = std::make_shared< Effect >();
    }

    return sptr->data;
}

bool EffectsManager::Save( const char* effect_name, const std::shared_ptr< Effect >& effect_data ) {
    if ( !effect_name || effect_name[0] == '\0' ) {
        ERR("Can't save effect without a name provided.\n");
        return false;
    }

    AssetData< Effect >* sptr = manifest.Get( effect_name );


    // ** create new effect by that name if it doesn't exist
    if ( !sptr ) {
        ERR("Creating new effect: %s\n", effect_name );

        std::shared_ptr< Effect > effect = Create( effect_name );
        if ( effect == nullptr ) {
            ERR("Couldn't create effect: %s\n", effect_name);
            return false;
        }

        *effect = *effect_data;
        effect->SetName( effect_name );

        sptr = manifest.Get( effect_name );
        ASSERT( sptr );
    }

    if ( !sptr->data ) {
        ERR("Cannot save effect, it is manifested but doesn't exist: %s\n", effect_name );
        return false;
    }

    // if the file was loaded from an archive or there is no filepath, save it to the default location on the filesystem
    if ( !sptr->GetContainer().empty() || sptr->GetPath().empty() ) {
        sptr->SetContainer( "" );

        // stick it in the default directory
        std::string path( DataDir );
        path.append("1/fx/");
        if ( ! FileSystem::CreateDir( path.c_str() ) )
            return false;

        path += effect_name;
        path += ".fx";
        sptr->SetPath( path );
    }

    File file( sptr->GetPath().c_str(), "wb" );
    if ( !file.IsOpen() ) {
        ERR("Could not open file %s to save effect %s\n", sptr->GetPath().c_str(), effect_name );
        return false;
    }

    ParserBinary parser( file );

    std::shared_ptr< Effect > &effect = sptr->data;

    parser.WriteVec3f( effect->GetOrigin() );
    parser.WriteUInt( effect->GetDuration() );
    parser.WriteUInt( effect->GetParticlesListRef().Num() );

    Link< Particle >* plink = effect->GetParticlesListRef().GetFirst();
    while ( plink != nullptr ) {
        plink->Data().Save( parser, file );
        plink = plink->GetNext();
    }

    file.Close();
    return true;
}

bool EffectsManager::Load( File& opened_file ) {
    // ** make sure it's open

    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open model file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    AssetData< Effect >* asset = manifest.Get( name.c_str() );
    if ( !asset ) {
        ERR_DIALOG("Couldn't load effect file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }
    
    auto effect = std::make_shared< Effect >();
    effect->SetName( name );
    Load( *effect, opened_file );
    
    if ( asset->data )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );

    asset->data = effect;
    return true;
}

void EffectsManager::Reload( const char* effect_name ) {
    AssetData< Effect >* sptr = manifest.Get( effect_name );
    if ( !sptr ) {
        ERR("Cannot reload effect, there is none by that name: %s\n", effect_name );
        return;
    }

    if ( !sptr->data ) {
        ERR("Cannot reload effect, it is manifested but doesn't exist: %s\n", effect_name );
        return;
    }

    File file( sptr->GetPath().c_str(), "rb" );
    OpenAssetFile( asset_type_name.c_str(), *sptr, file );

    if ( !file.IsOpen() ) {
        ERR("Could not open file %s to reload effect %s\n", sptr->GetPath().c_str(), effect_name );
        return;
    }

    std::shared_ptr< Effect > &effect = sptr->data;

    if ( !effect )
        effect = std::make_shared< Effect >();

    effect->GetParticlesListRef().DelAll();

    Load( *effect, file );
    file.Close();
}

void EffectsManager::Load( Effect& effect, File& opened_file ) {
    ASSERT( opened_file.IsOpen() );

    ParserBinary parser( opened_file );

    effect.SetOrigin( parser.ReadVec3f() );
    effect.SetDuration( parser.ReadUInt() );
    uint num_particles = parser.ReadUInt();

    LinkList< Particle > &list = effect.GetParticlesListRef();

    while ( num_particles > 0 ) {
        list.Append()->Data().Load( parser, opened_file );
        --num_particles;
    }

    CMSG_LOG("Loaded effect: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
}
