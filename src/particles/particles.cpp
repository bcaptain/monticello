// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./effect.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../rendering/materialManager.h"
#include "../rendering/vbo.h"

const uint PARTICLE_MIN_CEASE = 0; // zero means never
const uint PARTICLE_MAX_CEASE = 5_s;
const uint PARTICLE_MIN_DELAY = 0;
const uint PARTICLE_MAX_DELAY = 3_s;
const uint PARTICLE_MIN_QTY = 1;
const uint PARTICLE_MAX_QTY = 500_ms;
const float PARTICLE_ROTATE_MIN_ANG_PER_SEC = -360;
const float PARTICLE_ROTATE_MAX_ANG_PER_SEC = 360;
const float PARTICLE_MIN_OPACITY = 0.0f;
const float PARTICLE_MAX_OPACITY = 1.0f;
const float PARTICLE_MIN_SCALE = 0.0f;
const float PARTICLE_MAX_SCALE = 3.0f;
const uint PARTICLE_MIN_LIFESPAN = 50_ms;
const uint PARTICLE_MAX_LIFESPAN = 5_s;
const std::string PARTICLE_DEFAULT_MATERIAL = "prt_blood";

VBO<float>* Particle::vbo(nullptr);
VBO<float>* Particle::vbo_uv(nullptr);

Particle::ParticleState::ParticleState( void )
    : origin()
    , dest()
    , dest2()
    , birth_time(0)
    , rot_ang_initial(0.0f)
    , rot_ang_per_sec(0.0f)
{ }

Particle::ParticleBehavior::ParticleBehavior( void )
    : life_span_ms(1000)
    , quantity(1)
    , stunts( ParticleStuntsT::None )
    , color()
    , color_weight(0.0f)
    , color_to()
    , color_to_weight(0.0f)
    , scale_to(1.0f)
    , scale_from(1.0f)
    , rot_dps_min(0)
    , rot_dps_max(0)
    , bunching_frac(0.0f)
    , opacity_from(1.0f)
    , opacity_mid(1.0f)
    , opacity_to(1.0f)
    , origin()
    , origin_distrib_xy(0.0f)
    , origin_distrib_z(0.0f)
    , dest()
    , dest_distrib_xy(0.0f)
    , dest_distrib_z(0.0f)
    , dest2()
    , dest2_distrib_xy(0.0f)
    , dest2_distrib_z(0.0f)
    , material( materialManager.Get( PARTICLE_DEFAULT_MATERIAL.c_str() ) )
    , hide( false )
    , face_camera(false)
{
    ASSERT( material );
    ASSERT( material->GetDiffuse() );
}

Particle::Particle( void )
    : behavior( new ParticleBehavior )
    , values(nullptr)
    , allocated(0)
    , delay(0)
    , startTime(0)
    , cease(0)
{
}

Particle::Particle( const Particle& other )
    : behavior( other.behavior )
    , values(nullptr)
    , allocated(0)
    , delay(other.delay)
    , startTime(0)
    , cease(0)
{
}

Particle& Particle::operator=( const Particle& other ) {
    behavior = other.behavior;
    DeleteValues(); // this is fine if &other == this since values are regenerated
    allocated = 0;
    delay = other.delay;
    startTime = other.startTime;
    cease = other.cease;
    return *this;
}

Particle::~Particle( void ) {
    DeleteValues();
}

void Particle::Init( void ) {
    ASSERT_MSG( !vbo, "Initialized particle system twice.\n" );
    ASSERT_MSG( !vbo_uv, "Initialized particle system twice.\n" );

    vbo = new VBO<float>( 3, VBOChangeFrequencyT::FREQUENTLY );
    vbo_uv = new VBO<float>( 2, VBOChangeFrequencyT::RARELY );

    const float UV_BOTTOM = 0.0f;
    const float UV_LEFT = 0.0f;
    const float UV_TOP = 1.0f;
    const float UV_RIGHT = 1.0f;

    vbo_uv->Pack( UV_BOTTOM );
    vbo_uv->Pack( UV_LEFT );
    vbo->Pack( -1.0f );
    vbo->Pack( -1.0f );
    vbo->Pack( 0.0f );

    vbo_uv->Pack( UV_BOTTOM );
    vbo_uv->Pack( UV_RIGHT );
    vbo->Pack( -1.0f );
    vbo->Pack( 1.0f );
    vbo->Pack( 0.0f );

    vbo_uv->Pack( UV_TOP );
    vbo_uv->Pack( UV_LEFT );
    vbo->Pack( 1.0f );
    vbo->Pack( -1.0f );
    vbo->Pack( 0.0f );

    vbo_uv->Pack( UV_TOP );
    vbo_uv->Pack( UV_RIGHT );
    vbo->Pack( 1.0f );
    vbo->Pack( 1.0f );
    vbo->Pack( 0.0f );

    vbo->MoveToVideoCard();
    vbo_uv->MoveToVideoCard();

    ASSERT( vbo->Finalized() );
    ASSERT( vbo_uv->Finalized() );
}

void Particle::Deinit( void ) {
    delete Particle::vbo;
    delete Particle::vbo_uv;
}

std::string Particle::GetMaterialName( void ) const {
    if (! behavior->material )
        return std::string();

    return behavior->material->GetName();
}

void Particle::SetQuantity( const uint qty ) {
    if ( qty == behavior->quantity )
        return;

    behavior->quantity = qty;

    DeleteValues();
}

void Particle::DeleteValues( void ) {
    while ( allocated>0 ) {
        --allocated;
        delete values[allocated];
    }

    DELNULLARRAY( values );
}

void Particle::SetDest( const float x, const float y, const float z ) {
    behavior->stunts |= ParticleStuntsT::Dest;
    behavior->dest.Set( x, y, z );
}

void Particle::SetDest2( const float x, const float y, const float z ) {
    behavior->stunts |= ParticleStuntsT::Dest2;
    behavior->dest2.Set( x, y, z );
}

void Particle::SetFaceCamera( const bool face_cam ) {
    behavior->face_camera = face_cam;
}

void Particle::Reset( const Uint32 curTime ) {
    startTime = curTime + delay;

    // reset birth_time to zero so the Setup function knows we're starting from scratch
    if ( values )
        for ( uint i=0; i<allocated; ++i )
            values[i]->birth_time = 0;

    Setup( EffectStateT::Initialize, curTime );
}

void Particle::Setup( const EffectStateT effectState, const Uint32 curTime ) {
    uint i;

    bool force = ( effectState == EffectStateT::Initialize );

    // initial setup
    if ( !values || allocated != behavior->quantity ) {
        DeleteValues();
        values = new ParticleState*[behavior->quantity];
        allocated = behavior->quantity;

        for ( i=0; i<allocated; ++i )
            values[i] = new ParticleState;

        force = true;
    }

    // if time is beyond the cease time
    if ( cease > 0 && curTime > startTime + delay + cease )
        return;

    // if time is before our start time
    if ( curTime < startTime + delay )
        return;

    uint die_time;

    // frame-ly checks
    for ( i=0; i<allocated; ++i ) {
        // todo: because the birth_time gets changed when a particle dies, it will be set to a time in the future.
        // due to this, we are unable to use (curTime - prevtime < duration) math to ignore integer wrapping.
        // particles may get funky after the integer wraps...

        die_time = values[i]->birth_time + behavior->life_span_ms;

        if ( effectState == EffectStateT::Dying )
            continue;

        if ( !force ) {
            if ( curTime < die_time ) {
                continue;
            }
        }

        values[i]->birth_time = curTime;

        values[i]->origin = behavior->origin;
        if ( behavior->origin_distrib_xy > 0.0f ) {
            values[i]->origin.x += Random::Float( -behavior->origin_distrib_xy, behavior->origin_distrib_xy );
            values[i]->origin.y += Random::Float( -behavior->origin_distrib_xy, behavior->origin_distrib_xy );
        }

        if ( behavior->origin_distrib_z > 0.0f ) {
            values[i]->origin.z += Random::Float( -behavior->origin_distrib_z, behavior->origin_distrib_z );
        }

    //TODO Brandon from Jesse: In the effects editor this "dest" stuff doesn't say anything about it being random.  Right now I'm trying to get a fixed behavior, so I'm disabling for now.
         /*
            if ( BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest ) ) {
                values[i]->dest = behavior->dest;
                if ( behavior->dest_distrib_xy > 0.0f ) {
                    values[i]->dest.x += Random::Float( -behavior->dest_distrib_xy, behavior->dest_distrib_xy );
                    values[i]->dest.y += Random::Float( -behavior->dest_distrib_xy, behavior->dest_distrib_xy );
                }
                if ( behavior->dest_distrib_z > 0.0f ) {
                    values[i]->dest.z += Random::Float( -behavior->dest_distrib_z, behavior->dest_distrib_z );
                }
            }

            if ( BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest2 ) ) {
                values[i]->dest2 = behavior->dest2;
                if ( behavior->dest2_distrib_xy > 0.0f ) {
                    values[i]->dest2.x += Random::Float( -behavior->dest2_distrib_xy, behavior->dest2_distrib_xy );
                    values[i]->dest2.y += Random::Float( -behavior->dest2_distrib_xy, behavior->dest2_distrib_xy );
                }
                if ( behavior->dest2_distrib_z > 0.0f ) {
                    values[i]->dest2.z += Random::Float( -behavior->dest2_distrib_z, behavior->dest2_distrib_z );
                }
            }
        */
        
            if ( BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest ) ) {
                values[i]->dest = behavior->dest;
                if ( !Maths::Approxf( behavior->dest_distrib_xy, 0.0f ) ) {
                    values[i]->dest.x += behavior->dest_distrib_xy;
                    values[i]->dest.y += behavior->dest_distrib_xy;
                }
                if ( !Maths::Approxf( behavior->dest_distrib_z, 0.0f ) ) {
                    values[i]->dest.z += behavior->dest_distrib_z;
                }
            }

            if ( BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest2 ) ) {
                values[i]->dest2 = behavior->dest2;
                if ( !Maths::Approxf( behavior->dest2_distrib_xy, 0.0f ) ) {
                    values[i]->dest2.x += behavior->dest2_distrib_xy;
                    values[i]->dest2.y += behavior->dest2_distrib_xy;
                }
                if ( !Maths::Approxf( behavior->dest2_distrib_z, 0.0f ) ) {
                    values[i]->dest2.z += behavior->dest2_distrib_z;
                }
            }
        
    //End TODO
        
        if ( behavior->bunching_frac > 0.0f ) {
            float t = i;
            t /= allocated; // t is percentage of the way through all the particles
            t *= behavior->life_span_ms; // t is percentage of the way through the life_span
            t *= behavior->bunching_frac;

            values[i]->birth_time += static_cast<uint>( t );
        }

        values[i]->rot_ang_initial = BitwiseAnd( behavior->stunts, ParticleStuntsT::RandomAngle ) ? Random::Float( 0, 360 ) : 0.0f;

        if ( !Maths::Approxf( behavior->rot_dps_max, 0 ) || !Maths::Approxf( behavior->rot_dps_min, 0 ) ) {
            if ( Maths::Approxf( behavior->rot_dps_max - behavior->rot_dps_min, 0.0f ) ) {
                values[i]->rot_ang_per_sec = behavior->rot_dps_min;
            } else {
                values[i]->rot_ang_per_sec = Random::Float( behavior->rot_dps_min, behavior->rot_dps_max );
            }
        }
    }
}

bool Particle::Draw( const EffectStateT effectState, const Uint32 curTime ) {

    if ( IsHidden() )
        return false;

    if ( curTime < startTime + delay )
        return false;

    // ** basic shader stuff
    shaders->UseProg(  GLPROG_PARTICLE  );
    shaders->SendData_Matrices();

    if ( ! behavior->material || ! behavior->material->GetDiffuse() )
        return false;

    shaders->SetTexture( behavior->material->GetDiffuse()->GetID() );

    Setup( effectState, curTime );

    shaders->SetUniform1f("life_span_ms", behavior->life_span_ms );

    // ** draw each thing
    float life_frac; // percentage 0-1
    uint time_since_birth;

    bool do_rotate;
    bool do_opacity;

    shaders->SetAttrib( "vPos", *vbo );
    shaders->SetAttrib( "vUV", *vbo_uv );

    bool drawn = false;

    for ( uint i=0; i<allocated; ++i ) {

        if ( behavior->life_span_ms == 0 )
            continue;

        if ( curTime < values[i]->birth_time )
            continue;

        if ( curTime >= values[i]->birth_time + behavior->life_span_ms )
            continue;

        drawn = true;

        time_since_birth = curTime - values[i]->birth_time;
        life_frac = static_cast< float >( time_since_birth ) / behavior->life_span_ms;
        shaders->SetUniform1f("life_frac", life_frac );
        shaders->SetUniform3f("origin", values[i]->origin );

        shaders->SetUniform1i("stuntDest", BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest ) );
        if ( BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest ) ) {
            shaders->SetUniform3f("dest", values[i]->dest );

            shaders->SetUniform1i("stuntDest2", BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest2 ) );
            if ( BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest2 ) ) {
                shaders->SetUniform3f("dest2", values[i]->dest2 );
            }
        } else {
            shaders->SetUniform1i("stuntDest2", 0 );
        }

        shaders->SetUniform1f("fWeight", behavior->color_weight ); // must send the weight even if zero, so shader knows not to weight the color
        if ( behavior->color_weight > 0.0f ) {
            shaders->SetUniform3f("vColor", behavior->color );
        }

        shaders->SetUniform1f("fWeightTo", behavior->color_to_weight ); // must send the weight even if zero, so shader knows not to weight the color
        if ( behavior->color_to_weight > 0.0f ) {
            shaders->SetUniform3f("vColorTo", behavior->color_to );
        }

        shaders->SetUniform1f("scale_from", behavior->scale_from );
        shaders->SetUniform1f("scale_to", behavior->scale_to );
        shaders->SetUniform1f("rot_ang_initial", values[i]->rot_ang_initial );

        do_rotate = !Maths::Approxf( behavior->rot_dps_max, 0 ) || !Maths::Approxf( behavior->rot_dps_min, 0 );
        shaders->SetUniform1i("stuntRotate", do_rotate );
        if ( do_rotate ) {
            shaders->SetUniform1f("rot_ang_per_sec", values[i]->rot_ang_per_sec );
        }

        do_opacity = !(
            Maths::Approxf( behavior->opacity_to, 1.0f ) &&
            Maths::Approxf( behavior->opacity_from, 1.0f ) &&
            Maths::Approxf( behavior->opacity_mid, 1.0f )
        );

        shaders->SetUniform1i("stuntOpacity", do_opacity );
        if ( do_opacity ) {
            shaders->SetUniform1f("opacity_from", behavior->opacity_from );
            shaders->SetUniform1f("opacity_mid", behavior->opacity_mid );
            shaders->SetUniform1f("opacity_to", behavior->opacity_to );
        }

        shaders->DrawArrays( GL_TRIANGLE_STRIP, 0, vbo->Num() );
    }

    return drawn;
}

void Particle::Save( ParserBinary& parser, File& opened_fx_file ) {
    if ( !opened_fx_file.IsOpen() ) {
        ERR("Can't save particle, file not open: %s\n", opened_fx_file.GetFilePath().c_str() );
        return;
    }

    ASSERT( behavior );
    parser.WriteUInt8( static_cast< ParticleStuntsT_BaseType >( behavior->stunts ) );
    parser.WriteStr( GetMaterialName() );
    parser.WriteVec3f( GetColorize() );
    parser.WriteFloat( GetColorizeWeight() );
    parser.WriteVec3f( GetColorizeTo() );
    parser.WriteFloat( GetColorizeToWeight() );
    parser.WriteFloat( GetScaleFrom() );
    parser.WriteFloat( GetScaleTo() );
    parser.WriteUInt( GetLifeSpan() );
    parser.WriteFloat( GetBunching() );
    parser.WriteUInt( GetQuantity() );
    parser.WriteFloat( GetOpacityFrom() );
    parser.WriteFloat( GetOpacityMid() );
    parser.WriteFloat( GetOpacityTo() );
    parser.WriteBool( GetRandomInitialAngle() );
    parser.WriteFloat( GetRotationFrom() );
    parser.WriteFloat( GetRotationTo() );
    parser.WriteVec3f( GetOrigin() );
    parser.WriteFloat( GetOriginDistribXYRadius() );
    parser.WriteFloat( GetOriginDistribZRadius() );
    parser.WriteVec3f( GetDest() );
    parser.WriteVec3f( GetDest2() );
    parser.WriteFloat( GetDestDistribXYRadius() );
    parser.WriteFloat( GetDestDistribZRadius() );
    parser.WriteFloat( GetDest2DistribXYRadius() );
    parser.WriteFloat( GetDest2DistribZRadius() );
    parser.WriteUInt( GetDelay() );
    parser.WriteUInt( GetCease() );
    //parser.WriteBool( GetFaceCamera() );
}

void Particle::Load( ParserBinary& parser, File& opened_fx_file ) {
    if ( !opened_fx_file.IsOpen() ) {
        ERR("Can't load particle, file not open: %s\n", opened_fx_file.GetFilePath().c_str() );
        return;
    }

    ASSERT( behavior );
    behavior->stunts = static_cast< ParticleStuntsT >( parser.ReadUInt8() );
    SetMaterial( materialManager.Get( parser.ReadStr().c_str() ) );
    SetColorize( parser.ReadVec3f() );
    SetColorizeWeight( parser.ReadFloat() );
    SetColorizeTo( parser.ReadVec3f() );
    SetColorizeToWeight( parser.ReadFloat() );
    SetScaleFrom( parser.ReadFloat() );
    SetScaleTo( parser.ReadFloat() );
    SetLifeSpan( parser.ReadUInt() );
    SetBunching( parser.ReadFloat() );
    SetQuantity( parser.ReadUInt() );
    SetOpacityFrom( parser.ReadFloat() );
    SetOpacityMid( parser.ReadFloat() );
    SetOpacityTo( parser.ReadFloat() );
    if ( parser.ReadBool() ) {
        SetRandomInitialAngle();
    } else {
        UnsetRandomInitialAngle();
    }
    SetRotationFrom( parser.ReadFloat() );
    SetRotationTo( parser.ReadFloat() );
    SetOrigin( parser.ReadVec3f() );
    SetOriginDistribXYRadius( parser.ReadFloat() );
    SetOriginDistribZRadius( parser.ReadFloat() );
    SetDest( parser.ReadVec3f() );
    SetDest2( parser.ReadVec3f() );
    SetDestDistribXYRadius( parser.ReadFloat() );
    SetDestDistribZRadius( parser.ReadFloat() );
    SetDest2DistribXYRadius( parser.ReadFloat() );
    SetDest2DistribZRadius( parser.ReadFloat() );
    SetDelay( parser.ReadUInt() );
    SetCease( parser.ReadUInt() );
    //SetFaceCamera( parser.ReadBool() );

    CMSG_LOG("Loaded particle: %s from %s\n", opened_fx_file.GetFilePath().c_str(), opened_fx_file.GetFileContainerName_Full().c_str() );
}

uint Particle::GetDelay( void ) const {
    return delay;
}

uint Particle::GetCease( void ) const {
    return cease;
}

// **** Hiding

void Particle::Hide( void ) {
    behavior->hide = true;
}

void Particle::Show( void ) {
    behavior->hide = false;
}

bool Particle::IsHidden( void ) const {
    return behavior->hide;
}

bool Particle::GetFaceCamera( void ) const {
    return behavior->face_camera;
}

// **** Origin

void Particle::SetOrigin( const Vec3f& to ) {
    SetOrigin( to.x, to.y, to.z );
}

void Particle::SetOrigin( const float x, const float y, const float z ) {
    behavior->origin.Set( x, y, z );
}

Vec3f Particle::GetOrigin( void ) const {
    return behavior->origin;
}

// **** Origin Distribution

float Particle::GetOriginDistribXYRadius( void ) const {
    return behavior->origin_distrib_xy;
}

float Particle::GetOriginDistribZRadius( void ) const {
    return behavior->origin_distrib_xy;
}

void Particle::UnsetOriginDistribXYRadius( void ) {
    behavior->origin_distrib_xy = 0.0f;
}

void Particle::UnsetOriginDistribZRadius( void ) {
    behavior->origin_distrib_z = 0.0f;
}

void Particle::SetOriginDistribXYRadius( const float radius ) {
    behavior->origin_distrib_xy = radius;
}

void Particle::SetOriginDistribZRadius( const float radius ) {
    behavior->origin_distrib_z = radius;
}

// **** Destination

void Particle::SetDest( const Vec3f& to ) {
    SetDest( to.x, to.y, to.z );
}

void Particle::UnsetDest( void ) {
    behavior->stunts &= ~ParticleStuntsT::Dest;
}

Vec3f Particle::GetDest( void ) const {
    return behavior->dest;
}

bool Particle::IsDestSet( void ) const {
    return BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest );
}

// **** Destination Distribution

float Particle::GetDestDistribXYRadius( void ) const {
    return behavior->dest_distrib_xy;
}

float Particle::GetDestDistribZRadius( void ) const {
    return behavior->dest_distrib_z;
}

void Particle::UnsetDestDistribXYRadius( void ) {
    behavior->dest_distrib_xy = 0.0f;
}

void Particle::UnsetDestDistribZRadius( void ) {
    behavior->dest_distrib_z = 0.0f;
}

void Particle::SetDestDistribXYRadius( const float radius ) {
    behavior->dest_distrib_xy = radius;
}

void Particle::SetDestDistribZRadius( const float radius ) {
    behavior->dest_distrib_z = radius;
}

// **** Destination 2

void Particle::SetDest2( const Vec3f& to ) {
    SetDest2( to.x, to.y, to.z );
}

void Particle::UnsetDest2( void ) {
    behavior->stunts &= ~ParticleStuntsT::Dest2;
}

Vec3f Particle::GetDest2( void ) const {
    return behavior->dest2;
}

bool Particle::IsDest2Set( void ) const {
    return BitwiseAnd( behavior->stunts, ParticleStuntsT::Dest2 );
}

// **** Destination 2 Distribution

float Particle::GetDest2DistribXYRadius( void ) const {
    return behavior->dest2_distrib_xy;
}

float Particle::GetDest2DistribZRadius( void ) const {
    return behavior->dest2_distrib_xy;
}

void Particle::UnsetDest2DistribXYRadius( void ) {
    behavior->dest2_distrib_xy = 0.0f;
}

void Particle::UnsetDest2DistribZRadius( void ) {
    behavior->dest2_distrib_z = 0.0f;
}

void Particle::SetDest2DistribXYRadius( const float radius ) {
    behavior->dest2_distrib_xy = radius;
}

void Particle::SetDest2DistribZRadius( const float radius ) {
    behavior->dest2_distrib_z = radius;
}

// **** Scale

void Particle::SetScaleFrom( const float scaleFrom ) {
    behavior->scale_from = scaleFrom;
}

void Particle::SetScaleTo( const float scaleTo ) {
    behavior->scale_to = scaleTo;
}

float Particle::GetScaleFrom( void ) const {
    return behavior->scale_from;
}

float Particle::GetScaleTo( void ) const {
    return behavior->scale_to;
}

// **** Rotation

void Particle::SetRotationFrom( const float speedFrom ) {
    behavior->rot_dps_min = speedFrom;
}

void Particle::SetRotationTo( const float speedTo ) {
    behavior->rot_dps_max = speedTo;
}

float Particle::GetRotationFrom( void ) const {
    return behavior->rot_dps_min;
}

float Particle::GetRotationTo( void ) const {
    return behavior->rot_dps_max;
}

// **** Opacity

void Particle::SetOpacityFrom( const float fromFrac ) {
    behavior->opacity_from = fromFrac;
}

void Particle::SetOpacityMid( const float midFrac ) {
    behavior->opacity_mid = midFrac;
}

void Particle::SetOpacityTo( const float toFrac ) {
    behavior->opacity_to = toFrac;
}

float Particle::GetOpacityFrom( void ) const {
    return behavior->opacity_from;
}

float Particle::GetOpacityMid( void ) const {
    return behavior->opacity_mid;
}

float Particle::GetOpacityTo( void ) const {
    return behavior->opacity_to;
}

// **** Bunching

void Particle::SetBunching( const float bunchingFrac ) {
    behavior->bunching_frac = bunchingFrac;
}

float Particle::GetBunching( void ) const {
    return behavior->bunching_frac;
}

// **** Other

uint Particle::GetQuantity( void ) const {
    return behavior->quantity;
}

void Particle::SetMaterial( const std::shared_ptr< const Material >& to ) {
    behavior->material = to;
}

std::shared_ptr< const Material > Particle::GetMaterial( void ) const {
    return behavior->material;
}

// **** Colorize

void Particle::SetColorize( const Vec3f& newColor ) {
    SetColorize( newColor.x, newColor.y, newColor.z );
}

void Particle::SetColorize( const float colorX, const float colorY, const float colorZ ) {
    behavior->color.Set( colorX, colorY, colorZ );
}

void Particle::UnsetColorize( void ) {
    behavior->color_weight = 0.0f;
}

Vec3f Particle::GetColorize( void ) const {
    return behavior->color;
}

void Particle::SetColorizeWeight( const float colorize_weight ) {
    behavior->color_weight = colorize_weight;
}

float Particle::GetColorizeWeight( void ) const {
    return behavior->color_weight;
}

// **** Colorize To

void Particle::SetColorizeTo( const Vec3f& newColorTo ) {
    SetColorizeTo( newColorTo.x, newColorTo.y, newColorTo.z );
}

void Particle::SetColorizeTo( const float colorToX, const float colorToY, const float colorToZ ) {
    behavior->color_to.Set( colorToX, colorToY, colorToZ );
}

void Particle::UnsetColorizeTo( void ) {
    behavior->color_to_weight = 0.0f;
}

Vec3f Particle::GetColorizeTo( void ) const {
    return behavior->color_to;
}

void Particle::SetColorizeToWeight( const float colorize_weight ) {
    behavior->color_to_weight = colorize_weight;
}

float Particle::GetColorizeToWeight( void ) const {
    return behavior->color_to_weight;
}

// **** Start Angle

void Particle::UnsetRandomInitialAngle( void ) {
    behavior->stunts &= ~ParticleStuntsT::RandomAngle;
}

void Particle::SetRandomInitialAngle( void ) {
    behavior->stunts |= ParticleStuntsT::RandomAngle;
}

bool Particle::GetRandomInitialAngle( void ) const {
    return BitwiseAnd( behavior->stunts, ParticleStuntsT::RandomAngle );
}

// **** Life Span

void Particle::SetLifeSpan( const uint lifeMS ) {
    behavior->life_span_ms = lifeMS;
}

uint Particle::GetLifeSpan( void ) const {
    return behavior->life_span_ms;
}

void Particle::SetDelay( const uint ms ) {
    delay = ms;
}

void Particle::SetCease( const uint ms ) {
    cease = ms;
}
