// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_PARTICLES_EFFECTSMANAGER_H
#define SRC_PARTICLES_EFFECTSMANAGER_H

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../files/assetManager.h"
#include "./effect.h"

class EffectsManager : public AssetManager< AssetData<Effect>, Effect > {
public:
    EffectsManager( void );

public:
    void ProcessAndDraw( void );
    std::shared_ptr< Effect > GetNonconst( const char* effect_name );
    std::shared_ptr< Effect > Create( const char* effect_name );
    std::weak_ptr< Effect > Spawn( const char* effect_name, const Vec3f& origin );
    std::weak_ptr< Effect > Spawn( const Effect& fx, const Vec3f& origin );

    bool Save( const char* effect_name, const std::shared_ptr< Effect >& effect_data );
    void Reload( const char* effect_name );

private:
    bool Load( File& opened_damage_file ) override;
    void Load( Effect& effect, File& opened_file );
    using AssetManager< AssetData<Effect>,Effect >::Load;

private:
    static bool instantiated;

public:
    LinkList< std::shared_ptr< Effect > > drawn; //!< effects that are in-game
};

extern EffectsManager effectsManager;

#endif  // SRC_PARTICLES_EFFECTSMANAGER_H
