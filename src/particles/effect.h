// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_PARTICLES_EFFECT_H
#define SRC_PARTICLES_EFFECT_H

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./particles.h"

struct EffectEvent {
    EffectEvent( void ) = default;
    explicit EffectEvent( const std::shared_ptr< const Effect > _effect, const Vec3f& _origin_offset = {} ) : effect(_effect), origin_offset(_origin_offset) { }
    EffectEvent( const EffectEvent& other ) = default;
    ~EffectEvent( void ) = default;
    
    EffectEvent& operator=( const EffectEvent& other ) = default;
    
    std::shared_ptr< const Effect > effect;
    
    Vec3f origin_offset;
};

constexpr const uint LOOP_EFFECT_FOREVER = 0;

typedef Uint8 EffectStateT_BaseType;
enum class EffectStateT : EffectStateT_BaseType {
    Initialize
    , Alive
    , Dying
};

extern const uint EFFECT_DEFAULT_DURATION;
extern const uint EFFECT_MIN_DURATION;
extern const uint EFFECT_MAX_DURATION;

class Effect {
public:
    Effect( void );
    ~Effect( void );
    Effect( const Effect& other );

public:
    Effect& operator=( const Effect& other );
    Link<Particle>* AddParticle( void );
    bool Draw( const Uint32 curTime );
    void DrawPreview( const EffectStateT effectState, const Uint32 curTime );

    void SetName( const char *nam );
    void SetName( const std::string& nam );
    std::string GetName( void ) const;
    void SetStartTime( const uint to );
    uint GetStartTime( void ) const;
    void SetOrigin( const Vec3f& to );
    Vec3f GetOrigin( void ) const;
    uint GetDuration( void ) const;
    void SetDuration( const uint duration_ms );
    
    void SetOwner( const std::weak_ptr< Entity > ent );

    LinkList<Particle>& GetParticlesListRef( void );
    const LinkList<Particle>& GetParticlesListRef( void ) const;

    void Reset( const Uint32 curTime );

private:
    LinkList<Particle> particles; //!< the UI depends on this not being re-arranged anywhere but with UI controls
    std::string name;
    uint startTime;
    Vec3f origin;
    uint duration;
    std::weak_ptr< Entity > owner;
};

#endif  // SRC_PARTICLES_EFFECT_H
