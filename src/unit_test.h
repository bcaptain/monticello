// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifndef MONTICELLO_SRC_UNIT_TEST_H
#define MONTICELLO_SRC_UNIT_TEST_H

#include "./clib/src/warnings.h"
#ifdef CLIB_UNIT_TEST

    void UnitTest( void );

#endif // CLIB_UNIT_TEST

#endif // MONTICELLO_SRC_UNIT_TEST_H
