// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./console.h"
#include "./commandsys.h"
#include "./rendering/renderer.h"
#include "./rendering/vbo.h"
#include "./messages.h"
#include "./rendering/fontManager.h"

const uint CONSOLE_DISPLAY_LINES=23;
const uint CONSOLE_HISTORY_LINES=500;
const Vec3f CONSOLE_HISTORY_COMMAND_COLOR(0.3f, 0.3f, 1.0f); // blue
const Vec3f CONSOLE_HISTORY_MESSAGE_COLOR(0.3f, 1.0f, 0.3f); // green
const Vec3f CONSOLE_HISTORY_WARNING_COLOR(1.0f, 0.5f, 0.0f); // orange
const Vec3f CONSOLE_HISTORY_ERROR_COLOR(1.0f, 0.3f, 0.3f); // red
const Vec3f CONSOLE_FONT_COLOR(1.0f, 1.0f, 1.0f); // black

Console* console=nullptr;

HistoryElement::HistoryElement( void )
    : display( HistoryDisplayT::COMMAND )
    , string()
    {
}

HistoryElement::HistoryElement( const HistoryDisplayT display_type )
    : display( display_type )
    , string()
    {
}

HistoryElement::HistoryElement( const std::string& str, const HistoryDisplayT display_type )
    : display( display_type )
    , string( str )
    {
}

HistoryElement::HistoryElement( const char* str, const HistoryDisplayT display_type )
    : display( display_type )
    , string( str )
    {
}

Console::Console( void )
    : displayhistory()
    , firstDisplayLine( nullptr )
    , historySelection( nullptr )
    , vbo_textcolor( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_textuv( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_background( new VBO<float>(3,VBOChangeFrequencyT::RARELY) )
    , just_opened(false)
    {

    visible = false;
}

Console::~Console( void ) {
    delete vbo_textuv;
    delete vbo_textcolor;
    delete vbo_background;
}

void Console::Init( void ) {
    PopulateConsoleVarDescriptions();

    // ** show startup arguments in console
    std::string arg( "Startup Args: " );

    for ( int i=1; i< Arg::arg_num; ++i ) {
        arg += Arg::args[i];
        arg += " ";
    }
    arg += "\n";

    CMSG( arg.c_str() );
    LOG( arg.c_str() );
    
    if ( game->preConsoleMessages.size() < 1 )
        return;
    
    CMSG( "** Begin Messages generated before console was ready" );
    for ( auto const & msg : game->preConsoleMessages )
        Append( msg.first, msg.second );
    game->preConsoleMessages.clear();
    CMSG( "** End Messages generated before console was ready" );
}

void Console::DrawVBO( void ) const {
    ASSERT( vbo_textuv );
    ASSERT( vbo_secondary );
    ASSERT( vbo_background );

    // first check the font is valid since it may produce a console message (and thus clear our console VBOs)
    std::shared_ptr< const Font > fnt = fontManager.Get( fi.fontname.c_str() );
    Font::AssertValid( fnt, fi.fontname.c_str() );

    if ( !vbo_vert->Finalized()
        || !vbo_background->Finalized()
        || !vbo_secondary->Finalized()
        || !vbo_textuv->Finalized()
        || !vbo_textcolor->Finalized()
    ) {
        pack_vbo();
        if ( !vbo_vert->Finalized()
            || !vbo_background->Finalized()
            || !vbo_secondary->Finalized()
            || !vbo_textuv->Finalized()
            || !vbo_textcolor->Finalized()
        ) {
            ERR("Error packing Console VBO.\n");
            return;
        }
    }

    // ** Draw the background

    shaders->UseProg(  GLPROG_GRADIENT  );
    shaders->SendData_Matrices();
    shaders->SetAttrib( "vColor", *vbo_secondary );
    shaders->SetAttrib( "vPos", *vbo_background );
    shaders->DrawArrays( GL_QUADS, 0, vbo_background->Num() );

    // ** Draw the text

    shaders->UseProg(  GLPROG_MULTICOLORIZE_TEXTURE_2D  );
    shaders->SendData_Matrices();
    shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
    shaders->SetUniform1f("fWeight", 0.8f );
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->SetAttrib( "vUV", *vbo_textuv );
    shaders->SetAttrib( "vColor", *vbo_textcolor );
    shaders->DrawArrays( GL_QUADS, 0, vbo_vert->Num() );
}

void Console::pack_vbo( void ) const {
    ASSERT( vbo_vert );
    ASSERT( vbo_secondary );
    ASSERT( vbo_textuv );
    ASSERT( vbo_textcolor );
    // background doesnt change

    vbo_vert->Clear();
    vbo_textuv->Clear();
    vbo_textcolor->Clear();
    vbo_secondary->Clear();

    Vec2f& nextCharPosRef = const_cast< Vec2f& >( nextCharPos );


    static FontInfo font; /* this is static to avoid cost of frequent construction
                             not using inherited fi because Draw functions are const */

    uint num_faces_packed;
    nextCharPosRef.Zero();

    // ** start drawing low enough so that the bottom of the console is filled with text
    if ( displayhistory.Num() < CONSOLE_DISPLAY_LINES )
        nextCharPosRef.y += fi.size_ui_px * static_cast<float>( ( CONSOLE_DISPLAY_LINES - displayhistory.Num() ) );

    // ** draw visible history
    uint linesDrawn=0; // counter so we don't draw past the edge of the console when scrolling
    for ( const Link< HistoryElement >* itr=firstDisplayLine; itr != nullptr; itr = itr->GetNext() ) {
        if ( linesDrawn >= CONSOLE_DISPLAY_LINES )
            break;

        switch ( itr->Data().display ) {
            case HistoryDisplayT::COMMAND:
                font.color = CONSOLE_HISTORY_COMMAND_COLOR;
                break;
            case HistoryDisplayT::MESSAGE:
                font.color = CONSOLE_HISTORY_MESSAGE_COLOR;
                break;
            case HistoryDisplayT::WARNING:
                font.color = CONSOLE_HISTORY_WARNING_COLOR;
                break;
            case HistoryDisplayT::MSG_ERROR:
                font.color = CONSOLE_HISTORY_ERROR_COLOR;
                break;
            default:
                font.color = CONSOLE_FONT_COLOR;
                break;
        }

        num_faces_packed = vbo_vert->PackText( *vbo_textuv, itr->Data().string.c_str(), font, &nextCharPosRef);

        // pack colors
        if ( num_faces_packed > 0)
            vbo_textcolor->PackRepeatedly( num_faces_packed * 4, font.color );

        nextCharPosRef.x=0.0f;
        nextCharPosRef.y+=fi.size_ui_px;
        ++linesDrawn;
    }

    if ( label.size() > 0 ) {
        // ** draw current command line being typed

        font.color = CONSOLE_FONT_COLOR;
        num_faces_packed = vbo_vert->PackText(*vbo_textuv, label.c_str(), font, &nextCharPosRef);
    } else {
        // ** draw error / warning count

        std::string tmp;

        if ( Messages::errors > 0 ) {
            font.color = CONSOLE_HISTORY_ERROR_COLOR;
        } else {
            font.color = CONSOLE_FONT_COLOR;
        }

        tmp = "( ";
        tmp += std::to_string( Messages::errors );
        tmp += " Errors, ";
        num_faces_packed = vbo_vert->PackText(*vbo_textuv, tmp.c_str(), font, &nextCharPosRef);
        vbo_textcolor->PackRepeatedly( num_faces_packed * 4, font.color );

        if ( Messages::warnings > 0 ) {
            font.color = CONSOLE_HISTORY_WARNING_COLOR;
        } else {
            font.color = CONSOLE_FONT_COLOR;
        }

        tmp.erase();
        tmp += std::to_string( Messages::warnings );
        tmp += " Warnings )";
        num_faces_packed = vbo_vert->PackText(*vbo_textuv, tmp.c_str(), font, &nextCharPosRef);
        vbo_textcolor->PackRepeatedly( num_faces_packed * 4, font.color );
    }

    // pack colors
    if ( num_faces_packed > 0)
        vbo_textcolor->PackRepeatedly( num_faces_packed * 4, font.color );

    if ( !vbo_background->Finalized() ) {
        // floats promoted to double in variadic funcs
        const uint data_size=12;
        const float data[data_size]{
            0,   0,     0,
            800, 0,     0,
            800, 250,   0,
            0,   250.0, 0
        };
        vbo_background->PackArray(data_size,data);
    }

    const Vec3f console_top_color(0,0,0);
    const Vec3f console_bottom_color(0.1f,0.1f,0.1f);

    if ( !vbo_secondary->Finalized() ) {
        vbo_secondary->Pack( console_top_color );
        vbo_secondary->Pack( 1.0f );
        vbo_secondary->Pack( console_top_color );
        vbo_secondary->Pack( 1.0f );
        vbo_secondary->Pack( console_bottom_color );
        vbo_secondary->Pack( 1.0f );
        vbo_secondary->Pack( console_bottom_color );
        vbo_secondary->Pack( 1.0f );
    }

    vbo_vert->MoveToVideoCard();
    vbo_textuv->MoveToVideoCard();
    vbo_textcolor->MoveToVideoCard();
    vbo_secondary->MoveToVideoCard(); // background color
    vbo_background->MoveToVideoCard();
}

void Console::Append( const std::string& str, const HistoryDisplayT display_type ) {
    std::string& appended_line = displayhistory.Append( HistoryElement( str, display_type ) )->Data().string;
    
    // remove newlines so it shows on one line in the console
    String::Trim( appended_line );
    std::replace( begin( appended_line ), end( appended_line ), '\n', ' ' );

    if ( !firstDisplayLine )
        firstDisplayLine = displayhistory.GetFirst();
    else if ( displayhistory.Num() > CONSOLE_DISPLAY_LINES )
        firstDisplayLine = firstDisplayLine->GetNext();

    while ( displayhistory.Num() > CONSOLE_HISTORY_LINES )
        displayhistory.GetFirst()->Del();

    UpdateVBO();
}

void Console::PressEnter( void ) {
    Append( label );
    LOG("%s\n", label.c_str());
    CommandSys::Execute( label.c_str() );
    label.erase();
    historySelection = nullptr;
}

void Console::ClearHistory( void ) {
    displayhistory.DelAll();
    historySelection = nullptr;
    firstDisplayLine = nullptr;
}

// browse through previously typed commands
void Console::PressUp( void ) {
    do {
        if ( ! historySelection ) {
            historySelection = displayhistory.GetLast();
        } else {
            if ( ! historySelection->GetPrevious() )
                return;

            historySelection = historySelection->GetPrevious();
        }

    // browse past neighboring duplicates and empty lines
    } while (
        historySelection && (
            historySelection->Data().string.size() == 0 ||
            label == historySelection->Data().string ||
            historySelection->Data().display != HistoryDisplayT::COMMAND
        )
    );

    if ( ! historySelection )
        return;

    if ( historySelection->Data().display != HistoryDisplayT::COMMAND ) {
        historySelection = nullptr;
        return;
    }

    label = historySelection->Data().string;
}

// browse through previously typed commands
void Console::PressDown( void ) {
    if ( ! historySelection )
        return;

    do {
        // if there is nothing at the bottom of history, clear the label
        if ( ! historySelection->GetNext() ) {
            historySelection = nullptr;
            label.erase();
            return;
        }

        historySelection = historySelection->GetNext();

    // browse past neighboring duplicates and empty lines
    } while (
        historySelection->Data().string.size() == 0 ||
        label == historySelection->Data().string ||
        historySelection->Data().display != HistoryDisplayT::COMMAND );

    label = historySelection->Data().string;
}

// scroll through history
void Console::PressPageUp( void ) {
    // ** if holding shift, scroll straight to the top of the console buffer
    if ( SDL_GetModState() & KMOD_LSHIFT || SDL_GetModState() & KMOD_RSHIFT ) {
        if ( displayhistory.GetFirst() )
            firstDisplayLine = displayhistory.GetFirst();
        return;
    }

    if ( !firstDisplayLine )
        return;

    if ( ! firstDisplayLine->GetPrevious() )
        return;

    firstDisplayLine = firstDisplayLine->GetPrevious();
}

// scroll through history
void Console::PressPageDown( void ) {
    Link<HistoryElement>* itr;
    uint linesTillEnd;

    if ( ! firstDisplayLine )
        return;

    const bool HoldingShiftToScrollToBottom = SDL_GetModState() & KMOD_LSHIFT || SDL_GetModState() & KMOD_RSHIFT;

    if ( HoldingShiftToScrollToBottom ) {
        linesTillEnd = 1;
        for ( itr = displayhistory.GetLast(); itr; itr = itr->GetPrevious() ) {
            if ( linesTillEnd >= CONSOLE_DISPLAY_LINES && itr != nullptr ) {
                firstDisplayLine = itr;
                return;
            }
            ++linesTillEnd;
        }

        return;
    }

    // ** count lines from firstDisplayLine till the last entered, so as to not scroll too far
    linesTillEnd = 0;
    for ( itr = firstDisplayLine; itr; itr = itr->GetNext() ) {
        if ( linesTillEnd > CONSOLE_DISPLAY_LINES )
            break;

        ++linesTillEnd;

        // just so itr isn't nullptr for the next code block
        if ( itr->GetNext() == nullptr ) {
            break;
        }
    }

    if ( linesTillEnd <= CONSOLE_DISPLAY_LINES )
        return;

    if ( ! firstDisplayLine->GetNext() )
        return;

    firstDisplayLine = firstDisplayLine->GetNext();
}

void Console::PressLeft( void ) {

}

void Console::PressRight( void ) {

}

void Console::UpdateVBO( void )  {
    ASSERT( vbo_vert );
    ASSERT( vbo_secondary );
    ASSERT( vbo_textuv );
    ASSERT( vbo_textcolor );
    ASSERT( vbo_background );

    vbo_vert->Clear();
    vbo_textuv->Clear();
    vbo_textcolor->Clear();
    
    //Ignore the toggle console keybind press as text input //todoconsole
    if ( just_opened ) {
        label.clear();
        just_opened = false;
    }
        
    /* background doesnt change
    vbo_background->Clear();
    */
    /* secondary doesnt change
    vbo_secondary->Clear();
    */
}

void Console::PrintFloats( const float* flt, const std::size_t num_floats, const char* postfix_message ) {
    std::string msg;
    msg.reserve( num_floats*15 ); // room for floats of length 13 plus some commas and spaces
    for ( std::size_t i = 0; i < num_floats; ++i ) {
        if ( i > 0 )
            msg += ", ";

        std::string str( std::to_string( flt[i] ) );
        String::TrimTrailing( str, "0" );
        msg += str;
    }

    if ( postfix_message && postfix_message[0] != '\0' )
        msg += postfix_message;

    CMSG("%s\n", msg.c_str() );
}

void Console::PrintFloat( const float flt, const char* postfix_message ) {
    PrintFloats( &flt, 1, postfix_message );
}

void Console::SetVisible( const bool whether ) {
    if ( whether ) {
        game->Pause( PauseStateT::PausedByConsole );
    } else {
        game->Unpause( PauseStateT::PausedByConsole );
    }
    Sprite::SetVisible( whether );
}

void Console::Append( const char* str, const HistoryDisplayT display_type ) {
    Append( std::string( str ), display_type );
}
