// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_COMMANDSYS_H_
#define SRC_COMMANDSYS_H_

#include "./clib/src/warnings.h"
#include "./base/main.h"
#include "./rendering/shapes.h"

typedef void (*ConsoleCommand)( const std::vector< std::string >& args );

class CommandSys {
public:
    CommandSys( void ) = delete;
    ~CommandSys( void ) = delete;

    static void Init( void );

private:
    static void Main_Init( void );
    static void Math_Init( void );
    static void Dev_Init( void );

public:
    static void Execute( const std::vector<std::string>& args );
    static void Execute( const char* str );

    static void clear_debug( const std::vector< std::string >& args_unused ); //!< clear the file
    static void ClearDebug( void ); //!< clear the file

    static void send_debug( const std::vector< std::string >& args ); //!< write console messages to file
    static void SendDebug( const std::string& str ); //!< write console messages to file

    static void exec( const std::vector< std::string >& args ); //!< processes console commands from file
    static void dbreak( const std::vector< std::string >& args  ); //!< used to break in debug mode
    static void debug( const std::vector< std::string >& args  ); //!< print debug info

    static void dlg( const std::vector< std::string >& args );

    static void unset( const std::vector< std::string >& args ); //!< unset a variable
    static void set( const std::vector<std::string>& args ); //!< set or display a variable

    static void bind( const std::vector<std::string>& args ); //!< binds a key to an in-game action
    static void unbind( const std::vector<std::string>& args ); //!< unbinds a key from an in-game action

    static void numEnts( const std::vector<std::string>& args_unused ); //!< prints how many entities exist
    
    static void loadPads( const std::vector<std::string>& args_unused ); //!< reloads the SDL Controller Database

    static void force_map( const std::vector< std::string >& args ); //!< load map without confirmation dialog
    static void force_editor( const std::vector< std::string >& args_unused ); //!< load/close editor without confirmation dialog
    static void map( const std::vector< std::string >& args ); //!< load map / prefab

    #ifdef MONTICELLO_EDITOR
    static void save( const std::vector< std::string >& args ); //!< save map
    #endif // MONTICELLO_EDITOR

    static void ls( const std::vector< std::string >& args ); //!< list directory
    static void exit( const std::vector< std::string >& args ); //!< exit program
    static void clear( const std::vector< std::string >& args ); //!< clear the map
    static void v_restart( const std::vector< std::string >& args ); //!< restart video
    static void v_reloadTex( const std::vector< std::string >& args ); //!< restart video

    // math
    static void norm( const std::vector<std::string>& args ); //!< print the normal of a plane given 3 verts
    static void normalize( const std::vector<std::string>& args ); //!< normalize a vector
    static void mod( const std::vector<std::string>& args ); //!< modulous
    static void dot( const std::vector<std::string>& args ); //!< dot product
    static void cross( const std::vector<std::string>& args ); //!< cross product
    static void cos( const std::vector< std::string >& args );
    static void acos( const std::vector< std::string >& args );
    static void sin( const std::vector< std::string >& args );
    static void asin( const std::vector< std::string >& args );
    static void tan( const std::vector< std::string >& args );
    static void atan( const std::vector< std::string >& args );
    static void sqrt( const std::vector< std::string >& args );
    static void sq( const std::vector< std::string >& args );
    static void len( const std::vector< std::string >& args ); //!< vector lengt

    static int GetInt( const std::string& str ); //! if str is "last", return the last int. otherwise return str converted to an int
    static float GetFloat( const std::string& str ); //! if str is "last", return the last float. otherwise return str converted to an float
    static uint GetUInt( const std::string& str ); //! if str is "last", return the last uint. otherwise return str converted to an uint
    static bool GetVec3f( const std::string& str, Vec3f& out ); //! if str is "last", out is set to the last vec. otherwise str is converted to vec3f and assigned into out. returns false if anything went wrong.

    // ** dev
    static void tet( const std::vector<std::string>& args ); //!< draw a tetrahedron given 4 verts
    static void tri( const std::vector<std::string>& args ); //!< draw a triangle given 3 verts
    static void line( const std::vector<std::string>& args ); //!< draw a line given 2 verts
    static void point( const std::vector<std::string>& args ); //!< draw a point at the given vert
    static void sphere( const std::vector<std::string>& args ); //!< draw a sphere at the given vert/radius
    static void capsule( const std::vector<std::string>& args ); //!< draw a capsule at the given verts/radius
    static void shape( const std::vector<std::string>& args ); //!< draw a shape given an arbitrary number of vertices
    static void aabox( const std::vector<std::string>& args ); //!< draw a aabox given 6 floats
    static void delshape( const std::vector<std::string>& args ); //!< remove a shape by index or name
    static void delallshapes( const std::vector<std::string>& args ); //!< remove all shapes
    static void dellastshape( const std::vector<std::string>& args ); //!< remove the last shape added
    static void nameshape( const std::vector<std::string>& args ); //!< rename a shape by index or name
    static void namelastshape( const std::vector<std::string>& args ); //!< rename the last shape that was added

#ifdef MONTICELLO_EDITOR
    static void anim( const std::vector<std::string>& args );
    static void devmode( const std::vector< std::string >& args ); //!< open/close the editor
#endif // MONTICELLO_EDITOR

    static void give( const std::vector<std::string>& args ); //!< give player an item
    static void spawn( const std::vector< std::string >& args ); //!< spawn entity based on its entname name (ie, .ent files)
    static void newent( const std::vector< std::string >& args ); //!< spawn entity based on its C++ class name (ie, Entity)
    static void resaveMaps( const std::vector< std::string >& args ); //!< reload and resave all maps in filesystem

public:
    static void GameIsReady( void );
    static void EditorIsReady( void );

private:
    static void CacheGameSetCommand( const std::string& key, const std::string& val );
    static void CacheEditorSetCommand( const std::string& key, const std::string& val );

    static bool CheckSetEvent( const std::string& var, const std::string& value ); //!< performs special functions when certain vars are un/set
    static void set_var( const std::string& var, const std::string& value );
    static void display_var( std::string var_byval );
    static void DrawShape( const char* shape_kind, const std::string& name_unused, const std::string* verts, const std::size_t num_verts );
    static void DrawAABox( const std::string& name_unused, const std::string* six_floats, const Vec3f& origin );

    static void EditorNotOpenError( void );

private:
    static HashList< ConsoleCommand > commands;

    //! if we call a special "set" console command that can't be executed before the game is constructed, and it hasn't yet, we cache it here
    static std::vector< std::pair< std::string, std::string > > cachedGameSetCommands;

    //! if we call a special "set" console command that can't be executed before the editor is constructed, and it hasn't yet, we cache it here
    static std::vector< std::pair< std::string, std::string > > cachedEditorSetCommands;

    static float last_float;
    static Vec3f last_vec;
};

#endif  // SRC_COMMANDSYS_H_
