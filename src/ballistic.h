// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_BALLISTIC_H_
#define SRC_BALLISTIC_H_

#include "./base/main.h"

/*!

Ballistic \n\n

A general set of functions for throwing bullets around \n

**/

struct Ballistic {
    //!< on success, returns the entity that it hits.
    static std::shared_ptr< Entity > Fire( const Vec3f& origin, const Vec3f& direction_normalized, const std::string& damageInfo, const float range, const std::weak_ptr< Entity >& originator );


    /*!< shoots a bullet from the camera (moved forward enough to be perpendicular with the player's origin)
        toward the center of the screen (ie, crosshair).
        returns the entity that it hits
    */
    static std::shared_ptr< Entity > PlayerFire( const std::string& damageInfo, const float range, const std::weak_ptr< Entity >& originator );
};

#endif  // SRC_BALLISTIC_H_
