// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_FILES_BM1ANIM
#define SRC_FILES_BM1ANIM

#include "../base/main.h"

class Bone;
class Armature;
class AnimatedModel;
class BoneKeyFrame;

class ParserBM1Anim : public ParserBinary {
public:
    ParserBM1Anim( void ) = delete;
    ParserBM1Anim( File& opened_bm1anim_file );
    ParserBM1Anim( const ParserBM1Anim& other ) = delete;

public:
    ParserBM1Anim& operator=( const ParserBM1Anim& other ) = delete;

public:
    bool Load( AnimatedModel& model, const char* anim_name ); //!< Warning: do not pass in an animation that was already loaded

private:
    bool ReadHeader( void );
    void ReadBoneFrame( Armature& arm, Bone& animBone );

private:
    Uint8 version;
    Vec3f scale;
    static std::vector< std::string > globalAnimIDs; //!< a list of all anim names for every model and animation, the index of which is their ID
};

#endif  //SRC_FILES_BM1ANIM
