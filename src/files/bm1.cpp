// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./bm1.h"

ParserBM1::ParserBM1( File& file_to_parse )
    : ParserBinary( file_to_parse )
    , version(0)
    , format(ModelFormatT::STATIC)
    , scale(1,1,1)
{ }

bool ParserBM1::ReadHeader( void ) {
    // ** make sure it's actually a BM1 file
    const std::size_t chr_len=3;
    std::vector<char> chr(chr_len+1);
    ReadChar( chr.data(), chr_len );
    chr[chr_len]='\0';
    if ( String::Cmp( chr.data(), "BM1" ) != 0 ) {
        ERR("BM1: Invalid header: %s\n", file->GetFilePath().c_str());
        return false;
    }

    // ** verify version
    ReadUInt8( version );

    const int HIGHEST_BM1_VERSION_SUPPORTED = 3;

    if ( version > HIGHEST_BM1_VERSION_SUPPORTED ) {
        ERR("BM1:  Incorrect version (highest supported is %i, got %i): %s\n", HIGHEST_BM1_VERSION_SUPPORTED, version, file->GetFilePath());
        return false;
    }

    format = static_cast< ModelFormatT >( ReadUInt8() );

    return true;
}

void ParserBM1::ReadFaces( Mesh& mesh ) {
    Vec3f vtmp;

    uint faceNum;
    ReadUInt( faceNum );
    mesh.faces.resize( faceNum );
    uint verts_with_no_uv = 0;

    for ( uint face=0; face<faceNum; ++face ) {
        // FACE NORMAL
        ReadVec3f( vtmp );

        // MATERIAL INDEX
        ReadUChar(); // todo: remove from format
        verts_with_no_uv += ReadFaceVerts( mesh, mesh.faces[face] );

        // smoothing group id
        // bone bound to
    }

    if ( verts_with_no_uv > 0 ) {
        WARN("BM1: %u vertices in a mesh have no UV data: %s.\n", verts_with_no_uv, file->GetFilePath().c_str() );
    }
}

uint ParserBM1::ReadFaceVerts( Mesh& mesh, ModelFace& modelface ) {
    Uint8 idx;
    Uint8 uvCount;
    Vec2f tex_coord;
    Uint8 vert_index_count;
    Vec3f vtmp;
    uint verts_with_no_uv = 0;

    ReadUChar( vert_index_count );
    for ( idx=0; idx<vert_index_count; ++idx ) {
        // append new vertex
        VertexInfo* reference_vert = &modelface.vert.Append()->Data();

        // vertex index
        ReadUInt( reference_vert->vertIndex );

        // vertex normal :
        ReadFloat( vtmp.x );
        ReadFloat( vtmp.y );
        ReadFloat( vtmp.z );

        reference_vert->normIndex = new uint( reference_vert->vertIndex );
        
        // UV COUNT (0,1,2)
        ReadUChar( uvCount );

        tex_coord.x = 0;
        tex_coord.y = 0;

        // Texture Vertex Index
        if ( uvCount >= 1 ) {
            ReadFloat( tex_coord.x ); //vt

            // Vertex Normal Index
            if ( uvCount >= 2 ) {
                ReadFloat( tex_coord.y ); //vn

                if ( uvCount >= 3 ) {
                    WARN("BM1: Extra UVs (total is %u): %s.\n", uvCount, file->GetFilePath().c_str() );
                }
            } else {
                WARN("BM1: Didn't find second texture coordinate (only got %u): %s.\n", uvCount, file->GetFilePath().c_str() );
            }

            int i = FindIndex( mesh.texs, tex_coord );
            if ( i < 0 ) {
                reference_vert->texIndex = new uint( mesh.texs.size() );
                mesh.texs.emplace_back( tex_coord );
            } else {
                reference_vert->texIndex = new uint( static_cast< uint> ( i ) );
            }

        } else {
            ++verts_with_no_uv;
        }
    }

    return verts_with_no_uv;
}

void ParserBM1::ReadVertices( Mesh& mesh, const uint count, const bool is_animated ) {
    uchar numGroups;
    uint i,c;
    
    for ( i=0; i<count; ++i ) {
        mesh.verts.emplace_back( ReadVec3f() );
        MeshVert *v = &mesh.verts[i];
        
        v->co *= scale;
        
        if ( version > 1 )
            ReadVec3f( v->normal );
            
        if ( is_animated ) {
            ReadUChar( numGroups );  //As of 10/30/2013 only 2 groups supported = num of bones the vertex is weighted to.
            v->bone_ids = new uchar[ numGroups ];
            v->bone_weights = new float[ numGroups ];
            v->numGroups = numGroups;
            for ( c=0; c < numGroups; ++c ) {
                ReadUChar( v->bone_ids[c] );
                ReadFloat( v->bone_weights[c] );
            }
        }
    }
}

void ParserBM1::ReadAllMeshes( Model& model ) {
    ASSERT( file );

    const Uint8 numMeshes = ReadUInt8();
    model.meshes.resize(numMeshes);

    uint vertNum;
    for ( uint m = 0; m < numMeshes; ++m ) {
        model.meshes[m].SetVersion( version );
        ReadUInt( vertNum );
        model.meshes[m].verts.reserve( vertNum );
        ReadVertices( model.meshes[m], vertNum, model.IsAnimated() );
        ReadFaces( model.meshes[m] );
    }
}

void ParserBM1::ReadChannelData( AnimatedModel& anim_model ) {
    ReadUInt(anim_model.numChannels);
    anim_model.channels = new AnimChannelT[anim_model.numChannels];
    for (uint i=0; i < anim_model.numChannels; i++) {
        const uint c = ReadUInt();
        anim_model.channels[i] = static_cast< AnimChannelT >(c);
    }
    anim_model.BuildChannelList();
    
}

void ParserBM1::ReadBoneData( AnimatedModel& anim_model ) {
    ReadUInt(anim_model.numBones);
}

//todo return std::make_shared< Model >()
std::shared_ptr< Model > ParserBM1::Load( void ) {
    if ( ! file ) {
        ERR("BM1: Something went horribly wrong, parser's file is null.\n");
        return nullptr;
    }

    if ( ! file->IsOpen() ) {
        ERR("BM1: File not open\n");
        return nullptr;
    }

    if ( ! ReadHeader() )
        return nullptr;
        
    if ( version > 2 )
        ReadVec3f( scale );

    std::shared_ptr< Model > model;
    std::shared_ptr< AnimatedModel > anim_model;

    switch ( format ) {
        case ModelFormatT::ANIMATED:
            anim_model = std::make_shared< AnimatedModel >();
            model = anim_model;
            ReadChannelData( *anim_model );
            ReadBoneData( *anim_model );
            break;
        case ModelFormatT::STATIC:
            model = std::make_shared< Model >();
            break;
        default:
            ASSERT( false );
            return nullptr;
    }

    ReadAllMeshes( *model );

    return model;
}
