// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./bm1anim.h"
#include "../rendering/animation/animation.h"

constexpr const bool BM1ANIM_DEBUG = false;

std::vector< std::string > ParserBM1Anim::globalAnimIDs;

ParserBM1Anim::ParserBM1Anim( File& opened_bm1anim_file )
    : ParserBinary( opened_bm1anim_file )
    , version()
    , scale(1,1,1)
{ }

bool ParserBM1Anim::Load( AnimatedModel& model, const char* anim_name ) {
    if ( ! file ) {
        ERR("ParserBM1Anim: Something went horribly wrong, file is null.\n");
        return false;
    }

    // ** make sure it's opened
    if ( ! file->IsOpen() ) {
        ERR("BM1A: File not open\n");
        return false;
    }

    if ( ! ReadHeader() )
        return false;
    
    if ( version > 1 )
        ReadVec3f( scale );
        
    const int idx = FindIndex( globalAnimIDs, anim_name );
    std::size_t animID;
    if ( idx > 0 ) {
        animID = static_cast<uint>( idx );
    } else {
        animID = static_cast<uint>( globalAnimIDs.size() );
        globalAnimIDs.emplace_back( anim_name );
    }
    
    Uint8 numChannels;
    ReadUInt8( numChannels );
    
    // reserve at least enough room for how many channels we say we have.
    if ( numChannels > model.anims.size() )
        model.anims.resize(numChannels);

    //Read all armatures
    for (uint c=0; c<numChannels; c++) {
        const uint channel_index = ReadUInt();
        
        // channel_index is zero-indexed, so if we have 1 channel and it's ID is 1, we actually need to reserve room for two channels.
        if ( channel_index >= model.anims.size() )
            model.anims.resize(channel_index+1);

        if ( !model.anims[channel_index] )
            model.anims[channel_index].reset( new LinkList<Armature> );
        ASSERT( model.anims[channel_index] );

        Link<Armature> *link = model.anims[channel_index]->Append();
        Armature &arm = link->Data();

        arm.channel = static_cast< AnimChannelT >(channel_index);
        ReadUInt16( arm.numKeyFrames );
        const Uint8 num_total_pose_bones = ReadUInt8();
        ASSERT( num_total_pose_bones >= 1 );
        arm.SetAnimName( anim_name );
        arm.SetAnimID( animID );
        ReadBoneFrame( arm, arm.root );
    }
    return true;
}

bool ParserBM1Anim::ReadHeader( void ) {
    // ** make sure it's actually a BM1 file
    const std::size_t chr_len=4;
    std::vector<char> chr(chr_len+1);
    ReadChar( chr.data(), chr_len );
    chr[chr_len]='\0';
    if ( String::Cmp( chr.data(), "BMA1" ) != 0 ) {
        ERR("BM1A: Invalid header: %s\n", file->GetFilePath().c_str());
        return false;
    }

    // ** verify version
    ReadUInt8( version );
    const int HIGHEST_BM1ANIM_VERSION_SUPPORTED = 3;
    if ( version > HIGHEST_BM1ANIM_VERSION_SUPPORTED ) {
        ERR("BM1A: Incorrect version (highest supported is %i, got %i): %s\n", HIGHEST_BM1ANIM_VERSION_SUPPORTED, version, file->GetFilePath().c_str());
        return false;
    }

    ReadUInt8(); // unused
    return true;
}

void ParserBM1Anim::ReadBoneFrame( Armature& arm, Bone& animBone ) {
    arm.SetBoneCount( arm.GetBoneCount()+1 );
    animBone.keyframes.resize( arm.numKeyFrames );
    
    const Uint8 nameLen = ReadUInt8();
    if ( nameLen > 0 )
        animBone.SetName( ReadStr(static_cast<std::size_t>(nameLen)) );

    animBone.SetID( ReadUInt8() );
    
    if constexpr ( BM1ANIM_DEBUG ) {
        CMSG_LOG("  DEBUG: Loading Bone: %u/%s\n", animBone.GetID(), animBone.GetName().c_str() );
    }
    
    //Read Bone Attachments
    // todobrandon / todojesse - this should be in ParserBM1Anim::Load, since it's only for the root bone. this will require changing the exporter to only export these values for the ROOT bones of EACH CHANNEL.
    if ( version > 2 ) {
        int attachToBoneID( ReadInt() );
        if ( attachToBoneID >= 0 ) {
            float attach_point( ReadFloat() );
            
            auto const & rootBone = arm.GetRootBone_Ref();
            if ( &animBone == &rootBone ) {
                arm.AttachToBoneID( attachToBoneID );
                arm.AttachToPointOnBone( attach_point );
            }
        }
    }

    Dualquat bindPos( ReadQuat() );
    
    const Quat tran(ReadQuat()); //In exporter this is really just a Vec3() translation vector exported in a Quat (i.e. Vec4) container.  It's not an actual quat.
    const Vec3f translation( tran.x * scale.x, tran.y * scale.y, tran.z * scale.z);
    
    bindPos.CalcDual_RotTran( translation );
    
    animBone.SetBindPose( bindPos );

    //Read inverse bind pose, pre-calc the dual part.
    const Quat inverseBindPose_q_real = ReadQuat();
    ReadQuat(); //Unused

    const Dualquat inverseBindPose( inverseBindPose_q_real, Dualquat::GetDual_TranRot( -translation, inverseBindPose_q_real ) );
    animBone.SetInvBindPose(inverseBindPose);

    const Vec3f TPoseHeadRest = ReadVec3f();
    const Vec3f TPoseTailRest = ReadVec3f();
    animBone.SetTeePoseHead( TPoseHeadRest * scale );
    animBone.SetTeePoseTail( TPoseTailRest * scale );

    for ( std::size_t id=0; id < arm.numKeyFrames; id++ ) {
        BoneKeyFrame* keyFrame = &animBone.keyframes[id];
        
        const Uint16 keyframe_frame = ReadUInt16();
        const Vec3f pose_head_pos = ReadVec3f() * scale;
        const Vec3f pose_tail_pos = ReadVec3f() * scale;
        const Vec3f boneLoc = ReadVec3f() * scale; //Keyframe : Translation
        const Quat keyframe_rot( ReadQuat() );
        
        keyFrame->SetFrame( keyframe_frame );
        keyFrame->SetHead( pose_head_pos );
        keyFrame->SetTail( pose_tail_pos );
        keyFrame->SetRotation( keyframe_rot );
        
        Dualquat boneDQ( keyFrame->GetQuat(), {0,0,0,0} );
        
        bool useTranslationData = false;

        if ( !arm.IsAttachedToBone() ) {
            auto const & rootBone = arm.GetRootBone_Ref();
            if ( &animBone == &rootBone )
                useTranslationData = true;
        } else {
            if ( animBone.parent )
                useTranslationData = true;
        }
        
        if ( useTranslationData )
            boneDQ.CalcDual_RotTran( boneLoc );
        
        keyFrame->SetDualQuat(boneDQ);
    }

    const uchar numChildren = ReadUChar();
    if ( numChildren > 0 ) {
        animBone.children.reserve(numChildren);
        const auto parent = &animBone;
        for ( std::size_t cnt=0; cnt < numChildren; cnt++) {
            animBone.children.emplace_back( parent );
            ReadBoneFrame( arm, animBone.children[cnt] );
        }
    }
}

