// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_FILES_ASSETMANAGER_H_
#define SRC_FILES_ASSETMANAGER_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../files/manifest.h"
#include "../filesystem.h" // for OverrideWarning()
/*!

AssetManager \n\n

Keeps track of assets from files and archives

**/

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
class AssetManager {
public:
    AssetManager( void ) = delete;
    AssetManager( const AssetManager& other ) = delete;
    AssetManager( const std::string& assetTypeName ) : manifest(), asset_type_name(assetTypeName) {}
    virtual ~AssetManager( void ) {}
    AssetManager& operator=( const AssetManager& other ) = delete;

public:
    void Register( const std::string& path, const char* container = "" /* blank means filesystem */ );
    void RegisterName( const std::string& name, const char* container = "" /* blank means filesystem */ );
    bool IsRegistered( const char* name ) const;
    bool EnsureLoaded( const char* name );
    virtual std::shared_ptr< const ASSET_TYPE > Get( const char* name );
    std::shared_ptr< const ASSET_TYPE > GetRandom( const char* name );
    std::shared_ptr< const ASSET_TYPE > Get_NoErr( const char* name ); //!< same as Get() but doesn't throw and error message if it's not found
    uint GetAllNames( LinkList< std::string >& names ) const;
    void UnloadUnusedAssets( void ); //!< unload assets that are not in use and were not loaded or retrieved on or after this current frame
    const ASSET_CONTAINER* GetManifestData( const char* name ) const;
    std::string GetAssetNameFromFilePath( const char* filepath ) const;
    std::string GetAssetTypeName( void ) const;

protected:
    virtual void BeingUnloaded( [[maybe_unused]] ASSET_TYPE& asset ) {}; //!< called whenever the asset is about to be unloaded (so you can free it if it's a pointer, for example)
    virtual bool Load( File& opened_file ) = 0;
    virtual bool Load( const AssetManifest& data );

protected:
    HashList< ASSET_CONTAINER > manifest; //scaleme
    const std::string asset_type_name;
};

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
inline std::string AssetManager<ASSET_CONTAINER,ASSET_TYPE>::GetAssetTypeName( void ) const {
    return asset_type_name;
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
inline const ASSET_CONTAINER* AssetManager<ASSET_CONTAINER,ASSET_TYPE>::GetManifestData( const char* name ) const {
    return manifest.Get( name );
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
uint AssetManager<ASSET_CONTAINER,ASSET_TYPE>::GetAllNames( LinkList< std::string >& names ) const {
    for ( const auto& data : manifest ) {
        names.Append( data.second.GetName() );
    }

    return names.Num();
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
void AssetManager<ASSET_CONTAINER,ASSET_TYPE>::Register( const std::string& path, const char* container ) {
    if ( path.size() == 0 ) {
        ERR("Attempted to register a %s with no path.\n", GetAssetTypeName().c_str() );
        return;
    }

    const std::string name = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );
    const std::string message_start( GetAssetTypeName() + " Manifest");

    if ( manifest.Set( name.c_str(), ASSET_CONTAINER( name, path, container ) ) )
        OverrideWarning(message_start.c_str(), name, path, container );
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
void AssetManager<ASSET_CONTAINER,ASSET_TYPE>::RegisterName( const std::string& name, const char* container ) {
    if ( name.size() == 0 ) {
        ERR("Attempted to register-by-name a %s with no name.\n", GetAssetTypeName().c_str() );
        return;
    }

    const std::string message_start( GetAssetTypeName() + " Manifest");

    if ( manifest.Set( name.c_str(), ASSET_CONTAINER( name, "", container ) ) )
        OverrideWarning(message_start.c_str(), name, "", container );
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
bool AssetManager<ASSET_CONTAINER,ASSET_TYPE>::Load( const AssetManifest& data ) {
    File file;
    if ( ! OpenAssetFile( GetAssetTypeName().c_str(), data, file ) )
        return false;

    return Load( file );
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
bool AssetManager<ASSET_CONTAINER,ASSET_TYPE>::EnsureLoaded( const char* name ) {
    if (!name)
        return false;

    ASSET_CONTAINER* data = manifest.Get( name );
    if ( !data )
        return false;

    if ( !data->IsLoaded() )
        if ( !Load( *data ) )
            return false;

    data->UpdateLoadTime();
    return true;
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
std::shared_ptr< const ASSET_TYPE > AssetManager<ASSET_CONTAINER,ASSET_TYPE>::Get_NoErr( const char* name ) {
    if ( !EnsureLoaded( name ) )
        return nullptr;

    ASSET_CONTAINER* asset_pp = manifest.Get( name );
    if ( !asset_pp )
        return nullptr;

    return asset_pp->data;
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
std::shared_ptr< const ASSET_TYPE > AssetManager<ASSET_CONTAINER,ASSET_TYPE>::Get( const char* name ) {
    auto ret = Get_NoErr( name );
    if ( ! ret )
        ERR("%s asset could not be loaded: %s\n", GetAssetTypeName().c_str(), name);
    return ret;
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
std::shared_ptr< const ASSET_TYPE > AssetManager<ASSET_CONTAINER,ASSET_TYPE>::GetRandom( const char* name ) {

    std::vector< std::string > names;
    std::string checkName = name;
    bool found = IsRegistered(checkName.c_str());
    uint idx=1;

    while ( found ) {
        names.push_back( checkName );
        checkName = name;
        checkName += std::to_string(idx);
        found = IsRegistered( checkName.c_str() );
        ++idx;
    }

    switch ( names.size() ) {
        case 0:
            ERR("%s asset not found: %s.\n", GetAssetTypeName().c_str(), name );
            return nullptr;
        case 1:
            return Get( names[0].c_str() );
        default:
            return Get( Random::Element(names).c_str() );
    }
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
void AssetManager<ASSET_CONTAINER,ASSET_TYPE>::UnloadUnusedAssets( void ) {
    for ( auto& data : manifest ) {
        if ( !data.second.IsLoaded() )
            continue;

        if ( data.second.WasLoadedThisFrame() )
            continue;

        if ( data.second.data.use_count() > 1 )
            continue; // something else is still utilizing this asset

        LOG("Unloading %s\n", data.second.GetPath().c_str() );
        BeingUnloaded( *data.second.data );
        data.second.data = nullptr; // set pointer to null to unload
    }
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
std::string AssetManager<ASSET_CONTAINER,ASSET_TYPE>::GetAssetNameFromFilePath( const char* filepath ) const {
    return String::TrimExtentionFromFileName( String::GetFileFromPath( filepath ) );
}

template< typename ASSET_CONTAINER, typename ASSET_TYPE >
inline bool AssetManager<ASSET_CONTAINER,ASSET_TYPE>::IsRegistered( const char* name ) const {
    return manifest.Get( name ) != nullptr;
}

#endif  // SRC_FILES_ASSETMANAGER_H_
