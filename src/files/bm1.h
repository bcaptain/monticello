// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_FILES_BM1
#define SRC_FILES_BM1

#include "../base/main.h"
#include "../rendering/models/model.h"
#include "../rendering/models/animatedModel.h"

typedef bool ModelFormatT_BaseType;
enum class ModelFormatT : ModelFormatT_BaseType {
    STATIC=false
    , ANIMATED=true
};

/*!

ParserBM1 \n\n

This parser is used to read a bm1 file and store it as a Model in the global Models hashlist

**/

class ParserBM1 : public ParserBinary {
public:
    ParserBM1( void ) = delete;
    ParserBM1( File& file_to_parse ); //!< file_to_parse is not resource managed
    ParserBM1( const ParserBM1& other ) = delete;
    ~ParserBM1( void ) override { };

public:
    ParserBM1& operator=( const ParserBM1& other ) = delete;

public:
    std::shared_ptr< Model > Load( void );

private:
    bool ReadHeader( void );

    void ReadChannelData( AnimatedModel& model );
    void ReadBoneData( AnimatedModel& model );
    void ReadAllMeshes( Model& model );
    void ReadMesh( Model& model );
    void ReadVertices( Mesh& mesh, const uint vertNum, const bool is_animated = false );
    void ReadFaces( Mesh& mesh );
    uint ReadFaceVerts( Mesh& mesh, ModelFace& modelface ); //!< returns number of verts with no uv data

    void ReadAllSkeletons( Model& model );
    void ReadSkeleton( Model& model );
    //void ReadBone( Bone& bone, uint& bone_id_to_set );

private:
    Uint8 version;
    ModelFormatT format;
    Vec3f scale;
};

#endif  //SRC_CLIB_FILE_BM1
