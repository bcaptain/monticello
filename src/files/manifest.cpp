// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#include "./clib/src/warnings.h"
#include "./manifest.h"
#include "../game.h"

class Texture;
class EntityInfo;
class Material;
class Font;
class Effect;
class SoundData;
struct MapScript;

//
//
// AssetManifest
//
//

AssetManifest::AssetManifest( void )
    : name()
    , path()
    , container()
{}

AssetManifest::AssetManifest( const std::string& _name, const std::string& _path, const char* _container )
    : name( _name )
    , path( _path )
    , container( _container )
{}


AssetManifest::AssetManifest( const AssetManifest& other )
    : name( other.name )
    , path( other.path )
    , container( other.container )
{ }

AssetManifest& AssetManifest::operator=( const AssetManifest& other ) {
    name = other.name;
    path = other.path;
    container = other.container;
    return *this;
}

template<>
void AssetData<Texture>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<Texture>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

template<>
void AssetData<EntityInfo>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<EntityInfo>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

class DamageInfo;
template<>
void AssetData<DamageInfo>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<SoundData>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

template<>
void AssetData<SoundData>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<DamageInfo>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

template<>
void AssetData<Material>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<Material>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

class Model;
template<>
void AssetData<Model>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<Model>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

template<>
void AssetData<Font>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<Font>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

class Effect;
template<>
void AssetData<Effect>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<Effect>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

struct MapScript;
template<>
void AssetData<MapScript>::UpdateLoadTime( void ) {
    load_time = game->GetRenderTime();
}

template<>
bool AssetData<MapScript>::WasLoadedThisFrame( void ) const {
    return load_time == game->GetRenderTime();
}

void AssetManifest::SetName( const std::string& _name ) {
    name = _name;
}

void AssetManifest::SetPath( const std::string& _path ) {
    path = _path;
}

void AssetManifest::SetContainer( const char* _container ) {
    container = _container;
}

std::string AssetManifest::GetName( void ) const {
    return name;
}

std::string AssetManifest::GetPath( void ) const {
    return path;
}

std::string AssetManifest::GetContainer( void ) const {
    return container;
}
