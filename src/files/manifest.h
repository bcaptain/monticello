// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_FILES_MANIFEST
#define SRC_FILES_MANIFEST

#include "../base/main.h"

/*!

AssetManifest \n\n

**/

class AssetManifest {
public:
    AssetManifest( void );
    AssetManifest( const std::string& _name, const std::string& _path, const char* _container = "" /* blank means filesystem */ );
    AssetManifest( const AssetManifest& other );
    virtual ~AssetManifest( void ) {}

public:
    AssetManifest& operator=( const AssetManifest& other );

public:
    std::string GetName( void ) const;
    std::string GetPath( void ) const;
    std::string GetContainer( void ) const;

    void SetName( const std::string& _name );
    void SetPath( const std::string& _path );
    void SetContainer( const char* _container );

private:
    std::string name;
    std::string path;
    std::string container;
};

template< typename TYPE >
class AssetData : public AssetManifest {
public:
    AssetData( void );
    AssetData( const std::string& _name, const std::string& _path, const char* _container = "" /* blank means filesystem */ );
    AssetData( const AssetData<TYPE>& other );
    ~AssetData( void ) override { };

public:
    AssetData<TYPE>& operator=( const AssetData<TYPE>& other );

public:
    bool IsLoaded( void ) const;
    void UpdateLoadTime( void );
    bool WasLoadedThisFrame( void ) const; //!< this technically can fail if the time has wrapped, but it's so astronomically unlikely that it's hardly worth mentioning

public:
    std::shared_ptr< TYPE > data;
    Uint32 load_time;
};

template< typename TYPE >
AssetData<TYPE>::AssetData( void )
    : AssetManifest()
    , data()
    , load_time( 0 )
{ }

template< typename TYPE >
AssetData<TYPE>::AssetData( const std::string& _name, const std::string& _path, const char* _container )
    : AssetManifest( _name, _path, _container )
    , data()
    , load_time( 0 )
{ }

template< typename TYPE >
AssetData<TYPE>::AssetData( const AssetData<TYPE>& other )
    : AssetManifest( other )
    , data( other.data )
    , load_time( other.load_time )
{ }

template< typename TYPE >
AssetData<TYPE>& AssetData<TYPE>::operator=( const AssetData<TYPE>& other ) {
    AssetManifest::operator=( other );
    data = other.data;
    load_time = other.load_time;
    return *this;
}

template< typename TYPE >
inline bool AssetData<TYPE>::IsLoaded( void ) const {
    return data != nullptr;
}

#endif  // SRC_FILES_MANIFEST
