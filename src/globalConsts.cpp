// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./globalConsts.h"

#include "./clib/src/hash.h"

std::vector< std::pair< std::string, std::string > > varDescriptions;

void PopulateConsoleVarDescriptions( void ) {
    varDescriptions.clear();
    varDescriptions.resize(50);

    // things to Allow
    varDescriptions.push_back( gvaldesc_a_editor );
    varDescriptions.push_back( gvaldesc_a_console );
    varDescriptions.push_back( gvaldesc_a_menu );
    
    // States
    varDescriptions.push_back( gvaldesc_s_menu );
    varDescriptions.push_back( gvaldesc_s_levelTimerExpired );
    
    // Drawing and Debug
    varDescriptions.push_back( gvaldesc_d_boundsMode );
    varDescriptions.push_back( gvaldesc_d_bounds );
    varDescriptions.push_back( gvaldesc_d_boundsAlpha );
    varDescriptions.push_back( gvaldesc_d_bindLines );
    varDescriptions.push_back( gvaldesc_d_boneNames );
    varDescriptions.push_back( gvaldesc_d_facingDir );
    varDescriptions.push_back( gvaldesc_d_playerSpeed );
    varDescriptions.push_back( gvaldesc_d_aiState );
    varDescriptions.push_back( gvaldesc_d_fps );
    varDescriptions.push_back( gvaldesc_d_compass );
    varDescriptions.push_back( gvaldesc_d_ghost );
    varDescriptions.push_back( gvaldesc_d_grid );
    varDescriptions.push_back( gvaldesc_d_gridOpacity );
    varDescriptions.push_back( gvaldesc_d_testAnim );
    varDescriptions.push_back( gvaldesc_d_frust );
    varDescriptions.push_back( gvaldesc_d_projectors );
    varDescriptions.push_back( gvaldesc_d_projections );
    varDescriptions.push_back( gvaldesc_d_models );
    varDescriptions.push_back( gvaldesc_d_clipModels );
    varDescriptions.push_back( gvaldesc_d_flatLines );
    varDescriptions.push_back( gvaldesc_d_names );
    varDescriptions.push_back( gvaldesc_d_vertNormals );
    varDescriptions.push_back( gvaldesc_d_vertNormalsLen );
    varDescriptions.push_back( gvaldesc_d_noFade );
    varDescriptions.push_back( gvaldesc_d_skel );
    varDescriptions.push_back( gvaldesc_d_triggerLines );
    varDescriptions.push_back( gvaldesc_d_viewCones );
    varDescriptions.push_back( gvaldesc_d_debugPoints );
    varDescriptions.push_back( gvaldesc_d_crossHair );
    varDescriptions.push_back( gvaldesc_d_worldOrigin );
    varDescriptions.push_back( gvaldesc_d_attackCollision );
    varDescriptions.push_back( gvaldesc_d_mapDebug );
    varDescriptions.push_back( gvaldesc_d_axisLineLength );
    varDescriptions.push_back( gvaldesc_d_globalAxis );
    varDescriptions.push_back( gvaldesc_d_playerStatusBar );
    varDescriptions.push_back( gvaldesc_d_damageFeedback );
    
        
    //JESSE - Playtesting
    varDescriptions.push_back( gvaldesc_pt_moveSpeedAdjust );
    
    // Drawing & Debugging Octree
    varDescriptions.push_back( gvaldesc_d_octree );
    varDescriptions.push_back( gvaldesc_d_voxelTrace );
    //todo varDescriptions.push_back( gvaldesc_d_voxelDensity );
    varDescriptions.push_back( gvaldesc_d_voxelTrace );
    varDescriptions.push_back( gvaldesc_voxelColorWeight );
    varDescriptions.push_back( gvaldesc_g_octreeSize );
    varDescriptions.push_back( gvaldesc_g_octreeSubs );
    
    // Editor
    varDescriptions.push_back( gvaldesc_e_pan_scale );
    varDescriptions.push_back( gvaldesc_e_zoom_scale );
    varDescriptions.push_back( gvaldesc_e_snapGridDistance );
    
    // Other
    varDescriptions.push_back( gvaldesc_god );
    varDescriptions.push_back( gvaldesc_volume );
    varDescriptions.push_back( gvaldesc_crosshairPos );
    
    // Video
    varDescriptions.push_back( gvaldesc_i_gl );
    varDescriptions.push_back( gvaldesc_i_glext );
    varDescriptions.push_back( gvaldesc_v_frustDetach );
    varDescriptions.push_back( gvaldesc_v_fullscreen );
    varDescriptions.push_back( gvaldesc_v_borderlessWindow );
    varDescriptions.push_back( gvaldesc_v_cullFace );
    varDescriptions.push_back( gvaldesc_v_farPlane );
    varDescriptions.push_back( gvaldesc_v_fov );
    varDescriptions.push_back( gvaldesc_v_height );
    varDescriptions.push_back( gvaldesc_v_nearPlane );
    varDescriptions.push_back( gvaldesc_v_width );
    varDescriptions.push_back( gvaldesc_v_frustCull );
    varDescriptions.push_back( gvaldesc_v_launchOnDisplay );
    varDescriptions.push_back( gvaldesc_v_mipmap );
    varDescriptions.push_back( gvaldesc_v_renderSort );
    varDescriptions.push_back( gvaldesc_v_pauseAfterRender );
    varDescriptions.push_back( gvaldesc_v_keepFramelyShapes );
    
    // Game
    varDescriptions.push_back( gvaldesc_g_creep );
    varDescriptions.push_back( gvaldesc_g_noClip );
    varDescriptions.push_back( gvaldesc_g_autorun );
    varDescriptions.push_back( gvaldesc_g_faceMoveDir );
    varDescriptions.push_back( gvaldesc_g_friction );
    varDescriptions.push_back( gvaldesc_g_noTrample );
    varDescriptions.push_back( gvaldesc_g_maxIncline );
    varDescriptions.push_back( gvaldesc_g_throwVel );
    varDescriptions.push_back( gvaldesc_g_noLevelSummary );
    
    // Gravity
    varDescriptions.push_back( gvaldesc_grav_disable );
    varDescriptions.push_back( gvaldesc_grav_force );
    varDescriptions.push_back( gvaldesc_grav_dir );
    varDescriptions.push_back( gvaldesc_grav_terminal );

    // Light
    varDescriptions.push_back( gvaldesc_d_lights );
    varDescriptions.push_back( gvaldesc_d_light );
    varDescriptions.push_back( gvaldesc_v_light );

    // Input Basics
    varDescriptions.push_back( gvaldesc_i_mouseInvertVert );
    varDescriptions.push_back( gvaldesc_i_mouseSens );
    
    // Joystick
    varDescriptions.push_back( gvaldesc_d_joyInput );
    varDescriptions.push_back( gvaldesc_axis_buttonThreshold );
    varDescriptions.push_back( gvaldesc_axis0_sens );
    varDescriptions.push_back( gvaldesc_axis1_sens );
    varDescriptions.push_back( gvaldesc_axis2_sens );
    varDescriptions.push_back( gvaldesc_axis3_sens );
    varDescriptions.push_back( gvaldesc_axis4_sens );
    varDescriptions.push_back( gvaldesc_axis5_sens );
    varDescriptions.push_back( gvaldesc_axis0_deadzone );
    varDescriptions.push_back( gvaldesc_axis1_deadzone );
    varDescriptions.push_back( gvaldesc_axis2_deadzone );
    varDescriptions.push_back( gvaldesc_axis3_deadzone );
    varDescriptions.push_back( gvaldesc_axis4_deadzone );
    varDescriptions.push_back( gvaldesc_axis5_deadzone );
    varDescriptions.push_back( gvaldesc_axis0_invert );
    varDescriptions.push_back( gvaldesc_axis1_invert );
    varDescriptions.push_back( gvaldesc_axis2_invert );
    varDescriptions.push_back( gvaldesc_axis3_invert );
    varDescriptions.push_back( gvaldesc_axis4_invert );
    varDescriptions.push_back( gvaldesc_axis5_invert );

    // Camera
    varDescriptions.push_back( gvaldesc_cam_zoom );
    varDescriptions.push_back( gvaldesc_cam_tilt );
    varDescriptions.push_back( gvaldesc_cam_rot );
    varDescriptions.push_back( gvaldesc_cam_radius );
    varDescriptions.push_back( gvaldesc_cam_drawCap );
    varDescriptions.push_back( gvaldesc_cam_drawSphere );
    varDescriptions.push_back( gvaldesc_cam_focalOffset );
    varDescriptions.push_back( gvaldesc_cam_noReset );
    varDescriptions.push_back( gvaldesc_cam_moveRelative );
    varDescriptions.push_back( gvaldesc_cam_tetherSlack );
    varDescriptions.push_back( gvaldesc_cam_tether );

    // Nav Meshes
    varDescriptions.push_back( gvaldesc_d_navAll );
    varDescriptions.push_back( gvaldesc_d_navLines );
    varDescriptions.push_back( gvaldesc_d_navMesh );
    varDescriptions.push_back( gvaldesc_d_navOccupied );
    varDescriptions.push_back( gvaldesc_d_navPolies );
    varDescriptions.push_back( gvaldesc_d_navPortals );

    // Flow Fields
    varDescriptions.push_back( gvaldesc_d_ffPush );
    varDescriptions.push_back( gvaldesc_d_flowfield );
    varDescriptions.push_back( gvaldesc_ai_flowfield );
    varDescriptions.push_back( gvaldesc_ai_ffPush );
    varDescriptions.push_back( gvaldesc_ai_disable );
    varDescriptions.push_back( gvaldesc_ai_autoFindPlayer );
    varDescriptions.push_back( gvaldesc_d_animSpeed );
    varDescriptions.push_back( gvaldesc_d_animBlend );
    varDescriptions.push_back( gvaldesc_d_animBlendFull );
}
