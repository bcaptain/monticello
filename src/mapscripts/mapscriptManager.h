// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_MAPSCRIPTMANAGER_H_
#define SRC_MAPSCRIPTMANAGER_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../files/manifest.h"
#include "../files/assetManager.h"
#include "./mapscript.h"

struct MapScript;

/*!

MapScriptManager \n\n

holds all the MapScripts for the game

**/

class MapScriptManager : public AssetManager< AssetData<MapScript>, MapScript > {
public:
    MapScriptManager( void );

private:
    bool Load( File& opened_mapscript_file ) override;
    using AssetManager< AssetData<MapScript>,MapScript >::Load;

    void ReadSubAction( ParserSSV& parser, MapScript::Action& action );
    void ReadDescriptors( ParserSSV& parser, std::vector< std::pair<std::string,std::string> >& descriptors );

private:
    static bool instantiated;
};

extern MapScriptManager mapScriptManager;

#endif  // SRC_MAPSCRIPTMANAGER_H_
