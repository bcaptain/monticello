// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "./mapscriptManager.h"
#include "../filesystem.h" // for OverrideWarning()

class ReadWord;
bool MapScriptManager::instantiated( false );
MapScriptManager mapScriptManager;

MapScriptManager::MapScriptManager( void )
    : AssetManager( "MapScript" )
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

bool MapScriptManager::Load( File& opened_file ) {
    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open mapscript file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    AssetData< MapScript >* manifest_s = manifest.Get( name.c_str() );
    if ( !manifest_s ) {
        ERR_DIALOG("Couldn't load mapscript file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }

    auto mapscript = std::make_shared<MapScript>();

    // ** parse the file
    ParserSSV parser( opened_file );
    parser.SetCommentIndicator('#');
    std::string entry;

    // READ ALL ACTIONS
    while ( parser.ReadWord( entry ) ) {
        if ( entry == "atTime" ) {
            mapscript->actions.emplace_back( MapScript::ActionType::mapscript_action_atTime );
            auto& action = mapscript->actions.back();

            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR( "Mapscript %s: expected time argument for expression \"atTime\"\n", opened_file.GetFilePath() );
                if ( !parser.AtEndOfLine() )
                    parser.SkipToNextNonblankLine();
                continue;
            }

            action.idata = String::ToUInt( entry );

            ReadSubAction( parser, action );
            continue;
        }

        if ( entry == "ifKilled" ) {
            mapscript->actions.emplace_back( MapScript::ActionType::mapscript_action_ifKilled );
            auto& action = mapscript->actions.back();

            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR( "Mapscript %s: expected entity name argument for expression \"ifDies\"\n", opened_file.GetFilePath() );
                if ( !parser.AtEndOfLine() )
                    parser.SkipToNextNonblankLine();
                continue;
            }

            action.sdata = entry;

            ReadSubAction( parser, action );
            continue;
        }

        ERR("Mapscript %s: Unknown mapscript action: \"%s\"\n", opened_file.GetFilePath(), entry );
        if ( !parser.AtEndOfLine() )
            parser.SkipToNextNonblankLine();
    }

    const bool asset_existed = manifest_s->data != nullptr;
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );
        
    manifest_s->data = mapscript;

    CMSG_LOG("Loaded map_script: %s from %s\n", opened_file.GetFilePath(), opened_file.GetFileContainerName_Full() );

    return true;
}

void MapScriptManager::ReadSubAction( ParserSSV& parser, MapScript::Action& action ) {
    ASSERT( ! action.sub_action );

    std::string entry;
    if ( !parser.ReadWordOnLine( entry ) ) {
        ERR( "Mapscript %s: expected action argument for expression \"atTime\"\n", parser.GetFilePath().c_str() );
        if ( !parser.AtEndOfLine() )
            parser.SkipToNextNonblankLine();
        return;
    }

    if ( entry == "spawnEnemy" ) {
        if ( !parser.ReadWordOnLine( entry ) ) {
            ERR( "Mapscript %s: expected action classtype argument for expression \"spawnEnemy\"\n", parser.GetFilePath().c_str() );
            if ( !parser.AtEndOfLine() )
                parser.SkipToNextNonblankLine();
        }

        action.sub_action = std::make_unique<MapScript::Action>( MapScript::ActionType::mapscript_subaction_spawnEnemy );
        action.sub_action->sdata = entry;
        ReadDescriptors( parser, action.sub_action->descriptors );

        return;
    }

    ERR("Mapscript %s: Unknown sub-action: \"%s\"\n", parser.GetFilePath().c_str(), entry.c_str() );
    if ( !parser.AtEndOfLine() )
        parser.SkipToNextNonblankLine();
}

void MapScriptManager::ReadDescriptors( ParserSSV& parser, std::vector< std::pair<std::string,std::string> >& descriptors ) {
    std::string desc;
    std::string val;
    while ( parser.ReadWordOnLine( desc ) ) {
        if ( ! parser.ReadWordOnLine( val ) ) {
            ERR( "Mapscript %s: expected value for descriptor \"%s\"\n", parser.GetFilePath().c_str(), desc.c_str() );
            if ( !parser.AtEndOfLine() )
                parser.SkipToNextNonblankLine();
            return;
        }

        descriptors.emplace_back( desc, val );
    }
}
