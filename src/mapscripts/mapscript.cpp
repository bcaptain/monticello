// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./mapscript.h"
#include "../game.h"
#include "../entities/entity.h"
#include "../entities/entityInfoManager.h"

void MapScript::Clone( MapScript& other ) const {
    other.Erase();

    for ( auto & act : actions ) {
        other.actions.emplace_back();
        act.Clone( other.actions.back() );
    }
}

void MapScript::Action::Clone( MapScript::Action& other ) const {
    other.sub_action.reset();
    other.descriptors = descriptors;
    other.idata = idata;
    other.sdata = sdata;
    other.type = type;

    if ( sub_action ) {
        other.sub_action = std::make_unique< MapScript::Action >();
        sub_action->Clone( *other.sub_action );
    }
}

void MapScript::EvalActions( std::vector< MapScript::Action >& evalActs ) {
    for ( auto & act : evalActs )
        if ( !act.done )
            EvalAction( act );
}

void MapScript::EvalAction( MapScript::Action& act ) {
    switch ( act.type ) {
        case MapScript::ActionType::mapscript_action_atTime:
            Eval_AtTime( act );
            break;
        case MapScript::ActionType::mapscript_subaction_spawnEnemy:
            Eval_SpawnEnemy( act.sdata, act.descriptors );
            break;
        case MapScript::ActionType::mapscript_action_none: break;
        case MapScript::ActionType::mapscript_action_ifKilled: break;
        default:
            break;
    }
}

void MapScript::Eval_AtTime( MapScript::Action& act ) {
    if ( game->GetGameTime() < act.idata )
        return;

    if ( ! act.sub_action ) {
        ERR("MapScript: atTime specified with no sub-action\n" );
    }

    switch ( act.sub_action->type ) {
        case ActionType::mapscript_subaction_spawnEnemy:
            Eval_SpawnEnemy( act.sub_action->sdata, act.sub_action->descriptors );
            break;
        case ActionType::mapscript_action_none: FALLTHROUGH;
        case ActionType::mapscript_action_atTime: FALLTHROUGH;
        case ActionType::mapscript_action_ifKilled: FALLTHROUGH;
        default:
            ERR("MapScript: unknown atTime sub-action type: \"%i\"", static_cast<ActionType_BaseType>(act.sub_action->type) );
            break;
    }

    act.done = true;
}

void MapScript::Eval_SpawnEnemy( const std::string& classType, const std::vector< std::pair<std::string,std::string> >& desc ) {
    auto sptr = entityInfoManager.LoadAndSpawn( classType.c_str()  );
    if ( ! sptr )
        return;
        
    for ( auto const & d : desc ) {
        if ( d.first == "withName" ) {
            sptr->SetName( d.second );
        } else if ( d.first == "atPos" ) {
            Vec3f org;
            String::ToVec3f( d.second, org );
            sptr->SetOrigin( org );
        }
    }

    sptr->SetTeamName( "" ); //for now, enemies are just unnamed-team
}

void MapScript::EntityKilled( Entity& ent, std::vector< MapScript::Action >& evalActs ) {
    for ( auto & act : evalActs ) {
        if ( act.done )
            continue;

        switch ( act.type ) {
            case MapScript::ActionType::mapscript_action_ifKilled:
                if ( act.sdata == ent.GetName() ) {
                    if ( act.sub_action ) {
                        EvalAction( *act.sub_action );
                    }
                    act.done = true;
                }
                break;
            case MapScript::ActionType::mapscript_action_none: break;
            case MapScript::ActionType::mapscript_action_atTime: break;
            case MapScript::ActionType::mapscript_subaction_spawnEnemy: break;
            default:
                break;
        }
    }
}

void MapScript::Erase( void ) {
    actions.clear();
}
