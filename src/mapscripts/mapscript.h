// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MAPSCRIPT_H_
#define SRC_MAPSCRIPT_H_

#include "../base/main.h"

struct MapScript {
    MapScript( void ) : actions() { }

    typedef uint ActionType_BaseType;
    enum class ActionType : ActionType_BaseType {
        mapscript_action_none=0,
        mapscript_action_atTime,
        mapscript_action_ifKilled,
        mapscript_subaction_spawnEnemy,
    };

    struct Action {
        void Clone( MapScript::Action& other ) const;

        Action( MapScript::ActionType _type ) : sub_action(), descriptors(), idata(), sdata(), type( _type ), done( false ) { }
        Action( void ) : sub_action(), descriptors(), idata(), sdata(), type( MapScript::ActionType::mapscript_action_none ), done( false ) { }

        std::unique_ptr< Action > sub_action;
        std::vector< std::pair<std::string,std::string> > descriptors;
        uint idata;
        std::string sdata;
        MapScript::ActionType type;
        bool done;
    };

public:
    void Erase( void );
    void Clone( MapScript& other ) const;

    static void EvalActions( std::vector< MapScript::Action >& evalActs ); // to be called by Game class

    // Actions
    static void Eval_AtTime( MapScript::Action& act );

    // Sub-Actions
    static void Eval_SpawnEnemy( const std::string& classType, const std::vector< std::pair<std::string,std::string> >& desc );

    static void EntityKilled( Entity& ent, std::vector< MapScript::Action >& evalActs );

    static void EvalAction( MapScript::Action& act );

public:
    std::vector< MapScript::Action > actions;
};

#endif // SRC_MAPSCRIPT_H_
