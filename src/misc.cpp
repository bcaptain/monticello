// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./input/input.h"
#include "./game.h"
#include "./misc.h"
#include "./entities/entity.h"
#include "./rendering/renderer.h"
#include "./commandsys.h"

#undef OBJECTTREE_DEBUG
#undef SPRITESTACK_DEBUG

void DrawPoint( const float x, const float y, const float z, const char* name ) {
     std::string p("point ");
     p += name;
     p += " ";
     p += std::to_string(x);
     p += ",";
     p += std::to_string(y);
     p += ",";
     p += std::to_string(z);

     // make it dark
//     CommandSys::Execute("delallshapes");

     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
}

void DrawPoint( const Vec3f& p, const char* name ) {
    DrawPoint( p.x, p.y, p.z, name );
}

void DrawLine( const Vec3f& start, const Vec3f& end, const char* name ) {
     std::string p("line ");
     p += name;
     p += " ";
     p += std::to_string(start.x);
     p += ",";
     p += std::to_string(start.y);
     p += ",";
     p += std::to_string(start.z);
     p += " ";
     p += std::to_string(end.x);
     p += ",";
     p += std::to_string(end.y);
     p += ",";
     p += std::to_string(end.z);

     // make it dark
//     CommandSys::Execute("delallshapes");

     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
     CommandSys::Execute(p.c_str());
}

void SortVerts( std::vector< Vec3f >& verts, const Vec3f& axis ) {

    for ( uint i=0; i < verts.size(); i++) {
        bool finished = true;

        for ( uint v=0; v < verts.size()-2; v++) {
            Vec3f h = verts[v+1] - verts[0];
            Vec3f j = verts[v+2] - verts[0];

            Vec3f c = h.Cross(j);
            c.Normalize();

            if ( c.Dot(axis) < 0 ) {
                finished = false;

                auto tmp = verts[v+2];
                verts[v+2] = verts[v+1];
                verts[v+1] = tmp;
            }
        }

        if (finished)
            return;
    }
}

extern Game* game;
class Actor : public Entity { };

std::shared_ptr< Entity > GetPlayerIfInList( const std::vector< CollisionItem >& list ) {
    auto player_ent = game->GetPlayerEntity();
    if ( ! player_ent )
        return nullptr;
        
    for ( auto const & item : list ) {
        auto ent = item.ent.lock();
        if ( game->IsPlayer( ent ) )
            return ent;
    }
    
    return nullptr;
}

bool Unproject( const Vec3f& winCoords, const Mat4f& modelMatrix, const Mat4f& projMatrix, const int viewport[4], Vec3f& point_out ) {
    Mat4f finalMatrix = projMatrix * modelMatrix; //todomatmult
    
    if ( ! finalMatrix.Invert() )
        return false;

    Vec3f in( winCoords );

    // ** map x and y from window coordinates
    in.x = (in.x - viewport[0]) / viewport[2];
    in.y = (in.y - viewport[1]) / viewport[3];

    // *** map to range from -1 to 1
    in.x = in.x * 2 - 1;
    in.y = in.y * 2 - 1;
    in.z = in.z * 2 - 1;

    point_out = in * finalMatrix;
    return true;
}

Line3f GetPickLine( Vec3f mouse_pos ) {
    const int *vp = renderer->GetViewPort();
    mouse_pos.y = vp[3] - mouse_pos.y; // our mouse pos is from top of screen (inverted y axis)
    mouse_pos.z = globalVals.GetFloat(gval_v_farPlane);
    Vec3f near_p, far_p;
    if ( !Unproject( Vec3f( mouse_pos.x, mouse_pos.y, 0), renderer->ModelView(), renderer->Projection(), vp, near_p ) ||
         !Unproject( Vec3f( mouse_pos.x, mouse_pos.y, 1.0f), renderer->ModelView(), renderer->Projection(), vp, far_p )
    ) {
        WARN("Couldn't GetPickLine().\n");
        return {};
    }

    return Line3f( near_p, far_p );
}

Line3f GetPickLine( void ) {
    return GetPickLine( Input::GetWindowMousePos() );
}

#ifdef MONTICELLO_EDITOR

#include "./editor/editor.h"
#include "./entities/modules/flat.h"
#include "./entities/debug/sweepDebug.h"
#include "./entities/debug/clipDebug.h"
#include "./entities/entityInfoManager.h"

void debug_shatter_flat( void ) {
    if ( ! editor )
        return;
        
    auto ent = editor->GetSelectedEntity().lock();
    if ( ! ent )
        return;

    if ( ! derives_from< Flat >( *ent ) )
        return;

    auto flat = std::static_pointer_cast<Flat>( ent );
    flat->ModelChanged();
    flat->Fracture(3);
}

void debug_merge_models( void ) { 
    if ( ! editor )
        return;
        
    auto ent = editor->GetSelectedEntity().lock();
    if ( ! ent )
        return;
        
    auto mdl = ent->GetModel();
    auto test = mdl->GetCopy();
    auto sptr = game->NewEnt( TypeID_Entity );
    sptr->SetModel( test );
    sptr->bounds.SetToAABox3D(*test);
    sptr->Spawn();
}

void debug_print_entity_indices( void ) { 
    for ( uint i=0; i<game->all_entities.size(); ++i ) {
        auto const & ent = game->all_entities[i];
        Printf("index %u: %s.\n", i, ent->GetIdentifier().c_str() );
    }
}

// (this comment only here for global searches: debugprint, printdebug debugout
void debugOutput( void ) {
    //debug_merge_models();
    //debug_shatter_flat();
    //debug_print_entity_indices();
    
    if ( ! editor )
        return;
        
    auto ent = editor->GetSelectedEntity().lock();
    if ( ! ent )
        return;
    
    ent->bounds.SetToAABox3D();
}

void GetGroupOrigin( const LinkList< std::weak_ptr< Entity> > &ent_list, Vec3f& origin_out ) {
    Vec3f origin_max;
    Vec3f origin_min;

    origin_out.Zero();

    int count = 0;
    for ( auto const& entHandle : ent_list ) {
        if ( auto ent = entHandle.lock() ) {
            if ( count == 0 ) {
                origin_out = ent->GetOrigin();
                origin_min = origin_max = origin_out;
            } else {
                origin_out = ent->GetOrigin();
                origin_max.x = std::fmaxf( origin_max.x, origin_out.x );
                origin_max.y = std::fmaxf( origin_max.y, origin_out.y );
                origin_max.z = std::fmaxf( origin_max.z, origin_out.z );

                origin_min.x = std::fminf( origin_min.x, origin_out.x );
                origin_min.y = std::fminf( origin_min.y, origin_out.y );
                origin_min.z = std::fminf( origin_min.z, origin_out.z );
            }
            ++count;
        }
    }

    if ( count > 1 ) {
        origin_out = origin_min;
        origin_out += origin_max;
        origin_out /= 2;
    } // otherwise origin_out will be zeroed for 0, or the first entity's origin
}

#endif // MONTICELLO_EDITOR

void SnapOrigin( Vec3f& origin, const float to_meters ) {
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal" // fine
    ASSERT_MSG( to_meters == 1.0f || to_meters == 0.5f, "snapping to anything but one or half meter is not yet supported" );
    #pragma GCC diagnostic pop
    if ( to_meters < 1.0f ) {
        origin.x = RoundToNearestHalf( origin.x );
        origin.y = RoundToNearestHalf( origin.y );
        origin.z = RoundToNearestHalf( origin.z );
    } else {
        origin.x = Round( origin.x );
        origin.y = Round( origin.y );
        origin.z = Round( origin.z );
    }
}

bool VerifyArgNum( const char* func_name, const std::vector< std::string >& args, const uint expected_num ) {
    if ( args.size() >= expected_num )
        return true;

    std::string arg_string;
    if ( args.size() > 0 ) {
        arg_string = args[0];
        for ( uint i=1; i<args.size(); ++i ) {
            arg_string += ", ";
            arg_string += args[i];
        }
    }

    ERR("%s: not enough arguments passed (%i): %s\n", func_name, args.size(), arg_string.c_str() );
    return false;
}
