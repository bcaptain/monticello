// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./base/main.h"
#include "./game.h"
#include "./saveFile.h"
#include "./editor/editor.h"
#include "./entities/entityInfoManager.h"
#include "./entities/actor.h"
#include "./rendering/renderer.h"
#include "./nav/navMesh.h"

class Entity;

FileMap::FileMap( void )
    : entities()
    , width(0)
    , length(0)
    , height(0)
    , hasPlayer(false)
    , playerIndex(0)
    , hasFollowEnt(false)
    , followEntIndex(0)
    , parser( *this )
{ }

FileMap::~FileMap( void ) {
}

// todo:write and read across level saves (or any entity search) could be faster if we implement a parallel Game::sorted_entities to Game::all_entities
void FileMap::WriteEntityRef( const Entity* ent ) {
    Uint i = 0;
    
    const uint map_file_version = 0;
    
    parser.WriteUInt( map_file_version );
    
    if ( ! ent ) {
        parser.WriteBool( false );
        return;
    }

    for ( auto& item : entities ) {
        ASSERT( item );
          if ( item->GetIndex() == ent->GetIndex() )
              break;

        ++i;
    }

    if ( i >= entities.size() ) {
        ERR("Bad entity index in save file (%u), cannot save. The entity does not exist in Game::all_entities. The entity is \"%s\"\n", i, ent->GetIdentifier().c_str() );
        parser.WriteBool( false );
        return;
    }
    
    parser.WriteBool( true );
    parser.WriteUInt( i );
}

std::shared_ptr< Entity > FileMap::ReadEntityRef( void ) {
    [[maybe_unused]] const uint map_version = parser.ReadUInt();
    
    if ( ! parser.ReadBool() )
        return {};

    const uint i =parser.ReadUInt();

    if ( i >= entities.size() ) {
        ERR_DIALOG("Bad entity index in save file.\n");
        return {};
    }

    return entities[i];
}

bool FileMap::Save( const char * fname ) {
    const Uint32 startTime = game->GetRealTime();
    CMSG_LOG("Saving: %s\n", fname );

    if ( ! Open( fname, "wb" ) ) {
        ERR("Couldn't open map file for save: %s.\n", fname);
        return false;
    }

    const Uint8 map_version = 0;
    parser.WriteUInt8( map_version );

    const Vec3f world_size( globalVals.GetVec3f( gval_g_octreeSize ) );
    parser.WriteUInt8( static_cast< Uint8 >( world_size.x ) );
    parser.WriteUInt8( static_cast< Uint8 >( world_size.y ) );
    parser.WriteUInt8( static_cast< Uint8 >( world_size.z ) );

    LinkList< std::string> string_pool;

    // we'll come back to here and write the address of string pool later
    const long string_pool_address_fpos = FTell();
    parser.WriteInt( 0 );

    NavMesh::MapSaveAll( *this );
    SaveEntities( string_pool );

    // ** write string table
    int string_pool_fpos = FTell(); // get the address in the file of the string table
    parser.WriteUInt( string_pool.Num() );
    for ( auto& str : string_pool )
        parser.WriteStr( str );

    // ** go back and write string pool address, then come back to this fpos
    const long cur_pos = FTell();
    SeekSet( string_pool_address_fpos );
    parser.WriteInt( string_pool_fpos );
    SeekSet( cur_pos );
    Close();

    // cleanup
    entities.clear();

    const Uint32 entTime = game->GetRealTime();
    CMSG_LOG("Map %s saved in %ums\n", fname, entTime - startTime);
    return true;
}

bool FileMap::Load( void ) {
    if ( ! IsOpen() )
        return false; // not throwing errors, Game will handle that so we can sort of "brute force" the mapload from various possible file locations
        
    const Uint32 startTime = game->GetRealTime();

    Uint8 map_version;
    parser.ReadUInt8( map_version );

    width = parser.ReadUInt8();
    length = parser.ReadUInt8();
    height = parser.ReadUInt8();

    Uint string_count;

    int string_pool_fpos;
    parser.ReadInt( string_pool_fpos );

    // ** jump ahead read string table and then come back
    int cur_pos = FTell();
    SeekSet( string_pool_fpos );
    parser.ReadUInt( string_count );
    std::vector< std::string > string_pool;
    string_pool.reserve( string_count );
    for ( uint i=0; i<string_count; ++i )
        string_pool.emplace_back( parser.ReadStr() );
    SeekSet( cur_pos );

    NavMesh::MapLoadAll( *this );
    LoadEntities( string_pool );

    Close();
    
    const Uint32 endTime = game->GetRealTime();
    
    CMSG_LOG("Map loaded in %ums\n", endTime - startTime);
    return true;
}

void FileMap::SaveEntities( LinkList< std::string >& string_pool ) {
    // we'll come back to here and write the number of entities
    const long num_entities_fpos = FTell();
    parser.WriteUInt( 0 );

    // ** first save each entity's classname and entname ( we do not call MapSave() on them yet )
    uint player_index = 0;
    bool player_found = false;

    uint focus_ent_index = 0;
    bool focus_entity_found = false;

    auto player = game->GetPlayerEntity();
    auto follow_ent = renderer->camera.GetFollowEntity();

    uint cnt=0;
    for ( auto& ent : game->GetEntityListRef() ) {
        if ( ent && ent->GetNumVoxelsOccupied() > 0 ) {
            parser.WriteUInt( string_pool.AppendUnique( get_classname(*ent) ) );
            parser.WriteUInt( string_pool.AppendUnique( ent->GetEntName() ) );
            if ( !player_found && ent == player ) {
                player_found = true;
                player_index = cnt;
            }

            if ( !focus_entity_found ) {
                if ( follow_ent ) {
                    if ( ent == follow_ent ) {
                        focus_entity_found = true;
                        focus_ent_index = cnt;
                    }
                }
            }

            ++cnt;
        }
    }

    // ** populate the entities array now that we have a count of all entities
    entities.clear();
    for ( auto& ent : game->GetEntityListRef() ) {
        if ( ent && ent->GetNumVoxelsOccupied() > 0 ) {
            entities.push_back( ent );
        }
    }

    // ** then save all the rest of entities in detail
    for ( auto const& ent : entities )
        ent->MapSave( *this, string_pool );

    parser.WriteBool( player_found );
    if ( player_found )
        parser.WriteUInt( player_index );

    parser.WriteBool( focus_entity_found );
    if ( focus_entity_found )
        parser.WriteUInt( focus_ent_index );

    // ** go back and write num entities, and then return back to this position
    const long cur_pos = FTell();
    SeekSet( num_entities_fpos );
    parser.WriteUInt( entities.size() );
    SeekSet( cur_pos );
}

void FileMap::LoadEntities( const std::vector< std::string >& string_pool ) {
    uint numEntities = 0;
    parser.ReadUInt( numEntities );
    Uint idx, i;

    // ** first create all bare-bones entities based on the .ent files
    entities.clear();
    for ( i=0; i<numEntities; i++ ) {
        parser.ReadUInt( idx );

        // entity's ctor will fill it with the defaults from it's .ent file if we are loading a savegame
        auto ptr = game->NewUnmanagedEnt( get_typeid( string_pool[idx].c_str() ) );
        ASSERT( ptr );
        entities.push_back( ptr );
        parser.ReadUInt( idx );
        entities[i]->SetEntName( string_pool[idx].c_str() );
    }

    /* later
    std::string *tmp;
    if ( entities[i]->GetTypeName() != defEntity->GetTypeName() ) {
        Entity *tmp;

        // The saved classname differs from the entityInfo's classname, we must first load the entity as
        // it's old type since that's how it is saved to file. Then we will convert it to the new type
        // via operator=. The next time the entity is saved, it will save as the new type.

        WARN("Changing entity of type %s (from map %s from %s) to type %s is allowed by may not have the effect you want. If it is not a base class of the new type, the type will be sliced.\n", defEntity->GetTypeName().c_str(), GetFilePath().c_str(), GetFileContainerName().c_str(), defEntity->GetTypeName().c_str() );

        const TypeID type = Getget_typeid( *defEntity->GetTypeName().c_str() );
        entities[i] = NewUnmanagedEnt( type );
        tmp = NewUnmanagedEnt( get_typeid( *typeName ) );
        *entities[i] = *tmp;
        DELNULL( tmp );
    } */

    for ( auto& ent : entities ) {
        ent->MapLoad( *this, string_pool );
    }
        
    hasPlayer = parser.ReadBool();
    if ( hasPlayer )
        playerIndex = parser.ReadUInt();

    hasFollowEnt = parser.ReadBool();
    if ( hasFollowEnt )
        followEntIndex = parser.ReadUInt();
}

void FileMap::SetFollowEnt( void ) const {
    if ( !hasFollowEnt )
        return;

    ASSERT( followEntIndex < entities.size() );
    renderer->camera.SetFollowEntity( std::static_pointer_cast< Actor >( entities[followEntIndex] ) );
}

void FileMap::SetPlayer( void ) const {
    if ( !hasPlayer )
        return;
    
    ASSERT( playerIndex < entities.size() );
    auto player = std::static_pointer_cast< Actor >( entities[playerIndex] );
    game->SetPlayer( player );
}

bool FileMap::Load( const char * fname ) {
    Open( fname, "rb" );
    return Load();
}

void FileMap::WriteAABox3D( const AABox3D& aabox ) {
    parser.WriteFloat( aabox.min.x );
    parser.WriteFloat( aabox.max.x );
    parser.WriteFloat( aabox.min.y );
    parser.WriteFloat( aabox.max.y );
    parser.WriteFloat( aabox.min.z );
    parser.WriteFloat( aabox.max.z );
}

void FileMap::ReadAABox3D( AABox3D& aabox ) {
    parser.ReadFloat( aabox.min.x );
    parser.ReadFloat( aabox.max.x );
    parser.ReadFloat( aabox.min.y );
    parser.ReadFloat( aabox.max.y );
    parser.ReadFloat( aabox.min.z );
    parser.ReadFloat( aabox.max.z );
}

void FileMap::WriteBox3D( const Box3D& box ) {
    for ( uint i=0;i<8;++i )
        parser.WriteVec3f( box.VertAt(i) );
}

void FileMap::ReadBox3D( Box3D& box ) {
    Vec3f verts[8];
    for ( uint i=0;i<8;++i )
        parser.ReadVec3f( verts[i] );
    box.Set( verts );
}

void FileMap::ReadDict( Dict& dict ) {
    uint d_size;
    parser.ReadUInt( d_size );
    if ( d_size != dict.GetTableSize() ) {
        dict.DestroyAndResize( d_size );
        WARN("ReadDict(): Dicts were different sizes, old values lost.\n");
    }

    uint numEntVals;
    parser.ReadUInt( numEntVals );

    std::string key, val;

    for ( uint i=0; i<numEntVals; i++ ) {
        parser.ReadStr( key );
        parser.ReadStr( val );
        dict.Set( key.c_str(), val.c_str() );
    }
}

void FileMap::WriteDict_IgnoreFromVec( const Dict& dict, const std::vector< DictEntry >* values_to_ignore ) {
    parser.WriteUInt( dict.GetTableSize() );

    // we'll come back to this position to write how many entries there are
    long int count_pos = FTell();
    parser.WriteUInt( 0 );

    std::size_t numEntVals = 0;
    for ( auto const & pair : dict ) {

        // if the value is the same as one in our ignore dict, don't save it.
        if ( values_to_ignore ) {
            if ( Contains( std::cbegin(*values_to_ignore), std::cend(*values_to_ignore), pair ) ) {
                continue;
            }
        }

        ++numEntVals;
        parser.WriteStr( pair.first );
        parser.WriteStr( pair.second );
    }

    long int bak_pos = FTell();
    SeekSet( count_pos );
    parser.WriteUInt( numEntVals );
    SeekSet( bak_pos );
}

void FileMap::WriteDict_IgnoreFromDict( const Dict& dict, const Dict* values_to_ignore ) {
    parser.WriteUInt( dict.GetTableSize() );

    // we'll come back to this position to write how many entries there are
    long int count_pos = FTell();
    parser.WriteUInt( 0 );

    std::size_t numEntVals = 0;

    for ( auto const & pair : dict ) {

        // if the value is the same as one in our ignore dict, don't save it.
        if ( values_to_ignore ) {
            const std::string *ignored_val = values_to_ignore->Get( pair.first.c_str() );
            if ( ignored_val && *ignored_val == pair.second )
                continue;
        }

        ++numEntVals;
        parser.WriteStr( pair.first );
        parser.WriteStr( pair.second );
    }

    long int bak_pos = FTell();
    SeekSet( count_pos );
    parser.WriteUInt( numEntVals );
    SeekSet( bak_pos );
}

void FileMap::WriteDict( const Dict& dict ) {
    return WriteDict_IgnoreFromVec( dict, nullptr );
}

Uint8 FileMap::GetMapWidth( void ) const {
    return width;
}

Uint8 FileMap::GetMapLength( void ) const {
    return length;
}

Uint8 FileMap::GetMapHeight( void ) const {
    return height;
}
