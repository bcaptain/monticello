// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./spriteList.h"
#include "./sprite.h"
#include "../rendering/renderer.h"

SpriteList::SpriteList( void )
    : list()
    , owner( nullptr )
{ }

SpriteList::SpriteList( Sprite* _owner )
    : list()
    , owner( _owner )
{ }

SpriteList::~SpriteList( void ) {
    DelAll();
}

void SpriteList::DelAll( void ) {
    ClearPointerVector<Sprite>( list );
}

Sprite* SpriteList::FindNamed( const std::string& name ) {
    for ( auto& sp : list ) {
        if ( sp ) {
            if ( sp->GetName() == name ) {
                return sp;
            }

            if ( sp->children ) {
                sp->children->FindNamed( name );
            }
        }
    }

    return nullptr;
}

int SpriteList::Allocated( void ) {
    int count=0;

    for ( auto& sprite_ptr : list  )
        if ( sprite_ptr )
            ++count;

    return count;
}

void SpriteList::DelNulls( void ) {
    list.erase( std::remove( std::begin( list ), std::end( list ), nullptr ), std::end( list ) );
}

void SpriteList::Del( const Sprite* sprite ) {
    // serach backward, generally more likely that we are removing something that is in the foreground (dialogs, etc)
    for ( uint i=list.size(); i>0; ) {
        --i;
        if ( list[i] == sprite ) {
            delete list[i];
            list[i] = nullptr;
            DelNulls();
            return;
        }
    }
}

void SpriteList::SetVisible( const bool whether ) {
    for ( auto& sp : list )
        if ( sp )
            sp->SetVisible( whether );
}

void SpriteList::DelAt( const uint index ) {
    if ( index >= list.size() )
        return;

    delete list[index];

    for ( uint i=index+1; i<list.size(); ++i )
        list[i-1]=list[i];

    list.resize( list.size() - 1 );
}

void SpriteList::Add( Sprite* sprite ) {
    if ( ! sprite )
        return;

    sprite->parent = owner;
    list.push_back( sprite );
}

void SpriteList::DrawVBO( void ) const {
    renderer->DrawSpriteList( list );
}

void SpriteList::UpdateVBO( void )  {
    for ( auto& sprite_ptr : list ) {
        ASSERT( sprite_ptr );
        sprite_ptr->UpdateVBO();
    }
}
