// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_SPRITES_SPRITE_H_
#define SRC_SPRITES_SPRITE_H_

#include "../base/main.h"
#include "../entities/object.h"
#include "../rendering/face.h"

class SpriteList;
class Material;

/*!

Sprite \n\n

a static image which other classes derive from

**/

class Sprite : public Object {
public:
    Sprite( void );
    Sprite( const float width, const float height );
    ~Sprite( void ) override;
    Sprite( const Sprite& other ) = delete;
private:
    void Construct( void );

public:
    Sprite& operator=( const Sprite& other ) = delete;

public:
    virtual void Think( void );
    
    virtual Vec3f GetOrigin( void ) const;
    virtual void SetOrigin( const Vec3f & org );
    virtual void SetOrigin( const float x, const float y, const float z );
    virtual void SetOriginOffset( const Vec3f& _originOffset );
    virtual Vec3f GetOriginOffset( void ) const;
    void NudgeOrigin( const Vec3f & amt );

    Face2D* GetFace( void );

    float GetHeight( void ) const;
    float GetWidth( void ) const;

    void SetHeight( const float height );
    void SetWidth( const float width );
    void SetDimensions( const float width, const float height );

    void SetFaceBottomColor( const Vec3f& color );
    void SetFaceTopColor( const Vec3f& color );
    void SetFaceColor( const Vec3f& color );
    void SetFaceOpacity( const float opac );
    float GetFaceOpacity( void ) const;
    void UnsetGradient( void );

    void DoRotation( void ) const;

    void DrawVBO( void ) const override;
    virtual void UpdateVBO( void ); //!< vbo will be updated at next render

    void SetColor( const Color4f& newColor );

    Sprite* GetParent( void ) const; //!< if this sprite is a child, return the parent, otherwise return nullptr
    bool AreAllParentsVisible( void ) const;

public:
    bool SetMaterial( const std::shared_ptr< const Material >& mtl );
    bool SetMaterial( const char* mtl );

    void UnsetMaterial( void );
    std::shared_ptr< const Material > GetMaterial( void ) const;
    void SetupUV( void );
    void RemoveUV( void );

protected:
    void pack_vbo( void ) const;
protected:
    Vec3f origin;
    Vec3f origin_offset;
public: //todo: protected
    SpriteList* children;
    Sprite* parent;
protected:
    Face2D face;
    Color4f color;
    std::shared_ptr< const Material > material;
    VBO<float>* vbo_uv;
    VBO<float>* vbo_vert; //!< used for vertices
    VBO<float>* vbo_secondary; //!< forseeable uses include texture UVs or vertex colors.
};

#endif  // SRC_SPRITES_SPRITE_H_
