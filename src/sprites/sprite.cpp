// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./sprite.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../sprites/spriteList.h"
#include "../rendering/materialManager.h"

Sprite::Sprite( void )
    : origin()
    , origin_offset()
    , children( nullptr )
    , parent( nullptr )
    , face()
    , color(0,0,0,0)
    , material( nullptr )
    , vbo_uv( nullptr )
    , vbo_vert(nullptr)
    , vbo_secondary(nullptr)
    {

    Construct();
}

Sprite::Sprite( const float width, const float height )
    : origin()
    , origin_offset()
    , children( nullptr )
    , parent( nullptr )
    , face()
    , color(0,0,0,0)
    , material( nullptr )
    , vbo_uv( nullptr )
    , vbo_vert(nullptr)
    , vbo_secondary(nullptr)
    {

    face.BuildRect( width, height );
    Construct();
}

void Sprite::Construct( void ) {
    ASSERT( !vbo_vert );
    vbo_vert = new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY);
    vbo_secondary = new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY); // used for background color
    // vbo_uv will be initialized elsewhere if needed
    UpdateVBO();
}

Sprite::~Sprite( void ) {
    delete vbo_uv;
    delete vbo_vert;
    delete vbo_secondary;
    delete children;
}

Vec3f Sprite::GetOrigin( void ) const {
    return origin;
}

bool Sprite::AreAllParentsVisible( void ) const {
    if ( !parent )
        return true;

    return parent->IsVisible() && parent->AreAllParentsVisible();
}

void Sprite::Think( void ) {
}

void Sprite::UpdateVBO( void )  {
    // VBO will be updated on the next rendering
    ASSERT( vbo_vert );
    ASSERT( vbo_secondary );

    vbo_vert->Clear();
    vbo_secondary->Clear();

    if ( vbo_uv ) // sprite might not have an image on it, so this is safe to be null
        vbo_uv->Clear();

    if ( children )
        children->UpdateVBO();
}

void Sprite::DrawVBO( void ) const {
    ASSERT( vbo_secondary );
    ASSERT( vbo_vert );

    if ( !vbo_vert->Finalized() || !vbo_secondary->Finalized() || ( vbo_uv && !vbo_uv->Finalized() ) ) {
        pack_vbo();
    }

    // ** image
    if ( vbo_uv ) {
        if ( !material || !material->GetDiffuse() ) {
            DIE("Could not find material or diffuse when drawing sprite.\n");
            return;
        }

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SendData_Matrices();
        shaders->SetTexture( material->GetDiffuse()->GetID() );
        shaders->SetAttrib( "vPos", *vbo_vert );
        shaders->SetAttrib( "vUV", *vbo_uv );
        shaders->SetUniform4f("vColor", color.r, color.g, color.b, 1 );
        shaders->SetUniform1f("fWeight", color.a );
        shaders->DrawArrays( GL_QUADS, 0, vbo_vert->Num() );

    } else { // we don't draw the background if we have an overlay image

        // ** background color
        shaders->UseProg(  GLPROG_GRADIENT  );
        shaders->SendData_Matrices();
        shaders->SetAttrib( "vColor", *vbo_secondary ); // color
        shaders->SetAttrib( "vPos", *vbo_vert );
        shaders->DrawArrays( GL_QUADS, 0, vbo_vert->Num() );
    }

    if ( children )
        children->DrawVBO();
}

void Sprite::NudgeOrigin( const Vec3f & amt ) {
    origin += amt;
}

void Sprite::SetOrigin( const Vec3f & _origin ) {
    origin = _origin;
}

void Sprite::SetOrigin( const float x, const float y, const float z ) {
    origin.Set( x, y, z );
}

void Sprite::SetHeight( const float height ) {
    face.BuildRect(GetWidth(),height);
    UpdateVBO();
}

void Sprite::SetWidth( const float width ) {
    face.BuildRect(width,GetHeight());
    UpdateVBO();
}

void Sprite::SetDimensions( const float width, const float height ) {
    face.BuildRect(width,height);
    UpdateVBO();
}

void Sprite::SetFaceBottomColor( const Vec3f& newColor ) {
    std::size_t num = face.NumVert();

    if ( num < 2 )
        return;

    for ( std::size_t v = num/2; v < num; ++v )
        face.vert[v].color = newColor;

    UpdateVBO();
}

void Sprite::SetFaceColor( const Vec3f& newColor ) {
    face.SetColor( newColor );
    UpdateVBO();
}

void Sprite::SetFaceTopColor( const Vec3f& newColor ) {
    std::size_t num = face.NumVert();

    if ( num < 2 )
        return;

    num /= 2;

    for ( std::size_t v = 0; v < num; ++v )
        face.vert[v].color = newColor;

    UpdateVBO();
}

void Sprite::UnsetGradient( void ) {
    std::size_t num = face.NumVert();

    if ( num < 2 )
        return;

    for ( std::size_t v = num/2; v < num; ++v )
        face.vert[v].color = face.vert[0].color;

    UpdateVBO();
}

void Sprite::pack_vbo( void ) const {
    ASSERT( vbo_vert );
    ASSERT( vbo_secondary );

    vbo_vert->Clear();
    vbo_secondary->Clear();

    vbo_secondary->PackFace2DColors( face ); // bg color

    if ( vbo_uv ) {
        vbo_uv->PackFace2DTexCoords( face ); // bg image uv
        vbo_uv->MoveToVideoCard();
    }

    vbo_vert->PackFace2DVerts( face );

    vbo_vert->MoveToVideoCard();
    vbo_secondary->MoveToVideoCard();
}

bool Sprite::SetMaterial( const char* mtl ) {
    return SetMaterial( materialManager.Get( mtl ) );
}

bool Sprite::SetMaterial( const std::shared_ptr< const Material >& mtl ) {
    if ( !mtl )
        return false;

    material = mtl;
    return true;
}

void Sprite::RemoveUV( void ) {
    delete vbo_uv;
    vbo_uv = nullptr;
}

void Sprite::SetupUV( void ) {
    if ( ! vbo_uv ) {
        vbo_uv = new VBO<float>(2,VBOChangeFrequencyT::RARELY);
        vbo_uv->PackQuad2D_CCW_BR();
        vbo_uv->MoveToVideoCard();

        ASSERT( vbo_uv->Finalized() );
    }
}

Sprite* Sprite::GetParent( void ) const {
    return parent;
}

void Sprite::SetColor( const Color4f& newColor ) {
    color = newColor;
}

void Sprite::SetFaceOpacity( const float opac ) {
    face.SetOpacity( opac );
}

float Sprite::GetFaceOpacity( void ) const {
    return face.GetOpacity();
}

float Sprite::GetHeight( void ) const {
    return Face2D::GetHeight( face );
}

float Sprite::GetWidth( void ) const {
    return Face2D::GetWidth( face );
}

Face2D* Sprite::GetFace( void ) {
    return &face;
}

std::shared_ptr< const Material > Sprite::GetMaterial( void ) const {
    return material;
}

void Sprite::UnsetMaterial( void ) {
    material = nullptr;
}

Vec3f Sprite::GetOriginOffset( void ) const {
    return origin_offset;
}

void Sprite::SetOriginOffset( const Vec3f& _originOffset ) {
    origin_offset = _originOffset;
}
