// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./meter.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../sprites/spriteList.h"
#include "../rendering/materialManager.h"

Meter::Meter( void )
    : mask()
    , progress(0)
    , sub_progress(0)
    , mask_type(MeterMaskType::DIMMER)
{ }

Meter::Meter( const float width, const float height )
    : Sprite( width, height )
    , mask()
    , progress(0)
    , sub_progress(0)
    , mask_type(MeterMaskType::DIMMER)
{ }
    
void Meter::DrawVBO( void ) const {
    ASSERT( vbo_secondary );
    ASSERT( vbo_vert );
    ASSERT( vbo_uv );

    if ( !vbo_vert->Finalized() || !vbo_secondary->Finalized() || ( vbo_uv && !vbo_uv->Finalized() ) ) {
        pack_vbo();
    }

    if( !mask ) {
        WARN("No material received for mask slot during draw call for Meter\n");
        return;
    }
    
    switch ( mask_type ) {
        case MeterMaskType::DIMMER : 
            shaders->UseProg( GLPROG_METER_TEX_DIMMER_RGB_MASK ); 
            shaders->SetUniform1f( "subPercentage", sub_progress );
            break;
        case MeterMaskType::HARD_ALPHA : shaders->UseProg( GLPROG_METER_TEX_HARD_ALPHA_MASK ); break;
        default : shaders->UseProg( GLPROG_METER_TEX_DIMMER_RGB_MASK );
    }
    
    shaders->SetTexture( material->GetDiffuse()->GetID() );
    shaders->SetTexture( mask->GetDiffuse()->GetID(), 1, "mask" );
    shaders->SetUniform1f( "percentage", progress );
    shaders->SetAttrib( "vUV", *vbo_uv );
    shaders->SendData_Matrices();
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->DrawArrays( GL_QUADS, 0, vbo_vert->Num() );
    
    if ( children )
        children->DrawVBO();
}

bool Meter::SetMaskMaterial( const char* mtl ) {
    return SetMaskMaterial( materialManager.Get( mtl ) );
}

bool Meter::SetMaskMaterial( const std::shared_ptr< const Material >& mtl ) {
    if ( !mtl )
        return false;

    mask = mtl;
    return true;
}

void Meter::SetMaskType( const MeterMaskType _maskType ) {
    mask_type = _maskType;
}

MeterMaskType Meter::GetMaskType( void ) const {
    return mask_type;
}
