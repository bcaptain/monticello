// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_SPRITES_METER_H_
#define SRC_SPRITES_METER_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./sprite.h"

typedef uint MeterMaskType_BaseType;
enum class MeterMaskType : MeterMaskType_BaseType {
    DIMMER = 0
    , HARD_ALPHA = 1
};

class Material;

/*!

Meter \n\n

an image that uses a gradient mask

**/

class Meter : public Sprite {
public:
    Meter( void );
    Meter( const float width, const float height );
    ~Meter( void ) override {};
    Meter( const Meter& other ) = delete;

public:
    Meter& operator=( const Meter& other ) = delete;

public:
    void DrawVBO( void ) const override;
    
    bool SetMaskMaterial( const std::shared_ptr< const Material >& mtl );
    bool SetMaskMaterial( const char* mtl );
    
    void SetMaskType( const MeterMaskType _maskType );
    MeterMaskType GetMaskType( void ) const;
    
public:
    std::shared_ptr< const Material > mask;
    float progress;
    float sub_progress;
    MeterMaskType mask_type;
    
};

#endif  // SRC_SPRITES_METER_H_
