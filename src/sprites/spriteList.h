// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_SPRITES_SPRITELIST_H_
#define SRC_SPRITES_SPRITELIST_H_

#include "../base/main.h"
#include "../game.h"

class Sprite;

/*!

SpriteList \n\n

specialized methods for lists of sprites.
deletes sprites automatically in dtor.

**/

class SpriteList {
    friend Game::Game( void ); // game ctor needs to access this class's ctor

//! we want to force the programmer to specify an owner, and null is okay. just as a reminger that we may expect one.
private:
    SpriteList( void );
    SpriteList( const SpriteList& other ) = delete;

public:
    SpriteList( Sprite* _owner ); //!< null is acceptable as an owner
    ~SpriteList( void );
    SpriteList( SpriteList && other_rref );

public:
    SpriteList& operator=( const SpriteList& other ) = delete;

public:
    Sprite* FindNamed( const std::string& label );
    int Allocated( void );
    void DelAll( void );
    void Add( Sprite* sprite ); //!< preferred method of adding sprites
    void DelAt( const uint index );
    void Del( const Sprite* sprite );
    void DelNulls( void );

    void SetVisible( const bool whether );

    void DrawVBO( void ) const;
    void UpdateVBO( void );

public:
    std::vector< Sprite* > list;
    Sprite* owner;
};

#endif  // SRC_SPRITES_SPRITELIST_H_
