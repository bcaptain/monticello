// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_FILESYSTEM_H_
#define SRC_FILESYSTEM_H_

#include "./base/main.h"

extern const std::string DataDir;

class AssetManifest;

void TruncateDirFromPath( int count, std::string& out ); //!< all directories are expected to have a trailing '/'
void TruncateFileFromPath( std::string& out ); //!< all directories are expected to have a trailing '/'
void GetContainingFolder( const char* path, std::string& out ); //!< all directories are expected to have a trailing '/'

bool FileExt_IsSoundFile( const std::string& filename ); //!< whether extention is a supported sound file

void ExtractPackagePath( const char * path, std::string& out ); //!< attempts to extract which package directory the path is in

void OverrideWarning( const char* asset_type, const std::string& asset_name, const std::string& asset_path, const char* asset_container );
void OverrideWarning( const char* asset_type, const std::string& asset_name, File& opened_asset_file );

void WontOverrideWarning( const char* asset_type, const std::string& asset_name, const std::string& asset_path, const char* asset_container );
void WontOverrideWarning( const char* asset_type, const std::string& asset_name, File& opened_asset_file );

//! open an asset file by name, based on it's entry in the provided manifest, from either filesystem or an archive. returns false if file did not open.
bool OpenAssetFile( const char* asset_type, const char* asset_name, const HashList< AssetManifest >& manifest, File& asset_file_out );
bool OpenAssetFile_NoErr( const char* asset_type, const char* asset_name, const HashList< AssetManifest >& manifest, File& asset_file_out ); //!< will not err if file doesn't exit.
bool OpenAssetFile( const char* asset_type, const AssetManifest& data, File& asset_file_out );

#endif // SRC_FILESYSTEM_H_
