// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_TYPES_TYPES_H_
#define SRC_TYPES_TYPES_H_

#include "./clib/src/warnings.h"
#include "./clib/src/macros.h"
#include "./clib/src/types.h"
#include "./clib/src/std.h"
#include "./clib/src/strings.h"
#include "./clib/src/message.h"

//! Meters
constexpr unsigned long long int operator "" _m ( unsigned long long int meters ) {
    return meters;
}

//! Meters (Floating Point)
constexpr float operator "" _mf ( unsigned long long int meters ) {
    return meters;
}

//! Meters (Floating Point)
constexpr float operator "" _mf ( long double meters ) {
    return static_cast<float>(meters);
}

typedef void(*FuncThatTakesStringVector)( const std::vector< std::string >& );

constexpr const std::size_t ENTVAL_DICT_SIZE = 127;

typedef std::pair< const char *const, const std::size_t > EntVal;

typedef unsigned int TypeID;

#define PrecomputeEntVal( key ) \
    constexpr const std::pair< const char *const, const std::size_t > entval_##key( #key, Hashes::OAT_CONSTEXPR( #key, ENTVAL_DICT_SIZE ) );
    
template< class TYPE >
constexpr TypeID get_typeid( void );

template< class TYPE >
constexpr const char* get_classname( void );

#define GenerateType( _type ) \
    class _type; \
    constexpr const char* TypeName_##_type( #_type ); \
    constexpr const TypeID TypeID_##_type( __COUNTER__ ); \
    template<> constexpr TypeID get_typeid< _type >( void ) { return TypeID_##_type; } \
    template<> constexpr const char* get_classname< _type >( void ) { return #_type; } \

#define UnusedTypeID static_assert(__COUNTER__); // increments counter since the given index is unused

GenerateType( Entity )
GenerateType( Actor )
UnusedTypeID
UnusedTypeID
UnusedTypeID
GenerateType( Emitter )
GenerateType( Module )
GenerateType( Flat )
GenerateType( PickupItem )
GenerateType( Projectile )
GenerateType( Trigger )
GenerateType( Trigger_Door )
GenerateType( Trigger_Mover )
GenerateType( Trigger_SafeZone )
GenerateType( Trigger_Teleporter )
UnusedTypeID
GenerateType( Waypoint )
GenerateType( Trigger_NextMap )
UnusedTypeID
UnusedTypeID
UnusedTypeID
UnusedTypeID
UnusedTypeID
UnusedTypeID
UnusedTypeID
GenerateType( Mine )
UnusedTypeID
GenerateType( Gasbomb )
GenerateType( UIInteractor )
GenerateType( Grenade )
GenerateType( SpawnBox )
UnusedTypeID
GenerateType( Light )
GenerateType( DirLight )
GenerateType( PointLight )
GenerateType( Shard )
GenerateType( SweepDebug )
UnusedTypeID
UnusedTypeID
GenerateType( Projector )
UnusedTypeID
GenerateType( ClipDebug )
UnusedTypeID
UnusedTypeID
GenerateType( Eugene )
UnusedTypeID
GenerateType( BasicBug )
GenerateType( BugFood )
GenerateType( BugNest )
GenerateType( GarbageBin )
GenerateType( BasicBugSign )
GenerateType( Sofa )
GenerateType( StompEffect )
GenerateType( PoisonGrenade )
GenerateType( GasTurret )
GenerateType( Moth )
GenerateType( Beetle )
GenerateType( Pedestal )
GenerateType( Bedbug )
GenerateType( SleepDust )
GenerateType( Valuable )
UnusedTypeID
GenerateType( Forager )
constexpr const uint NUM_ENTITY_TYPES = __COUNTER__;
constexpr const TypeID TypeID_None = std::numeric_limits<TypeID>::max();

//Note: Do not change the values of pre-existing types as they will may have references in saved maps

class Entity;
void AddClassToolTip( const TypeID type_id, const char* key, const char* val );
void InitEntityTypes( void );
bool is_valid_classname( const std::string& type_name );

typedef uint (*GetSetterIndex_MethodPtr)( const std::string& );
extern std::vector< GetSetterIndex_MethodPtr > ALL_GET_SETTER_INDEX_PTRS;

TypeID get_typeid( const std::string& type_name );
std::string get_classname( const TypeID type_id );

constexpr TypeID get_parent_typeid( const TypeID type_id );

template< class TYPE_DERIVED, class TYPE_BASE >
struct TypeInfo {
    static constexpr bool derives_from( const TypeID derived ) {
        if ( derived == ::get_typeid<TYPE_DERIVED>() )
            return true;
            
        if ( std::is_same<TYPE_DERIVED,TYPE_BASE>() )
            return false;
            
        return TYPE_BASE::typeinfo.derives_from(derived);
    }
    static constexpr bool is_child_of( [[maybe_unused]] const TypeID id ) {
        return ( (id != ::get_typeid<TYPE_DERIVED>()) && (id == ::get_typeid<TYPE_BASE>() ) );
    }
    static constexpr bool is_parent_of( const TypeID child ) {
        return ( child != ::get_parent_typeid(child) ) && (::get_parent_typeid(child) == ::get_typeid<TYPE_DERIVED>());
    }
    static constexpr bool is_base_of( const TypeID derived ) {
        if ( derived == ::get_typeid<TYPE_DERIVED>() )
            return true;
        const TypeID parent_id = ::get_parent_typeid(derived);
        return ( derived != parent_id ) && ( is_base_of( parent_id ) );
    }
    template< class CHILD > static bool is_parent_of( const CHILD& child ) {
        return is_parent_of(child.GetObjectTypeID());
    }
    template< class PARENT > static bool is_child_of( const PARENT& parent ) {
        return is_child_of(parent.GetObjectTypeID());
    }
    template< class DERIVED > static bool is_base_of( const DERIVED& derived ) {
        return is_base_of(derived.GetObjectTypeID());
    }
    template< class BASE > static bool derives_from( const BASE& base ) {
        return derives_from(base.GetObjectTypeID());
    }

    template< class CHILD > constexpr static bool is_parent_of( void ) {
        return is_parent_of( ::get_typeid<CHILD>() );
    }
    template< class PARENT > constexpr static bool is_child_of( void ) {
        return (!std::is_same<TYPE_DERIVED,PARENT>()) && std::is_same<PARENT,TYPE_BASE>();
    }
    template< class DERIVED > constexpr static bool is_base_of( void ) {
        return is_base_of( ::get_typeid<DERIVED>() );
        //todo:consider: return std::is_base_of<TYPE_DERIVED, DERIVED>();
    }
    template< class BASE > constexpr static bool derives_from( void ) {
        //todo: C++20 will have: return std::DerivedFrom<BASE, DERIVED>();
        //todo:consider: return std::is_base_of<BASE, TYPE_DERIVED>();
        return derives_from( ::get_typeid<BASE>() );
    }
    constexpr static const char* get_classname( void ) {
        return ::get_classname< TYPE_DERIVED >();
    }
    constexpr static TypeID get_typeid( void ) {
        return ::get_typeid<TYPE_DERIVED>();
    }
    constexpr static TypeID get_parent_typeid( void ) {
        return ::get_typeid<TYPE_BASE>();
    }
    constexpr static TYPE_BASE* get_base_type( void ) {
        return static_cast<TYPE_BASE*>(nullptr);
    }
};

constexpr TypeID get_parent_typeid( const TypeID type_id ) {
    switch ( type_id ) {
        case TypeID_Entity: return TypeID_Entity;
        case TypeID_Actor: return TypeID_Entity;
        case TypeID_Emitter: return TypeID_Entity;
        case TypeID_Projectile: return TypeID_Entity;
        case TypeID_Valuable: return TypeID_Entity;
        case TypeID_PickupItem: return TypeID_Entity;
        case TypeID_Module: return TypeID_Entity;
        case TypeID_Trigger: return TypeID_Entity;
        case TypeID_Trigger_Mover: return TypeID_Trigger;
        case TypeID_SpawnBox: return TypeID_Entity;
        case TypeID_Trigger_Teleporter: return TypeID_Trigger;
        case TypeID_Flat: return TypeID_Module;
        case TypeID_Mine: return TypeID_Entity;
        case TypeID_Trigger_SafeZone: return TypeID_Trigger;
        case TypeID_Trigger_NextMap: return TypeID_Trigger;
        case TypeID_Trigger_Door: return TypeID_Trigger;
        case TypeID_Waypoint: return TypeID_Entity;
        case TypeID_Gasbomb: return TypeID_Projectile;
        case TypeID_Grenade: return TypeID_Projectile;
        case TypeID_UIInteractor: return TypeID_Entity;
        case TypeID_Light: return TypeID_Entity;
        case TypeID_DirLight: return TypeID_Light;
        case TypeID_PointLight: return TypeID_Light;
        case TypeID_Shard: return TypeID_Flat;
        case TypeID_SweepDebug: return TypeID_Entity;
        case TypeID_Projector: return TypeID_Entity;
        case TypeID_Eugene: return TypeID_Actor;
        case TypeID_BasicBug: return TypeID_Actor;
        case TypeID_Forager: return TypeID_BasicBug;
        case TypeID_Moth: return TypeID_BasicBug;
        case TypeID_Beetle: return TypeID_BasicBug;
        case TypeID_Bedbug: return TypeID_BasicBug;
        case TypeID_BugFood: return TypeID_Entity;
        case TypeID_BugNest: return TypeID_Entity;
        case TypeID_Pedestal: return TypeID_Actor;
        case TypeID_PoisonGrenade: return TypeID_Projectile;
        case TypeID_GasTurret: return TypeID_Actor;
        case TypeID_SleepDust: return TypeID_Projectile;
        case TypeID_GarbageBin: return TypeID_Actor;
        case TypeID_Sofa: return TypeID_Actor;
        case TypeID_BasicBugSign: return TypeID_Actor;
        case TypeID_StompEffect: return TypeID_Entity;
        case TypeID_ClipDebug: return TypeID_Entity;
        default:
            DIE("get_parent_typeid: Unknown type ID: %i\n", type_id );
            return TypeID_None; // if user wishes to not abort the program
    }
}

constexpr bool derives_from( const TypeID base, const TypeID derived ) {
    if ( base == derived )
        return true;
    const TypeID parent = ::get_parent_typeid(derived);
    if ( parent == derived )
        return false;
    return derives_from( base, parent );
}

template< class DERIVED >
constexpr bool derives_from( const TypeID base, const DERIVED& derived ) {
    return derives_from( base, derived.GetObjectTypeID() );
}

template<class BASE >
constexpr bool derives_from( const TypeID derived ) {
    return BASE::typeinfo.is_base_of(derived);
}

template<class BASE, class DERIVED >
constexpr bool derives_from( const DERIVED& derived ) {
    return BASE::typeinfo.is_base_of( derived.GetObjectTypeID() );
}

template<class BASE, class DERIVED >
constexpr bool derives_from( void ) {
    //todo: C++20 will have: return std::DerivedFrom<BASE, DERIVED>();
    //todo:consider: return std::is_base_of<BASE, DERIVED>();
    return DERIVED::typeinfo.template derives_from< BASE >();
}

template<class PARENT >
constexpr bool is_child_of( const TypeID child ) {
    return PARENT::typeinfo.is_parent_of(child);
}

template<class PARENT, class CHILD >
constexpr bool is_child_of( const CHILD& child ) {
    return PARENT::typeinfo.is_parent_of( child.GetObjectTypeID() );
}

template<class PARENT, class CHILD >
constexpr bool is_child_of( void ) {
    return CHILD::typeinfo.template is_child_of< PARENT >();
}

constexpr bool is_child_of( const TypeID parent, const TypeID child ) {
    if ( child == parent )
        return false;
    return parent == ::get_parent_typeid(child);
}

template< class DERIVED >
constexpr bool is_child_of( const TypeID parent, const DERIVED& child ) {
    return is_child_of( parent, child.GetObjectTypeID() );
}

template<class DERIVED >
constexpr bool is_base_of( const TypeID base ) {
    return DERIVED::typeinfo.derives_from(base);
}

template<class DERIVED, class BASE >
constexpr bool is_base_of( const BASE& base ) {
    return DERIVED::typeinfo.derives_from( base.GetObjectTypeID() );
}

template<class DERIVED, class BASE >
constexpr bool is_base_of( void ) {
    //todo:consider: return std::is_base_of<BASE, DERIVED>();
    return BASE::typeinfo.template is_base_of< DERIVED >();
}

constexpr bool is_base_of( const TypeID derived, const TypeID base ) {
    return derives_from( base, derived );
}

template< class DERIVED >
constexpr bool is_base_of( const TypeID derived, const DERIVED& base ) {
    return derives_from( base.GetObjectTypeID(), derived );
}

template<class CHILD >
constexpr bool is_parent_of( const TypeID parent ) {
    return CHILD::typeinfo.is_child_of(parent);
}

template<class CHILD, class PARENT >
constexpr bool is_parent_of( const PARENT& parent ) {
    return CHILD::typeinfo.is_child_of( parent.GetObjectTypeID() );
}

template<class PARENT, class CHILD >
constexpr bool is_parent_of( void ) {
    return PARENT::typeinfo.template is_parent_of< CHILD >();
}

constexpr bool is_parent_of( const TypeID child, const TypeID parent ) {
    return is_child_of( parent, child );
}

template< class DERIVED >
constexpr bool is_parent_of( const TypeID child, const DERIVED& parent ) {
    return is_child_of( parent.GetObjectTypeID(), child );
}

template< class TYPE >
std::string get_classname( const TYPE& object ) {
    return get_classname(object.GetObjectTypeID());
}
    
#include "./types_unittest.h"

#endif  // SRC_TYPES_TYPES_H_
