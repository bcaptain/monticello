// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_TYPES_TYPES_UNITTEST_H_
#define SRC_TYPES_TYPES_UNITTEST_H_

#include "../clib/src/warnings.h"

void Types_UnitTest( void );

#endif  // SRC_TYPES_TYPES_UNITTEST
