// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./types.h"
#include "../game.h"
#include "../entities/entity.h"
#include "../entities/waypoint.h"
#include "../entities/triggers/trigger.h"
#include "../entities/triggers/trigger_mover.h"
#include "../entities/triggers/trigger_teleporter.h"
#include "../entities/triggers/trigger_safezone.h"
#include "../entities/triggers/trigger_door.h"
#include "../entities/triggers/trigger_nextmap.h"
#include "../entities/actor.h"
#include "../entities/hazards/mine.h"
#include "../entities/effects/stompEffect.h"
#include "../entities/characters/eugene.h"
#include "../entities/characters/basicBug.h"
#include "../entities/characters/forager.h"
#include "../entities/characters/moth.h"
#include "../entities/characters/beetle.h"
#include "../entities/characters/bedbug.h"
#include "../entities/pedestal.h"
#include "../entities/bugFood.h"
#include "../entities/bugNest.h"
#include "../entities/garbageBin.h"
#include "../entities/sofa.h"
#include "../entities/basicBugSign.h"
#include "../entities/projector.h"
#include "../entities/modules/module.h"
#include "../entities/modules/flat.h"
#include "../entities/modules/uiInteractor.h"
#include "../entities/emitter.h"
#include "../entities/light/allLights.h"
#include "../entities/projectile.h"
#include "../entities/projectiles/gasBomb.h"
#include "../entities/projectiles/grenade.h"
#include "../entities/projectiles/poisonGrenade.h"
#include "../entities/projectiles/sleepdust.h"
#include "../entities/gasTurret.h"
#include "../entities/pickupitem.h"
#include "../entities/valuable.h"
#include "../entities/spawnBox.h"
#include "../entities/debug/sweepDebug.h"
#include "../entities/debug/clipDebug.h"
#include "../ui/window.h"
#include "../sprites/spriteList.h"

typedef void (*AddClassToolTip_MethodPtr)( const char*, const char* );
std::vector< AddClassToolTip_MethodPtr > ALL_ADD_TOOLTIP_PTRS;

AddClassToolTip_MethodPtr Get_AddClassToolTip_MethodPtr( const TypeID type_id );
AddClassToolTip_MethodPtr Get_AddClassToolTip_MethodPtr( const TypeID type_id ) {
    if ( type_id < ALL_ADD_TOOLTIP_PTRS.size() )
        return ALL_ADD_TOOLTIP_PTRS[type_id];

    ERR("Get_AddClassToolTip_MethodPtr: Unknown type ID: %i\n", type_id );
    return nullptr;
}

std::vector< GetSetterIndex_MethodPtr > ALL_GET_SETTER_INDEX_PTRS;

std::vector< std::string > ALL_TYPES;

bool is_valid_classname( const std::string& type_name ) {
    for ( uint idx=0; idx<ALL_TYPES.size(); ++idx )
        if ( type_name == ALL_TYPES[idx] )
            return true;

    return false;
}

TypeID get_typeid( const std::string& type_name ) {
    for ( uint idx=0; idx<ALL_TYPES.size(); ++idx )
        if ( type_name == ALL_TYPES[idx] )
            return idx;

    DIE("get_typeid: Unknown type: %s\n", type_name.c_str());
    return TypeID_None; // if user wishes to not abort the program
}

std::string get_classname( const TypeID type_id ) {
    if ( type_id < ALL_TYPES.size() )
        return ALL_TYPES[type_id];
    ERR("get_classname: Unknown type ID: %i\n", type_id );
    return nullptr;
}

void AddClassToolTip( const TypeID type_id, const char* key, const char* val ) {
    AddClassToolTip_MethodPtr addToolTip = Get_AddClassToolTip_MethodPtr( type_id );
    if ( !addToolTip ) {
        ERR("AddClassToolTip: No such class id (%u) when adding key \"%s\" with val \"%s\"\n", type_id, key, val );
        return;
    }
    (*addToolTip)( key, val );
}

void InitEntityTypes( void ) {
    ASSERT( ALL_TYPES.size() < 1 );
    
    ALL_TYPES.resize( NUM_ENTITY_TYPES );
    ALL_ADD_TOOLTIP_PTRS.resize( NUM_ENTITY_TYPES, nullptr );

    #define INIT_ENTITY_SUBCLASS( _type ) \
        SetAt( ALL_TYPES, TypeID_##_type, TypeName_##_type ); \
        SetAt( ALL_ADD_TOOLTIP_PTRS, TypeID_##_type, &_type::editor_tooltips.Add ); \
        SetAt( ALL_GET_SETTER_INDEX_PTRS, TypeID_##_type, &_type::GetSetterIndex ); \
        _type::InitSetterNames()

    INIT_ENTITY_SUBCLASS( Entity );
    INIT_ENTITY_SUBCLASS( Actor );
    INIT_ENTITY_SUBCLASS( Emitter );
    INIT_ENTITY_SUBCLASS( Flat );
    INIT_ENTITY_SUBCLASS( Module );
    INIT_ENTITY_SUBCLASS( PickupItem );
    INIT_ENTITY_SUBCLASS( Valuable );
    INIT_ENTITY_SUBCLASS( Projectile );
    INIT_ENTITY_SUBCLASS( Trigger );
    INIT_ENTITY_SUBCLASS( Trigger_Door );
    INIT_ENTITY_SUBCLASS( Trigger_Mover );
    INIT_ENTITY_SUBCLASS( Trigger_SafeZone );
    INIT_ENTITY_SUBCLASS( Trigger_Teleporter );
    INIT_ENTITY_SUBCLASS( Waypoint );
    INIT_ENTITY_SUBCLASS( Trigger_NextMap );
    INIT_ENTITY_SUBCLASS( Mine );
    INIT_ENTITY_SUBCLASS( Gasbomb );
    INIT_ENTITY_SUBCLASS( UIInteractor );
    INIT_ENTITY_SUBCLASS( Grenade );
    INIT_ENTITY_SUBCLASS( SpawnBox );
    INIT_ENTITY_SUBCLASS( Light );
    INIT_ENTITY_SUBCLASS( DirLight );
    INIT_ENTITY_SUBCLASS( PointLight );
    INIT_ENTITY_SUBCLASS( Shard );
    INIT_ENTITY_SUBCLASS( SweepDebug );
    INIT_ENTITY_SUBCLASS( Projector );
    INIT_ENTITY_SUBCLASS( Eugene );
    INIT_ENTITY_SUBCLASS( BasicBug );
    INIT_ENTITY_SUBCLASS( Moth );
    INIT_ENTITY_SUBCLASS( Beetle );
    INIT_ENTITY_SUBCLASS( Bedbug );
    INIT_ENTITY_SUBCLASS( BugFood );
    INIT_ENTITY_SUBCLASS( BugNest );
    INIT_ENTITY_SUBCLASS( SleepDust );
    INIT_ENTITY_SUBCLASS( ClipDebug );
    INIT_ENTITY_SUBCLASS( GarbageBin );
    INIT_ENTITY_SUBCLASS( BasicBugSign );
    INIT_ENTITY_SUBCLASS( StompEffect );
    INIT_ENTITY_SUBCLASS( PoisonGrenade );
    INIT_ENTITY_SUBCLASS( GasTurret );
    INIT_ENTITY_SUBCLASS( Pedestal );
    INIT_ENTITY_SUBCLASS( Valuable );
    INIT_ENTITY_SUBCLASS( Sofa );
    INIT_ENTITY_SUBCLASS( Forager );
    
    ASSERT( std::string( TypeName_Shard ) == ALL_TYPES[TypeID_Shard] );
}

std::shared_ptr< Entity > Game::NewEntityFromID( const TypeID type_id ) {
    switch ( type_id ) {
        case TypeID_Entity: return NewEnt_Helper< Entity >();
        case TypeID_Actor: return NewEnt_Helper< Actor >();
        case TypeID_Emitter: return NewEnt_Helper< Emitter >();
        case TypeID_Projectile: return NewEnt_Helper< Projectile >();
        case TypeID_Valuable: return NewEnt_Helper< Valuable >();
        case TypeID_PickupItem: return NewEnt_Helper< PickupItem >();
        case TypeID_Module: return NewEnt_Helper< Module >();
        case TypeID_Trigger: return NewEnt_Helper< Trigger >();
        case TypeID_Trigger_Mover: return NewEnt_Helper< Trigger_Mover >();
        case TypeID_SpawnBox: return NewEnt_Helper< SpawnBox >();
        case TypeID_Trigger_Teleporter: return NewEnt_Helper< Trigger_Teleporter >();
        case TypeID_Flat: return NewEnt_Helper< Flat >();
        case TypeID_Mine: return NewEnt_Helper< Mine >();
        case TypeID_Trigger_SafeZone: return NewEnt_Helper< Trigger_SafeZone >();
        case TypeID_Trigger_NextMap: return NewEnt_Helper< Trigger_NextMap >();
        case TypeID_Trigger_Door: return NewEnt_Helper< Trigger_Door >();
        case TypeID_Waypoint: return NewEnt_Helper< Waypoint >();
        case TypeID_Gasbomb: return NewEnt_Helper< Gasbomb >();
        case TypeID_Grenade: return NewEnt_Helper< Grenade >();
        case TypeID_UIInteractor: return NewEnt_Helper< UIInteractor >();
        case TypeID_Light: return NewEnt_Helper< Light >();
        case TypeID_DirLight: return NewEnt_Helper< DirLight >();
        case TypeID_PointLight: return NewEnt_Helper< PointLight >();
        case TypeID_Shard: return NewEnt_Helper< Shard >();
        case TypeID_SweepDebug: return NewEnt_Helper< SweepDebug >();
        case TypeID_Projector: return NewEnt_Helper< Projector >();
        case TypeID_Eugene: return NewEnt_Helper< Eugene >();
        case TypeID_BasicBug: return NewEnt_Helper< BasicBug >();
        case TypeID_Forager: return NewEnt_Helper< Forager >();
        case TypeID_Moth: return NewEnt_Helper< Moth >();
        case TypeID_Beetle: return NewEnt_Helper< Beetle >();
        case TypeID_Bedbug: return NewEnt_Helper< Bedbug >();
        case TypeID_BugFood: return NewEnt_Helper< BugFood >();
        case TypeID_BugNest: return NewEnt_Helper< BugNest >();
        case TypeID_Pedestal: return NewEnt_Helper< Pedestal >();
        case TypeID_PoisonGrenade: return NewEnt_Helper< PoisonGrenade >();
        case TypeID_GasTurret: return NewEnt_Helper< GasTurret >();
        case TypeID_SleepDust: return NewEnt_Helper< SleepDust >();
        case TypeID_GarbageBin: return NewEnt_Helper< GarbageBin >();
        case TypeID_Sofa: return NewEnt_Helper< Sofa >();
        case TypeID_BasicBugSign: return NewEnt_Helper< BasicBugSign >();
        case TypeID_StompEffect: return NewEnt_Helper< StompEffect >();
        case TypeID_ClipDebug: return NewEnt_Helper< ClipDebug >();
        default:
            DIE("NewEntityFromID: Unknown type ID: %i\n", type_id );
            return {}; // if user wishes to not abort the program
    }
}
