// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef CLIB_UNIT_TEST
#include "../base/main.h"
#include "./types_unittest.h"
#include "./types.h"
#include "../entities/entity.h"
#include "../entities/light/light.h"
#include "../entities/projectile.h"
#include "../entities/projectiles/grenade.h"
#include "../entities/projectiles/sleepdust.h"

void Types_UnitTest( void ) {
    Projectile p;
    Grenade g;
    Entity e;
    SleepDust s;
        
// **** get_typeid

    // ** ID & Class - dynamic
    ASSERT( get_typeid< Entity >() == TypeID_Entity );
    ASSERT( get_typeid< Grenade >() == TypeID_Grenade );
    ASSERT( get_typeid< Projectile >() == TypeID_Projectile );

//todomaybe    ASSERT( ( Entity::typeinfo.get_typeid() == TypeID_Entity ) );
//todomaybe    ASSERT( ( Grenade::typeinfo.get_typeid() == TypeID_Grenade ) );
//todomaybe    ASSERT( ( Projectile::typeinfo.get_typeid() == TypeID_Projectile ) );

    // ** ID & Class - static
    static_assert( get_typeid< Entity >() == TypeID_Entity );
    static_assert( get_typeid< Grenade >() == TypeID_Grenade );
    static_assert( get_typeid< Projectile >() == TypeID_Projectile );
    
//todomaybe    static_assert( ( Entity::typeinfo.get_typeid() == TypeID_Entity ) );
//todomaybe    static_assert( ( Grenade::typeinfo.get_typeid() == TypeID_Grenade ) );
//todomaybe    static_assert( ( Projectile::typeinfo.get_typeid() == TypeID_Projectile ) );

    // ** Class & Object - dynamic only
    ASSERT( p.GetObjectTypeID() == get_typeid< Projectile >() );
    ASSERT( e.GetObjectTypeID() == get_typeid< Entity >() );
    ASSERT( g.GetObjectTypeID() == get_typeid< Grenade >() );

// **** get_parent_typeid
    
    // ** ID & Class - dynamic
    ASSERT( Entity::typeinfo.get_parent_typeid() == TypeID_Entity );
    ASSERT( Projectile::typeinfo.get_parent_typeid() == TypeID_Entity );
    ASSERT( Grenade::typeinfo.get_parent_typeid() == TypeID_Projectile );

    ASSERT( get_parent_typeid( get_typeid<Entity>() ) == TypeID_Entity );
    ASSERT( get_parent_typeid( get_typeid<Projectile>() ) == TypeID_Entity );
    ASSERT( get_parent_typeid( get_typeid<Grenade>() ) == TypeID_Projectile );
    
    // ** ID & Class - static
    static_assert( Entity::typeinfo.get_parent_typeid() == TypeID_Entity );
    static_assert( Projectile::typeinfo.get_parent_typeid() == TypeID_Entity );
    static_assert( Grenade::typeinfo.get_parent_typeid() == TypeID_Projectile );

    static_assert( get_parent_typeid( get_typeid<Entity>() ) == TypeID_Entity );
    static_assert( get_parent_typeid( get_typeid<Projectile>() ) == TypeID_Entity );
    static_assert( get_parent_typeid( get_typeid<Grenade>() ) == TypeID_Projectile );
    
// **** derives_from

    // ** Class to Class - static
    static_assert( !derives_from< Projectile, Entity >() );
    static_assert( derives_from< Entity, Entity >() );
    static_assert( !derives_from< Grenade, Entity >() );
    static_assert( derives_from< Projectile, Projectile >() );
    static_assert( derives_from< Entity, Projectile >() );
    static_assert( !derives_from< Grenade, Projectile >() );
    static_assert( derives_from< Projectile, Grenade >() );
    static_assert( derives_from< Entity, Grenade >() );
    static_assert( derives_from< Grenade, Grenade >() );
    
    static_assert( Entity::typeinfo.derives_from< Entity >() );      
    static_assert( Projectile::typeinfo.derives_from< Projectile >() );      
    static_assert( Projectile::typeinfo.derives_from< Entity >() );
    static_assert( Grenade::typeinfo.derives_from< Entity >() );
    static_assert( !Entity::typeinfo.derives_from< Projectile >() );

    // ** Class to Class - dynamic
    ASSERT( ! ( derives_from< Projectile, Entity >() ) );
    ASSERT(  ( derives_from< Entity, Entity >() ) );
    ASSERT( ! ( derives_from< Grenade, Entity >() ) );
    ASSERT(  ( derives_from< Projectile, Projectile >() ) );
    ASSERT(  ( derives_from< Entity, Projectile >() ) );
    ASSERT( ! ( derives_from< Grenade, Projectile >() ) );
    ASSERT(  ( derives_from< Projectile, Grenade >() ) );
    ASSERT(  ( derives_from< Entity, Grenade >() ) );
    ASSERT(  ( derives_from< Grenade, Grenade >() ) );
    
    ASSERT( Entity::typeinfo.derives_from< Entity >() );      
    ASSERT( Projectile::typeinfo.derives_from< Projectile >() );      
    ASSERT( Projectile::typeinfo.derives_from< Entity >() );
    ASSERT( Grenade::typeinfo.derives_from< Entity >() );
    ASSERT( !Entity::typeinfo.derives_from< Projectile >() );
    
    // ** ID to Class - dynamic
    ASSERT( ( derives_from<Entity>( TypeID_Entity ) ) );
    ASSERT( ( derives_from<Projectile>( TypeID_Projectile ) ) );
    ASSERT( ( derives_from<Entity>( TypeID_Projectile ) ) );
    ASSERT( ( derives_from<Entity>( TypeID_Grenade ) ) );
    ASSERT( ( derives_from<Projectile>( TypeID_Grenade ) ) );
    ASSERT( ( !derives_from<Projectile>( TypeID_Entity ) ) );
    ASSERT( ( !derives_from<Grenade>( TypeID_Entity ) ) );
    ASSERT( ( !derives_from<Grenade>( TypeID_Projectile ) ) );
    
    ASSERT( !( Entity::typeinfo.derives_from( TypeID_Projectile ) ) );      
    ASSERT( ( Entity::typeinfo.derives_from( TypeID_Entity ) ) );      
    ASSERT( !( Entity::typeinfo.derives_from( TypeID_Grenade ) ) );      
    ASSERT( ( Projectile::typeinfo.derives_from( TypeID_Projectile ) ) );      
    ASSERT( ( Projectile::typeinfo.derives_from( TypeID_Entity ) ) );      
    ASSERT( !( Projectile::typeinfo.derives_from( TypeID_Grenade ) ) );
    ASSERT( ( Grenade::typeinfo.derives_from( TypeID_Projectile ) ) );
    ASSERT( ( Grenade::typeinfo.derives_from( TypeID_Entity ) ) );
    ASSERT( ( Grenade::typeinfo.derives_from( TypeID_Grenade ) ) );

    // ** ID to Class - static
    static_assert( !( Entity::typeinfo.derives_from( TypeID_Projectile ) ) );      
    static_assert( ( Entity::typeinfo.derives_from( TypeID_Entity ) ) );      
    static_assert( !( Entity::typeinfo.derives_from( TypeID_Grenade ) ) );      
    static_assert( ( Projectile::typeinfo.derives_from( TypeID_Projectile ) ) );      
    static_assert( ( Projectile::typeinfo.derives_from( TypeID_Entity ) ) );      
    static_assert( !( Projectile::typeinfo.derives_from( TypeID_Grenade ) ) );
    static_assert( ( Grenade::typeinfo.derives_from( TypeID_Projectile ) ) );
    static_assert( ( Grenade::typeinfo.derives_from( TypeID_Entity ) ) );
    static_assert( ( Grenade::typeinfo.derives_from( TypeID_Grenade ) ) );

    static_assert( ( derives_from<Entity>( TypeID_Entity ) ) );
    static_assert( ( derives_from<Projectile>( TypeID_Projectile ) ) );
    static_assert( ( derives_from<Entity>( TypeID_Projectile ) ) );
    static_assert( ( derives_from<Entity>( TypeID_Grenade ) ) );
    static_assert( ( derives_from<Projectile>( TypeID_Grenade ) ) );
    static_assert( ( !derives_from<Projectile>( TypeID_Entity ) ) );
    static_assert( ( !derives_from<Grenade>( TypeID_Entity ) ) );
    static_assert( ( !derives_from<Grenade>( TypeID_Projectile ) ) );

    // ** Object to Class - dynamic only
    ASSERT( derives_from< Entity >( p ) );
    ASSERT( derives_from< Entity >( e ) );
    ASSERT( derives_from< Entity >( g ) );
    
    ASSERT( derives_from< Projectile >( p ) );
    ASSERT( !derives_from< Projectile >( e ) );
    ASSERT( derives_from< Projectile >( g ) );
    
    ASSERT( !derives_from< Grenade >( p ) );
    ASSERT( !derives_from< Grenade >( e ) );
    ASSERT( derives_from< Grenade >( g ) );

    ASSERT( Projectile::typeinfo.derives_from( p ) );
    ASSERT( Projectile::typeinfo.derives_from( e ) );
    ASSERT( !Projectile::typeinfo.derives_from( g ) );
    
    ASSERT( Grenade::typeinfo.derives_from( p ) );
    ASSERT( Grenade::typeinfo.derives_from( e ) );
    ASSERT( Grenade::typeinfo.derives_from( g ) );
    
    ASSERT( !Entity::typeinfo.derives_from( p ) );
    ASSERT( Entity::typeinfo.derives_from( e ) );
    ASSERT( !Entity::typeinfo.derives_from( p ) );
    
    // ** Object to ID - dynamic
    ASSERT( ( derives_from( TypeID_Entity, p ) ) );      
    ASSERT( ( derives_from( TypeID_Entity, e ) ) );      
    ASSERT( ( derives_from( TypeID_Entity, g ) ) );      
    ASSERT( ( derives_from( TypeID_Projectile, p ) ) );      
    ASSERT( ! ( derives_from( TypeID_Projectile, e ) ) );      
    ASSERT( ( derives_from( TypeID_Projectile, g ) ) );
    ASSERT( ! ( derives_from( TypeID_Grenade, p ) ) );
    ASSERT( ! ( derives_from( TypeID_Grenade, e ) ) );
    ASSERT( ( derives_from( TypeID_Grenade, g ) ) );

    // ** ID to ID - static
    static_assert( ( derives_from( TypeID_Entity, TypeID_Projectile ) ) );      
    static_assert( ( derives_from( TypeID_Entity, TypeID_Entity ) ) );      
    static_assert( ( derives_from( TypeID_Entity, TypeID_Grenade ) ) );      
    static_assert( ( derives_from( TypeID_Projectile, TypeID_Projectile ) ) );      
    static_assert( ! ( derives_from( TypeID_Projectile, TypeID_Entity ) ) );      
    static_assert( ( derives_from( TypeID_Projectile, TypeID_Grenade ) ) );
    static_assert( ! ( derives_from( TypeID_Grenade, TypeID_Projectile ) ) );
    static_assert( ! ( derives_from( TypeID_Grenade, TypeID_Entity ) ) );
    static_assert( ( derives_from( TypeID_Grenade, TypeID_Grenade ) ) );
    
    // ** ID to ID - dynamic
    ASSERT( ( derives_from( TypeID_Entity, TypeID_Projectile ) ) );      
    ASSERT( ( derives_from( TypeID_Entity, TypeID_Entity ) ) );      
    ASSERT( ( derives_from( TypeID_Entity, TypeID_Grenade ) ) );      
    ASSERT( ( derives_from( TypeID_Projectile, TypeID_Projectile ) ) );      
    ASSERT( ! ( derives_from( TypeID_Projectile, TypeID_Entity ) ) );      
    ASSERT( ( derives_from( TypeID_Projectile, TypeID_Grenade ) ) );
    ASSERT( ! ( derives_from( TypeID_Grenade, TypeID_Projectile ) ) );
    ASSERT( ! ( derives_from( TypeID_Grenade, TypeID_Entity ) ) );
    ASSERT( ( derives_from( TypeID_Grenade, TypeID_Grenade ) ) );
    
// **** is_base_of
    // ** Class to Class - static
    static_assert( is_base_of< Entity, Entity >() );      
    static_assert( !is_base_of< Entity, Projectile >() );
    static_assert( !is_base_of< Entity, Grenade >() );
    static_assert( is_base_of< Projectile, Projectile >() );      
    static_assert( is_base_of< Projectile, Entity >() );
    static_assert( !is_base_of< Projectile, Grenade >() );
    static_assert( is_base_of< Grenade, Projectile >() );
    static_assert( is_base_of< Grenade, Entity >() );
    static_assert( is_base_of< Grenade, Grenade >() );
    
    static_assert( Entity::typeinfo.is_base_of< Entity >() );
    static_assert( Projectile::typeinfo.is_base_of< Projectile >() );
    static_assert( Entity::typeinfo.is_base_of< Projectile >() );
    static_assert( Entity::typeinfo.is_base_of< Grenade >() );
    static_assert( Projectile::typeinfo.is_base_of< Grenade >() );
    static_assert( !Projectile::typeinfo.is_base_of< Entity >() );
    static_assert( !Projectile::typeinfo.is_base_of< Entity >() );
    static_assert( !Grenade::typeinfo.is_base_of< Projectile >() );
    
    // ** Class to Class - dynamic
    ASSERT( Entity::typeinfo.is_base_of< Entity >() );
    ASSERT( Projectile::typeinfo.is_base_of< Projectile >() );
    ASSERT( Entity::typeinfo.is_base_of< Projectile >() );
    ASSERT( Entity::typeinfo.is_base_of< Grenade >() );
    ASSERT( Projectile::typeinfo.is_base_of< Grenade >() );
    ASSERT( !Projectile::typeinfo.is_base_of< Entity >() );
    ASSERT( !Projectile::typeinfo.is_base_of< Entity >() );
    ASSERT( !Grenade::typeinfo.is_base_of< Projectile >() );

    ASSERT(  ( is_base_of< Entity, Entity >() ) );      
    ASSERT( ! ( is_base_of< Entity, Projectile >() ) );
    ASSERT( ! ( is_base_of< Entity, Grenade >() ) );
    ASSERT(  ( is_base_of< Projectile, Projectile >() ) );      
    ASSERT(  ( is_base_of< Projectile, Entity >() ) );
    ASSERT( ! ( is_base_of< Projectile, Grenade >() ) );
    ASSERT(  ( is_base_of< Grenade, Projectile >() ) );
    ASSERT(  ( is_base_of< Grenade, Entity >() ) );
    ASSERT(  ( is_base_of< Grenade, Grenade >() ) );
    
    // ** ID to Class - dynamic
    ASSERT( ( Entity::typeinfo.is_base_of( TypeID_Entity ) ) );
    ASSERT( ( Projectile::typeinfo.is_base_of( TypeID_Projectile ) ) );
    ASSERT( ( Entity::typeinfo.is_base_of( TypeID_Projectile ) ) );
    ASSERT( ( Entity::typeinfo.is_base_of( TypeID_Grenade ) ) );
    ASSERT( ( Projectile::typeinfo.is_base_of( TypeID_Grenade ) ) );
    ASSERT( ( !Projectile::typeinfo.is_base_of( TypeID_Entity ) ) );
    ASSERT( ( !Grenade::typeinfo.is_base_of( TypeID_Entity ) ) );
    ASSERT( ( !Grenade::typeinfo.is_base_of( TypeID_Projectile ) ) );

    ASSERT( ( is_base_of<Projectile>( TypeID_Entity ) ) );
    ASSERT( ( is_base_of<Entity>( TypeID_Entity ) ) );
    ASSERT( ( is_base_of<Grenade>( TypeID_Entity ) ) );
    ASSERT( ( is_base_of<Projectile>( TypeID_Projectile ) ) );
    ASSERT( ( !is_base_of<Entity>( TypeID_Projectile ) ) );
    ASSERT( ( is_base_of<Grenade>( TypeID_Projectile ) ) );
    ASSERT( ( !is_base_of<Projectile>( TypeID_Grenade ) ) );
    ASSERT( ( !is_base_of<Entity>( TypeID_Grenade ) ) );
    ASSERT( ( is_base_of<Grenade>( TypeID_Grenade ) ) );
    
    // ** ID to Class - static

    static_assert( ( Entity::typeinfo.is_base_of( TypeID_Entity ) ) );
    static_assert( ( Projectile::typeinfo.is_base_of( TypeID_Projectile ) ) );
    static_assert( ( Entity::typeinfo.is_base_of( TypeID_Projectile ) ) );
    static_assert( ( Entity::typeinfo.is_base_of( TypeID_Grenade ) ) );
    static_assert( ( Projectile::typeinfo.is_base_of( TypeID_Grenade ) ) );
    static_assert( ( !Projectile::typeinfo.is_base_of( TypeID_Entity ) ) );
    static_assert( ( !Grenade::typeinfo.is_base_of( TypeID_Entity ) ) );
    static_assert( ( !Grenade::typeinfo.is_base_of( TypeID_Projectile ) ) );

    static_assert( ( is_base_of<Projectile>( TypeID_Entity ) ) );
    static_assert( ( is_base_of<Entity>( TypeID_Entity ) ) );
    static_assert( ( is_base_of<Grenade>( TypeID_Entity ) ) );
    static_assert( ( is_base_of<Projectile>( TypeID_Projectile ) ) );
    static_assert( ( !is_base_of<Entity>( TypeID_Projectile ) ) );
    static_assert( ( is_base_of<Grenade>( TypeID_Projectile ) ) );
    static_assert( ( !is_base_of<Projectile>( TypeID_Grenade ) ) );
    static_assert( ( !is_base_of<Entity>( TypeID_Grenade ) ) );
    static_assert( ( is_base_of<Grenade>( TypeID_Grenade ) ) );
    
    // ** Object to Class - dynamic only
    ASSERT( is_base_of< Projectile >( p ) );
    ASSERT( is_base_of< Projectile >( e ) );
    ASSERT( !is_base_of< Projectile >( g ) );
    
    ASSERT( is_base_of< Grenade >( p ) );
    ASSERT( is_base_of< Grenade >( e ) );
    ASSERT( is_base_of< Grenade >( g ) );
    
    ASSERT( !is_base_of< Entity >( p ) );
    ASSERT( is_base_of< Entity >( e ) );
    ASSERT( !is_base_of< Entity >( p ) );
    
    ASSERT( Entity::typeinfo.is_base_of( p ) );
    ASSERT( Entity::typeinfo.is_base_of( e ) );
    ASSERT( Entity::typeinfo.is_base_of( g ) );
    
    ASSERT( Projectile::typeinfo.is_base_of( p ) );
    ASSERT( !Projectile::typeinfo.is_base_of( e ) );
    ASSERT( Projectile::typeinfo.is_base_of( g ) );
    
    ASSERT( !Grenade::typeinfo.is_base_of( p ) );
    ASSERT( !Grenade::typeinfo.is_base_of( e ) );
    ASSERT( Grenade::typeinfo.is_base_of( g ) );
    
    // ** Object to ID - dynamic
    ASSERT( ! ( is_base_of( TypeID_Entity, p ) ) );      
    ASSERT( ( is_base_of( TypeID_Entity, e ) ) );      
    ASSERT( ! ( is_base_of( TypeID_Entity, g ) ) );      
    ASSERT( ( is_base_of( TypeID_Projectile, p ) ) );      
    ASSERT( ( is_base_of( TypeID_Projectile, e ) ) );      
    ASSERT( ! ( is_base_of( TypeID_Projectile, g ) ) );
    ASSERT( ( is_base_of( TypeID_Grenade, p ) ) );
    ASSERT( ( is_base_of( TypeID_Grenade, e ) ) );
    ASSERT( ( is_base_of( TypeID_Grenade, g ) ) );

    // ** ID to ID - static
    static_assert( ! ( is_base_of( TypeID_Entity, TypeID_Projectile ) ) );      
    static_assert( ( is_base_of( TypeID_Entity, TypeID_Entity ) ) );      
    static_assert( ! ( is_base_of( TypeID_Entity, TypeID_Grenade ) ) );      
    static_assert( ( is_base_of( TypeID_Projectile, TypeID_Projectile ) ) );      
    static_assert( ( is_base_of( TypeID_Projectile, TypeID_Entity ) ) );      
    static_assert( ! ( is_base_of( TypeID_Projectile, TypeID_Grenade ) ) );
    static_assert( ( is_base_of( TypeID_Grenade, TypeID_Projectile ) ) );
    static_assert( ( is_base_of( TypeID_Grenade, TypeID_Entity ) ) );
    static_assert( ( is_base_of( TypeID_Grenade, TypeID_Grenade ) ) );
    
    // ** ID to ID - dynamic
    ASSERT( ! ( is_base_of( TypeID_Entity, TypeID_Projectile ) ) );      
    ASSERT( ( is_base_of( TypeID_Entity, TypeID_Entity ) ) );      
    ASSERT( ! ( is_base_of( TypeID_Entity, TypeID_Grenade ) ) );      
    ASSERT( ( is_base_of( TypeID_Projectile, TypeID_Projectile ) ) );      
    ASSERT( ( is_base_of( TypeID_Projectile, TypeID_Entity ) ) );      
    ASSERT( ! ( is_base_of( TypeID_Projectile, TypeID_Grenade ) ) );
    ASSERT( ( is_base_of( TypeID_Grenade, TypeID_Projectile ) ) );
    ASSERT( ( is_base_of( TypeID_Grenade, TypeID_Entity ) ) );
    ASSERT( ( is_base_of( TypeID_Grenade, TypeID_Grenade ) ) );

// **** is_parent_of
    // ** Class to Class - static
    static_assert( !is_parent_of< Projectile, Entity >() );
    static_assert( !is_parent_of< Entity, Entity >() );
    static_assert( !is_parent_of< Grenade, Entity >() );
    static_assert( is_parent_of< Projectile, Grenade >() );
    static_assert( !is_parent_of< Entity, Grenade >() );
    static_assert( !is_parent_of< Grenade, Grenade >() );
    static_assert( !is_parent_of< Projectile, Projectile >() );
    static_assert( is_parent_of< Entity, Projectile >() );
    static_assert( !is_parent_of< Grenade, Projectile >() );

    static_assert( Entity::typeinfo.is_parent_of( TypeID_Projectile ) );
    static_assert( Projectile::typeinfo.is_parent_of( TypeID_Grenade ) );
    static_assert( !Entity::typeinfo.is_parent_of( TypeID_Grenade ) );
    static_assert( !Grenade::typeinfo.is_parent_of( TypeID_Projectile ) );
    static_assert( !Grenade::typeinfo.is_parent_of( TypeID_Entity ) );
    static_assert( Entity::typeinfo.is_parent_of( get_typeid< Projectile>() ) );
    static_assert( Projectile::typeinfo.is_parent_of( get_typeid< Grenade >() ) );
    static_assert( !Entity::typeinfo.is_parent_of( get_typeid< Grenade >() ) );
    static_assert( !Grenade::typeinfo.is_parent_of( get_typeid< Projectile >() ) );
    static_assert( !Grenade::typeinfo.is_parent_of( get_typeid< Entity >() ) );

    // ** Class to Class - dynamic
    ASSERT( ! ( is_parent_of< Projectile, Entity >() ) );
    ASSERT( ! ( is_parent_of< Entity, Entity >() ) );
    ASSERT( ! ( is_parent_of< Grenade, Entity >() ) );
    ASSERT(  ( is_parent_of< Projectile, Grenade >() ) );
    ASSERT( ! ( is_parent_of< Entity, Grenade >() ) );
    ASSERT( ! ( is_parent_of< Grenade, Grenade >() ) );
    ASSERT( ! ( is_parent_of< Projectile, Projectile >() ) );
    ASSERT(  ( is_parent_of< Entity, Projectile >() ) );
    ASSERT( ! ( is_parent_of< Grenade, Projectile >() ) );

    ASSERT( ( Entity::typeinfo.is_parent_of( TypeID_Projectile ) ) );
    ASSERT( ( Projectile::typeinfo.is_parent_of( TypeID_Grenade ) ) );
    ASSERT( ( !Entity::typeinfo.is_parent_of( TypeID_Grenade ) ) );
    ASSERT( ( !Grenade::typeinfo.is_parent_of( TypeID_Projectile ) ) );
    ASSERT( ( !Grenade::typeinfo.is_parent_of( TypeID_Entity ) ) );
    ASSERT( ( Entity::typeinfo.is_parent_of( get_typeid< Projectile>() ) ) );
    ASSERT( ( Projectile::typeinfo.is_parent_of( get_typeid< Grenade >() ) ) );
    ASSERT( ( !Entity::typeinfo.is_parent_of( get_typeid< Grenade >() ) ) );
    ASSERT( ( !Grenade::typeinfo.is_parent_of( get_typeid< Projectile >() ) ) );
    ASSERT( ( !Grenade::typeinfo.is_parent_of( get_typeid< Entity >() ) ) );

    // ** ID to Class - dynamic
    ASSERT( !is_parent_of< Entity >( TypeID_Projectile ) );
    ASSERT( !is_parent_of< Entity >( TypeID_Entity ) );
    ASSERT( !is_parent_of< Entity >( TypeID_Grenade ) );
    ASSERT( is_parent_of< Grenade >( TypeID_Projectile ) );
    ASSERT( !is_parent_of< Grenade >( TypeID_Entity ) );
    ASSERT( !is_parent_of< Grenade >( TypeID_Grenade ) );
    ASSERT( !is_parent_of< Projectile >( TypeID_Projectile ) );
    ASSERT( is_parent_of< Projectile >( TypeID_Entity ) );
    ASSERT( !is_parent_of< Projectile >( TypeID_Grenade ) );

    ASSERT( Entity::typeinfo.is_parent_of( TypeID_Projectile ) );
    ASSERT( Projectile::typeinfo.is_parent_of( TypeID_Grenade ) );
    ASSERT( !Entity::typeinfo.is_parent_of( TypeID_Grenade ) );
    ASSERT( !Grenade::typeinfo.is_parent_of( TypeID_Projectile ) );
    ASSERT( !Grenade::typeinfo.is_parent_of( TypeID_Entity ) );
    ASSERT( Entity::typeinfo.is_parent_of( get_typeid< Projectile>() ) );
    ASSERT( Projectile::typeinfo.is_parent_of( get_typeid< Grenade >() ) );
    ASSERT( !Entity::typeinfo.is_parent_of( get_typeid< Grenade >() ) );
    ASSERT( !Grenade::typeinfo.is_parent_of( get_typeid< Projectile >() ) );
    ASSERT( !Grenade::typeinfo.is_parent_of( get_typeid< Entity >() ) );

    // ** ID to Class - static
    static_assert( !is_parent_of< Entity >( TypeID_Projectile ) );
    static_assert( !is_parent_of< Entity >( TypeID_Entity ) );
    static_assert( !is_parent_of< Entity >( TypeID_Grenade ) );
    static_assert( is_parent_of< Grenade >( TypeID_Projectile ) );
    static_assert( !is_parent_of< Grenade >( TypeID_Entity ) );
    static_assert( !is_parent_of< Grenade >( TypeID_Grenade ) );
    static_assert( !is_parent_of< Projectile >( TypeID_Projectile ) );
    static_assert( is_parent_of< Projectile >( TypeID_Entity ) );
    static_assert( !is_parent_of< Projectile >( TypeID_Grenade ) );

    static_assert( Entity::typeinfo.is_parent_of( TypeID_Projectile ) );
    static_assert( Projectile::typeinfo.is_parent_of( TypeID_Grenade ) );
    static_assert( !Entity::typeinfo.is_parent_of( TypeID_Grenade ) );
    static_assert( !Grenade::typeinfo.is_parent_of( TypeID_Projectile ) );
    static_assert( !Grenade::typeinfo.is_parent_of( TypeID_Entity ) );
    static_assert( Entity::typeinfo.is_parent_of( get_typeid< Projectile>() ) );
    static_assert( Projectile::typeinfo.is_parent_of( get_typeid< Grenade >() ) );
    static_assert( !Entity::typeinfo.is_parent_of( get_typeid< Grenade >() ) );
    static_assert( !Grenade::typeinfo.is_parent_of( get_typeid< Projectile >() ) );
    static_assert( !Grenade::typeinfo.is_parent_of( get_typeid< Entity >() ) );

    // ** Object to Class - dynamic only
    ASSERT( !is_parent_of< Entity >(p) );
    ASSERT( !is_parent_of< Entity >(e) );
    ASSERT( !is_parent_of< Entity >(g) );
    
    ASSERT( is_parent_of< Grenade >(p) );
    ASSERT( !is_parent_of< Grenade >(e) );
    ASSERT( !is_parent_of< Grenade >(g) );
    
    ASSERT( !is_parent_of< Projectile >(p) );
    ASSERT( is_parent_of< Projectile >(e) );
    ASSERT( !is_parent_of< Projectile >(g) );

    ASSERT( ( Entity::typeinfo.is_parent_of(p) ) );
    ASSERT( ! ( Entity::typeinfo.is_parent_of(e) ) );
    ASSERT( ! ( Entity::typeinfo.is_parent_of(g) ) );
    
    ASSERT( ! ( Grenade::typeinfo.is_parent_of(p) ) );
    ASSERT( ! ( Grenade::typeinfo.is_parent_of(e) ) );
    ASSERT( ! ( Grenade::typeinfo.is_parent_of(g) ) );
    
    ASSERT( ! ( Projectile::typeinfo.is_parent_of(p) ) );
    ASSERT( ! ( Projectile::typeinfo.is_parent_of(e) ) );
    ASSERT( ( Projectile::typeinfo.is_parent_of(g) ) );
    
    // ** Object to ID - dynamic
    ASSERT( ! ( is_parent_of( TypeID_Entity, p ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Entity, e ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Entity, g ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Projectile, p ) ) );      
    ASSERT( ( is_parent_of( TypeID_Projectile, e ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Projectile, g ) ) );
    ASSERT( ( is_parent_of( TypeID_Grenade, p ) ) );
    ASSERT( ! ( is_parent_of( TypeID_Grenade, e ) ) );
    ASSERT( ! ( is_parent_of( TypeID_Grenade, g ) ) );

    // ** ID to ID - static
    static_assert( ! ( is_parent_of( TypeID_Entity, TypeID_Projectile ) ) );      
    static_assert( ! ( is_parent_of( TypeID_Entity, TypeID_Entity ) ) );      
    static_assert( ! ( is_parent_of( TypeID_Entity, TypeID_Grenade ) ) );      
    static_assert( ! ( is_parent_of( TypeID_Projectile, TypeID_Projectile ) ) );      
    static_assert( ( is_parent_of( TypeID_Projectile, TypeID_Entity ) ) );      
    static_assert( ! ( is_parent_of( TypeID_Projectile, TypeID_Grenade ) ) );
    static_assert( ( is_parent_of( TypeID_Grenade, TypeID_Projectile ) ) );
    static_assert( ! ( is_parent_of( TypeID_Grenade, TypeID_Entity ) ) );
    static_assert( ! ( is_parent_of( TypeID_Grenade, TypeID_Grenade ) ) );
    
    // ** ID to ID - dynamic
    ASSERT( ! ( is_parent_of( TypeID_Entity, TypeID_Projectile ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Entity, TypeID_Entity ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Entity, TypeID_Grenade ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Projectile, TypeID_Projectile ) ) );      
    ASSERT( ( is_parent_of( TypeID_Projectile, TypeID_Entity ) ) );      
    ASSERT( ! ( is_parent_of( TypeID_Projectile, TypeID_Grenade ) ) );
    ASSERT( ( is_parent_of( TypeID_Grenade, TypeID_Projectile ) ) );
    ASSERT( ! ( is_parent_of( TypeID_Grenade, TypeID_Entity ) ) );
    ASSERT( ! ( is_parent_of( TypeID_Grenade, TypeID_Grenade ) ) );

// **** is_child_of
    // ** Class to Class - static
    static_assert( !is_child_of< Projectile, Entity >() );
    static_assert( !is_child_of< Entity, Entity >() );
    static_assert( !is_child_of< Grenade, Entity >() );
    static_assert( !is_child_of< Projectile, Projectile >() );
    static_assert( is_child_of< Entity, Projectile >() );
    static_assert( !is_child_of< Grenade, Projectile >() );
    static_assert( is_child_of< Projectile, Grenade >() );
    static_assert( is_child_of< Projectile, SleepDust >() );
    static_assert( !is_child_of< Entity, Grenade >() );
    static_assert( !is_child_of< Grenade, Grenade >() );

    static_assert( !Entity::typeinfo.is_child_of< Projectile >() );
    static_assert( !Entity::typeinfo.is_child_of< Entity >() );
    static_assert( !Entity::typeinfo.is_child_of< Grenade >() );
    static_assert( Grenade::typeinfo.is_child_of< Projectile >() );
    static_assert( !Grenade::typeinfo.is_child_of< Entity >() );
    static_assert( !Grenade::typeinfo.is_child_of< Grenade >() );
    static_assert( !Projectile::typeinfo.is_child_of< Projectile >() );
    static_assert( Projectile::typeinfo.is_child_of< Entity >() );
    static_assert( !Projectile::typeinfo.is_child_of< Grenade >() );
    static_assert( !Projectile::typeinfo.is_child_of< SleepDust >() );

    // ** Class to Class - dynamic
    ASSERT(  ( is_child_of< Entity, Projectile >() ) );
    ASSERT( ! ( is_child_of< Entity, Entity >() ) );
    ASSERT( ! ( is_child_of< Entity, Grenade >() ) );
    ASSERT( ! ( is_child_of< Projectile, Projectile >() ) );
    ASSERT( ! ( is_child_of< Projectile, Entity >() ) );
    ASSERT(  ( is_child_of< Projectile, Grenade >() ) );
    ASSERT(  ( is_child_of< Projectile, SleepDust >() ) );
    ASSERT( ! ( is_child_of< Grenade, Projectile >() ) );
    ASSERT( ! ( is_child_of< Grenade, Entity >() ) );
    ASSERT( ! ( is_child_of< Grenade, Grenade >() ) );

    ASSERT( !Entity::typeinfo.is_child_of< Projectile >() );
    ASSERT( !Entity::typeinfo.is_child_of< Entity >() );
    ASSERT( !Entity::typeinfo.is_child_of< Grenade >() );
    ASSERT( Grenade::typeinfo.is_child_of< Projectile >() );
    ASSERT( !Grenade::typeinfo.is_child_of< Entity >() );
    ASSERT( !Grenade::typeinfo.is_child_of< Grenade >() );
    ASSERT( !Projectile::typeinfo.is_child_of< Projectile >() );
    ASSERT( Projectile::typeinfo.is_child_of< Entity >() );
    ASSERT( !Projectile::typeinfo.is_child_of< Grenade >() );
    
    // ** ID to Class - dynamic
    ASSERT( is_child_of< Entity >( TypeID_Projectile ) );
    ASSERT( is_child_of< Projectile >( TypeID_Grenade ) );
    ASSERT( !is_child_of< Entity >( TypeID_Grenade ) );
    ASSERT( !is_child_of< Grenade >( TypeID_Projectile ) );
    ASSERT( !is_child_of< Grenade >( TypeID_Entity ) );
    ASSERT( is_child_of< Entity >( get_typeid< Projectile >() ) );
    ASSERT( is_child_of< Projectile >( get_typeid< Grenade >() ) );
    ASSERT( is_child_of< Projectile >( get_typeid< SleepDust >() ) );
    ASSERT( !is_child_of< Entity >( get_typeid< Grenade >() ) );
    ASSERT( !is_child_of< Grenade >( get_typeid< Projectile >() ) );
    ASSERT( !is_child_of< Grenade >( get_typeid< Entity >() ) );

    ASSERT( !Entity::typeinfo.is_child_of( TypeID_Projectile ) );
    ASSERT( !Entity::typeinfo.is_child_of( TypeID_Entity ) );
    ASSERT( !Entity::typeinfo.is_child_of( TypeID_Grenade ) );
    ASSERT( Grenade::typeinfo.is_child_of( TypeID_Projectile ) );
    ASSERT( !Grenade::typeinfo.is_child_of( TypeID_Entity ) );
    ASSERT( !Grenade::typeinfo.is_child_of( TypeID_Grenade ) );
    ASSERT( !Projectile::typeinfo.is_child_of( TypeID_Projectile ) );
    ASSERT( Projectile::typeinfo.is_child_of( TypeID_Entity ) );
    ASSERT( !Projectile::typeinfo.is_child_of( TypeID_Grenade ) );
    ASSERT( SleepDust::typeinfo.is_child_of( TypeID_Projectile ) );

    // ** ID to Class - static
    static_assert( !is_child_of< Entity >( TypeID_Entity ) );
    static_assert( !is_child_of< Projectile >( TypeID_Projectile ) );
    static_assert( is_child_of< Entity >( TypeID_Projectile ) );
    static_assert( is_child_of< Projectile >( TypeID_Grenade ) );
    static_assert( !is_child_of< Entity >( TypeID_Grenade ) );
    static_assert( !is_child_of< Grenade >( TypeID_Projectile ) );
    static_assert( !is_child_of< Grenade >( TypeID_Entity ) );
    static_assert( is_child_of< Entity >( get_typeid< Projectile >() ) );
    static_assert( is_child_of< Projectile >( get_typeid< Grenade >() ) );
    static_assert( !is_child_of< Entity >( get_typeid< Grenade >() ) );
    static_assert( !is_child_of< Grenade >( get_typeid< Projectile >() ) );
    static_assert( !is_child_of< Grenade >( get_typeid< Entity >() ) );
    static_assert( is_child_of<Projectile>( get_typeid<SleepDust>() ) );

    static_assert( !Entity::typeinfo.is_child_of( TypeID_Projectile ) );
    static_assert( !Entity::typeinfo.is_child_of( TypeID_Entity ) );
    static_assert( !Entity::typeinfo.is_child_of( TypeID_Grenade ) );
    static_assert( Grenade::typeinfo.is_child_of( TypeID_Projectile ) );
    static_assert( !Grenade::typeinfo.is_child_of( TypeID_Entity ) );
    static_assert( !Grenade::typeinfo.is_child_of( TypeID_Grenade ) );
    static_assert( !Projectile::typeinfo.is_child_of( TypeID_Projectile ) );
    static_assert( Projectile::typeinfo.is_child_of( TypeID_Entity ) );
    static_assert( !Projectile::typeinfo.is_child_of( TypeID_Grenade ) );
    static_assert( SleepDust::typeinfo.is_child_of( TypeID_Projectile ) );
        
    // ** Object to Class - dynamic only
    ASSERT( ( is_child_of< Entity >(p) ) );
    ASSERT( ! ( is_child_of< Entity >(e) ) );
    ASSERT( ! ( is_child_of< Entity >(g) ) );
    
    ASSERT( ! ( is_child_of< Grenade >(p) ) );
    ASSERT( ! ( is_child_of< Grenade >(e) ) );
    ASSERT( ! ( is_child_of< Grenade >(g) ) );
    
    ASSERT( ! ( is_child_of< Projectile >(p) ) );
    ASSERT( ! ( is_child_of< Projectile >(e) ) );
    ASSERT( ( is_child_of< Projectile >(g) ) );

    ASSERT( !Entity::typeinfo.is_child_of(p) );
    ASSERT( !Entity::typeinfo.is_child_of(e) );
    ASSERT( !Entity::typeinfo.is_child_of(g) );
    
    ASSERT( Grenade::typeinfo.is_child_of(p) );
    ASSERT( !Grenade::typeinfo.is_child_of(e) );
    ASSERT( !Grenade::typeinfo.is_child_of(g) );
    
    ASSERT( !Projectile::typeinfo.is_child_of(p) );
    ASSERT( Projectile::typeinfo.is_child_of(e) );
    ASSERT( !Projectile::typeinfo.is_child_of(g) );
    
    ASSERT( SleepDust::typeinfo.is_child_of(p) );
    ASSERT( !SleepDust::typeinfo.is_child_of(e) );
    ASSERT( !SleepDust::typeinfo.is_child_of(g) );

    // ** Object to ID - dynamic
    ASSERT( ( is_child_of( TypeID_Entity, p ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Entity, e ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Entity, g ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Projectile, p ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Projectile, e ) ) );      
    ASSERT( ( is_child_of( TypeID_Projectile, g ) ) );
    ASSERT( ! ( is_child_of( TypeID_Grenade, p ) ) );
    ASSERT( ! ( is_child_of( TypeID_Grenade, e ) ) );
    ASSERT( ! ( is_child_of( TypeID_Grenade, g ) ) );
    ASSERT( ( is_child_of( TypeID_Projectile, s ) ) );

    // ** ID to ID - static
    static_assert( ( is_child_of( TypeID_Entity, TypeID_Projectile ) ) );      
    static_assert( ! ( is_child_of( TypeID_Entity, TypeID_Entity ) ) );      
    static_assert( ! ( is_child_of( TypeID_Entity, TypeID_Grenade ) ) );      
    static_assert( ! ( is_child_of( TypeID_Projectile, TypeID_Projectile ) ) );      
    static_assert( ! ( is_child_of( TypeID_Projectile, TypeID_Entity ) ) );      
    static_assert( ( is_child_of( TypeID_Projectile, TypeID_Grenade ) ) );
    static_assert( ( is_child_of( TypeID_Projectile, TypeID_SleepDust ) ) );
    static_assert( ! ( is_child_of( TypeID_Grenade, TypeID_Projectile ) ) );
    static_assert( ! ( is_child_of( TypeID_Grenade, TypeID_Entity ) ) );
    static_assert( ! ( is_child_of( TypeID_Grenade, TypeID_Grenade ) ) );
    
    // ** ID to ID - dynamic
    ASSERT( ( is_child_of( TypeID_Entity, TypeID_Projectile ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Entity, TypeID_Entity ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Entity, TypeID_Grenade ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Projectile, TypeID_Projectile ) ) );      
    ASSERT( ! ( is_child_of( TypeID_Projectile, TypeID_Entity ) ) );      
    ASSERT( ( is_child_of( TypeID_Projectile, TypeID_Grenade ) ) );
    ASSERT( ( is_child_of( TypeID_Projectile, TypeID_SleepDust ) ) );
    ASSERT( ! ( is_child_of( TypeID_Grenade, TypeID_Projectile ) ) );
    ASSERT( ! ( is_child_of( TypeID_Grenade, TypeID_Entity ) ) );
    ASSERT( ! ( is_child_of( TypeID_Grenade, TypeID_Grenade ) ) );
}
#endif // CLIB_UNIT_TEST

