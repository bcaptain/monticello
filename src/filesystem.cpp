// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./filesystem.h"
#include "./files/manifest.h"

// all directories are expected to have a trailing '/'
void TruncateFileFromPath( std::string& out ) {
    const std::size_t len = out.size();

    if ( len == 0 )
        return;

    const std::size_t last_idx = len - 1;

    std::size_t i = len;
    while ( i > 0 ) {
        --i;
        if ( out[i] == '/' ) {
            if ( i < last_idx ) {
                out.resize(i+1);
            }
            return;
        }
    }

    out.erase(); // there was nothing or there was only one file
}

// all directories are expected to have a trailing '/'
void TruncateDirFromPath( int count, std::string& out ) {
    if ( count == 0 )
        return;

    const std::size_t len = out.size();

    if ( len == 0 )
        return;

    const std::size_t last_idx = len - 1;

    if ( out[last_idx] != '/' )
        return; // can't truncate dir if there's a file at the end of it

    std::size_t i = len;
    while ( i > 0 ) {
        --i;
        if ( out[i] == '/' ) {
            if ( count == 0 ) {
                if ( i < last_idx ) {
                    out.resize(i+1);
                }
                return;
            }
            --count;
        }
    }

    out.erase(); // there was nothing or there was only one directory
}

void GetContainingFolder( const char* path, std::string& out ) {
    // ** determine model which it belongs to base on directory it is in
    std::size_t i = String::Len( path );

    if ( i == 0 ) {
        ERR("empty path given for animation to load.\n");
        out.erase();
        return;
    }

    std::size_t start=0, end=0;

    while ( i > 0 ) {
        --i;
        char ch = path[i];
        if ( ch == '/' || ch == '\\' ) {
            if ( end != 0 ) {
                start = i+1;
                break;
            } else {
                end = i-1;
            }
        }
    }

    if ( end <= start ) {
        ERR("couldn't determine containing folder for: %s\n", path );
        out.erase();
        return;
    }

    out = String::Sub( path, start, end );
}

void OverrideWarning( const char* assert_type, const std::string& asset_name, const std::string& asset_path, const char* asset_container ) {
    WARN("Override %s: %s with %s from %s\n"
        , assert_type
        , asset_name.c_str()
        , asset_path.c_str()
        , ( asset_container == nullptr || asset_container[0] == '\0' ) ? "filesystem" : asset_container
    );
}

void WontOverrideWarning( const char* assert_type, const std::string& asset_name, const std::string& asset_path, const char* asset_container ) {
    WARN("Override Not Allowed %s: %s with %s from %s\n"
        , assert_type
        , asset_name.c_str()
        , asset_path.c_str()
        , ( asset_container == nullptr || asset_container[0] == '\0' ) ? "filesystem" : asset_container
    );
}

bool OpenAssetFile( const char* asset_type, const char* asset_name, const HashList< AssetManifest >& manifest, File& asset_file_out ) {
    if ( ! OpenAssetFile_NoErr( asset_type, asset_name, manifest, asset_file_out ) ) {
        ERR("%s not found: %s\n", asset_type, asset_name);
        return false;
    }
    return true;
}

bool OpenAssetFile_NoErr( const char* asset_type, const char* asset_name, const HashList< AssetManifest >& manifest, File& asset_file_out ) {
    const AssetManifest * sp = manifest.Get( asset_name );

    if ( sp == nullptr ) {
        return false;
    }

    return OpenAssetFile( asset_type, *sp, asset_file_out );
}

bool OpenAssetFile( const char* asset_type, const AssetManifest& data, File& asset_file_out ) {
    // ** check if the asset exists on the filesystem
    if ( data.GetContainer() == "" ) {
        return asset_file_out.Open( data.GetPath().c_str() );
    }

    // ** file must be in an archive
    FileZip archive( data.GetContainer().c_str() );
    if ( ! archive.IsOpen() ) {
        ERR("Unable to load %s %s from archive %s, archive does not exist.\n", asset_type, data.GetPath().c_str(), data.GetContainer().c_str() );
        return false;
    }

    std::string without_data_path;
    archive.OpenFile( data.GetPath().c_str(), asset_file_out );
    if ( ! archive.IsOpen() ) {
        ERR("Unable to load %s %s from archive %s, archive does not contain file.\n", asset_type, without_data_path.c_str(), data.GetContainer().c_str() );
        return false;
    }

    return true;
}

void ExtractPackagePath( const char * path, std::string& out ) {
    uint slashes_looking_for = 3; // we want to skup two '/', so stop on the third
    std::string tmp = String::Left(path, 5);
    if ( tmp != "data/" ) {
        tmp = String::Left(path, DataDir.size() );
        if ( tmp != DataDir.c_str() ) {
            out.erase();
            return;
        } else {
            ++slashes_looking_for; // skip the './' also
        }
    }

    std::size_t len = String::Len(path);
    uint slashes = 0;

    for ( uint i=0; i<len; ++i ) {
        if ( path[i] == '/' ) {
            if ( ++slashes == slashes_looking_for ) {
                out = String::Left(path, ++i);
                return;
            }
        }
    }

    WARN("ExtractPackagePath: Couldn't determine Package Path: %s\n", path);
    out.erase();
}

bool FileExt_IsSoundFile( const std::string& filename ) {
    return (
        String::RightIs( filename, ".wav" ) ||
        String::RightIs( filename, ".ogg" )
    );

}

void OverrideWarning( const char* asset_type, const std::string& asset_name, File& opened_asset_file ) {
    OverrideWarning( asset_type, asset_name, opened_asset_file.GetFilePath(), opened_asset_file.GetFileContainerName().c_str() );
}

void WontOverrideWarning( const char* asset_type, const std::string& asset_name, File& opened_asset_file ) {
    OverrideWarning( asset_type, asset_name, opened_asset_file.GetFilePath(), opened_asset_file.GetFileContainerName().c_str() );
}
