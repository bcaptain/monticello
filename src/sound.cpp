// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./sound.h"
#include "./game.h"
#include "./rendering/renderer.h"

bool SoundManager::instantiated(false);

const int SDL_AUDIO_MONO = 1;
const int SDL_AUDIO_STEREO = 2;
const int MONTICELLO_SOUND_AUDIOCHANNELS = MIX_DEFAULT_CHANNELS;
const int MONTICELLO_SOUND_FREQUENCY = MIX_DEFAULT_FREQUENCY;
const Uint16 MONTICELLO_SOUND_FORMAT = MIX_DEFAULT_FORMAT;
const int MONTICELLO_SOUND_CHUNKSIZE = 4096;
const int MONTICELLO_SOUND_CHANNELS = 32;
const int MAX_HEARABLE_DISTANCE_FROM_CAMERA = 30;
const int MAX_HEARABLE_DISTANCE_FROM_CAMERA_SQ = MAX_HEARABLE_DISTANCE_FROM_CAMERA * MAX_HEARABLE_DISTANCE_FROM_CAMERA;
const int MAX_VOLUME = 128;

SoundManager soundManager;

SoundManager::SoundManager( void )
    : AssetManager( "Sound" )
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

void SoundManager::UnloadAll( void ) {
    for ( const auto& itr : manifest ) {
        if ( itr.second.data ) {
            SoundData& snd = *itr.second.data;
            if ( snd.chunk ) {
                Mix_FreeChunk( snd.chunk );
                snd.chunk = nullptr;
            }
        }
    }
}

void SoundManager::BeingUnloaded( SoundData& asset ) {
    if ( ! asset.chunk )
        return;

    Mix_FreeChunk( asset.chunk );
    asset.chunk = nullptr;
}

bool SoundManager::Load( const std::string& handle ) {
    AssetManifest* asset = manifest.Get(handle.c_str());
    return asset && Load( *asset );
}

bool SoundManager::Load( File& opened_sound_file ) {
    // ** make sure the file is open

    if ( ! opened_sound_file.IsOpen() ) {
        ERR("Couldn't load sound file %s.\n", opened_sound_file.GetFilePath().c_str() );
        return false;
    }

    // ** create sound handle from file name
    const std::string handle = String::TrimExtentionFromFileName( String::GetFileFromPath( opened_sound_file.GetFilePath() ) );

    // ** check if overriding
    AssetData< SoundData >* mix_pp = manifest.Get( handle.c_str() );
    if ( mix_pp && mix_pp->data ) {
        WARN("Override Sound: %s with %s from %s\n"
             , handle.c_str()
             , opened_sound_file.GetFilePath().c_str()
             , opened_sound_file.GetFileContainerName_Full().c_str()
        );
        // Note: we dont free the previous data until later in this function when we know for sure the new data is successfully read
    }

    // ** load it

    /*
        We have to throw file data into SDL__RWops which is what SDL uses to load files from memory.

        from http://wiki.libsdl.org/SDL_RWFromMem -

        "This memory buffer is not copied by the RWops; the pointer you provide must remain valid
        until you close the stream. Closing the stream will not free the original buffer."
    */

    SDL_RWops *rw = SDL_RWFromConstMem( opened_sound_file.GetBuffer(), opened_sound_file.GetSize() );
    std::shared_ptr< SoundData > snd = std::make_shared< SoundData >();
    snd->chunk = Mix_LoadWAV( opened_sound_file.GetFilePath().c_str() );
    SDL_FreeRW(rw);

    if ( snd == nullptr ) {
        ERR("SDL: %s: \"%s\"\n", SDL_GetError(), opened_sound_file.GetFilePath().c_str() );

        return false;
    } else {
        //todo: this is a little ugly
        if ( mix_pp && mix_pp->data && mix_pp->data->chunk ) {
            // now we can free the previous data since the overriding data was successfully read
            Mix_FreeChunk( mix_pp->data->chunk );
        }
    }

    mix_pp->data = snd;

    CMSG_LOG("Loaded sound: %s from %s\n", opened_sound_file.GetFilePath().c_str(), opened_sound_file.GetFileContainerName_Full().c_str() );

    return true;
}

int SoundManager::GetVolumeForDistance( const Vec3f& origin, const int volume ) {
    const Vec3f cameraOrigin = renderer->camera.GetOrigin();
    const float dist_sq = ( origin - cameraOrigin ).SquaredLen();
    const float volumePercentLost = dist_sq / MAX_HEARABLE_DISTANCE_FROM_CAMERA_SQ;
    int newVolume = static_cast< int >( volume - ( volumePercentLost * volume ) );
    return newVolume;
}

bool SoundManager::PlayAt( const Vec3f& origin, const char *handle, const int volume ) {
    const int newVolume = GetVolumeForDistance( origin, volume );
    return Play( handle, newVolume );
}

bool SoundManager::Play( const char *handle, const int volume, const int loop_times ) {
    auto snd = Get(handle);

    // if sound chunk couldn't be found, we may not have loaded it yet
    if ( !snd || ! snd->chunk )
        if ( ! Load( handle ) )
            return false;

    snd = Get(handle);
    if ( !snd || ! snd->chunk ) {
        ERR("Couldn't play sound handle \"%s\".\n",handle);
        return false;
    }

    int vol = volume;
    if ( vol > 128 )
        vol = 128;

    int globalVol = globalVals.GetInt( gval_volume, 100 );
    if ( globalVol < 0 )
        globalVol = 0;
    if ( globalVol > 100 )
        globalVol = 100;
    globalVol /= 100;

    vol *= globalVol;

    if ( volume < 0 )   
        return true;

    Mix_VolumeChunk(snd->chunk, vol);
    Mix_PlayChannel(-1,snd->chunk,loop_times);

    return true;
}
