// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"
#include "./selectMode.h"
#include "../input/input.h"
#include "../rendering/renderer.h"

void Editor::SetEditorInputDefaults( void ) {
    editor_binds.Clear();

    editor_binds.Set("camRotCW", { SDL_SCANCODE_COMMA } );
    editor_binds.Set("camRotCCW", { SDL_SCANCODE_PERIOD } );
    editor_binds.Set("camTiltF", { SDL_SCANCODE_PAGEUP } );
    editor_binds.Set("camTiltB", { SDL_SCANCODE_PAGEDOWN } );
}

void Editor::EvalMouseMotion( void ) {
    // do "all edit modes" stuff furst
    switch ( mouseMode ) {
        case MouseModeT::SCENEPAN:
            DoScenePan();
            return;
        case MouseModeT::SCENEROTATE:
            DoSceneRot();
            return;
        case MouseModeT::SLIDER:
            SliderUpdate();
            return;
        case MouseModeT::RECTANGLE_SELECT:
            RectangleSelect_Drag();
            return;
        case MouseModeT::ENTITY_GRABBED: break;
        case MouseModeT::NAVPOLY_GRABBED: break;
        case MouseModeT::MENU: break;
        case MouseModeT::BLEND: break;
        case MouseModeT::ADD_TRIGGER_CHAIN: break;
        case MouseModeT::ADD_TRIGGER_TARGET: break;
        case MouseModeT::BIND_TO_MASTER: break;
        case MouseModeT::NONE: break;
        default: break;
    }

    // then individual edit mode things
    switch ( editmode ) {
        case EditModeT::SELECT:
            MouseMove_SelectMode();
            break;
        case EditModeT::NAVMESH:
            MouseMove_NavMeshMode();
            break;
        case EditModeT::ADD: break;
        case EditModeT::EFFECTS: break;
        case EditModeT::MAPS: break;
        case EditModeT::WORLDSIZE: break;
        case EditModeT::NONE: break;
        default: break;
    }
}

bool Editor::EvalStateBinds( const std::string& action ) {
    if ( action == "camRotCW" ) {
        renderer->camera.Rotate( 10 );
        return true;
    }

    if ( action == "camRotCCW" ) {
        renderer->camera.Rotate( -10 );
        return true;
    }

    if ( action == "camTiltF" ) {
        renderer->camera.Tilt( 10 );
        return true;
    }

    if ( action == "camTiltB" ) {
        renderer->camera.Tilt( -10 );
        return true;
    }

    return false;
}

bool Editor::EvalEventBinds( const std::string& action ) {
    // then check basic editor stuff
    if ( CheckInputEvent_Editor( action ) )
        return true;

    // then check per-mode things
    switch ( editmode ) {
        case EditModeT::ADD: return CheckInputEvent_AddMode( action );
        case EditModeT::SELECT: return CheckInputEvent_SelectMode( action );
        case EditModeT::NAVMESH: return CheckInputEvent_NavMeshMode( action );
        case EditModeT::EFFECTS: break;
        case EditModeT::MAPS: break;
        case EditModeT::WORLDSIZE: break;
        case EditModeT::NONE: break;
        default: break;
    }
    return false;
}

#endif // MONTICELLO_EDITOR
