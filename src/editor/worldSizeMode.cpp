// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./editor.h"
#include "./ui/editmode.h"
#include "../game.h"

void Editor::WorldSizeMode( void ) {
    btnWorldSizeMode->SetFaceBottomColor( COLOR_RADIOBUTTON_GRADIENT_B );
    wndToolBox->ShowNamedElement( WINDOW_WORLDSIZE_NAME, true );

    UpdateWorldSize( globalVals.GetVec3f( gval_g_octreeSize ) );
}

void Editor::UpdateWorldSize( const Vec3f& dim ) {
    std::string tmp( std::to_string( static_cast< std::size_t >( dim.x ) ) );
    String::TrimTrailing( tmp, "0" );
    worldSizeX->SetLabel( tmp );

    tmp = std::to_string( static_cast< std::size_t >( dim.y ) );
    String::TrimTrailing( tmp, "0" );
    worldSizeY->SetLabel( tmp );

    tmp = std::to_string( static_cast< std::size_t >( dim.z ) );
    String::TrimTrailing( tmp, "0" );
    worldSizeZ->SetLabel( tmp );
}

void Editor::SetWorldSize( void ) {
    game->SetEntTreeDimensions(
        String::ToUInt( worldSizeX->GetLabel() )
        , String::ToUInt( worldSizeY->GetLabel() )
        , String::ToUInt( worldSizeZ->GetLabel() )
    );
}

#endif // MONTICELLO_EDITOR
