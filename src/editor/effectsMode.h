// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_EDITOR_EFFECTSMODE_H
#define SRC_EDITOR_EFFECTSMODE_H

#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"

extern const char* CSTR_PARTICLE_SET_RAND_START_ANGLE;
extern const char* CSTR_PARTICLE_UNSET_RAND_START_ANGLE;
extern const char* CSTR_PARTICLE_SET_DESTINATION;
extern const char* CSTR_PARTICLE_UNSET_DESTINATION;
extern const char* CSTR_PARTICLE_SET_DESTINATION2;
extern const char* CSTR_PARTICLE_UNSET_DESTINATION2;

void Event_EffectSelected( const std::vector< std::string >& args );
void Event_ParticleSelected( const std::vector< std::string >& args );
void Event_ParticleTexSelected( const std::vector< std::string >& args );

void Event_SetRenderEffectData( const uint to );
void Event_SetParticleQuantity( const uint to );
void Event_SetParticleDelay( const uint to );
void Event_SetParticleCease( const uint to );
void Event_SetParticleLifeSpan( const uint to );
void Event_SetParticleBunching( const float to );
void Event_SetParticleRotationTo( const float to );
void Event_SetParticleRotationFrom( const float to );
void Event_SetParticleColorizeWeight( const float to );
void Event_SetParticleColorizeToWeight( const float to );
void Event_SetParticleOpacityFrom( const float to );
void Event_SetParticleOpacityTo( const float to );
void Event_SetParticleOpacityMid( const float to );
void Event_SetParticleScaleFrom( const float to );
void Event_SetParticleScaleTo( const float to );

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_EFFECTSMODE_H
