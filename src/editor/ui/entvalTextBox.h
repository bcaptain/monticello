// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UI_ENTVALTEXTBOX_H_
#define SRC_EDITOR_UI_ENTVALTEXTBOX_H_

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"
#include "../../ui/listbox.h"

class TextBox_FuncWithArg;

void EntValListItem_PressEnter( const std::vector< std::string >& arguments );

/*!

EntValListBox \n\n

text box that applies value to keyLabel when enter is pressed.

**/

class EntValListBox : public ListBox {
    friend void EntValListItem_PressEnter( const std::vector< std::string >& arguments );

public:
    EntValListBox( void ) = delete;
    EntValListBox( const int visibleElements, const int firstItemTabstopIndex, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    EntValListBox( const EntValListBox& other ) = delete;
    EntValListBox operator=( const EntValListBox& other ) = delete;

    void EntValListBoxNew_PressEnter( void );

protected:
    void CreateItems( const uint y_pos_begin ) override;

public:
    void Refresh( void ) override;

private:
    TextBox_FuncWithArg* txtNewEntVal_Key;
    TextBox_FuncWithArg* txtNewEntVal_Val;
};

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UI_ENTVALTEXTBOX_H_
