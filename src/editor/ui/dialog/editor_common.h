// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_EDITOR_UI_DIALOG_COMMON_H
#define SRC_EDITOR_UI_DIALOG_COMMON_H

#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "../../../base/main.h"
#include "../../../ui/dialog/dialog.h"

void Dialog_NewMap( void );

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UI_DIALOG_COMMON_H

