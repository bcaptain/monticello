// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor_common.h"
#include "../../../game.h"

void Dialog_NewMap( void ) {
    auto newMap = [](){ game->ClearMap(); };
    auto cancel = [](){};

    auto dlg = Dialog::Create( "Start a new map?" );
    dlg->AddButton( new DialogButton<decltype(newMap) >( "Yes", *dlg, newMap, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton<decltype(cancel) >( "No", *dlg, cancel, ButtonTypeT::Decline ) );
}

#endif // MONTICELLO_EDITOR
