// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./entvalTextBox.h"
#include "../../ui/textbox.h"
#include "../editor.h"
#include "../../input/input.h"

void EntValListBoxNew_PressEnter( [[maybe_unused]] const std::vector< std::string >& args_unused );
void EntValListBoxNew_PressEnter( [[maybe_unused]] const std::vector< std::string >& args_unused ) {
    editor->EntValListBoxNew_PressEnter();
}

void EntValListBox::EntValListBoxNew_PressEnter( void ) {
    editor->ApplyEntValToSelectedEnts( txtNewEntVal_Key->GetLabel().c_str(), txtNewEntVal_Val->GetLabel().c_str() );
    txtNewEntVal_Key->SetLabel("");
    txtNewEntVal_Val->SetLabel("");
}

EntValListBox::EntValListBox( const int visibleElements, const int firstItemTabstopIndex, const uint width, const uint height )
    : ListBox( visibleElements, firstItemTabstopIndex, nullptr, width, height )
    , txtNewEntVal_Key(nullptr)
    , txtNewEntVal_Val(nullptr)
{
    const uint halfListBoxWidth = static_cast< uint >( GetWidth() / 2 );

    txtNewEntVal_Key = new TextBox_FuncWithArg( "", &::EntValListBoxNew_PressEnter, {}, halfListBoxWidth );
    txtNewEntVal_Key->SetName( "txtNewEntVal_Key" );

    txtNewEntVal_Val = new TextBox_FuncWithArg( "", &::EntValListBoxNew_PressEnter, {}, halfListBoxWidth );
    txtNewEntVal_Val->SetName( "txtNewEntVal_Val" );
    txtNewEntVal_Val->NudgeOrigin( Vec3f( halfListBoxWidth, 0, 0 ) );

    children->Add( txtNewEntVal_Val );
    children->Add( txtNewEntVal_Key );

    CreateItems( WIDGET_STANDARD_HEIGHT );
}

void EntValListItem_PressEnter( const std::vector< std::string >& arguments ) {
    if ( ! VerifyArgNum( "EntValListItem_PressEnter", arguments, 2 ) )
        return;

    editor->ApplyEntValToSelectedEnts( arguments[0].c_str(), arguments[1].c_str() );
}

void EntValListBox::Refresh( void ) {
    Sort();

    if ( topOfList < 0 )
        topOfList = 0;

    if ( numRows < 1 )
        numRows = 1;

    for ( uint idx = static_cast<uint>( topOfList ); idx < static_cast< uint >( numRows+topOfList ); ++idx ) {

        // ** set up the item names we're going to be searching for
        // ** we must search because children can be in any of order

        // we start at the zeroth element and on
        const std::string intString( std::to_string( idx-static_cast<uint>( topOfList ) ) );

        std::string labelName( "EntValListBox_Label" );
        std::string textboxName( "EntValListBox_TextBox" );

        labelName += intString;
        textboxName += intString;

        // ** find the widgets by name

        Label* lblEntVal = dynamic_cast< Label* >( children->FindNamed( labelName ) );
        TextBox_FuncWithArg* txtEntVal = dynamic_cast< TextBox_FuncWithArg* >( children->FindNamed( textboxName ) );

        if ( !lblEntVal || !txtEntVal ) {
            std::string names;

            if ( !lblEntVal )
                names += labelName;

            if ( !txtEntVal ) {
                names += " ";
                names += textboxName;
            }

            ERR( "EntValListBox: could not find or cast child: %s\n.", names.c_str() );
            continue;
        }

        Input::UnsetFocusWidget( txtEntVal );
        Input::UnsetFocusWidget( lblEntVal );

        // ** update their label/etc based on our arguments

        std::string label;
        std::string value;
        std::string id;

        if ( idx < argument_data.size() ) {
            switch ( argument_data[idx].size() ) {
                default: FALLTHROUGH;
                case 4: value = argument_data[idx][3]; FALLTHROUGH;
                case 3: tooltip = argument_data[idx][2]; FALLTHROUGH;
                case 2: id = argument_data[idx][1]; FALLTHROUGH;
                case 1: label = argument_data[idx][0]; break;
                case 0: break;
            }
        } else {
            lblEntVal->SetVisible( false );
            txtEntVal->SetVisible( false );
            continue;
        }

        lblEntVal->SetVisible( true );
        txtEntVal->SetVisible( true );

        lblEntVal->SetLabel( label );
        txtEntVal->SetLabel( value );

        lblEntVal->SetToolTip( tooltip.c_str() );
        txtEntVal->SetToolTip( tooltip.c_str() );

        txtEntVal->arguments.clear();
        txtEntVal->arguments.push_back( label );
        // the value is pushed onto the vector when the TextBox's function is called
    }
}

void EntValListBox::CreateItems( const uint y_pos_begin ) {
    if ( created ) {
        ERR("ListBox: Items already created.\n");
        return;
    }
    created = true;
    bottomTabstopIndex = numRows + topTabstopIndex - 1;

    const uint halfListBoxWidth = static_cast< uint >( GetWidth() / 2 );

    Vec3f nextLabelPos(0,y_pos_begin,0);
    Vec3f nextTextBoxPos( halfListBoxWidth, y_pos_begin, 0);

    for ( int i=0; i<numRows; ++i ) {
        // ** create item names
        const std::string intString( std::to_string( i ) );

        std::string labelName( "EntValListBox_Label" );
        std::string textboxName( "EntValListBox_TextBox" );

        labelName += intString;
        textboxName += intString;

        // ** create the label
        Label* lblBind = new Label( "", halfListBoxWidth );
        lblBind->SetOrigin( nextLabelPos );
        lblBind->SetName( labelName );
        children->Add( lblBind );

        // ** create the textbox
        TextBox_FuncWithArg* txtBind = new TextBox_FuncWithArg( "", &EntValListItem_PressEnter, {}, halfListBoxWidth );
        txtBind->SetOrigin( nextTextBoxPos );
        txtBind->SetName( textboxName );
        txtBind->SetTabstopIndex(topTabstopIndex+i);
        children->Add( txtBind );

        // ** set up the position of the next widgets
        nextLabelPos.y += WIDGET_STANDARD_HEIGHT;
        nextTextBoxPos.y += WIDGET_STANDARD_HEIGHT;
    }
}

#endif // MONTICELLO_EDITOR
