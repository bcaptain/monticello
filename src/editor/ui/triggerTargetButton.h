// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UI_TRIGGERTARGETBUTTON_H
#define SRC_EDITOR_UI_TRIGGERTARGETBUTTON_H

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"
#include "../../ui/ui.h"

/*!

TriggerTargetButton \n\n

button that centers camera on trigger-target entity when clicked

**/

class TriggerTargetButton : public Button {
public:
    TriggerTargetButton( void );
    TriggerTargetButton( const char* text, const std::weak_ptr< Entity >& target, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    ~TriggerTargetButton( void ) override;

public:
    void    Click( void ) override;

public:
    const std::weak_ptr< Entity > pTarget; //!< the key of the entVal. our label (if applied) will be the new value
};

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UI_TRIGGERTARGETBUTTON_H
