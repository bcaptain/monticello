// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UI_COLLECION_H_
#define SRC_EDITOR_UI_COLLECION_H_

typedef unsigned int uint;

extern const uint SECOND_INNER_TOOLBOX_ITEM_WIDTH;
extern const uint HALF_SECOND_INNER_TOOLBOX_ITEM_WIDTH;

extern const uint widget_h_spacing;
extern const uint widget_v_spacing;

extern const uint TOOLTIP_MAX_WIDTH;
extern const uint TOOLTIP_MAX_HEIGHT;

extern const uint TOOLBOX_WIDTH;
extern const uint TOOLBOX_HEIGHT;
extern const uint TOOLBOX_ITEM_WIDTH;
extern const uint toolbar_item_halfwidth;
extern const uint toolbar_nested_item_width;
extern const uint toolbar_nested_item_halfwidth;
extern const uint update_button_width;

extern const uint shade_button_width;

extern const uint editmode_container_width;
extern const uint editmode_button_width;

extern const uint sprite_cm_width;
extern const uint sprite_cm_height;

#ifdef MONTICELLO_EDITOR

class EditMode_Button;

#include "./editmode.h"
#include "./entvalTextBox.h"
#include "./triggerTargetButton.h"

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UI_COLLECION_H_
