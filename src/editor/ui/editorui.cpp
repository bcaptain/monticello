// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./editorui.h"

const uint widget_h_spacing = 2;
const uint widget_v_spacing = 2;

const uint TOOLTIP_MAX_WIDTH = 400; // half screen width
const uint TOOLTIP_MAX_HEIGHT = 300; // half screen height

#ifdef MONTICELLO_EDITOR

const uint TOOLBOX_WIDTH = 250;
const uint TOOLBOX_HEIGHT = 600;
const uint TOOLBOX_ITEM_WIDTH = TOOLBOX_WIDTH - widget_h_spacing*2;
const uint TOOLBOX_HALF_ITEM_WIDTH = TOOLBOX_ITEM_WIDTH/2;

const uint INNER_TOOLBOX_ITEM_WIDTH = TOOLBOX_ITEM_WIDTH - WIDGET_STANDARD_HEIGHT;
const uint SECOND_INNER_TOOLBOX_ITEM_WIDTH = INNER_TOOLBOX_ITEM_WIDTH - WIDGET_STANDARD_HEIGHT;
const uint HALF_SECOND_INNER_TOOLBOX_ITEM_WIDTH = SECOND_INNER_TOOLBOX_ITEM_WIDTH / 2;

const uint toolbar_item_halfwidth = TOOLBOX_HALF_ITEM_WIDTH;
const uint toolbar_nested_item_width = TOOLBOX_ITEM_WIDTH - widget_h_spacing*2;
const uint toolbar_nested_item_halfwidth = TOOLBOX_HALF_ITEM_WIDTH - widget_h_spacing;
const uint update_button_width = 60;

const uint shade_button_width = WIDGET_STANDARD_HEIGHT;

const uint editmode_container_width = 110;
const uint editmode_button_width = editmode_container_width - widget_h_spacing*2;

const uint sprite_cm_width = 100;
const uint sprite_cm_height = 100;

#include "../../input/input.h"
#include "../../game.h"
#include "../addMode.h"
#include "../selectMode.h"
#include "../effectsMode.h"
#include "../../ui/dialog/common.h"
#include "./dialog/editor_common.h"

void BtnSelectMode_Click( void );
void BtnAddMode_Click( void );
void BtnPaintMode_Click( void );
void BtnEffectsMode_Click( void );
void BtnMapsMode_Click( void );
void BtnNavMeshMode_Click( void );
void BtnWorldSizeMode_Click( void );
void BtnDelSprite_Click( void );
void BtnOriginDisplay_Click( void );
void BtnUpdateParticleOffset_Click( void );
void BtnSaveEffect_Click( void );
void BtnRestartEffect_Click( void );
void BtnReloadEffect_Click( void );
void BtnDupEffect_Click( void );
void BtnNewParticle_Click( void );
void BtnDupParticle_Click( void );
void BtnDelParticle_Click( void );
void BtnHideParticle_Click( void );
void BtnShowParticle_Click( void );
void BtnSoloParticle_Click( void );
void BtnToggleParticleRandInitialAngle_Click( void );
void BtnToggleParticleDest_Click( void );
void BtnUpdateParticleDest_Click( void );
void BtnToggleParticleDest2_Click( void );
void BtnUpdateParticleDest2_Click( void );
void ChkParticleFaceCamera_Check( const bool checked );
void BtnSetColorize_Click( void );
void BtnSetColorizeTo_Click( void );
void BtnDeleteSelectedEntity_Click( void );
void BtnSetPlayer_Click( void );
void BtnUnsetPlayer_Click( void );
void BtnSetEnemy_Click( void );
void BtnUnsetEnemy_Click( void );
void BtnUnselectAllEntities_Click( void );
void CombineEntities_Click( void );
void BtnLoadMap_Click( void );
void BtnSaveMapAs_Click( void );
void BtnUpdateEntOrigin_Click( void );
void BtnUpdateEntName_Click( void );
void BtnTargetAdd_Click( void );
void BtnTriggerChainAdd_Click( void );
void BtnSetFlatUV_Click( void );
void BtnUpdateEntBBox_Click( void );
void BtnUpdateEntAngle_Click( void );
void BtnUpdateEntBBoxFromModel_Click( void );
void BtnEntitysBind_Click( void );
void BtnUnbind_Click( void );
void BtnBindTo_Click( void );
void BtnAddNavMesh_Click( void );
void BtnDelNavMesh_Click( void );
void BtnSetWorldSize_Click( void );

void BtnSetWorldSize_Click( void ) {
    editor->SetWorldSize();
}

void BtnAddNavMesh_Click( void ) {
    editor->AddNavMeshNode();
}

void BtnDelNavMesh_Click( void ) {
    editor->Confirm_DelNavMesh();
}

void BtnEntitysBind_Click( void ) {
    editor->TranslateToSelectedEntsBindMaster();
}

void BtnUnbind_Click( void ) {
    editor->UnbindEntSelectionsFromMaster();
}

void BtnBindTo_Click( void ) {
    editor->SelectMasterToBindEntSelectionsTo();
}

void BtnUpdateEntBBoxFromModel_Click( void ) {
    editor->UpdateEntBBoxFromModel();
}

void BtnUpdateEntBBox_Click( void ) {
    editor->UpdateEntBBox();
}

void BtnUpdateEntAngle_Click( void ) {
    editor->UpdateEntAngle();
}

void BtnSetFlatUV_Click( void ) {
    editor->SetFlatUV();
}

void BtnTriggerChainAdd_Click( void ) {
    editor->SelectTriggerChain();
}

void BtnTargetAdd_Click( void ) {
    editor->SelectTriggerTarget();
}

void BtnUpdateEntOrigin_Click( void ) {
    editor->UpdateEntOrigin();
}

void BtnUpdateEntName_Click( void ) {
    editor->UpdateEntName();
}

void BtnLoadMap_Click( void ) {
    editor->Confirm_Map_LoadSelected();
}

void BtnSaveMapAs_Click( void ) {
    editor->Confirm_Map_SaveToSelected();
}

void BtnUpdateParticleOffset_Click( void ) {
    editor->ApplyParticleOffset();
}

void BtnSelectMode_Click( void ) {
    editor->SetMode( EditModeT::SELECT );
}

void BtnAddMode_Click( void ) {
    editor->SetMode( EditModeT::ADD );
}

void BtnWorldSizeMode_Click( void ) {
    editor->SetMode( EditModeT::WORLDSIZE );
}

void BtnNavMeshMode_Click( void ) {
    editor->SetMode( EditModeT::NAVMESH );
}

void BtnMapsMode_Click( void ) {
    editor->SetMode( EditModeT::MAPS );
}

void BtnEffectsMode_Click( void ) {
    editor->SetMode( EditModeT::EFFECTS );
}

void BtnSaveEffect_Click( void ) {
    editor->SaveEffect();
}

void BtnRestartEffect_Click( void ) {
    editor->RestartEffect();
}

void BtnReloadEffect_Click( void ) {
    editor->ReloadEffect();
}

void BtnDupEffect_Click( void ) {
    // todo
}

void BtnNewParticle_Click( void ) {
    editor->NewParticle();
}

void BtnDupParticle_Click( void ) {
    // todo
}

void BtnDelParticle_Click( void ) {
    editor->DeleteSelectedParticle();
}

void BtnHideParticle_Click( void ) {
    editor->HideSelectedParticle();
}

void BtnShowParticle_Click( void ) {
    editor->ShowSelectedParticle();
}

void BtnSoloParticle_Click( void ) {
    editor->SoloSelectedParticle();
}

void BtnToggleParticleRandInitialAngle_Click( void ) {
    editor->ToggleParticleRandInitialAngle();
}

void BtnToggleParticleDest_Click( void ) {
    editor->ToggleParticleDest();
}

void BtnUpdateParticleDest_Click( void ) {
    editor->UpdateParticleDest();
}

void BtnToggleParticleDest2_Click( void ) {
    editor->ToggleParticleDest2();
}

void BtnUpdateParticleDest2_Click( void ) {
    editor->UpdateParticleDest2();
}

void ChkParticleFaceCamera_Check( const bool checked ) {
    editor->SetParticleFaceCamera( checked );
}

void BtnSetColorize_Click( void ) {
    editor->SetColorize();
}

void BtnSetColorizeTo_Click( void ) {
    editor->SetColorizeTo();
}

void BtnDeleteSelectedEntity_Click( void ) {
    editor->DeleteSelectedEnts();
    editor->CloseContextMenus();
}

void BtnSetPlayer_Click( void ) {
    editor->SetSelectedActorTeamName( "player" );
    editor->CloseContextMenus();
}

void BtnUnsetPlayer_Click( void ) {
    editor->SetSelectedActorTeamName( "" );
    editor->CloseContextMenus();
}

void BtnSetEnemy_Click( void ) {
    editor->SetSelectedActorTeamName("");
    editor->CloseContextMenus();
}

void BtnUnsetEnemy_Click( void ) {
    editor->SetSelectedActorTeamName("player");
    editor->CloseContextMenus();
}

void CombineEntities_Click( void ) {
    editor->CombineSelectedEnts();
    editor->CloseContextMenus();
}

void BtnUnselectAllEntities_Click( void ) {
    editor->UnselectAllEntities();
    editor->CloseContextMenus();
}

void Editor::Create_AddModeUI( void ) {

    // ** Window

    const uint wndAddEntOpts_width = TOOLBOX_ITEM_WIDTH;
    const uint wndAddEntOpts_height = WIDGET_STANDARD_HEIGHT*5;

    wndAddEntOpts = Window::NewWindow( WINDOW_ENTADD_NAME.c_str(), "Add Options", wndAddEntOpts_width, wndAddEntOpts_height, wndToolBox );
    wndAddEntOpts->SetVisible( false );

    // ** Entity Selector

    Sprite* sprite = new Label("Entity:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndAddEntOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndAddEntOpts->next_element_vpos,0.0f);

    wndAddEntOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    lblEntFileSelected = new Label( "", toolbar_nested_item_width );
    lblEntFileSelected->SetFontColor( COLOR_DISPLAYLABEL );
    wndAddEntOpts->children->Add( lblEntFileSelected );
    lblEntFileSelected->SetOrigin(  widget_h_spacing, wndAddEntOpts->next_element_vpos, 0.0f );

    wndAddEntOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const int numVisibleItmes = 15;
    const int firstTabstopIndex = 1;
    dboxEntBrowser = new DropDownList( numVisibleItmes, firstTabstopIndex, &Event_EntityInfoSelected, toolbar_nested_item_width );
    wndAddEntOpts->children->Add( dboxEntBrowser );
    dboxEntBrowser->SetOrigin(  widget_h_spacing, wndAddEntOpts->next_element_vpos, 0.0f );
}

void Editor::Create_SelectModeUI( void ) {

    // ** Window

    const uint wndSelectEntOpts_width = TOOLBOX_ITEM_WIDTH;
    const uint wndSelectEntOpts_height = WIDGET_STANDARD_HEIGHT*40;

    wndSelectOpts = Window::NewWindow( WINDOW_ENTSELECT_NAME.c_str(), "Add Options", wndSelectEntOpts_width, wndSelectEntOpts_height, wndToolBox );
    wndSelectOpts->SetVisible( false );

    // ** Selected Material

    Sprite* sprite = new Label("Model:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const int dbox_models_numVisibleItmes = 15;
    const int dbox_models_firstTabstopIndex = 1;
    dboxModels = new DropDownList( dbox_models_numVisibleItmes, dbox_models_firstTabstopIndex, &Event_ModelSelected, toolbar_nested_item_width );
    wndSelectOpts->children->Add( dboxModels );
    dboxModels->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Label("Material:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const int dbox_mtl_numVisibleItmes = 15;
    const int dbox_mtl_firstTabstopIndex = 1;
    dboxMaterials = new DropDownList( dbox_mtl_numVisibleItmes, dbox_mtl_firstTabstopIndex, &Event_MaterialSelected, toolbar_nested_item_width );
    wndSelectOpts->children->Add( dboxMaterials );
    dboxMaterials->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    // ** Entitiy's Origin
    sprite = new Label("Origin XYZ:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const uint origin_textbox_witdh = 100;
    txtEntOriginX = new TextBox( "", origin_textbox_witdh );
    wndSelectOpts->children->Add( txtEntOriginX );
    txtEntOriginX->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    txtEntOriginY = new TextBox( "", origin_textbox_witdh );
    wndSelectOpts->children->Add( txtEntOriginY );
    txtEntOriginY->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func("Apply", &BtnUpdateEntOrigin_Click, ButtonTypeT::None, update_button_width);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing + origin_textbox_witdh, wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    txtEntOriginZ = new TextBox( "", origin_textbox_witdh );
    wndSelectOpts->children->Add( txtEntOriginZ );
    txtEntOriginZ->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    // ** Entity's Name

    sprite = new Label("Entity Type:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const uint name_textbox_witdh = 180;
    lblEntType = new Label( "", name_textbox_witdh );
    wndSelectOpts->children->Add( lblEntType );
    lblEntType->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Label("Entity Name:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    txtEntName = new TextBox( "", name_textbox_witdh );
    wndSelectOpts->children->Add( txtEntName );
    txtEntName->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func("Apply", &BtnUpdateEntName_Click, ButtonTypeT::None, update_button_width);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing + name_textbox_witdh,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    // ** Entity's Name

    sprite = new Label("BBox Bounds:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const uint third_width = 80;
    txtEntBBoxX = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntBBoxX );
    txtEntBBoxX->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    txtEntBBoxY = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntBBoxY );
    txtEntBBoxY->SetOrigin(  widget_h_spacing + third_width, wndSelectOpts->next_element_vpos, 0.0f );

    txtEntBBoxZ = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntBBoxZ );
    txtEntBBoxZ->SetOrigin(  widget_h_spacing + third_width*2, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Label("Bounds Offset:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    txtEntBoundsOffsetX = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntBoundsOffsetX );
    txtEntBoundsOffsetX->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    txtEntBoundsOffsetY = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntBoundsOffsetY );
    txtEntBoundsOffsetY->SetOrigin(  widget_h_spacing + third_width, wndSelectOpts->next_element_vpos, 0.0f );

    txtEntBoundsOffsetZ = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntBoundsOffsetZ );
    txtEntBoundsOffsetZ->SetOrigin(  widget_h_spacing + third_width*2, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    sprite = new Button_Func("Apply", &BtnUpdateEntBBox_Click, ButtonTypeT::None, third_width);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing, wndSelectOpts->next_element_vpos,0.0f);

    sprite = new Button_Func("Set From Model", &BtnUpdateEntBBoxFromModel_Click, ButtonTypeT::None, third_width * 2);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing + third_width, wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    sprite = new Label("Angle:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndSelectOpts->next_element_vpos,0.0f);

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    txtEntAngleX = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntAngleX );
    txtEntAngleX->SetOrigin(  widget_h_spacing, wndSelectOpts->next_element_vpos, 0.0f );

    txtEntAngleY = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntAngleY );
    txtEntAngleY->SetOrigin(  widget_h_spacing + third_width, wndSelectOpts->next_element_vpos, 0.0f );

    txtEntAngleZ = new TextBox( "", third_width );
    wndSelectOpts->children->Add( txtEntAngleZ );
    txtEntAngleZ->SetOrigin(  widget_h_spacing + third_width*2, wndSelectOpts->next_element_vpos, 0.0f );

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    sprite = new Button_Func("Apply", &BtnUpdateEntAngle_Click, ButtonTypeT::None, third_width);
    wndSelectOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing, wndSelectOpts->next_element_vpos,0.0f);

    Create_BindUI();

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    Create_TargetsUI();

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    Create_TriggerChainUI();

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;

    Create_FlatsUI();

    wndSelectOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + widget_v_spacing;
}

void Editor::Create_EntValsUI( void ) {

    // ** Window
    const uint wndEntVals_horizontal_buffer = 4;
    const uint wndEntVals_width = TOOLBOX_ITEM_WIDTH - ( wndEntVals_horizontal_buffer * 2 );
    const uint wndEntVals_height = WIDGET_STANDARD_HEIGHT*10;

    wndEntVals = Window::NewWindow( WINDOW_ENTVALS_NAME.c_str(), "entVals", wndEntVals_width, wndEntVals_height, wndSelectOpts );
    //wndEntVals->SetVisible( false );
    wndEntVals->SetOrigin( wndEntVals_horizontal_buffer, wndSelectOpts->next_element_vpos, 0 );

    const uint listEntValsHeight = wndEntVals_height - WIDGET_STANDARD_HEIGHT - 4;
    const int numVisisbleItems = static_cast<int>( listEntValsHeight / WIDGET_STANDARD_HEIGHT );
    wndEntVals_listBox = new EntValListBox( numVisisbleItems, 0, SECOND_INNER_TOOLBOX_ITEM_WIDTH, listEntValsHeight );
    wndEntVals_listBox->SetOrigin( widget_h_spacing, wndEntVals->next_element_vpos, 0 );
    wndEntVals->children->Add( wndEntVals_listBox );

    wndEntVals->next_element_vpos += WIDGET_STANDARD_HEIGHT;
}

void Editor::Create_BindUI( void ) {

    // ** Window
    const uint wndBind_horizontal_buffer = 4;
    const uint wndBind_width = TOOLBOX_ITEM_WIDTH - ( wndBind_horizontal_buffer * 2 );
    const uint wndBind_height = WIDGET_STANDARD_HEIGHT*10;

    wndBind = Window::NewWindow( WINDOW_ENTVALS_NAME.c_str(), "Bind Master", wndBind_width, wndBind_height, wndSelectOpts );
    //wndBind->SetVisible( false );
    wndBind->SetOrigin( wndBind_horizontal_buffer, wndSelectOpts->next_element_vpos, 0 );

    Sprite* sprite = new Label("Master:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndBind->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndBind->next_element_vpos,0.0f);

    wndBind->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    btnEntitysBind = new Button_Func( "", &BtnEntitysBind_Click, ButtonTypeT::None, SECOND_INNER_TOOLBOX_ITEM_WIDTH );
    btnEntitysBind->SetOrigin( widget_h_spacing, wndBind->next_element_vpos, 0 );
    wndBind->children->Add( btnEntitysBind );

    wndBind->next_element_vpos += WIDGET_STANDARD_HEIGHT;
    wndBind->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Unbind From Master", &BtnUnbind_Click, ButtonTypeT::None, SECOND_INNER_TOOLBOX_ITEM_WIDTH );
    sprite->SetOrigin( widget_h_spacing, wndBind->next_element_vpos, 0 );
    wndBind->children->Add( sprite );

    wndBind->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Select Bind Master", &BtnBindTo_Click, ButtonTypeT::None, SECOND_INNER_TOOLBOX_ITEM_WIDTH );
    sprite->SetOrigin( widget_h_spacing, wndBind->next_element_vpos, 0 );
    wndBind->children->Add( sprite );

    wndBind->next_element_vpos += WIDGET_STANDARD_HEIGHT;
}

void Editor::Create_TargetsUI( void ) {

    // ** Window
    const uint wndTargets_horizontal_buffer = 4;
    const uint wndTargets_width = TOOLBOX_ITEM_WIDTH - ( wndTargets_horizontal_buffer * 2 );
    const uint wndTargets_height = WIDGET_STANDARD_HEIGHT*10;

    wndTargets = Window::NewWindow( WINDOW_ENTVALS_NAME.c_str(), "Trigger Targets", wndTargets_width, wndTargets_height, wndSelectOpts );
    //wndTargets->SetVisible( false );
    wndTargets->SetOrigin( wndTargets_horizontal_buffer, wndSelectOpts->next_element_vpos, 0 );

    Label* sprite = new Button_Func( "Add Target", &BtnTargetAdd_Click, ButtonTypeT::None, SECOND_INNER_TOOLBOX_ITEM_WIDTH );
    sprite->SetOrigin( widget_h_spacing, wndTargets->next_element_vpos, 0 );
    wndTargets->children->Add( sprite );

    wndTargets->next_element_vpos += WIDGET_STANDARD_HEIGHT;
}

void Editor::Create_TriggerChainUI( void ) {

    // ** Window
    const uint wndTriggerChain_horizontal_buffer = 4;
    const uint wndTriggerChain_width = TOOLBOX_ITEM_WIDTH - ( wndTriggerChain_horizontal_buffer * 2 );
    const uint wndTriggerChain_height = WIDGET_STANDARD_HEIGHT*10;

    wndTriggerChain = Window::NewWindow( WINDOW_ENTVALS_NAME.c_str(), "Trigger Chains", wndTriggerChain_width, wndTriggerChain_height, wndSelectOpts );
    wndTriggerChain->SetOrigin( wndTriggerChain_horizontal_buffer, wndSelectOpts->next_element_vpos, 0 );

    Label* sprite = new Label( "Chained targets are triggers which are triggered once this one has finished.", INNER_TOOLBOX_ITEM_WIDTH, WIDGET_STANDARD_HEIGHT * 3 );
    sprite->SetOrigin( widget_h_spacing, wndTriggerChain->next_element_vpos, 0 );
    wndTriggerChain->children->Add( sprite );

    wndTriggerChain->next_element_vpos += WIDGET_STANDARD_HEIGHT * 4;

    sprite = new Button_Func( "Add Trigger", &BtnTriggerChainAdd_Click, ButtonTypeT::None, INNER_TOOLBOX_ITEM_WIDTH );
    sprite->SetOrigin( widget_h_spacing, wndTriggerChain->next_element_vpos, 0 );
    wndTriggerChain->children->Add( sprite );

    wndTriggerChain->next_element_vpos += WIDGET_STANDARD_HEIGHT;
}

void Editor::Create_FlatsUI( void ) {

    // ** Window
    const uint wndFlats_horizontal_buffer = 4;
    const uint wndFlats_width = TOOLBOX_ITEM_WIDTH - ( wndFlats_horizontal_buffer * 2 );
    const uint wndFlats_height = WIDGET_STANDARD_HEIGHT*10;

    wndFlats = Window::NewWindow( WINDOW_ENTVALS_NAME.c_str(), "Flats", wndFlats_width, wndFlats_height, wndSelectOpts );
    wndFlats->SetOrigin( wndFlats_horizontal_buffer, wndSelectOpts->next_element_vpos, 0 );

    const uint half_width = SECOND_INNER_TOOLBOX_ITEM_WIDTH / 2;

    Sprite* sprite = new Label("U.Scale:", TOOLBOX_HALF_ITEM_WIDTH-widget_v_spacing-13);
    wndFlats->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndFlats->next_element_vpos,0.0f);

    txtFlatUVScaleVertical = new TextBox( "", half_width );
    txtFlatUVScaleVertical->SetOrigin( half_width + widget_h_spacing, wndFlats->next_element_vpos, 0 );
    wndFlats->children->Add( txtFlatUVScaleVertical );

    wndFlats->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Label("V.Scale:", TOOLBOX_HALF_ITEM_WIDTH-widget_v_spacing-13);
    wndFlats->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndFlats->next_element_vpos,0.0f);

    txtFlatUVScaleHorizontal = new TextBox( "", half_width );
    txtFlatUVScaleHorizontal->SetOrigin( half_width + widget_h_spacing, wndFlats->next_element_vpos, 0 );
    wndFlats->children->Add( txtFlatUVScaleHorizontal );

    wndFlats->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Label("U.Align:", TOOLBOX_HALF_ITEM_WIDTH-widget_v_spacing-13);
    wndFlats->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndFlats->next_element_vpos,0.0f);

    txtFlatUVAlignVertical = new TextBox( "", half_width );
    txtFlatUVAlignVertical->SetOrigin( half_width + widget_h_spacing, wndFlats->next_element_vpos, 0 );
    wndFlats->children->Add( txtFlatUVAlignVertical );

    wndFlats->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Label("V.Align:", TOOLBOX_HALF_ITEM_WIDTH-widget_v_spacing-13);
    wndFlats->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndFlats->next_element_vpos,0.0f);

    txtFlatUVAlignHorizontal = new TextBox( "", half_width );
    txtFlatUVAlignHorizontal->SetOrigin( half_width + widget_h_spacing, wndFlats->next_element_vpos, 0 );
    wndFlats->children->Add( txtFlatUVAlignHorizontal );

    wndFlats->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Set", &BtnSetFlatUV_Click, ButtonTypeT::None, half_width );
    sprite->SetOrigin( widget_h_spacing, wndFlats->next_element_vpos, 0 );
    wndFlats->children->Add( sprite );

    wndFlats->next_element_vpos += WIDGET_STANDARD_HEIGHT;

}

void Editor::Create_EffectsModeUI( void ) {
    Button_Func *button;

    // ** Window

    const uint wndEffectsOpts_width = TOOLBOX_ITEM_WIDTH;
    const uint wndEffectsOpts_height = WIDGET_STANDARD_HEIGHT*45;

    wndEffectsOpts = Window::NewWindow( WINDOW_EFFECTS_NAME.c_str(), "Effects Options", wndEffectsOpts_width, wndEffectsOpts_height, wndToolBox );
    wndEffectsOpts->SetVisible( false );

    // ** Effects List

    const uint toolpar_pos_middle = widget_h_spacing+toolbar_item_halfwidth;
    const uint half_window_width = wndEffectsOpts_width / 2;

    Sprite* sprite = new Label( "Effect:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    const int box_fx_numVisibleItmes = 15;
    const int box_fx_firstTabstopIndex = 1;
    dboxEffects = new DropDownList( box_fx_numVisibleItmes, box_fx_firstTabstopIndex, &Event_EffectSelected, half_window_width );
    wndEffectsOpts->children->Add( dboxEffects );
    dboxEffects->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // **** Effect: New / Dup / Del / Save / Reload

    const float one_sixth_window_width = half_window_width/3;

    button = new Button_Func( "Reload", &BtnReloadEffect_Click, ButtonTypeT::None, one_sixth_window_width * 2 );
    wndEffectsOpts->children->Add( button );
    effectValElements.push_back(button);
    button->SetOrigin(  toolpar_pos_middle + one_sixth_window_width, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    button = new Button_Func( "Save", &BtnSaveEffect_Click, ButtonTypeT::None, one_sixth_window_width );
    wndEffectsOpts->children->Add( button );
    effectValElements.push_back(button);
    button->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    button = new Button_Func( "Restart", &BtnRestartEffect_Click, ButtonTypeT::None, one_sixth_window_width * 2 );
    wndEffectsOpts->children->Add( button );
    effectValElements.push_back(button);
    button->SetOrigin(  toolpar_pos_middle + one_sixth_window_width, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Fx Duration:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    effectValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldRenderEffectData = new Slider<Uint>( "", half_window_width );
    sldRenderEffectData->SetMethod(&Event_SetRenderEffectData);
    sldRenderEffectData->SetMin( EFFECT_MIN_DURATION );
    sldRenderEffectData->SetMax( EFFECT_MAX_DURATION );
    wndEffectsOpts->children->Add( sldRenderEffectData );
    effectValElements.push_back(sldRenderEffectData);
    sldRenderEffectData->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;


    // ** Effects' Particle List

    sprite = new Label( "Particles:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    effectValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    const int box_prt_numVisibleItmes = 15;
    const int box_prt_firstTabstopIndex = 1;
    dboxParticles = new DropDownList( box_prt_numVisibleItmes, box_prt_firstTabstopIndex, &Event_ParticleSelected, half_window_width );
    wndEffectsOpts->children->Add( dboxParticles );
    effectValElements.push_back(dboxParticles);
    dboxParticles->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // **** New Particle

    sprite = new Button_Func( "New", &BtnNewParticle_Click, ButtonTypeT::None, one_sixth_window_width );
    wndEffectsOpts->children->Add( sprite );
    effectValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func( "Del", &BtnDelParticle_Click, ButtonTypeT::None, one_sixth_window_width );
    wndEffectsOpts->children->Add( sprite );
    effectValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + one_sixth_window_width*2, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // **** Hide/Show/Solo

    sprite = new Button_Func( "Hide", &BtnHideParticle_Click, ButtonTypeT::None, one_sixth_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func( "Show", &BtnShowParticle_Click, ButtonTypeT::None, one_sixth_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + one_sixth_window_width, wndEffectsOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func( "Solo", &BtnSoloParticle_Click, ButtonTypeT::None, one_sixth_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + one_sixth_window_width*2, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Particle's Texture (List of all textures)

    sprite = new Label( "Material:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    const int box_tex_numVisibleItmes = 15;
    const int box_tex_firstTabstopIndex = 1;
    dboxParticleTex = new DropDownList( box_tex_numVisibleItmes, box_tex_firstTabstopIndex, &Event_ParticleTexSelected, half_window_width );
    wndEffectsOpts->children->Add( dboxParticleTex );
    particleValElements.push_back(dboxParticleTex);
    dboxParticleTex->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Delay

    sprite = new Label( "Start at:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleDelay = new Slider<Uint>( "", half_window_width );
    sldParticleDelay->SetMethod(&Event_SetParticleDelay);
    sldParticleDelay->SetMin( PARTICLE_MIN_DELAY );
    sldParticleDelay->SetMax( PARTICLE_MAX_DELAY );
    wndEffectsOpts->children->Add( sldParticleDelay );
    particleValElements.push_back(sldParticleDelay);
    sldParticleDelay->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Cease

    sprite = new Label( "PrtDuration:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleCease = new Slider<Uint>( "", half_window_width );
    sldParticleCease->SetMethod(&Event_SetParticleCease);
    sldParticleCease->SetMin( PARTICLE_MIN_CEASE );
    sldParticleCease->SetMax( PARTICLE_MAX_CEASE );
    wndEffectsOpts->children->Add( sldParticleCease );
    particleValElements.push_back(sldParticleCease);
    sldParticleCease->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Quantity

    sprite = new Label( "Quantity:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleQty = new Slider<uint>( "", half_window_width );
    sldParticleQty->SetMethod(&Event_SetParticleQuantity);
    sldParticleQty->SetMin( PARTICLE_MIN_QTY );
    sldParticleQty->SetMax( PARTICLE_MAX_QTY );
    wndEffectsOpts->children->Add( sldParticleQty );
    particleValElements.push_back(sldParticleQty);
    sldParticleQty->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Quantity

    sprite = new Label( "LifeSpan:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleLifeSpan = new Slider<Uint>( "", half_window_width );
    sldParticleLifeSpan->SetMethod(&Event_SetParticleLifeSpan);
    sldParticleLifeSpan->SetMin( PARTICLE_MIN_LIFESPAN );
    sldParticleLifeSpan->SetMax( PARTICLE_MAX_LIFESPAN );
    wndEffectsOpts->children->Add( sldParticleLifeSpan );
    particleValElements.push_back(sldParticleLifeSpan);
    sldParticleLifeSpan->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Bunching

    sprite = new Label( "Bunching:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleBunching = new Slider<float>( "", half_window_width );
    sldParticleBunching->SetMethod(&Event_SetParticleBunching );
    sldParticleBunching->SetMin( 0.0f ); // zero percent
    sldParticleBunching->SetMax( 1.0f ); // one hundred percent
    wndEffectsOpts->children->Add( sldParticleBunching );
    particleValElements.push_back(sldParticleBunching);
    sldParticleBunching->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Rotate Random Angle From

    sprite = new Label( "Rot Rand From:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleRotateFrom = new Slider<float>( "", half_window_width );
    sldParticleRotateFrom->SetMethod(&Event_SetParticleRotationFrom );
    sldParticleRotateFrom->SetMin( PARTICLE_ROTATE_MIN_ANG_PER_SEC );
    sldParticleRotateFrom->SetMax( PARTICLE_ROTATE_MAX_ANG_PER_SEC );
    wndEffectsOpts->children->Add( sldParticleRotateFrom );
    particleValElements.push_back(sldParticleRotateFrom);
    sldParticleRotateFrom->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Rotate Random Angle To

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Rot Rand To:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleRotateTo = new Slider<float>( "", half_window_width );
    sldParticleRotateTo->SetMethod(&Event_SetParticleRotationTo );
    sldParticleRotateTo->SetMin( PARTICLE_ROTATE_MIN_ANG_PER_SEC );
    sldParticleRotateTo->SetMax( PARTICLE_ROTATE_MAX_ANG_PER_SEC );
    wndEffectsOpts->children->Add( sldParticleRotateTo );
    particleValElements.push_back(sldParticleRotateTo);
    sldParticleRotateTo->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Random Inial Rotate Angle

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    btnToggleParticleRandInitialAngle = new Button_Func( ""/*set when particle is changes*/, &BtnToggleParticleRandInitialAngle_Click, ButtonTypeT::None, TOOLBOX_ITEM_WIDTH );
    wndEffectsOpts->children->Add( btnToggleParticleRandInitialAngle );
    particleValElements.push_back(btnToggleParticleRandInitialAngle);
    btnToggleParticleRandInitialAngle->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Color From

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    const float vec_val_width = 30;

    sprite = new Label( "Color:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtParticleColorX = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtParticleColorX );
    particleValElements.push_back(txtParticleColorX);
    txtParticleColorX->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    txtParticleColorY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtParticleColorY );
    particleValElements.push_back(txtParticleColorY);
    txtParticleColorY->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    txtParticleColorZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtParticleColorZ );
    particleValElements.push_back(txtParticleColorZ);
    txtParticleColorZ->SetOrigin(  toolpar_pos_middle + vec_val_width * 2, wndEffectsOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func( "Set", &BtnSetColorize_Click, ButtonTypeT::None, vec_val_width, WIDGET_STANDARD_HEIGHT + widget_v_spacing /* 2 widgets tall plus space between */ );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + vec_val_width * 3, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Color Alpha

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Color Weight:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleColorWeight = new Slider<float>( "", half_window_width );
    sldParticleColorWeight->SetMethod(&Event_SetParticleColorizeWeight );
    sldParticleColorWeight->SetMin( 0.0f ); // zero percent
    sldParticleColorWeight->SetMax( 1.0f ); // 100 percent
    wndEffectsOpts->children->Add( sldParticleColorWeight );
    particleValElements.push_back(sldParticleColorWeight);
    sldParticleColorWeight->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Color To

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Color To:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtParticleColorToX = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtParticleColorToX );
    particleValElements.push_back(txtParticleColorToX);
    txtParticleColorToX->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    txtParticleColorToY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtParticleColorToY );
    particleValElements.push_back(txtParticleColorToY);
    txtParticleColorToY->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    txtParticleColorToZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtParticleColorToZ );
    particleValElements.push_back(txtParticleColorToZ);
    txtParticleColorToZ->SetOrigin(  toolpar_pos_middle + vec_val_width * 2, wndEffectsOpts->next_element_vpos, 0.0f );

    sprite = new Button_Func( "Set", &BtnSetColorizeTo_Click, ButtonTypeT::None, vec_val_width, WIDGET_STANDARD_HEIGHT + widget_v_spacing /* 2 widgets tall plus space between */ );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + vec_val_width * 3, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Color To Alpha

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Color To Weight:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleColorToWeight = new Slider<float>( "", half_window_width );
    sldParticleColorToWeight->SetMethod(&Event_SetParticleColorizeToWeight );
    sldParticleColorToWeight->SetMin( 0.0f ); // zero percent
    sldParticleColorToWeight->SetMax( 1.0f ); // 100 percent
    wndEffectsOpts->children->Add( sldParticleColorToWeight );
    particleValElements.push_back(sldParticleColorToWeight);
    sldParticleColorToWeight->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Opacity From

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Opacity From:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleOpacityFrom = new Slider<float>( "", half_window_width );
    sldParticleOpacityFrom->SetMethod(&Event_SetParticleOpacityFrom );
    sldParticleOpacityFrom->SetMin( PARTICLE_MIN_OPACITY );
    sldParticleOpacityFrom->SetMax( PARTICLE_MAX_OPACITY );
    wndEffectsOpts->children->Add( sldParticleOpacityFrom );
    particleValElements.push_back(sldParticleOpacityFrom);
    sldParticleOpacityFrom->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Opacity Mid

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Opacity Mid:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleOpacityMid = new Slider<float>( "", half_window_width );
    sldParticleOpacityMid->SetMethod(&Event_SetParticleOpacityMid );
    sldParticleOpacityMid->SetMin( PARTICLE_MIN_OPACITY );
    sldParticleOpacityMid->SetMax( PARTICLE_MAX_OPACITY );
    wndEffectsOpts->children->Add( sldParticleOpacityMid );
    particleValElements.push_back(sldParticleOpacityMid);
    sldParticleOpacityMid->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Opacity To

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Opacity To:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleOpacityTo = new Slider<float>( "", half_window_width );
    sldParticleOpacityTo->SetMethod(&Event_SetParticleOpacityTo );
    sldParticleOpacityTo->SetMin( PARTICLE_MIN_OPACITY );
    sldParticleOpacityTo->SetMax( PARTICLE_MAX_OPACITY );
    wndEffectsOpts->children->Add( sldParticleOpacityTo );
    particleValElements.push_back(sldParticleOpacityTo);
    sldParticleOpacityTo->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Scale From

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Scale From:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleScaleFrom = new Slider<float>( "", half_window_width );
    sldParticleScaleFrom->SetMethod(&Event_SetParticleScaleFrom );
    sldParticleScaleFrom->SetMin( PARTICLE_MIN_SCALE );
    sldParticleScaleFrom->SetMax( PARTICLE_MAX_SCALE );
    wndEffectsOpts->children->Add( sldParticleScaleFrom );
    particleValElements.push_back(sldParticleScaleFrom);
    sldParticleScaleFrom->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Scale To

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Scale To:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    sldParticleScaleTo = new Slider<float>( "", half_window_width );
    sldParticleScaleTo->SetMethod(&Event_SetParticleScaleTo );
    sldParticleScaleTo->SetMin( PARTICLE_MIN_SCALE );
    sldParticleScaleTo->SetMax( PARTICLE_MAX_SCALE );
    wndEffectsOpts->children->Add( sldParticleScaleTo );
    particleValElements.push_back(sldParticleScaleTo);
    sldParticleScaleTo->SetOrigin( toolpar_pos_middle,wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Offset

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Offset:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtOffsetX = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtOffsetX );
    particleValElements.push_back(txtOffsetX);
    txtOffsetX->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    txtOffsetY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtOffsetY );
    particleValElements.push_back(txtOffsetY);
    txtOffsetY->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    txtOffsetZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtOffsetZ );
    particleValElements.push_back(txtOffsetZ);
    txtOffsetZ->SetOrigin(  toolpar_pos_middle + vec_val_width * 2, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    // ** Offset/Distrib Update Button

    sprite = new Button_Func( "Update\nOffset", &BtnUpdateParticleOffset_Click, ButtonTypeT::None, vec_val_width * 3 /* three buttons wide */, WIDGET_STANDARD_HEIGHT * 2 + widget_v_spacing /* 2 widgets tall plus space between */ );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Offset Distrib

    sprite = new Label( " Distrib X/Y:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtOriginDistribXY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtOriginDistribXY );
    particleValElements.push_back(txtOriginDistribXY);
    txtOriginDistribXY->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( " Distrib Z:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtOriginDistribZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtOriginDistribZ );
    particleValElements.push_back(txtOriginDistribZ);
    txtOriginDistribZ->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Dest:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDestX = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDestX );
    particleValElements.push_back(txtDestX);
    txtDestX->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDestY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDestY );
    particleValElements.push_back(txtDestY);
    txtDestY->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDestZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDestZ );
    particleValElements.push_back(txtDestZ);
    txtDestZ->SetOrigin(  toolpar_pos_middle + vec_val_width * 2, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest/Distrib Set/Unset Button

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    btnToggleParticleDest = new Button_Func( "", &BtnToggleParticleDest_Click, ButtonTypeT::None, vec_val_width * 3 /* three buttons wide */, WIDGET_STANDARD_HEIGHT );
    wndEffectsOpts->children->Add( btnToggleParticleDest );
    particleValElements.push_back(btnToggleParticleDest);
    btnToggleParticleDest->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest Distrib

    sprite = new Label( " Distrib X/Y:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDestDistribXY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDestDistribXY );
    particleValElements.push_back(txtDestDistribXY);
    txtDestDistribXY->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( " Distrib Z:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDestDistribZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDestDistribZ );
    particleValElements.push_back(txtDestDistribZ);
    txtDestDistribZ->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest/Distrib Update Button

    sprite = new Button_Func( "Apply", &BtnUpdateParticleDest_Click, ButtonTypeT::None, vec_val_width * 3 /* three buttons wide */, WIDGET_STANDARD_HEIGHT );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest2

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( "Dest2:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDest2X = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDest2X );
    particleValElements.push_back(txtDest2X);
    txtDest2X->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDest2Y = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDest2Y );
    particleValElements.push_back(txtDest2Y);
    txtDest2Y->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDest2Z = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDest2Z );
    particleValElements.push_back(txtDest2Z);
    txtDest2Z->SetOrigin(  toolpar_pos_middle + vec_val_width * 2, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest2/Distrib2 Set/Unset Button

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    btnToggleParticleDest2 = new Button_Func( "", &BtnToggleParticleDest2_Click, ButtonTypeT::None, vec_val_width * 3 /* three buttons wide */, WIDGET_STANDARD_HEIGHT );
    wndEffectsOpts->children->Add( btnToggleParticleDest2 );
    particleValElements.push_back(btnToggleParticleDest2);
    btnToggleParticleDest2->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest2 Distrib

    sprite = new Label( " Distrib X/Y:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDest2DistribXY = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDest2DistribXY );
    particleValElements.push_back(txtDest2DistribXY);
    txtDest2DistribXY->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Label( " Distrib Z:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    txtDest2DistribZ = new TextBox( "", vec_val_width );
    wndEffectsOpts->children->Add( txtDest2DistribZ );
    particleValElements.push_back(txtDest2DistribZ);
    txtDest2DistribZ->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** Dest2/Distrib2 Update Button

    sprite = new Button_Func( "Apply", &BtnUpdateParticleDest2_Click, ButtonTypeT::None, vec_val_width * 3 /* three buttons wide */, WIDGET_STANDARD_HEIGHT );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  toolpar_pos_middle + vec_val_width, wndEffectsOpts->next_element_vpos, 0.0f );

    // ** orientation
    
    wndEffectsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT+widget_v_spacing;
    
    sprite = new Label( "Face Camera:", half_window_width );
    wndEffectsOpts->children->Add( sprite );
    particleValElements.push_back(sprite);
    sprite->SetOrigin(  widget_h_spacing, wndEffectsOpts->next_element_vpos, 0.0f );

    chkFaceCamera = new CheckBox( &ChkParticleFaceCamera_Check );
    wndEffectsOpts->children->Add( chkFaceCamera );
    particleValElements.push_back(chkFaceCamera);
    chkFaceCamera->SetOrigin(  toolpar_pos_middle, wndEffectsOpts->next_element_vpos, 0.0f );

    // now hide it all
    ShowEffectUIValues( false );
}

void Editor::Create_ToolTip( void ) {
    lblToolTip = new Label("ToolTip", TOOLTIP_MAX_WIDTH,TOOLTIP_MAX_HEIGHT );
    game->sprites[UI_LAYER_EDITORUI_DIALOG].Add( lblToolTip );
    lblToolTip->SetVisible( false );
    lblToolTip->SetFaceOpacity( UI_OPACITY );
    lblToolTip->SetBorder( true );
}

void Editor::Create_ContextMenu( void ) {
    // ** window

    const uint wndContextMenu_width = sprite_cm_width+widget_v_spacing*2;

    wndContextMenu = new Window( WindowOptionsT::NO_COLLAPSE_BUTTON, wndContextMenu_width,sprite_cm_height );
    game->sprites[UI_LAYER_EDITORUI].Add( wndContextMenu );

    wndContextMenu->SetVisible( false );

    // ** window title

    const uint wndContextMenu_title_width = wndContextMenu_width - WINDOW_COLLAPSE_BUTTON_WIDTH;

    Sprite* sprite = new WindowTitle("Entity", wndContextMenu_title_width);
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin( WINDOW_COLLAPSE_BUTTON_WIDTH,0.0f,0.0f);

    wndContextMenu->next_element_vpos = WIDGET_STANDARD_HEIGHT+widget_v_spacing;

    sprite = new Button_Func( "Delete", &BtnDeleteSelectedEntity_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Unselect All", &BtnUnselectAllEntities_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Debug", &debugOutput, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Combine Entities", &CombineEntities_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Set Player", &BtnSetPlayer_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Unset Player", &BtnUnsetPlayer_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Set Enemy", &BtnSetEnemy_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );

    wndContextMenu->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Unset Enemy", &BtnUnsetEnemy_Click, ButtonTypeT::None, sprite_cm_width );
    wndContextMenu->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndContextMenu->next_element_vpos, 0.0f );
}

void Editor::Create_ToolBoxUI( void ) {
    // ** the window

    const uint wndToolBox_width = TOOLBOX_WIDTH+widget_v_spacing*2;

    wndToolBox = new Window( WindowOptionsT::DEFAULT, wndToolBox_width, TOOLBOX_HEIGHT );
    game->sprites[UI_LAYER_EDITORUI].Add( wndToolBox );

    Sprite* sprite;

    // ** the window title

    const uint wndToolBox_title_width = wndToolBox_width - WINDOW_COLLAPSE_BUTTON_WIDTH;

    sprite = new WindowTitle("Toolbox", wndToolBox_title_width);
    wndToolBox->children->Add( sprite );
    sprite->SetOrigin( WINDOW_COLLAPSE_BUTTON_WIDTH,0,0.0f);

    // ** Editor Mode Buttons
    btnSelectMode = new Button_Func( "Select", &BtnSelectMode_Click, ButtonTypeT::None, editmode_button_width );
    wndToolBox->children->Add( btnSelectMode );

    btnAddMode = new Button_Func( "Add", &BtnAddMode_Click, ButtonTypeT::None, editmode_button_width );
    wndToolBox->children->Add( btnAddMode );

    btnEffectsMode = new Button_Func( "Effects", &BtnEffectsMode_Click, ButtonTypeT::None, editmode_button_width );
    wndToolBox->children->Add( btnEffectsMode );

    btnMapsMode = new Button_Func( "Maps", &BtnMapsMode_Click, ButtonTypeT::None, editmode_button_width );
    wndToolBox->children->Add( btnMapsMode );

    btnNavMeshMode = new Button_Func( "NavMesh", &BtnNavMeshMode_Click, ButtonTypeT::None, editmode_button_width );
    wndToolBox->children->Add( btnNavMeshMode );

    btnWorldSizeMode = new Button_Func( "World Size", &BtnWorldSizeMode_Click, ButtonTypeT::None, editmode_button_width );
    wndToolBox->children->Add( btnWorldSizeMode );
}

void Editor::Create_MapsUI( void ) {

    // ** Window

    const uint wndMapsOpts_width = TOOLBOX_ITEM_WIDTH;
    const uint wndMapsOpts_height = WIDGET_STANDARD_HEIGHT*20;

    wndMapsOpts = Window::NewWindow( WINDOW_MAPS_NAME.c_str(), "Maps", wndMapsOpts_width, wndMapsOpts_height, wndToolBox );
    wndMapsOpts->SetVisible( false );

    // ** Map Selector

    Sprite* sprite = new Label("Map:", TOOLBOX_ITEM_WIDTH-widget_v_spacing);
    wndMapsOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndMapsOpts->next_element_vpos,0.0f);

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    const int box_maps_numVisibleItmes = 15;
    const int box_maps_firstTabstopIndex = 1;
    dboxMapsBrowser = new DropDownList( box_maps_numVisibleItmes, box_maps_firstTabstopIndex, nullptr, toolbar_nested_item_width );
    wndMapsOpts->children->Add( dboxMapsBrowser );
    dboxMapsBrowser->SetOrigin(  widget_h_spacing, wndMapsOpts->next_element_vpos, 0.0f );

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + WIDGET_HALF_HEIGHT;

    sprite = new Button_Func( "Load Selected Map", &BtnLoadMap_Click, ButtonTypeT::None, wndMapsOpts_width - widget_h_spacing*2 );
    wndMapsOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndMapsOpts->next_element_vpos, 0.0f );

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + WIDGET_HALF_HEIGHT;

    sprite = new Button_Func( "Reload Current Map", &Dialog_ReloadMap, ButtonTypeT::None, wndMapsOpts_width - widget_h_spacing*2 );
    wndMapsOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndMapsOpts->next_element_vpos, 0.0f );

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + WIDGET_HALF_HEIGHT;

    sprite = new Button_Func( "New Map", &Dialog_NewMap, ButtonTypeT::None, sprite_cm_width );
    wndMapsOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndMapsOpts->next_element_vpos, 0.0f );

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + WIDGET_HALF_HEIGHT;

    sprite = new Button_Func( "Save Loaded Map", &Dialog_SaveLoadedMap, ButtonTypeT::None, wndMapsOpts_width - widget_h_spacing*2 );
    wndMapsOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndMapsOpts->next_element_vpos, 0.0f );

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + WIDGET_HALF_HEIGHT;

    sprite = new Button_Func( "Save Map As Selection", &BtnSaveMapAs_Click, ButtonTypeT::None, wndMapsOpts_width- widget_h_spacing*2 );
    wndMapsOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndMapsOpts->next_element_vpos, 0.0f );

    wndMapsOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

}

void Editor::Create_NavMeshUI( void ) {

    // ** Window

    const uint wndNavMeshOpts_width = TOOLBOX_ITEM_WIDTH;
    const uint wndNavMeshOpts_height = WIDGET_STANDARD_HEIGHT*20;

    wndNavMeshOpts = Window::NewWindow( WINDOW_NAVMESH_NAME.c_str(), "Nav Mesh", wndNavMeshOpts_width, wndNavMeshOpts_height, wndToolBox );
    wndNavMeshOpts->SetVisible( false );

    Sprite* sprite = new Button_Func( "Add NavMesh", &BtnAddNavMesh_Click, ButtonTypeT::None, toolbar_nested_item_width );
    wndNavMeshOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndNavMeshOpts->next_element_vpos, 0.0f );

    wndNavMeshOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    sprite = new Button_Func( "Delete Selected NavMesh", &BtnDelNavMesh_Click, ButtonTypeT::None, toolbar_nested_item_width );
    wndNavMeshOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndNavMeshOpts->next_element_vpos, 0.0f );

    wndNavMeshOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

}

void Editor::Create_WorldSizeUI( void ) {

    // ** Window

    const uint WorldSizeOpts_width = TOOLBOX_ITEM_WIDTH;
    const uint WorldSizeOpts_height = WIDGET_STANDARD_HEIGHT*20;

    wndWorldSizeOpts = Window::NewWindow( WINDOW_WORLDSIZE_NAME.c_str(), "World Size", WorldSizeOpts_width, WorldSizeOpts_height, wndToolBox );
    wndWorldSizeOpts->SetVisible( false );

    const uint button_width = WorldSizeOpts_width/3 - widget_h_spacing*2;

    Sprite* sprite = new Label("VoxelTree Array XYZ:", TOOLBOX_ITEM_WIDTH);
    wndWorldSizeOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndWorldSizeOpts->next_element_vpos,0.0f);

    wndWorldSizeOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT;

    worldSizeX = new TextBox( "", button_width );
    wndWorldSizeOpts->children->Add( worldSizeX );
    worldSizeX->SetOrigin(  widget_h_spacing, wndWorldSizeOpts->next_element_vpos, 0.0f );

    worldSizeY = new TextBox( "", button_width );
    wndWorldSizeOpts->children->Add( worldSizeY );
    worldSizeY->SetOrigin(  widget_h_spacing*3 + button_width, wndWorldSizeOpts->next_element_vpos, 0.0f );

    worldSizeZ = new TextBox( "", button_width );
    wndWorldSizeOpts->children->Add( worldSizeZ );
    worldSizeZ->SetOrigin(  widget_h_spacing*5 + button_width*2, wndWorldSizeOpts->next_element_vpos, 0.0f );

    wndWorldSizeOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT * 2;

    sprite = new Label("WARNING: Updating world size will delete any entity outside the new bounds.", TOOLBOX_ITEM_WIDTH, WIDGET_STANDARD_HEIGHT * 4 );
    wndWorldSizeOpts->children->Add( sprite );
    sprite->SetOrigin( widget_v_spacing,wndWorldSizeOpts->next_element_vpos,0.0f);

    wndWorldSizeOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT * 4;

    sprite = new Button_Func( "Set World Size", &BtnSetWorldSize_Click, ButtonTypeT::None, toolbar_nested_item_width );
    wndWorldSizeOpts->children->Add( sprite );
    sprite->SetOrigin(  widget_h_spacing, wndWorldSizeOpts->next_element_vpos, 0.0f );

    wndWorldSizeOpts->next_element_vpos += WIDGET_STANDARD_HEIGHT + WIDGET_HALF_HEIGHT;
}

void Editor::CreateUI( void ) {
    Create_ToolBoxUI();
    Create_AddModeUI();
    Create_SelectModeUI();
    Create_EffectsModeUI();
    Create_MapsUI();
    Create_NavMeshUI();
    Create_WorldSizeUI();

    PositionToolBoxUI();

    Create_ContextMenu();
    Create_ToolTip();
}

void Editor::PositionToolBoxUI( void ) {
    // I did my best here to lay out in code how these buttons are laid out in the game, visually.

    Vec3f org( widget_v_spacing, WIDGET_STANDARD_HEIGHT + 2, 0.0f);
    btnSelectMode->SetOrigin( org ); btnMapsMode->SetOrigin(  org.x + editmode_button_width, org.y, org.z );

    org.y+=WIDGET_STANDARD_HEIGHT;
    btnAddMode->SetOrigin( org ); btnNavMeshMode->SetOrigin(  org.x + editmode_button_width, org.y, org.z );

    org.y+=WIDGET_STANDARD_HEIGHT;
    btnEffectsMode->SetOrigin( org ); btnWorldSizeMode->SetOrigin(  org.x + editmode_button_width, org.y, org.z );

    org.x+=widget_v_spacing;
    org.y+=WIDGET_STANDARD_HEIGHT + 2;
    wndSelectOpts->SetOrigin( org );
    wndAddEntOpts->SetOrigin( org );
    wndEffectsOpts->SetOrigin( org );
    wndMapsOpts->SetOrigin( org );
    wndNavMeshOpts->SetOrigin( org );
    wndWorldSizeOpts->SetOrigin( org );

    wndToolBox->UpdateVBO();
}

void Editor::ShowContextMenu( void ) {
    Input::SetFocusWidget( nullptr );

    Vec3f uipos( Input::GetUIMousePos() );

    wndContextMenu->SetOrigin( uipos );
    wndContextMenu->SetVisible( true );
}

void Editor::CloseContextMenus( void ) {
    wndContextMenu->SetVisible( false );
}

void Editor::SliderUpdate( void ) {
    if ( selectedSlider )
        selectedSlider->DragUpdate();
    else
        SetMouseMode( MouseModeT::NONE );
}

void Editor::SetSelectedSlider( SliderBase* slider ) {
    selectedSlider = slider;

    if ( slider )
        SetMouseMode( MouseModeT::SLIDER );
    else if ( mouseMode == MouseModeT::SLIDER )
        SetMouseMode( MouseModeT::NONE );
}

void Editor::ShowUI( const bool whether ) {
    CloseContextMenus();
    wndToolBox->SetVisible( whether );
}

void Editor::Think_ToolTips( void ) {
    static const Widget* widgetWithTip = nullptr; // we will use this only for comparison, never dereference.

    const Uint32 cur_time = game->GetRenderTime();
    const Uint32 last_mouse_move_time = Input::GetLastMouseMoveTime();
    const Uint32 time_since_last_mouse_move = cur_time - last_mouse_move_time;

    if ( time_since_last_mouse_move < EDITOR_TOOLTIP_DELAY ) {

        const Widget* widgetUnderMouse = Input::GetWidgetUnderMouse();

        if ( widgetWithTip && widgetWithTip != widgetUnderMouse ) {

            // if the tooltip is what we're over, don't hide it
            if ( widgetUnderMouse != static_cast< Widget* >( lblToolTip ) ) {
                lblToolTip->SetVisible( false );
                widgetWithTip = nullptr;
            }
        }
        return;
    }

    // no need to check if we need to show a tooltip if one is already visible
    if ( lblToolTip->IsVisible() )
        return;

    widgetWithTip = CheckToolTip();
}

Widget* Editor::CheckToolTip( void ) {
    Widget* widget = Input::GetWidgetUnderMouse();
    if ( ! widget ) {
        lblToolTip->SetVisible( false );
        return nullptr;
    }

    std::string tooltip = widget->GetClassToolTip();
    if ( tooltip.size() == 0 ) {
        lblToolTip->SetVisible( false );
        return nullptr;
    }

    Vec3f tooltipPos( Input::GetUIMousePos() );
    tooltipPos.x += 2; // move the tooltip over so it doesn't absorb mouse events, like scroll wheel.
    tooltipPos.y += 2;
    lblToolTip->SetOrigin( tooltipPos );
    lblToolTip->SetLabel( tooltip.c_str() );
    lblToolTip->SetVisible( true );

    uint height = WIDGET_STANDARD_HEIGHT;
    uint width = static_cast< uint > (tooltip.size() * lblToolTip->GetFontSize() );

    if ( width > TOOLTIP_MAX_WIDTH ) {
        height *= 1 + width / TOOLTIP_MAX_WIDTH;
        width = TOOLTIP_MAX_WIDTH;
    }

    lblToolTip->SetWidth( width );
    lblToolTip->SetHeight( height );

    return widget;
}

#endif // MONTICELLO_EDITOR
