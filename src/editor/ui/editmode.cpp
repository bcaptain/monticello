// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./editmode.h"
#include "../editor.h"

const std::string EDIT_MENU_EDIT_ENTITIES("Edit Entities");
const std::string EDIT_MENU_ADD_ENTITIES("Add Entities");
const std::string EDIT_MENU_EFFECTS("Effects");

const std::string WINDOW_ENTADD_NAME("wndAddOpts");
const std::string WINDOW_EFFECTS_NAME("wndEffectsOpts");
const std::string WINDOW_ENTSELECT_NAME("wndSelectOpts");
const std::string WINDOW_ENTVALS_NAME("wndEntVals");
const std::string WINDOW_MAPS_NAME("wndMapsOpts");
const std::string WINDOW_NAVMESH_NAME("wndNavMeshOpts");
const std::string WINDOW_WORLDSIZE_NAME("wndWorldSizeOpts");

void Editor::SetMode( EditModeT newmode ) {

    // ** unset the mode
    btnSelectMode->SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );
    btnAddMode->SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );
    btnEffectsMode->SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );
    btnMapsMode->SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );
    btnNavMeshMode->SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );
    btnWorldSizeMode->SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );

    wndToolBox->ShowNamedElement( WINDOW_ENTSELECT_NAME, false );
    wndToolBox->ShowNamedElement( WINDOW_ENTADD_NAME, false );
    wndToolBox->ShowNamedElement( WINDOW_EFFECTS_NAME, false );
    wndToolBox->ShowNamedElement( WINDOW_MAPS_NAME, false );
    wndToolBox->ShowNamedElement( WINDOW_NAVMESH_NAME, false );
    wndToolBox->ShowNamedElement( WINDOW_WORLDSIZE_NAME, false );

    SetMouseMode( MouseModeT::NONE );
    editmode = newmode;

    switch ( editmode ) {
        case EditModeT::SELECT:
            SelectMode();
            break;
        case EditModeT::ADD:
            AddMode();
            break;
        case EditModeT::EFFECTS:
            EffectsMode();
            break;
        case EditModeT::MAPS:
            MapsMode();
            break;
        case EditModeT::NAVMESH:
            NavMeshMode();
            break;
        case EditModeT::WORLDSIZE:
            WorldSizeMode();
            break;
        case EditModeT::NONE: FALLTHROUGH;
        default: break;
    }
}

#endif // MONTICELLO_EDITOR
