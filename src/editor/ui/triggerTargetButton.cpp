// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./triggerTargetButton.h"
#include "../editor.h"
#include "../../rendering/renderer.h"

TriggerTargetButton::TriggerTargetButton( void )
    : pTarget()
{ }

TriggerTargetButton::TriggerTargetButton( const char* text, const std::weak_ptr< Entity >& target, const uint width, const uint height )
    : Button( text, ButtonTypeT::None, width, height )
    , pTarget( target )
{ }

TriggerTargetButton::~TriggerTargetButton( void ) {
}

void TriggerTargetButton::Click( void ) {
    Button::Click();

    if ( auto sptr = pTarget.lock() ) {
        renderer->camera.LookAt( sptr->GetOrigin() );
    } else {
        ERR("Couldn't find trigger target to center on - may have been deleted.\n");
    }
}

#endif // MONTICELLO_EDITOR
