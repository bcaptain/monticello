// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UI_EDITMODE_H_
#define SRC_EDITOR_UI_EDITMODE_H_

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"
#include "../../ui/ui.h"
#include "../../particles/particles.h"

extern const std::string FILE_MENU_SAVE_MAP;
extern const std::string FILE_MENU_LOAD_MAP;
extern const std::string FILE_MENU_SAVE_EFFECTS;
extern const std::string FILE_MENU_LOAD_EFFECTS;
extern const std::string FILE_MENU_QUIT;

extern const std::string WINDOW_ENTSELECT_NAME;
extern const std::string WINDOW_ENTVALS_NAME;
extern const std::string WINDOW_ENTADD_NAME;
extern const std::string WINDOW_EFFECTS_NAME;
extern const std::string WINDOW_PAINT_NAME;
extern const std::string WINDOW_MAPS_NAME;
extern const std::string WINDOW_NAVMESH_NAME;
extern const std::string WINDOW_WORLDSIZE_NAME;

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UI_EDITMODE_H_
