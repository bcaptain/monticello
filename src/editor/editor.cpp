// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./editor.h"

Editor* editor=nullptr;

#ifdef MONTICELLO_EDITOR

#include "../input/input.h"
#include "../game.h"
#include "../commandsys.h"
#include "../rendering/renderer.h"
#include "./ui/entvalTextBox.h"
#include "../entities/entityInfoManager.h"
#include "../ui/dialog/common.h"

const Uint32 EDITOR_TOOLTIP_DELAY = 1300_ms;
const float DEFAULT_OBJECT_SNAP_GRID_DISTANCE = 1.0f; // in meters/units

Editor::Editor( void )
    : editor_binds()
    , undo_history()
    , selection_origin()
    , selection_angle(0.0f)
    , click_pos()
    , drag_pos()
    , pan_scale( 0.35f )
    , zoom_scale( 3.0f )
    , scale_multiplier( 1.0f )
    , selectedSlider(nullptr)
    , editmode(EditModeT::SELECT)
    , mouseMode(MouseModeT::NONE)
    , mouseGrabType(MouseGrabT::NONE)
    , prev_tooltip_mouse_x(0)
    , prev_tooltip_mouse_y(0)
    , lblToolTip(nullptr)
    , wndToolBox(nullptr)
    , wndSelectOpts(nullptr)
    , wndEntVals(nullptr)
    , wndEntVals_listBox(nullptr)
    , wndBind(nullptr)
    , wndTargets(nullptr)
    , wndTriggerChain(nullptr)
    , wndFlats(nullptr)
    , txtFlatUVScaleVertical(nullptr)
    , txtFlatUVScaleHorizontal(nullptr)
    , txtFlatUVAlignVertical(nullptr)
    , txtFlatUVAlignHorizontal(nullptr)
    , wndEffectsOpts(nullptr)
    , particleValElements()
    , effectValElements()
    , wndAddEntOpts(nullptr)
    , wndContextMenu( nullptr )
    , btnSelectMode(nullptr)
    , btnAddMode(nullptr)
    , btnEffectsMode(nullptr)
    , btnMapsMode(nullptr)
    , btnNavMeshMode(nullptr)
    , btnWorldSizeMode(nullptr)
    , chkFaceCamera(nullptr)

    , flat_deform_indices()
    , selectedEntInfo(nullptr)
    , dboxModels(nullptr)
    , dboxMaterials(nullptr)
    , lblEntType(nullptr)
    , txtEntName(nullptr)
    , txtEntOriginX(nullptr)
    , txtEntOriginY(nullptr)
    , txtEntOriginZ(nullptr)
    , txtEntBBoxX(nullptr)
    , txtEntBBoxY(nullptr)
    , txtEntBBoxZ(nullptr)
    , txtEntBoundsOffsetX(nullptr)
    , txtEntBoundsOffsetY(nullptr)
    , txtEntBoundsOffsetZ(nullptr)
    , txtEntAngleX(nullptr)
    , txtEntAngleY(nullptr)
    , txtEntAngleZ(nullptr)
    , btnEntitysBind(nullptr)
    , selected_ents()
    , vbo_select_rect( new VBO<float>( 3, VBOChangeFrequencyT::FREQUENTLY ) )
    , vbo_select_rect_colors( new VBO<float>( 4, VBOChangeFrequencyT::FREQUENTLY ) )
    , vbo_grid( nullptr )
    , entityGrabActionStarted( false )

    // add mode
    , dboxEntBrowser(nullptr)
    , lblEntFileSelected(nullptr)
    , addSelectionOrigin()
    , addSelectionAngles()

    // maps mode
    , wndMapsOpts(nullptr)
    , dboxMapsBrowser(nullptr)

    // nav mesh
    , selected_nav_poly()
    , wndNavMeshOpts(nullptr)
    , navpoly_deform_indicies()
    , wndWorldSizeOpts(nullptr)
    , worldSizeX(nullptr)
    , worldSizeY(nullptr)
    , worldSizeZ(nullptr)

    // paint mode
//    , rkrPaintSize(nullptr)

    // effects mode
    , dboxEffects(nullptr)
    , dboxParticles(nullptr)
    , dboxParticleTex(nullptr)
    , sldRenderEffectData(nullptr)
    , sldParticleDelay(nullptr)
    , sldParticleCease(nullptr)
    , sldParticleQty(nullptr)
    , sldParticleLifeSpan(nullptr)
    , sldParticleBunching(nullptr)
    , sldParticleRotateFrom(nullptr)
    , sldParticleRotateTo(nullptr)
    , sldParticleScaleTo(nullptr)
    , sldParticleScaleFrom(nullptr)
    , sldParticleOpacityFrom(nullptr)
    , sldParticleOpacityMid(nullptr)
    , sldParticleOpacityTo(nullptr)
    , txtOffsetX(nullptr)
    , txtOffsetY(nullptr)
    , txtOffsetZ(nullptr)
    , txtOriginDistribXY(nullptr)
    , txtOriginDistribZ(nullptr)
    , txtDestX(nullptr)
    , txtDestY(nullptr)
    , txtDestZ(nullptr)
    , txtDestDistribXY(nullptr)
    , txtDestDistribZ(nullptr)
    , txtSetDest(nullptr)
    , txtDest2X(nullptr)
    , txtDest2Y(nullptr)
    , txtDest2Z(nullptr)
    , txtDest2DistribXY(nullptr)
    , txtDest2DistribZ(nullptr)
    , txtSetDest2(nullptr)
    , btnToggleParticleRandInitialAngle(nullptr)
    , btnToggleParticleDest(nullptr)
    , btnToggleParticleDest2(nullptr)
    , txtParticleColorX(nullptr)
    , txtParticleColorY(nullptr)
    , txtParticleColorZ(nullptr)
    , sldParticleColorWeight(nullptr)
    , txtParticleColorToX(nullptr)
    , txtParticleColorToY(nullptr)
    , txtParticleColorToZ(nullptr)
    , sldParticleColorToWeight(nullptr)
    , effectPreviewStartTime(0)

    , selectedEffect(nullptr)
    , selectedParticleLink(nullptr)
    , snapping( false )
    , ghostSelectionModelInfo()
    , testAnimModelInfo()
    {

    entityInfoManager.LoadAllEntValToolTips();

    SetEditorInputDefaults();
    CreateUI();

    RepopulateEntityInfoDropBox();
    RepopulateMapsDropBox();
    RepopulateModelsDropBox();
    RepopulateParticleTexDropBox();
    RepopulateEffectsDropBox();

    editor = this;
    
    CommandSys::EditorIsReady();
}

Editor::~Editor( void ) {
    delete vbo_select_rect;
    delete vbo_select_rect_colors;
    delete vbo_grid;
    // window, label, button, etc pointers - their data is released by their containing windows
}

void Editor::Think( void ) {
    Think_GhostSelection();
    Think_TestAnimModel();
    Think_ToolTips();
    Think_Lights();
}

void Editor::Think_Lights( void ) {
    for ( uint i=0; i<renderer->pointLights.size(); ++i ) {
        auto light = renderer->pointLights[i].light.lock();
        if ( ! light )
            continue;
        light->Editor_Think();
    }
}

void Editor::PackVBO( void ) {
    vbo_packGridLines();
    // selection rect is packed in RectangleSelect_Drag
}

void Editor::Draw( void ) {
    Draw_EffectMode();
    Draw_NavMeshMode();
    Draw_SelectMode();

    const bool draw_grid = globalVals.GetBool( gval_d_grid, true );
    if ( draw_grid )
        Draw_Grid();

    const bool draw_ghost = globalVals.GetBool( gval_d_ghost, true );
    if ( draw_ghost )
        Draw_GhostSelection();

    const bool draw_testAnimation = globalVals.GetBool( gval_d_testAnim );
    if (draw_testAnimation) {
        Think_TestAnimModel();
    }
        

    Draw_SelectionRect();
}

void Editor::Update_Grid( void ) {
    DELNULL( vbo_grid );
}

void Editor::Draw_Grid( void ) const {
    if ( !vbo_grid )
        return;

    if ( !vbo_grid->Finalized() )
        return;

    float grid_opacity = globalVals.GetFloat( gval_d_gridOpacity );
    const float DEFAULT_GRID_OPACITY = 0.08f;

    const Color4f color( 1,1,0, grid_opacity > 0 ? grid_opacity : DEFAULT_GRID_OPACITY );

    shaders->UseProg(  GLPROG_MINIMAL  );
    shaders->SendData_Matrices();
    shaders->SetUniform4f("vColor", color );    
    shaders->SetAttrib("vPos", *vbo_grid );
    shaders->DrawArrays( GL_LINES, 0, vbo_grid->Num() );
}

void Editor::vbo_packGridLines( void ) {
    if ( !vbo_grid )
        vbo_grid = new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY);

    if ( vbo_grid->Finalized() )
        return;

    const Vec3f size = globalVals.GetVec3f( gval_g_octreeSize ) / 2;

    for ( int i=-static_cast<int>(size.y); i<size.y; ++i ) {
        vbo_grid->Pack( Vec3f(-size.x,i,0) );
        vbo_grid->Pack( Vec3f(size.x,i,0) );
    }

    for ( int i=-static_cast<int>(size.x); i<size.x; ++i ) {
        vbo_grid->Pack( Vec3f(i,-size.y,0) );
        vbo_grid->Pack( Vec3f(i,size.y,0) );
    }

    vbo_grid->MoveToVideoCard();
}

void Editor::DoScenePan( void ) {
    if (Input::IsKeyDown(SDLK_LSHIFT)) {
        scale_multiplier = 2.0f;
    } else if ( Input::IsKeyDown(SDLK_LALT)) {
        scale_multiplier = 0.15f;
    }
    Vec3f amt( Input::GetWindowMousePos() );
    amt.x = ( click_pos.x - amt.x ) * (pan_scale * scale_multiplier);
    amt.y = - (click_pos.y - amt.y ) * (pan_scale * scale_multiplier);
    renderer->camera.NudgeFocusOrigin( amt );
    
    scale_multiplier = 1.0f;

    sdl.WarpMouse( static_cast<Uint16>(click_pos.x), static_cast<Uint16>(click_pos.y) );
}

void Editor::DoSceneZoom( const float zoomScale ) {
    if (Input::IsKeyDown(SDLK_LSHIFT)) {
        scale_multiplier = 2.0f;
    } else if ( Input::IsKeyDown(SDLK_LALT)) {
        scale_multiplier = 0.15f;
    }
    renderer->camera.NudgeZoom( zoomScale * scale_multiplier );
    
    scale_multiplier = 1.0f;
}

void Editor::DoSceneRot( void ) {
    Vec3f amt( Input::GetWindowMousePos() );
    amt.x = click_pos.x - amt.x;
    amt.y = click_pos.y - amt.y;

    renderer->camera.Rotate( amt.x );
    renderer->camera.Tilt( amt.y );

    sdl.WarpMouse( static_cast<Uint16>(click_pos.x), static_cast<Uint16>(click_pos.y) );
}

bool Editor::EvalButtonReleased( const std::string& action ) {
    switch ( mouseMode ) {
        case MouseModeT::SLIDER:
            if ( selectedSlider ) {
                if ( ! ( Input::GetMouseButtonState( MouseButtonT::MOUSE_BUTTON_LEFT ) ) ) { // left mouse raise
                    selectedSlider = nullptr;
                    SetMouseMode( MouseModeT::NONE );
                    return true;
                }
            }
            break;
        case MouseModeT::ENTITY_GRABBED:
            if ( action == "SelectEnt" || action == "DragEntZ" ) {
                ReleaseEntity();
                return true;
            }
            break;
        case MouseModeT::SCENEPAN: 
            if ( action == "ScenePan" ) {
                SetMouseMode( MouseModeT::NONE );
                return true;
            }
            break;
        case MouseModeT::SCENEROTATE:
            if ( action == "SceneRot" ) {
                SetMouseMode( MouseModeT::NONE );
                return true;
            }
            break;
        case MouseModeT::RECTANGLE_SELECT:
            if ( action == "SelectEntRect" ) {
                RectangleSelect();
                return true;
            }
            break;
        case MouseModeT::NAVPOLY_GRABBED:
            if ( mouseGrabType == MouseGrabT::DEFORM_NAVPOLY ) {
                if ( action == "NavPolyDeform" ) {
                    DoneDeformNavPoly();
                    return true;
                }
            }
            break;
        case MouseModeT::NONE: break;
        case MouseModeT::MENU: break;
        case MouseModeT::BLEND: break;
        case MouseModeT::ADD_TRIGGER_TARGET: break;
        case MouseModeT::ADD_TRIGGER_CHAIN: break;
        case MouseModeT::BIND_TO_MASTER: break;
        default: break;
    }
    
    return false;
}

bool Editor::GetSnap( void ) const {
    return snapping != Input::IsKeyDown( globalVals.GetString( "key_snap" ) ); // LCTRL will invert the state of snapping
}

float Editor::GetSnapGridDistance( void ) const {
    return globalVals.GetFloat( gval_e_snapGridDistance, DEFAULT_OBJECT_SNAP_GRID_DISTANCE );
}

bool Editor::CheckInputEvent_Editor( const std::string& action ) {
    if ( action == "ToggleTools" ) {
        if ( wndToolBox ) {
            wndToolBox->ToggleCollapsed();
            return true;
        }
    }

    if ( action == "Undo" ) {
        undo_history.UndoLastAction();
        return true;
    }

    if ( action == "Redo" ) {
        undo_history.RedoLastAction();
        return true;
    }

    if ( action == "ContextMenu" ) {
        if ( RightClickSelectEntity() )
            return true;
    }

    if ( action == "ZoomIn" ) {
        DoSceneZoom( zoom_scale );
        return true;
    }

    if ( action == "ZoomOut" ) {
        DoSceneZoom( -zoom_scale );
        return true;
    }

    if ( action == "ToggleSnap" ) {
        editor->ToggleSnap();
        return true;
    }

    if ( action == "ScenePan" ) {
        SetMouseMode( MouseModeT::SCENEPAN );
        click_pos = Input::GetWindowMousePos();
        return true;
    }

    if ( action == "SceneRot" ) {
        SetMouseMode( MouseModeT::SCENEROTATE );
        click_pos = Input::GetWindowMousePos();
        return true;
    }

    if ( action == "ReloadMap" ) {
        Dialog_ReloadMap();
        return true;
    }

    if ( action == "SaveLoadedMap" ) {
        Dialog_SaveLoadedMap();
        return true;
    }

    if ( action == "NextFrame" ) {
        game->NextFrame();
        return true;
    }

    if ( action == "SnapCameraTopDown" ) {
        renderer->camera.SetTilt(0);
        return true;
    }

    return false;
}

MouseModeT Editor::GetMouseMode( void ) const {
    return mouseMode;
}

void Editor::UnsetMode( void ) {
    SetMode( EditModeT::NONE );
}

EditModeT Editor::GetMode( void ) const {
    return editmode;
}

void Editor::SetSnap( const bool whether ) {
    snapping = whether;
}

void Editor::ToggleSnap( void ) {
    snapping = !snapping;
}

bool Editor::IsContextMenuOpen( void ) const {
    return wndContextMenu->IsVisible();
}

float Editor::GetPanScale( void ) const {
    return pan_scale;
}

float Editor::GetZoomScale( void ) const {
    return zoom_scale;
}

void Editor::SetPanScale( const float to ) {
    pan_scale = to;
}

void Editor::SetZoomScale( const float to ) {
    zoom_scale = to;
}

void Editor::SetMouseMode( const MouseModeT mode ) {
    constexpr const bool debugMouseMode = false;
    if ( debugMouseMode ) {
        std::string themode;
        switch ( mode ) {
            case MouseModeT::NONE: themode="none"; break;
            case MouseModeT::ENTITY_GRABBED: themode="entgrab"; break;
            case MouseModeT::NAVPOLY_GRABBED: themode="navgrab"; break;
            case MouseModeT::SCENEPAN: themode="pan"; break;
            case MouseModeT::MENU: themode="menu"; break;
            case MouseModeT::BLEND: themode="blend"; break;
            case MouseModeT::SLIDER: themode="slide"; break;
            case MouseModeT::SCENEROTATE: themode="rot"; break;
            case MouseModeT::ADD_TRIGGER_TARGET: themode="targ"; break;
            case MouseModeT::ADD_TRIGGER_CHAIN: themode="chain"; break;
            case MouseModeT::BIND_TO_MASTER: themode="master"; break;
            case MouseModeT::RECTANGLE_SELECT: themode="rect"; break;
            default: themode="UNKNOWN"; break;
        }

        Printf("MouseMode: %s \n", themode.c_str() );
    }
    
    mouseMode = mode;
}

#endif // MONTICELLO_EDITOR
