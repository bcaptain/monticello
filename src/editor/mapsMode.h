// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_EDITOR_MAPSMODE_H
#define SRC_EDITOR_MAPSMODE_H

#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./clib/src/macros.h"
#include "./clib/src/types.h"
#include "./clib/src/std.h"
#include "./clib/src/strings.h"
#include "./editor.h"

#endif // MONTICELLO_EDITOR

#endif // SRC_EDITOR_MAPSMODE_H
