// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UNDO_H
#define SRC_EDITOR_UNDO_H

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"

class UndoAction;

/*

UndoHistory

We add items to the undo history by calling, for example:
new Undo_EntityAdd( undo_history_ref, entity_ref ); // The UndoHistory will manage the new Undo_EntityAdd object.

*/

class UndoHistory {
    friend class UndoAttorney;

public:
    UndoHistory( void );
    UndoHistory( const UndoHistory& other ) = delete;
    ~UndoHistory( void );

public:
    UndoHistory& operator=( const UndoHistory& other ) = delete;
    
public:
    void Clear( void );
    void UndoLastAction( void );
    void RedoLastAction( void );

private:
    void Add( UndoAction* action ); //!< UndoHistory object will own the UndoAction given. Will clear the redo_list
    void UpdateAllEntityObjects( const UndoAction* originator, std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ); //!< updates any std::shared_ptr inside this class and it's members
    void UpdateAllEntityObjects( const UndoAction* originator, LinkList< UndoAction* >& history, std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent );
    void ClearList( LinkList< UndoAction* >& list );
    void UpdateAllEntityObjectsInHistory( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent );

private:
    LinkList< UndoAction* > undo_items;
    LinkList< UndoAction* > redo_items;
};

/*!

UndoAction \n\n

Base class for undo actions

**/

class UndoAction {
    friend class UndoAttorney;

public:
    UndoAction( UndoHistory& _owner );
    virtual ~UndoAction( void );
    UndoAction( const UndoAction& other ) = default;

public:
    UndoAction& operator=( const UndoAction& other ) = default;

public:
    void Connect( const UndoAction& other ); // connect to another UndoAction adjacent in the undo/redo lists to make batch undo/redo (such as on groups of entities, eg undo/redo of deletion of several entities at once)
    bool IsSameBatchAs( const UndoAction& other ) const;

protected:
    virtual bool PerformUndo( void ) = 0;
    virtual bool PerformRedo( void ) = 0;
    void UpdateAllEntityObjectsInHistory( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) const;
    
private:
    UndoHistory* owner; //will not be null
    Uint32 batch_id;
    static uint next_undo_id;
};

inline void UndoAction::Connect( const UndoAction& other ) {
    batch_id = other.batch_id;
}

inline bool UndoAction::IsSameBatchAs( const UndoAction& other ) const {
    return batch_id == other.batch_id;
}

/*!

Undo_EntityAction \n\n

An action that targets an entity (or many)

**/

class Undo_EntityAction : public UndoAction {
    friend class UndoHistory;

public:
    Undo_EntityAction( UndoHistory& _owner );
    ~Undo_EntityAction( void ) override;
    Undo_EntityAction( const Undo_EntityAction& other ) = default;

public:
    Undo_EntityAction& operator=( const Undo_EntityAction& other ) = default;

protected:
    virtual void UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) = 0;
};

class UndoAttorney {
private:
    friend class UndoHistory;
    friend class UndoAction;

    static void UpdateAllEntityObjectsInHistory( UndoHistory& history, const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
        history.UpdateAllEntityObjectsInHistory( old_ent, new_ent );
    }
    
    static bool PerformRedo( UndoAction& action ) {
        return action.PerformRedo();
    }

    static bool PerformUndo( UndoAction& action ) {
        return action.PerformUndo();
    }
    
    static void AddUndo( UndoHistory& history, UndoAction* action ) {
        history.Add( action );
    }
};

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UNDO_H
