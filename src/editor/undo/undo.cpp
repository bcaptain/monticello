// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./undo.h"

// **
//
// UndoHistory
//
// The undo system keeps a strong reference to entities that are not in the world.
// If entities are in the world, it keeps a weak reference to them,
// so if the entity gets deleted during gameplay the undo system will just
// forget that it exists, notify the user when he does an undo/redo the first time,
// and the next undo/redo in the history will be attempted next time.
//
// **

UndoHistory::UndoHistory( void )
    : undo_items()
    , redo_items()
{ }

void UndoHistory::RedoLastAction( void ) {
    bool ret = true;
    
    UndoAction* prev = nullptr;
    UndoAction* cur = nullptr;
    
    do {
        Link< UndoAction* > *link = redo_items.GetLast();
        if ( ! link ) {
            CMSG_LOG("Redo: nothing to redo.\n");
            break;
        }
        
        if ( cur )
            prev = cur;
        cur = link->Data();
        
        if ( prev && !prev->IsSameBatchAs( *cur ) )
            break;
        
        if ( ! UndoAttorney::PerformRedo( *cur ) )
            ret = false;
            
        undo_items.Append( cur );
        link->Del();

    } while ( true );
    
    if ( !ret ) {
        undo_items.clear();
    }
}

void UndoHistory::UndoLastAction( void ) {
    bool ret = true;
    
    UndoAction* prev = nullptr;
    UndoAction* cur = nullptr;
    
    do {
        Link< UndoAction* > *link = undo_items.GetLast();
        if ( ! link ) {
            CMSG_LOG("Redo: nothing to undo.\n");
            break;
        }
        
        if ( cur ) {
            prev = cur;
        }
        cur = link->Data();
        
        if ( prev && !prev->IsSameBatchAs( *cur ) )
            break;
        
        if ( ! UndoAttorney::PerformUndo( *cur ) )
            ret = false;
            
        redo_items.Append( cur );
        link->Del();

    } while ( true );
    
    if ( !ret ) {
        undo_items.clear();
    }
}

UndoHistory::~UndoHistory( void ) {
    Clear();
}

void UndoHistory::Add( UndoAction* action ) {
    ASSERT( action );
    undo_items.Append( action );
    ClearList( redo_items );
}

void UndoHistory::ClearList( LinkList< UndoAction* >& list ) {
    Link< UndoAction* > *link = list.GetFirst();
    while ( link ) {
        delete link->Data();
        link = link->Del();
    }
}

void UndoHistory::UpdateAllEntityObjects( const UndoAction* originator, std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
    UpdateAllEntityObjects( originator, undo_items, old_ent, new_ent );
    UpdateAllEntityObjects( originator, redo_items, old_ent, new_ent );
}

void UndoHistory::UpdateAllEntityObjects( const UndoAction* originator, LinkList< UndoAction* >& history, std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
    Link< UndoAction* > *link = history.GetLast();
    
    while ( link ) {
        Undo_EntityAction *action = dynamic_cast< Undo_EntityAction* >( link->Data() );
        ASSERT( action );
        if ( action == originator )
            continue;
        action->UpdateEntityObject( old_ent, new_ent );
        link = link->GetPrevious();
    }
}

void UndoHistory::UpdateAllEntityObjectsInHistory( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
    auto UpdateAllEntityObjectsInHistory_Helper=[&]( Link< UndoAction* > *link ) {
        while ( link ) {
            // utilizing dynamic_cast for dynamic type checking
            Undo_EntityAction *ent_action = dynamic_cast< Undo_EntityAction* >( link->Data() );

            if ( ent_action )
                ent_action->UpdateEntityObject( old_ent, new_ent );
            link = link->GetNext();
        }
    };

    UpdateAllEntityObjectsInHistory_Helper( undo_items.GetFirst() );
    UpdateAllEntityObjectsInHistory_Helper( redo_items.GetFirst() );
}

void UndoHistory::Clear( void ) {
    ClearList( undo_items );
    ClearList( redo_items );
}

uint UndoAction::next_undo_id( 0 );

UndoAction::UndoAction( UndoHistory& _owner )
    : owner( &_owner )
    , batch_id( next_undo_id++ )
{
    UndoAttorney::AddUndo( _owner, this );
}

UndoAction::~UndoAction( void ) {
}

void UndoAction::UpdateAllEntityObjectsInHistory( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) const {
    UndoAttorney::UpdateAllEntityObjectsInHistory( *owner, old_ent, new_ent );
}

Undo_EntityAction::Undo_EntityAction( UndoHistory& _owner )
    : UndoAction( _owner )
{ }

Undo_EntityAction::~Undo_EntityAction( void ) {
}

#endif // MONTICELLO_EDITOR
