// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UNDO_UNDO_ENTITYPROPERTY
#define SRC_EDITOR_UNDO_UNDO_ENTITYPROPERTY

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"
#include "./undo.h"

class Entity;

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
class Undo_EntityProperty : public Undo_EntityAction {
public:
    Undo_EntityProperty( UndoHistory& _owner, Entity& ent, SET_METHOD _setter, GET_METHOD _getter, VALUE_TYPE _old_value );
    Undo_EntityProperty( UndoHistory& _owner, const std::weak_ptr< Entity >& ent, SET_METHOD setter, GET_METHOD _getter, VALUE_TYPE _old_value );
    Undo_EntityProperty( void ) = delete;
    ~Undo_EntityProperty( void ) override {};
    Undo_EntityProperty( const Undo_EntityProperty& other ) = default;

public:
    Undo_EntityProperty& operator=( const Undo_EntityProperty& other ) = default;

private:
    void UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) override;
    bool PerformUndo( void ) override;
    bool PerformRedo( void ) override;
    
private:
    bool SetValue( VALUE_TYPE to_val );

private:
    std::weak_ptr< Entity > entity;
    SET_METHOD setter;
    GET_METHOD getter;
    VALUE_TYPE old_value;
};

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
Undo_EntityProperty<SET_METHOD,GET_METHOD,VALUE_TYPE>::Undo_EntityProperty( UndoHistory& _owner, Entity& _ent, SET_METHOD _setter, GET_METHOD _getter, VALUE_TYPE _old_value )
    : Undo_EntityAction( _owner )
    , entity( _ent.GetWeakPtr() )
    , setter( _setter )
    , getter( _getter )
    , old_value( _old_value )
{
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p: Undo_EntityProperty:Undo_EntityProperty: %p.\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<const std::intptr_t>(&_ent) );
}

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
Undo_EntityProperty<SET_METHOD,GET_METHOD,VALUE_TYPE>::Undo_EntityProperty( UndoHistory& _owner, const std::weak_ptr< Entity >& _ent, SET_METHOD _setter, GET_METHOD _getter, VALUE_TYPE _old_value )
    : Undo_EntityAction( _owner )
    , entity( _ent )
    , setter( _setter )
    , getter( _getter )
    , old_value( _old_value )
{
    auto ent = entity.lock();
    if ( ent )
        ERR("Create Undo: Property Entity: Entity does not exist.\n");
    
    if constexpr ( undoDebug )    
        CMSG_LOG("Undo-Debug: %p: Undo_EntityProperty:Undo_EntityProperty: %p.\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<const std::intptr_t>(&ent) );
}

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
bool Undo_EntityProperty<SET_METHOD,GET_METHOD,VALUE_TYPE>::SetValue( VALUE_TYPE to_val ) {
    auto object = entity.lock();
    if ( !object ) {
        ERR("Undo/Redo: Property Entity: Entity no longer exists, may have left the world during gameplay.\n");
        return false;
    }
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityProperty: Setting property to ent %p.\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<const std::intptr_t>(object.get()) );
    old_value = ((*object).*getter)();
    ((*object).*setter)(to_val);
    return true;
}

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
bool Undo_EntityProperty<SET_METHOD,GET_METHOD,VALUE_TYPE>::PerformUndo( void ) {
    return SetValue( old_value );
}

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
bool Undo_EntityProperty<SET_METHOD,GET_METHOD,VALUE_TYPE>::PerformRedo( void ) {
    return SetValue( old_value );
}

template< typename SET_METHOD, typename GET_METHOD, typename VALUE_TYPE >
void Undo_EntityProperty<SET_METHOD,GET_METHOD,VALUE_TYPE>::UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
    
    auto ent = entity.lock();
    if ( ent ) {
        if ( ent == old_ent ) {
            if constexpr ( undoDebug )
                CMSG_LOG("Undo-Debug: %p Undo_EntityProperty:UpdateEntityObject: the backup changed from %p to %p\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<const std::intptr_t>(ent.get()), reinterpret_cast<const std::intptr_t>(new_ent.get()) );
            entity = new_ent;
        }
        return;
    }
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: Undo_EntityProperty:UpdateEntityObject: No entity managed - Likely the added entity left the world.\n" );
}

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UNDO_UNDO_ENTITYPROPERTY
