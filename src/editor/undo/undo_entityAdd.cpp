// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./undo_entityAdd.h"
#include "../../entities/entity.h"
#include "../../game.h"

//
//
// Undo_EntityAdd
//
//

Undo_EntityAdd::Undo_EntityAdd( UndoHistory& _owner, const std::weak_ptr< Entity >& weakPtr )
    : Undo_EntityAction( _owner )
    , added_entity( weakPtr )
    , backup_entity( nullptr ) // gets assigned during the undo
{
    auto ent = weakPtr.lock();
    if ( ent ) {
        ERR("Create Undo: Add Entity: Entity does not exist.\n");
        return;
    }
        
    CMSG_LOG("Undo-Debug: %p: Undo_EntityAdd:Undo_EntityAdd: %p.\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<std::intptr_t>(ent.get()) );
}

Undo_EntityAdd::Undo_EntityAdd( UndoHistory& _owner, Entity& ent )
    : Undo_EntityAction( _owner )
    , added_entity( ent.GetWeakPtr() )
    , backup_entity( nullptr ) // gets assigned during the undo
{
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p: Undo_EntityAdd:Undo_EntityAdd: %p.\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<const std::intptr_t>(&ent) );
}

bool Undo_EntityAdd::PerformUndo( void ) {
    auto addent = added_entity.lock();
    if ( !addent ) {
        ERR("Undo: Add Entity: Entity no longer exists, may have left the world during gameplay.\n");
        return false;;
    }
    
    CMSG_LOG("Undo: Add Entity.\n");
    ASSERT( ! backup_entity );
    backup_entity = addent->CloneShared();
    ASSERT( backup_entity );
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityAdd: Removing ent %p backed up to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>( addent.get()), reinterpret_cast<std::intptr_t>( backup_entity.get()) );
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityAdd: Updating: %p to           %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>( addent.get()), reinterpret_cast<std::intptr_t>( backup_entity.get()) );
    UpdateAllEntityObjectsInHistory( addent, backup_entity );
    game->MarkEntityForDeletion( addent );
    added_entity.reset();
    
    return true;
}

bool Undo_EntityAdd::PerformRedo( void ) {
    CMSG_LOG("Redo: Add Entity.\n");
    ASSERT( backup_entity );
    ASSERT( added_entity.expired() );

    auto newEnt = backup_entity->CloneShared();
    game->InsertEntity( newEnt );
    newEnt->Spawn();
    newEnt->UpdateVBO();
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityAdd: Spawned ent %p from %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(newEnt.get()), reinterpret_cast<std::intptr_t>(backup_entity.get()) );
    
    auto lock = backup_entity; // hold onto this entity to keep it locked until we're done updating everything
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityAdd: Updating ent %p to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(backup_entity.get()), reinterpret_cast<std::intptr_t>(newEnt.get()) );
    UpdateAllEntityObjectsInHistory( backup_entity, newEnt );
    
    backup_entity.reset();
    added_entity = newEnt;
    
    return true;
}

Undo_EntityAdd::~Undo_EntityAdd( void ) {
}

void Undo_EntityAdd::UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
    ASSERT( new_ent );
    ASSERT( old_ent );
    
    if ( backup_entity ) {
        if ( backup_entity == old_ent ) {
            if constexpr ( undoDebug )
                CMSG_LOG("Undo-Debug: %p Undo_EntityAdd:UpdateEntityObject: the backup changed from %p to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(backup_entity.get()), reinterpret_cast<std::intptr_t>(new_ent.get()) );
            backup_entity = new_ent;
        }
        return;
    }
        
    if ( auto addent = added_entity.lock() ) {
        if ( addent == old_ent ) {
            if constexpr ( undoDebug )
                CMSG_LOG("Undo-Debug: %p Undo_EntityAdd:UpdateEntityObject: The added changed from %p to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(addent.get()), reinterpret_cast<std::intptr_t>(new_ent.get()) );
            added_entity = new_ent;
        }
        return;
    }
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: Undo_EntityAdd:UpdateEntityObject: No entity managed - Likely the added entity left the world.\n" );
}

#endif // MONTICELLO_EDITOR
