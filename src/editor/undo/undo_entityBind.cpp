// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./undo_entityBind.h"
#include "../../entities/entity.h"
#include "../editor.h"
//
////
////
//// Undo_EntityBind::UndoData_EntityBind
////
////
//
//Undo_EntityBind::UndoData_EntityBind::UndoData_EntityBind( void )
//    : master()
//{ }
//
////
////
//// Undo_EntityBind
////
////
//
//Undo_EntityBind::Undo_EntityBind( UndoHistory& _owner, LinkList< std::weak_ptr< Entity> >& entity_selections )
//{
//    Initialize( entity_selections );
//}
//
//Undo_EntityBind::Undo_EntityBind( UndoHistory& _owner, Entity& entity )
//{
//    Initialize( entity );
//}
//
//Undo_EntityBind::~Undo_EntityBind( void ) {
//}
//
//bool Undo_EntityBind::PerformUndo( void ) {
//    CMSG_LOG("Undo: Bind/Unbind Entity.\n");
//    return false; //todo
//}
//
//bool Undo_EntityBind::PerformRedo( void ) {
//    CMSG_LOG("Redo: Bind/Unbind Entity.\n");
//    return false; //todo
//}
//
//void Undo_EntityBind::BackupData( Entity& ent, UndoData_Base *& data ) {
//    ASSERT( data == nullptr );
//    UndoData_EntityBind *cdata = new UndoData_EntityBind;
//    data = cdata;
//
//    if ( auto sptr = ent.GetBindMaster() ) {
//        cdata->master = sptr;
//    }
//}
//
//bool Undo_EntityBind::Redo( std::weak_ptr< Entity >& entp_smp, UndoData_Base*& data ) {
//    return SwapBinds( entp_smp, data );
//}
//
//bool Undo_EntityBind::Undo( std::weak_ptr< Entity >& entp_smp, UndoData_Base*& data ) {
//    return SwapBinds( entp_smp, data );
//}
//
//bool Undo_EntityBind::SwapBinds( std::weak_ptr< Entity >& entp_smp, UndoData_Base* data ) {
//    ASSERT( data );
//    auto slave = entp_smp.lock();
//    ASSERT( !entp_smp.expired() );
//
//    if ( ! slave ) {
//        ERR("Undo_EntityBind: Slave no longer exists.\n");
//        return false;
//    }
//
//    // ** store the old master into a temp
//    UndoData_EntityBind* cdata = static_cast< UndoData_EntityBind* >( data );
//    std::weak_ptr< Entity > tmp = cdata->master;
//
//    // ** backup the current master
//    if ( auto current_master = slave->GetBindMaster() ) {
//        cdata->master = current_master;
//    } else {
//        cdata->master.reset();
//    }
//
//    // ** bind to the old master (from temp)
//    if ( auto sptr = tmp.lock() ) {
//        slave->BindToMaster( *sptr );
//    } else {
//        slave->UnbindFromMaster();
//    }
//
//    editor->UpdateEntSelectionInfo_Bind(); // update editor UI
//    return true;
//}
//
//void Undo_EntityBind::UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
//    ASSERT( old_ent );
//    ASSERT( new_ent );
//
//
//    // ** update our record of world entities
//
//    for ( uint i=0; i<data_list.size(); ++i ) {
//        ASSERT( data_list[i] );
//        UndoData_EntityBind *cdata = static_cast< UndoData_EntityBind* >( data_list[i] );
//        ASSERT( cdata );
//
//        auto old_end = cdata->master.lock();
//        if ( old_ent == old_ent ) {
//            CMSG_LOG("Undo-Debug: %p Undo_EntityDelete:UpdateEntityObject: backup changed from %x to %x\n", this, old_ent.get(), new_ent.get() );
//
//            cdata->master = new_ent;
//        }
//    }
//
//    editor->UpdateEntSelectionInfo_All(); // update editor UI
//}

#endif // MONTICELLO_EDITOR
