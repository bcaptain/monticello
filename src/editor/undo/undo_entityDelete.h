// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UNDO_UNDO_ENTITYDEL
#define SRC_EDITOR_UNDO_UNDO_ENTITYDEL

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"
#include "./undo.h"

class Entity;

/*!

Undo_EntityDelete \n\n

This undo class doesn't need to do anything special with the entities like Undo_EntityDelete does, since an entity will always be "added" to a map without any preconditions (binds, targets, etc)

**/

class Undo_EntityDelete : public Undo_EntityAction {
public:
    Undo_EntityDelete( UndoHistory& _owner, Entity& ent );
    Undo_EntityDelete( UndoHistory& _owner, const std::weak_ptr< Entity >& entp_smp_to );
    Undo_EntityDelete( void ) = delete;
    ~Undo_EntityDelete( void ) override;
    Undo_EntityDelete( const Undo_EntityDelete& other ) = default;

public:
    Undo_EntityDelete& operator=( const Undo_EntityDelete& other ) = default;

private:
    void BackupEntity( const std::shared_ptr< Entity >& ent );
    void UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) override;
    bool PerformUndo( void ) override;
    bool PerformRedo( void ) override;

private:
    std::weak_ptr< Entity > undeleted_entity;
    std::shared_ptr< Entity > backup_entity;
};

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UNDO_UNDO_ENTITYDEL
