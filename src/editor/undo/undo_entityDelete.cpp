// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./undo_entityDelete.h"
#include "../../entities/entity.h"
#include "../../game.h"

Undo_EntityDelete::Undo_EntityDelete( UndoHistory& _owner, const std::weak_ptr< Entity >& weakPtr )
    : Undo_EntityAction( _owner )
    , undeleted_entity( weakPtr )
    , backup_entity( nullptr ) // gets assigned during the undo
{

    auto ent = weakPtr.lock();
    if ( ent ) {
        ERR("Create Undo: Delete Entity: Entity does not exist.\n");
        return;
    }
        
    CMSG_LOG("Undo-Debug: %p: Undo_EntityDelete:Undo_EntityDelete: %p.\n",
        reinterpret_cast<const std::intptr_t>(this),
        reinterpret_cast<const std::intptr_t>(ent.get()) );
    BackupEntity( undeleted_entity.lock() );
}

Undo_EntityDelete::Undo_EntityDelete( UndoHistory& _owner, Entity& ent )
    : Undo_EntityAction( _owner )
    , undeleted_entity( ent.GetWeakPtr() )
    , backup_entity( nullptr ) // gets assigned during the undo
{
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p: Undo_EntityDelete:Undo_EntityDelete: %p.\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<const std::intptr_t>(&ent) );
    BackupEntity( undeleted_entity.lock() );
}

bool Undo_EntityDelete::PerformUndo( void ) {
    if constexpr ( undoDebug )
        CMSG_LOG("Undo: Delete Entity.\n");
    ASSERT( backup_entity );
    ASSERT( undeleted_entity.expired() );

    auto newEnt = backup_entity->CloneShared();
    game->InsertEntity( newEnt );
    newEnt->Spawn();
    newEnt->UpdateVBO();
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityDelete: Spawned ent %p from %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(newEnt.get()), reinterpret_cast<std::intptr_t>(backup_entity.get()) );
    
    auto lock = backup_entity; // hold onto this entity to keep it locked until we're done updating everything
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityDelete: Updating ent %p to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(backup_entity.get()), reinterpret_cast<std::intptr_t>(newEnt.get()) );
    UpdateAllEntityObjectsInHistory( backup_entity, newEnt );
    
    backup_entity.reset();
    undeleted_entity = newEnt;
    
    return true;
}

bool Undo_EntityDelete::PerformRedo( void ) {
    auto addent = undeleted_entity.lock();
    if ( !addent ) {
        ERR("Redo: Delete Entity: Entity no longer exists, may have left the world during gameplay.\n");
        return false;;
    }
    
    if constexpr ( undoDebug )
        CMSG_LOG("Redo: Delete Entity.\n");
    
    BackupEntity(addent);
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityDelete: Removing ent %p backed up to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(addent.get()), reinterpret_cast<std::intptr_t>(backup_entity.get()) );
    
    game->MarkEntityForDeletion( addent );
    undeleted_entity.reset();
    
    return true;
}

void Undo_EntityDelete::BackupEntity( const std::shared_ptr< Entity >& ent ) {
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: Deleting Entity.\n");
    ASSERT( ! backup_entity );
    backup_entity = ent->CloneShared();
    ASSERT( backup_entity );
    UpdateAllEntityObjectsInHistory( ent, backup_entity );
    
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityDelete: Updating: %p to %p\n", reinterpret_cast<const std::intptr_t>(this), reinterpret_cast<std::intptr_t>(ent.get()), reinterpret_cast<std::intptr_t>(backup_entity.get()) );
}

Undo_EntityDelete::~Undo_EntityDelete( void ) {
}

void Undo_EntityDelete::UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) {
    ASSERT( new_ent );
    ASSERT( old_ent );
    
    if ( backup_entity ) {
        if ( backup_entity == old_ent ) {
            if constexpr ( undoDebug )
                CMSG_LOG("Undo-Debug: %p Undo_EntityDelete:UpdateEntityObject: the backup changed from %p to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(backup_entity.get()), reinterpret_cast<std::intptr_t>(new_ent.get()) );
            backup_entity = new_ent;
        }
        return;
    }
        
    if ( auto addent = undeleted_entity.lock() ) {
        if ( addent == old_ent ) {
            if constexpr ( undoDebug )
                CMSG_LOG("Undo-Debug: %p Undo_EntityDelete:UpdateEntityObject: The added changed from %p to %p\n", reinterpret_cast<std::intptr_t>(this), reinterpret_cast<std::intptr_t>(addent.get()), reinterpret_cast<std::intptr_t>(new_ent.get()) );
            undeleted_entity = new_ent;
        }
        return;
    }
    if constexpr ( undoDebug )
        CMSG_LOG("Undo-Debug: %p Undo_EntityDelete:UpdateEntityObject: No entity managed - Likely the added entity left the world.\n" );
}

#endif // MONTICELLO_EDITOR
