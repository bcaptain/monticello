// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UNDO_UNDO_ENTITYADD
#define SRC_EDITOR_UNDO_UNDO_ENTITYADD

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"
#include "./undo.h"

class Entity;

/*!

Undo_EntityAdd \n\n

**/

class Undo_EntityAdd : public Undo_EntityAction {
public:
    Undo_EntityAdd( UndoHistory& _owner, Entity& ent );
    Undo_EntityAdd( UndoHistory& _owner, const std::weak_ptr< Entity >& entp_smp_to );
    Undo_EntityAdd( void ) = delete;
    ~Undo_EntityAdd( void ) override;
    Undo_EntityAdd( const Undo_EntityAdd& other ) = default;

public:
    Undo_EntityAdd& operator=( const Undo_EntityAdd& other ) = default;

private:
    void UpdateEntityObject( const std::shared_ptr< Entity >& old_ent, std::shared_ptr< Entity >& new_ent ) override;
    bool PerformUndo( void ) override;
    bool PerformRedo( void ) override;

private:
    std::weak_ptr< Entity > added_entity;
    std::shared_ptr< Entity > backup_entity;
};

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UNDO_UNDO_ENTITYADD
