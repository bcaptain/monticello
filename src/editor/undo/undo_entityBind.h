// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_UNDO_ENTITY_BIND_H
#define SRC_EDITOR_UNDO_ENTITY_BIND_H

#ifdef MONTICELLO_EDITOR

#include "../../base/main.h"

class Undo_EntityBind  {
};

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_UNDO_ENTITY_BIND_H
