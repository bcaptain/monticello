// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_EDITOR_H_
#define SRC_EDITOR_EDITOR_H_

class EntityInfo;
class Editor;

extern Editor* editor;

#ifdef MONTICELLO_EDITOR

#include "../base/main.h"

typedef Uint8 EntitySelectOptionT_BaseType;
enum class EntitySelectOptionT : EntitySelectOptionT_BaseType;

#include "../ui/ui.h"
#include "../math/collision/collision.h"
#include "../particles/effect.h"
#include "./undo/undo.h"

extern const Uint32 EDITOR_TOOLTIP_DELAY;

class Slider_ParticleUInt;
class Slider_ParticleInt;
class Slider_ParticleFloat;
class Slider_ParticleBool;
class Trigger;
class Flat;
class EntValListBox;
class NavPoly;
class Navmesh_Poly;
union SDL_Event;

typedef Uint8 RotationAxisT_BaseType;
enum class RotationAxisT : RotationAxisT_BaseType;

typedef Uint8 EditModeT_BaseType;
enum class EditModeT : EditModeT_BaseType {
    NONE = 0
    , SELECT = 1
    , ADD = 2
    , EFFECTS = 4
    , MAPS = 5
    , NAVMESH = 6
    , WORLDSIZE = 7
};

typedef Uint8 MouseModeT_BaseType;
enum class MouseModeT : MouseModeT_BaseType { //!< this tells us what we're currently doing with the mouse
    NONE
    , ENTITY_GRABBED
    , NAVPOLY_GRABBED
    , SCENEPAN
    , MENU
    , BLEND
    , SLIDER
    , SCENEROTATE
    , ADD_TRIGGER_TARGET //!< add target entity to selected trigger
    , ADD_TRIGGER_CHAIN //!< add trigger to selected trigger. When it completes, it will trigger the chain
    , BIND_TO_MASTER  //!< select master to bind selected entity to
    , RECTANGLE_SELECT
};

typedef Uint8 MouseGrabT_BaseType;
enum class MouseGrabT : MouseGrabT_BaseType { //!< this tells us what kind of things we're doing with a grabbed entity
    NONE = 0
    , MOVE_XY
    , MOVE_Z
    , ROTATE_X
    , ROTATE_Y
    , ROTATE_Z
    , DEFORM_FLAT
    , SCALE_FLAT
    , DEFORM_NAVPOLY
};

// ****
//
//  Editor
//
// ****

class Editor {
public:
    Editor( void );
    ~Editor( void );
    Editor( const Editor& other ) = delete;

public:
    Editor& operator=( const Editor& other ) = delete;

// ****
//
//  General
//
// ****

public:
    uint ApplyEntValToSelectedEnts( const char* key, const char* val ); //!< returns how many entities it was applied to
    uint AddTargetToSelections( Entity& target ); //!< returns how many targets were added
    uint AddTriggerChainToSelections( Trigger& trig ); //!< returns how many targets were added
    uint BindSelectionsToMaster( Entity& target ); //!< returns how many targets were added

    void ShowUI( bool val );
    void SetSelectedSlider( SliderBase* slider );
    void ShowContextMenu( void );
    Widget* CheckToolTip( void ); //!< returns the widget for which we are showing a tooltip (or null if we're not)

    bool EvalEventBinds( const std::string& action );
    bool EvalStateBinds( const std::string& action );
    void EvalMouseMotion( void );
    bool EvalButtonReleased( const std::string& action );

    bool CheckInputEvent_Editor( const std::string& action ); //!< returns whether event caused an action

    bool IsContextMenuOpen( void ) const;
    void CloseContextMenus( void );

    void Think( void );
    void Think_TestAnimModel( void );
    void Think_ToolTips( void );
    void Think_Lights( void );
    void Think_GhostSelection( void );
    void RotateGhostSelectionX( const float normalized_direction, const bool snap_to_grid );
    void RotateGhostSelectionY( const float normalized_direction, const bool snap_to_grid );
    void RotateGhostSelectionZ( const float normalized_direction, const bool snap_to_grid );
private:
    bool UpdateGhostSelectionModel( void ); //! returns false if ghost selection is null (sets ghost material to null if so, if so, and vice versa)
    void Draw_GhostSelection( void ) const;
    void Draw_TestAnimModel( void ) const;
    void Draw_Grid( void ) const;
    void vbo_packGridLines( void );

public:
    void Update_Grid( void );
    void PackVBO( void );
    void Draw( void );

    void SetPanScale( const float to );
    void SetZoomScale( const float to );
    float GetPanScale( void ) const;
    float GetZoomScale( void ) const;
    bool SetTestAnim( const char *animName );
    bool SetTestAnimModel( const char *modelName);
    bool UpdateTestAnimMaterial( void ); //uses default material

public:
    void SetMouseMode( const MouseModeT to );
    MouseModeT GetMouseMode( void ) const;

    void SetMode( const EditModeT to );
    EditModeT GetMode( void ) const;
    void UnsetMode( void );

    void SetEditorInputDefaults( void );

    void UnhighlightSelectedEnts( void );
    bool ClearSelectionListIfEntityIsNotInIt( Entity& ent );
    bool RemoveEntityFromSelectionList( Entity& ent ); //!< returns true of the entity was found in the list
    void UnselectAllEntities( void );
    bool IsEntitySelected( Entity& ent ) const;
    bool SelectEntity( const EntitySelectOptionT opt, std::shared_ptr< Entity >& ent );
    bool SelectEntity_ForDrag( const EntitySelectOptionT opt, std::shared_ptr< Entity >& ent );

    void SetSnap( const bool whether );
    bool GetSnap( void ) const;
    float GetSnapGridDistance( void ) const;
    void ToggleSnap( void );

private:
    void CreateUI( void );
    void Create_ToolTip( void );
    void Create_ContextMenu( void );
    void Create_ToolBoxUI( void );
    void Create_AddModeUI( void );
    void Create_SelectModeUI( void );
    void Create_EntValsUI( void );
    void Create_BindUI( void );
    void Create_TargetsUI( void );
    void Create_FlatsUI( void );
    void Create_TriggerChainUI( void );
    void Create_EffectsModeUI( void );
    void Create_MapsUI( void );
    void Create_NavMeshUI( void );
    void Create_WorldSizeUI( void );

    void PositionToolBoxUI( void );

    void SliderUpdate( void );
    void DoScenePan( void );
    void DoSceneRot( void );
    void DoSceneZoom( const float zoom_scale );

private:
    HashList<LinkList<Uint16> > editor_binds;

public:
    UndoHistory undo_history;

private:
    Vec3f selection_origin; //! used as temporary variable for various modes (selecting ents, nav meshes, etc)
    float selection_angle; //! used as temporary variable for various modes (selecting ents, nav meshes, etc)
    Vec3f click_pos; //! used as temporary variable for various modes (selecting ents, nav meshes, etc)
    Vec3f drag_pos; //! used as temporary variable for various modes (selecting ents, nav meshes, etc)

    float pan_scale; // increment for panning the screen
    float zoom_scale; // increment for zooming
    float scale_multiplier; //modifies scale/zoom increment.  Shift = Larger, Alt = Smaller

    SliderBase* selectedSlider;

    EditModeT editmode;
    MouseModeT mouseMode;
    MouseGrabT mouseGrabType;

    Uint32 prev_tooltip_mouse_x;
    Uint32 prev_tooltip_mouse_y;

    Label *lblToolTip;
    Window *wndToolBox;
    Window *wndSelectOpts;
    Window *wndEntVals;
    EntValListBox *wndEntVals_listBox;
    Window *wndBind;
    Window *wndTargets;
    Window *wndTriggerChain;
    Window *wndFlats;
    TextBox *txtFlatUVScaleVertical;
    TextBox *txtFlatUVScaleHorizontal;
    TextBox *txtFlatUVAlignVertical;
    TextBox *txtFlatUVAlignHorizontal;
    Window *wndEffectsOpts;
    std::vector<Sprite*> particleValElements; //!< this is just here so we can quickly hide/show the elements. they are managed/deleted elsewhere
    std::vector<Sprite*> effectValElements; //!< this is just here so we can quickly hide/show the elements. they are managed/deleted elsewhere
    Window *wndAddEntOpts;

    // ContextMenus
    Window *wndContextMenu;

    Button_Func *btnSelectMode;
    Button_Func *btnAddMode;
    Button_Func *btnEffectsMode;
    Button_Func *btnMapsMode;
    Button_Func *btnNavMeshMode;
    Button_Func *btnWorldSizeMode;
    
    CheckBox *chkFaceCamera;

// ****
//
//  Select Mode
//
// ****

public:
    MouseGrabT GetMouseGrabType( void ) const;
    void SetMouseGrabType( const MouseGrabT to );

    void UpdateEntSelectionInfo_All( void );
    void UpdateEntSelectionInfo_Bounds( void ) const;
    void UpdateEntSelectionInfo_ModelAndMaterials( void ) const;
    void UpdateEntSelectionInfo_Basics( void ) const;
    void UpdateEntSelectionInfo_EntVals( void );
    void UpdateEntSelectionInfo_TriggerTargets( void ) const;
    void UpdateEntSelectionInfo_TriggerChain( void ) const;
    void UpdateEntSelectionInfo_Flats( void ) const;
    void UpdateEntSelectionInfo_Bind( void ) const;

    void DuplicateSelectedEnts( void );
    void DeleteSelectedEnts( void );
    void CombineSelectedEnts( void );

    std::weak_ptr< Entity > GetSelectedEntity( void ) const;
    void SetSelectedActorTeamName( const std::string& teamName );
    void ReleaseEntity( void );
    bool DragEntity( void );
    bool DragEntityVertical( void );
    bool RotateEntityLocal( const RotationAxisT local_axis );

    void SetEntitySelectionModel( const char* model_name );
    void SetEntitySelectionMaterial( const char* mtl_name );
    void UpdateEntOrigin( void );
    void UpdateEntName( void );
    void UpdateEntBBox( void );
    void UpdateEntBBoxFromModel( void );
    void UpdateEntAngle( void );

    void SelectTriggerTarget( void ); //!< to be applied to currently selected target
    void SelectTriggerChain( void ); //!< to be applied to currently selected target

    void SetFlatUV( void );
    Flat* GetSelectedFlat( void );
    const Flat* GetSelectedFlat( void ) const;

    void TranslateToSelectedEntsBindMaster( void );
    void UnbindEntSelectionsFromMaster( void );
    void SelectMasterToBindEntSelectionsTo( void );

    void RepopulateModelsDropBox( void );
    Link< std::weak_ptr< Entity > >* GetLinkEntityIfSelected( Entity& ent );
    const Link< std::weak_ptr< Entity > >* GetLinkEntityIfSelected( Entity& ent ) const;

    void EntValListBoxNew_PressEnter( void  );

private:
    std::vector< std::size_t > flat_deform_indices;

private:
    void SelectMode( void );
    bool RightClickSelectEntity( void );
    void MouseMove_SelectMode( void );
    bool CheckInputEvent_SelectMode( const std::string& action ); //!< returns whether event caused an action
    void Draw_SelectMode( void ) const;
    void RectangleSelect_Drag( void );
    void RectangleSelect( void );
    void Draw_SelectionRect( void ) const;
    void vbo_packSelectionRect( void );

    // ** Flats
    void DeformFlat( void );
    void DeformFlatVert_Preview( const Flat& flat ) const;
    void BeginFlatDeform( const Flat& flat );
    bool BeginFlatDeform_SingleVert( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts );
    bool FlatDeformPreview_SingleVert( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts ) const;
    bool BeginFlatDeform_Lines( const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm );
    bool FlatDeformPreview_Lines( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm ) const;
    bool BeginFlatDeform_AllVerts( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm );
    bool FlatDeformPreview_AllVerts( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm ) const;
    void ContinueFlatDeform( Flat& flat );
    void AddFlatVert_Preview( const Flat& flat ) const;
    void AddFlatVert( Flat& flat ) const;
    void DelFlatVert( Flat& flat ) const;
    void DelFlatVert_Preview( const Flat& flat ) const;

public:
    std::shared_ptr< Flat > GetFlatFromSelection( void );
    std::shared_ptr< const Flat > GetFlatFromSelection( void ) const;

public:
    std::shared_ptr< const EntityInfo > selectedEntInfo;

private:
    DropDownList* dboxModels;
    DropDownList* dboxMaterials;
    Label* lblEntType;
    TextBox* txtEntName;
    TextBox* txtEntOriginX;
    TextBox* txtEntOriginY;
    TextBox* txtEntOriginZ;
    TextBox* txtEntBBoxX;
    TextBox* txtEntBBoxY;
    TextBox* txtEntBBoxZ;
    TextBox* txtEntBoundsOffsetX;
    TextBox* txtEntBoundsOffsetY;
    TextBox* txtEntBoundsOffsetZ;
    TextBox* txtEntAngleX;
    TextBox* txtEntAngleY;
    TextBox* txtEntAngleZ;
    Button_Func *btnEntitysBind;
    LinkList< std::weak_ptr< Entity> > selected_ents;
    VBO<float>* vbo_select_rect;
    VBO<float>* vbo_select_rect_colors;
    VBO<float>* vbo_grid;
    bool entityGrabActionStarted; //!< set to false when an action begins (for use with adding undo actions)

// ****
//
// Add Mode
//
// ****

public:
    bool SpawnEntity( void );
    void SetSelectedEntInfo( const char* to ); //!< takes a file without path
    void RepopulateEntityInfoDropBox( void );

private:
    void AddMode( void );
    bool CheckInputEvent_AddMode( const std::string& action ); //!< returns whether event caused an action

private:
    DropDownList    *dboxEntBrowser;
    Label *lblEntFileSelected;
    Vec3f addSelectionOrigin;
    Vec3f addSelectionAngles;

// ****
//
// Maps
//
// ****

public:
    void SetSelectedMap( const std::string& to );
    std::string GetSelectedMap( void ) const;
    void Confirm_Map_LoadSelected( void );
    void Confirm_Map_SaveToSelected( void );

    void Map_Clearing( void ); //!< called before the game clears a new map (for example, to load another)
    void Map_Loaded( const char* name ); //!< called after the game loads a new map

    void RepopulateMapsDropBox( void );

private:
    void MapsMode( void );

private:
    Window* wndMapsOpts;
    DropDownList* dboxMapsBrowser;

// ****
//
// NavMesh
//
// ****

public:
    bool SelectNavPolyUnderMouse( void );
    bool SelectNavPolyAt( const Vec2f& at );
    void AddNavMeshNode( void );
    void DelNavMesh( void );
    void Confirm_DelNavMesh( void );

private:
    void Draw_NavMeshMode( void );
    void Draw_SelectedNavPoly( void ) const;
    void Draw_NavMeshVertMergePreview( void );
    void NavMeshMode( void );
    void MouseMove_NavMeshMode( void );
    bool CheckInputEvent_NavMeshMode( const std::string& action ); //!< returns whether event caused an action
    bool ExtrudeSelectedNavPoly_Preview( const Vec2f& at );
    bool ExtrudeSelectedNavPoly( const Vec2f& at );
    void DeformNavPoly( void );
    void DeformNavPoly_Preview( void );
    void BeginNavPolyDeform( void );
    void ContinueNavPolyDeform( void );
    int GetVertIndexForMerge( void ) const; //!< when dragging a vertex during deforming. returns negative if none.
    void DoneDeformNavPoly( void );
    void AddVertToNavPoly( void );
    void NavPolyAddVert_Preview( void );

private:
    std::weak_ptr< NavPoly > selected_nav_poly;
    Window* wndNavMeshOpts;
    std::vector< std::size_t > navpoly_deform_indicies;


// ****
//
//  World Size Mode
//
// ****

public:
    void UpdateWorldSize( const Vec3f& dim );
    void SetWorldSize( void );

private:
    void WorldSizeMode( void );

private:
    Window* wndWorldSizeOpts;
    TextBox *worldSizeX;
    TextBox *worldSizeY;
    TextBox *worldSizeZ;

// ****
//
//  Effects Mode
//
// ****

public:
    void Draw_EffectMode( void );

    void SetEffectUIValues( const Effect& effect ) const;
    void SetParticleUIValues( const Particle& prt ) const;

    void ShowEffectUIValues( const bool whether ) const;
    void ShowParticleUIValues( const bool whether ) const;

    Link< Effect >* GetSelectedEffectLink( void );
    void SetSelectedEffectLink( Link< Effect >* ); //!< doesn't update UI. UI utilizes this function.

    std::shared_ptr< Effect > GetSelectedEffect( void ) const;
    void SetSelectedEffect( const std::string& name ); //!< doesn't update UI. UI utilizes this function.

    Link< Particle >* GetSelectedParticleLink( void ) const;
    void SetSelectedParticle( const int list_index ); //!< doesn't update UI. UI utilizes this function.

    void ApplyParticleOffset( void ) const;
    void ToggleParticleDest( void ) const;
    void ToggleParticleDest2( void ) const;
    void UpdateParticleDest( void ) const;
    void UpdateParticleDest2( void ) const;
    void SetParticleFaceCamera( const bool whether ) const;
    void DeleteSelectedParticle( void );
    void HideSelectedParticle( void );
    void ShowSelectedParticle( void );
    void SoloSelectedParticle( void );
    void SetColorize( void ) const;
    void SetColorizeTo( void ) const;
    void ToggleParticleRandInitialAngle( void ) const;
    void NewParticle( void );
    void SetParticleTex( const char* selection ) const;

    void SaveEffect( void );
    void ReloadEffect( void );
    void RestartEffect( void );

    void SetRenderEffectData( const uint to );
    void SetParticleQuantity( const uint to ) const;
    void SetParticleDelay( const uint to ) const;
    void SetParticleCease( const uint to ) const;
    void SetParticleLifeSpan( const uint to ) const;
    void SetParticleBunching( const float to ) const;
    void SetParticleRotationTo( const float to ) const;
    void SetParticleRotationFrom( const float to ) const;
    void SetParticleColorizeWeight( const float to ) const;
    void SetParticleColorToWeight( const float to ) const;
    void SetParticleOpacityFrom( const float to ) const;
    void SetParticleOpacityTo( const float to ) const;
    void SetParticleOpacityMid( const float to ) const;
    void SetParticleScaleFrom( const float to ) const;
    void SetParticleScaleTo( const float to ) const;

    void RepopulateParticleTexDropBox( void );
    void RepopulateParticlesDropBox( void );
    void RepopulateEffectsDropBox( void );

private:
    void EffectsMode( void );
    void HideParticle( Particle& prt );
    void ShowParticle( Particle& prt );

    std::string GetDropDownOptionNameForParticle( const uint index, const Particle& prt ) const;

public:
    DropDownList* dboxEffects;
    DropDownList* dboxParticles;
    DropDownList* dboxParticleTex;

private:
    Slider<Uint>* sldRenderEffectData;
    Slider<Uint>* sldParticleDelay;
    Slider<Uint>* sldParticleCease;
    Slider<Uint>* sldParticleQty;
    Slider<Uint>* sldParticleLifeSpan;
    Slider<float>* sldParticleBunching;
    Slider<float>* sldParticleRotateFrom;
    Slider<float>* sldParticleRotateTo;
    Slider<float>* sldParticleScaleTo;
    Slider<float>* sldParticleScaleFrom;
    Slider<float>* sldParticleOpacityFrom;
    Slider<float>* sldParticleOpacityMid;
    Slider<float>* sldParticleOpacityTo;
    TextBox *txtOffsetX, *txtOffsetY, *txtOffsetZ;
    TextBox *txtOriginDistribXY;
    TextBox *txtOriginDistribZ;
    TextBox *txtDestX, *txtDestY, *txtDestZ;
    TextBox *txtDestDistribXY;
    TextBox *txtDestDistribZ;
    Button_Func *txtSetDest;
    TextBox *txtDest2X, *txtDest2Y, *txtDest2Z;
    TextBox *txtDest2DistribXY;
    TextBox *txtDest2DistribZ;
    Button_Func *txtSetDest2;
    Button_Func *btnToggleParticleRandInitialAngle;
    Button_Func *btnToggleParticleDest;
    Button_Func *btnToggleParticleDest2;
    TextBox *txtParticleColorX, *txtParticleColorY, *txtParticleColorZ;
    Slider<float>* sldParticleColorWeight;
    TextBox *txtParticleColorToX, *txtParticleColorToY, *txtParticleColorToZ;
    Slider<float>* sldParticleColorToWeight;
    Uint32 effectPreviewStartTime;

// ****
//
// Editor
//
// ****

private:
    std::shared_ptr< Effect > selectedEffect;
    Link< Particle >* selectedParticleLink;
    bool snapping;
    ModelInfo ghostSelectionModelInfo;
    ModelInfo testAnimModelInfo;
};

// must include the mode headers after the class type is completed so as to define inline functions in them
#include "./navMeshMode.h"
#include "./selectMode.h"
#include "./effectsMode.h"
#include "./mapsMode.h"

#else // MONTICELLO_EDITOR
class Editor {};
#endif  // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_EDITOR_H_
