// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./editor.h"
#include "./ui/editmode.h"
#include "../game.h"
#include "../ui/dialog/common.h"

void Editor::MapsMode( void ) {
    btnMapsMode->SetFaceBottomColor( COLOR_RADIOBUTTON_GRADIENT_B );
    wndToolBox->ShowNamedElement( WINDOW_MAPS_NAME, true );
}

void Editor::Confirm_Map_LoadSelected( void ) {
    Dialog_LoadMap( dboxMapsBrowser->GetLabel() );
}

void Editor::Confirm_Map_SaveToSelected( void ) {
    Dialog_SaveMap( dboxMapsBrowser->GetLabel() );
}

void Editor::Map_Clearing( void ) {
    SetMouseMode( MouseModeT::NONE );
    UnselectAllEntities();
    undo_history.Clear();
}

void Editor::Map_Loaded( const char* name ) {
    SetMouseMode( MouseModeT::NONE );
    
    if ( name == nullptr || name[0] == '\0' )
        return;

    if ( editmode == EditModeT::WORLDSIZE )
        UpdateWorldSize( globalVals.GetVec3f( gval_g_octreeSize ) );

    editor->SetSelectedMap( name );
}

void Editor::RepopulateMapsDropBox( void ) {
    dboxMapsBrowser->Clear();
    LinkList< std::string > names;
    game->GetAllMapNames( names );
    for ( const auto & name : names )
        dboxMapsBrowser->Add( { name, name } );
    dboxMapsBrowser->Refresh();
}

void Editor::SetSelectedMap( const std::string& to ) {
    dboxMapsBrowser->SetLabel( to.c_str() );
}

std::string Editor::GetSelectedMap( void ) const {
    return dboxMapsBrowser->GetLabel();
}

#endif // MONTICELLO_EDITOR
