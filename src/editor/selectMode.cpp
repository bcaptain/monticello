// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef MONTICELLO_EDITOR

#include "./editor.h"
#include "../math/collision/collision.h"
#include "../input/input.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../entities/modules/module.h"
#include "../entities/modules/flat.h"
#include "../entities/actor.h"
#include "./ui/editorui.h"
#include "../entities/triggers/trigger.h"
#include "./undo/undo_entityAdd.h"
#include "./undo/undo_entityDelete.h"
#include "./undo/undo_entityProperty.h"
#include "./undo/undo_entityBind.h"
#include "../rendering/materialManager.h"
#include "../rendering/models/modelManager.h"

const float SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT = 0.5f;
const float SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT_SQ = SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT * SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT;

std::shared_ptr< Entity > GetFirstEntityPtr( LinkList< std::weak_ptr< Entity> >& list );
std::shared_ptr< Entity > GetFirstEntityPtr( LinkList< std::weak_ptr< Entity> >& list ) {
    Link< std::weak_ptr< Entity > > *link = list.GetFirst();
    if ( ! link )
        return nullptr;

    return link->Data().lock();
}

std::shared_ptr< Entity > GetFirstEntityPtr( const LinkList< std::weak_ptr< Entity> >& list );
std::shared_ptr< Entity > GetFirstEntityPtr( const LinkList< std::weak_ptr< Entity> >& list ) {
    const Link< std::weak_ptr< Entity > > *link = list.GetFirst();
    if ( ! link )
        return nullptr;

    return link->Data().lock();
}

bool Editor::RightClickSelectEntity( void ) {
    std::shared_ptr< Entity > ent = ClickSweep();
    
    const bool deselect_selection = !ent && selected_ents.Num(); // if we click nothing and have a selectin, we deselect whatever is currently selected
    const bool entity_already_selected = ent && IsEntitySelected( *ent );

    if ( deselect_selection ) {
        UnselectAllEntities();
        return false;
    }
    // try selecting the an entity if we didn't click on one we selected
    if ( !entity_already_selected ) {
        if ( !ent || !SelectEntity( EntitySelectOptionT::SELECT_SINGLE, ent ) ) {
            CloseContextMenus();
            return false;
        }
    }

    ShowContextMenu();

    return true;
}

void Editor::SelectMode( void ) {
    btnSelectMode->SetFaceBottomColor( COLOR_RADIOBUTTON_GRADIENT_B ); // highlight the button
    wndToolBox->ShowNamedElement( WINDOW_ENTSELECT_NAME, true ); // show the options ui
}

void Editor::MouseMove_SelectMode( void ) {
    switch ( mouseMode ) {
        case MouseModeT::ENTITY_GRABBED:
            switch ( mouseGrabType ) {
                case MouseGrabT::DEFORM_FLAT:
                    DeformFlat(); // continue deforming a flat
                    return;

                case MouseGrabT::MOVE_Z:
                    DragEntityVertical(); // continue dragging entity vertically
                    return;
                case MouseGrabT::ROTATE_Z:
                    RotateEntityLocal( RotationAxisT::Z ); // continue rotating entity
                    return;
                case MouseGrabT::ROTATE_X:
                    RotateEntityLocal( RotationAxisT::X ); // continue rotating entity
                    return;
                case MouseGrabT::ROTATE_Y:
                    RotateEntityLocal( RotationAxisT::Y ); // continue rotating entity
                    return;
                case MouseGrabT::MOVE_XY:
                    DragEntity(); // continue dragging entity
                    return;
                case MouseGrabT::SCALE_FLAT: return;
                case MouseGrabT::DEFORM_NAVPOLY: break;
                case MouseGrabT::NONE: break;
                default: return;
            }

        case MouseModeT::NAVPOLY_GRABBED: break;
        case MouseModeT::SCENEPAN: break;
        case MouseModeT::MENU: break;
        case MouseModeT::BLEND: break;
        case MouseModeT::SLIDER: break;
        case MouseModeT::SCENEROTATE: break;
        case MouseModeT::ADD_TRIGGER_CHAIN: break;
        case MouseModeT::ADD_TRIGGER_TARGET: break;
        case MouseModeT::BIND_TO_MASTER: break;
        case MouseModeT::RECTANGLE_SELECT: break;
        case MouseModeT::NONE: break;
        default: return;
    }
}

uint Editor::BindSelectionsToMaster( Entity& target ) {
    if ( selected_ents.Num() < 1 ) {
        WARN("No entity selected to bind as master.\n");
        return false;
    }

    if ( IsEntitySelected( target ) ) {
        WARN("Entity cannot be master of itself. Not binding.\n");
        return false;
    }

    //todoundo new Undo_EntityBind( undo_history, selected_ents );

    uint added=0;

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {
            sptr->BindToMaster( target );
            ++added;
        }
        link = link->GetNext();
    }

    UpdateEntSelectionInfo_Bind();

    return added;
}

uint Editor::AddTargetToSelections( Entity& target ) {
    if ( selected_ents.Num() < 1 ) {
        WARN("No entity selected to add a target to.\n");
        return false;
    }

    if ( IsEntitySelected( target ) ) {
        WARN("Trigger cannot trigger itself.\n");
        return false;
    }

    uint added = 0;

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {
            if ( derives_from< Trigger >( *sptr ) ) {
                if ( static_cast< Trigger* >( sptr.get() )->AddTarget( target ) ) {
                    ++added;
                }
            }
        }
        link = link->GetNext();
    }

    UpdateEntSelectionInfo_TriggerTargets();
    UpdateEntSelectionInfo_TriggerChain();

    return added;
}

uint Editor::AddTriggerChainToSelections( Trigger& trig ) {
    if ( selected_ents.Num() < 1 ) {
        WARN("No trigger selected to add a chain to.\n");
        return false;
    }
    
    if ( IsEntitySelected( trig ) ) {
        WARN("Trigger cannot trigger itself.\n");
        return false;
    }

    uint added = 0;

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {
            if ( derives_from< Trigger >( *sptr ) ) {
                auto ent_sptr = trig.GetWeakPtr().lock();
                auto trig_sptr = std::static_pointer_cast< Trigger >( ent_sptr );
                if ( static_cast< Trigger* >( sptr.get() )->AddTriggerChain( trig_sptr ) ) {
                    ++added;
                }
            }
        }
        link = link->GetNext();
    }

    UpdateEntSelectionInfo_TriggerTargets();
    UpdateEntSelectionInfo_TriggerChain();

    return added;
}

void Editor::Draw_SelectMode( void ) const {
    auto flat_sptr = GetFlatFromSelection();
    if ( !flat_sptr )
        return;

    if ( Input::IsKeyDown( globalVals.GetString("key_deformFlat") ) ) {
        DeformFlatVert_Preview( *flat_sptr );
        return;
    }

    if ( Input::IsKeyDown( globalVals.GetString("key_addFlatVert") ) ) {
        AddFlatVert_Preview( *flat_sptr );
        return;
    }

    if ( Input::IsKeyDown( globalVals.GetString("key_delFlatVert") ) ) {
        DelFlatVert_Preview( *flat_sptr );
        return;
    }
}

bool Editor::CheckInputEvent_SelectMode( const std::string& action ) {
    if ( action == "TranslateToEnt" ) {
        return renderer->camera.LookAtGroup( selected_ents );
    }

    bool single_select = action == "SelectEnt";

    if ( mouseMode == MouseModeT::ADD_TRIGGER_TARGET ) {
        if ( !single_select )
            return false;
        SetMouseMode( MouseModeT::NONE );
        auto target = ClickSweep();
        if ( ! target )
            return false;
        return AddTargetToSelections( *target ) > 0;
    }

    if ( mouseMode == MouseModeT::ADD_TRIGGER_CHAIN ) {
        if ( !single_select )
            return false;
        SetMouseMode( MouseModeT::NONE );
        std::shared_ptr< Entity > trig = ClickSweep();
        if ( !trig  ) {
            return false;
        }

        if ( ! derives_from< Trigger >( *trig ) ) {
            ERR("Selected entity is not a trigger\n");
            return false;
        }
        auto selTrig = std::static_pointer_cast< Trigger >( trig );
        if ( !selTrig )
            return false;
        return AddTriggerChainToSelections( *selTrig ) > 0;
    }

    if ( mouseMode == MouseModeT::BIND_TO_MASTER ) {
        if ( !single_select )
            return false;
        SetMouseMode( MouseModeT::NONE );
        auto selEnt = ClickSweep();
        if ( ! selEnt )
            return false;
        return BindSelectionsToMaster( *selEnt ) > 0;
    }

    bool multiple_select = action == "SelectEntMult"; // only put !single_select here to prevent from doing unnecessary work. they are mutually exlusive or both false.

    if ( single_select || multiple_select ) {
        std::shared_ptr< Entity > ent = ClickSweep();
        if ( single_select ) {
            return SelectEntity_ForDrag( EntitySelectOptionT::SELECT_SINGLE, ent );
        } else {
            return SelectEntity( EntitySelectOptionT::SELECT_MULTIPLE, ent );
        }
    }

    if ( action == "SelectEntRect" ) {
        RectangleSelect();
        return true;
    }

    if ( action == "DeformFlat" ) {
        DeformFlat();
        return true;
    }

    if ( action == "AddFlatVert" ) {
        auto flat_sptr = GetFlatFromSelection();
        if ( flat_sptr )
            AddFlatVert( *flat_sptr );
        return true;
    }

    if ( action == "DelFlatVert" ) {
        auto flat_sptr = GetFlatFromSelection();
        if ( flat_sptr )
            DelFlatVert( *flat_sptr );
        return true;
    }

    if ( selected_ents.Num() < 1 )
        return false;

    if ( action == "DupSelection" ) {
        editor->DuplicateSelectedEnts();
        return true;
    }

    if ( action == "DelSelection" ) {
        editor->DeleteSelectedEnts();
        editor->CloseContextMenus();
        return true;
    }

    if ( action == "DragEntZ" ) {
        std::shared_ptr< Entity > ent = ClickSweep();
        if ( !ent )
            return false;

        bool clicked_entity_in_group = false;
        if ( selected_ents.Num() > 0 && IsEntitySelected( *ent ) ) {
            clicked_entity_in_group = true;
        }

        if ( !clicked_entity_in_group ) {
            SelectEntity( EntitySelectOptionT::SELECT_SINGLE, ent );
        }

        Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
        if ( ! link )
            return false;

        auto sptr = link->Data().lock();
        if ( !sptr )
            return false;

        selection_origin = ent->GetOrigin();
        click_pos = Input::GetUIMousePos();

        SetMouseMode( MouseModeT::ENTITY_GRABBED );
        SetMouseGrabType( MouseGrabT::MOVE_Z );
        return true;
    }

    bool rotate_z = ( action == "RotateEntZ" );
    bool rotate_x = !rotate_z && ( action == "RotateEntX" ); //noconstexpr
    bool rotate_y = !rotate_z && !rotate_x && ( action == "RotateEntY" ); //noconstexpr

    if ( rotate_x || rotate_z || rotate_y ) {
        std::shared_ptr< Entity > ent = ClickSweep();
        if ( ! ent )
            return false;

        bool clicked_entity_in_group = false;
        if ( selected_ents.Num() > 0 && IsEntitySelected( *ent ) ) {
            clicked_entity_in_group = true;
        }

        if ( !clicked_entity_in_group ) {
            SelectEntity( EntitySelectOptionT::SELECT_SINGLE, ent );
        }

        auto sptr = GetFirstEntityPtr( selected_ents );

        if ( ! sptr )
            return false;

        if ( selected_ents.Num() < 1 )
            return false;

        GetGroupOrigin( selected_ents, selection_origin );
        click_pos = Input::GetWindowMousePos();
        drag_pos = click_pos;
        SetMouseMode( MouseModeT::ENTITY_GRABBED );

        if ( rotate_z ) {
            selection_angle = sptr->bounds.primitive->GetYaw();
            SetMouseGrabType( MouseGrabT::ROTATE_Z);
        } else if ( rotate_x ) {
            selection_angle = sptr->bounds.primitive->GetRoll();
            SetMouseGrabType( MouseGrabT::ROTATE_X );
        } else if ( rotate_y ) {
            selection_angle = sptr->bounds.primitive->GetRoll();
            SetMouseGrabType( MouseGrabT::ROTATE_Y );
        }
        return true;
    }

    return false;
}

std::shared_ptr< Flat > Editor::GetFlatFromSelection( void ) {
    return std::const_pointer_cast< Flat >( const_cast< const Editor* >( this )->GetFlatFromSelection() );
}

std::shared_ptr< const Flat > Editor::GetFlatFromSelection( void ) const {
    if ( selected_ents.Num() != 1 )
        return nullptr;

    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( ! sptr )
        return nullptr;

    if ( !derives_from< Flat >( *sptr ) )
        return nullptr;

    return std::static_pointer_cast<const Flat>( sptr );
}

std::vector< Vec3f > GetFlatVertList( const Flat& flat, const Quat& rot );
std::vector< Vec3f > GetFlatVertList( const Flat& flat, const Quat& rot ) {
    std::vector< Vec3f > localVerts;
    localVerts.reserve( flat.NumVerts() );
    for ( std::size_t v=0; v<flat.NumVerts(); ++v )
        localVerts.emplace_back( flat.GetLocalVert( v ) * rot );
    return localVerts;
}

int AddFlatVert_Helper( const Flat& flat );
int AddFlatVert_Helper( const Flat& flat ) {
    const Vec3f flat_origin( flat.GetOrigin() );
    const Quat rot( Quat::FromAngles_ZYX( flat.bounds.primitive->GetAngles() ) );
    const Vec3f norm( Vec3f(0,0,-1) * rot );
    std::vector<Vec3f> localVerts( GetFlatVertList( flat, rot ) );
    const Vec3f pos( Input::GetWorldMousePos( localVerts[0] + flat_origin, norm ) );
    const Vec3f local_click_pos(pos - flat_origin);
    std::size_t closest_vert_index;

    //we're going to send the average of each vertice with the one before it,
    //so that we can click where the vertex will actually go

    for ( std::size_t v = localVerts.size()-1; v>0; --v ) {
        localVerts[v] += localVerts[v-1];
        localVerts[v] /= 2;
    }
    localVerts[0] += localVerts[localVerts.size()-1];
    localVerts[0] /= 2;

    if ( Polygon3D::GetClosestVertIndex( local_click_pos, localVerts.data(), localVerts.size(), closest_vert_index ) > SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT )
        return -1;

    return static_cast< int >( closest_vert_index );
}

void Editor::DeformFlatVert_Preview( const Flat& flat ) const {
    // ** setup
    const Quat rot( Quat::FromAngles_ZYX( flat.bounds.primitive->GetAngles() ) );
    const Vec3f flat_norm( Vec3f(0,0,-1) * rot );
    const std::vector<Vec3f> localVerts( GetFlatVertList( flat, rot ) );
    auto hover_pos = Input::GetWorldMousePos( localVerts[0] + flat.GetOrigin(), flat_norm );
    const Vec3f local_click_pos(hover_pos - flat.GetOrigin());

    if ( FlatDeformPreview_SingleVert( flat, local_click_pos, localVerts ) )
        return;

    if ( FlatDeformPreview_AllVerts( flat, local_click_pos, localVerts, flat_norm ) )
        return;

    FlatDeformPreview_Lines( flat, local_click_pos, localVerts, flat_norm );
}

void Editor::AddFlatVert_Preview( const Flat& flat ) const {
    int closest_vert_index = AddFlatVert_Helper( flat );
    if ( closest_vert_index >= 0)
        flat.AddVertBefore_Preview( static_cast< uint >( closest_vert_index ) );
}

void Editor::AddFlatVert( Flat& flat ) const {
    int closest_vert_index = AddFlatVert_Helper( flat );
    if ( closest_vert_index >= 0)
        flat.AddVertBefore( static_cast< uint >( closest_vert_index ) );
}

int DelFlatVert_Helper( const Flat& flat );
int DelFlatVert_Helper( const Flat& flat ) {
    const Vec3f flat_origin( flat.GetOrigin() );
    const Quat rot( Quat::FromAngles_ZYX( flat.bounds.primitive->GetAngles() ) );
    const Vec3f norm( Vec3f(0,0,-1) * rot );
    const std::vector<Vec3f> localVerts( GetFlatVertList( flat, rot ) );
    const Vec3f pos( Input::GetWorldMousePos( localVerts[0] + flat_origin, norm ) );
    const Vec3f local_click_pos(pos - flat_origin);
    std::size_t closest_vert_index;

    if ( Polygon3D::GetClosestVertIndex( local_click_pos, localVerts.data(), localVerts.size(), closest_vert_index ) > SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT )
        return -1;

    return static_cast< int >( closest_vert_index );
}

void Editor::DelFlatVert_Preview( const Flat& flat ) const {
    int closest_vert_index = AddFlatVert_Helper( flat );
    if ( closest_vert_index >= 0)
        flat.DeleteVert_Preview( static_cast< uint >( closest_vert_index ) );
}

void Editor::DelFlatVert( Flat& flat ) const {
    int closest_vert_index = AddFlatVert_Helper( flat );
    if ( closest_vert_index >= 0)
        flat.DeleteVert( static_cast< uint >( closest_vert_index ) );
}

void Editor::DeformFlat( void ) {
    auto flat = GetFlatFromSelection();
    if ( ! flat ) {
        SetMouseMode( MouseModeT::NONE );
        flat_deform_indices.clear();
        return;
    }

    if ( mouseMode != MouseModeT::ENTITY_GRABBED ) {
        BeginFlatDeform( *flat );
        return;
    }

    if ( mouseMode != MouseModeT::ENTITY_GRABBED )
        return;

    if ( mouseGrabType != MouseGrabT::DEFORM_FLAT )
        return;

    if ( flat_deform_indices.size() > 0 )
        ContinueFlatDeform( *flat );
}

std::vector< std::size_t > GetLineIndicesFacingVert( const Vec3f& to, const Vec3f* clockwise_verts, const std::size_t vert_num, const Vec3f& face_normal );
    std::vector< std::size_t > GetLineIndicesFacingVert( const Vec3f& to, const Vec3f* clockwise_verts, const std::size_t vert_num, const Vec3f& face_normal ) {
    std::vector< std::size_t > ret;
    ret.reserve(3); // something reasonable

    Vec3f lineNormal( face_normal.Cross( clockwise_verts[0] - clockwise_verts[vert_num-1] ) );
    if ( Collision3D::TestPoint::BehindPlane( to, clockwise_verts[0], lineNormal ) ) {
        // first check this line
        ret.emplace_back( vert_num-1 );
        ret.emplace_back( 0 );
    }

    // then check the rest.
    for ( std::size_t v=1; v<vert_num; ++v ) {
        lineNormal = face_normal.Cross( clockwise_verts[v] - clockwise_verts[v-1] );
        if ( Collision3D::TestPoint::BehindPlane( to, clockwise_verts[v], lineNormal ) ) {
            // this line, too
            ret.emplace_back( v );
            ret.emplace_back( v-1 );
        }
    }

    SortAndRemoveDuplicates( ret );
    return ret;
}

bool Editor::BeginFlatDeform_SingleVert( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts ) {
    for ( std::size_t v=0; v<flat.NumVerts(); ++v ) {
        if ( Vec3f::SquaredLen( local_click_pos - localVerts[v] ) <= SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT_SQ ) {
            flat_deform_indices.emplace_back( v );
            SetMouseMode( MouseModeT::ENTITY_GRABBED );
            SetMouseGrabType( MouseGrabT::DEFORM_FLAT );
            return true;
        }
    }

    return false;
}

bool Editor::FlatDeformPreview_SingleVert( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts ) const {
    for ( std::size_t v=0; v<flat.NumVerts(); ++v ) {
        if ( Vec3f::SquaredLen( local_click_pos - localVerts[v] ) <= SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT_SQ ) {
            //const Quat rot( Quat::FromAngles_ZYX( flat.GetRotation() ) );
            const Vec3f org = flat.GetOrigin();

            renderer->DrawPoint( localVerts[v] + org, FLAT_DEFORM_VERT_PREVIEW_COLOR, FLAT_DEFORM_VERT_PREVIEW_SIZE );
            return true;
        }
    }

    return false;
}

bool Editor::BeginFlatDeform_Lines( const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm ) {
    flat_deform_indices = GetLineIndicesFacingVert( local_click_pos, localVerts.data(), localVerts.size(), flat_norm );
    if ( flat_deform_indices.size() > 0 ) {
        SetMouseMode( MouseModeT::ENTITY_GRABBED );
        SetMouseGrabType( MouseGrabT::DEFORM_FLAT );
        return true;
    }
    return false;
}

bool Editor::FlatDeformPreview_Lines( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm ) const {
    auto flat_preview_indices = GetLineIndicesFacingVert( local_click_pos, localVerts.data(), localVerts.size(), flat_norm );
    if ( flat_preview_indices.size() > 0 ) {
        //const Quat rot( Quat::FromAngles_ZYX( flat.GetRotation() ) );
        const Vec3f org = flat.GetOrigin();
        for ( auto const & v : flat_preview_indices ) {
            renderer->DrawPoint( localVerts[v] + org, FLAT_DEFORM_VERT_PREVIEW_COLOR, FLAT_DEFORM_VERT_PREVIEW_SIZE );
        }
        return true;
    }
    return false;
}

bool Editor::BeginFlatDeform_AllVerts( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm ) {
    if ( Collision3D::TestPoint::ToPoly( local_click_pos, localVerts.data(), localVerts.size(), flat_norm ) ) {
        for ( std::size_t v=0; v<flat.NumVerts(); ++v )  // preview move of all verts
            flat_deform_indices.emplace_back( v );

        SetMouseMode( MouseModeT::ENTITY_GRABBED );
        SetMouseGrabType( MouseGrabT::DEFORM_FLAT );
        return true;
    }
    return false;
}

bool Editor::FlatDeformPreview_AllVerts( const Flat& flat, const Vec3f& local_click_pos, const std::vector<Vec3f>& localVerts, const Vec3f& flat_norm ) const {
    if ( Collision3D::TestPoint::ToPoly( local_click_pos, localVerts.data(), localVerts.size(), flat_norm ) ) {
        //const Quat rot( Quat::FromAngles_ZYX( flat.GetRotation() ) );
        const Vec3f org = flat.GetOrigin();

        for ( std::size_t v=0; v<flat.NumVerts(); ++v )  // preview move of all verts
            renderer->DrawPoint( localVerts[v] + org, FLAT_DEFORM_VERT_PREVIEW_COLOR, FLAT_DEFORM_VERT_PREVIEW_SIZE );
        return true;
    }
    return false;
}

void Editor::BeginFlatDeform( const Flat& flat ) {
    // ** setup
    const Quat rot( Quat::FromAngles_ZYX( flat.bounds.primitive->GetAngles() ) );
    const Vec3f flat_norm( Vec3f(0,0,-1) * rot );
    const std::vector<Vec3f> localVerts( GetFlatVertList( flat, rot ) );
    click_pos = Input::GetWorldMousePos( localVerts[0] + flat.GetOrigin(), flat_norm );
    const Vec3f local_click_pos(click_pos - flat.GetOrigin());
    selection_origin = localVerts[0]; // while we're at it, we need to set this

    flat_deform_indices.clear();
    flat_deform_indices.reserve( localVerts.size() );

    if ( BeginFlatDeform_SingleVert( flat, local_click_pos, localVerts ) )
        return;

    if ( BeginFlatDeform_AllVerts( flat, local_click_pos, localVerts, flat_norm ) )
        return;

    BeginFlatDeform_Lines( local_click_pos, localVerts, flat_norm );
}

void Editor::ContinueFlatDeform( Flat& flat ) {
    // ** setup
    const Vec3f angles( flat.bounds.primitive->GetAngles() );
    const Quat rot( Quat::FromAngles_ZYX( angles ) );
    const Vec3f norm( Vec3f(0,0,1) * rot );
    const std::vector<Vec3f> localVerts( GetFlatVertList( flat, rot ) );
    const Vec3f old_click_pos( click_pos );
    const Vec3f flat_origin( flat.GetOrigin() );
    click_pos = Input::GetWorldMousePos( localVerts[0] + flat_origin, norm );

    // ** now drag them (either a point, a line, or the whole face)
    Vec3f nudge( click_pos - old_click_pos );
    nudge *= rot.GetInverse(); // we must rotate the nudge by the inverse rotation of the Flat since that is how model data is stored.

    if ( GetSnap() ) {
        // snpping a floor on x or y axis doesn't make sense
        if ( Maths::Approxf( angles.x, 0 ) && Maths::Approxf( angles.y, 0 ) ) {
// todobrandon: snap to grid!
//            const Vec3f unrotated_flat_origin( flat_origin * rot.GetInverse() );
//            nudge += unrotated_flat_origin;
            Vec3f snap_remainder( nudge );
            SnapOrigin( nudge, GetSnapGridDistance() );
            snap_remainder -= nudge;
  //          nudge -= unrotated_flat_origin;
            click_pos -= snap_remainder * rot; // keep any moved distance that was ignored by snapping
        }
    }

    for ( auto v : flat_deform_indices ) {
        ASSERT( v < flat.NumVerts() );
        flat.NudgeLocalVert( v, nudge  );
    }

    selection_origin = localVerts[0];
}

void Editor::RectangleSelect_Drag( void ) {
    vbo_packSelectionRect();
}

void Editor::RectangleSelect( void ) {
    if ( mouseMode != MouseModeT::RECTANGLE_SELECT ) {

        // the end of the line is retrievved when mouse button lifts
        SetMouseMode( MouseModeT::RECTANGLE_SELECT );
        click_pos = Input::GetWindowMousePos();

        return;
    }

    if ( Input::GetMouseButtonState( MOUSE_BUTTON_LEFT ) )
        return;

    // ** button was released - perform selection

    const Vec3f br_ui_pos( Input::GetWindowMousePos() );

    // if picklines are the same on either axis, just do a single select

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal" // this is okay
    if ( br_ui_pos.x == click_pos.x || br_ui_pos.y == click_pos.y ) {
    #pragma GCC diagnostic pop
        std::shared_ptr< Entity > ent = ClickSweep();
        SelectEntity( EntitySelectOptionT::SELECT_SINGLE, ent );
        return;
    }

    const Vec3f tl_ui_pos( click_pos );
    const Vec3f tr_ui_pos( br_ui_pos.x, tl_ui_pos.y, 0.0f );
    const Vec3f bl_ui_pos( tl_ui_pos.x, br_ui_pos.y, 0.0f );

    const Line3f tl_pickline( GetPickLine( click_pos ) );
    const Line3f br_pickline( GetPickLine() );
    const Line3f tr_pickline( GetPickLine( tr_ui_pos ) );
    const Line3f bl_pickline( GetPickLine( bl_ui_pos ) );

    const Vec3f& trn( tr_pickline.vert[0] );
    const Vec3f& trf( tr_pickline.vert[1] );
    const Vec3f& bln( bl_pickline.vert[0] );
    const Vec3f& blf( bl_pickline.vert[1] );
    const Vec3f& tln( tl_pickline.vert[0] );
    const Vec3f& brn( br_pickline.vert[0] );
    const Vec3f& tlf( tl_pickline.vert[1] );
    const Vec3f& brf( br_pickline.vert[1] );

    const auto primitive = Primitive_Box3D( Box3D( tlf, trf, tln, trn, bln, brn, blf, brf ) );

    LinkList< EntityTree* > voxelList;
    game->entTree->GetTraceVoxels( primitive, voxelList, VoxelTypeT::Occupied );
    
    IntersectData coldata;
    coldata.opts |= MatchOptsT::MATCH_IGNORE_INVISIBLE_CLIPMODEL;

    std::vector< const Entity* > checked_ents;

    for ( auto & voxel : voxelList ) {
        if ( ! voxel )
            continue;
            
        for ( auto & item : voxel->GetEntityListRef() ) {
            auto ent = item.lock();
            if ( ! ent )
                continue;
            
            if ( ! AppendUnique( checked_ents, ent.get() ) )
                continue;

            if ( ent && ent->Intersects( primitive ) ) {
                AppendUniqueWptr( selected_ents, ent );
                ent->SetSelectedByEditor( true );
            }
        }
    }

    CMSG_LOG("Editor: %u entities selected\n", selected_ents.Num() );

    SetMouseMode( MouseModeT::NONE );

    UpdateEntSelectionInfo_All();

    vbo_select_rect->Clear();
    vbo_select_rect_colors->Clear();
}

void Editor::vbo_packSelectionRect( void ) {
    if ( mouseMode != MouseModeT::RECTANGLE_SELECT )
        return;

    // **** vertices

    Line3f tl_pickline( GetPickLine( click_pos ) );
    Line3f br_pickline( GetPickLine() );

    Vec3f& tln( tl_pickline.vert[0] );
    Vec3f& brn( br_pickline.vert[0] );

    const Vec3f tl_ui_pos( click_pos );
    const Vec3f br_ui_pos( Input::GetWindowMousePos() );

    const Vec3f tr_ui_pos( br_ui_pos.x, tl_ui_pos.y, 0.0f );
    const Vec3f bl_ui_pos( tl_ui_pos.x, br_ui_pos.y, 0.0f );

    const Line3f tr_pickline( GetPickLine( tr_ui_pos ) );
    Vec3f trn( tr_pickline.vert[0] );

    const Line3f bl_pickline( GetPickLine( bl_ui_pos ) );
    Vec3f bln( bl_pickline.vert[0] );

    // ** move each vertex slightly in front of the camera to make the plane visible

    const float nudge_away_from_camera=0.0001f;

    trn.z -= nudge_away_from_camera;
    bln.z -= nudge_away_from_camera;
    tln.z -= nudge_away_from_camera;
    brn.z -= nudge_away_from_camera;

    // ** now pack them

    vbo_select_rect->Clear();
    vbo_select_rect->Pack( tln );
    vbo_select_rect->Pack( trn );
    vbo_select_rect->Pack( brn );
    vbo_select_rect->Pack( bln );

    vbo_select_rect->MoveToVideoCard();
    ASSERT( vbo_select_rect->Finalized() );

    // **** vertex colors

    const Color4f color(1,0,0,0.5f);

    vbo_select_rect_colors->Clear();
    vbo_select_rect_colors->PackRepeatedly( 4, color );
    vbo_select_rect_colors->MoveToVideoCard();
    ASSERT( vbo_select_rect_colors->Finalized() );
}

void Editor::Draw_SelectionRect( void ) const {
    glDisable( GL_DEPTH_TEST );
    if ( mouseMode != MouseModeT::RECTANGLE_SELECT )
        return;

    if ( !vbo_select_rect->Finalized() ) // colors vbo will also be finalized when this is
        return;

    // ** backup culling and disable it
    const bool culling = glIsEnabled( GL_CULL_FACE );
    int culling_mode;
    glGetIntegerv( GL_CULL_FACE_MODE, &culling_mode );
    glDisable( GL_CULL_FACE );

    // ** draw it
    shaders->UseProg(  GLPROG_GRADIENT  );
    shaders->SendData_Matrices();
    shaders->SetAttrib( "vColor", *vbo_select_rect_colors );
    shaders->SetAttrib( "vPos", *vbo_select_rect );
    shaders->DrawArrays( GL_QUADS, 0, vbo_select_rect->Num() );

    // ** restore culling
    glCullFace( static_cast< GLenum >( culling_mode ) );
    if ( culling ) {
        glEnable( GL_CULL_FACE );
    }
    glEnable( GL_DEPTH_TEST );
}

void Editor::UnhighlightSelectedEnts( void ) {
    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() )
            sptr->SetSelectedByEditor( false );

        link = link->GetNext();
    }
}

bool Editor::ClearSelectionListIfEntityIsNotInIt( Entity& ent ) {
    if ( IsEntitySelected( ent ) ) {
        UnselectAllEntities();
        return true;
    }

    return false;
}

void Editor::UnselectAllEntities( void ) {
    UnhighlightSelectedEnts();
    selected_ents.DelAll();
    UpdateEntSelectionInfo_All(); // clear UI data
    CloseContextMenus();
}

bool Editor::RemoveEntityFromSelectionList( Entity& ent ) {
    Link< std::weak_ptr< Entity > >* link = GetLinkEntityIfSelected( ent );
    if ( link ) {
        ent.SetSelectedByEditor( false );
        link->Del();
        return true;
    }

    return false;
}

bool Editor::SelectEntity_ForDrag( const EntitySelectOptionT opt, std::shared_ptr< Entity >& ent ) {
    if ( ! SelectEntity( opt, ent ) )
        return false;

    SetMouseMode( MouseModeT::ENTITY_GRABBED );
    SetMouseGrabType( MouseGrabT::MOVE_XY );

    return true;
}

bool Editor::SelectEntity( const EntitySelectOptionT opt, std::shared_ptr< Entity >& ent ) {
    if ( ent == nullptr ) {
        if ( opt != EntitySelectOptionT::SELECT_MULTIPLE )
            UnselectAllEntities();
        return false;
    }

    switch ( opt ) {
        case EntitySelectOptionT::SELECT_MULTIPLE:
            if ( IsEntitySelected( *ent ) )
                return RemoveEntityFromSelectionList( *ent );
            break;
        case EntitySelectOptionT::SELECT_GROUP:
            if ( ClearSelectionListIfEntityIsNotInIt( *ent ) )
                return true;
            break;
        case EntitySelectOptionT::SELECT_SINGLE:
            if ( IsEntitySelected( *ent ) )
                break;
            FALLTHROUGH;
        default:
            UnselectAllEntities();
            break;
    }

    AppendUniqueWptr( selected_ents, ent );
    ent->SetSelectedByEditor( true );
    UpdateEntSelectionInfo_All();

    // we move in relation to the first entity in the list
    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( ! sptr )
        return false; // no entity, we're done

    // set these up for functions which depend on it
    selection_origin = sptr->GetOrigin();
    click_pos = Input::GetWorldMousePos( Vec3f(0,0,selection_origin.z) );

    return true;
}

void Editor::DuplicateSelectedEnts( void ) {
    // only copy what is in the list before we append the new stuff.
    // the duplicated entities will be the new selection
    const uint num = selected_ents.Num();

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    for ( uint i=0; i<num; ++i ) {
        std::shared_ptr< Entity > sptr = link->Data().lock();
        if ( sptr ) {
            sptr->SetSelectedByEditor( false );
            std::shared_ptr< Entity > new_sptr = sptr->CloneShared();
            game->InsertEntity( new_sptr );

            Vec3f origin = new_sptr->GetOrigin();
            // move it over so we can select it
            origin.x += 1;
            origin.y += 1;
            new_sptr->SetOrigin( origin );
            SelectEntity( EntitySelectOptionT::SELECT_MULTIPLE, new_sptr );
            new_sptr->Spawn();
        }
        link = link->Del();
    }
}

std::weak_ptr< Entity > Editor::GetSelectedEntity( void ) const {
    if ( selected_ents.Num() < 1 )
        return {};

    if ( selected_ents.Num() > 1 ) {
        WARN("Select only one actor with this option\n" );
        return {};
    }

    const Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    if ( ! link )
        return {};

    return link->Data();
}

void Editor::SetSelectedActorTeamName( const std::string& teamName ) {
    auto wptr = GetSelectedEntity();
    auto ent = wptr.lock();
    if ( !ent )
        return;

    if ( !derives_from< Actor >( *ent ) ) {
        WARN("Only Actors can be set to enemies\n");
        return;
    }

    // todo undo_history
    if ( auto act = std::static_pointer_cast< Actor >( ent ) ) {
        act->SetTeamName( teamName );
        
        if ( teamName == "player" ) {
            //todoundo new Undo_SetTeamName( undo_history, selected_ents );
            game->SetPlayer( std::static_pointer_cast< Actor >( ent ) );
        } else {
            // todo undo_history
            game->UnsetPlayer( static_cast< Actor* >( ent.get() ) );
        }
    }
    
    UpdateEntSelectionInfo_All();
}

void Editor::DeleteSelectedEnts( void ) {
    if ( selected_ents.Num() < 1 )
        return;

    Link< std::weak_ptr< Entity > > *link;

    UndoAction* first = nullptr;
    for ( link = selected_ents.GetFirst(); link != nullptr; link = link->Del() ) {
        auto sptr = link->Data().lock();
        if ( !sptr )
            continue;
        
        auto undo = new Undo_EntityDelete( undo_history, *sptr );
        if ( !first ) {
            first = undo;
        } else {
            undo->Connect( *first );
        }

        game->MarkEntityForDeletion( sptr );
    }
    
    UpdateEntSelectionInfo_All();
}

void CombineEntityModel( Model& dest_model, const Model& source_model, const Entity& master, const Vec3f& master_origin, const Quat& master_unrot );
void CombineEntityModel( Model& dest_model, const Model& source_model, const Entity& master, const Vec3f& master_origin, const Quat& master_unrot ) {
    uint m = dest_model.NumMeshes();
    
    dest_model.Combine( source_model, master.GetModelOffset() );
    
    const Vec3f offset( master_unrot * (master.GetOrigin() - master_origin) );
    const Quat rot( Quat::FromAngles_ZYX( master.bounds.primitive->GetAngles() ) );
    for ( ; m<dest_model.NumMeshes(); ++m ) {
        // undo the rotation of the master entity from the new mesh
        dest_model.TransformMeshVerts( m, [master_unrot,rot](Vec3f& vert){vert *= master_unrot; vert *= rot;} );
        
        // translate the added mesh to it's proper place
        dest_model.TransformMeshVerts( m, [offset](Vec3f& vert){vert += offset;} );
    }
}

void Editor::CombineSelectedEnts( void ) {
    if ( selected_ents.Num() < 2 )
        return;

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    if ( ! link )
        return;
    
    auto master_entity = link->Data().lock();
    if ( !master_entity )
        return;
        
    // todo: todoundo: create an undo/redo for this if we keep this feature.

    // **** Combine them
    const Vec3f master_angles = master_entity->bounds.primitive->GetAngles();
    const Vec3f master_unorigin = master_entity->GetOrigin();
    const Quat master_unrot( Quat::FromAngles_ZYX( -master_angles ) );
    
    std::shared_ptr< Model > model( std::make_shared<Model>() );
    std::shared_ptr< Model > clipModel;
    
    // ** add the first model
    model->Combine( master_entity->GetModel(), master_entity->GetModelOffset() );
    
    // ** add the other models
    bool makeClipModel = false;
    
    link = selected_ents.GetFirst(); // skipping the fist entity, since it is master
        if ( ! link )
            return;
            
    for ( link = link->GetNext(); link != nullptr; link = link->GetNext() ) {
        auto sptr = link->Data().lock();
        if ( ! sptr )
            continue;
            
        if ( sptr->HasClipModel() )
            makeClipModel = true;

        auto src_model = sptr->GetModel();
        if ( ! src_model )
            continue;
        
        CombineEntityModel( *model, *src_model,  *sptr, master_unorigin, master_unrot );
        game->MarkEntityForDeletion( link->Data() );
    }

    model->SetName( CUSTOM_MODEL_COMBINED.c_str() );
    model->PackVBO();
    
    // ** make the clip model, if necessary
    link = selected_ents.GetFirst();
    if ( makeClipModel && link ) {
        clipModel = std::make_shared<Model>();
        clipModel->Combine( master_entity->GetClipModel(), master_entity->GetModelOffset() );

        // we skip the first entity, since it is the master
        for ( link = link->GetNext(); link != nullptr; link = link->GetNext() ) {
            auto sptr = link->Data().lock();
            if ( ! sptr )
                continue;
                
            auto src_model = sptr->GetClipModel();
            if ( !src_model )
                continue;
            
            CombineEntityModel( *clipModel, *src_model, *sptr, master_unorigin, master_unrot );
        }
        
        clipModel->SetName( CUSTOM_MODEL_COMBINED.c_str() );
    }
    
    master_entity->SetModel( model );
    if ( clipModel )
        master_entity->SetClipModel( clipModel );
    
    master_entity->bounds.SetToAABox3D(*model);
    master_entity->bounds.primitive->SetAngle( master_angles );
    
    UnselectAllEntities();
    SelectEntity( EntitySelectOptionT::SELECT_SINGLE, master_entity );
    UpdateEntSelectionInfo_All();
}

void Editor::ReleaseEntity( void ) {
    if ( mouseMode == MouseModeT::ENTITY_GRABBED ) {
        SetMouseMode( MouseModeT::NONE );
        SetMouseGrabType( MouseGrabT::NONE );
    }

    UpdateEntSelectionInfo_All();
    entityGrabActionStarted = false;
}

void Editor::UpdateEntSelectionInfo_Bounds( void ) const {
    ASSERT( txtEntBBoxX );
    ASSERT( txtEntBBoxY );
    ASSERT( txtEntBBoxZ );
    ASSERT( txtEntBoundsOffsetX );
    ASSERT( txtEntBoundsOffsetY );
    ASSERT( txtEntBoundsOffsetZ );
    ASSERT( txtEntAngleX );
    ASSERT( txtEntAngleY );
    ASSERT( txtEntAngleZ );

    if ( selected_ents.Num() < 1 ) {
        return;
    }

    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( ! sptr ) {
        return;
    }

    Vec3f offset = sptr->GetOffset();
    std::string tmp( std::to_string( offset.x ) );
    String::TrimTrailing( tmp, "0" );
    txtEntBoundsOffsetX->SetLabel( tmp );

    tmp = std::to_string( offset.y );
    String::TrimTrailing( tmp, "0" );
    txtEntBoundsOffsetY->SetLabel( tmp );

    tmp = std::to_string( offset.z );
    String::TrimTrailing( tmp, "0" );
    txtEntBoundsOffsetZ->SetLabel( tmp );

    AABox3D aabox(0,0,0,0,0,0);
    if ( sptr->bounds.IsAABox3D() )
        aabox = sptr->bounds.GetAABox3D_Local();

    float val = aabox.max.x - aabox.min.x; val /= 2;
    tmp = std::to_string( val );
    String::TrimTrailing( tmp, "0" );
    txtEntBBoxX->SetLabel( tmp );

    val = aabox.max.y - aabox.min.y; val /= 2;
    tmp = std::to_string( val );
    String::TrimTrailing( tmp, "0" );
    txtEntBBoxY->SetLabel( tmp );

    val = aabox.max.z - aabox.min.z; val /= 2;
    tmp = std::to_string( val );
    String::TrimTrailing( tmp, "0" );
    txtEntBBoxZ->SetLabel( tmp );

    // ** angle

    Vec3f angle = sptr->bounds.primitive->GetAngles();
    tmp = std::to_string( angle.x );
    String::TrimTrailing( tmp, "0" );
    txtEntAngleX->SetLabel( tmp );

    tmp = std::to_string( angle.y );
    String::TrimTrailing( tmp, "0" );
    txtEntAngleY->SetLabel( tmp );

    tmp = std::to_string( angle.z );
    String::TrimTrailing( tmp, "0" );
    txtEntAngleZ->SetLabel( tmp );
}

void Editor::UpdateEntSelectionInfo_ModelAndMaterials( void ) const {
    // ** update the materials listbox with materials available to this entitie's model

    ASSERT( dboxModels );
    ASSERT( dboxMaterials );
    ASSERT( lblEntType );
    ASSERT( txtEntName );
    ASSERT( txtEntOriginX );
    ASSERT( txtEntOriginY );
    ASSERT( txtEntOriginZ );

    //dboxModels->Clear(); // don't do this, it is populated only once, at editor load
    dboxMaterials->Clear();
    lblEntType->SetLabel("");
    txtEntName->SetLabel("");
    txtEntOriginX->SetLabel("");
    txtEntOriginY->SetLabel("");
    txtEntOriginZ->SetLabel("");

    if ( selected_ents.Num() < 1 )
        return;

    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( ! sptr )
        return;

    if ( !sptr->HasModel() || derives_from< Actor >( *sptr ) ) {
        CMSG("(Can Ignore): Not updating editor model list: this feature is for non-actor entities that have a model.\n");
        dboxModels->SetLabel( "" );
        return;
    }

    std::shared_ptr< const Model > model = sptr->GetModel();
    if ( model ) {
        const std::string model_name = model->GetName();
        const bool custom_model = String::LeftIs( model_name, CUSTOM_MODEL_ );
        const bool allowAllMaterials = custom_model || derives_from< Projector >( *sptr );
        dboxModels->SetLabel( model_name );

        LinkList< std::string > names;
        materialManager.GetAllNames( names );
        Link< std::string > *link = names.GetFirst();

        while ( link != nullptr ) {
            if ( allowAllMaterials || String::Left( link->Data(), model_name.size() ) == model_name ) {
                dboxMaterials->Add( { link->Data(), link->Data() } );
            }

            link = link->Del();
        }

        if ( sptr->GetMaterial() )
            dboxMaterials->SetLabel( sptr->GetMaterial()->GetName() );
    }

    dboxMaterials->Refresh();
    UpdateEntSelectionInfo_Basics();
}

void Editor::UpdateEntSelectionInfo_Basics( void ) const {
    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( ! sptr )
        return;

    // ** Name
    lblEntType->SetLabel( get_classname( *sptr ) );
    txtEntName->SetLabel( sptr->GetName() );

    // ** Origin
    const Vec3f entOrigin = sptr->GetOrigin();
    std::string tmp( std::to_string( entOrigin.x ) );
    String::TrimTrailing( tmp, "0" );
    txtEntOriginX->SetLabel( tmp );
    tmp = std::to_string( entOrigin.y );
    String::TrimTrailing( tmp, "0" );
    txtEntOriginY->SetLabel( tmp );
    tmp = std::to_string( entOrigin.z );
    String::TrimTrailing( tmp, "0" );
    txtEntOriginZ->SetLabel( tmp );
}

void Editor::UpdateEntSelectionInfo_EntVals( void ) {
    // ** make sure the UI is created
    if ( ! wndEntVals )
        Create_EntValsUI();

    ASSERT( wndEntVals_listBox );

    wndEntVals_listBox->Clear();

    // ** populate entVal listbox with the first entity selected
    if ( auto sptr = GetFirstEntityPtr( selected_ents ) ) {
        for ( auto& pair : sptr->entVals ) {
            const std::string tooltip = sptr->GetClassToolTip( pair.first.c_str() );
            wndEntVals_listBox->Add( { pair.first, "", tooltip, pair.second } );
        }
    }

    wndEntVals_listBox->Refresh();
}

void Editor::UpdateEntSelectionInfo_Bind( void ) const {
    ASSERT( btnEntitysBind );

    btnEntitysBind->SetLabel("");

    // must only have one trigger selected
    if ( selected_ents.Num() > 1 ) {
        wndBind->SetVisible( false );
        return;
    } else {
        wndBind->SetVisible( true );
    }

    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( !sptr ) {
        wndBind->SetVisible( false );
        return;
    }

    sptr = sptr->GetBindMaster();
    if ( !sptr ) {
        wndBind->SetVisible( false );
        return;
    }

    btnEntitysBind->SetLabel( sptr->GetIdentifier() );
}

void Editor::UpdateEntSelectionInfo_TriggerTargets( void ) const {
    ASSERT( wndTargets );

    // ** delete anything that lower than the titlebar or the ADD button.
    const uint window_height = 1;
    const uint button_height = 1;
    const uint total_height = button_height + window_height;
    wndTargets->DeleteElementsBelowYPos( WIDGET_STANDARD_HEIGHT * total_height );

    // ** must only have one trigger selected
    if ( selected_ents.Num() != 1 ) {
        wndTargets->SetVisible( false );
        return;
    }

    // ** cast to trigger, if it is indeed a trigger
    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( ! sptr )
        return;

    if ( ! derives_from< Trigger >( *sptr ) ) {
        wndTargets->SetVisible( false );
        return;
    }

    wndTargets->SetVisible( true );

    const Trigger* trigger = static_cast< const Trigger* >( sptr.get() );

    // ** populate

    // items must be set to not visible if the window is not visible or is collapsed
    const bool vis = wndTargets->IsVisible() && !wndTargets->IsCollapsed();

    const std::weak_ptr< Entity > *target_smp_p;

    Label* label;

    const Link< std::weak_ptr< Entity > > *tlink = trigger->targets.GetFirst();
    while ( tlink != nullptr ) {
        target_smp_p = &tlink->Data();
        if ( target_smp_p == nullptr ) {
            tlink = tlink->GetNext();
            continue;
        }

        if ( auto target = target_smp_p->lock() ) {

            // ** Key Label
            label = new TriggerTargetButton( target->GetIdentifier().c_str(), *target_smp_p, SECOND_INNER_TOOLBOX_ITEM_WIDTH );
            wndTargets->children->Add( label );
            label->SetOrigin( widget_h_spacing, wndTargets->next_element_vpos, 0 );
            label->SetVisible( vis );

            // if we're collapsed, we need to append visibility information
            if ( wndTargets->IsCollapsed() ) {
                wndTargets->element_visibility.Append( true ); // for the button
            }

            wndTargets->next_element_vpos += WIDGET_STANDARD_HEIGHT;
        }
        tlink = tlink->GetNext();
    }
}

Flat* Editor::GetSelectedFlat( void ) {
    // ** must only have one flat selected
    if ( selected_ents.Num() != 1 )
        return nullptr;

    // ** cast to flat, if it is indeed a flat
    if ( auto sptr = GetFirstEntityPtr( selected_ents ) ) {
        if ( ! derives_from< Flat >( *sptr ) ) {
            wndFlats->SetVisible( false );
            return nullptr;
        }

        return static_cast< Flat* >( sptr.get() );
    }

    return nullptr;
}

const Flat* Editor::GetSelectedFlat( void ) const {
    // ** must only have one flat selected
    if ( selected_ents.Num() != 1 )
        return nullptr;

    // ** cast to flat, if it is indeed a flat
    if ( auto sptr = GetFirstEntityPtr( selected_ents ) ) {
        if ( ! derives_from< Flat >( *sptr ) ) {
            wndFlats->SetVisible( false );
            return nullptr;
        }

        return static_cast< const Flat* >( sptr.get() );

    }

    return nullptr;
}

void Editor::SetFlatUV( void ) {
    Flat* flat = GetSelectedFlat();

    if ( !flat ) {
        wndFlats->SetVisible( false );
        return;
    }

    wndFlats->SetVisible( true );
    
    const Vec2f scale(
        String::ToFloat( txtFlatUVScaleHorizontal->GetLabel() ),
        String::ToFloat( txtFlatUVScaleVertical->GetLabel() )
    );
	
    const Vec2f scroll(
        String::ToFloat( txtFlatUVAlignHorizontal->GetLabel() ),
        String::ToFloat( txtFlatUVAlignVertical->GetLabel() )
    );
    
    flat->SetTexAlign( scroll );
    flat->SetTexScale( scale );
}

void Editor::UpdateEntSelectionInfo_Flats( void ) const {
    const Flat* flat = GetSelectedFlat();

    if ( !flat ) {
        wndFlats->SetVisible( false );
        return;
    }

    wndFlats->SetVisible( true );
    
    Vec2f scale = flat->GetTexScale();
    Vec2f align = flat->GetTexAlign();

    std::string str;

    str = std::to_string( scale.x );
    String::TrimTrailing( str, "0" );
    if ( String::RightIs( str, "." ) ) {
        str += '0';
    }
    txtFlatUVScaleHorizontal->SetLabel( str.c_str() );
    
    str = std::to_string( scale.y );
    String::TrimTrailing( str, "0" );
    if ( String::RightIs( str, "." ) ) {
        str += '0';
    }
    txtFlatUVScaleVertical->SetLabel( str.c_str() );
    
    str = std::to_string( align.x );
    String::TrimTrailing( str, "0" );
    if ( String::RightIs( str, "." ) ) {
        str += '0';
    }
    txtFlatUVAlignHorizontal->SetLabel( str.c_str() );
    
    str = std::to_string( align.y );
    String::TrimTrailing( str, "0" );
    if ( String::RightIs( str, "." ) ) {
        str += '0';
    }
    txtFlatUVAlignVertical->SetLabel( str.c_str() );
}

void Editor::UpdateEntSelectionInfo_TriggerChain( void ) const {
    ASSERT( wndTriggerChain );

    // ** delete anything that lower than the titlebar, the label, or the ADD button.
    const uint window_height = 1;
    const uint button_height = 1;
    const uint label_height = 4;
    const uint total_height = label_height  + button_height + window_height;
    wndTriggerChain->DeleteElementsBelowYPos( WIDGET_STANDARD_HEIGHT * total_height );

    // ** must only have one trigger selected
    if ( selected_ents.Num() != 1 ) {
        wndTriggerChain->SetVisible( false );
        return;
    }

    // ** cast to trigger, if it is indeed a trigger
    auto sptr = GetFirstEntityPtr( selected_ents );

    if ( ! sptr )
        return;

    if ( !derives_from< Trigger >( *sptr ) ) {
        wndTriggerChain->SetVisible( false );
        return;
    }

    wndTriggerChain->SetVisible( true );

    const Trigger* trigger = static_cast< const Trigger* >( sptr.get() );

    // ** populate

    // items must be set to not visible if the window is not visible or is collapsed
    const bool vis = wndTriggerChain->IsVisible() && !wndTriggerChain->IsCollapsed();

    Label* label;

    const Link< std::weak_ptr< Entity > > *tlink = trigger->triggerChain.GetFirst();
    while ( tlink != nullptr ) {
        auto weakp = &tlink->Data();
        if ( weakp == nullptr ) {
            tlink = tlink->GetNext();
            continue;
        }

        if ( auto target_sptr = weakp->lock() ) {
            // ** Key Label
            label = new TriggerTargetButton( target_sptr->GetIdentifier().c_str(), target_sptr, SECOND_INNER_TOOLBOX_ITEM_WIDTH );
            wndTriggerChain->children->Add( label );
            label->SetOrigin( widget_h_spacing, wndTriggerChain->next_element_vpos, 0 );
            label->SetVisible( vis );

            // if we're collapsed, we need to append visibility information
            if ( wndTriggerChain->IsCollapsed() ) {
                wndTriggerChain->element_visibility.Append( true ); // for the button
            }

            wndTriggerChain->next_element_vpos += WIDGET_STANDARD_HEIGHT;
            tlink = tlink->GetNext();
        }
    }
}

void Editor::UpdateEntSelectionInfo_All( void ) {
    UpdateEntSelectionInfo_ModelAndMaterials();
    UpdateEntSelectionInfo_Basics();
    UpdateEntSelectionInfo_EntVals();
    UpdateEntSelectionInfo_TriggerTargets();
    UpdateEntSelectionInfo_TriggerChain();
    UpdateEntSelectionInfo_Flats();
    UpdateEntSelectionInfo_Bounds();
    UpdateEntSelectionInfo_Bind();
}

bool Editor::DragEntity( void ) {
    if ( selected_ents.Num() < 1 ) {
        ReleaseEntity(); // resets mouse mode
        return false;
    }
    
    Vec3f newClickPos = Input::GetWorldMousePos( Vec3f(0,0,selection_origin.z) );

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    UndoAction* first_undo = nullptr;
    
    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {

            link = link->GetNext();
            
            const auto cur_origin = sptr->GetOrigin();
            
            if ( !entityGrabActionStarted ) {
                using type = Vec3f;
                using setter = void (Entity::*)( const type& );
                using getter = type (Entity::*)( void ) const;
                auto undo_drag = new Undo_EntityProperty<setter,getter,type>( undo_history, *sptr, &Entity::SetOrigin, &Entity::GetOrigin, cur_origin );

                if ( first_undo ) {
                    undo_drag->Connect( *first_undo );
                } else {
                    first_undo = undo_drag;
                }
            }
            
            Vec3f drag_to( cur_origin - click_pos + newClickPos );

            if ( GetSnap() ) {
                Vec3f snap_remainder = drag_to;
                SnapOrigin( drag_to, GetSnapGridDistance() );

                // on the last link of the class, keep any moved distance that was ignored by snapping
                if ( ! link ) {
                    snap_remainder -= drag_to;
                    newClickPos -= snap_remainder;
                }
            }

            // if the origin is outside the voxeltree, do not allow the move
            if ( game->entTree->CheckBounds( drag_to ) )
                sptr->SetOrigin( drag_to );
        } else {
            link = link->Del();
        }
    }
    
    entityGrabActionStarted = true;
    click_pos = newClickPos;

    return true;
}

bool Editor::DragEntityVertical( void ) {
    if ( selected_ents.Num() < 1 ) {
        ReleaseEntity(); // resets mouse mode
        return false;
    }

    Vec3f newClickPos = Input::GetUIMousePos();

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {
            link = link->GetNext();

            Vec3f drag_amount( sptr->GetOrigin() );
            drag_amount.z += click_pos.y / 10.0f; // scale the movement down a tad. nothing special about 10.0f
            drag_amount.z -= newClickPos.y / 10.0f;

            if ( GetSnap() ) {
                float tmp = drag_amount.z;
                SnapOrigin( drag_amount, GetSnapGridDistance() );
                // on the last link of the class, keep any moved distance that was ignored by snapping
                // this isn't perfect, but it works well enough.
                if ( ! link ) {
                    #pragma GCC diagnostic push
                    #pragma GCC diagnostic ignored "-Wfloat-equal" // fine
                    if ( tmp == drag_amount.z ) {
                    #pragma GCC diagnostic pop
                        newClickPos -= newClickPos.z;
                    }
                }
            }

            sptr->SetOrigin( drag_amount );
        } else {
            link = link->Del();
        }
    }

    entityGrabActionStarted = true;
    click_pos = newClickPos;

    return true;
}

void Editor::UpdateEntBBoxFromModel( void ) {
    if ( selected_ents.Num() != 1 ) {
        return;
    }

    if ( auto sptr = GetFirstEntityPtr( selected_ents ) )
        UpdateEntSelectionInfo_Bounds();
}

void ValidateBoundsInput( float& units );
void ValidateBoundsInput( float& units ) {
    if ( units < -BOX_BOUNDS_MIN )
        units = -units;

    if ( units < BOX_BOUNDS_MIN )
        units = BOX_BOUNDS_MIN;
}

void Editor::UpdateEntBBox( void ) {
    if ( selected_ents.Num() != 1 ) {
        return;
    }

    auto sptr = GetFirstEntityPtr( selected_ents );
    if ( !sptr )
        return;

    if ( sptr->bounds.IsAABox3D() ) {

        ASSERT(txtEntBBoxX);
        ASSERT(txtEntBBoxY);
        ASSERT(txtEntBBoxZ);

        float x=String::ToFloat( txtEntBBoxX->GetLabel() );
        float y=String::ToFloat( txtEntBBoxY->GetLabel() );
        float z=String::ToFloat( txtEntBBoxZ->GetLabel() );

        ValidateBoundsInput( x );
        ValidateBoundsInput( y );
        ValidateBoundsInput( z );

        sptr->bounds.SetToAABox3D( -x, x, -y, y, -z, z );
    }

    // both bbox and sphere utilize an offset
    sptr->SetOffset(
        Vec3f(
            String::ToFloat( txtEntBoundsOffsetX->GetLabel() )
            , String::ToFloat( txtEntBoundsOffsetY->GetLabel() )
            , String::ToFloat( txtEntBoundsOffsetZ->GetLabel() )
        )
    );
}

void Editor::UpdateEntAngle( void ) {
    if ( selected_ents.Num() != 1 ) {
        return;
    }

    if ( auto sptr = GetFirstEntityPtr( selected_ents ) ) {
        ASSERT(txtEntAngleX);
        ASSERT(txtEntAngleY);
        ASSERT(txtEntAngleZ);

        float x=String::ToFloat( txtEntAngleX->GetLabel() );
        float y=String::ToFloat( txtEntAngleY->GetLabel() );
        float z=String::ToFloat( txtEntAngleZ->GetLabel() );

        sptr->bounds.primitive->SetAngle( x,y,z );
    }
}

void Editor::UpdateEntName( void ) {
    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    UndoAction* first_undo = nullptr;
    
    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {
            const auto old_name = sptr->GetName();
            
            using type = std::string;
            using setter = void (Entity::*)( const type& );
            using getter = type (Entity::*)( void ) const;
            auto undo_name = new Undo_EntityProperty<setter,getter,type>( undo_history, *sptr, &Entity::SetName, &Entity::GetName, old_name );
            
            if ( first_undo ) {
                undo_name->Connect( *first_undo );
            } else {
                first_undo = undo_name;
            }
            
            sptr->SetName( txtEntName->GetLabel() );
        }
        link = link->GetNext();
    }
}

void Editor::UpdateEntOrigin( void ) {
    if ( selected_ents.Num() != 1 ) {
        ERR("You can only manually set the origin of entities one at a time.\n");
        return;
    }

    if ( auto sptr = GetFirstEntityPtr( selected_ents ) ) {


        //todoundo new Undo_EntityDrag( undo_history, selected_ents );

        sptr->SetOrigin(
            Vec3f(
                String::ToFloat( txtEntOriginX->GetLabel() )
                , String::ToFloat( txtEntOriginY->GetLabel() )
                , String::ToFloat( txtEntOriginZ->GetLabel() )
            )
        );
    }
}

void Editor::SelectTriggerTarget( void ) {
    SetMouseMode( MouseModeT::ADD_TRIGGER_TARGET );
}

void Editor::SelectTriggerChain( void ) {
    SetMouseMode( MouseModeT::ADD_TRIGGER_CHAIN );
}

bool Editor::RotateEntityLocal( const RotationAxisT local_axis ) {
    if ( selected_ents.Num() < 1 ) {
        ReleaseEntity(); // also resets mouse mode
        return false;
    }

    // in order to rotate multiple entities, we have to do two rotations:
    // one for just setting the angle of the entity in the world,
    // the other for actually translating their origins around their collective center

    Vec3f new_drag_pos = Input::GetWindowMousePos();
    float drag_diff = new_drag_pos.y - drag_pos.y;

    const float snap_angle = 15;
    
    if ( GetSnap() ) {
        const float diff = new_drag_pos.y - click_pos.y;
        const float remainder = fmodf( diff, snap_angle );
        if ( Maths::Approxf( drag_diff, 0.0 ) ) {
            return false;
        }
        drag_diff -= remainder; // subtract remainter to make it snap
        new_drag_pos.y -= remainder; // subtract remainter so it'll be accunted for next time
    }
    
    drag_pos.y = new_drag_pos.y;
    
    const bool entity_group = selected_ents.Num() > 1;

    for ( auto & ent_sptr : selected_ents ) {
        auto ent = ent_sptr.lock();
        if ( ! ent )
            continue;
        
        Quat quat;
        switch ( local_axis ) {
            case RotationAxisT::X:
                ent->bounds.primitive->NudgeRoll( drag_diff );
                quat.SetFromAngles_ZYX(drag_diff,0,0);
                break;
            case RotationAxisT::Y:
                ent->bounds.primitive->NudgePitch( drag_diff );
                quat.SetFromAngles_ZYX(0,drag_diff,0);
                break;
            case RotationAxisT::Z:
                ent->bounds.primitive->NudgeYaw( drag_diff );
                quat.SetFromAngles_ZYX(0,0,drag_diff);
                break;
            default:
                ERR_DIALOG("Invalid axis\n");
                return false;
        }
        
        if ( entity_group ) {
            Vec3f newOrigin = ent->GetOrigin();
            newOrigin -= selection_origin;
            newOrigin *= quat;
            newOrigin += selection_origin;
            ent->SetOrigin( newOrigin );
            continue;
        }
        
        if ( ! GetSnap() )
            continue;

        switch ( local_axis ) {
            case RotationAxisT::X: ent->bounds.primitive->SnapRoll( snap_angle ); break;
            case RotationAxisT::Y: ent->bounds.primitive->SnapPitch( snap_angle ); break;
            case RotationAxisT::Z: ent->bounds.primitive->SnapYaw( snap_angle ); break;
            default: ERR("Editor::RotateEntityLocal: Invalid axis\n");
        }
    }

    return true;
}

void Editor::SetEntitySelectionModel( const char* model_name ) {
    if ( selected_ents.Num() < 1 )
        return;

    std::shared_ptr< const Model > new_model = modelManager.Get( model_name );
    if ( !new_model )
        return;

    std::shared_ptr< const Material > new_mtl = materialManager.Get( model_name );
    if ( !new_mtl ) {
        ERR("Couldn't find default material for model (%s)\n", model_name );
    }

    UndoAction* first_undo = nullptr;
    
    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {
            auto old_model = sptr->GetModel();
            auto old_mtl = sptr->GetMaterial();
            
            if ( sptr->SetModel( new_model ) ) {
                using type = std::shared_ptr< const Model >;
                using setter = bool (Entity::*)( const type& );
                using getter = type (Entity::*)( void ) const;
                auto undo_model = new Undo_EntityProperty<setter,getter,type>( undo_history, *sptr, &Entity::SetModel, &Entity::GetModel, old_model );
                
                using mtl_type = std::shared_ptr< const Material >;
                using mtl_setter = bool (Entity::*)( const mtl_type& );
                using mtl_getter = mtl_type (Entity::*)( void ) const;
                auto undo_material = new Undo_EntityProperty<mtl_setter,mtl_getter,mtl_type>( undo_history, *sptr, &Entity::SetMaterial, &Entity::GetMaterial, old_mtl );
                
                if ( first_undo ) {
                    undo_model->Connect( *first_undo );
                } else {
                    first_undo = undo_material;
                }
                
                undo_material->Connect( *undo_model );
                
                sptr->SetMaterial( new_mtl );
            }
            
            link = link->GetNext();
        } else {
            link = link->Del();
        }
    }

    UpdateEntSelectionInfo_ModelAndMaterials();
}

void Editor::SetEntitySelectionMaterial( const char* mtl_name ) {
    if ( selected_ents.Num() < 1 )
        return;

    std::shared_ptr< const Material > new_mtl = materialManager.Get( mtl_name );
    if ( !new_mtl )
        return;

    UndoAction* first_undo = nullptr;

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    while ( link != nullptr ) {
        if ( auto ent = link->Data().lock() ) {
            auto old_mtl = ent->GetMaterial();

            using mtl_type = std::shared_ptr< const Material >;
            using mtl_setter = bool (Entity::*)( const mtl_type& );
            using mtl_getter = mtl_type (Entity::*)( void ) const;
            auto undo_material = new Undo_EntityProperty<mtl_setter,mtl_getter,mtl_type>( undo_history, *ent, &Entity::SetMaterial, &Entity::GetMaterial, old_mtl );
            
            if ( first_undo ) {
                undo_material->Connect( *first_undo );
            } else {
                first_undo = undo_material;
            }
            
            ent->SetMaterial( new_mtl );
            link = link->GetNext();
        } else {
            link = link->Del();
        }
    }
}

void Editor::TranslateToSelectedEntsBindMaster( void ) {
    if ( selected_ents.Num() != 1 ) {
        WARN("Must select only one entity.\n");
        return;
    }

    if ( auto sptr = GetFirstEntityPtr( selected_ents ) ) {
        sptr = sptr->GetBindMaster();
        if ( sptr ) {
            renderer->camera.LookAt( sptr->GetOrigin() );
        }
    }
}

void Editor::UnbindEntSelectionsFromMaster( void ) {
    if ( selected_ents.Num() < 1 ) {
        WARN("No entity selected to unbind.\n");
        return;
    }

    //todoundo new Undo_EntityBind( undo_history, selected_ents );

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() )
            sptr->UnbindFromMaster();

        link = link->GetNext();
    }

    UpdateEntSelectionInfo_Bind();
}

void Editor::SelectMasterToBindEntSelectionsTo( void ) {
    SetMouseMode( MouseModeT::BIND_TO_MASTER );
}

void Editor::EntValListBoxNew_PressEnter( void ) {
    wndEntVals_listBox->EntValListBoxNew_PressEnter();
}

uint Editor::ApplyEntValToSelectedEnts( const char* key, const char* val ) { //toNamedSetter
    ASSERT( key );
    ASSERT( val );

    Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();

    uint count = 0;

    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() ) {

            link = link->GetNext();

            if ( val[0] == '\0' ) {
                sptr->entVals.Remove( key );
                continue;
            }

            sptr->entVals.Set( key, val );
            ++count;
        } else {
            link = link->Del();
        }
    }

    // update the info on the UI
    UpdateEntSelectionInfo_EntVals();

    return count;
}

void Event_ModelSelected( const std::vector< std::string >& args ) {
    if ( args.size() > 0 )
        editor->SetEntitySelectionModel( args[0].c_str() );
}

void Event_MaterialSelected( const std::vector< std::string >& args ) {
    if ( args.size() > 0 )
        editor->SetEntitySelectionMaterial( args[0].c_str() );
}

void Editor::RepopulateModelsDropBox( void ) {
    dboxModels->Clear();
    LinkList< std::string > names;
    modelManager.GetAllNames( names );
    for ( const auto & name : names )
        dboxModels->Add( {name,name} );
    dboxModels->Refresh();
}

const Link< std::weak_ptr< Entity > >* Editor::GetLinkEntityIfSelected( Entity& ent ) const {
    const uint id = ent.GetIndex();

    const Link< std::weak_ptr< Entity > > *link = selected_ents.GetFirst();
    while ( link != nullptr ) {
        if ( auto sptr = link->Data().lock() )
            if ( sptr->GetIndex() == id )
                return link;
        link = link->GetNext();
    }

    return nullptr;
}

MouseGrabT Editor::GetMouseGrabType( void ) const {
    return mouseGrabType;
}

void Editor::SetMouseGrabType( const MouseGrabT to ) {
    mouseGrabType = to;
}

bool Editor::IsEntitySelected( Entity& ent ) const {
    return GetLinkEntityIfSelected( ent ) != nullptr;
}

Link< std::weak_ptr< Entity > >* Editor::GetLinkEntityIfSelected( Entity& ent ) {
    return const_cast< Link< std::weak_ptr< Entity > >* >(
        static_cast< const Editor* >( this )->GetLinkEntityIfSelected( ent )
    );
}

#endif // MONTICELLO_EDITOR
