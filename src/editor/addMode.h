// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_EDITOR_ADDMODE
#define SRC_EDITOR_ADDMODE

#ifdef MONTICELLO_EDITOR

#include "../base/main.h"

void Event_EntityInfoSelected( const std::vector< std::string >& args );

extern const float OFFSET_RADIUS_BUFFER;

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_ADDMODE
