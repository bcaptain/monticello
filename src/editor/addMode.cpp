// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifdef MONTICELLO_EDITOR
#include "./clib/src/warnings.h"
#include "./addMode.h"
#include "./editor.h"
#include "../input/input.h"
#include "./undo/undo_entityAdd.h"
#include "../entities/entity.h"
#include "../entities/entityInfo.h"
#include "../entities/entityInfoManager.h"
#include "../game.h"

const std::string WINDOW_ENTADD_NAME("wndAddOpts");
const float OFFSET_RADIUS_BUFFER = EP;

void Editor::AddMode( void ) {
    btnAddMode->SetFaceBottomColor( COLOR_RADIOBUTTON_GRADIENT_B );
    wndToolBox->ShowNamedElement( WINDOW_ENTADD_NAME, true );
}

bool Editor::CheckInputEvent_AddMode( const std::string& action ) {
    if ( action =="PlaceEnt" ) {
        SpawnEntity();
        return true;
    }

    if ( action == "GhostRotCWX" ) {
        RotateGhostSelectionX( 1.0f, editor->GetSnap() );
        return true;
    }

    if ( action == "GhostRotCCWX" ) {
        RotateGhostSelectionX( -1.0f, editor->GetSnap() );
        return true;
    }

    if ( action == "GhostRotCWY" ) {
        RotateGhostSelectionY( 1.0f, editor->GetSnap() );
        return true;
    }

    if ( action == "GhostRotCCWY" ) {
        RotateGhostSelectionY( -1.0f, editor->GetSnap() );
        return true;
    }

    if ( action == "GhostRotCWZ" ) {
        RotateGhostSelectionZ( 1.0f, editor->GetSnap() );
        return true;
    }

    if ( action == "GhostRotCCWZ" ) {
        RotateGhostSelectionZ( -1.0f, editor->GetSnap() );
        return true;
    }

    return false;
}

void Event_EntityInfoSelected( const std::vector< std::string >& args ) {
    if ( args.size() > 0 )
        editor->SetSelectedEntInfo( args[0].c_str() );
}

void Editor::SetSelectedEntInfo( const char* name ) {
    lblEntFileSelected->SetLabel( name );

    if ( String::Len( name ) == 0 ) {
        selectedEntInfo = nullptr;
        return;
    }

    // ** if the entity has already been loaded, return it's info
    selectedEntInfo = entityInfoManager.Get( name );
    if ( selectedEntInfo ) {
        addSelectionAngles.x = 0;
        auto mval = selectedEntInfo->GetMemberValue("roll");
        if ( mval.size() > 0 )
            addSelectionAngles.x += String::ToFloat( mval );

        addSelectionAngles.y = 0;
        mval = selectedEntInfo->GetMemberValue("pitch");
        if ( mval.size() > 0 )
            addSelectionAngles.y += String::ToFloat( mval );

        addSelectionAngles.z = 0;
        mval = selectedEntInfo->GetMemberValue("yaw");
        if ( mval.size() > 0 )
            addSelectionAngles.z += String::ToFloat( mval );

    } else {
        WARN("Could not find Entity: %s.\n", name );
    }

    UpdateGhostSelectionModel();
}

void Editor::RepopulateEntityInfoDropBox( void ) {
    dboxEntBrowser->Clear();
    LinkList< std::string > names;
    entityInfoManager.GetAllNames( names );
    for ( const auto & name : names )
        dboxEntBrowser->Add( {name,name} );
    dboxEntBrowser->Sort();
    dboxEntBrowser->Refresh();
}

bool Editor::SpawnEntity( void ) {
    if ( !selectedEntInfo ) {
        CMSG_LOG("No entity selected to add.\n" );
        return false;
    }

    auto ent = entityInfoManager.Load( *selectedEntInfo, addSelectionOrigin + Vec3f(0,0,OFFSET_RADIUS_BUFFER) );
    ent->bounds.primitive->SetAngle( addSelectionAngles );
    ent->Spawn();
    if ( editor )
        new Undo_EntityAdd( editor->undo_history, *ent );

    return true;
}

#endif // MONTICELLO_EDITOR
