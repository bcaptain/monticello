// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"
#include "./ui/editmode.h"
#include "../ui/dialog/common.h"
#include "../input/input.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../math/collision/collision.h"
#include "../nav/navMesh.h"

const Color4f NAV_POLY_COLOR_SELECTED( 0.0f,1.0f,0,0.5f );
const Color4f NAV_POLY_VERTEX_MERGE_COLOR(1,0,0,1);
const float NAV_POLY_VERTEX_MERGE_SIZE(10);

void Dialog_DelNavMesh( void ) {
    auto delNavMeshFunc = std::bind(
        static_cast< void(Editor::*)( void ) >( &Editor::DelNavMesh )
        , editor
    );

    auto DialogFunc_Cancel = [](){};

    auto dlg = Dialog::Create( "Delete this entire nav mesh?" );
    dlg->AddButton( new DialogButton<decltype(delNavMeshFunc) >( "Yes", *dlg, delNavMeshFunc, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton<decltype(DialogFunc_Cancel) >( "No", *dlg, DialogFunc_Cancel, ButtonTypeT::Decline ) );
}

void Editor::NavMeshMode( void ) {
    btnNavMeshMode->SetFaceBottomColor( COLOR_RADIOBUTTON_GRADIENT_B );
    wndToolBox->ShowNamedElement( WINDOW_NAVMESH_NAME, true );
}

void Editor::AddNavMeshNode( void ) {
    auto newMesh = std::make_shared<NavMesh>();

    // put it in the middle of the screen
    const auto vec = renderer->camera.GetFocusOrigin().ToVec2();

    newMesh->AppendVert( Vec2f(1,0) + vec );
    newMesh->AppendVert( Vec2f(-1,0) + vec );
    newMesh->AppendVert( Vec2f(0,1) + vec );

    NavMesh::meshes.Append( newMesh );
}

void Editor::Confirm_DelNavMesh( void ) {
    Dialog_DelNavMesh();
}


void Editor::DelNavMesh( void ) {
    auto poly = selected_nav_poly.lock();
    if ( !poly )
        return;

    const auto & mesh = poly->GetMaster();
    if ( ! mesh )
        return;

    for ( auto lnk = NavMesh::meshes.GetFirst(); lnk != nullptr; lnk = lnk->GetNext() ) {
        if ( auto checkMesh = lnk->Data() ) {
            if ( *checkMesh == *mesh ) {
                lnk->Del(); // delete the mesh
                return;
            }
        }
    }
}

void Editor::DoneDeformNavPoly( void ) {
    if ( navpoly_deform_indicies.size() == 1 ) { // if moving a single vert
        const int index = GetVertIndexForMerge();
        if ( index >= 0 ) {
            NavMesh::MergeVert( navpoly_deform_indicies[0], static_cast< uint >( index ) );
        }
    }

    if ( mouseGrabType == MouseGrabT::DEFORM_NAVPOLY )
        SetMouseGrabType( MouseGrabT::NONE );
    navpoly_deform_indicies.clear();
}

int Editor::GetVertIndexForMerge( void ) const {
    if ( mouseMode != MouseModeT::NAVPOLY_GRABBED )
        return -1;

    if ( mouseGrabType != MouseGrabT::DEFORM_NAVPOLY )
        return -1;

    if ( navpoly_deform_indicies.size() != 1 )
        return -1;

    auto poly = selected_nav_poly.lock();
    if ( ! poly )
        return -1;

    if ( !Input::IsKeyDown( globalVals.GetString("key_mergeNavMeshVert" ) ) )
        return -1;

    const Vec3f pos( Input::GetWorldMousePos( Vec3f(0,0,0), Vec3f(0,0,-1) ) );
    const int hovered_index = NavMesh::GetVertexFromPoolIndexAt( pos.ToVec2(), {navpoly_deform_indicies[0]} );
    if ( hovered_index < 0 )
        return -1;

    if ( poly->ChildOrParentContainsVertIndex( static_cast< unsigned int >( hovered_index ) ) )
        return -1;

    return hovered_index;
}

void Editor::MouseMove_NavMeshMode( void ) {
    if ( mouseMode != MouseModeT::NAVPOLY_GRABBED )
        return;

    if ( mouseGrabType != MouseGrabT::DEFORM_NAVPOLY )
        return;

    DeformNavPoly();

}

bool Editor::SelectNavPolyAt( const Vec2f& at ) {
    std::shared_ptr< NavPoly > poly;
    for ( auto & mesh : NavMesh::meshes ) {
        poly = mesh->GetPolyAt( at );
        if ( poly ) {
            selected_nav_poly = poly;
            SetMouseMode( MouseModeT::NAVPOLY_GRABBED );
            return true;
        }
    }

    return false;
}

bool Editor::SelectNavPolyUnderMouse( void ) {
    return SelectNavPolyAt( Input::GetWorldMousePos( Vec3f(0,0,selection_origin.z) ).ToVec2() );
}

bool Editor::CheckInputEvent_NavMeshMode( const std::string& action ) {    
    if ( action == "NavPolySelect" ) {
        return SelectNavPolyUnderMouse();
    }

    if ( action == "NavPolyExtrude" ) {
        return ExtrudeSelectedNavPoly( Input::GetWorldMousePos( Vec3f(0,0,selection_origin.z) ).ToVec2() );
    }

    if ( action == "NavPolyDeform" ) {
        DeformNavPoly();
        return true;
    }

    if ( action == "NavPolyAddVert" ) {
        AddVertToNavPoly();
        return true;
    }

    if ( action == "DelSelection" ) {
        editor->Confirm_DelNavMesh();
        return true;
    }

    return false;
}

bool Editor::ExtrudeSelectedNavPoly_Preview( const Vec2f& at ) {
    if ( Input::NumButtonsDown() != 1 )
        return false;

    if ( !Input::IsKeyDown( globalVals.GetString("key_extrudeNavPoly" ) ) )
        return false;

    auto poly = selected_nav_poly.lock();
    if ( !poly )
        return false;

    poly->Extrude_Preview( at );
    return true;
}

bool Editor::ExtrudeSelectedNavPoly( const Vec2f& at ) {
    auto poly = selected_nav_poly.lock();
    if ( !poly )
        return false;

    poly->Extrude( at );
    return true;
}

void Editor::Draw_NavMeshVertMergePreview( void ) {
    if ( auto poly = selected_nav_poly.lock() ) {
        if ( GetVertIndexForMerge() >= 0 ) {
            renderer->DrawPoint( poly->GetVertFromPool( navpoly_deform_indicies[0] ).ToVec3(), NAV_POLY_VERTEX_MERGE_COLOR, NAV_POLY_VERTEX_MERGE_SIZE );
        }
    }
}

void Editor::NavPolyAddVert_Preview( void ) {
    if ( Input::NumButtonsDown() != 1 )
        return;

    if ( !Input::IsKeyDown( globalVals.GetString("key_navPolyAddVert" ) ) )
        return;

    auto poly = selected_nav_poly.lock();
    if ( ! poly )
        return;

    const Vec3f pos( Input::GetWorldMousePos( Vec3f(0,0,0), Vec3f(0,0,-1) ) );
    poly->InsertVertOnEdge_Preview( pos.ToVec2() );
}

void Editor::DeformNavPoly_Preview( void ) {
    if ( Input::NumButtonsDown() != 1 )
        return;

    if ( !Input::IsKeyDown( globalVals.GetString("key_navPolyDeform" ) ) )
        return;

    auto poly = selected_nav_poly.lock();
    if ( ! poly )
        return;

    const Vec3f pos( Input::GetWorldMousePos( Vec3f(0,0,0), Vec3f(0,0,-1) ) );
    poly->NudgeVert_Preview( pos.ToVec2() );
    SetMouseMode( MouseModeT::NAVPOLY_GRABBED );
}

void Editor::DeformNavPoly( void ) {
    if ( mouseMode != MouseModeT::NAVPOLY_GRABBED ) {
        DoneDeformNavPoly();
        return;
    }

    if ( mouseGrabType != MouseGrabT::DEFORM_NAVPOLY ) {
        BeginNavPolyDeform();
        return;
    }

    ContinueNavPolyDeform();
}

void Editor::AddVertToNavPoly( void ) {
    auto poly = selected_nav_poly.lock();
    if ( ! poly )
        return;

    click_pos = Input::GetWorldMousePos( Vec3f(0,0,0), Vec3f(0,0,-1) );
    poly->InsertVertOnEdge( click_pos.ToVec2() );
}

void Editor::BeginNavPolyDeform( void ) {
    auto poly = selected_nav_poly.lock();
    if ( ! poly ) {
        DoneDeformNavPoly();
        return;
    }

    navpoly_deform_indicies.clear();
    navpoly_deform_indicies.reserve( 3 );

    click_pos = Input::GetWorldMousePos( Vec3f(0,0,0), Vec3f(0,0,-1) );
    navpoly_deform_indicies = poly->GetIndicesForDeform( click_pos.ToVec2() );
    SetMouseGrabType( MouseGrabT::DEFORM_NAVPOLY );
}

void Editor::ContinueNavPolyDeform( void ) {
    auto poly = selected_nav_poly.lock();
    if ( !poly || navpoly_deform_indicies.size() < 1 ) {
        DoneDeformNavPoly();
        return;
    }

    const Vec3f old_click_pos( click_pos );
    click_pos = Input::GetWorldMousePos( Vec3f(0,0,0), Vec3f(0,0,-1) );
    Vec3f nudge( click_pos - old_click_pos );

    for ( auto index : navpoly_deform_indicies ) {
        poly->NudgeVert(index, nudge.ToVec2());
    }
}

void Editor::Draw_NavMeshMode( void ) {
    if ( editmode != EditModeT::NAVMESH )
        return;

    if ( !globalVals.GetBool( gval_d_navMesh ) )
        return;

    Draw_SelectedNavPoly();
    NavPolyAddVert_Preview();
    DeformNavPoly_Preview();
    ExtrudeSelectedNavPoly_Preview( Input::GetWorldMousePos( Vec3f(0,0,selection_origin.z) ).ToVec2() );
    Draw_NavMeshVertMergePreview();
}

void Editor::Draw_SelectedNavPoly( void ) const {
    auto poly = selected_nav_poly.lock();
    if ( !poly )
        return;

    renderer->DrawPoly( poly->GetVerts3D(), NAV_POLY_COLOR_SELECTED );
}

#endif // MONTICELLO_EDITOR
