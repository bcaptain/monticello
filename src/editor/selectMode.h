// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_EDITOR_SELCTMODE_H_
#define SRC_EDITOR_SELCTMODE_H_

#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./clib/src/macros.h"
#include "./clib/src/types.h"
#include "./clib/src/std.h"
#include "./clib/src/strings.h"
#include "./editor.h"

extern const float SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT;
extern const float SELECTION_DIST_FROM_CLICKPOS_TO_FLAT_VERT_SQ;

void Event_ModelSelected( const std::vector< std::string >& args );
void Event_MaterialSelected( const std::vector< std::string >& args );

typedef Uint8 RotationAxisT_BaseType;
enum class RotationAxisT : RotationAxisT_BaseType {
    X=0,
    Y=1,
    Z=2
};

typedef Uint8 EntitySelectOptionT_BaseType;
enum class EntitySelectOptionT : EntitySelectOptionT_BaseType {
    SELECT_SINGLE = 0, //!< one at a time
    SELECT_MULTIPLE = 1, //!< add or remove from selection list, deselects if already selected
    SELECT_GROUP = 2 //!< must be in the selection or it will clear before set
};

#endif // MONTICELLO_EDITOR

#endif // SRC_EDITOR_SELCTMODE_H_
