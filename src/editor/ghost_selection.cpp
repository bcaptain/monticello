// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"
#include "../input/input.h"
#include "../rendering/renderer.h"
#include "../rendering/models/modelManager.h"
#include "../rendering/materialManager.h"

extern float OBJECT_SNAP_GRID_DISTANCE;
extern float ENTITY_SNAP_ANGLE;

void Editor::RotateGhostSelectionX( const float normalized_direction, const bool snap_to_grid ) {
    if ( snap_to_grid ) {
        addSelectionAngles.x += ENTITY_SNAP_ANGLE * normalized_direction;
        addSelectionAngles.x = ( Round( addSelectionAngles.x / ENTITY_SNAP_ANGLE ) * ENTITY_SNAP_ANGLE );
    } else {
        addSelectionAngles.x += 90.0f * normalized_direction;
    }

    // normalize angle
    addSelectionAngles.x = Maths::NormalizeAngle( addSelectionAngles.x );
}

void Editor::RotateGhostSelectionY( const float normalized_direction, const bool snap_to_grid ) {
    if ( snap_to_grid ) {
        addSelectionAngles.y += ENTITY_SNAP_ANGLE * normalized_direction;
        addSelectionAngles.y = ( Round( addSelectionAngles.y / ENTITY_SNAP_ANGLE ) * ENTITY_SNAP_ANGLE );
    } else {
        addSelectionAngles.y += 90.0f * normalized_direction;
    }

    // normalize angle
    addSelectionAngles.y = Maths::NormalizeAngle( addSelectionAngles.y );
}

void Editor::RotateGhostSelectionZ( const float normalized_direction, const bool snap_to_grid ) {
    if ( snap_to_grid ) {
        addSelectionAngles.z += ENTITY_SNAP_ANGLE * normalized_direction;
        addSelectionAngles.z = ( Round( addSelectionAngles.z / ENTITY_SNAP_ANGLE ) * ENTITY_SNAP_ANGLE );
    } else {
        addSelectionAngles.z += 90.0f * normalized_direction;
    }

    // normalize angle
    addSelectionAngles.z = Maths::NormalizeAngle( addSelectionAngles.z );
}

void Editor::Think_GhostSelection( void ) {
    if ( editmode != EditModeT::ADD || !selectedEntInfo )
        return;

    addSelectionOrigin = Input::GetWorldMousePos();

    if ( GetSnap() ) {
        addSelectionOrigin.x = Round(addSelectionOrigin.x) * GetSnapGridDistance();
        addSelectionOrigin.y = Round(addSelectionOrigin.y) * GetSnapGridDistance();
        addSelectionOrigin.z = Round(addSelectionOrigin.z) * GetSnapGridDistance();
    }
}

bool Editor::UpdateGhostSelectionModel( void ) {
    if ( !selectedEntInfo ) {
        ghostSelectionModelInfo.UnsetModel();
        return false;
    }

    // ** find the model
    const std::string model_name = selectedEntInfo->GetMemberValue("model");
    if ( model_name.size() < 1 ) {
        ghostSelectionModelInfo.UnsetModel();
        return false;
    }

    std::shared_ptr< const Model > ghostselection_model = modelManager.Get( model_name.c_str() );
    if ( ! ghostselection_model ) {
        ghostSelectionModelInfo.UnsetModel();
        return false;
    }

    // ** find the material
    const std::string mtl_name = selectedEntInfo->GetMemberValue("mtl");
    std::string material_name;
    if ( model_name.size() > 0 ) {
        material_name = model_name; // use default
    } else {
        material_name = mtl_name; // use specified
    }

    ghostSelectionModelInfo.SetMaterial( materialManager.Get( material_name.c_str() ) );
    ghostSelectionModelInfo.SetModel( ghostselection_model );
    return true;
}

void Editor::Draw_GhostSelection( void ) const {
    if ( editmode != EditModeT::ADD )
        return;

    if ( !ghostSelectionModelInfo.HasDrawModel() )
        return;

    ModelDrawInfo di;

    di.opacity = 0.5f;

    renderer->PushMatrixMV();
        if ( !Maths::Approxf(addSelectionAngles, Vec3f(0,0,0) ) ) {
            renderer->ModelView().RotateZYX( addSelectionAngles );
        }
        renderer->ModelView().Translate( addSelectionOrigin );
        ghostSelectionModelInfo.Draw( di );
    renderer->PopMatrixMV();
}

#endif // MONTICELLO_EDITOR
