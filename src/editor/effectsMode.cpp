// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"
#include "./ui/editorui.h"
#include "../rendering/materialManager.h"
#include "../particles/effectsManager.h"

const char* CSTR_PARTICLE_SET_RAND_START_ANGLE = "Set Random Start Angle";
const char* CSTR_PARTICLE_UNSET_RAND_START_ANGLE = "Unset Random Start Angle";

const char* CSTR_PARTICLE_SET_DESTINATION = "Set Dest";
const char* CSTR_PARTICLE_UNSET_DESTINATION = "Unset Dest";

const char* CSTR_PARTICLE_SET_DESTINATION2 = "Set Dest2";
const char* CSTR_PARTICLE_UNSET_DESTINATION2 = "Unset Dest2";

void Event_SetRenderEffectData( const uint to ) {
    editor->SetRenderEffectData( to );
}

void Editor::SetRenderEffectData( const uint to ) {
    if ( selectedEffect != nullptr )
        selectedEffect->SetDuration( to );
}

void Event_SetParticleQuantity( const uint to ) {
    editor->SetParticleQuantity( to );
}

void Editor::SetParticleQuantity( const uint to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetQuantity( to );
}

void Event_SetParticleDelay( const uint to ) {
    editor->SetParticleDelay( to );
}

void Editor::SetParticleDelay( const uint to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetDelay( to );
}

void Event_SetParticleCease( const uint to ) {
    editor->SetParticleCease( to );
}

void Editor::SetParticleCease( const uint to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetCease( to );
}

void Event_SetParticleLifeSpan( const uint to ) {
    editor->SetParticleLifeSpan( to );
}

void Editor::SetParticleLifeSpan( const uint to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetLifeSpan( to );
}

void Event_SetParticleBunching( const float to ) {
    editor->SetParticleBunching( to );
}

void Editor::SetParticleBunching( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetBunching( to );
}

void Event_SetParticleRotationTo( const float to ) {
    editor->SetParticleRotationTo( to );
}

void Editor::SetParticleRotationTo( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetRotationTo( to );
}

void Event_SetParticleRotationFrom( const float to ) {
    editor->SetParticleRotationFrom( to );
}

void Editor::SetParticleRotationFrom( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetRotationFrom( to );
}

void Event_SetParticleColorizeWeight( const float to ) {
    editor->SetParticleColorizeWeight( to );
}

void Editor::SetParticleColorizeWeight( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetColorizeWeight( to );
}

void Event_SetParticleColorizeToWeight( const float to ) {
    editor->SetParticleColorToWeight( to );
}

void Editor::SetParticleColorToWeight( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetColorizeToWeight( to );
}

void Event_SetParticleOpacityFrom( const float to ) {
    editor->SetParticleOpacityFrom( to );
}

void Editor::SetParticleOpacityFrom( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetOpacityFrom( to );
}

void Event_SetParticleOpacityTo( const float to ) {
    editor->SetParticleOpacityTo( to );
}

void Editor::SetParticleOpacityTo( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetOpacityTo( to );
}

void Event_SetParticleOpacityMid( const float to ) {
    editor->SetParticleOpacityMid( to );
}

void Editor::SetParticleOpacityMid( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetOpacityMid( to );
}

void Event_SetParticleScaleFrom( const float to ) {
    editor->SetParticleScaleFrom( to );
}

void Editor::SetParticleScaleFrom( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetScaleFrom( to );
}

void Event_SetParticleScaleTo( const float to ) {
    editor->SetParticleScaleTo( to );
}

void Editor::SetParticleScaleTo( const float to ) const {
    if ( selectedParticleLink )
        selectedParticleLink->Data().SetScaleTo( to );
}

void Editor::EffectsMode( void ) {
    btnEffectsMode->SetFaceBottomColor( COLOR_RADIOBUTTON_GRADIENT_B );
    wndToolBox->ShowNamedElement( WINDOW_EFFECTS_NAME, true );
}

void Editor::ApplyParticleOffset( void ) const {
    if ( ! selectedParticleLink )
        return;

    Vec3f to(
        String::ToFloat( txtOffsetX->GetLabel() )
        , String::ToFloat( txtOffsetY->GetLabel() )
        , String::ToFloat( txtOffsetZ->GetLabel() )
    );

    selectedParticleLink->Data().SetOrigin( to );

    to.x = String::ToFloat( txtOriginDistribXY->GetLabel() );

    if ( Maths::Approxf( to.x, 0.0f ) ) {
        selectedParticleLink->Data().UnsetOriginDistribXYRadius();
    } else {
        selectedParticleLink->Data().SetOriginDistribXYRadius( to.x );
    }

    to.x = String::ToFloat( txtOriginDistribZ->GetLabel() );

    if ( Maths::Approxf( to.x, 0.0f ) ) {
        selectedParticleLink->Data().UnsetOriginDistribZRadius();
    } else {
        selectedParticleLink->Data().SetOriginDistribZRadius( to.x );
    }

}

void Editor::ToggleParticleDest( void ) const {
    if ( ! selectedParticleLink )
        return;

    if ( selectedParticleLink->Data().IsDestSet() ) {
        selectedParticleLink->Data().UnsetDest();
        btnToggleParticleDest->SetLabel( CSTR_PARTICLE_SET_DESTINATION );
    } else {
        UpdateParticleDest();
        btnToggleParticleDest->SetLabel( CSTR_PARTICLE_UNSET_DESTINATION );
    }
}

void Editor::ToggleParticleDest2( void ) const {
    if ( ! selectedParticleLink )
        return;

    if ( selectedParticleLink->Data().IsDest2Set() ) {
        selectedParticleLink->Data().UnsetDest2();
        btnToggleParticleDest2->SetLabel( CSTR_PARTICLE_SET_DESTINATION2 );
    } else {
        UpdateParticleDest2();
        btnToggleParticleDest2->SetLabel( CSTR_PARTICLE_UNSET_DESTINATION2 );
    }
}

void Editor::UpdateParticleDest( void ) const {
    if ( ! selectedParticleLink )
        return;

    btnToggleParticleDest->SetLabel( CSTR_PARTICLE_UNSET_DESTINATION );

    Vec3f to(
        String::ToFloat( txtDestX->GetLabel() )
        , String::ToFloat( txtDestY->GetLabel() )
        , String::ToFloat( txtDestZ->GetLabel() )
    );

    selectedParticleLink->Data().SetDest( to );

    to.x = String::ToFloat( txtDestDistribXY->GetLabel() );

    if ( Maths::Approxf( to.x, 0.0f ) ) {
        selectedParticleLink->Data().UnsetDestDistribXYRadius();
    } else {
        selectedParticleLink->Data().SetDestDistribXYRadius( to.x );
    }

    to.x = String::ToFloat( txtDestDistribZ->GetLabel() );

    if ( Maths::Approxf( to.x, 0.0f ) ) {
        selectedParticleLink->Data().UnsetDestDistribZRadius();
    } else {
        selectedParticleLink->Data().SetDestDistribZRadius( to.x );
    }

    const Uint32 curTime = game->GetRenderTime();
    selectedParticleLink->Data().Setup( EffectStateT::Initialize, curTime );
}

void Editor::SetParticleFaceCamera( const bool whether ) const {
    if ( ! selectedParticleLink )
        return;

    selectedParticleLink->Data().SetFaceCamera( whether );
}

void Editor::UpdateParticleDest2( void ) const {
    if ( ! selectedParticleLink )
        return;

    btnToggleParticleDest2->SetLabel( CSTR_PARTICLE_UNSET_DESTINATION2 );

    Vec3f to(
        String::ToFloat( txtDest2X->GetLabel() )
        , String::ToFloat( txtDest2Y->GetLabel() )
        , String::ToFloat( txtDest2Z->GetLabel() )
    );

    selectedParticleLink->Data().SetDest2( to );

    to.x = String::ToFloat( txtDest2DistribXY->GetLabel() );

    if ( Maths::Approxf( to.x, 0.0f ) ) {
        selectedParticleLink->Data().UnsetDest2DistribXYRadius();
    } else {
        selectedParticleLink->Data().SetDest2DistribXYRadius( to.x );
    }

    to.x = String::ToFloat( txtDest2DistribZ->GetLabel() );

    if ( Maths::Approxf( to.x, 0.0f ) ) {
        selectedParticleLink->Data().UnsetDest2DistribZRadius();
    } else {
        selectedParticleLink->Data().SetDest2DistribZRadius( to.x );
    }

    const Uint32 curTime = game->GetRenderTime();
    selectedParticleLink->Data().Setup( EffectStateT::Initialize, curTime );
}

void SetVec3TextBoxes( const Vec3f& vec, TextBox& xbox, TextBox& ybox, TextBox& zbox );
void SetVec3TextBoxes( const Vec3f& vec, TextBox& xbox, TextBox& ybox, TextBox& zbox ) {
    std::string tmp_str( std::to_string( vec.x ) );
    String::TrimTrailing( tmp_str, "0" );
    xbox.SetLabel( tmp_str );

    tmp_str = std::to_string( vec.y );
    String::TrimTrailing( tmp_str, "0" );
    ybox.SetLabel( tmp_str );

    tmp_str = std::to_string( vec.z );
    String::TrimTrailing( tmp_str, "0" );
    zbox.SetLabel( tmp_str );
}

void SetFloatTextBox( const float val, TextBox& box );
void SetFloatTextBox( const float val, TextBox& box ) {
    std::string tmp_str( std::to_string( val ) );
    String::TrimTrailing( tmp_str, "0" );
    box.SetLabel( tmp_str );
}

void Editor::SetParticleUIValues( const Particle& prt ) const {
    dboxParticleTex->Deselect();
    dboxParticleTex->SelectItemByName( prt.GetMaterialName().c_str() );
    sldParticleDelay->SetVal( prt.GetDelay() );
    sldParticleCease->SetVal( prt.GetCease() );
    sldParticleQty->SetVal( prt.GetQuantity() );
    sldParticleLifeSpan->SetVal( prt.GetLifeSpan() );
    sldParticleBunching->SetVal( prt.GetBunching() );
    sldParticleRotateFrom->SetVal( prt.GetRotationFrom() );
    sldParticleRotateTo->SetVal( prt.GetRotationTo() );
    sldParticleOpacityFrom->SetVal( prt.GetOpacityFrom() );
    sldParticleOpacityMid->SetVal( prt.GetOpacityMid() );
    sldParticleOpacityTo->SetVal( prt.GetOpacityTo() );
    sldParticleScaleTo->SetVal( prt.GetScaleTo() );
    sldParticleScaleFrom->SetVal( prt.GetScaleFrom() );
    sldParticleColorWeight->SetVal( prt.GetColorizeWeight() );
    sldParticleColorToWeight->SetVal( prt.GetColorizeToWeight() );

    // ** offset
    SetVec3TextBoxes( prt.GetOrigin(), *txtOffsetX, *txtOffsetY, *txtOffsetZ );

    // ** offset distribution
    SetFloatTextBox( prt.GetOriginDistribXYRadius(), *txtOriginDistribXY );
    SetFloatTextBox( prt.GetOriginDistribZRadius(), *txtOriginDistribZ );

    // ** dest
    SetVec3TextBoxes( prt.GetDest(), *txtDestX, *txtDestY, *txtDestZ );

    // ** dest distribution
    SetFloatTextBox( prt.GetDestDistribXYRadius(), *txtDestDistribXY );
    SetFloatTextBox( prt.GetDestDistribZRadius(), *txtDestDistribZ );

    // ** dest 2
    SetVec3TextBoxes( prt.GetDest2(), *txtDest2X, *txtDest2Y, *txtDest2Z );

    // ** dest2 distribution
    SetFloatTextBox( prt.GetDest2DistribXYRadius(), *txtDest2DistribXY );
    SetFloatTextBox( prt.GetDest2DistribZRadius(), *txtDest2DistribZ );

    // ** random start angle
    btnToggleParticleRandInitialAngle->SetLabel( prt.GetRandomInitialAngle() ? CSTR_PARTICLE_UNSET_RAND_START_ANGLE : CSTR_PARTICLE_SET_RAND_START_ANGLE );
    btnToggleParticleDest->SetLabel( prt.IsDestSet() ? CSTR_PARTICLE_UNSET_DESTINATION : CSTR_PARTICLE_SET_DESTINATION );
    btnToggleParticleDest2->SetLabel( prt.IsDest2Set() ? CSTR_PARTICLE_UNSET_DESTINATION2 : CSTR_PARTICLE_SET_DESTINATION2 );

    // ** colorize
    SetVec3TextBoxes( prt.GetColorize(), *txtParticleColorX, *txtParticleColorY, *txtParticleColorZ );
    SetFloatTextBox( prt.GetColorizeWeight(), *sldParticleColorWeight );

    // ** colorize to
    SetVec3TextBoxes( prt.GetColorizeTo(), *txtParticleColorToX, *txtParticleColorToY, *txtParticleColorToZ );
    SetFloatTextBox( prt.GetColorizeToWeight(), *sldParticleColorToWeight );
}

void Editor::ShowEffectUIValues( const bool whether ) const {
    for ( Sprite* element : effectValElements )
        element->SetVisible( whether );

    if ( ! whether ) {
        ShowParticleUIValues( false );
    }
}

void Editor::ShowParticleUIValues( const bool whether ) const {
    for ( Sprite* element : particleValElements )
        element->SetVisible( whether );
}

void Editor::HideParticle( Particle& prt ) {
    prt.Hide();
}

void Editor::ShowParticle( Particle& prt ) {
    prt.Show();

    // update labels
    if ( selectedEffect )
        RepopulateParticlesDropBox();
}

void Editor::HideSelectedParticle( void ) {
    if ( ! selectedParticleLink )
        return;

    HideParticle( selectedParticleLink->Data() );
    RepopulateParticlesDropBox(); // update labels
}

void Editor::ShowSelectedParticle( void ) {
    if ( ! selectedParticleLink )
        return;

    ShowParticle( selectedParticleLink->Data() );
    RepopulateParticlesDropBox(); // update labels
}

void Editor::SoloSelectedParticle( void ) {
    if ( ! selectedParticleLink )
        return;

    if ( !selectedEffect )
        return;

    Link< Particle > *prt = selectedEffect->GetParticlesListRef().GetFirst();

    while ( prt != nullptr ) {
        if ( &prt->Data() == &selectedParticleLink->Data() ) {
            prt->Data().Show();
        } else {
            prt->Data().Hide();
        }
        prt = prt->GetNext();
    }


    RepopulateParticlesDropBox(); // update labels
}

void Editor::SetColorize( void ) const {
    if ( ! selectedParticleLink )
        return;

    selectedParticleLink->Data().SetColorize(
        String::ToFloat( txtParticleColorX->GetLabel() )
        , String::ToFloat( txtParticleColorY->GetLabel() )
        , String::ToFloat( txtParticleColorZ->GetLabel() )
    );
}

void Editor::SetColorizeTo( void ) const {
    if ( ! selectedParticleLink )
        return;

    selectedParticleLink->Data().SetColorizeTo(
        String::ToFloat( txtParticleColorToX->GetLabel() )
        , String::ToFloat( txtParticleColorToY->GetLabel() )
        , String::ToFloat( txtParticleColorToZ->GetLabel() )
    );
}

void Editor::ToggleParticleRandInitialAngle( void ) const {
    if ( ! selectedParticleLink )
        return;

    Particle& prt = selectedParticleLink->Data();

    if ( prt.GetRandomInitialAngle() ) {
        prt.UnsetRandomInitialAngle();
        btnToggleParticleRandInitialAngle->SetLabel(CSTR_PARTICLE_SET_RAND_START_ANGLE);
    } else {
        prt.SetRandomInitialAngle();
        btnToggleParticleRandInitialAngle->SetLabel(CSTR_PARTICLE_UNSET_RAND_START_ANGLE);
    }
}

std::string Editor::GetDropDownOptionNameForParticle( const uint index, const Particle& prt ) const {
    std::string name( std::to_string( index ) );
    name += " ";

    if ( prt.IsHidden() )
        name += "(H) ";

    if ( prt.GetMaterial() )
        name += prt.GetMaterial()->GetName();

    return name;
}

void Editor::RepopulateParticlesDropBox( void ) {
    dboxParticles->Clear();

    if ( !selectedEffect )
        return;

    uint index=0;
    Link< Particle > *link = selectedEffect->GetParticlesListRef().GetFirst();

    // labels in the format of "0 X materialName" where 0 is the first in the list, and X is an optional descriptive tag
    while ( link != nullptr ) {
        const std::string name = GetDropDownOptionNameForParticle( index, link->Data() );
        dboxParticles->Add( { name, std::to_string( index ), "" /*tooltip empty*/ } );
        ++index;
        link = link->GetNext();
    }
    
    dboxParticles->Refresh();
    // note: we don't sort the particles because their indices are important
}

void Editor::RepopulateParticleTexDropBox( void ) {
    dboxParticleTex->Clear();
    LinkList< std::string > names;
    materialManager.GetAllNames( names );

    int i=1;
    for ( const auto& name : names ) {
        // only add textures that were designed for particles
        if ( String::Left( name, 4 ) == "prt_" ) {
            dboxParticleTex->Add( { name, std::to_string(i) } );
            ++i;
        }
    }

    dboxParticleTex->Sort();
    dboxParticleTex->Refresh();
}

void Editor::DeleteSelectedParticle( void ) {
    // delete the particle first, since deleting the DropDownOption will change it to the next available one in the list
    if ( selectedParticleLink ) {
        selectedParticleLink->Del();
        selectedParticleLink = nullptr;
    }

    dboxParticles->RemoveSelected();
    dboxParticles->Refresh();
    dboxParticles->Deselect();
    ShowParticleUIValues( false );
}

void Editor::SetSelectedParticle( const int list_index ) {
    if ( ! selectedEffect )
        return;

    Link< Particle > *link = selectedEffect->GetParticlesListRef().GetFirst();

    int p = 0;
    while ( link != nullptr ) {
        if ( p == list_index ) {
            selectedParticleLink = link;
            SetParticleUIValues( link->Data() );
            ShowParticleUIValues( true );
            return;
        }
        ++p;
        link = link->GetNext();
    }
}

void Editor::NewParticle( void ) {
    if ( !selectedEffect )
        return;

    selectedEffect->AddParticle();
    RepopulateParticlesDropBox();
    dboxParticles->SelectLastItem();
}

void Editor::SetSelectedEffect( const std::string& name ) {
    dboxParticles->Close();
    dboxParticleTex->Close();
    auto newSelection = effectsManager.GetNonconst( name.c_str() );
    effectPreviewStartTime = game->GetRenderTime();

    if ( selectedEffect == newSelection ) {
        // just reload the values
        selectedParticleLink = nullptr;
        SetEffectUIValues( *selectedEffect );
        RepopulateParticlesDropBox();
        selectedEffect->Reset( effectPreviewStartTime );
        return;
    }

    selectedEffect = newSelection;

    dboxParticles->Deselect();
    dboxParticleTex->Deselect();

    if ( !selectedEffect ) {
        ERR("Selected effect was not found: %s\n", name.c_str() );
        ShowEffectUIValues( true );
        ShowParticleUIValues( false );
        return;
    } else {
        selectedEffect->Reset( effectPreviewStartTime );
        SetEffectUIValues( *selectedEffect );
    }

    selectedParticleLink = nullptr;
    RepopulateParticlesDropBox();

    ShowEffectUIValues( true );
    ShowParticleUIValues( false );
}

void Editor::SetEffectUIValues( const Effect& effect ) const {
    sldRenderEffectData->SetVal( effect.GetDuration() );
}

void Event_EffectSelected( const std::vector< std::string >& args ) {
    if ( args.size() > 0 )
        editor->SetSelectedEffect( args[0] );
}

void Event_ParticleSelected( const std::vector< std::string >& args ) {
    // we store our selection index at 2
    if ( args.size() > 2 )
        editor->SetSelectedParticle( String::ToInt( args[1] ) );
}

void Editor::SetParticleTex( const char* selection ) const {
    if ( ! selectedParticleLink )
        return;

    Particle& prt = selectedParticleLink->Data();

    std::shared_ptr< const Material > material = materialManager.Get( selection );

    if ( ! material ) {
        ERR("Particle material does not exist: %s\n", selection );
        return;
    }

    if ( !material->GetDiffuse() ) {
        ERR("Particle material has no diffuse: %s\n", selection );
        return;
    }

    prt.SetMaterial( material );

    // update selected particles dbox name
    const int index = dboxParticles->GetSelectedIndex();
    if ( index >= 0 && static_cast<uint>(index) < dboxParticles->Num() ) {
        const std::string new_name( GetDropDownOptionNameForParticle( static_cast<uint>(index), prt ) );
        dboxParticles->argument_data[static_cast<uint>(index)][0] = new_name;
        dboxParticles->Refresh();
    }
}

void Event_ParticleTexSelected( const std::vector< std::string >& args ) {
    if ( args.size() > 0 )
        editor->SetParticleTex( args[0].c_str() );
}

void Editor::RepopulateEffectsDropBox( void ) {
    dboxEffects->Clear();

    LinkList< std::string > names;
    effectsManager.GetAllNames( names );
    const Link< std::string >* link = names.GetFirst();

    int i=1;
    while ( link != nullptr ) {
        dboxEffects->Add( { link->Data(), std::to_string(i) } );
        link = link->GetNext();
        ++i;
    }

    dboxEffects->Sort();
    dboxEffects->Refresh();
}

void Editor::SaveEffect( void ) {
    if ( !selectedEffect ) {
        ERR("No effect selected in which to save.\n");
        return;
    }

    std::string name = dboxEffects->GetLabel();
    if ( !effectsManager.Save( name.c_str(), selectedEffect ) )
        return;

    effectsManager.Reload( name.c_str() );
    RepopulateEffectsDropBox();
}

void Editor::ReloadEffect( void ) {
    if ( !selectedEffect ) {
        ERR("No effect selected in which to reload.\n");
        return;
    }

    effectsManager.Reload( selectedEffect->GetName().c_str() );
    SetSelectedEffect( selectedEffect->GetName() );
}

void Editor::RestartEffect( void ) {
    if ( !selectedEffect ) {
        ERR("No effect selected in which to restart.\n");
        return;
    }

    const Uint32 curTime = game->GetRenderTime();    
    selectedEffect->Reset( curTime );
    effectPreviewStartTime = game->GetGameTime();
}

void Editor::Draw_EffectMode( void ) {
    if ( editmode != EditModeT::EFFECTS )
        return;

    if ( !selectedEffect )
        return;

    const Uint32 curTime = game->GetRenderTime();
    const Uint32 fxDuration = selectedEffect->GetDuration();

    bool alive = false;

    if ( fxDuration == 0 ) {
        alive = true;
    } else if ( curTime-effectPreviewStartTime < fxDuration ) {
        alive = true;
    }
    selectedEffect->DrawPreview( alive ? EffectStateT::Alive : EffectStateT::Dying, curTime );
}

Link< Particle >* Editor::GetSelectedParticleLink( void ) const {
    return selectedParticleLink;
}

std::shared_ptr< Effect > Editor::GetSelectedEffect( void ) const {
    return selectedEffect;
}

#endif // MONTICELLO_EDITOR
