// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"
#include "../input/input.h"
#include "../rendering/renderer.h"
#include "../rendering/models/modelManager.h"
#include "../rendering/materialManager.h"
#include "../rendering/animation/animInfo.h"
#include "../rendering/animation/animation.h"

bool Editor::SetTestAnim( [[maybe_unused]] const char *animName ) {
#ifdef MONTICELLO_DEBUG
    if (! testAnimModelInfo.IsAnimatedModel() )
#endif // MONTICELLO_DEBUG
        return false;
        
#ifdef MONTICELLO_DEBUG
    testAnimModelInfo.TestModel_Animate( animName );
    return true;
#endif // MONTICELLO_DEBUG
}

bool Editor::UpdateTestAnimMaterial( void ) {
    auto mdl = testAnimModelInfo.GetModel();
    if ( ! mdl )
        return false;

    auto mtl = materialManager.Get( mdl->GetName().c_str() );
    if ( ! mtl )
        return false;

    testAnimModelInfo.SetMaterial( mtl );
    return true;
}

void Editor::Think_TestAnimModel( void ) {

    ArmatureInfo *armInfo = testAnimModelInfo.GetArmatureInfo(AnimChannelT::Legs);

    if (testAnimModelInfo.GetModel() == nullptr)
        return;

    if ( !armInfo || !armInfo->HasValidAnim() )
        return;
        
    Draw_TestAnimModel();
}

void Editor::Draw_TestAnimModel( void ) const {
#ifdef MONTICELLO_DEBUG
    testAnimModelInfo.TestModel_Animate();
    
    if (! testAnimModelInfo.GetModel() )
        return;

    ModelDrawInfo di;
    di.noAnimPause = true;
    testAnimModelInfo.Draw( di );
#endif // MONTICELLO_DEBUG
}

bool Editor::SetTestAnimModel( const char* modelName ) {
    const bool ret = testAnimModelInfo.SetModel(modelManager.Get(modelName));
    if ( ret ) {
        testAnimModelInfo.SetMaterialToDefault();
    }
    
    return ret;
}


#endif // MONTICELLO_EDITOR
