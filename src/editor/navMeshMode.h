// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_EDITOR_NAVMESHMODE_H_
#define SRC_EDITOR_NAVMESHMODE_H_

#ifdef MONTICELLO_EDITOR

#include "./clib/src/warnings.h"
#include "./editor.h"
#include "../rendering/vbo.h"

extern const Color4f NAV_POLY_VERTEX_MERGE_COLOR;
extern const float NAV_POLY_VERTEX_MERGE_SIZE;

void Dialog_DelNavMesh( void );

#endif // MONTICELLO_EDITOR

#endif  // SRC_EDITOR_NAVMESHMODE_H_
