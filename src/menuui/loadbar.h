// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MENUUI_LOADBAR_H_
#define SRC_MENUUI_LOADBAR_H_

#include "../base/main.h"
#include "./menuui.h"

class LoadBar : public Label {
public:
    LoadBar( void ) = delete;
    LoadBar( const char* text, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    ~LoadBar( void );
    LoadBar( const LoadBar& other ) = delete;

public:
    LoadBar& operator=( const LoadBar& other ) = delete;
};

#endif  // SRC_MENUUI_LOADBAR_H_
