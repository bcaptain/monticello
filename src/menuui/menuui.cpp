// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./menuui.h"
#include "../game.h"
#include "../input/input.h"
#include "../ui/dialog/common.h"

void NewGame_Click( void ) {
    ASSERT( game );
    game->LoadMap("kitchen");

    auto page = game->GetUIPage( "mainMenu" );
    ASSERT( page );
    page->Hide();
}

void QuitProgram_Click( void ) {
    ASSERT( game );
    game->SetState( GameStateT::SHUTDOWN );
}

void UI_MainMenu::Create( void ) {
    ASSERT( ! mainMenu );
    created = true;

    mainMenu = new Window( WindowOptionsT::NO_COLLAPSE_BUTTON, 600,350 );
    mainMenu->SetOrigin(100,100,0);
    mainMenu->GetFace()->SetOpacity( 0.85f );
    mainMenu->SetMenu( true );
    mainMenu->SetPage( this );
    game->sprites[UI_LAYER_PROGRAM_WINDOW].Add( mainMenu );

    mainMenu->SetOverlayImage( "wndMainMenu" );

    Button_Func* btnMenuNewGame=new Button_Func( "New Game", &NewGame_Click, ButtonTypeT::None, 220, 32);
    btnMenuNewGame->SetOrigin( 180,180,0);
    btnMenuNewGame->GetFace()->SetOpacity(0.7f);
    btnMenuNewGame->SetFontSize(14);
    btnMenuNewGame->SetTabstopIndex( 1 );
    btnMenuNewGame->SetOverlayImage( "btnNewGame" );
    mainMenu->children->Add( btnMenuNewGame );

    Button_Func* btnMenuEditor=new Button_Func( "Back", &CloseTopWindow_Click, ButtonTypeT::None, 220, 32);
    btnMenuEditor->SetOrigin( 180,220,0);
    btnMenuEditor->GetFace()->SetOpacity(0.7f);
    btnMenuEditor->SetFontSize(14);
    btnMenuEditor->SetTabstopIndex( 2 );
    mainMenu->children->Add( btnMenuEditor );

    Button_FuncWithArg* btnMenuBinds=new Button_FuncWithArg( "Controls", &OpenUIPage_Click, {"binds"}, ButtonTypeT::None, 220, 32);
    btnMenuBinds->SetOrigin( 180,260,0);
    btnMenuBinds->GetFace()->SetOpacity(0.7f);
    btnMenuBinds->SetFontSize(14);
    btnMenuBinds->SetTabstopIndex( 3 );
    btnMenuBinds->SetOverlayImage( "btnControls" );
    mainMenu->children->Add( btnMenuBinds );

    Button_Func* btnMenuQuit=new Button_Func( "Quit", &QuitProgram_Click, ButtonTypeT::None, 220, 32);
    btnMenuQuit->SetOrigin( 180,300,0);
    btnMenuQuit->GetFace()->SetOpacity(0.7f);
    btnMenuQuit->SetFontSize(14);
    btnMenuQuit->SetTabstopIndex( 4 );
    btnMenuQuit->SetOverlayImage( "btnQuit" );
    mainMenu->children->Add( btnMenuQuit );
}

UI_MainMenu::UI_MainMenu( void )
    : mainMenu(nullptr)
    , created( false )
{
    name = "mainMenu";
}

void UI_MainMenu::Show( void ) {

    if ( !mainMenu ) {
        Create();
    }

    mainMenu->SetVisible( true );
    isOpen = true;
    globalVals.SetBool("s_menu", true);
    
    game->Pause( PauseStateT::PausedByMenu );

    GameUI_Page::Show();

    Input::SetFocusWidget( mainMenu->GetFirstTabstop() );

    SDL_ShowCursor(true);
}

void UI_MainMenu::Hide( void ) {
    if ( mainMenu )
        mainMenu->SetVisible( false );
    
    isOpen = false;
    globalVals.SetBool("s_menu", false);
    
    game->Unpause( PauseStateT::PausedByMenu );

    GameUI_Page::Hide();

    game->UpdateCursor();
}
