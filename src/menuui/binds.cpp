// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./menuui.h"
#include "../game.h"
#include "../ui/dialog/dialog.h"
#include "../input/input.h"
#include "../ui/dialog/common.h"
#include "../commandsys.h"
#include "./binds.h"


void BtnBind_Click( const std::vector< std::string >& bindName ) {
    ASSERT( game );

    if ( ! VerifyArgNum("BtnBind_Click", bindName, 1 ) )
        return;

    auto page = game->GetUIPage( "binds" );
    ASSERT( page );

    std::shared_ptr< UI_Binds > binds_page = std::static_pointer_cast< UI_Binds >( page );
    binds_page->BtnBind_Click( bindName[0] );
}


UI_Binds::UI_Binds( void )
    : ui_binds_description()
    , ui_binds_names()
    , ui_binds_types()
    , lastClickedBindType()
    , lastClickedBindName()
    , bindMenu(nullptr)
    , dlg(nullptr)
    , created( false )
{
    name = "binds";
}

void UI_Binds::Show( void ) {

    if ( !bindMenu ) {
        Create();
    }

    UpdateBindLabels();

    bindMenu->SetVisible( true );
    isOpen = true;
    Input::SetFocusWidget( bindMenu->GetFirstTabstop() );

    GameUI_Page::Show();
}

void UI_Binds::CancelBind( void ) {
    dlg = nullptr;
    Input::StopWaitingForBindKey();

    // dialog will be closed automatically by clicking the button
}

void UI_Binds::Hide( void ) {
    if ( bindMenu )
        bindMenu->SetVisible( false );
    isOpen = false;

    GameUI_Page::Hide();
}

void UI_Binds::DoBind( const std::string& bindName ) {
    // ** prune out the bind type
    lastClickedBindType.clear();
    std::string type;
    uint i;
    for ( i=0; i<bindName.size(); ++i ) {
        if ( bindName[i] == '_' ) {
            break;
        }
        lastClickedBindType += bindName[i];
    }

    lastClickedBindName = bindName;
    String::TrimLeadingNum( lastClickedBindName, i+1 ); // plus one for the underscore

    dlg = Dialog::Create( "Press a button to bind, delete to clear, or escape to cancel.", DEFAULT_DIALOG_WIDTH, DEFAULT_DIALOG_HEIGHT, UI_LAYER_PROGRAM_WINDOW_DIALOG );

    Input::GetBindKey();
}

void UI_Binds::BtnBind_Click( const std::string& bindName ) {
    auto cancel = std::bind(
        static_cast< void(UI_Binds::*)( void ) >( &UI_Binds::CancelBind )
        , this
    );

    auto bindSpecialButton = std::bind(
        static_cast< void(UI_Binds::*)( const std::string& ) >( &UI_Binds::DoBind )
        , this, bindName
    );

    // if a dialog is already open
    if ( dlg )
        return;

    // if we're trying to rebind the menu-open button
    if ( String::Left( bindName, 2 ) == "ui" ) {
        dlg = Dialog::Create( "Rebinding UI is not recommended! But you can delete config.cfg if things break. Are you sure?.", DEFAULT_DIALOG_WIDTH, DEFAULT_DIALOG_HEIGHT, UI_LAYER_PROGRAM_WINDOW_DIALOG );

        dlg->AddButton( new DialogButton< decltype( bindSpecialButton ) >( "Yes", *dlg, bindSpecialButton, ButtonTypeT::Accept ) );
        dlg->AddButton( new DialogButton< decltype( cancel ) >( "Cancel", *dlg, cancel, ButtonTypeT::Decline ) );
        return;
    }

    DoBind( bindName );
}

void UI_Binds::BindKeyPressed( const std::string& button ) {
    if ( button == "escape" ) {
        CMSG("Bind cancelled\n");
    } else {
        std::string fullCommand;

        // delete clears
        if ( button == "delete" ) {
            if ( lastClickedBindType == "game" ) {
                Input::UnsetGameBind( lastClickedBindName );
            } else if ( lastClickedBindType == "ui" ) {
                Input::UnsetUIBind( lastClickedBindName );
            } else if ( lastClickedBindType == "editor" ) {
                Input::UnsetEditorBind( lastClickedBindName );
            } else {
                ERR("UI_Binds::BindKeyPressed(): Unknown bind type: %s\n", lastClickedBindType.c_str() );
                return;
            }

            fullCommand += "unsetting all ";
            fullCommand += lastClickedBindType;
            fullCommand += " binds for: ";
            fullCommand += lastClickedBindName;

            CMSG_LOG( fullCommand.c_str() );
        } else {
            std::vector< std::string > command;
            command.reserve(4);

            command.emplace_back("bind");
            command.emplace_back( lastClickedBindType );
            command.emplace_back( button );
            command.emplace_back( lastClickedBindName );

            fullCommand += "bind ";
            fullCommand += lastClickedBindType;
            fullCommand += " ";
            fullCommand += button;
            fullCommand += " ";
            fullCommand += lastClickedBindName;
            fullCommand += "\n";

            CMSG_LOG( fullCommand.c_str() );
            CommandSys::bind( command );
        }
    }

    UpdateBindLabels();

    // clicking a button on the dialog will close it, but none was clicked - so we have to do this manually
    if ( dlg ) {
        dlg->Close();
        dlg = nullptr;
    }
}

void UI_Binds::Create( void ) {
    ASSERT( ! created );
    created = true;

    const uint col_action_width = 200;

    LoadDefaultsConfig();

    bindMenu = new Window( WindowOptionsT::NO_COLLAPSE_BUTTON, 800,600 );
    bindMenu->SetOrigin(0,0,0);
    bindMenu->GetFace()->SetOpacity( 0.85f );
    bindMenu->SetVisible( false );
    bindMenu->SetMenu( true );
    bindMenu->SetPage( this );
    game->sprites[UI_LAYER_PROGRAM_WINDOW].Add( bindMenu );

    Label* lbl = new Label("Action", col_action_width, 20 );
    lbl->GetFace()->SetOpacity(0);
    lbl->SetFontSize(WIDGET_STANDARD_HEIGHT);
    lbl->SetFontColor( Vec3f(0,0,0) );
    lbl->SetOrigin(0,0,0);
    bindMenu->children->Add( lbl );

    lbl = new Label("Bindings", 150, 20 );
    lbl->GetFace()->SetOpacity(0);
    lbl->SetFontSize(WIDGET_STANDARD_HEIGHT);
    lbl->SetFontColor( Vec3f(0,0,0) );
    lbl->SetOrigin(200,0,0);
    bindMenu->children->Add( lbl );

    uint cur_tabstop=0;

    for ( uint line=0; line<ui_binds_description.size(); ++line ) {
        uint cur_row_pos = line * WIDGET_STANDARD_HEIGHT + WIDGET_STANDARD_HEIGHT;

        lbl = new Label(ui_binds_description[line].c_str(), col_action_width, 20 );
        //lbl->GetFace()->SetOpacity(0);
        lbl->SetFontSize(WIDGET_STANDARD_HEIGHT);
        lbl->SetFontColor( Vec3f(0,0,0) );
        lbl->SetOrigin(0,cur_row_pos,0);
        bindMenu->children->Add( lbl );

        std::string type_and_bind( ui_binds_types[line] );
        type_and_bind += "_";
        type_and_bind += ui_binds_names[line];

        Button_FuncWithArg* btnButtonBind=new Button_FuncWithArg( "unbound", &::BtnBind_Click, {type_and_bind}, ButtonTypeT::None, 650, WIDGET_STANDARD_HEIGHT);
        btnButtonBind->SetName( std::string( "btn_bind_" ) + type_and_bind );
        btnButtonBind->SetOrigin( col_action_width,cur_row_pos,0);
        btnButtonBind->GetFace()->SetOpacity(0.7f);
        btnButtonBind->SetFontSize(WIDGET_STANDARD_HEIGHT);
        btnButtonBind->SetTabstopIndex( ++cur_tabstop );
        bindMenu->children->Add( btnButtonBind );
    }
}

void UI_Binds::UpdateBindLabels( void ) {
    for ( auto& btn : bindMenu->children->list ) {

        const std::string btnName( btn->GetName() );

        // first characters are "btn_bind_"
        const std::string prefix = String::Sub( btnName, 0, 8 );
        if ( prefix != "btn_bind_" ) {
            continue;
        }

        std::string bindName = String::Sub( btnName, 9, btnName.size() );

        // ** prune out the bind type from the bindName
        std::string bindType;
        uint i;
        for ( i=0; i<bindName.size(); ++i ) {
            if ( bindName[i] == '_' ) {
                break;
            }
            bindType += bindName[i];
        }

        String::TrimLeadingNum( bindName, i+1 ); //+ for the underscore
        
        Label* lbl = dynamic_cast< Label* >( btn );
        if ( ! lbl ) {
            continue;
        }


        std::vector< std::string > bind;
        if ( bindType == "ui" ) {
            bind = Input::GetUIBind( bindName );
        } else if ( bindType == "game" ) {
            bind = Input::GetGameBind( bindName );
        } else if ( bindType == "editor" ) {
            bind = Input::GetEditorBind( bindName );
        } else {
            ERR("UI_Binds::UpdateBindLabels(): Unknown bind type: %s\n", bindType.c_str() );
            continue;
        }
        
        std::string newLabel;
        if ( bind.size() > 0 ) {
            newLabel = Input::joystick.GetButtonUIDisplayName( bind[0] );
            
            for ( uint b = 1; b<bind.size(); ++b ) {
                newLabel += " or ";
                newLabel += Input::joystick.GetButtonUIDisplayName( bind[b] );
            }
        }
        
        lbl->SetLabel( newLabel );
    }
}

void UI_Binds::LoadDefaultsConfig( void ) {

    std::string cfgPath( DataDir );
    cfgPath += "ui_binds.cfg";
    File file( cfgPath.c_str() );

    if ( ! file.IsOpen() ) {
        WARN("ui_binds.cfg file not found.\n");
        return;
    }

    CMSG_LOG("Loading cfg: %s\n", cfgPath.c_str() );

    ParserText parser( file );
    parser.SetCommentIndicator('#');

    std::string line;

    while ( ! file.EndOfFile() ) {
        parser.ReadUnquotedString( line );

        // ** don't keep comments
        String::Trim( line );
        if ( line[0] == '#' ) {
            parser.SkipToNextNonblankLine();
            continue;
        }

        // ** prune out the bind type
        std::string type;
        uint i;
        for ( i=0; i<line.size(); ++i ) {
            if ( line[i] == ' ' ) {
                break;
            }
            type += line[i];
        }

        if ( i >= line.size() ) {
            ERR("invalid ui_binds.cfg entry: %s\n", line.c_str() );
            parser.SkipToNextNonblankLine();
            continue;
        }

        while ( i < line.size() && line[i] == ' ' )
            ++i;

        String::TrimLeadingNum( line, i );

        // ** prune out the bind name
        std::string bind;
        for ( i=0; i<line.size(); ++i ) {
            if ( line[i] == ' ' ) {
                break;
            }
            bind += line[i];
        }

        if ( i >= line.size() ) {
            ERR("invalid ui_binds.cfg entry: %s\n", line.c_str() );
            parser.SkipToNextNonblankLine();
            continue;
        }

        String::TrimLeadingNum( line, i );

        ui_binds_types.emplace_back( type );
        ui_binds_names.emplace_back( bind );
        ui_binds_description.emplace_back( line );

        parser.SkipToNextNonblankLine();
    }

    CMSG_LOG("Loaded cfg: %s\n", cfgPath.c_str() );
}
