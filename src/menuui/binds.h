// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_MENUUI_BINDS_H_
#define SRC_MENUUI_BINDS_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../ui/ui.h"
#include "../gameui/gameui.h"

class UI_Binds : public GameUI_Page {
    friend void BtnBind_Click( const std::vector< std::string >& bindName );

public:
    UI_Binds( void );
    ~UI_Binds( void ) override { };
    UI_Binds( const UI_Binds& other ) = delete;

public:
    UI_Binds& operator=( const UI_Binds& other ) = delete;

    void Show( void ) override;
    void Hide( void ) override;

    void BindKeyPressed( const std::string& button );

private:
    void LoadDefaultsConfig( void );
    void UpdateBindLabels( void );
    void BtnBind_Click( const std::string& bindName );

private:
    void CancelBind( void );
    void DoBind( const std::string& bindName );
private:
    void Create( void );

private:
    std::vector< std::string > ui_binds_description; //!< paralell to ui_binds_names and ui_binds_names
    std::vector< std::string > ui_binds_names; //!< paralell to ui_binds_description and ui_binds_names
    std::vector< std::string > ui_binds_types; //!< paralell to ui_binds_description and ui_binds_names
    std::string lastClickedBindType;
    std::string lastClickedBindName;
    Window* bindMenu;
    Dialog* dlg;
    bool created;
};

#endif  // SRC_MENUUI_BINDS_H_
