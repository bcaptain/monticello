// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MENUUI_MENUUI_H_
#define SRC_MENUUI_MENUUI_H_

#include "../base/main.h"
#include "../sdl/sdl.h"
#include "../ui/ui.h"

class LoadBar;
class Dialog;

void QuitProgram_Click( void );
void NewGame_Click( void );
void ConfigureButtons_Click( void );

#include "../gameui/gameui.h"

class UI_MainMenu : public GameUI_Page {
public:
    UI_MainMenu( void );
    ~UI_MainMenu( void ) override { };
    UI_MainMenu( const UI_MainMenu& other ) = delete;

public:
    UI_MainMenu& operator=( const UI_MainMenu& other ) = delete;

    void Show( void ) override;
    void Hide( void ) override;

public:
    //void Toggle( void );
    //void SetOpen( const bool whether );

private:
    void Create( void );

private:
    Window* mainMenu;
    bool created;
};

#endif  // SRC_MENUUI_MENUUI_H_
