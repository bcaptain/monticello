// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./loadbar.h"

/*!

LoadBar

**/

LoadBar::LoadBar( const char* text, const uint width, const uint height )
    : Label( text, width, height )
{
    face.SetOpacity( UI_OPACITY );
    SetBorder( true );
}

LoadBar::~LoadBar( void ) {

}
