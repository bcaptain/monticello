// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./commandsys.h"
#include "./game.h"
#include "./console.h"
#include "./rendering/renderer.h"
#include "./editor/editor.h"
#include "./ui/dialog/common.h"
#include "./input/input.h"
#include "./entities/actor.h"
#include "./rendering/models/modelManager.h"

HashList< ConsoleCommand > CommandSys::commands;
float CommandSys::last_float;
Vec3f CommandSys::last_vec;
std::vector< std::pair< std::string, std::string > > CommandSys::cachedGameSetCommands;
std::vector< std::pair< std::string, std::string > > CommandSys::cachedEditorSetCommands;


void CommandSys::Init( void ) {
    Main_Init();
    Math_Init();
    Dev_Init();
}

void CommandSys::Main_Init( void ) {
    commands.Set("unset", &unset );
    commands.Set("set", &set );
    commands.Set("map", &map );
    commands.Set("force_map", &force_map );
    commands.Set("force_editor", &force_editor );
    commands.Set("save", &save );
    commands.Set("ls", &ls );
    commands.Set("len", &len );
    commands.Set("quit", &exit );
    commands.Set("exit", &exit );
    commands.Set("clear", &clear);
    commands.Set("v_restart", &v_restart );
    commands.Set("v_reloadTex", &v_reloadTex );
    commands.Set("exec", &exec );
    commands.Set("send_debug", &send_debug );
    commands.Set("clear_debug", &clear_debug );
    commands.Set("dlg", &dlg );
    commands.Set("bind", &bind );
    commands.Set("unbind", &unbind );
    commands.Set("num", &numEnts );
    commands.Set("loadPads", &loadPads );
}

void CommandSys::clear_debug( [[maybe_unused]] const std::vector< std::string >& args_unused ) {
    ClearDebug();
}

void CommandSys::send_debug( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("send_debug takes exactly 1 argument\n");
        return;
    }

    SendDebug( args[1].c_str() );
}

void CommandSys::dlg( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("dlg takes exactly 1 argument\n");
        return;
    }

    Dialog::Create( args[1].c_str() );
}

void CommandSys::SendDebug( const std::string& str ) {
    const char* debug_file("debug.exec");
    File file;
    if ( ! file.Open( debug_file, "ab" ) ) {
        ERR("Couldn't save debug report.\n");
        return;
    }
    file.WriteChar( str.c_str(), str.size() );
    file.FFlush();
}

void CommandSys::exec( const std::vector< std::string >& args ) {
    File file;

    if ( args.size() != 2 ) {
        ERR("usage: exec [optional_file].\n");
        return;
    }

    if ( ! file.Open( args[1].c_str(), "rb" ) ) {
        ERR("Couldn't open file: %s\n", args[1].c_str());
        return;
    }

    file.ReadToBuffer();
    const char *buf = file.GetBuffer();

    std::string line;
    line.reserve( 100 );
    const int size = file.GetSize();
    for ( int i=0; i < size; ++i ) {
        if ( buf[i] == '\n' ) {
            ASSERT( console );
            console->Append( line, HistoryDisplayT::COMMAND );
            Execute( line.c_str() );
            line.erase();
        } else {
            line += buf[i];
        }
    }
}

void CommandSys::ClearDebug( void ) { //!< clear the file debug.exec
    const char* debug_file("debug.exec");
    File file(debug_file, "wb");
    file.Close();
}

void CommandSys::Execute( const char* str ) {
    std::vector< std::string > list =String::ListifyString( str );
    if ( list.size() < 1 )
        return;
    Execute( list );
}

void CommandSys::Execute( const std::vector<std::string>& args ) {
    if ( args.size() < 1 )
        return;

    const ConsoleCommand *cmd = commands.Get( args[0].c_str() );
    if ( cmd ) {
        (*cmd)( args );
    } else {
        std::string fullCmd;
        for ( uint i=0; i<args.size(); ++i ) {
            fullCmd += args[i];
            fullCmd += " ";
        }
        WARN("Command has no effect, maybe mispelled or too many/few arguments: %s\n", fullCmd.c_str());
    }
}

//
//
// Messages
//
//

void CommandSys::EditorNotOpenError( void ) {
    ERR("Open the editor before using that command (type 'editor' and then enter)\n.");
}

//
//
// Commands
//
//

void CommandSys::exit( [[maybe_unused]] const std::vector< std::string >& args ) {
    game->SetState( GameStateT::SHUTDOWN );
}

void CommandSys::clear( [[maybe_unused]] const std::vector< std::string >& args ) {
    console->ClearHistory();
}

void CommandSys::v_restart( [[maybe_unused]] const std::vector< std::string >& args ) {
    renderer->UpdateWindowSize();
}

void CommandSys::v_reloadTex( [[maybe_unused]] const std::vector< std::string >& args ) {
    textureManager.Reload();
}

void CommandSys::unset( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("unset takes exactly 1 argument\n");
        return;
    }

    globalVals.Remove( args[1].c_str() );
}

void CommandSys::map( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: map [map_name]\n");
        return;
    }

    Dialog_LoadMap(args[1]);
}

void CommandSys::force_editor( [[maybe_unused]] const std::vector< std::string >& args_unused ) {
    ASSERT( game );
    game->ToggleEditor();
}

void CommandSys::force_map( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: map [map_name]\n");
        return;
    }

    game->LoadMap(args[1]);
}

void CommandSys::save( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: save [map_name]\n");
        return;
    }

   Dialog_SaveMap( args[1] );
}

void CommandSys::ls( const std::vector< std::string >& args ) {
    std::string dir;
    switch ( args.size() ) {
        case 1:
            dir = ".";
            break;
        case 2:
            dir = args[1];
            break;
        default:
            ERR("usage: ls [dir_name]\n");
            return;
    }

    LinkList< std::string > files;
    FileSystem::ListDir(dir.c_str(), files);
    files.SortData();

    for ( auto const& str : files )
        CMSG("%s\n", str.c_str());
}

bool CommandSys::CheckSetEvent( const std::string& var, const std::string& value ) {

    if ( var.compare( gval_v_width.first ) == 0 || var.compare( gval_v_height.first ) == 0 ) {
        if ( renderer ) {
            renderer->UpdateWindowSize();
        } else {
            CacheGameSetCommand( var, value );
        }
        return true;
    }
    
    if ( var.compare( gval_v_mipmap.first ) == 0 ) {
        if ( game && renderer ) {
            textureManager.Reload();
        }
        return true;
    }

    if ( var == gval_d_octree.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            game->entTree->ShowAll( globalVals.GetBool( gval_d_octree ) );
        }
        return true;
    }

    if ( var == gval_d_voxelTrace.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            game->entTree->ShowTrace( globalVals.GetBool( gval_d_voxelTrace ) );
        }
        return true;
    }
/* todo
    if ( var == gval_d_voxelDensity ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            game->entTree->ShowTrace( globalVals.GetBool( gval_d_voxelDensity ) );
        }
        return true;
    }
*/
    if ( var == gval_d_names.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            if ( globalVals.GetBool( gval_d_names ) ) {
                game->CallOnAllEntities( &Entity::pack_and_send_name_vbo );
            } else {
                game->CallOnAllEntities( &Entity::destroy_name_vbo ); // names will regenerate for entities which are set to always show them
            }
        }
        return true;
    }
    
    if ( var == gval_d_vertNormals.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            if ( globalVals.GetBool( gval_d_vertNormals ) ) {
                game->CallOnAllEntities( &Entity::pack_and_send_vert_normals_vbo );
            } else {
                game->CallOnAllEntities( &Entity::destroy_vert_normals_vbo );
            }
        }
    }
    
    if ( var == gval_d_vertNormalsLen.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            if ( globalVals.GetBool( gval_d_vertNormals ) ) {
                //Repacks vertex normal VBO's and redraw
                game->CallOnAllEntities( &Entity::destroy_vert_normals_vbo );
                game->CallOnAllEntities( &Entity::pack_and_send_vert_normals_vbo );
            }
        }
    }
    
    if ( var == gval_g_octreeSize.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            Vec3f size;
            String::ToVec3f( value, size );
            game->SetEntTreeDimensions(
                static_cast< uint >( size.x ),
                static_cast< uint >( size.y ),
                static_cast< uint >( size.z )
            );
        }
    }
    
    if ( var == gval_g_octreeSubs.first ) {
        if ( !game->IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            // reload the octree
            const uint subs = String::ToUInt( value );
            game->SetEntTreeSubdivisions(
                static_cast< uint >( subs )
            );
        }
    }
    
    return false;
}

void CommandSys::set_var( const std::string& var, const std::string& value ) {
    globalVals.Set( var.c_str(), value.c_str() );

    if ( CheckSetEvent( var, value ) )
        return;

#ifdef MONTICELLO_EDITOR
    if ( var.compare( gval_e_pan_scale.first ) == 0 ) {
        if ( editor ) {
            editor->SetPanScale( String::ToFloat( value ) );
        } else {
            CacheEditorSetCommand( var, value );
        }
        return;
    }

    if ( var.compare( gval_e_zoom_scale.first ) == 0 ) {
        if ( editor ) {
            editor->SetZoomScale( String::ToFloat( value ) );
        } else {
            CacheEditorSetCommand( var, value );
        }
        return;
    }
    
    if ( var.compare( gval_d_clipModels.first ) == 0 ) {
        if ( gval_d_clipModels.second != 0 ) {
            modelManager.PackAllClipModelVBOs();
        }
        
        return;
    }
#endif // MONTICELLO_EDITOR

    if ( var == gval_v_cullFace.first ) {
        if ( !sdl.IsInitialized() ) {
            CacheGameSetCommand( var, value );
        } else {
            if ( value == "none" ) {
                glDisable( GL_CULL_FACE );
                return;

            } else if ( value == "back" ) {
                glCullFace( GL_BACK );

            } else if ( value == "front" ) {
                glCullFace( GL_FRONT );

            } else if ( value == "both" ) {
                glCullFace( GL_FRONT_AND_BACK );

            } else {
                WARN("You must specify to cull front, back, both, or none\n");
                return;
            }
        }
    }
}

void CommandSys::display_var( std::string var_byval ) {
    uint descriptionIndex;
    for ( descriptionIndex=0; descriptionIndex<varDescriptions.size(); ++descriptionIndex )
        if ( varDescriptions[descriptionIndex].first == var_byval )
            break;

    std::string desc(var_byval);
    desc += ": ";
    if ( descriptionIndex < varDescriptions.size() ) {
        desc += varDescriptions[descriptionIndex].second;
    } else {
        desc += "no description.";
    }

#ifdef MONTICELLO_EDITOR
    if ( var_byval.compare( gval_e_pan_scale.first ) == 0 ) {
        if ( !editor ) {
            EditorNotOpenError();
            return;
        }
        var_byval += " is ";
        var_byval += "\"";
        var_byval += std::to_string( editor->GetPanScale() );
        var_byval += "\"";
    } else if ( var_byval.compare( gval_e_zoom_scale.first ) == 0 ) {
        if ( !editor ) {
            EditorNotOpenError();
            return;
        }
        var_byval += " is ";
        var_byval += "\"";
        var_byval += std::to_string( editor->GetZoomScale() );
        var_byval += "\"";
    } else
#endif // MONTICELLO_EDITOR
    {
        const std::string* val = globalVals.Get( var_byval.c_str() );

        var_byval += " is ";
        if ( !val ) {
            var_byval += "not set";
        } else {
            var_byval += "\"";
            var_byval += *val;
            var_byval += "\"";
        }
    }

    console->Append( desc, HistoryDisplayT::MESSAGE );
    console->Append( var_byval, HistoryDisplayT::MESSAGE );
}

void CommandSys::set( const std::vector<std::string>& args ) {
    switch ( args.size() ) {
        case 3:
            set_var( args[1], args[2] );
            return;
        case 2:
            display_var( args[1] );
            return;
        default:
            ERR("usage: set [type_of_item] // shows what it's set to\nusage: set [type_of_item] [item_name] // sets the variable \n");
            return;
    }
}

void DoBind( const uint button, const std::string& action, std::vector< BindInfo >& binds, const std::vector< std::string >& combo, const BindType bindType );
void DoBind( const uint button, const std::string& action, std::vector< BindInfo >& binds, const std::vector< std::string >& combo, const BindType bindType ) {
    if ( binds.size() < button+1 )
        binds.resize( button+1 );

    // if we already have the exact combo bound, re-assign it
    for ( uint i=0; i< binds[button].all.size(); ++i ) {
        if ( binds[button].all[i].combo == combo ) {
            binds[button].all[i].action = action;
            return;
        }
    }

    // doesn't exist yet, so add it
    const uint i = binds[button].all.size();
    binds[button].all.resize(i+1);
    binds[button].all[i].action = action;
    binds[button].all[i].combo = combo;
    binds[button].all[i].type = bindType;
}

void ShowBind( const std::string& button_name, const uint button, std::vector< BindInfo >& binds );
void ShowBind( const std::string& button_name, const uint button, std::vector< BindInfo >& binds ) {
    uint printed = 0;

    if ( button < binds.size() ) {
        if ( binds[button].all.size() > 0 ) {

            for ( uint i=0; i < binds[button].all.size(); ++i ) {
                // if the action is blank, we are essentially unbound
                if ( binds[button].all[i].action.size() < 1 )
                    continue;

                ++printed;
                std::string line;

                for ( auto const & mod : binds[button].all[i].combo ) {
                    line += mod.c_str();
                    line += " ";
                }

                CMSG( "%s is bound to \"%s\"\n", line.c_str(), binds[button].all[i].action.c_str() );
            }
        }
    }

    if ( printed < 1 ) {
        CMSG( "%s is not bound\n", button_name.c_str() );
    }
}

bool ValidateButtonName( const std::string& button );
bool ValidateButtonName( const std::string& button ) {
    if ( String::Left( button, 5 ) == "mouse" ) {
        const uint index = String::ToUInt( String::Sub( button, 5, button.size()-1 ) );
        return index < MAX_MOUSE_BUTTONS;
    }

    if ( String::Left( button, 3 ) == "joy" ) {
        const uint index = String::ToUInt( String::Sub( button, 3, button.size()-1 ) );
        return index <= ( Input::joystick.GetNumButtons() );
    }
    
    // axis can be axis1, axis1+, or axis1- where 1 can be any number
    if ( String::Left( button, 4 ) == "axis" ) {
        const uint index = String::ToUInt( String::Sub( button, 3, button.size()-1 ) );
        return index < MAX_JOYSTICK_AXIS;
    }

    return Input::GetButtonIDByName( button ) != "";
}

bool ValidateKeyNames( const std::vector< std::string >& buttons );
bool ValidateKeyNames( const std::vector< std::string >& buttons ) {
    for ( auto const & button : buttons )
        if ( !ValidateButtonName( button ) )
            return false;
    return true;
}

bool TryBind( const std::string& device_name, const std::string& button, const std::string *action, const std::vector< std::string >& combo, const BindType bindType, std::vector< BindInfo >& binds, const uint max_buttons );
bool TryBind( const std::string& device_name, const std::string& button, const std::string *action, const std::vector< std::string >& combo, const BindType bindType, std::vector< BindInfo >& binds, const uint max_buttons ) {
    std::string button_s;
    uint button_u;

    // check that the device type is the one we're attempting
    if ( device_name.size() > 0 ) {
        if ( String::Left( button, device_name.size() ) != device_name ) {
            return false;
        }

        button_s = String::Sub( button, device_name.size(), button.size() );
        button_u = String::ToUInt( button_s );
    } else {
        // find the button id
        for ( button_u=0; button_u<SDL_NUM_SCANCODES; ++button_u )
            if ( Input::keyNames[button_u] == button )
                break;
    }
    
    if ( button_u > max_buttons ) {
        return false;
    }

    if ( ! ValidateKeyNames( combo ) ) {
        return false;
    }

    if ( action ) {
        DoBind( button_u, *action, binds, combo, bindType );
    } else {
        ShowBind( button, button_u, binds );
    }

    return true;
}

void DeviceBind( const std::string& button, const std::string *action, const std::vector< std::string >& combo, const BindType bindType, std::vector< BindInfo >& mouse_binds, std::vector< BindInfo >& joy_binds, std::vector< BindInfo >& axis_binds, std::vector< BindInfo >& key_binds );
void DeviceBind( const std::string& button, const std::string *action, const std::vector< std::string >& combo, const BindType bindType, std::vector< BindInfo >& mouse_binds, std::vector< BindInfo >& joy_binds, std::vector< BindInfo >& axis_binds, std::vector< BindInfo >& key_binds ) {
    if ( TryBind( "mouse", button, action, combo, bindType, mouse_binds, MAX_MOUSE_BUTTONS ) )
        return;
    
    if ( TryBind( "axis", button, action, combo, bindType, axis_binds, MAX_JOYSTICK_AXIS ) )
        return;
    
    if ( TryBind( "joy", button, action, combo, bindType, joy_binds, Input::joystick.GetNumButtons() ) )
        return;

    if ( TryBind( "", button, action, combo, bindType, key_binds, SDL_NUM_SCANCODES-1 ) ) // the last button isn't a button
        return;
    
    std::string message;
    if ( action ) {
        message = "failure binding button [ ";
        for ( const auto & c : combo ) {
            message += c;
            message += " ";
        }
        message += "] to action \"";
        message += *action;
        message += "\"";
    } else {
        message = "failure unbinding button [ ";
        message += button;
        message += " ]";
    }
    
    ERR("%s\n", message.c_str());
}

void CommandSys::loadPads( [[maybe_unused]] const std::vector<std::string>& args_unused ) {
    SDL::LoadControllerDatabase();
}

void CommandSys::numEnts( [[maybe_unused]] const std::vector<std::string>& args_unused ) {
    CMSG_LOG( "There are %u entities in the world.\n", game->NumEnts() );
}

void CommandSys::unbind( const std::vector<std::string>& args ) {
    if ( args.size() < 3 ) {
        ERR("usage: unbind [bindtype] [button_names] // unbind a button combination\nbindtype is either game, ed, ed_nav. ed_select, or ed_add\n");
        return;
    }

    const std::string bindType_s(args[1] ); // first is the bindType

    BindType bindType( BindType::Invalid );
    if ( bindType_s == "game" ) {
        bindType = BindType::Game;
    } else if ( bindType_s == "ed" ) {
        bindType = BindType::Editor;
    } else if ( bindType_s == "ed_nav" ) {
        bindType = BindType::Editor_Navmesh;
    } else if ( bindType_s == "ed_select" ) {
        bindType = BindType::Editor_Select;
    } else if ( bindType_s == "ed_add" ) {
        bindType = BindType::Editor_Add;
    } else if ( bindType_s == "ui" ) {
        bindType = BindType::UI;
    }

    const auto & key_name = args[args.size()-1];
    const std::string key( Input::joystick.GetGenericButtonName( key_name ) ); // last argument is the key

    // all other args (if there are any) are modifier buttons
    std::vector< std::string > combo;
    combo.reserve( args.size() );
    for ( uint i=2; i<args.size()-2; ++i ) {
        combo.emplace_back( Input::joystick.GetGenericButtonName( args[i] ) );
    }

    combo.emplace_back( key );

    const std::string empty("");

    switch ( bindType ) {
        case BindType::Game:
            DeviceBind( key, &empty, combo, bindType, game_mouse_binds, game_joy_binds, game_axis_binds, game_key_binds );
            break;
        case BindType::Editor:
            DeviceBind( key, &empty, combo, bindType, editor_mouse_binds, editor_joy_binds, game_axis_binds, editor_key_binds );
            break;
        case BindType::Editor_Navmesh:
            DeviceBind( key, &empty, combo, bindType, editor_navmeshmode_mouse_binds, editor_navmeshmode_joy_binds, game_axis_binds, editor_navmeshmode_key_binds );
            break;
        case BindType::Editor_Select:
            DeviceBind( key, &empty, combo, bindType, editor_selectmode_mouse_binds, editor_selectmode_joy_binds, game_axis_binds, editor_selectmode_key_binds );
            break;
        case BindType::Editor_Add:
            DeviceBind( key, &empty, combo, bindType, editor_addmode_mouse_binds, editor_addmode_joy_binds, game_axis_binds, editor_addmode_key_binds );
            break;
        case BindType::UI:
            DeviceBind( key, &empty, combo, bindType, ui_mouse_binds, ui_joy_binds, ui_axis_binds, ui_key_binds );
            return;
        case BindType::Invalid:
            FALLTHROUGH;
        default:
            ERR("Invalid bind type: %s\n", bindType_s.c_str() );
            return;
    }
}

void CommandSys::bind( const std::vector<std::string>& args ) {
    if ( args.size() < 3 ) {
        ERR("usage: bind [bindtype] [button_names] // shows what action a button is\n");
        ERR("set to\n\n");
        
        ERR("usage: bind [bindtype] [button_names] [action] // sets the key to the\n");
        ERR("action. bindtype is either game, ed, ed_nav. ed_select, or ed_add\n");
        return;
    }

    const std::string bindType_s(args[1] ); // first is the bindType

    BindType bindType( BindType::Invalid );
    if ( bindType_s == "game" ) {
        bindType = BindType::Game;
    } else if ( bindType_s == "ed" ) {
        bindType = BindType::Editor;
    } else if ( bindType_s == "ed_nav" ) {
        bindType = BindType::Editor_Navmesh;
    } else if ( bindType_s == "ed_select" ) {
        bindType = BindType::Editor_Select;
    } else if ( bindType_s == "ed_add" ) {
        bindType = BindType::Editor_Add;
    } else if ( bindType_s == "ui" ) {
        bindType = BindType::UI;
    }

    std::string action;
    std::string *action_p=nullptr;
    std::string key;

    // are we just checking a bind? (no action supplied)
    if ( args.size() < 4 ) {
        key = args[ args.size()-1 ]; // last argument is the key
        action_p = nullptr;
    } else {
        action = args[args.size()-1]; // last argument is the action name
        action_p = &action;
        key = args[ args.size()-2 ]; // second to last is the key
    }
    
    key = Input::joystick.GetGenericButtonName( key );

    // all other args (if there are any) are modifier buttons
    std::vector< std::string > combo;
    combo.reserve( args.size() );
    for ( uint i=2; i<args.size()-2; ++i ) {
        combo.emplace_back( Input::joystick.GetGenericButtonName( args[i] ) );
    }

    combo.emplace_back( key );

    switch ( bindType ) {
        case BindType::Game:
            DeviceBind( key, action_p, combo, bindType, game_mouse_binds, game_joy_binds, game_axis_binds, game_key_binds );
            break;
        case BindType::Editor:
            DeviceBind( key, action_p, combo, bindType, editor_mouse_binds, editor_joy_binds, game_axis_binds, editor_key_binds );
            break;
        case BindType::Editor_Navmesh:
            DeviceBind( key, action_p, combo, bindType, editor_navmeshmode_mouse_binds, editor_navmeshmode_joy_binds, game_axis_binds, editor_navmeshmode_key_binds );
            break;
        case BindType::Editor_Select:
            DeviceBind( key, action_p, combo, bindType, editor_selectmode_mouse_binds, editor_selectmode_joy_binds, game_axis_binds, editor_selectmode_key_binds );
            break;
        case BindType::Editor_Add:
            DeviceBind( key, action_p, combo, bindType, editor_addmode_mouse_binds, editor_addmode_joy_binds, game_axis_binds, editor_addmode_key_binds );
            break;
        case BindType::UI:
            DeviceBind( key, action_p, combo, bindType, ui_mouse_binds, ui_joy_binds, ui_axis_binds, ui_key_binds );
            break;
        case BindType::Invalid:
            FALLTHROUGH;
        default:
            ERR("Invalid bind type: %s\n", bindType_s.c_str() );
            return;
    }
}

void CommandSys::CacheGameSetCommand( const std::string& key, const std::string& val ) {
    Printf("caching set command because game is not ready: \"set %s %s\"\n", key.c_str(), val.c_str() );

    cachedGameSetCommands.push_back( {key,val} );
}
void CommandSys::CacheEditorSetCommand( const std::string& key, const std::string& val ) {
    Printf("caching set command because editor is not ready: \"set %s %s\"\n", key.c_str(), val.c_str() );

    cachedEditorSetCommands.push_back( {key,val} );
}

void CommandSys::GameIsReady( void ) {
    ASSERT( game );
    const std::vector< StringPair > cmds = cachedGameSetCommands;
    cachedGameSetCommands.clear();

    for ( const StringPair & cmd : cmds ) {
        CMSG_LOG("executing cached set command [game]: \"%s %s\"\n", cmd.first.c_str(), cmd.second.c_str() );
        set_var( cmd.first, cmd.second );
    }

    if ( cachedGameSetCommands.size() != 0 ) {
        ERR("CommandSys::GameIsReady(): Subsystem not loaded for \"set %s %s\", dropping it from queue.\n");
        cachedGameSetCommands.clear();
    }
}

void CommandSys::EditorIsReady( void ) {
    ASSERT( game );
    const std::vector< StringPair > cmds = cachedEditorSetCommands;
    cachedEditorSetCommands.clear();

    for ( const StringPair & cmd : cmds ) {
        CMSG_LOG("executing cached set command [editor]: \"%s %s\"\n", cmd.first.c_str(), cmd.second.c_str() );
        set_var( cmd.first, cmd.second );
    }

    if ( cachedEditorSetCommands.size() != 0 ) {
        ERR("CommandSys::EditorIsReady(): Subsystem not loaded for \"set %s %s\", dropping it from queue.\n");
        cachedEditorSetCommands.clear();
    }
}
