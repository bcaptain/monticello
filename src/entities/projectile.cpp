// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./projectile.h"
#include "../sound.h"
#include "../particles/effectsManager.h"
#include "./actor.h"
#include "../damage/damageManager.h"

const int PROJECTILE_DEFAULT_DURATION = 5000;
const int PROJECTILE_DEFAULT_TRAIL_DELAY = 0;
const float PROJECTILE_DEFAULT_SPEED = 0;
const float PROJECTILE_MINIMUM_SPEED = 0.05f;
const Uint32 HUGE_DELAY = 10000;
const Uint32 DEFAULT_INTERSECT_DELAY  = 100;

constexpr const bool intersectDamageDebug = false;
constexpr const bool damageDebug = false;
constexpr const bool projectileFlightDebug = false;

extern const uint ANIM_FRAME_DURATION_MS;

std::vector< std::string > Projectile::projectileSetters;

void Projectile::InitSetterNames( void ) {
    if ( projectileSetters.size() == Projectile_NUM_SETTERS )
        return; // already set
        
    const uint i = Projectile_SETTERS_START;
    projectileSetters.resize(Projectile_NUM_SETTERS);
    projectileSetters[Projectile_SetLaunchEffect-i] = "fx_launch";
    projectileSetters[Projectile_SetAttachedEffect-i] = "fx_attached";
    projectileSetters[Projectile_SetTrailEffect-i] = "fx_trail";
    projectileSetters[Projectile_SetExplodeEffect-i] = "fx_explode";
    projectileSetters[Projectile_SetTrailDelay-i] = "trailDelay";
    projectileSetters[Projectile_SetDuration-i] = "duration";
    projectileSetters[Projectile_SetSpeed-i] = "speed";
    projectileSetters[Projectile_SetSpeedMod-i] = "speedMod";
    projectileSetters[Projectile_SetExplodeDamage-i] = "explodeDamage";
    projectileSetters[Projectile_SetHitDamage-i] = "dmg_hit";
    projectileSetters[Projectile_SetExplodeOnImpact-i] = "explode_on_impact";
    projectileSetters[Projectile_SetExplodeOnExpire-i] = "explode_on_expire";
    projectileSetters[Projectile_SetDamageIntersectors-i] = "intersectDamage";
    projectileSetters[Projectile_SetIntersectDelay-i] = "intersectDelay";
    projectileSetters[Projectile_SetLaunchSound-i] = "snd_launch";
    projectileSetters[Projectile_SetExplodeSound-i] = "snd_explode";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Projectile,Entity>(projectileSetters);
}

bool Projectile::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<ProjectileSetterT>(setter) ) {
        case Projectile_SetLaunchEffect: SetLaunchEffect( value ); return true;
        case Projectile_SetAttachedEffect: SetAttachedEffect( value ); return true;
        case Projectile_SetTrailEffect: SetTrailEffect( value ); return true;
        case Projectile_SetExplodeEffect: SetExplodeEffect( value ); return true;
        //case Projectile_SetPrevTrail: SetPrevTrail( String::ToUInt( value ) ); return true; //No need to save/load??
        case Projectile_SetTrailDelay: SetTrailDelay( String::ToUInt( value ) ); return true;
        case Projectile_SetDuration: SetDuration( String::ToUInt( value ) ); return true;
        case Projectile_SetSpeed: SetSpeed( String::ToFloat( value ) ); return true;
        case Projectile_SetSpeedMod: SetSpeedMod( String::ToFloat( value ) ); return true;
        case Projectile_SetExplodeDamage: SetExplodeDamage( value ); return true;
        case Projectile_SetHitDamage: SetHitDamage( value ); return true;
        case Projectile_SetExplodeOnImpact: SetExplodeOnImpact( String::ToBool( value ) ); return true;
        case Projectile_SetExplodeOnExpire: SetExplodeOnExpire( String::ToBool( value ) ); return true;
        case Projectile_SetDamageIntersectors: SetDamageIntersectors( String::ToBool( value ) ); return true;
        case Projectile_SetIntersectDelay: SetIntersectDelay( String::ToUInt( value ) ); return true;
        case Projectile_SetLaunchSound: SetLaunchSound( value ); return true;
        case Projectile_SetExplodeSound: SetExplodeSound( value ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Projectile_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Projectile_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Projectile>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Projectile::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<ProjectileSetterT>(setter) ) {
        case Projectile_SetLaunchEffect: return GetLaunchEffectName();
        case Projectile_SetAttachedEffect: return GetAttachedEffectName();
        case Projectile_SetTrailEffect: return GetTrailEffectName();
        case Projectile_SetExplodeEffect: return GetExplodeEffectName();
        //case Projectile_SetPrevTrail: return String::ToString(GetPrevTrail()); //No need to save/load??
        case Projectile_SetTrailDelay: return String::ToString(GetTrailDelay());
        case Projectile_SetDuration: return String::ToString(GetDuration());
        case Projectile_SetSpeed: return String::ToString(GetSpeed());
        case Projectile_SetSpeedMod: return String::ToString(GetSpeedMod());
        case Projectile_SetExplodeDamage: return GetExplodeDamageName();
        case Projectile_SetHitDamage: return GetHitDamageName();
        case Projectile_SetExplodeOnImpact: return String::ToString(GetExplodeOnImpact());
        case Projectile_SetExplodeOnExpire: return String::ToString(GetExplodeOnExpire());
        case Projectile_SetDamageIntersectors: return String::ToString(GetDamageIntersectors());
        case Projectile_SetIntersectDelay: return String::ToString(GetIntersectDelay());
        case Projectile_SetLaunchSound: return GetLaunchSoundName();
        case Projectile_SetExplodeSound: return GetExplodeSoundName();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Projectile_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Projectile_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Projectile>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Projectile::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret = GetSetterIndexIn( projectileSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Projectile_SETTERS_START;
    
    static_assert( is_parent_of<Projectile>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn"
#pragma GCC diagnostic ignored "-Wunused-parameter"
    void Projectile::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
        ERR("Aborting (1/2): Projectiles only exist during game-time and therefore should not be saved to maps.\n");
        ERR("Aborting (2/2): The current map will not be over-written.\n");
        ASSERT(false);
    }

    void Projectile::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
        ERR("Aborting (1/2): Projectiles only exist during game-time and therefore should not be saved to maps.\n");
        ERR("Aborting (2/2): The map being loaded is invalid.\n");
        ASSERT(false);
    }
#pragma GCC diagnostic pop

Projectile::Projectile( void )
    : damage_explode( nullptr )
    , damage_hit( nullptr )
    , launchPoint()
    , dir(0,1,0)
    , snd_launch()
    , snd_explode()
    , fx_launch(nullptr)
    , fx_explode(nullptr)
    , fx_trail(nullptr)
    , fx_attached(nullptr)
    , owner()
    , target()
    , intersectingEntities()
    , prevTrail(0)
    , trailDelay(PROJECTILE_DEFAULT_TRAIL_DELAY)
    , duration(PROJECTILE_DEFAULT_DURATION)
    , bornTime(game->GetGameTime() + PROJECTILE_DEFAULT_DURATION)
    , speed(PROJECTILE_DEFAULT_SPEED)
    , speedMod(0.0f)
    , status(ProjectileStatusT::NONE)
    , intersect_delay( DEFAULT_INTERSECT_DELAY )
    , opts(ProjectileOptsT::Empty)
    {
}

Projectile::~Projectile( void ) {

}

Projectile::Projectile( const Projectile& other )
    : Entity( other )
    , damage_explode(other.damage_explode)
    , damage_hit(other.damage_hit)
    , launchPoint(other.launchPoint)
    , dir(other.dir)
    , snd_launch(other.snd_launch)
    , snd_explode(other.snd_explode)
    , fx_launch(other.fx_launch)
    , fx_explode(other.fx_explode)
    , fx_trail(other.fx_trail)
    , fx_attached(other.fx_attached)
    , owner(other.owner)
    , target(other.target)
    , intersectingEntities(other.intersectingEntities)
    , prevTrail(other.prevTrail)
    , trailDelay(other.trailDelay)
    , duration(other.duration)
    , bornTime(other.bornTime)
    , speed(other.speed)
    , speedMod(other.speedMod)
    , status(other.status != ProjectileStatusT::EXPLODED ? other.status : ProjectileStatusT::NONE)
    , intersect_delay(other.intersect_delay)
    , opts(other.opts)
{ }

Projectile& Projectile::operator=( const Projectile& other ) {
    damage_hit = other.damage_hit;
    damage_explode = other.damage_explode;
    dir = other.dir;
    snd_launch = other.snd_launch;
    snd_explode = other.snd_explode;
    fx_launch = other.fx_launch;
    fx_explode = other.fx_explode;
    fx_trail = other.fx_trail;
    fx_attached = other.fx_attached;
    owner = other.owner;
    prevTrail = other.prevTrail;
    trailDelay = other.trailDelay;
    duration = other.duration;
    bornTime = other.bornTime;
    speed = other.speed;
    speedMod = other.speedMod;
    status = other.status != ProjectileStatusT::EXPLODED ? other.status : ProjectileStatusT::NONE;

    Entity::operator=( other );
    return *this;
}

void Projectile::RunFrame( void ) {
    Entity::RunFrame();

    if ( !IsSpawned() )
        return;
        
    if ( ReadyToAnimate() ) {
        Animate();
    }
}

void Projectile::Think( void ) {
    if ( !IsSpawned() )
        return;

    if ( game->GetGameTime() - bornTime > duration ) {
        Expire();
    }
    
    std::shared_ptr< Entity > target_ent = target.lock();
    if ( target_ent ) {
        dir = target_ent->GetOrigin() - GetOrigin();
        dir.Normalize();
    }

    if ( status == ProjectileStatusT::LAUNCHED ) {
        DamageIntersectingEntities();
    }
    
    if ( fx_trail ) {
        if ( game->GetGameTime() - prevTrail > trailDelay ) {
            effectsManager.Spawn( *fx_trail, GetOrigin() );
            prevTrail = game->GetGameTime();
        }
    }

    speed += (speedMod/FPS_CAP);
    
    if ( speed < PROJECTILE_MINIMUM_SPEED ) {
        SetVelocity( Vec3f() );
        
        if constexpr ( projectileFlightDebug ) {
            float dist_traveled = Vec3f( GetOrigin() - launchPoint ).Len();
            if ( speedMod < 0 )
                entVals.SetFloat("saveSpeedMod", speedMod); //debug entval
            Printf("F|SM|D : %u | %f.3 | %f.3\n", game->GetGameFrame(), entVals.GetFloat("saveSpeedMod"), dist_traveled );  //toNamedSetter
        }
        
        speed = speedMod = 0;
    }
    
    if ( speed > 0 ) {
        Vec3f savedGravity;
        if ( IsGravityEnabled() ) {
            savedGravity.z = bounds.GetVelocity().z;
        }
        bounds.SetVelocity( (dir * speed) + savedGravity);
    }
    
    Entity::Think();
}

void Projectile::DamageIntersectingEntities( void ) {
    if ( ! damage_hit )
        return;

    const Uint32 curTime = game->GetGameTime();
    
    for ( uint i=0; i<intersectingEntities.size(); ++i ) {
        if ( !intersectingEntities[i].IsTimeUp(curTime) ) {
            continue;
        }
            
        intersectingEntities[i].UpdateTime(curTime);
        
        auto ent = intersectingEntities[i].data.ent.lock();
        std::shared_ptr< Primitive > primitive;
        if ( ent ) {
            primitive = ent->bounds.primitive;
        }
        
        if ( !ent || !primitive || !Intersects( *primitive ) ) {
            intersectingEntities[i].SetDelay( HUGE_DELAY );
            PopSwap( intersectingEntities, i-- );
            continue;
        }
            
        if constexpr ( intersectDamageDebug ) {
            CMSG_LOG("%u: Projectile: Intersect Damage to %s. Delay(%u) Prev(%u),\n"
                , game->GetGameTime()
                , ent->GetIdentifier().c_str()
                , intersectingEntities[i].GetDelay()
                , intersectingEntities[i].GetPrev()
            );
        }
        
        damage_hit->Inflict( *ent, owner );
    }
}

void Projectile::Expire( void ) {
    if ( BitwiseAnd( opts, ProjectileOptsT::ExplodeOnExpire ) ) {
        Explode();
    } else {
        MarkForDeletion();
    }
}

void Projectile::Launch( void ) {
    if ( status != ProjectileStatusT::NONE )
        return;

    if ( !IsSpawned() )
        Spawn();
        
    bornTime = game->GetGameTime();
    launchPoint = GetOrigin();

    if ( snd_launch.size() > 0 )
        soundManager.PlayAt( GetOrigin(), snd_launch.c_str() );

    if ( fx_launch )
        effectsManager.Spawn( *fx_launch, GetOrigin() );

    status = ProjectileStatusT::LAUNCHED;
}

void Projectile::SetEffectFromGVal( const EntVal& entvalHash, EFFECT_SETTER_METHOD_PTR effectSetter ) {
    const std::string* effect_name = entVals.Get(entvalHash);
    if ( effect_name )
        (this->*effectSetter)(effect_name->c_str());
}

void Projectile::Spawn( void ) {
    Entity::Spawn();
       
    if ( fx_attached ) {
        std::weak_ptr< Effect > fx_attached_wptr = effectsManager.Spawn( *fx_attached, GetOrigin() );
        std::shared_ptr< Effect > fx_attached_sptr = fx_attached_wptr.lock();
        if ( fx_attached_sptr )
            fx_attached_sptr->SetOwner( GetWeakPtr() );
    }
    
    WakeUp();
}


bool Projectile::IsSameTeamAsOwner( const Entity* collider ) const {
    // ** enemy projectils don't hurt enemies, and player projectiles don't hurt players

    // can't have a team if it has no owner (this has the effect that rockets who's owners have expired will hurt everything)
    auto sptr = owner.lock();
    if ( !sptr )
        return false;

    if ( collider == nullptr )
        return false;
        
    // if collider is the owner
    if ( sptr.get() == collider )
        return true;

    // if our owner isn't an actor, we have no team
    if ( !derives_from< Actor >( *sptr ) )
        return false;

    const Actor* actor_owner = static_cast< const Actor* >( sptr.get() );
    ASSERT( actor_owner );

    // projectiles shouldn't collide with one another
    if ( derives_from< Projectile >( *collider ) )
        return true;

    // thing we're colliding with isn't an Actor, so it doesn't have a team //damageteams
    if ( !derives_from< Actor >( *collider ) )
        return false;

    const Actor* actor_collider = static_cast< const Actor* >( collider );
    ASSERT( actor_collider );
    return actor_owner->GetTeamName() == actor_collider->GetTeamName();
}

void Projectile::BumpInto( Entity& ent ) {
    Hit( ent );
}

void Projectile::BumpedIntoBy( Entity& ent ) {
    Hit( ent );
}

void Projectile::Animate( void ) {

}

void Projectile::Hit( Entity& ent ) {
    if ( IsSameTeamAsOwner( &ent ) ) //todo:damageteams
        return;
        
    if ( game->IsPlayer( ent ) ) //todo:damageteams
        return;
        
    if constexpr ( damageDebug ) {
        CMSG_LOG("%u: Projectile: Hit %s\n", game->GetGameTime(), ent.GetIdentifier().c_str() );
    }
    
    if ( damage_hit ) {
        damage_hit->Inflict( ent, owner );
    }
    
    if ( BitwiseAnd( opts, ProjectileOptsT::ExplodeOnImpact ) ) {
        Explode();
        return;
    }
    
    if ( BitwiseAnd( opts, ProjectileOptsT::IntersectDamage ) ) {
        AddIntersector( ent );
    }
}

void Projectile::AddIntersector( Entity& ent ) {
    for ( auto& intersector : intersectingEntities ) {
        auto intersector_ent = intersector.data.ent.lock();
        if ( !intersector_ent )
            continue;
            
        if ( intersector_ent.get() == &ent ) {
            return; // already in there
        }
    }
    
    auto entity = ent.GetWeakPtr();
    intersectingEntities.emplace_back( DamageEvent( damage_hit, entity, owner ) );
    auto & event = intersectingEntities[intersectingEntities.size()-1];
    event.SetDelay( intersect_delay );
}

void Projectile::Explode( void ) {
    if ( status == ProjectileStatusT::EXPLODED )
        return;

    if ( fx_explode )
        effectsManager.Spawn( *fx_explode, GetOrigin() );

    if ( damage_explode ) {
        DamageZone( Primitive_Sphere( Sphere( GetOrigin(), 1 ) ), 0, *damage_explode, owner, DamageFalloffT::Linear );
    }

    status = ProjectileStatusT::EXPLODED;
    MarkForDeletion();

    if ( snd_explode.size() > 0 )
        soundManager.PlayAt( GetOrigin(), snd_explode.c_str() );

    return;
}

void Projectile::SetDir( const Vec3f& newDir ) {
    dir = newDir;
    SetFacingDir( dir );
}

bool Projectile::ReactsToPhysicsAgainst( const Entity& ent ) const {
    if ( IsSameTeamAsOwner( &ent ) )
        return true;

    return Entity::ReactsToPhysicsAgainst( ent );
}

void Projectile::SetLaunchEffect( const std::shared_ptr< const Effect >& fx ) {
    fx_launch = fx;
}

void Projectile::SetAttachedEffect( const std::shared_ptr< const Effect >& fx ) {
    fx_attached = fx;
}

void Projectile::SetTrailEffect( const std::shared_ptr< const Effect >& fx ) {
    fx_trail = fx;
}

uint Projectile::GetTrailDelay( void ) const {
    return trailDelay;
}

void Projectile::SetTrailDelay( const uint to ) {
    trailDelay = to;
}

void Projectile::SetExplodeEffect( const std::shared_ptr< const Effect >& fx ) {
    fx_explode = fx;
}

void Projectile::SetExplodeOnImpact( const bool whether ) {
    if ( whether ) {
        opts = BitwiseOr( opts, ProjectileOptsT::ExplodeOnImpact );
    } else {
        opts = BitwiseRemove( opts, ProjectileOptsT::ExplodeOnImpact );
    }
}

void Projectile::SetExplodeOnExpire( const bool whether ) {
    if ( whether ) {
        opts = BitwiseOr( opts, ProjectileOptsT::ExplodeOnExpire );
    } else {
        opts = BitwiseRemove( opts, ProjectileOptsT::ExplodeOnExpire );
    }
}

void Projectile::SetDamageIntersectors( const bool whether ) {
    if ( whether ) {
        opts = BitwiseOr( opts, ProjectileOptsT::IntersectDamage );
    } else {
        opts = BitwiseRemove( opts, ProjectileOptsT::IntersectDamage );
    }
}

void Projectile::SetSpeed( const float to ) {
    speed = to;
}

float Projectile::GetSpeed( void ) const {
    return speed;
}

void Projectile::SetSpeedMod( const float to ) {
    speedMod = to;
}

float Projectile::GetSpeedMod( void ) const {
    return speedMod;
}

const Vec3f Projectile::GetDir( void ) const {
    return dir;
}

void Projectile::SetOwner( const std::weak_ptr< Entity >& _owner ) {
    owner = _owner;
}

void Projectile::SetTarget( const std::weak_ptr< Entity >& _target ) {
    target = _target;
}

void Projectile::SetExplodeDamage( const std::string& dmg_name ) {
    SetExplodeDamage( damageManager.Get( dmg_name.c_str() ) );
}

void Projectile::SetExplodeDamage( const std::shared_ptr< const DamageInfo >& to ) {
    damage_explode = to;
}

void Projectile::SetHitDamage( const std::string& dmg_name ) {
    SetHitDamage( damageManager.Get( dmg_name.c_str() ) );
}

void Projectile::SetHitDamage( const std::shared_ptr< const DamageInfo >& to ) {
    damage_hit = to;
}

std::shared_ptr< const DamageInfo > Projectile::GetExplodeDamage( void ) const {
    return damage_explode;
}

std::shared_ptr< const DamageInfo > Projectile::GetHitDamage( void ) const {
    return damage_hit;
}

const std::weak_ptr< Entity > Projectile::GetOwner( void ) const {
    return owner;
}

void Projectile::SetDuration( const Uint _duration ) {
    duration = _duration;
}

uint Projectile::GetDuration( void ) const {
    return duration;
}

void Projectile::SetIntersectDelay( const Uint32 delay ) {
    intersect_delay = delay;
}

void Projectile::SetLaunchSound( const std::string& snd_name ) {
    snd_launch = snd_name;
}

const std::string Projectile::SetLaunchSound( void ) const {
    return snd_launch;
}

void Projectile::SetExplodeSound( const std::string& snd_name ) {
    snd_explode = snd_name;
}

const std::string Projectile::SetExplodeSound( void ) const {
    return snd_explode;
}

void Projectile::SetBornTime( const uint to ) {
    bornTime = to;
}

uint Projectile::GetBornTime( void ) const {
    return bornTime;
}

void Projectile::SetLaunchEffect( const std::string& _Name ) {
    SetLaunchEffect( effectsManager.Get( _Name.c_str() ) );
}

void Projectile::SetTrailEffect( const std::string& _Name ) {
    SetTrailEffect( effectsManager.Get( _Name.c_str() ) );
}

void Projectile::SetAttachedEffect( const std::string& _Name ) {
    SetAttachedEffect( effectsManager.Get( _Name.c_str() ) );
}

void Projectile::SetExplodeEffect( const std::string& _Name ) {
    SetExplodeEffect( effectsManager.Get( _Name.c_str() ) );
}

std::string Projectile::GetLaunchEffectName( void ) const {
    return fx_launch ? fx_launch->GetName() : "";
}

std::string Projectile::GetAttachedEffectName( void ) const {
    return fx_attached ? fx_attached->GetName() : "";
}

std::string Projectile::GetTrailEffectName( void ) const {
    return fx_trail ? fx_trail->GetName() : "";
}

std::string Projectile::GetExplodeEffectName( void ) const {
    return fx_explode ? fx_explode->GetName() : "";
}

std::string Projectile::GetHitDamageName( void ) const {
    return damage_hit ? damage_hit->GetName() : "";
}

std::string Projectile::GetExplodeDamageName( void ) const {
    return damage_explode ? damage_explode->GetName() : "";
}

std::string Projectile::GetLaunchSoundName( void ) const {
    return snd_launch;
}

std::string Projectile::GetExplodeSoundName( void ) const {
    return snd_explode;
}

bool Projectile::GetExplodeOnImpact( void ) const {
    return BitwiseAnd( opts, ProjectileOptsT::ExplodeOnImpact );
}

bool Projectile::GetExplodeOnExpire( void ) const {
    return BitwiseAnd( opts, ProjectileOptsT::ExplodeOnExpire );
}

bool Projectile::GetDamageIntersectors( void ) const {
    return BitwiseAnd( opts, ProjectileOptsT::IntersectDamage );
}

Uint32 Projectile::GetIntersectDelay( void ) const {
    return intersect_delay;
}
