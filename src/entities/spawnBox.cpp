// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./spawnBox.h"
#include "./entityInfoManager.h"
#include "../game.h"

bool SpawnBox::SpawnEntity( const std::string& entity_name ) const {
    const std::shared_ptr< const EntityInfo > ent_info = entityInfoManager.Get( entity_name.c_str() );
    if ( ! ent_info ) {
        ERR("SpawnEntity: Couldn't find entity to spawn: %s\n", entity_name.c_str());
        return false;
    }

    const Vec3f my_origin( GetOrigin() );

    bool ret = false;
    auto radius_str = ent_info->GetMemberValue("radius");
    if ( radius_str.size() > 0 ) {
        const float sphere_radius = String::ToFloat(radius_str);
        const Vec3f sphere_origin(my_origin.x, my_origin.y, my_origin.z + sphere_radius);
        const Sphere ent_sphere( sphere_origin, sphere_radius );
        ret = IsAreaAvailable( Primitive_Sphere( ent_sphere ) );
    } else if ( bounds.IsAABox3D() ) {
        Box3D bbox = bounds.GetOrientedBox3D();
        ret = IsAreaAvailable( Primitive_Box3D( Box3D( bbox ) ) );
    } else if ( bounds.IsSphere() ) {
        ret = IsAreaAvailable( Primitive_Sphere( Sphere( GetOrigin(), bounds.GetRadius() ) ) );
    }

    if ( ret ) {
        entityInfoManager.LoadAndSpawn( *ent_info, my_origin );
    }

    return ret;
}

bool SpawnBox::IsAreaAvailable( const Primitive& primitive ) const {
    
    const IntersectData coldata;

    std::vector< CollisionItem > ents;
    game->entTree->GetEntityTraceCollisions( coldata, primitive, &ents );

    if ( ents.size() < 1 )
        return true;

    for ( const auto & item : ents )
        if ( auto ent = item.ent.lock() )
            CMSG("collision with: %s\n", ent->GetIdentifier().c_str());

    for ( const auto & item : ents )
        if ( auto ent = item.ent.lock() )
            if ( ent.get() != this )
                return false;

    return true;
}
