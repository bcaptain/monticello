// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_GARBAGEBIN_H
#define SRC_ENTITIES_GARBAGEBIN_H

#include "../base/main.h"
#include "./actor.h"
#include "../sprites/meter.h"


class GarbageBin : public Entity {
public:
    static constexpr TypeInfo<GarbageBin, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    GarbageBin( void );
    ~GarbageBin( void ) override;
    GarbageBin( const GarbageBin& other );

public:
    GarbageBin& operator=( const GarbageBin& other );
    
public:
    void Think( void ) override;
    void Animate( void ) override;
    void SetMarkedForUse( const bool whether ) override;
    bool UseBy( Actor* user = nullptr ) override;
    bool IsUsable( void ) const override;
    
private:
    void UpdateInspectUIValue( void );
    void UpdateUseValue( void );
    void SpawnInspectUI( void );
    
private:
    std::shared_ptr< Meter > useMeter;
    
// ** ToolTips
public:
    static_assert( is_parent_of<GarbageBin>( get_typeid<Entity>() ) );
    static ToolTip< GarbageBin, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_GARBAGEBIN_H
