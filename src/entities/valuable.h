// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_VALUABLE_H_
#define SRC_ENTITIES_VALUABLE_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"


class Valuable : public Entity {
public:
    static constexpr TypeInfo<Valuable, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Valuable( void );
    Valuable( const Valuable& other );
    ~Valuable( void ) override;

public:
    Valuable& operator=( const Valuable& other );

public:
    void Spawn( void ) override;
    void BumpInto( Entity& ent ) override;
    
    void Think( void ) override;
    
    void SetObjectValue( const int to );
    int GetObjectValue( void ) const;
    
    void SetBreakEffect( const std::string& _Name );
    void SetBreakEffect( const std::shared_ptr< const Effect >& fx );
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
protected:
    bool ReactsToPhysicsAgainst( const Entity& ent ) const override;
    
private:
    void BreakValuable( void );
    
private:
    std::shared_ptr< const Effect > breakEffect;
    std::weak_ptr< const Effect > activeEffect;
    int objectValue;
    bool intact;

// ** Named Setters
private:
    static const uint Valuable_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum ValuableSetterT : NamedSetterT_BaseType {
        Valuable_SetObjectValue = Valuable_SETTERS_START
        , Valuable_SETTERS_END
        , Valuable_NUM_SETTERS = Valuable_SETTERS_END - Valuable_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > valuableSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Valuable>( get_typeid<Entity>() ) );
    static ToolTip< Valuable, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_VALUABLE_H_
