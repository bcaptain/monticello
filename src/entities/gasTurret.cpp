// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./gasTurret.h"
#include "./entityInfoManager.h"
#include "../game.h"
#include "../rendering/animation/animInfo.h"

std::vector< std::string > GasTurret::gasTurretSetters;

void GasTurret::InitSetterNames( void ) {
    if ( gasTurretSetters.size() == GasTurret_NUM_SETTERS )
        return; // already set
    gasTurretSetters.resize(GasTurret_NUM_SETTERS);
    
    const uint i = GasTurret_SETTERS_START;
    gasTurretSetters[GasTurret_SetGasLaunchPeriod-i] = "gasLaunchPeriod";
    gasTurretSetters[GasTurret_SetTimeNextGasLaunch-i] = "timeNextGasLaunch";
    gasTurretSetters[GasTurret_SetNumProjectilesPerLaunch-i] = "numProjectilesPerLaunch";
    gasTurretSetters[GasTurret_SetGasLaunchRotZPerSecond-i] = "gasLaunchRotZPerSecond";
    gasTurretSetters[GasTurret_SetDuration-i] = "duration";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<GasTurret,Entity>(gasTurretSetters);
}

bool GasTurret::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<GasTurretSetterT>(setter) ) {
        case GasTurret_SetGasLaunchPeriod: SetGasLaunchPeriod( String::ToUInt( value ) ); return true;
        case GasTurret_SetTimeNextGasLaunch: SetTimeNextGasLaunch(  String::ToUInt( value ) ); return true;
        case GasTurret_SetNumProjectilesPerLaunch: SetNumProjectilesPerLaunch(  String::ToUInt( value ) ); return true;
        case GasTurret_SetGasLaunchRotZPerSecond: SetGasLaunchRotZPerSecond(  String::ToUInt( value ) ); return true;
        case GasTurret_SetDuration: SetDuration(  String::ToUInt( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case GasTurret_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case GasTurret_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<GasTurret>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}

std::string GasTurret::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<GasTurretSetterT>(setter) ) {
        case GasTurret_SetGasLaunchPeriod: return String::ToString(GetGasLaunchPeriod());
        case GasTurret_SetTimeNextGasLaunch: return String::ToString(GetTimeNextGasLaunch());
        case GasTurret_SetNumProjectilesPerLaunch: return String::ToString(GetNumProjectilesPerLaunch());
        case GasTurret_SetGasLaunchRotZPerSecond: return String::ToString(GetGasLaunchRotZPerSecond());
        case GasTurret_SetDuration: return String::ToString(GetDuration());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case GasTurret_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case GasTurret_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<GasTurret>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT GasTurret::GetSetterIndex( const std::string& key ) {
    NamedSetterT ret = GetSetterIndexIn( gasTurretSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + GasTurret_SETTERS_START;

    static_assert( is_parent_of<GasTurret>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

GasTurret::GasTurret( void )
    : Entity()
    , gasLaunchPeriod()
    , timeNextGasLaunch()
    , numProjectilesPerLaunch()
    , gasLaunchRotZPerSecond()
    , duration()
    , dir()
    {
}

GasTurret::GasTurret( const GasTurret& other ) 
    : Entity( other )
    , gasLaunchPeriod(other.gasLaunchPeriod)
    , timeNextGasLaunch(other.timeNextGasLaunch)
    , numProjectilesPerLaunch(other.numProjectilesPerLaunch)
    , gasLaunchRotZPerSecond(other.gasLaunchRotZPerSecond)
    , duration(other.duration)
    , dir(other.dir)
    {
}

GasTurret& GasTurret::operator=( const GasTurret& other ) {
    Entity::operator=( other );
    
    gasLaunchPeriod = other.gasLaunchPeriod;
    timeNextGasLaunch = other.timeNextGasLaunch;
    numProjectilesPerLaunch = other.numProjectilesPerLaunch;
    gasLaunchRotZPerSecond = other.gasLaunchRotZPerSecond;
    duration = other.duration;
    dir = other.dir;
    
    return *this;
}

GasTurret::~GasTurret( void ) {
}

void GasTurret::Spawn( void ) {
    Entity::Spawn();

/* todo:jesse:entsets:fixit
    const std::string *str = entVals.Get(entval_duration);
    if ( str ) SetDuration( String::ToUInt( *str ));
        
    timeNextGasLaunch = game->GetGameTime() + gasLaunchPeriod;
*/
}

void GasTurret::Think( void ) {
    
    if ( game->GetGameTime() - GetSpawnTime() > duration ) {
        SetNoPhys( true );
        SetVisible( false );
        MarkForDeletion();
    }
    
    if ( timeNextGasLaunch <= game->GetGameTime() ){
        LaunchGas();
        timeNextGasLaunch = game->GetGameTime() + gasLaunchPeriod;
    }
    
    Entity::Think();
}

void GasTurret::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

void GasTurret::LaunchGas( void ) {
        
    float rot_change = 360.0f / numProjectilesPerLaunch;
    float start_rot = GetGasLaunchRotation();
    
    Vec3f tmp;
    Mat3f rot;
    
    for ( uint i = 0; i < numProjectilesPerLaunch; i++ ) {
        
        auto ent = entityInfoManager.LoadAndSpawn( "sprayerGas" );
        std::shared_ptr< Projectile > proj = std::static_pointer_cast< Projectile >( ent );
        
        if ( proj ) {
            proj->SetOwner( GetWeakPtr() );
            proj->SetOrigin( GetOrigin() );
            
            Vec3f attack_dir = GetFacingDir();
            
            rot.LoadIdentity();
            rot.Rotate( start_rot + ( rot_change * i ), 0,0,1 );
            
            attack_dir *= rot;
        
            proj->SetDir( attack_dir );
            proj->Launch();
        }
    }
}

float GasTurret::GetGasLaunchRotation( void ) const {
    
    const float timeAlive_seconds = ( game->GetGameTime() - GetSpawnTime() ) / 1000.0f;
    const float degree_rotation_total = timeAlive_seconds * gasLaunchRotZPerSecond;
    const float degree_rotation_normalized = fmodf( degree_rotation_total, 360 );
    
    return degree_rotation_normalized;
}

void GasTurret::SetDuration( const Uint _duration ) {
    duration = _duration;
}

uint GasTurret::GetDuration( void ) const {
    return duration;
}

void GasTurret::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<GasTurret>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, gasTurretSetters );
}

void GasTurret::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<GasTurret>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, gasTurretSetters );
}
