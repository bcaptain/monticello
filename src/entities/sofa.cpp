// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "./sofa.h"
#include "./entityInfoManager.h"
#include "../sprites/sprite.h"
#include "../rendering/animation/animInfo.h"

const int DEFAULT_BUG_INFEST_VALUE = 1;
const auto ANIMSTATE_SOFA_INFESTED = AnimStateT::Special1;
const Uint32 IN_USE_TIMER_THRESHOLD = 250;
const float USE_VALUE_PER_FRAME = 0.025f;
const float USE_LIMIT = 1.0f;
const float DEFAULT_SOFA_UI_EDGE_LENGTH = 1.25f;
const float MINIMUM_DISPLAYED_USE_VALUE = 0.01f;
const std::string SOFA_DEFAULT_BUGTYPE = "forager";

std::vector< std::string > Sofa::sofaSetters;

void Sofa::InitSetterNames( void ) {
    if ( sofaSetters.size() == Sofa_NUM_SETTERS )
        return; // already set
    sofaSetters.resize(Sofa_NUM_SETTERS);
    
    const uint i = Sofa_SETTERS_START;
    sofaSetters[Sofa_SetSpawnRandNumBugs-i] = "spawnRandNumBugs";
    sofaSetters[Sofa_SetBugType-i] = "bugType";
    sofaSetters[Sofa_SetBugSpawnOffset-i] = "bugSpawnOffset";
    sofaSetters[Sofa_SetMaxBugsInSofa-i] = "maxBugsInSofa";
    sofaSetters[Sofa_SetNumBugsInSofa-i] = "numBugsInSofa";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Sofa,Entity>(sofaSetters);
}

bool Sofa::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<SofaSetterT>(setter) ) {
        case Sofa_SetSpawnRandNumBugs: SetSpawnRandNumBugs( String::ToBool(value) ); return true;
        case Sofa_SetBugType: SetBugType( value ); return true;
        case Sofa_SetBugSpawnOffset: SetBugSpawnOffset( String::ToVec3f( value ) ); return true;
        case Sofa_SetMaxBugsInSofa: SetMaxBugsInSofa( String::ToUInt( value ) ); return true;
        case Sofa_SetNumBugsInSofa: SetNumBugsInSofa( String::ToUInt( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Sofa_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Sofa_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Sofa>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Sofa::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<SofaSetterT>(setter) ) {
        case Sofa_SetSpawnRandNumBugs: return String::ToString(GetSpawnRandNumBugs());
        case Sofa_SetBugType: return GetBugType();
        case Sofa_SetBugSpawnOffset: return String::ToString(GetBugSpawnOffset());
        case Sofa_SetMaxBugsInSofa: return String::ToString(GetMaxBugsInSofa());
        case Sofa_SetNumBugsInSofa: return String::ToString(GetNumBugsInSofa());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Sofa_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Sofa_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Sofa>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Sofa::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( sofaSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Sofa_SETTERS_START;
    
    static_assert( is_parent_of<Sofa>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Sofa::Sofa( void )
    : Entity()
    , bugSpawnOffset()
    , bugType(SOFA_DEFAULT_BUGTYPE)
    , maxBugsInSofa()
    , numBugsInSofa()
    , spawnRandNumBugs()
    , useMeter()
    {
}

Sofa::Sofa( const Sofa& other ) 
    : Entity( other )
    , bugSpawnOffset(other.bugSpawnOffset)
    , bugType(other.bugType)
    , maxBugsInSofa(other.maxBugsInSofa)
    , numBugsInSofa(other.numBugsInSofa)
    , spawnRandNumBugs(other.spawnRandNumBugs)
    , useMeter()
    {
}

Sofa::~Sofa( void ) {
}

void Sofa::Spawn( void ){
    numBugsInSofa = Random::Int( 2, maxBugsInSofa );

    SetInfestValue( numBugsInSofa * DEFAULT_BUG_INFEST_VALUE );
    
    Entity::Spawn();
}

void Sofa::SetMarkedForUse( const bool whether ) {
    if ( whether ) {
        if ( !IsUsable() )
            return;
        SpawnInspectUI();
    }
        
    Entity::SetMarkedForUse( whether );
}

bool Sofa::UseBy( [[maybe_unused]] Actor* user_unused ) {
    if ( !IsUsable() )
        return false;
    
    useValue += USE_VALUE_PER_FRAME;
    lastTimeUsed = game->GetGameTime();
    
    if ( useValue > USE_LIMIT ) {
        SpawnBugs();
        useValue = 0;
        UpdateInspectUIValue();
        if ( !IsUsable() )
            useMeter.reset();
    }

    return true;
}

void Sofa::SpawnInspectUI( void ) {
    const auto & mdl = GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;
    
    if ( !useMeter ) {
        useMeter = std::make_shared< Meter >(DEFAULT_SOFA_UI_EDGE_LENGTH,DEFAULT_SOFA_UI_EDGE_LENGTH);
        useMeter->SetMaterial( "inspectUI" );
        useMeter->SetMaskMaterial( "radialMaskUI" );
        useMeter->SetupUV();
    }
    
    Vec3f useMeterOrigin = GetOrigin();
    useMeterOrigin.x -= useMeter->GetWidth() / 2;
    useMeterOrigin.z = height + useMeter->GetHeight();

    useMeter->SetOrigin( useMeterOrigin );
    
    auto sprite = std::static_pointer_cast< Sprite >(useMeter);
    game->AddWorldUI( sprite );
}

void Sofa::UpdateUseValue( void ) {
    if ( !IsUsable() )
        return;
        
    if ( useValue > 0 ) {
        const Uint32 gameTime = game->GetGameTime();
    
        if ( gameTime - lastTimeUsed > IN_USE_TIMER_THRESHOLD ) {
            useValue -= USE_VALUE_PER_FRAME;
            useValue = std::max( useValue, 0.0f );
        }
    }
    
    UpdateInspectUIValue();
}

void Sofa::UpdateInspectUIValue( void ) {
    if ( !useMeter )
        return;
        
    if ( !IsMarkedForUse() && useValue < MINIMUM_DISPLAYED_USE_VALUE ) {
        useMeter.reset();
        return;
    }
    
    useMeter->progress = useValue;
}

void Sofa::Think( void ) {
    if ( modelInfo.HasAnimState( AnimStateT::Die ) ) {
        Death();
        return;
    }
        
    if ( IsUsable() ) {
        modelInfo.AddAnimState( ANIMSTATE_SOFA_INFESTED );
        modelInfo.RemAnimState( AnimStateT::Idle );
    } else {
        modelInfo.AddAnimState( AnimStateT::Idle );
        modelInfo.RemAnimState( ANIMSTATE_SOFA_INFESTED );
    }
    
    UpdateUseValue();
    
    Entity::Think();
}

void Sofa::Animate( void ) {
    
    //TEST
        return;
    //END TEST
    
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        {
          "infested", ANIMSTATE_SOFA_INFESTED, AnimOptT::None,
            {
                { AnimChannelT::Legs, 8000 }
            }
        }
        , { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
        , { "death", AnimStateT::Die, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

void Sofa::SpawnBugs( void ) {    
    if ( numBugsInSofa < 1 )
        return;
    
    int numBugsToSpawn = 1;
    
    if ( spawnRandNumBugs )
        numBugsToSpawn = Random::Int( 1, numBugsInSofa );
    
    numBugsInSofa -= numBugsToSpawn;
    
    Vec3f spawnLocation = GetOrigin() + bugSpawnOffset;
    
    while ( numBugsToSpawn ) {
        auto ent = entityInfoManager.LoadAndSpawn( bugType.c_str() , spawnLocation );
        if ( ent ) {
            game->DecreaseInfestLevel( DEFAULT_BUG_INFEST_VALUE );
            numBugsToSpawn -= 1;
        }
    }
    
}

bool Sofa::IsUsable( void ) const {
    return numBugsInSofa > 0;
}

void Sofa::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Sofa>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, sofaSetters );
}

void Sofa::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Sofa>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, sofaSetters );
}
