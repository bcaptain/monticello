// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_SOFA_H
#define SRC_ENTITIES_SOFA_H

#include "../base/main.h"
#include "./actor.h"
#include "../sprites/meter.h"


class Sofa : public Entity {
public:
    static constexpr TypeInfo<Sofa, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Sofa( void );
    ~Sofa( void ) override;
    Sofa( const Sofa& other );

public:
    Sofa& operator=( const Sofa& other );

public:
    void Think( void ) override;
    void Animate( void ) override;
    void Spawn( void ) override;
    void SetMarkedForUse( const bool whether ) override;
    bool UseBy( Actor* user_unused = nullptr ) override;
    bool IsUsable( void ) const override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void SpawnInspectUI( void );
    void UpdateUseValue( void );
    void UpdateInspectUIValue();
    void SpawnBugs( void );
    
private:
    SETTER_GETTER_BYVAL( BugSpawnOffset, Vec3f, bugSpawnOffset )
    SETTER_GETTER( BugType, std::string, bugType )
    SETTER_GETTER_BYVAL( MaxBugsInSofa, uint, maxBugsInSofa )
    SETTER_GETTER_BYVAL( NumBugsInSofa, uint, numBugsInSofa )
    SETTER_GETTER_BYVAL( SpawnRandNumBugs, bool, spawnRandNumBugs )
    std::shared_ptr< Meter > useMeter;

// ** Named Setters
private:
    static const uint Sofa_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum SofaSetterT : NamedSetterT_BaseType {
        Sofa_SetSpawnRandNumBugs = Sofa_SETTERS_START
        , Sofa_SetBugType
        , Sofa_SetBugSpawnOffset
        , Sofa_SetMaxBugsInSofa
        , Sofa_SetNumBugsInSofa
        , Sofa_SETTERS_END
        , Sofa_NUM_SETTERS = Sofa_SETTERS_END - Sofa_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > sofaSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Sofa>( get_typeid<Entity>() ) );
    static ToolTip< Sofa, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_SOFA_H
