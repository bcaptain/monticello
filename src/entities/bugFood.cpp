// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./bugFood.h"
#include "./characters/basicBug.h"

std::vector< std::string > BugFood::bugFoodSetters;

void BugFood::InitSetterNames( void ) {
    if ( bugFoodSetters.size() == BugFood_NUM_SETTERS )
        return; // already set
    bugFoodSetters.resize(BugFood_NUM_SETTERS);
    
    const uint i = BugFood_SETTERS_START;
    bugFoodSetters[BugFood_SetFoodStock-i] = "foodStock";
    bugFoodSetters[BugFood_SetDefaultUserType-i] = "defaultUserType";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<BugFood,Entity>(bugFoodSetters);
}

bool BugFood::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<BugFoodSetterT>(setter) ) {
        case BugFood_SetFoodStock: SetFoodStock( String::ToUInt(value) ); return true;
        case BugFood_SetDefaultUserType: SetDefaultUserType( value ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case BugFood_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case BugFood_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<BugFood>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string BugFood::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<BugFoodSetterT>(setter) ) {
        case BugFood_SetFoodStock: return String::ToString(GetFoodStock());
        case BugFood_SetDefaultUserType: return GetDefaultUserType();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case BugFood_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case BugFood_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<BugFood>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT BugFood::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( bugFoodSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + BugFood_SETTERS_START;
        
    static_assert( is_parent_of<BugFood>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

BugFood::BugFood( void )
    : Entity()
    , defaultUserType()
    , foodStock()
    , userType()
    {
}

BugFood::BugFood( const BugFood& other ) 
    : Entity( other )
    , defaultUserType( other.defaultUserType )
    , foodStock( other.foodStock )
    , userType( other.userType )
    {
}

BugFood& BugFood::operator=( const BugFood& other ) {
    Entity::operator=( other );
    defaultUserType = other.defaultUserType;
    foodStock = other.foodStock;
    userType = other.userType;
    return *this;
}

BugFood::~BugFood( void ) {
}

void BugFood::Spawn( void ) {
    Entity::Spawn();
    SetFoodForTypeID( get_typeid( defaultUserType ) );
}

bool BugFood::UseBy( Actor* user ) {
    if( !user )
        return false;
        
    if ( foodStock < 1 )
        return false;
    
    if ( derives_from( userType, *user ) ) {
        BasicBug* bug = static_cast< BasicBug* >( user );
        bug->SetHasFood( true ); //todojesseno
        foodStock -= 1;
    }
    
    return true;
}

void BugFood::SetFoodForTypeID( const TypeID user_type ) {
    userType = user_type;
}

TypeID BugFood::GetUserType( void ) const {
    return userType;
}

bool BugFood::IsFoodForEnt( const Entity& ent ) const {
    return userType == ent.GetObjectTypeID();
}

bool BugFood::IsUsable( void ) const {
    return foodStock > 0;
}

void BugFood::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<BugFood>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, bugFoodSetters );
}

void BugFood::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<BugFood>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, bugFoodSetters );
}
