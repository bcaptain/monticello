// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entity.h"
#include "./actor.h"
#include "../game.h"
#include "../rendering/animation/animInfo.h"
#include "../rendering/animation/animCue.h"
#include "../rendering/renderer.h"
#include "../math/collision/collision.h"
#include "./projectile.h"
#include "../damage/damageManager.h"
#include "../rendering/models/modelManager.h"
#include "./entityInfoManager.h"
#include "../particles/effectsManager.h"
#include "../ballistic.h"
#include "../sound.h"

extern const uint ANIMCUE_EFFECT_NAME_INDEX;

bool Entity::HasAnimatedModel( void ) const {
    return modelInfo.IsAnimated();
}

void Entity::ProcessAnimCue_RenderEffect( const AnimCue& cue, [[maybe_unused]] const AnimChannelT channel ) {
    if ( cue.arguments.size() < 3 ) {
        ERR("in cue file for entity %s, renderEffect does not specify: what effect, duration, and intensity.\n", GetIdentifier().c_str());
    } else {
        renderer->ActivateEffect( cue.arguments[0], String::ToUInt( cue.arguments[1] ), String::ToFloat( cue.arguments[2] ) );
    }
}
void Entity::ProcessAnimCue_SpawnEffect( const AnimCue& cue, const AnimChannelT channel ) {
    bool onBone = false;
    
    if ( cue.arguments.size() > 1 ) {
        onBone = true;
        if ( cue.arguments[1] != "onBone" ) {
            ERR("in cue file for entity %s, spawnEffect invalid format. Expected an effect name, then \"onBone\", and then a bone name.\n", GetIdentifier().c_str());
            return;
        }
    }

    const char* effectName = cue.arguments[ANIMCUE_EFFECT_NAME_INDEX].c_str();

    Vec3f fx_origin;
    
    if ( onBone ) {
        if ( cue.arguments.size() < 4 ) {
            ERR("in cue file for entity %s, spawnEffect invalid format. Expected an effect name, then \"onBone\", and then a bone name.\n", GetIdentifier().c_str());
            return;
        } else {
            const char* boneName = cue.arguments[2].c_str();
            fx_origin = GetBonePos( boneName, channel, cue.arguments.size() >= 4 ? cue.arguments[3].c_str() : "center" );
            std::weak_ptr< Effect > fx = effectsManager.Spawn( effectName, fx_origin );
            BindEffectToBone( fx, boneName, channel );
            return;
        }

        fx_origin = GetOrigin();
        effectsManager.Spawn( effectName, fx_origin );
    }
    
}

void Entity::ProcessAnimCue_LaunchProjectile( const AnimCue& cue, const AnimChannelT channel ) {
    if ( cue.arguments.size() < 1 ) {
        ERR("in cue file for entity %s, launchProjectile does not specify what projectile.\n", GetIdentifier().c_str());
    } else {
        LaunchProjectile( cue, channel );
    }
}

void Entity::ProcessAnimCue_FireBullet( const AnimCue& cue, [[maybe_unused]] const AnimChannelT channel ) {
    if ( cue.arguments.size() < 2 || ! String::IsUInt( cue.arguments[1] ) ) {
        ERR("in cue file for entity %s, fireBullet does not specify what damageInfo and then range as an unsigned int.\n", GetIdentifier().c_str());
    } else {
        const std::string& damageInfo = cue.arguments[0];
        const uint range = String::ToUInt( cue.arguments[1] );

        if ( game->IsPlayer( this ) ) {
            Ballistic::PlayerFire( damageInfo, range, GetWeakPtr() );
        } else {
            const Vec3f bullet_origin = GetOrigin() + Vec3f(0,0,-1);
            const Vec3f bullet_dir = GetFacingDir();
            Ballistic::Fire( bullet_origin, bullet_dir, damageInfo, range, GetWeakPtr() );
        }
    }
}

void Entity::ProcessAnimCue_EnableTouchDamage( const AnimCue& cue, const AnimChannelT channel ) {
    if ( cue.arguments.size() < 1 ) {
        ERR("in cue file for entity %s, EnableTouchDamage does not specify what damageInfo.\n", GetIdentifier().c_str());
    } else {
        SetAnimCueEntityEffect_SetTouchDamage( channel, cue.arguments[0].c_str() );
    }
}

void Entity::ProcessAnimCue_SetMoveSpeedBoost( const AnimCue& cue, const AnimChannelT channel ) {
    if ( cue.arguments.size() < 1 ) {
        ERR("in cue file for entity %s, setMoveSpeedBoost must specify an integer speed (in units per second).\n", GetIdentifier().c_str());
    } else {
        SetAnimCueEntityEffect_SetMoveSpeedBost( channel, String::ToFloat( cue.arguments[0] ) );
    }
}

void Entity::SetAnimCueEntityEffect_TurnAnimationOverride( const AnimChannelT channel, const bool whether ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
            
    if ( whether ) {
        SetTurnAnimOverride(true);
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_DISABLE_TURN_ANIM_OVERRIDE );
    } else {
        SetTurnAnimOverride(false);
    }
}

void Entity::ProcessAnimCue_Die( void ) {
    SetOkayToDie(true);
}

void Entity::ProcessAnimCue( const AnimCue& cue, const AnimChannelT channel ) {
    for ( const auto& arg : cue.arguments ) {
        if ( arg == "ifPlayer" && !IsPlayer() )
            return;

        if ( arg == "ifNotPlayer" && IsPlayer() )
            return;
    }

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wswitch-enum" // this is okay
    
    switch ( cue.event ) {
        case AnimEventT::RENDER_EFFECT: ProcessAnimCue_RenderEffect( cue, channel ); return;
        case AnimEventT::LAUNCH_PROJECTILE: ProcessAnimCue_LaunchProjectile( cue, channel ); return;
        case AnimEventT::FIRE_BULLET: ProcessAnimCue_FireBullet( cue, channel ); return;
        case AnimEventT::SPAWN_EFFECT: ProcessAnimCue_SpawnEffect( cue, channel ); return;
        case AnimEventT::DISABLE_TURNING: SetAnimCueEntityEffect_StopTurning( channel, true ); return;
        case AnimEventT::ENABLE_TURNING: SetAnimCueEntityEffect_StopTurning( channel, false ); return;
        case AnimEventT::DISABLE_MOVING: SetAnimCueEntityEffect_StopMoving( channel, true ); return;
        case AnimEventT::ENABLE_MOVING: SetAnimCueEntityEffect_StopMoving( channel, false ); return;
        case AnimEventT::ENABLE_TOUCH_DAMAGE: ProcessAnimCue_EnableTouchDamage( cue, channel ); return;
        case AnimEventT::CHARGE_FORWARD: SetAnimCueEntityEffect_ChargeForward( channel, true ); return;
        case AnimEventT::DISABLE_TOUCH_DAMAGE: SetAnimCueEntityEffect_SetTouchDamage( channel, "" ); return;
        case AnimEventT::MOVE_SPEED_BOOST: ProcessAnimCue_SetMoveSpeedBoost( cue, channel ); return;
        case AnimEventT::ENABLE_TURN_ANIMATION_OVERRIDE: SetAnimCueEntityEffect_TurnAnimationOverride( channel, true ); return;
        case AnimEventT::DISABLE_TURN_ANIMATION_OVERRIDE: SetAnimCueEntityEffect_TurnAnimationOverride( channel, false ); return;
        case AnimEventT::DIE: ProcessAnimCue_Die(); return;
        case AnimEventT::TELEDASH: /* todobrandon: remove */ return;
        case AnimEventT::NONE: return;
        case AnimEventT::GENERIC_ATTACK_1: return;
        case AnimEventT::SET_OPACITY: SetAnimCueEntityEffect_SetOpacity( cue ); return;
        case AnimEventT::SPAWN_ENTITY: ProcessAnimCue_SpawnEntity( cue ); return;
        case AnimEventT::DISABLE_ATTACHMENT_VISIBILITY: SetAnimCueEntityEffect_DisableAttachmentVisiblity( channel, false ); return;
        case AnimEventT::ADD_ANIMSTATE:
            if ( cue.arguments.size() < 1 ) {
                ERR("in cue file for entity %s, addAnimState does not specify which.\n", GetIdentifier().c_str());
            } else {
                modelInfo.AddAnimState( GetAnimStateFromName( cue.arguments[0] ) );
            }
            break;
        case AnimEventT::REM_ANIMSTATE:
            if ( cue.arguments.size() < 1 ) {
                ERR("in cue file for entity %s, remAnimState does not specify which.\n", GetIdentifier().c_str());
            } else {
                modelInfo.RemAnimState( GetAnimStateFromName( cue.arguments[0] ) );
            }
            break;
        case AnimEventT::INTERACT: ProcessAnimCue_Interact(); return;
        case AnimEventT::PLAY_SOUND: ProcessAnimCue_PlaySound( cue ); return;
        case AnimEventT::STUN:
            SetAnimCueEntityEffect_Stun( cue, channel);
            break;
        case AnimEventT::WAKE_UP: ProcessAnimCue_UnsetSleepLevel(); return;
        default: break;
    }

    #pragma GCC diagnostic pop
    
    WARN("Unknown/unhandled anim cue event %i\n", static_cast<AnimEventT_BaseType>( cue.event ) );

    for ( auto const & arg : cue.arguments )
        WARN("with anim cue event argument %s\n", arg.c_str() );
}

void Entity::ProcessAnimCue_UnsetSleepLevel( void ) {
    //todocomposition
    if ( derives_from<Actor>(*this) )
        static_cast<Actor*>(this)->SetSleepLevel(0);
}

void Entity::ProcessAnimCue_Interact( void ) {
    //todocomposition
    if ( derives_from<Actor>(*this) )
        static_cast<Actor*>(this)->Interact();
}

void Entity::ProcessAnimCue_PlaySound( const AnimCue& cue ) const {
    if ( cue.arguments.size() < 1 ) {
        ERR("in cue file for entity %s, playSound does not specify what sound.\n", GetIdentifier().c_str());
        return;
    }
    
    soundManager.PlayAt( GetOrigin(), cue.arguments[0].c_str() );
}
    
AnimStateT Entity::GetAnimStateFromName( const std::string& animStateName ) const {
    ERR("Unknown anim state: %s\n", animStateName.c_str() );
    return AnimStateT::None;
}

void Entity::SetAnimCueEntityEffect_Stun( const AnimCue& cue, const AnimChannelT channel ) {
    //todocomposition
    
    if ( !derives_from<Actor>(*this) )
        return;
    
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
    
    bool value_err = false;
    int stun_length_ms;
    
    if ( cue.arguments.size() < 1 ) {
        value_err = true;
    } else {
        stun_length_ms = String::ToInt( cue.arguments[0] );

        if ( stun_length_ms < 0 ) {
            value_err = true;
        }   
    }
            
    if ( value_err ) {
        ERR("in cue file for entity %s, stun must specify a positive value stun [time_in_ms]\n", GetIdentifier().c_str() );
        return;
    }
    
    static_cast<Actor*>(this)->Stun( static_cast<uint>(stun_length_ms) );
}

void Entity::SetAnimCueEntityEffect_StopTurning( const AnimChannelT channel, const bool whether ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
    
    if ( whether ) {
        SetNoTurn(true);
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_DISABLE_TURNING );
    } else {
        SetNoTurn(false);
    }
}

void Entity::SetAnimCueEntityEffect_SetTouchDamage( const AnimChannelT channel, const char* damage_def ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
    
    if ( ! damage_def || String::Len( damage_def ) == 0 ) {
        SetTouchDamage("");
    } else {
        SetTouchDamage( damage_def );
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_DISABLE_TOUCH_DAMAGE );
    }
}

void Entity::SetAnimCueEntityEffect_ChargeForward( const AnimChannelT channel, const bool whether ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
    
    if ( whether ) {
        chargeForward = true;
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_STOP_CHARGE_FORWARD );
    } else {
        chargeForward = false;
    }
}

void Entity::SetAnimCueEntityEffect_SetMoveSpeedBost( const AnimChannelT channel, const float to ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
    
    if ( Maths::Approxf( to, 0 ) ) {
        SetMoveSpeedBoost(0);
    } else {
        SetMoveSpeedBoost( to );
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_MOVE_SPEED_BOOST );
    }
}

void Entity::SetAnimCueEntityEffect_StopMoving( const AnimChannelT channel, const bool whether ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
            animCueEntityEffects.resize( c+1 );
    
    if ( whether ) {
        SetNoMove(true);
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_DISABLE_MOVING );
    } else {
        SetNoMove(false);
    }
}

void Entity::SetAnimCueEntityEffect_SetOpacity( const AnimCue& cue ) {
    bool value_err = false;
    float opacity;

    if ( cue.arguments.size() < 1 ) {
        value_err = true;
    } else {
        opacity = String::ToFloat( cue.arguments[0] );
        if ( opacity < 0 || opacity > 1 )
            value_err = true;
    }
        
    if ( value_err ) {
        WARN("in cue file for entity %s, setOpacity must specify a value (between 0 and 1). Assumed 1\n", GetIdentifier().c_str() );
        opacity = 1;
    }
        
    SetOpacity( opacity );
}

void Entity::ProcessAnimCue_SpawnEntity( const AnimCue& cue ) {
    
    auto ent = entityInfoManager.LoadAndSpawn( cue.arguments[ANIMCUE_ENTITY_NAME_INDEX].c_str(), GetOrigin() );
    if ( ! ent ) {
        ERR("Couldn't create entity \"%s\" on entity \"%s\"\n", cue.arguments[ANIMCUE_ENTITY_NAME_INDEX].c_str(), GetIdentifier().c_str() );
        return;
    }
    
    if ( cue.arguments.size() > 1 ) {
        Vec3f offset;
        if ( cue.arguments[1] != "offset" || !String::ToVec3f( cue.arguments[2], offset ) ) {
            WARN("in cue file for entity %s, spawnEntity invalid format. Expected an entity type, then \"offset\", and then a Vec3f.\n", GetIdentifier().c_str());
        } else {
            const Mat3f rot( bounds.primitive->GetAngles() );
            const Vec3f rotatedOffset = offset * rot;
            ent->SetOrigin( GetOrigin() + rotatedOffset );
        }
    }

    ent->SetFacingDir( GetFacingDir() );
}

void Entity::SetAnimCueEntityEffect_DisableAttachmentVisiblity( const AnimChannelT channel, const bool visibility ) {

    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
        animCueEntityEffects.resize( c+1 );
       
    for ( uint i=0; i<modelInfo.NumAttachments(); ++i )
        modelInfo.GetAttachment(i)->SetVisible( visibility );
        
    if ( visibility == false )
        animCueEntityEffects[c].emplace_back( ENTITY_EFFECT_DISABLE_ATTACHMENT_VISIBILITY );
    
}

void Entity::StopAllAnimCueEntityEffects( const AnimChannelT channel ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( animCueEntityEffects.size() <= c )
        return;
        
    while ( animCueEntityEffects[c].size() > 0 ) {
        const ENTITY_EFFECT effect = animCueEntityEffects[c][ animCueEntityEffects[c].size() - 1 ];
        switch ( effect ) {
            case ENTITY_EFFECT_DISABLE_TURNING:
                SetAnimCueEntityEffect_StopTurning( channel, false );
                break;
            case ENTITY_EFFECT_DISABLE_MOVING:
                SetAnimCueEntityEffect_StopMoving( channel, false );
                break;
            case ENTITY_EFFECT_DISABLE_TOUCH_DAMAGE:
                SetAnimCueEntityEffect_SetTouchDamage( channel, "" );
                break;
            case ENTITY_EFFECT_MOVE_SPEED_BOOST:
                SetAnimCueEntityEffect_SetMoveSpeedBost( channel, 0 );
                break;
            case ENTITY_EFFECT_STOP_CHARGE_FORWARD:
                SetAnimCueEntityEffect_ChargeForward( channel, false );
                break;
            case ENTITY_EFFECT_NONE:
                break;
            case ENTITY_EFFECT_DISABLE_TURN_ANIM_OVERRIDE:
                SetAnimCueEntityEffect_TurnAnimationOverride( channel, false );
                break;
            case ENTITY_EFFECT_DISABLE_ATTACHMENT_VISIBILITY:
                SetAnimCueEntityEffect_DisableAttachmentVisiblity( channel, true );
                break;
            default:
                ERR("StopAllAnimCueEntityEffects(): Unknown effect id: %i\n", effect );
                break;
        }
        animCueEntityEffects[c].pop_back();
    }
}
