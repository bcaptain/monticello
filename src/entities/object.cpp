// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./object.h"

Object::Object( void )
    : name()
    , lastDrawFrame(0) // won't draw on frame zero, but so what - not likely to even happen anyhow
    , visible(true)
    {
}

Object::Object( const Object& other )
    : name(other.name)
    , lastDrawFrame(0) // won't draw on frame zero, but so what - not likely to even happen anyhow
    , visible(other.visible)
    {
}

Object& Object::operator=( const Object& other ) {
    if ( this == &other )
        return *this;

    name = other.name;
    lastDrawFrame = 0; // won't draw on frame zero, but so what - not likely to even happen anyhow
    visible = other.visible;

    return *this;
}

Object::~Object( void ) {
}

void Object::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteUInt( string_pool.AppendUnique( name ) );

    saveFile.parser.WriteBool( visible );
}

void Object::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    const std::string *strp;
    uint idx;

    saveFile.parser.ReadUInt( idx );
    strp = &string_pool[idx];
    ASSERT( strp );
    SetName( *strp );


    saveFile.parser.ReadBool( visible );
}

void Object::SetName( const std::string& _name ) {
    name = _name;
}

void Object::SetLastDrawFrame( const Uint32 frame ) {
    lastDrawFrame = frame;
}

Uint32 Object::GetLastDrawFrame( void ) const {
    return lastDrawFrame;
}

bool Object::IsVisible( void ) const {
    return visible;
}

void Object::SetVisible( const bool whether ) {
    visible = whether;
}

std::string Object::GetName( void ) const {
    return name;
}
