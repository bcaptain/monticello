// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./mine.h"

#include "../../game.h"
#include "../../damage/damageManager.h"
#include "../../particles/effectsManager.h"
#include "../../sound.h"

const float DEFAULT_MINE_DAMAGE_RADIUS = 2.0f;

std::vector< std::string > Mine::mineSetters;

void Mine::InitSetterNames( void ) {
    if ( mineSetters.size() == Mine_NUM_SETTERS )
        return; // already set
    mineSetters.resize(Mine_NUM_SETTERS);
    
    const uint i = Mine_SETTERS_START;
    mineSetters[Mine_SetRadius-i]="radius";
    mineSetters[Mine_SetDamageName-i]="dmg_explode";
    mineSetters[Mine_SetExplosionSound-i]="fx_explode";
    mineSetters[Mine_SetExplodeEffect-i]="snd_explode";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Mine,Entity>(mineSetters);
}

bool Mine::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<MineSetterT>(setter) ) {
        case Mine_SetRadius: SetRadius( String::ToFloat(value) ); return true;
        case Mine_SetDamageName: SetDamageName( value ); return true;
        case Mine_SetExplosionSound: SetExplosionSound( value ); return true;
        case Mine_SetExplodeEffect: SetExplodeEffect( value ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Mine_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Mine_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Mine>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Mine::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<MineSetterT>(setter) ) {
        case Mine_SetRadius: return String::ToString(GetRadius());
        case Mine_SetDamageName: return GetDamageName();
        case Mine_SetExplosionSound: return GetExplosionSound();
        case Mine_SetExplodeEffect: return GetExplodeEffectName();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Mine_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Mine_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Mine>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Mine::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( mineSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Mine_SETTERS_START;
        
    static_assert( is_parent_of<Mine>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Mine::Mine( void )
    : Entity()
    , dmg_explode()
    , snd_explode()
    , radius(DEFAULT_MINE_DAMAGE_RADIUS)
    , fx_explode()
{ }

Mine::Mine( const Mine& other )
    : Entity( other )
    , dmg_explode(other.dmg_explode)
    , snd_explode(other.snd_explode)
    , radius(other.radius)
    , fx_explode(other.fx_explode)
{ }

Mine& Mine::operator=( const Mine& other ) {

    dmg_explode = other.dmg_explode;
    snd_explode = other.snd_explode;
    radius = other.radius;
    //fx_explode = other.fx_explode;
    
    Entity::operator=( other );
    return *this;
}

Mine::~Mine( void ) {
}

void Mine::BumpedIntoBy( [[maybe_unused]] Entity& ent_unused ) {
    const Vec3f org( GetOrigin() );
    const auto mineDamage = damageManager.Get( dmg_explode.c_str() );

    if ( mineDamage ) {
        DamageZone( Primitive_Sphere( Sphere( org, radius ) ), 0, *mineDamage, GetWeakPtr(), DamageFalloffT::Linear );
    }

    if ( fx_explode )
        effectsManager.Spawn( *fx_explode, org );

    if ( snd_explode.size() > 0 )
        soundManager.PlayAt( GetOrigin(), snd_explode.c_str() );
    
    MarkForDeletion();
}

std::string Mine::GetDamageName( void ) const {
    return dmg_explode;
}

void Mine::SetDamageName( const std::string& dmg_name ) {
    dmg_explode = dmg_name;
}

std::string Mine::GetExplodeEffectName( void ) const {
    return fx_explode ? fx_explode->GetName() : "";
}

void Mine::SetExplodeEffect( const std::string& effect_name ) {
    SetExplodeEffect( effectsManager.Get( effect_name.c_str() ) );
}

void Mine::SetExplodeEffect( const std::shared_ptr< const Effect >& effect ) {
    fx_explode = effect;
}

float Mine::GetRadius( void ) const {
    return radius;
}

void Mine::SetRadius( const float to ) {
    radius = to;
}

std::string Mine::GetExplosionSound( void ) const {
    return snd_explode;
}

void Mine::SetExplosionSound( const std::string& snd_name ) {
    snd_explode = snd_name;
}

void Mine::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Mine>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, mineSetters );
}

void Mine::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Mine>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, mineSetters );
}
