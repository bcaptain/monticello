// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_HAZARDS_MINE_H_
#define SRC_ENTITIES_HAZARDS_MINE_H_

#include "../entity.h"


class Mine : public Entity {
public:
    static constexpr TypeInfo<Mine, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Mine( void );
    ~Mine( void ) override;
    Mine( const Mine& other );
    
    Mine& operator=( const Mine& other );
    
public:
    void BumpedIntoBy( Entity& ent_unused ) override;
    
    std::string GetDamageName( void ) const;
    void SetDamageName( const std::string& dmg_name ); 
    
    std::string GetExplosionSound( void ) const;
    void SetExplosionSound( const std::string& snd_name );
    
    void SetExplodeEffect( const std::string& effect_name );
    void SetExplodeEffect( const std::shared_ptr< const Effect >& effect );
    std::string GetExplodeEffectName( void ) const;
    
    float GetRadius( void ) const;
    void SetRadius( const float to );
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    std::string dmg_explode;
    std::string snd_explode;
    float radius;
    std::shared_ptr< const Effect > fx_explode;
    
// ** Named Setters
private:
    static const uint Mine_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum MineSetterT : NamedSetterT_BaseType {
        Mine_SetRadius = Mine_SETTERS_START
        , Mine_SetDamageName
        , Mine_SetExplosionSound
        , Mine_SetExplodeEffect
        , Mine_SETTERS_END
        , Mine_NUM_SETTERS = Mine_SETTERS_END - Mine_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > mineSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Mine>( get_typeid<Entity>() ) );
    static ToolTip< Mine, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};


#endif  // SRC_ENTITIES_HAZARDS_MINE_H_
