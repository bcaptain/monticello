// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./uiInteractor.h"
#include "../../gameui/gameui.h"
#include "../../game.h"

std::vector< std::string > UIInteractor::uiInteractorSetters;

void UIInteractor::InitSetterNames( void ) {
    if ( uiInteractorSetters.size() == UIInteractor_NUM_SETTERS )
        return; // already set
    uiInteractorSetters.resize(UIInteractor_NUM_SETTERS);
    
    const uint i = UIInteractor_SETTERS_START;
    uiInteractorSetters[UIInteractor_SetUIName-i] = "uiName";
    uiInteractorSetters[UIInteractor_SetUIPause-i] = "uiPause";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<UIInteractor,Module>(uiInteractorSetters);
}

bool UIInteractor::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<UIInteractorSetterT>(setter) ) {
        case UIInteractor_SetUIName: SetUIName( value ); return true;
        case UIInteractor_SetUIPause: SetUIPause( String::ToBool( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case UIInteractor_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case UIInteractor_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
            return Module::Set( setter, value );
    } 
}
std::string UIInteractor::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<UIInteractorSetterT>(setter) ) {
        case UIInteractor_SetUIName: return GetUIName();
        case UIInteractor_SetUIPause: return String::ToString(GetUIPause());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case UIInteractor_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case UIInteractor_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
            return Module::Get( setter );
    } 
}

NamedSetterT UIInteractor::GetSetterIndex( const std::string& key ) {
    NamedSetterT ret = Entity::GetSetterIndexIn( uiInteractorSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + UIInteractor_SETTERS_START;
        
    static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
    return Module::GetSetterIndex( key );
}

UIInteractor::UIInteractor( void )
    : Module()
    ,uiName()
    ,uiPause()
{
}

UIInteractor::UIInteractor( const UIInteractor& other )
    : Module( other )
    ,uiName( other.uiName )
    ,uiPause( other.uiPause )
{ }

UIInteractor& UIInteractor::operator=( const UIInteractor& other ) {
    uiName = other.uiName;
    uiPause = other.uiPause;
    
    Module::operator=( other );
    return *this;
}

UIInteractor::~UIInteractor( void ) {
}

bool UIInteractor::UseBy( [[maybe_unused]] Actor* user ) {
    auto page = game->GetUIPage( uiName );
    if ( !page )
        return false;

    game->OpenUIPage( page );

    if ( uiPause ) {
        game->Pause( PauseStateT::PausedByGameUI );
        page->UnpauseGameWhenClosed( true );
    }
    
    return true;
}

void UIInteractor::SetUIName( const std::string& to_name ) {
    uiName = to_name;
}

std::string UIInteractor::GetUIName( void ) const {
    return uiName;
}

void UIInteractor::SetUIPause( const bool to ) {
    uiPause = to;
}

bool UIInteractor::GetUIPause( void ) const {
    return uiPause;
}

void UIInteractor::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Module::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<UIInteractor>( get_typeid<Module>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, uiInteractorSetters );
}

void UIInteractor::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Module::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<UIInteractor>( get_typeid<Module>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, uiInteractorSetters );
}
