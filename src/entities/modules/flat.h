// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_MODULES_FLAT_H_
#define SRC_ENTITIES_MODULES_FLAT_H_

extern const float ENTITY_SNAP_ANGLE;

#include "../../base/main.h"

extern const uint FLAT_VERT_INDEX_BACK_LEFT;
extern const uint FLAT_VERT_INDEX_FRONT_LEFT;
extern const uint FLAT_VERT_INDEX_FRONT_RIGHT;
extern const uint FLAT_VERT_INDEX_BACK_RIGHT;

extern const Color4f FLAT_DEL_VERT_PREVIEW_COLOR;
extern const Color4f FLAT_ADD_VERT_PREVIEW_COLOR;
extern const Color4f FLAT_DEFORM_VERT_PREVIEW_COLOR;
extern const float FLAT_DEFORM_VERT_PREVIEW_SIZE;
extern const float FLAT_ADD_VERT_PREVIEW_SIZE;
extern const float FLAT_DEL_VERT_PREVIEW_SIZE;

extern const std::string CUSTOM_MODEL_FLAT; // custom models are not in the global models list

#include "./module.h"

/*!

Flat \n\n

A Module that has ownership of a unique model (it is not in the global models list) which has two coplanar
tris, and is resiable in the editor. This is for the purpose of being individually scalable so that we don't
have to use multiple modules to make up a single large flat area.

Verts are stored in the model clockwise.

**/

class Shard;


class Flat : public Module {
public:
    static constexpr TypeInfo<Flat, Module> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Flat( void );
    Flat( const Flat& other );
    ~Flat( void ) override;
    
public:
    void Spawn( void ) override;

public:
    Flat& operator=( const Flat& other );

public:
    void UpdateVBO( void )  override;
    using Module::DrawVBO;
    void DrawVBO( const float opacity ) const override;
    void PackVBO( void ) override;

    bool SetModel( const std::shared_ptr< const Model >& mod ) override; //! gives an error and does nothing
    bool SetMaterial( const std::shared_ptr< const Material >& mtl ) override;
    bool SetMaterial( const char* mtl ) override;
    bool SetMaterialToDefault( void ) override;

    std::size_t NumVerts( void ) const;
    Vec3f GetLocalVert( const std::size_t index ) const; //!< getting a vert outside of the bounds of the list is undefined behavior
    void SetLocalVert( const std::size_t index, const Vec3f& to ); //!< automatially updates UV. It us up to the caller to make sure the vert is coplanar.
    void NudgeLocalVert( const std::size_t index, const Vec3f& by ); //!< automatially updates UV. It us up to the caller to make sure the resulting is coplanar.

    virtual void ModelChanged( void );

    bool SetMemberVariable( const std::string& entry, const std::string& val ) override;

    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

    void AddVertBefore_Preview( const std::size_t index ) const;
    void AddVertBefore( const std::size_t index );
    void DeleteVert_Preview( const std::size_t index ) const;
    void DeleteVert( const std::size_t index );

    void Fracture( uint subFractures ); //!< split the model at a random place and create a separate entity for the half that broke off
    
    void SetTexAlign( const Vec2f& uv );
    void SetTexScale( const Vec2f& uv );
    Vec2f GetTexAlign( void ) const;
    Vec2f GetTexScale( void ) const;
    
private:
    std::shared_ptr< Shard > Fracture_Helper( const uint vertA, const uint vertB, const uint vertC ) const; //!< returns the new flat
    std::vector< std::shared_ptr< Shard > > SubFracture_3shards( void ); //!< if this flat is a Tri, this function will create 3 separate entities out of it and return them in a vector
    std::vector< std::shared_ptr< Shard > > SubFracture_4shards( void ); //!< if this flat is a Tri, this function will create 3 separate entities out of it and return them in a vector
    void GenerateFaces( void );
    std::pair< float, float > GetUVDivisor( void ) const;
    void UpdateUV( const std::size_t index );
    void UpdateAllUVs( void );
    void DrawSeams( const float opacity ) const;
    void DrawVertices( const float opacity ) const;

protected:
    std::shared_ptr< Model > mdl; //!< Entity's model will point to this. If Entity's model ever gets set to anything else, our model will still exist safe and sound right nere
    VBO<float>* vbo_verts;
    Vec2f texAlign;
    Vec2f texScale;

// ** ToolTips
public:
    static_assert( is_parent_of<Flat>( get_typeid<Module>() ) );
    static ToolTip< Flat, Module > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

/*!

Shard \n\n

A fracture of a flat that has been Fracture()'d

**/


class Shard : public Flat {
public:
    static constexpr TypeInfo<Shard, Flat> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    void ModelChanged( void ) override;
    void Think( void ) override;
};

#endif  // SRC_ENTITIES_MODULES_FLAT_H_
