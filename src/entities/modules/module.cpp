// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./module.h"
#include "../../game.h"

const float ENTITY_SNAP_ANGLE = 90.0f;

std::vector< std::string > Module::moduleSetters;

void Module::InitSetterNames( void ) {
    if ( moduleSetters.size() == Module_NUM_SETTERS )
        return; // already set
    moduleSetters.resize(Module_NUM_SETTERS);
    
    const uint i = Module_SETTERS_START;
    //moduleSetters[Module_SetSomething-i] = "something";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Module,Entity>(moduleSetters);
}

bool Module::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<ModuleSetterT>(setter) ) {
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Module_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Module_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
            return Module::Set( setter, value );
    } 
}
std::string Module::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<ModuleSetterT>(setter) ) {
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Module_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Module_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
            return Module::Get( setter );
    } 
}

NamedSetterT Module::GetSetterIndex( const std::string& key ) {
    NamedSetterT ret = Entity::GetSetterIndexIn( moduleSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Module_SETTERS_START;
        
    static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Module::Module( void )
    : Entity()
{
}

Module::Module( const Module& other )
    : Entity( other )
{ }

void Module::UpdateNavMeshOccupancy( void ) {
    // doesn't
}

Module& Module::operator=( const Module& other ) {
    Entity::operator=( other );
    return *this;
}

Module::~Module( void ) {

}

void Module::Think( void ) {
    if ( !( IsSpawned()) )
        return;

    if ( CheckDie() )
        return;

    ApplyRotation();
    RepositionBoundEffects();
    DoEffects();
}

void Module::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, moduleSetters );
}

void Module::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, moduleSetters );
}
