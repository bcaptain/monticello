// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./flat.h"
#include "../../rendering/vbo.h"
#include "../../rendering/renderer.h"
#include "../../rendering/materialManager.h"
#include "../../editor/editor.h"

//todo: fractured flats need their UVs to acknowledge texScale and texAlign

const uint FLAT_VERT_INDEX_BACK_LEFT = 0;
const uint FLAT_VERT_INDEX_FRONT_LEFT = 1;
const uint FLAT_VERT_INDEX_FRONT_RIGHT = 2;
const uint FLAT_VERT_INDEX_BACK_RIGHT = 3;

const Color4f FLAT_DEL_VERT_PREVIEW_COLOR(1,0,0,1);
const Color4f FLAT_ADD_VERT_PREVIEW_COLOR(1,0,0,1);
const Color4f FLAT_DEFORM_VERT_PREVIEW_COLOR(1,1,0,1);
const float FLAT_DEFORM_VERT_PREVIEW_SIZE(7);
const float FLAT_DEL_VERT_PREVIEW_SIZE(7);
const float FLAT_ADD_VERT_PREVIEW_SIZE(7);

const Vec3f FLAT_VERTEX_NORMAL(0,0,1);

const std::string CUSTOM_MODEL_FLAT = "CUSTOM_MODEL_FLAT";

Flat::Flat( void )
    : Module()
    , mdl( std::make_shared< Model >() )
    , vbo_verts(nullptr)
    , texAlign(0,0)
    , texScale(1,1)
{
    // ** set up the model
    mdl->SetName( CUSTOM_MODEL_FLAT.c_str() );
    mdl->meshes.resize( 1 );
    Mesh& mesh = mdl->meshes[0];
    mesh.SetVersion( 2 );

    // ** generate all the unique verts and UVs in a circular fashion
    const std::size_t numVerts=4;

    ASSERT( numVerts >= 3 );
    mesh.verts.reserve(numVerts);
    mesh.texs.reserve(numVerts);
    mesh.faces.reserve(numVerts-2); //!< because of the way we create faces, numVerts-2 will always be the number of faces

    // ** start with four verts, a 1x1 meter square with "top left" vert being the origin

    // back left
    mesh.verts.emplace_back( Vec3f(0,-1,0), FLAT_VERTEX_NORMAL );
    mesh.texs.emplace_back( 0,0 );

    // front left
    mesh.verts.emplace_back( Vec3f(0,0,0), FLAT_VERTEX_NORMAL );
    mesh.texs.emplace_back( 0,1 );

    // front right
    mesh.verts.emplace_back( Vec3f(1,0,0), FLAT_VERTEX_NORMAL );
    mesh.texs.emplace_back( 1,1 );

    // back right
    mesh.verts.emplace_back( Vec3f(1,-1,0), FLAT_VERTEX_NORMAL );
    mesh.texs.emplace_back( 1,0 );

    GenerateFaces();
    modelInfo.SetModel( mdl );
}

void Flat::Spawn( void ) {
    Entity::Spawn();
    bounds.SetToAABox3D();
    
    ModelChanged();
}

std::shared_ptr< Shard > Flat::Fracture_Helper( const uint vertA, const uint vertB, const uint vertC ) const {
    std::shared_ptr< Shard > newTriShard = std::static_pointer_cast<Shard>( game->NewEnt( TypeID_Shard ) );

    Mesh& new_mesh = newTriShard->mdl->meshes[0];
    const Mesh& my_mesh = mdl->meshes[0];

    new_mesh.verts[0] = my_mesh.verts[vertA];
    new_mesh.texs[0] = my_mesh.texs[vertA];

    new_mesh.verts[1] = my_mesh.verts[vertB];
    new_mesh.texs[1] = my_mesh.texs[vertB];

    new_mesh.verts[2] = my_mesh.verts[vertC];
    new_mesh.texs[2] = my_mesh.texs[vertC];

    newTriShard->DeleteVert(3);
 
    return newTriShard;
}

void Flat::Fracture( uint subFractures ) {
    // ** MAIN SHARDS - Just splits apart existing tris.

    if ( NumVerts() < 3 )
        return;

    std::vector< std::shared_ptr< Shard > > shards;

    const uint MAX_SUBFRACTURES = 4;
    if ( subFractures > MAX_SUBFRACTURES ) {
        WARN("Flat::Fracture(): Fracturing a face more than %u times is a lot. Doing %u times instead.\n", MAX_SUBFRACTURES, MAX_SUBFRACTURES);
        subFractures = MAX_SUBFRACTURES;
    }

    // ** first we must apply any rotation on the entity to all of the vertices

    Mat4f rot;

    const Vec3f my_origin = GetOrigin();
    rot.Translate( my_origin );
    SetOrigin(0,0,0);
    
    rot.RotateZYX( bounds.primitive->GetAngles() );
    bounds.primitive->SetAngle(0,0,0);

    rot.Translate( -modelInfo.GetOffset() );
    modelInfo.SetOffset( Vec3f(0,0,0) );

    auto & verts = mdl->meshes[0].verts;
    for ( uint v=0; v<verts.size(); ++v ) {
        verts[v].co = (verts[v].co) * rot;
    }

    // do the first face
    shards.push_back( Fracture_Helper(0,1,NumVerts()-1) );

    // vertices in a Flat are zippered. so every other tri has to be entered in reverse order. this way, all the new shards are in clockwise order
    for ( std::size_t A = 1, B=NumVerts()-1; A+1 < B; ++A, --B ) {
        shards.push_back( Fracture_Helper(A,A+1,B) );
        if ( A+1 < B-1 )
            shards.push_back( Fracture_Helper(B,A+1,B-1) );
    }

    game->MarkEntityForDeletion( GetWeakPtr() );

    // ** SUB SHARDS - split indivirual tris into smaller tris

    while ( subFractures > 0 ) {
        std::vector< std::shared_ptr< Shard > > sub_shards;

        // create all the new shards
        for ( uint s=0; s<shards.size(); ++s ) {
            const std::vector< std::shared_ptr< Shard > > new_shards = shards[s]->SubFracture_4shards();

            // add the new shards to the sub_shards list
            for ( uint n=0; n<new_shards.size(); ++n ) {
                sub_shards.push_back(new_shards[n]);
            }
        }

        shards = sub_shards; // we no longer care about the old shards
        sub_shards.clear();

        --subFractures;
    }

    for ( uint s=0; s<shards.size(); ++s ) {
        if ( ! shards[s] )
            continue;

        shards[s]->SetMaterial( GetMaterial() );
        shards[s]->bounds.primitive->SetRoll( bounds.primitive->GetRoll() );
        shards[s]->bounds.primitive->SetPitch( bounds.primitive->GetPitch() );
        shards[s]->bounds.primitive->SetYaw( bounds.primitive->GetYaw() );
        shards[s]->ModelChanged();

        auto & vertices = shards[s]->mdl->meshes[0].verts;
        Circle3f incircle = Triangle3f::GetIncircle( vertices[0].co, vertices[1].co, vertices[2].co );

        vertices[0].co -= incircle.origin;
        vertices[1].co -= incircle.origin;
        vertices[2].co -= incircle.origin;
        shards[s]->SetOrigin( incircle.origin );

        const float ten_centimeters = 0.1f;
        if ( incircle.radius < ten_centimeters )
            incircle.radius = ten_centimeters;
        shards[s]->bounds.SetToSphere( incircle.radius );

        shards[s]->SetNoGrav( true );
        shards[s]->SetGravityRatio( 1.0f );
        shards[s]->bounds.SetVelocity( Random::Direction() * Random::Float(-10,10) );
        shards[s]->bounds.SetAngularVelocity( Random::Direction() * Random::Float(-120,120) );
        shards[s]->SetNoCulling(true);
        shards[s]->Spawn();
        shards[s]->AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_GHOST_US );
        shards[s]->WakeUp();
    }
}

std::vector< std::shared_ptr< Shard > > Flat::SubFracture_4shards( void ) {
    ASSERT( NumVerts() == 3 );

    if ( NumVerts() != 3 )
        return {};

    // Get three random points along each edge, but not too close to the corners of the triangles.

    // ** calculate the new vertices
    const Mesh& my_mesh = mdl->meshes[0];
    const auto & A = my_mesh.verts[0].co;
    const auto & B = my_mesh.verts[1].co;
    const auto & C = my_mesh.verts[2].co;

    const float minumum_time=0.2f;
    const float maximum_time=0.8f;

    const float AB_len_sq = (B-A).SquaredLen();
    const float AB_newvert_pos = (B-A).SquaredLen() * Random::Float( minumum_time, maximum_time );
    const float AB_multiplier = AB_newvert_pos / AB_len_sq;
    const Vec3f new_AB_vert = (B-A) * AB_multiplier + A;

    const float BC_len_sq = (C-B).SquaredLen();
    const float BC_newvert_pos = (C-B).SquaredLen() * Random::Float( minumum_time, maximum_time );
    const float BC_multiplier =  BC_newvert_pos / BC_len_sq;
    const Vec3f new_BC_vert = (C-B) * BC_multiplier + B;

    const float CA_len_sq = (A-C).SquaredLen();
    const float CA_newvert_pos = (A-C).SquaredLen() * Random::Float( minumum_time, maximum_time );
    const float CA_multiplier = CA_newvert_pos / CA_len_sq;
    const Vec3f new_CA_vert = (A-C)*CA_multiplier + C;

    // ** create the shards

    std::vector< std::shared_ptr< Shard > > shards;

    for ( uint i=0; i<4; ++i ) {
        shards.push_back( std::static_pointer_cast< Shard >( game->NewEnt( TypeID_Shard ) ) );
    }

    Mesh* new_mesh = &shards[0]->mdl->meshes[0];
    new_mesh->verts[0].co = A;
    new_mesh->verts[1].co = new_AB_vert;
    new_mesh->verts[2].co = new_CA_vert;
    new_mesh->texs[0] = my_mesh.texs[0];
    shards[0]->DeleteVert(3);

    new_mesh = &shards[1]->mdl->meshes[0];
    new_mesh->verts[0].co = B;
    new_mesh->verts[1].co = new_BC_vert;
    new_mesh->verts[2].co = new_AB_vert;
    new_mesh->texs[0] = my_mesh.texs[1];
    shards[1]->DeleteVert(3);

    new_mesh = &shards[2]->mdl->meshes[0];
    new_mesh->verts[0].co = C;
    new_mesh->verts[1].co = new_CA_vert;
    new_mesh->verts[2].co = new_BC_vert;
    new_mesh->texs[0] = my_mesh.texs[2];
    shards[2]->DeleteVert(3);

    new_mesh = &shards[3]->mdl->meshes[0];
    new_mesh->verts[0].co = new_AB_vert;
    new_mesh->verts[1].co = new_BC_vert;
    new_mesh->verts[2].co = new_CA_vert;
    shards[3]->DeleteVert(3);

    game->MarkEntityForDeletion( GetWeakPtr() );

    return shards;
}

std::vector< std::shared_ptr< Shard > > Flat::SubFracture_3shards( void ) {
    ASSERT( NumVerts() == 3 );

    if ( NumVerts() != 3 )
        return {};

    // ** Get incircle of tri
    const Circle3f incircle = Triangle3f::GetIncircle(
        mdl->meshes[0].verts[0].co,
        mdl->meshes[0].verts[1].co,
        mdl->meshes[0].verts[2].co
    );
    
    // ** get a random point inside the the incircle by half it's radius.
    // ** This ensures that a point is not gotten outside the tri or too close to an edge

    if ( Maths::Approxf( incircle.radius, 0 ) ) {
        WARN("Flat::Fracture(): Fracture too small. Not proceeding.\n");
        return {};
    }

    Vec3f point = incircle.origin + Random::Direction() * Random::Float( 0, incircle.radius );
    point.z = (
        mdl->meshes[0].verts[0].co.z +
        mdl->meshes[0].verts[1].co.z +
        mdl->meshes[0].verts[2].co.z
    ) / 3;

    // ** create the shards

    const uint useless_index = 0;
    std::vector< std::shared_ptr< Shard > > shards;

    for ( uint i=0; i<3; ++i ) {
        shards.push_back( std::static_pointer_cast<Shard>( game->NewEnt( TypeID_Shard ) ) );
    }

    shards[0] = Fracture_Helper( 0, 1, useless_index );
    shards[1] = Fracture_Helper( 1, 2, useless_index );
    shards[2] = Fracture_Helper( 2, 0, useless_index );

    // ** calculate new UVs for the new vertice

    const auto & my_texs = mdl->meshes[0].texs;
    const auto & my_verts = mdl->meshes[0].verts;

    const float x_numer = point.x - my_verts[1].co.x;
    const float x_denom = my_verts[0].co.x - my_verts[1].co.x;
    float x_percent = 0;
    if ( ! Maths::Approxf( x_denom, 0 ) )
        x_percent = x_numer / x_denom;

    const float y_numer = point.y - my_verts[1].co.y;
    const float y_demon = my_verts[0].co.y - my_verts[1].co.y;
    float y_percent = 0;
    if ( ! Maths::Approxf( y_demon, 0 ) )
        y_percent = y_numer / y_demon;

    const Vec2f uv(
        x_percent * ( my_texs[0].x - my_texs[1].x ) + my_texs[1].x,
        y_percent * ( my_texs[0].y - my_texs[1].y ) + my_texs[1].y
    );

    // ** assign UVs and vertices
    for ( uint i=0; i<3; ++i ) {
        shards[i]->mdl->meshes[0].texs[2] = uv;
        shards[i]->mdl->meshes[0].verts[2].co = point;
    }

    // subfractures entities do not have to be removed from the game because they were never spawned.

    return shards;
}

void Flat::GenerateFaces( void ) {
    Mesh& mesh = mdl->meshes[0];
    ASSERT( mesh.verts.size() > 2 );
    auto & faces = mesh.faces;
    const std::size_t num_verts = mesh.verts.size();
    const std::size_t num_faces = num_verts - 2;

    faces.clear();
    faces.reserve( num_faces );

    /* ** the vertices are stored circularly clockwise. we will iterate through
          them as through they are a triangle strip and create faces from them.
    */

    auto addFaceAndUV = [&faces]( const std::size_t first, const std::size_t second, const std::size_t third ) {
        faces.emplace_back(
            std::initializer_list<uint>{first,second,third}, // vertex indices
            std::initializer_list<uint>{first,second,third}, // UV indices
            std::initializer_list<uint>{first,second,third} // normals
        );
    };

    // add the first face
    addFaceAndUV(0,1,num_verts-1);

    for ( std::size_t A = 1, B=num_verts-1; A+1 < B; ++A, --B ) {
        addFaceAndUV(A,A+1,B);
        if ( A+1 < B-1 )
            addFaceAndUV(B,A+1,B-1);
    }

    UpdateAllUVs();
}

void Flat::UpdateVBO( void )  {
    Entity::UpdateVBO();

    // when flats update vbo, we need to repack the model because the UV may have changed
    mdl->UpdateVBOs();
}

Flat::Flat( const Flat& other )
    : Module( other )
    , mdl( std::make_shared<Model>( *other.mdl ) )
    , vbo_verts( nullptr )
    , texAlign(other.texAlign)
    , texScale(other.texScale)
    {
    modelInfo.SetModel( mdl );
    ModelChanged();
}

Flat::~Flat( void ) {
}

Flat& Flat::operator=( const Flat& other ) {
    Module::operator=( other );
    mdl = std::make_shared<Model>( *other.mdl );

    return *this;
}

void Flat::SetTexAlign( const Vec2f& uv ) {
    texAlign = uv;
    UpdateAllUVs();
}

void Flat::SetTexScale( const Vec2f& uv ) {
    texScale = uv;
    UpdateAllUVs();
}
    
bool Flat::SetModel( [[maybe_unused]] const std::shared_ptr< const Model >& mod ) {
    ERR("You cannot change the model on a Flat entity, its model is unique and deformable.\n");
    return false;
}

void Flat::DeleteVert( const std::size_t index ) {
    if ( index >= NumVerts() )
        return;

    if ( NumVerts() <= 3 )
        return;

    Mesh& mesh = mdl->meshes[0];
    ShuffleErase( mesh.verts, index );
    ShuffleErase( mesh.texs, index );
    GenerateFaces();
}

void Flat::DeleteVert_Preview( const std::size_t index ) const {
    if ( index >= NumVerts() )
        return;

    if ( NumVerts() <= 3 )
        return;

    auto const verts( mdl->meshes[0].verts );
    Vec3f vert( verts[index].co );
    vert *= Quat::FromAngles_ZYX( bounds.primitive->GetAngles() );
    vert += GetOrigin();

    renderer->DrawPoint( vert, FLAT_DEL_VERT_PREVIEW_COLOR, FLAT_DEL_VERT_PREVIEW_SIZE  );
}

void Flat::AddVertBefore_Preview( const std::size_t idx ) const {
    ASSERT( idx <= NumVerts() );
    std::size_t a;
    std::size_t b;

    if ( idx == 0 ) {
        a = NumVerts()-1;
        b = 0;
    } else {
        a = idx-1;
        b = idx;
    }

    const Vec3f vertA( mdl->meshes[0].verts[a].co );
    const Vec3f vertB( mdl->meshes[0].verts[b].co );

    Vec3f vert( (vertA + vertB) / 2 );
    vert *= Quat::FromAngles_ZYX( bounds.primitive->GetAngles() );
    vert += GetOrigin();
    renderer->DrawPoint( vert, FLAT_ADD_VERT_PREVIEW_COLOR, FLAT_ADD_VERT_PREVIEW_SIZE );
}

void Flat::AddVertBefore( const std::size_t idx ) {
    ASSERT( idx <= NumVerts() );
    std::size_t a;
    std::size_t b;

    if ( idx == 0 ) {
        a = NumVerts()-1;
        b = 0;
    } else {
        a = idx-1;
        b = idx;
    }

    Mesh& mesh = mdl->meshes[0];
    auto & verts = mesh.verts;
    auto & texs = mesh.texs;
    auto & faces = mesh.faces;

    // ** insert vertex
    const MeshVert vert(
        ( verts[a].co+verts[b].co ) / 2,
        FLAT_VERTEX_NORMAL
    );
    verts.insert( begin(verts)+static_cast< std::ptrdiff_t >( b ), std::move(vert) );

    // ** insert uv
    const float new_tex_a_x( ( texs[a].x+texs[b].x ) / 2 );
    const float new_tex_a_y( ( (texs[a].y)+(texs[b].y) ) / 2 );
    texs.insert( begin(texs)+static_cast< std::ptrdiff_t >( b ), Vec2f( new_tex_a_x, new_tex_a_y ) );

    // ** every vert a at or beyond b increases as a result
    for ( auto & f : faces ) {
        for ( auto & v : f.vert ) {
            if ( v.vertIndex >= b ) {
                ++v.vertIndex; // coordinate
                ++*v.texIndex; // uv
            }
        }
    }

    GenerateFaces();
}

void Flat::ModelChanged( void ) {
    ASSERT( mdl != nullptr );
    mdl->UpdateVBOs();
    mdl->UpdateBounds();
    
    // if this is a sphere, for example if it's a shard, we don't update bounds
    if ( bounds.IsAABox3D() ) {
        bounds.SetToAABox3D( *mdl );
    }
}

void Flat::PackVBO( void ) {
    Entity::PackVBO();
    mdl->PackVBO();
}

bool Flat::SetMemberVariable( const std::string& entry, const std::string& val ) {
    return Entity::SetMemberVariable( entry, val );
}

void Flat::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Module::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Flat>( get_typeid<Module>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    // Entity::MapSave will save our model

    saveFile.parser.WriteVec2f( texAlign );
    saveFile.parser.WriteVec2f( texScale );
}

void Flat::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Module::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Flat>( get_typeid<Module>() ) );

    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    //TODOLOADTIME: we don't need to set up the model in the constructor for this, since we're just going to create a new model on load

    auto ent_mdl = modelInfo.GetModel();
    ASSERT_MSG( ent_mdl->GetName() == CUSTOM_MODEL_FLAT, "Entity did not properly save/load Flat model.\n");
    mdl = std::const_pointer_cast< Model >( ent_mdl );

    saveFile.parser.ReadVec2f( texAlign );
    saveFile.parser.ReadVec2f( texScale );

    modelInfo.SetModel( mdl );
}

std::pair< float, float > Flat::GetUVDivisor( void ) const {
    if ( auto mtl = GetMaterial() )
        if ( auto dif = mtl->GetDiffuse() )
            return std::pair<float,float>( dif->GetWidth()/PIXELS_PER_METER, dif->GetHeight()/PIXELS_PER_METER );

    return std::pair<float,float>( DEFAULT_TEXTURE_SIZE/PIXELS_PER_METER, DEFAULT_TEXTURE_SIZE/PIXELS_PER_METER );
}

void Flat::UpdateUV( const std::size_t index ) {
    auto & vert = mdl->meshes[0].verts[ index ].co;
    auto & tex = mdl->meshes[0].texs[ index ];
    const std::pair< float, float > divisor = GetUVDivisor();
    tex.x = vert.x/divisor.first;
    tex.y = vert.y/divisor.second;
    
    tex.x *= texScale.x;
    tex.y *= texScale.y;
    
    tex.x += texAlign.x;
    tex.y += texAlign.y;
}

void Flat::NudgeLocalVert( const std::size_t index, const Vec3f& by ) {
    mdl->meshes[0].verts[ index ].co += by;
    UpdateUV( index );
    ModelChanged();
}

bool Flat::SetMaterial( const char* mtl ) {
    return Flat::SetMaterial( materialManager.Get( mtl ) );
}

bool Flat::SetMaterial( const std::shared_ptr< const Material >& mtl ) {
    if ( !Entity::SetMaterial( mtl ) )
        return false;
    
    UpdateAllUVs();
    return true;
}

bool Flat::SetMaterialToDefault( void ) {
    WARN("Not setting Flat to default material, there is no such thing for Flats.\n");
    return true;
}

void Flat::UpdateAllUVs( void ) {
    for ( std::size_t v=0; v<NumVerts(); ++v )
        UpdateUV( v );

    ModelChanged(); // to update UVs
}

void Flat::DrawVBO( const float opacity ) const {
#ifdef MONTICELLO_EDITOR
    if ( editor ) {
        if ( auto flat = editor->GetFlatFromSelection() ) { // maybe hash_d_flatVerts ?
            if ( flat->GetIndex() == GetIndex() ) {
                DrawVertices( opacity );
            }
        }
    }
#endif // MONTICELLO_EDITOR

    Module::DrawVBO( opacity );

    if ( globalVals.GetBool( gval_d_flatLines ) )
        DrawSeams( opacity );
}

void Flat::DrawSeams( const float opacity ) const {
    const Quat rot( Quat::FromAngles_ZYX( bounds.primitive->GetAngles() ) );

    const Mesh& mesh = mdl->meshes[0];
    const auto num_verts = mesh.verts.size();
    ASSERT( num_verts > 2 );

    const Color4f color(1,0,0,opacity);
    
    const Vec3f origin = GetOrigin();

    // draw the outside lines
    for ( std::size_t a=num_verts-1, b=0; b < num_verts; a=b, b++ )
        renderer->DrawLine( ( mesh.verts[a].co * rot ) + origin, ( mesh.verts[b].co * rot ) + origin, color, color );

    // draw the inside lines
    for ( std::size_t a = 1, b=num_verts-1; a < b; ++a, --b ) {
        renderer->DrawLine( ( mesh.verts[a].co * rot ) + origin, ( mesh.verts[b].co * rot ) + origin, color, color );
        if ( a+1 < b-1 )
            renderer->DrawLine( ( mesh.verts[b].co * rot ) + origin, ( mesh.verts[a+1].co * rot ) + origin, color, color );
    }
}

void Flat::DrawVertices( const float opacity ) const {
    const Quat rot( Quat::FromAngles_ZYX( bounds.primitive->GetAngles() ) );
    
    const Vec3f org = GetOrigin();
    renderer->DrawPoint( org, Color4f(1,1,0,1), 5 );

    for ( auto const & vert : mdl->meshes[0].verts )
        renderer->DrawPoint( vert.co*rot + org, Color4f(1,0,0,opacity), 5 ); //todo: DrawFramelyShapes_Editor
}

void Shard::Think( void ) {
    if ( !( IsSpawned()) )
        return;

    if ( CheckDie() )
        return;

    ApplyRotation();

    if ( IsGravityEnabled() ) {
        bounds.ApplyGravityForce( GetGravity() );
        DoVelocity();
        bounds.DoAngularVelocity();
    }
}

void Shard::ModelChanged( void ) {
    ASSERT( mdl != nullptr );
    mdl->UpdateVBOs();
    mdl->UpdateBounds();
    SetMoved();
}

Vec2f Flat::GetTexAlign( void ) const {
    return texAlign;
}

Vec2f Flat::GetTexScale( void ) const {
    return texScale;
}
    
Vec3f Flat::GetLocalVert( const std::size_t index ) const {
    ASSERT( index < NumVerts() );
    return mdl->meshes[0].verts[ index ].co;
}

std::size_t Flat::NumVerts( void ) const {
    return mdl->meshes[0].verts.size();
}

void Flat::SetLocalVert( const std::size_t index, const Vec3f& to ) {
    NudgeLocalVert( index,  to - mdl->meshes[0].verts[ index ].co );
}
