// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_MODULES_MODULE_H_
#define SRC_ENTITIES_MODULES_MODULE_H_

extern const float ENTITY_SNAP_ANGLE;

#include "../../base/main.h"
#include "../entity.h"

/**

Module
a modelled section of map

**/


class Module : public Entity {
public:
    static constexpr TypeInfo<Module, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Module( void );
    Module( const Module& other );
    ~Module( void ) override;

public:
    Module& operator=( const Module& other );

public:
    void Think( void ) override;
    void UpdateNavMeshOccupancy( void ) override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
// ** Named Setters
private:
    static const uint Module_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum ModuleSetterT : NamedSetterT_BaseType {
        //Module_SetSomething.. = Module_SETTERS_START
        Module_SETTERS_END = Module_SETTERS_START
        , Module_NUM_SETTERS = Module_SETTERS_END - Module_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };
    
private:
    static std::vector< std::string > moduleSetters;

public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

public:
    static_assert( is_parent_of<Module>( get_typeid<Entity>() ) );
    static ToolTip< Module, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }


};

#endif  // SRC_ENTITIES_MODULES_MODULE_H_
