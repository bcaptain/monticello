// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_MODULES_UIINTERACTOR_H_
#define SRC_ENTITIES_MODULES_UIINTERACTOR_H_

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "./module.h"

/*!

UIInteractor \n\n

A module that, when used, will cause a specific UI page to display

**/


class UIInteractor : public Module {
public:
    static constexpr TypeInfo<UIInteractor, Module> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    UIInteractor( void );
    UIInteractor( const UIInteractor& other );
    ~UIInteractor( void ) override;

public:
    UIInteractor& operator=( const UIInteractor& other );

public:
    bool UseBy( Actor* user = nullptr ) override;
    
    void SetUIName( const std::string& to_name );
    std::string GetUIName( void ) const;
    
    void SetUIPause( const bool to );
    bool GetUIPause( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

private:

    std::string uiName;
    bool uiPause;
    
// ** Named Setters
private:
    static const uint UIInteractor_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum UIInteractorSetterT : NamedSetterT_BaseType {
        UIInteractor_SetUIName = UIInteractor_SETTERS_START
        , UIInteractor_SetUIPause
        , UIInteractor_SETTERS_END
        , UIInteractor_NUM_SETTERS = UIInteractor_SETTERS_END - UIInteractor_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > uiInteractorSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );
    
// ** ToolTips
public:
    static_assert( is_parent_of<UIInteractor>( get_typeid<Module>() ) );
    static ToolTip< UIInteractor, Module > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_MODULES_UIINTERACTOR_H_
