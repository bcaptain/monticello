// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_MAPSCRIPTMANAGER_H_
#define SRC_MAPSCRIPTMANAGER_H_

#include "./clib/src/warnings.h"
#include "./trigger.h"

/*!

Trigger_NextMap \n\n

If triggered, game will switch to the next map

**/


class Trigger_NextMap : public Trigger {
public:
    static constexpr TypeInfo<Trigger_NextMap, Trigger> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Trigger_NextMap( void );
    ~Trigger_NextMap( void ) override;
    Trigger_NextMap( const Trigger_NextMap& other ) = default;

public:
    Trigger_NextMap& operator=( const Trigger_NextMap& other ) = default;

public:
    void Think( void ) override;
    void InfluenceActivator( Entity& activator ) override;

// ** ToolTips
public:
    static_assert( is_parent_of<Trigger_NextMap>( get_typeid<Trigger>() ) );
    static ToolTip< Trigger_NextMap, Trigger > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_MAPSCRIPTMANAGER_H_
