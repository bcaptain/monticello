// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./trigger_safezone.h"
#include "../../game.h"
#include "../actor.h"
#include "../../rendering/materialManager.h"

Trigger_SafeZone::Trigger_SafeZone( void ) {
}

Trigger_SafeZone::~Trigger_SafeZone( void ) {
}

void Trigger_SafeZone::Think( void ) {
    Trigger::Think();
}

void Trigger_SafeZone::InfluenceActivator( Entity& activator ) {
    if ( !game->IsPlayer( &activator ) )
        return;

    game->SetSafeZone( GetWeakPtr() );
}
