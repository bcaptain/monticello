// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_TRIGGER_H_
#define SRC_ENTITIES_TRIGGER_H_

#include "../../base/main.h"
#include "../entity.h"

const Color4f TRIGGER_ACTIVE_TARGET_LINE_SOURCE_COLOR(1,1,1,1); // white
const Color4f TRIGGER_ACTIVE_TARGET_LINE_DEST_COLOR(0,1,0,1); // green

const Color4f TRIGGER_INACTIVE_TARGET_LINE_SOURCE_COLOR(1,1,1,1); // white
const Color4f TRIGGER_INACTIVE_TARGET_LINE_DEST_COLOR(1,0,0,1); // red

const Color4f TRIGGER_ACTIVE_CHAIN_LINE_SOURCE_COLOR(1,1,1,1); // white
const Color4f TRIGGER_ACTIVE_CHAIN_LINE_DEST_COLOR(1,1,0,1); // yellow

const Color4f TRIGGER_INACTIVE_CHAIN_LINE_SOURCE_COLOR(1,1,1,1); // white
const Color4f TRIGGER_INACTIVE_CHAIN_LINE_DEST_COLOR(1,0,0,1); // red

class Trigger;
class Actor;
class Entity;
typedef void (*TriggerActionFuncPtrT)( Entity& ent, Trigger* trig );


class Trigger : public Entity {
public:
    static constexpr TypeInfo<Trigger, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

private:
    friend void TriggerAction_Move( Entity& ent, Trigger* trig ); // this function is for all intents and purposes a "member" of this class, but must be a free function in order to use generic pointers to it

public:
    Trigger( void );
    ~Trigger( void ) override;
    Trigger( const Trigger& other );

public:
    Trigger& operator=( const Trigger& other );

public:
    void Spawn( void ) override;
    void Think( void ) override;

    void SetActive( const bool whether );
    bool IsActive( void ) const;
    void BumpedIntoBy( Entity& ent ) override;
    bool UseBy( Actor* user = nullptr ) override;
    bool TryToActivate( Entity* ent ); //!< called by Collide()
    
private:
    bool TryToActivate_ByPlayer( Entity* ent );
protected:
    void DoActivate( const std::vector< std::string >& list, Actor* actor ) override;
public:
    void DoChainedTargets( void );

    bool CheckItemRequirements( Actor* ent );
    bool CheckEntValRequirements( Actor* ent );

    virtual bool AddTarget( Entity& target );
    virtual bool AddTriggerChain( std::shared_ptr< Trigger >& trig );

    using Entity::DrawVBO;
    void DrawVBO( const float opacity ) const override;

    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

    void Triggered( Entity* by_ent );

    bool SetModel( const std::shared_ptr< const Model >& mod ) override;

    virtual void ActivateAllTargets( void );
    
private:
    void MapSave_Targets( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    void MapSave_Chain( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    void MapLoad_Targets( FileMap& saveFile, const std::vector< std::string >& string_pool );
    void MapLoad_Chain( FileMap& saveFile, const std::vector< std::string >& string_pool );

protected:
    virtual void ActivateTargets( TriggerActionFuncPtrT func );
    virtual void InfluenceActivator( Entity& activator );

private:
    void InfluenceTargets( void );

    void DrawTriggerTargetLines( void ) const;
    void DrawTriggerChainLines( void ) const;

private:
    void GiveItemsToActivator( Entity* by_ent );
   
private:

    SETTER_GETTER( TriggerSound, std::string, snd_trigger );
    SETTER_GETTER( RequiredItem, std::string, req_item );
    SETTER_GETTER( RequiredBoolVal, std::string, req_boolVal );
    SETTER_GETTER( NextMap, std::string, nextmap );
    SETTER_GETTER_BYVAL( NudgeTurn, float, nudgeTurnAmount );
    SETTER_GETTER_BYVAL( ActionDelay, uint, action_delay );
    SETTER_GETTER_BYVAL( Delay, uint, delay );
    SETTER_GETTER_BYVAL( TimeActivated, uint, timeActivated );
    SETTER_GETTER_BYVAL( Times, uint, times );
    SETTER_GETTER_BYVAL( Activations, uint, activations );
    SETTER_GETTER_BYVAL( InProgress, bool, inProgress );
    bool active;
    SETTER_GETTER_BYVAL( TriggersActive, bool, set_triggers_active );
    SETTER_GETTER_BYVAL( ChainsActive, bool, set_chains_active );
    SETTER_GETTER_BYVAL( ChainsInactive, bool, set_chains_inactive );
    
public:
    LinkList< std::weak_ptr< Entity > > targets;
    LinkList< std::weak_ptr< Entity > > triggerChain; //!< triggers that are triggered when this one completes

// ** Named Setters
private:
    static const uint Trigger_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum TriggerSetterT : NamedSetterT_BaseType {
        Trigger_SetActionDelay = Trigger_SETTERS_START
        , Trigger_SetDelay
        , Trigger_SetInProgress
        , Trigger_SetTimeActivated
        , Trigger_SetActive
        , Trigger_SetTimes
        , Trigger_SetActivations
        , Trigger_SetTriggerSound
        , Trigger_SetRequiredItem
        , Trigger_SetRequiredBoolVal
        , Trigger_SetTriggersActive
        , Trigger_SetChainsInactive
        , Trigger_SetChainsActive
        , Trigger_SetNextMap
        , Trigger_SetNudgeTurn
        , Trigger_SETTERS_END
        , Trigger_NUM_SETTERS = Trigger_SETTERS_END - Trigger_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > triggerSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
    static_assert( is_parent_of<Trigger>( get_typeid<Entity>() ) );
    static ToolTip< Trigger, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

void TriggerAction_Activate( Entity& ent, Trigger* trig );

#endif  // SRC_ENTITIES_TRIGGER_H_
