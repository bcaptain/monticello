// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./trigger_mover.h"
#include "../../game.h"

std::vector< std::string > Trigger_Mover::triggerMoverSetters;

void Trigger_Mover::InitSetterNames( void ) {
    if ( triggerMoverSetters.size() == TriggerMover_NUM_SETTERS )
        return; // already set
    triggerMoverSetters.resize(TriggerMover_NUM_SETTERS);
    
    const uint i = TriggerMover_SETTERS_START;
    triggerMoverSetters[TriggerMover_SetMoverScript-i] = "moverScript";
    triggerMoverSetters[TriggerMover_SetPrevMove-i] = "prevMove";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Trigger_Mover,Trigger>(triggerMoverSetters);
}

bool Trigger_Mover::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<TriggerMoverSetterT>(setter) ) {
        case TriggerMover_SetMoverScript: SetMoverScript( value ); return true;
        case TriggerMover_SetPrevMove: SetPrevMove( String::ToUInt( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case TriggerMover_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case TriggerMover_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Trigger_Mover>( get_typeid<Trigger>() ) );
            return Trigger::Set( setter, value );
    } 
}

std::string Trigger_Mover::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<TriggerMoverSetterT>(setter) ) {
        case TriggerMover_SetMoverScript: return GetMoverScript();
        case TriggerMover_SetPrevMove: return String::ToString( GetPrevMove() );
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case TriggerMover_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case TriggerMover_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Trigger_Mover>( get_typeid<Trigger>() ) );
            return Trigger::Get( setter );
    } 
}

NamedSetterT Trigger_Mover::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( triggerMoverSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + TriggerMover_SETTERS_START;
        
    static_assert( is_parent_of<Trigger_Mover>( get_typeid<Trigger>() ) );
    return Trigger::GetSetterIndex( key );
}

Trigger_Mover_TargetData::Trigger_Mover_TargetData( const Entity* ent, const Vec3f& tmpdat )
    : target( ent )
    , tmp( tmpdat )
{ }

const std::string DEFAULT_TRIGGER_MOVER_SCRIPT = "mover_script_name";

Trigger_Mover::Trigger_Mover( void )
    : moveOps()
    , targetData()
    , loadedScriptPath()
    , moverScript( DEFAULT_TRIGGER_MOVER_SCRIPT )
    , action_percent_complete( 0.0f )
    , prevMove()
    , curMoveOp( nullptr )
{
    entVals.SetUInt("times", 1 ); //toNamedSetter
    //todo entVals.Set( entval_moverScript, "mover_script_name"); // a default
    SetTimes(1);
    SetActionDelay(0); // delay between actions after triggered
}

Trigger_Mover::Trigger_Mover( const Trigger_Mover& other )
    : Trigger( other )
    , moveOps()
    , targetData()
    , loadedScriptPath()
    , moverScript(other.moverScript)
    , action_percent_complete( 0.0f )
    , prevMove( other.prevMove )
    , curMoveOp( nullptr )
{ }

Trigger_Mover& Trigger_Mover::operator=( const Trigger_Mover& other ) {
    loadedScriptPath = other.loadedScriptPath;
    moverScript = other.moverScript;
    moveOps = other.moveOps;
    ASSERT( entVals.GetUInt("activations") == 0 ); //toNamedSetter
    action_percent_complete = 0.0f;
    prevMove = other.prevMove;
    curMoveOp = nullptr; // as long as activated triggers aren't copied, we're fine. that shouldn't happen anyway, so let's do an ASSERT(:
    
    // Was targetData intentionally not copied? after some thought, it doens't seem like it should be.
    Trigger::operator=(other);
    return *this;
}

Trigger_Mover::~Trigger_Mover( void ) {
}

void Trigger_Mover::Spawn( void ) {
    Entity::Spawn();
    WakeUp();
}

void Trigger_Mover::ActivateAllTargets( void ) {
    if ( moverScript.size() < 1 ) {
        ERR("Trigger_Mover activated but not attached to a moverScript.\n");
        return;
    }

    if ( loadedScriptPath != moverScript ) {
        if ( ! game->LoadMoverScript( moverScript.c_str(), moveOps ) ) {
            ERR("Trigger_Mover activated but could not reload moverScript: %s.\n", moverScript.c_str());
            return;
        }
    }

    if ( moveOps.Num() == 0 ) {
        ERR("Trigger_Mover activated but has no move operations.\n");
        return;
    }

    SetupTargetData();
}

void Trigger_Mover::SetupTargetData( void ) {
    targetData.DelAll();

    Link< std::weak_ptr< Entity > > *link = targets.GetFirst();
    while ( link != nullptr ) {
        if ( auto target = link->Data().lock() ) {
            switch ( moveOps.GetFirst()->Data().GetMoveOpType() ) { // garunteed to exist by caller
                case MoveOpTypeT::MOVE_LOCAL: FALLTHROUGH;
                case MoveOpTypeT::MOVE:
                    targetData.Append( Trigger_Mover_TargetData( target.get(), target->GetOrigin() ) );
                    break;
                case MoveOpTypeT::ROTATE_OWN_ORIGIN:
                    targetData.Append( Trigger_Mover_TargetData( target.get(), Vec3f(0,0,target->bounds.primitive->GetYaw()) ) );
                    break;
                case MoveOpTypeT::NONE: FALLTHROUGH;
                case MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN: FALLTHROUGH;
                case MoveOpTypeT::RESET: FALLTHROUGH;
                default:
                    ASSERT( false );
                    break;
            };
        }

        link = link->GetNext();
    }
}

Trigger_Mover_TargetData* Trigger_Mover::GetTargetData( Entity* ent ) {

    Link< Trigger_Mover_TargetData > *link = targetData.GetFirst();
    while ( link != nullptr ) {
        if ( link->Data().target == ent ) {
            return &link->Data();
        }

        link = link->GetNext();
    }

    DIE("Trigger target has no data.\n");
    return nullptr;
}

void Trigger_Mover::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    ASSERT( false ); //todo: where did MapSave go? Should we just get rid of Mover?
}

void Trigger_Mover::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    ASSERT( false ); //todo: where did MapSave go? Should we just get rid of Mover?
    Trigger::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    if ( !moverScript.empty() ) {
        if ( ! game->LoadMoverScript( moverScript.c_str(), moveOps ) ) {
            ERR_DIALOG("Trigger_Mover could not load script: %s.\n", moverScript.c_str() );
        }
    }
}

void Trigger_Mover::Think( void ) {
    Entity::Think(); // Note: because trigger class doesn't override Think()

    if ( !( IsSpawned()) )
        return;

    if ( moveOps.Num() == 0 )
        return;

    if ( targets.Num() == 0 )
        return;

    if ( ! GetInProgress() )
        return;

    // check if we have an action delay when we are triggered
    Uint32 act_delay = GetActionDelay();
    if ( act_delay > 0 ) {
        if ( game->GetGameTime() - prevMove < act_delay ) {
            return;
        }

        // set up the next move after our delay
        prevMove = game->GetGameTime();
    }

    UpdateTime();

    ASSERT( curMoveOp );
    MoveOp* op = &curMoveOp->Data();

    if ( game->GetGameTime() < GetTimeActivated() + op->time_depart )
        return;

    Trigger::ActivateTargets( &TriggerAction_Move );

    if ( action_percent_complete >= 1.0f ) {
        // we make sure the current link's action is finished by activating the targets with one final
        // call that is at the ending time. The next link can wait for the sake of frame time
        curMoveOp = curMoveOp->GetNext();

        if ( curMoveOp == nullptr ) {
            SetInProgress( false );
            DoChainedTargets();
        }
    }
}

void Trigger_Mover::UpdateTime( void ) {
    if ( ! curMoveOp ) {
        curMoveOp = moveOps.GetFirst();
        ASSERT( curMoveOp );
    }

    MoveOp* op = &curMoveOp->Data();

    const Uint32 time = game->GetGameTime();

    ASSERT( time >= GetTimeActivated() );
    ASSERT( op->time_arrive >= op->time_depart );
    const Uint32 timeIntoAction = time - GetTimeActivated() - op->time_depart;
    const Uint32 totalActionTime = op->time_arrive - op->time_depart;

    if ( Maths::Approxf( totalActionTime, 0 ) || timeIntoAction >= totalActionTime ) {
        action_percent_complete = 1.0f;
    } else {
        action_percent_complete = static_cast<float>(timeIntoAction) / totalActionTime;
        ASSERT( action_percent_complete < 1.0f );
    }
}

void Trigger_Mover::ActivateTargets( TriggerActionFuncPtrT func ) {
    ASSERT( func );
    ASSERT( targets.Num() == targetData.Num() );

    Link< std::weak_ptr< Entity > > *link = targets.GetFirst();
    Link< Trigger_Mover_TargetData > *dlink = targetData.GetFirst();
    while ( link != nullptr ) {
        if ( auto target = link->Data().lock() ) {
            (*func)(*target, this);
            link = link->GetNext();
            dlink = dlink->GetNext();
        } else {
            link = link->Del();
            dlink = dlink->Del();
        }
    }
}

void TriggerAction_Move( Entity& ent, Trigger* trig ) {
    Trigger_Mover* trig_mover = static_cast< Trigger_Mover* >( trig ); // Note: trig is garunteed to be Trigger_Mover type, only those call this function
    MoveOp* op = &trig_mover->curMoveOp->Data(); // Note: link is garunteed to exist by our caller, Trigger_Mover::Think(). MoveOp links' datas are never null.
    Trigger_Mover_TargetData* target_data = trig_mover->GetTargetData( &ent );

    ASSERT( trig_mover );
    ASSERT( target_data );

    Vec3f vec_tmp;

    switch ( op->GetMoveOpType() ) {
        case MoveOpTypeT::MOVE_LOCAL: FALLTHROUGH;
        case MoveOpTypeT::MOVE:
            // set origin based on time along where ent started and where it ends
            op->GetMoveOrigin( vec_tmp );
            if ( op->GetMoveOpType() != MoveOpTypeT::MOVE_LOCAL )
                vec_tmp -= target_data->tmp;
            vec_tmp *= trig_mover->action_percent_complete;
            vec_tmp += target_data->tmp;
            ent.MoveTo( vec_tmp );

            // if this is the last time this action will be called, update the targetData
            if ( trig_mover->action_percent_complete >= 1.0f ) {
                target_data->tmp = vec_tmp;
            }
            break;
        case MoveOpTypeT::RESET:
            // reset
            trig_mover->SetTimeActivated( game->GetGameTime() );
            trig_mover->curMoveOp = trig_mover->moveOps.GetFirst();
            trig_mover->action_percent_complete = 0.0f;
            ASSERT( trig_mover->curMoveOp );
            break;
        case MoveOpTypeT::ROTATE_OWN_ORIGIN:
            // set angle based on time and angle
            op->GetRotationAxis( vec_tmp );
            //if ( op->GetMoveOpType() != MoveOpTypeT::MOVE_LOCAL )
                vec_tmp -= target_data->tmp;
            vec_tmp *= trig_mover->action_percent_complete;
            vec_tmp += target_data->tmp;
            ent.bounds.primitive->SetAngle( vec_tmp );
            // if this is the last time this action will be called, update the targetData
            if ( trig_mover->action_percent_complete >= 1.0f ) {
                target_data->tmp = ent.bounds.primitive->GetAngles();
            }

            break;
        case MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN:
            ASSERT_MSG( false, "TODO" ); //todo
            break;
        case MoveOpTypeT::NONE:
            break; // done
        default:
            ASSERT( false );
            break;
    }
}

std::string Trigger_Mover::GetMoverScript( void ) const {
    return moverScript;
}

void Trigger_Mover::SetMoverScript( const std::string& move_script ) {
    moverScript = move_script;
}

uint Trigger_Mover::GetPrevMove( void ) const {
    return prevMove;
}

void Trigger_Mover::SetPrevMove( const uint to ) {
    prevMove = to;
}

