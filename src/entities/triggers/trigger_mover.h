// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_TRIGGER_MOVER_H_
#define SRC_ENTITIES_TRIGGER_MOVER_H_

#include "../../base/main.h"
#include "./trigger.h"
#include "../../script/moverScript.h"

/*!

Trigger_Mover_TargetData \n\n

used to store information about the items being moved when activated.

NOTE: This class has not been used in some time and may be deprecated

**/

struct Trigger_Mover_TargetData {
    Trigger_Mover_TargetData( void ) = delete;
    Trigger_Mover_TargetData( const Trigger_Mover_TargetData& other ) = default;
    Trigger_Mover_TargetData( const Entity* ent, const Vec3f& tmpdat );

    Trigger_Mover_TargetData& operator=( const Trigger_Mover_TargetData& other ) = default;
    ~Trigger_Mover_TargetData( void ) = default;

    const Entity* target; /*!< not to be dereferenced, only used for comparison in Targets list \n\n

        Note: It is *incredibly* unlikely, but it is possible that if an entity is added to the targets,
        since it will be added here as well (in the linked list of Trigger_Mover_TargetData in Trigger_Mover),
        then if that entity is removed from that target list but not here, and another entity is added as a
        target that was created after the old one was destroyed, it may theoretically *possibly* have the same
        address and match up with the wrong TargetData. that would throw calculations off. Incredibly unlikely,
        but.... possible I think.
    */

    Vec3f tmp; //!< used to store origin, etc, for calculations
};


class Trigger_Mover : public Trigger {
public:
    static constexpr TypeInfo<Trigger_Mover, Trigger> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

private:
    friend void TriggerAction_Move( Entity& ent, Trigger* trig ); // this function is for all intents and purposes a "member" of this class, but must be a free function in order to use generic pointers to it

public:
    Trigger_Mover( void );
    ~Trigger_Mover( void ) override;
    Trigger_Mover( const Trigger_Mover& other );

public:
    Trigger_Mover& operator=( const Trigger_Mover& other );

public:
    void Spawn( void ) override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    void Think( void ) override;
    
    std::string GetMoverScript( void ) const;
    void SetMoverScript( const std::string& move_script );
    
    uint GetPrevMove( void ) const;
    void SetPrevMove( const uint to );

private:
    void UpdateTime( void );
    void SetupTargetData( void );
    Trigger_Mover_TargetData* GetTargetData( Entity* ent );

protected:
    void ActivateTargets( TriggerActionFuncPtrT func ) override;

private:
    void ActivateAllTargets( void ) override;

private:
    LinkList< MoveOp > moveOps;
    LinkList< Trigger_Mover_TargetData > targetData;
    std::string loadedScriptPath;
    std::string moverScript;
    float action_percent_complete;
    uint prevMove;
    Link< MoveOp > *curMoveOp;

// ** Named Setters
private:
    static const uint TriggerMover_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum TriggerMoverSetterT : NamedSetterT_BaseType {
        TriggerMover_SetMoverScript = TriggerMover_SETTERS_START
        , TriggerMover_SetPrevMove
        , TriggerMover_SETTERS_END
        , TriggerMover_NUM_SETTERS = TriggerMover_SETTERS_END - TriggerMover_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > triggerMoverSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Trigger_Mover>( get_typeid<Trigger>() ) );
    static ToolTip< Trigger_Mover, Trigger > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

void TriggerAction_Move( Entity& ent, Trigger* trig );

#endif  // SRC_ENTITIES_TRIGGER_MOVER_H_
