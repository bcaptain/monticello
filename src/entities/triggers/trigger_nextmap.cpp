// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./trigger_nextmap.h"
#include "../../game.h"
#include "../actor.h"
#include "../../rendering/materialManager.h"

Trigger_NextMap::Trigger_NextMap( void ) {
}

Trigger_NextMap::~Trigger_NextMap( void ) {
}

void Trigger_NextMap::Think( void ) {
    Trigger::Think();
}

void Trigger_NextMap::InfluenceActivator( Entity& activator ) {
    if ( !game->IsPlayer( &activator ) )
        return;

    game->LoadMap( GetNextMap() );
}
