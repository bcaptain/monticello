// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./trigger.h"
#include "../actor.h"
#include "../../game.h"
#include "../../sound.h"
#include "../../rendering/renderer.h"
#include "../../rendering/materialManager.h"

extern const Color4f TRIGGER_ACTIVE_TARGET_LINE_SOURCE_COLOR;
extern const Color4f TRIGGER_ACTIVE_TARGET_LINE_DEST_COLOR;

extern const Color4f TRIGGER_INACTIVE_TARGET_LINE_SOURCE_COLOR;
extern const Color4f TRIGGER_INACTIVE_TARGET_LINE_DEST_COLOR;

extern const Color4f TRIGGER_ACTIVE_CHAIN_LINE_SOURCE_COLOR;
extern const Color4f TRIGGER_ACTIVE_CHAIN_LINE_DEST_COLOR;

extern const Color4f TRIGGER_INACTIVE_CHAIN_LINE_SOURCE_COLOR;
extern const Color4f TRIGGER_INACTIVE_CHAIN_LINE_DEST_COLOR;

std::vector< std::string > Trigger::triggerSetters;

void Trigger::InitSetterNames( void ) {
    if ( triggerSetters.size() == Trigger_NUM_SETTERS )
        return; // already set
    triggerSetters.resize(Trigger_NUM_SETTERS);
    
    const uint i = Trigger_SETTERS_START;
    triggerSetters[Trigger_SetActionDelay-i] = "action_delay";
    triggerSetters[Trigger_SetDelay-i] = "delay";
    triggerSetters[Trigger_SetInProgress-i] = "inProgress";
    triggerSetters[Trigger_SetTimeActivated-i] = "timeActivated";
    triggerSetters[Trigger_SetActive-i] = "active";
    triggerSetters[Trigger_SetTimes-i] = "times";
    triggerSetters[Trigger_SetActivations-i] = "activations";
    triggerSetters[Trigger_SetTriggerSound-i] = "snd_trigger";
    triggerSetters[Trigger_SetRequiredItem-i] = "req_item";
    triggerSetters[Trigger_SetRequiredBoolVal-i] = "req_boolVal";
    triggerSetters[Trigger_SetTriggersActive-i] = "set_triggers_active";
    triggerSetters[Trigger_SetChainsActive-i] = "set_chains_active";
    triggerSetters[Trigger_SetChainsInactive-i] = "set_chains_inactive";
    triggerSetters[Trigger_SetNextMap-i] = "nextmap";
    triggerSetters[Trigger_SetNudgeTurn-i] = "nudgeTurn";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Trigger,Entity>(triggerSetters);
}

bool Trigger::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<TriggerSetterT>(setter) ) {
        case Trigger_SetTriggerSound: SetTriggerSound( value ); return true;
        case Trigger_SetRequiredItem: SetRequiredItem( value ); return true;
        case Trigger_SetRequiredBoolVal: SetRequiredBoolVal( value ); return true;
        case Trigger_SetActionDelay: SetActionDelay( String::ToUInt(value) ); return true;
        case Trigger_SetDelay: SetDelay( String::ToUInt(value) ); return true;
        case Trigger_SetInProgress: SetInProgress( String::ToBool(value) ); return true;
        case Trigger_SetTimeActivated: SetTimeActivated( String::ToUInt(value) ); return true;
        case Trigger_SetActive: SetActive( String::ToBool(value) ); return true;
        case Trigger_SetTimes: SetTimes( String::ToUInt(value) ); return true;
        case Trigger_SetActivations: SetActivations( String::ToUInt(value) ); return true;
        case Trigger_SetTriggersActive: SetTriggersActive( String::ToBool(value) ); return true;
        case Trigger_SetChainsInactive: SetChainsInactive( String::ToBool(value) ); return true;
        case Trigger_SetChainsActive: SetChainsActive( String::ToBool(value) ); return true;
        case Trigger_SetNextMap: SetNextMap( value ); return true;
        case Trigger_SetNudgeTurn: SetNudgeTurn( String::ToFloat( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Trigger_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Trigger_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Trigger>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    }
}
std::string Trigger::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<TriggerSetterT>(setter) ) {
        case Trigger_SetTriggerSound: return GetTriggerSound();
        case Trigger_SetRequiredItem: return GetRequiredItem();
        case Trigger_SetRequiredBoolVal: return GetRequiredBoolVal();
        case Trigger_SetActionDelay: return String::ToString(GetActionDelay());
        case Trigger_SetDelay: return String::ToString(GetDelay());
        case Trigger_SetInProgress: return String::ToString(GetInProgress());
        case Trigger_SetTimeActivated: return String::ToString(GetTimeActivated());
        case Trigger_SetActive: return String::ToString(IsActive());
        case Trigger_SetTimes: return String::ToString(GetTimes());
        case Trigger_SetActivations: return String::ToString(GetActivations());
        case Trigger_SetTriggersActive: return String::ToString(GetTriggersActive());
        case Trigger_SetChainsInactive: return String::ToString(GetChainsInactive());
        case Trigger_SetChainsActive: return String::ToString(GetChainsActive());
        case Trigger_SetNextMap: return GetNextMap();
        case Trigger_SetNudgeTurn: return String::ToString(GetNudgeTurn());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Trigger_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Trigger_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Trigger>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    }
}

NamedSetterT Trigger::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( triggerSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Trigger_SETTERS_START;
        
    static_assert( is_parent_of<Trigger>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Trigger::Trigger( void )
    : snd_trigger()
    , req_item()
    , req_boolVal()
    , nextmap()
    , nudgeTurnAmount()
    , action_delay()
    , delay()
    , timeActivated()
    , times()
    , activations()
    , inProgress()
    , active()
    , set_triggers_active()
    , set_chains_active()
    , set_chains_inactive()
    , targets()
    , triggerChain()
{ }

Trigger::Trigger( const Trigger& other )
    : Entity( other )
    , snd_trigger( other.snd_trigger )
    , req_item( other.req_item )
    , req_boolVal( other.req_boolVal )
    , nextmap( other.nextmap )
    , nudgeTurnAmount( other.nudgeTurnAmount )
    , action_delay( other.action_delay )
    , delay( other.delay )
    , timeActivated( other.timeActivated )
    , times( other.times )
    , activations( other.activations )
    , inProgress( other.inProgress )
    , active( other.active )
    , set_triggers_active(other.set_triggers_active)
    , set_chains_active(other.set_chains_active)
    , set_chains_inactive(other.set_chains_inactive)
    , targets( other.targets )
    , triggerChain( other.triggerChain )
{ }


Trigger& Trigger::operator=( const Trigger& other ) {
    Entity::operator=(other);

    snd_trigger = other.snd_trigger;
    req_item = other.req_item;
    req_boolVal = other.req_boolVal;
    nextmap = other.nextmap;
    nudgeTurnAmount = other.nudgeTurnAmount;
    targets = other.targets;
    triggerChain = other.triggerChain;
    action_delay = other.action_delay;
    delay = other.delay;
    timeActivated = other.timeActivated;
    times = other.times;
    activations = other.activations;
    inProgress = other.inProgress;
    active = other.active;
    set_triggers_active = other.set_triggers_active;
    set_chains_active = other.set_chains_active;
    set_chains_inactive = other.set_chains_inactive;
    
    return *this;
}

Trigger::~Trigger( void ) {
}

void Trigger::Spawn( void ) {
    Entity::Spawn();
    AddPhysFlag( PHYSFLAG_GHOST_THEM | PHYSFLAG_GHOST_US );
    WakeUp();
}

void Trigger::Think( void ) {
    if ( !IsSpawned() )
        return;
}

bool Trigger::SetModel( [[maybe_unused]] const std::shared_ptr< const Model >& mod ) {
    ERR("Triggers cannot have models.\n");
    return false;
}

void Trigger::BumpedIntoBy( Entity& ent ) {
    Bumped_DoFire( ent ); // because anything on fire should still catch other things on fire

    const bool manual_activation_only = IsUsable();
    if ( ! manual_activation_only )
        TryToActivate( &ent );
}

bool Trigger::UseBy( Actor* user ) {
    if ( IsUsable() )
        return TryToActivate( user );
        
    return false;
}

bool Trigger::TryToActivate( Entity* ent ) {
    if ( !IsActive() )
        return false;

    // check max activations
    const uint activate_infinitely = 0;
    if ( times != activate_infinitely && activations >= times ) {
        SetActive( false );
        return false;
    }

    const uint curTime = game->GetGameTime();
    
    // action delay
    if ( curTime - timeActivated < action_delay )
        return false;

    // activation delay
    if ( curTime - timeActivated < delay )
        return false;

    return TryToActivate_ByPlayer( ent );
}


bool Trigger::TryToActivate_ByPlayer( Entity* ent ) {
    if ( ent == nullptr )
        return false;

    if ( !GetByPlayer() )
        return false;

    if ( !derives_from< Actor >( *ent ) )
        return false;

    Actor* player = static_cast< Actor* >( ent );
    if ( !player )
        return false;

    if ( !game->IsPlayer( player ) )
        return false;

    if ( !CheckItemRequirements( player ) )
        return false;

    if ( !CheckEntValRequirements( player ) )
        return false;

    Activate( player );
    return true;
}

void Trigger::DoActivate( const std::vector< std::string >& list, Actor* actor ) {
    Entity::DoActivate( list, actor );
    Triggered( actor );
}

bool Trigger::CheckEntValRequirements( Actor* ent ) {
    // todo: "!" opertator for inverse.
    // todo: specific values, strings, etc
    if ( req_boolVal.empty() )
        return true;

    const std::vector< std::string > takes =String::ListifyString( req_boolVal.c_str() );
    for ( auto const& val : takes )
        if ( !ent->entVals.GetBool( val.c_str() ) ) //TODO Brandon :: std::vector< std::string > Entity::getterMethods?? //toNamedSetter
            return false;

    return true;
}

bool Trigger::CheckItemRequirements( Actor* ent ) {
    // todo: quantities of items
    if ( req_item.empty() )
        return true;

    const std::vector< std::string > takes =String::ListifyString( req_item.c_str() );
    for ( auto const& item : takes )
        if ( ent->inventory.Count( item.c_str() ) < 1 )
            return false;

    for ( auto const& item : takes )
        ent->inventory.Del( item.c_str(), 1 );

    return true;
}

void Trigger::GiveItemsToActivator( Entity* by_ent ) {
    const std::vector< std::string > _items; //todo
    if ( _items.size() < 1 ) {
        // nothing to give
    } else if ( ! by_ent ) {
        WARN("Trigger: Cannot give items to entity, no entity provided.\n");
    } else if ( ! derives_from< Actor >( *by_ent ) ) {
        WARN("Trigger: Cannot give items to entity %s, it is not an Actor.\n", by_ent->GetIdentifier().c_str() );
    } else {
        Actor* actor = static_cast< Actor* >( by_ent );
        for ( auto const& itemName : _items ) {
            actor->inventory.Add( Item( itemName.c_str(), itemName.c_str() ) );
        }
    }
}

void Trigger::Triggered( Entity* by_ent ) {
    if ( snd_trigger.size() > 0 )
        soundManager.Play( snd_trigger.c_str() );

    soundManager.Play("debug");
    inProgress = true; // used for derived classes
    ++activations;
    timeActivated = game->GetGameTime();

    InfluenceTargets();
    GiveItemsToActivator( by_ent );

    if ( targets.Num() != 0 ) {
        ActivateAllTargets();
    }

    if ( triggerChain.Num() != 0 )
        DoChainedTargets();

    if ( by_ent )
        InfluenceActivator( *by_ent );
}

void Trigger::InfluenceTargets( void ) {
    for ( auto & target : targets )
        if ( auto sptr = target.lock() )
            sptr->NudgeTurn( nudgeTurnAmount );
}

void Trigger::InfluenceActivator( [[maybe_unused]] Entity& activator ) {
    return;
}

void Trigger::ActivateAllTargets( void ) {
    ActivateTargets( &TriggerAction_Activate );
}

void Trigger::ActivateTargets( TriggerActionFuncPtrT func ) {
    ASSERT( func );

    Link< std::weak_ptr< Entity > > *link = targets.GetFirst();
    while ( link != nullptr ) {
        if ( auto target = link->Data().lock() ) {
            (*func)(*target, this);
            link = link->GetNext();
        } else {
            link = link->Del();
        }
    }
}

bool Trigger::AddTarget( Entity& target ) {
    // make sure the entity isn't already in our trigger list
    for ( auto& entHandle : targets )
        if ( const auto & handle = entHandle.lock() )
            if ( handle->GetIndex() == target.GetIndex() )
                return false;

    targets.Append( target.GetWeakPtr().lock() );
    return true;
}

bool Trigger::AddTriggerChain( std::shared_ptr< Trigger >& trig ) {
    if ( ! trig )
        return false;
        
    // make sure the entity isn't already in our triggerChain list
    for ( auto& entHandle : triggerChain )
        if ( const auto & handle = entHandle.lock() )
            if ( handle->GetIndex() == trig->GetIndex() )
                return false;

    triggerChain.Append( trig );
    return true;
}

void Trigger::DrawTriggerTargetLines( void ) const {
    const Link< std::weak_ptr< Entity > > *link = targets.GetFirst();
    while ( link != nullptr ) {
        if ( auto target = link->Data().lock() ) {
            if ( IsActive() ) {
                renderer->DrawLine( GetOrigin(), target->GetOrigin(), TRIGGER_ACTIVE_TARGET_LINE_SOURCE_COLOR, TRIGGER_ACTIVE_TARGET_LINE_DEST_COLOR );
            } else {
                renderer->DrawLine( GetOrigin(), target->GetOrigin(), TRIGGER_INACTIVE_TARGET_LINE_SOURCE_COLOR, TRIGGER_INACTIVE_TARGET_LINE_DEST_COLOR );
            }
        }
        // Note: empty links will be deleted when this entity is triggered
        link = link->GetNext();
    }
}

void Trigger::DrawTriggerChainLines( void ) const {
    const Link< std::weak_ptr< Entity > > *link = triggerChain.GetFirst();
    while ( link != nullptr ) {
        if ( auto trig = link->Data().lock() ) {
            if ( IsActive() ) {
                renderer->DrawLine( GetOrigin(), trig->GetOrigin(), TRIGGER_ACTIVE_CHAIN_LINE_SOURCE_COLOR, TRIGGER_ACTIVE_CHAIN_LINE_DEST_COLOR );
            } else {
                renderer->DrawLine( GetOrigin(), trig->GetOrigin(), TRIGGER_INACTIVE_CHAIN_LINE_SOURCE_COLOR, TRIGGER_INACTIVE_CHAIN_LINE_DEST_COLOR );
            }
        }
        // Note: empty links will be deleted when this entity is triggered
        link = link->GetNext();
    }
}

void Trigger::DrawVBO( const float opacity ) const {
    Entity::DrawVBO( opacity );

    if ( globalVals.GetBool( gval_d_triggerLines ) ) {
        DrawTriggerTargetLines();
        DrawTriggerChainLines();
    }
}

void Trigger::MapSave_Chain( FileMap& saveFile, [[maybe_unused]] LinkList< std::string >& string_pool ) const {

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    const Link< std::weak_ptr< Entity > >* link = triggerChain.GetFirst();
    Uint num_chain_indices=0;

    long cnt_pos = saveFile.FTell();
    saveFile.parser.WriteUInt( 0 ); // we'll come back to this

    while ( link != nullptr ) {
        if ( auto entp = link->Data().lock() ) {
            ++num_chain_indices;
            saveFile.WriteEntityRef( entp.get() );
        }

        link = link->GetNext();
    }

    long cur_pos = saveFile.FTell(); // go back and write the count
    saveFile.SeekSet( cnt_pos );
    saveFile.parser.WriteUInt( num_chain_indices );
    saveFile.SeekSet( cur_pos );
}

void Trigger::MapSave_Targets( FileMap& saveFile, [[maybe_unused]] LinkList< std::string >& string_pool ) const {

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    const Link< std::weak_ptr< Entity > >* link = targets.GetFirst();
    Uint num_target_indices=0;

    long cnt_pos = saveFile.FTell();
    saveFile.parser.WriteUInt( 0 ); // we'll come back to this

    while ( link != nullptr ) {
        if ( auto entp = link->Data().lock() ) {
            ++num_target_indices;
            saveFile.WriteEntityRef( entp.get() );
        }

        link = link->GetNext();
    }

    long cur_pos = saveFile.FTell(); // go back and write the count
    saveFile.SeekSet( cnt_pos );
    saveFile.parser.WriteUInt( num_target_indices );
    saveFile.SeekSet( cur_pos );
}

void Trigger::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Trigger>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSave_Targets( saveFile, string_pool );
    MapSave_Chain( saveFile, string_pool );
    
    MapSaveSetters( saveFile, string_pool, triggerSetters );
}

void Trigger::MapLoad_Chain( FileMap& saveFile, [[maybe_unused]] const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    // entities will be re=linked to this trigger once the map is fully loaded
    Uint num_trig_indices;
    saveFile.parser.ReadUInt( num_trig_indices );

    for ( uint i=0; i<num_trig_indices; ++i ) {
        std::shared_ptr< Entity > ep = saveFile.ReadEntityRef();
        ASSERT( ep );
        ASSERT( derives_from< Trigger >( *ep ) );
        auto chTrig = std::static_pointer_cast< Trigger >( ep );
        AddTriggerChain( chTrig );
    }
    
    
    MapLoadSetters( saveFile, string_pool, triggerSetters );
}

void Trigger::MapLoad_Targets( FileMap& saveFile, [[maybe_unused]] const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    // entities will be re=linked to this trigger once the map is fully loaded
    Uint num_target_indices;
    saveFile.parser.ReadUInt( num_target_indices );

    for ( uint i=0; i<num_target_indices; ++i ) {
        std::shared_ptr< Entity > ep = saveFile.ReadEntityRef();
        ASSERT( ep );
        AddTarget( *ep );
    }
}

void Trigger::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Trigger>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    MapLoad_Targets( saveFile, string_pool );
    MapLoad_Chain( saveFile, string_pool );
}

void Trigger::DoChainedTargets( void ) {
    Link< std::weak_ptr< Entity > > *link = triggerChain.GetFirst();
    while ( link != nullptr ) {
        if ( auto trig = link->Data().lock() ) {
            if ( set_chains_active ) {
                static_cast< Trigger* >( trig.get() )->SetActive( true );

            } else if ( set_chains_inactive ) {
                static_cast< Trigger* >( trig.get() )->SetActive( false );

            } else { // activate them
                 static_cast< Trigger* >( trig.get() )->Activate();
            }

            link = link->GetNext();
        } else {
            link = link->Del();
        }
    }
}

void TriggerAction_Activate( Entity& ent, Trigger* trig ) {
    if ( trig && trig->GetTriggersActive() && derives_from< Trigger >( ent ) ) {
        static_cast< Trigger* >( &ent )->SetActive( true );
    } else {
        ent.Activate();
    }
}

void Trigger::SetActive( const bool whether ) {
    active = whether;
}

bool Trigger::IsActive( void ) const {
    return active;
}
