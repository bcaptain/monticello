// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_TRIGGER_SAFEZONE_H_
#define SRC_ENTITIES_TRIGGER_SAFEZONE_H_

#include "./trigger.h"

/*!

Trigger_SafeZone \n\n

If triggered, game will spawn player directly in the middle of this trigger if he dies (that includes Z-height)

This class may be deprecated

**/


class Trigger_SafeZone : public Trigger {
public:
    static constexpr TypeInfo<Trigger_SafeZone, Trigger> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Trigger_SafeZone( void );
    ~Trigger_SafeZone( void ) override;
    Trigger_SafeZone( const Trigger_SafeZone& other ) = default;

public:
    Trigger_SafeZone& operator=( const Trigger_SafeZone& other ) = default;

public:
    void Think( void ) override;
    void InfluenceActivator( Entity& activator ) override;

public:
    static_assert( is_parent_of<Trigger_SafeZone>( get_typeid<Trigger>() ) );
    static ToolTip< Trigger_SafeZone, Trigger > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_TRIGGER_SAFEZONE_H_
