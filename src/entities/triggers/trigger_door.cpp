// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./trigger_door.h"
#include "../../game.h"
#include "../modules/module.h"
#include "../actor.h"

Trigger_Door::Trigger_Door( void )
{ }

Trigger_Door::~Trigger_Door( void ) {
}

void Trigger_Door::ActivateAllTargets( void ) {
    ActivateTargets( &TriggerAction_AnimateDoor );
//    ActivateTargets( &TriggerAction_Activate() );
}

void Trigger_Door::InfluenceActivator( Entity& activator ) {
    SlideCamera( activator );
}

void Trigger_Door::SlideCamera( Entity& activator ) {
    auto sptr = activator.GetWeakPtr().lock();
    if ( !sptr ) {
        WARN("SlideCamera(): Not executing for entity that appears to be marked for deletion: %s.\n", activator.GetIdentifier().c_str() );
        return;
    }
    
    auto player = game->GetPlayer();
    if ( !derives_from< Actor >( activator ) || player != std::static_pointer_cast< Actor >( sptr ) ) {
        WARN("SlideCamera(): Not executing for non-player entity: %s.\n", activator.GetIdentifier().c_str() );
        return;
    }
    
    Link< std::weak_ptr< Entity > > *link = targets.GetFirst();
    std::shared_ptr< Entity > door;
    if ( ! link || !( door = link->Data().lock() ) ) {
        WARN("SlideCamera(): No door\n");
        return;
    }
    
    // NOTE: we used to do this here (stop the player from moving, pause the game, etc) before we ripped out the rooms code:
    // game->QueueEvent_RoomChange( *door, player );
}

void Trigger_Door::BumpedIntoBy( Entity& ent ) {
    Bumped_DoFire( ent );

    const bool manual_activation_only = IsUsable();

    if ( ! manual_activation_only ) {
        TryToActivate( &ent );
    }
}

void TriggerAction_AnimateDoor( Entity& ent, Trigger* trig ) {
}
