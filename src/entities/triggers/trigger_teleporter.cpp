// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./trigger_teleporter.h"
#include "../../game.h"

std::vector< std::string > Trigger_Teleporter::triggerTeleporterSetters;

void Trigger_Teleporter::InitSetterNames( void ) {
    if ( triggerTeleporterSetters.size() == Trigger_Teleporter_NUM_SETTERS )
        return; // already set
    triggerTeleporterSetters.resize(Trigger_Teleporter_NUM_SETTERS);
    
    const uint i = Trigger_Teleporter_SETTERS_START;
    triggerTeleporterSetters[Trigger_Teleporter_SetWarpTo-i] = "warpTo";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Trigger_Teleporter,Trigger>(triggerTeleporterSetters);
}

bool Trigger_Teleporter::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<Trigger_TeleporterSetterT>(setter) ) {
        case Trigger_Teleporter_SetWarpTo: SetWarpTo( String::ToVec3f(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Trigger_Teleporter_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Trigger_Teleporter_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Trigger_Teleporter>( get_typeid<Trigger>() ) );
            return Trigger::Set( setter, value );
    } 
}

std::string Trigger_Teleporter::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<Trigger_TeleporterSetterT>(setter) ) {
        case Trigger_Teleporter_SetWarpTo: return String::ToString( GetWarpTo() );
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Trigger_Teleporter_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Trigger_Teleporter_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Trigger_Teleporter>( get_typeid<Trigger>() ) );
            return Trigger::Get( setter );
    } 
}

NamedSetterT Trigger_Teleporter::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( triggerTeleporterSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Trigger_Teleporter_SETTERS_START;
        
    static_assert( is_parent_of<Trigger_Teleporter>( get_typeid<Trigger>() ) );
    return Trigger::GetSetterIndex( key );
}

Trigger_Teleporter::Trigger_Teleporter( void )
    : activators()
    , warpTo()
{ }

Trigger_Teleporter::~Trigger_Teleporter( void ) {
}

void Trigger_Teleporter::InfluenceActivator( Entity& activator ) {
    AppendUniqueWptr( activators, activator.GetWeakPtr().lock() );
}

void Trigger_Teleporter::Think( void ) {
    if ( !( IsSpawned()) )
        return;
        
    Trigger::Think();

    if ( activators.Num() < 1 )
        return;

    if ( Maths::Approxf( warpTo, Vec3f(0,0,0) ) ) {
        ERR("Trigger_Teleporter: Not teleporting activator(s), warp_to is not set on trigger\n" );
        activators.DelAll();
        return;
    }

    Link< std::weak_ptr< Entity > >* link = activators.GetFirst();

    while ( link != nullptr ) {
        if ( auto ent = link->Data().lock() ) {
            warpTo -= ent->GetOrigin();
            //todowarp if ( ent->Warp( vel ) ) { // don't teleport into things
            //    ent->bounds.SetVelocity(0,0,0);
            //}
        }

        link = link->Del();
    }
}

void Trigger_Teleporter::SetWarpTo( const Vec3f& to ) {
    warpTo = to;
}

Vec3f Trigger_Teleporter::GetWarpTo( void ) const {
    return warpTo;
}

void Trigger_Teleporter::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Trigger::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Trigger_Teleporter>( get_typeid<Trigger>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, triggerTeleporterSetters );
}

void Trigger_Teleporter::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Trigger::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Trigger_Teleporter>( get_typeid<Trigger>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, triggerTeleporterSetters );
}
