// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_TRIGGER_DOOR_H_
#define SRC_ENTITIES_TRIGGER_DOOR_H_

#include "./trigger.h"

class Flat;


class Trigger_Door : public Trigger {
public:
    static constexpr TypeInfo<Trigger_Door, Trigger> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Trigger_Door( void );
    ~Trigger_Door( void ) override;
    Trigger_Door( const Trigger_Door& other ) = default;

public:
    Trigger_Door& operator=( const Trigger_Door& other ) = default;
    
public:
    void BumpedIntoBy( Entity& ent ) override;

protected:
    void ActivateAllTargets( void ) override;
    void InfluenceActivator( Entity& activator ) override;

private:
    void SlideCamera( Entity& activator );

// ** ToolTips
public:
    static_assert( is_parent_of<Trigger_Door>( get_typeid<Trigger>() ) );
    static ToolTip< Trigger_Door, Trigger > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

void TriggerAction_AnimateDoor( Entity& door, Trigger* trig );

#endif  // SRC_ENTITIES_TRIGGER_DOOR_H_
