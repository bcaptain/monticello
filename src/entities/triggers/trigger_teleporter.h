// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_TRIGGER_TELEPORTER_H_
#define SRC_ENTITIES_TRIGGER_TELEPORTER_H_

#include "./trigger.h"

/*!

Trigger_Teleporter \n\n

teleports those that trigger it to the first target the trigger has \n\n

This class may be deprecated

**/


class Trigger_Teleporter : public Trigger {
public:
    static constexpr TypeInfo<Trigger_Teleporter, Trigger> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Trigger_Teleporter( void );
    ~Trigger_Teleporter( void ) override;
    Trigger_Teleporter( const Trigger_Teleporter& other ) = default;

public:
    Trigger_Teleporter& operator=( const Trigger_Teleporter& other ) = default;

public:
    void Think( void ) override;
    void InfluenceActivator( Entity& activator ) override;
    
    void SetWarpTo( const Vec3f& to );
    Vec3f GetWarpTo( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

private:
    LinkList< std::weak_ptr< Entity > > activators; //!< list of activators who we need to teleport when we execute our Think() again. We can't do it in Collide() because it would be applied in the middle of collision/sliding
    
private:

    Vec3f warpTo;
    
// ** Named Setters
private:
    static const uint Trigger_Teleporter_SETTERS_START = Trigger_NUM_SETTERS;
protected:
    enum Trigger_TeleporterSetterT : NamedSetterT_BaseType {
        Trigger_Teleporter_SetWarpTo = Trigger_Teleporter_SETTERS_START
        , Trigger_Teleporter_SETTERS_END
        , Trigger_Teleporter_NUM_SETTERS = Trigger_Teleporter_SETTERS_END - Trigger_Teleporter_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > triggerTeleporterSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

public:
    static_assert( is_parent_of<Trigger_Teleporter>( get_typeid<Trigger>() ) );
    static ToolTip< Trigger_Teleporter, Trigger > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_TRIGGER_TELEPORTER_H_
