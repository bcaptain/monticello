// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./entityInfo.h"
#include "../game.h"
#include "../entities/entity.h"

EntityInfo::EntityInfo( const char* _classname )
    : entSets()
    , entVals()
    , entMembers()
    , entname()
    , type( get_typeid( _classname ) )
{ }

EntityInfo::EntityInfo( const uint _type )
    : entSets()
    , entVals() //toNamedSetter
    , entMembers()
    , entname()
    , type( _type )
{ }

EntityInfo::EntityInfo( const EntityInfo& other )
    : entSets( other.entSets )
    , entVals( other.entVals ) //toNamedSetter
    , entMembers( other.entMembers )
    , entname( other.entname)
    , type( other.type )
{ }

EntityInfo::EntityInfo( EntityInfo&& other_rref )
    : entSets( std::move( other_rref.entSets ) )
    , entVals( std::move( other_rref.entVals ) ) //toNamedSetter
    , entMembers( std::move( other_rref.entMembers ) )
    , entname( std::move( other_rref.entname ) )
    , type( other_rref.type )
{ }

EntityInfo& EntityInfo::operator=( const EntityInfo& other ) {
    if ( &other == this )
        return *this;

    entSets = other.entSets;
    entVals = other.entVals; //toNamedSetter
    entMembers = other.entMembers;
    entname = other.entname;
    type = other.type;

    return *this;
}

EntityInfo& EntityInfo::operator=( EntityInfo&& other_rref ) {
    if ( &other_rref == this )
        return *this;

    entSets = std::move( other_rref.entSets );
    entVals = std::move( other_rref.entVals );  //toNamedSetter:remove
    entMembers = std::move( other_rref.entMembers );
    entname = std::move( other_rref.entname );
    type = other_rref.type;

    return *this;
}

std::string EntityInfo::GetMemberValue( const std::string& key ) const {
    for ( auto const & member : entMembers )
        if ( member.first == key )
            return member.second;
            
    return {};
}

void EntityInfo::SetEntName( const std::string& _name ) {
    entname = _name;
}

uint EntityInfo::GetObjectTypeID( void ) const {
    return type;
}

std::string EntityInfo::GetEntName( void ) const {
    return entname;
}

std::string EntityInfo::GetTypeName( void ) const {
    return get_classname( type );
}

void EntityInfo::EntityInfoManager_Attorney::SetEntName( EntityInfo& entInfo, const std::string& _name ) {
    entInfo.SetEntName( _name );
}
