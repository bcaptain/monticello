// Copyright 2010-2019 Brandon Captain. You may not copy this work.

/**
    There are two ways to create entities:
        game->NewEnt( type_id/entObjToCopyFrom ) // creates brand new entities.
        entityInfoManager.Load/AndSpawn( ent_file_name/ent_info ) // creates an entity based on a .ent file.

    new entity classes must be added to types.h
**/

#ifndef SRC_ENTITIES_ENTITY_H_
#define SRC_ENTITIES_ENTITY_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"

typedef Uint32 NamedSetterT_BaseType;
typedef NamedSetterT_BaseType NamedSetterT;

constexpr const NamedSetterT INVALID_SETTER = std::numeric_limits<NamedSetterT_BaseType>::max();

/* todo:
//todoammo
Ammo stuff needs to go into a different class,
attachment models need to be dropped in favor of equippable entities,
and the entities need to define how much ammo to use for a weapon
*/

typedef Uint32 AmmoTypeT_BaseType;
enum class AmmoTypeT : AmmoTypeT_BaseType {
    Gas = 0
    , NUM_AMMO_TYPES
    , OutOfBounds = NUM_AMMO_TYPES
    , Invalid = std::numeric_limits<AmmoTypeT_BaseType>::max()
    // NOTE: don't forget to add new ammo type names to ammoTypes
};

struct Ammo {
    struct Clip {
        uint amount;
        uint capacity;
    };
    
    static const std::array< std::pair< std::string, AmmoTypeT >, static_cast<AmmoTypeT_BaseType>(AmmoTypeT::NUM_AMMO_TYPES) > ammoTypes;
    static const std::string str_invalid;
    
    static AmmoTypeT GetAmmoTypeFromName( const std::string& ammoName ) {
        auto pair = FindByFirst_Const<std::string,AmmoTypeT>(ammoTypes,ammoName); //todo:binary search
        if ( ! pair )
            return AmmoTypeT::Invalid;
        return pair->second;
    }
    static const std::string& GetTypeName( const AmmoTypeT _type ) { // return by const ref is okay because it is static
        if ( _type == AmmoTypeT::Invalid )
            return str_invalid;
        
        return ammoTypes[static_cast<uint>(_type)].first;
    }
};

class Entity;
class CollisionReport3D;
class Bone;
class ArmatureInfo;
class Effect;
class Actor;
class Projectile;
class NavPoly;
class NavPath;
class FlowField;
struct Mix_Chunk;
class Undo_EntityDelete;

typedef void (Entity::*EntityVoidMethodPtr)( void );

typedef Uint8 AwakeStateT_BaseType;
enum class AwakeStateT : AwakeStateT_BaseType {
    Awake = 0
    , Asleep = 1
    , FallingAsleep = 2
};

typedef uint TurnDirT_BaseType;
enum TurnDirT : TurnDirT_BaseType {
    TURNING_NOT_TURNING=0
    , TURNING_LEFT=1
    , TURNING_RIGHT=2
};

typedef Uint32 EntityEffectT_BaseType;
enum ENTITY_EFFECT : EntityEffectT_BaseType {
    ENTITY_EFFECT_NONE
    , ENTITY_EFFECT_DISABLE_TURNING
    , ENTITY_EFFECT_DISABLE_MOVING
    , ENTITY_EFFECT_DISABLE_TOUCH_DAMAGE
    , ENTITY_EFFECT_MOVE_SPEED_BOOST
    , ENTITY_EFFECT_STOP_CHARGE_FORWARD
    , ENTITY_EFFECT_DISABLE_TURN_ANIM_OVERRIDE
    , ENTITY_EFFECT_DISABLE_ATTACHMENT_VISIBILITY
};

typedef Uint16 EntityFlagsT_BaseType;
enum EntityFlagsT : EntityFlagsT_BaseType {
    ENTFLAG_NONE = 0
    , ENTFLAG_SPAWNED = 1
    , ENTFLAG_INITIALIZED = 1<<1
    , ENTFLAG_DELETED = 1<<2
    , ENTFLAG_MOVED = 1<<3
    , ENTFLAG_ROTATED = 1<<4
    , ENTFLAG_SELECTED_BY_EDITOR = 1<<5
    , ENTFLAG_UNUSED = 1<<6
    , ENTFLAG_MIGRATE_VOXELS = 1<<7
};

typedef Uint8 EntityPhysicsFlagsT_BaseType;
enum EntityPhysicsFlagsT : EntityPhysicsFlagsT_BaseType { 
    PHYSFLAG_EMPTY = 0
    , PHYSFLAG_GHOST_THEM = 1
    , PHYSFLAG_GHOST_US = 1<<1
    , PHYSFLAG_NO_PHYS = 1<<2
    , PHYSFLAG_IS_ON_GROUND = 1<<3
    , PHYSFLAG_NO_GRAV = 1<<4
};

PrecomputeEntVal( fireDur ) //TODO BRANDON : obsolete in favor of persistentDamage usage?
PrecomputeEntVal( prevFire ) //TODO BRANDON : obsolete in favor of persistentDamage usage?
PrecomputeEntVal( flammable ) //TODO BRANDON : Move this to damageVuln usage?

extern const uint DEFAULT_FIRE_DURATION;
extern const Uint32 ENT_ID_NONE;
extern const Color4f BIND_LINE_SOURCE_COLOR;
extern const Color4f BIND_LINE_DEST_COLOR;
extern const Vec3f ENTITY_FACING_DIRECTION_DEFAULT;
extern const float NAV_DESTINATION_ARRIVAL_DISTANCE;
extern const uint ACTOR_MAX_MS_FOR_180_TURN;
extern const float ACTOR_DEFAULT_MOVE_SPEED;
extern const uint DEFAULT_INFEST_VALUE;
extern const float ACTOR_TURN_TRIGGER_ANGLE;
extern const float ACTOR_MIN_MOVE_SPEED; 

template< typename TYPE >
bool ContainsEnt_helper( const std::weak_ptr< TYPE >& wptr, const TYPE& obj ) {
    if ( const auto & ent = wptr.lock() )
        return ent->GetIndex() == obj.GetIndex();
    return false;
}

template< typename ITR, typename TYPE >
bool ContainsEnt( const ITR& first, const ITR& last, const TYPE& obj ) {
    return (
        std::find_if(
            first,
            last,
            std::bind( ContainsEnt_helper<TYPE>, std::placeholders::_1, std::ref( obj ) )
        ) != last
    );
}

template< typename TYPE, typename CONTAINER >
inline bool ContainsEnt( const CONTAINER& list, const TYPE& obj ) {
    return ContainsEnt( std::cbegin(list), std::cend(list), obj );
}

#include "../math/collision/collision.h"
std::shared_ptr< Entity > ClickSweep( const MatchOptsT_BaseType matchType = MatchOptsT::MATCH_NORMAL, const TypeID desired_type = TypeID_Entity );
std::shared_ptr< Entity > ClickSweep( CollisionReport3D& report, const MatchOptsT_BaseType matchType = MatchOptsT::MATCH_NORMAL );
std::shared_ptr< Entity > GlobalSweep( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive );
std::shared_ptr< Entity > GlobalTrace( const IntersectData& coldata, const Primitive& primitive, std::vector< CollisionItem >* collisions_out = nullptr );
std::shared_ptr< Entity > GlobalSweep_VerticalLine( CollisionReport3D& report, const Vec3f& point );

#include "../entities/object.h"
#include "../rendering/models/modelInfo.h"
#include "../rendering/models/animatedModel.h"
#include "../rendering/animation/animCue.h"
#include "../entities/entityInfo.h"
#include "../entityTree/entityTree.h"
#include "../entityTree/entityTreeTicket.h"
#include "../damage/damage.h"
#include "../math/bounds.h"
#include "./tooltip.h"

struct EffectBind {
    EffectBind( void );
    EffectBind( const EffectBind& other );
    EffectBind& operator=( const EffectBind& other );
    EffectBind( const std::weak_ptr< Effect >& fx, const char* boneName, AnimChannelT _channel );

    std::weak_ptr< Effect > effect;
    std::string boneName;
    AnimChannelT channel;
};

class Entity : public Object {
public:
    constexpr static TypeInfo< Entity, Entity > typeinfo{};
    virtual TypeID GetObjectTypeID( void ) const { return get_typeid<Entity>(); }
    virtual std::shared_ptr< Entity > CloneShared( void ) const { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    class GameAttorney {
        friend class Game;
    private:
        static void SetGameIndex( Entity& ent, const uint index );
        static void SetAwakeState( Entity& ent, const AwakeStateT state );
        static void SetWeakPtr( Entity& ent, const std::weak_ptr<Entity>& wptr );
    };

    class EntityTreeAttorney {
        friend class EntityTree;
    private:
        static LinkList< EntityTreeTicket >& TicketsRef( Entity& ent );
    };
    
    class DamageAttorney {
    private:
        friend class DamageInfo;
        static bool ApplyDamage( const DamageInfo& damageInfo, Entity& ent, const std::weak_ptr< Entity >& inflictor, const float percent );
    };

public:
    Entity( void );
    Entity( const Entity& other );
    ~Entity( void ) override;

    Entity& operator=( const Entity& other );
    
private:
    void Init( void ); //!< initialize an entity the first time it is spawned. Subsequent calls do nothing.
    
protected:
    virtual void Init_Pathfinding( void );
    virtual void Init_Attachments( void );

public:
    void PreloadAssets( void );
    bool IsAwake( void ) const;
    void WakeUp( void );
    void GoToSleep( void );
    
    Uint32 GetIndex( void ) const;//!< the unique ID should only be considered unique to an entity for as long as the entity exists. If it dies, another entity may be assigned the same id.
    std::string GetIdentifier( void ) const; //! non-unique string identifier (will not be blank) to help out with distinguishing objects in the editor
    void SetName( const std::string& nam ) override;
    void SetEntName( const char *def_nam );
    void SetEntName( const std::string& def_nam );
    std::string GetEntName( void ) const;

    const std::weak_ptr< const Entity > GetWeakPtr( void ) const;
    std::weak_ptr< Entity > GetWeakPtr( void );
    
    void MarkForDeletion( void );
    virtual Vec3f GetAttackDir( void ) const;
    
    void RemovePhysFlag( const EntityPhysicsFlagsT_BaseType );
    void AddPhysFlag( const EntityPhysicsFlagsT_BaseType );
    bool GetPhysFlag( const EntityPhysicsFlagsT_BaseType ) const;
    
    Vec3f GetOrigin( void ) const; //!< the center of the bounding sphere. for a bounding box, this isn't necessarily the center of it, and is usually 0,0,0
    void SetOrigin( const Vec3f & org );
    void SetOrigin( const float x, const float y, const float z ); //!< overload to SetOrigin
    void SnapOrigin( const float to_meters );
    void NudgeOrigin( const Vec3f & amt );
    
    void SetVelocity( const Vec3f& vec );
    Vec3f GetVelocity( void ) const;

    //! "Move" is instant and cannot fail. It also affects all slaves
    void MoveBy( const Vec3f & by );
    void MoveBy( const float x, const float y, const float z );
    void MoveTo( const Vec3f & org );
    void MoveTo( const float x, const float y, const float z );
    void MoveBindSlavesBy( const Vec3f &by );

    void RotateTo( const Vec3f& angles );

    Vec3f GetFacingDir( void ) const; //!< sets angle instantly
    void SetFacingDir( const Vec3f& unit_vec ); //!< sets angle instantly

    void BindToMaster( Entity& ent );
    void UnbindFromMaster( void );
    void UnbindSlaves( void );
    std::shared_ptr< Entity > GetBindMaster( void );
    std::shared_ptr< const Entity > GetBindMaster( void ) const;
    LinkList< std::weak_ptr< Entity > >& GetBindSlaveListRef( void );

    bool GetSelectedByEditor( void ) const;
    void SetSelectedByEditor( const bool whether );

    virtual void RunFrame( void ); //!< called once each frame if entity is awake
    virtual void Think( void );
    virtual void Spawn( void );
    bool IsSpawned( void ) const;
    bool IsPlayer( void ) const;

    bool HasAmmo( const AmmoTypeT _type, const uint amount ) const;
    bool HasAmmo( const std::string& ammoName, const uint amount ) const;
    
    uint GetAmmoQuantity( const AmmoTypeT _type ) const;
    uint GetAmmoQuantity( const std::string& ammoName ) const;
    
    uint GetAmmoCapacity( const AmmoTypeT _type ) const;
    uint GetAmmoCapacity( const std::string& ammoName ) const;
    
    void SetAmmoCapacity( const AmmoTypeT _type, const uint amount );
    void SetAmmoCapacity( const std::string& ammoName, const uint amount );
    
    bool RemoveAmmo( const AmmoTypeT _type, const uint amount );
    bool RemoveAmmo( const std::string& ammoName, const uint amount );
    
    bool GiveAmmo( const AmmoTypeT _type, const uint amount );
    bool GiveAmmo( const std::string& ammoName, const uint amount );

// ** Animation Behavior
public:
    virtual void Animate( void ) {}
    virtual void AnimationCompleted( const AnimStateT completedAnimState, const AnimChannelT channel ); //!< this will be called any time an animation has completed successfully
    virtual void AnimationInterrupted( const AnimStateT interruptedAnimState, const AnimChannelT channel ); //!< this will be called any time an animation has been interrupted
    bool ReadyToAnimate( void ) const;
    virtual std::size_t GetAnimIndex( [[maybe_unused]] const AnimStateT state ) const { return MAX_INT32; }
    virtual AnimStateT GetAnimStateForAttachment( const AnimStateT state ) const { return state; }
    virtual void ProcessAnimCue( const AnimCue& cue, const AnimChannelT channel );
    virtual void StopAllAnimCueEntityEffects( const AnimChannelT channel );
    virtual AnimStateT GetAnimStateFromName( const std::string& animStateName ) const;
protected:
    void ProcessAnimCue_LaunchProjectile( const AnimCue& cue, const AnimChannelT channel );
    void ProcessAnimCue_FireBullet( const AnimCue& cue, const AnimChannelT channel );
    void ProcessAnimCue_EnableTouchDamage( const AnimCue& cue, const AnimChannelT channel );
    void ProcessAnimCue_SetMoveSpeedBoost( const AnimCue& cue, const AnimChannelT channel );
    void ProcessAnimCue_RenderEffect( const AnimCue& cue, const AnimChannelT channel );
    void ProcessAnimCue_SpawnEffect( const AnimCue& cue, const AnimChannelT channel );
    void SetAnimCueEntityEffect_StopTurning( const AnimChannelT channel, const bool whether );
    void SetAnimCueEntityEffect_StopMoving( const AnimChannelT channel, const bool whether );
    void SetAnimCueEntityEffect_SetMoveSpeedBost( const AnimChannelT channel, const float to );
    void SetAnimCueEntityEffect_SetTouchDamage( const AnimChannelT channel, const char* damage_def );
    void SetAnimCueEntityEffect_ChargeForward( const AnimChannelT channel, const bool whether );
    void SetAnimCueEntityEffect_TurnAnimationOverride( const AnimChannelT channel, const bool whether );
    void SetAnimCueEntityEffect_DisableAttachmentVisiblity( const AnimChannelT channel, const bool to );
    void SetAnimCueEntityEffect_SetOpacity( const AnimCue& cue );
    void ProcessAnimCue_SpawnEntity( const AnimCue& cue );
    void ProcessAnimCue_DoSpecial1( void );
    void ProcessAnimCue_Die( void );
    void SetAnimCueEntityEffect_Stun( const AnimCue& cue, const AnimChannelT channel );
    void ProcessAnimCue_PlaySound( const AnimCue& cue ) const;
    void ProcessAnimCue_Interact( void );
    void ProcessAnimCue_UnsetSleepLevel( void );

protected:
    virtual bool GhostsThroughThem( const Entity& ent ) const;
    virtual bool GhostsThroughUs( const Entity& ent ) const;
public:
    bool GhostsThrough( const Entity& ent ) const; //!< returns whether either entity has been designated us as ghosting through ent (not whether ent ghost through us)

protected:
    virtual bool ReactsToPhysicsAgainst( const Entity& ent ) const;
public:
    bool ReactsToPhysicsTogether( const Entity& ent ) const;  //!< returns whether either entity has been designated us as not colliding through ent OR vice versa
    
public:
    virtual void BumpedIntoBy( Entity& ent );
    virtual void BumpInto( Entity& ent );
    void Bumped_DoTouchDamage( Entity& ent );
    void Bumped_DoFire( Entity& ent );

    virtual bool IsUsable( void ) const;
    void SetUsable( const bool whether );
    bool IsThrowable( void ) const;
    void SetThrowable( const bool whether );
    virtual bool UseBy( Actor* user_unused = nullptr );

    bool IsOnFire( void ) const;
    bool HasDamageType( const std::string& type ) const;
    void TrySetOnFire( const uint duration = DEFAULT_FIRE_DURATION );

    bool IsFlamable( void ) const;
    
    std::string GetTouchDamage( void ) const;
    void SetTouchDamage( const std::string& damage_def );
    
    std::string GetImpactSound( void ) const;
    void SetImpactSound( const std::string& snd_name );
    
    void SetDamageSound( const std::string& snd_name );
    std::string GetDamageSound( void ) const;
    
    bool CanSee( const Entity& entity ) const;
    float GetSquaredSightDistance( const Entity& entity ) const; // returns -1 if sight is blocked

    virtual void UpdateNavMeshOccupancy( void );
    std::vector< std::weak_ptr< NavPoly > > GetOccupiedNavPolies( void ) const { return occupied_nav_polies; }
    void DrawOccupiedNavPolies( void ) const; // for debugging
    bool HasArrivedAt_2D( const Vec3f& destination, const float arrivalDist ) const; //!< returns true if the entitiy is within arrivalDist from destionation on the x/y plane

protected:
    void ApplyRotation( void );
public:
    void TurnTo( const Entity& ent ); //!< turns angle over time
    void TurnToFaceDir( const Vec3f& unit_vec ); //!< turns angle over time
    virtual void NudgeTurn( const float nudge_angle ); //!< turns angle over time
    virtual bool CanTurn( void ) const;
    bool IsDoneTurning( void ) const;
    uint GetTurnSpeed( void ) const;
    void SetTurnSpeed( const uint to );
    TurnDirT_BaseType GetTurnDirection( void ) const;
    void SetTurnDirection( TurnDirT_BaseType dir );
    uint GetMaxTurnSpeed( void ) const;
    void SetMaxTurnSpeed( const uint to );    
    void SetRunSpeedMultiplier( const float _multiplier );
    float GetRunSpeedMultiplier( void ) const;
    
    virtual void DoVelocity( void );
    
    float GetMoveSpeed( void ) const;
    void SetMoveSpeed( const float to );
    float GetTopSpeed( void ) const;
    
    void SetMoveSpeedBoost( const float to );
    float GetMoveSpeedBoost( void ) const;
    
    bool CanMove( void ) const;

    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
    void MapSaveSetters( FileMap& saveFile, LinkList< std::string >& string_pool, const std::vector< std::string >& setters ) const;
    void MapLoadSetters( FileMap& saveFile, const std::vector< std::string >& string_pool, const std::vector< std::string >& setters );

    void LoadDefaults( const EntityInfo& info ); //!< Specificlly for use by the editor when placing an entity. loads default member values from the EntitInfo object (does not do entVals)

    void Push( Entity& ent_unused, const Vec3f& frame_vel );

    bool IsOnGround( void ) const;
    void SetOnGround( const bool on_ground );

public:
    void Activate( Actor* actor = nullptr );
protected:
    virtual void DoActivate( const std::vector< std::string >& list, Actor* actor );

public:
    bool IsGravityEnabled( void ) const;
    void SetNoGrav( const bool whether );
    bool GetNoGrav( void ) const;
    void SetGravityRatio( const float to ); //!< a value from 0 to 1 representing percentage

    uint GetNumVoxelsOccupied( void ) const;
    bool IsInVoxel( const EntityTree& voxel ) const;
    virtual void LeaveAllVoxels( void );
    virtual void DoMigration( void ); //!< migration is when the entity is either moved, or rotated with a non-sphere bounding shape
    void Migrate( void );
    virtual void DoRotated( void );
    virtual void SetMoved( void );
    virtual void SetRotated( void );
protected:
    void TryQueueMovementCallback( void );
    void TryQueueRotationCallback( void );

public:
    bool Intersects( const Primitive& primitive, const IntersectData& coldata ) const;
    bool Intersects( const Primitive& primitive, const EntityClipTypeT clipType = EntityClipTypeT::BOUNDS_ONLY ) const;
    
    bool SweepWith( CollisionReport3D& report, const Primitive& primitive, const SweepData& coldata ) const;

    virtual bool SetMemberVariable( const std::string& entry, const std::string& val ); //!< A config value is a bare value listed in a .ent file (not a gVal). config values often reflect member variables, entvals refer to values inside the object's entVals variable

    void Colorize( const Color4f& color );
    virtual void SetMarkedForUse( const bool whether );
    bool IsMarkedForUse( void ) const;
    void Uncolorize( void );
    Color4f GetColorization( void ) const;
    virtual void UpdateVBO( void ); //!< flags the VBOs to be repacked before the next draw call
    
    void DrawVBO( void ) const override final;
    virtual void DrawVBO( const float opacity ) const;
    virtual void DrawVBO( const ModelDrawInfo& drawInfo ) const;
    virtual void PackVBO( void );

    void DrawNameVBO( void ) const; //!< Draw the name of the entity
    void DrawVertexNormals( void ) const;
    void pack_and_send_name_vbo( void );
    void destroy_name_vbo( void );
    void pack_and_send_vert_normals_vbo( void );
    void destroy_vert_normals_vbo( void );

    void SetAlwaysShowName( const bool whether );
    bool AlwaysShowName( void ) const;
    std::string GetTeamName( void ) const;
    void SetTeamName( const std::string& teamName );

    std::shared_ptr< const Model > GetModel( void ) const;
    virtual bool SetModel( const std::shared_ptr< const Model >& mod );
    bool SetModel( const char* model_name );
    void SetModelOffset( const Vec3f _offset );
    Vec3f GetModelOffset( void ) const;
    
    std::shared_ptr< const Model > GetClipModel( void ) const;
    bool HasClipModel( void ) const;
    void SetClipModel( const std::shared_ptr< const Model >& to );

    virtual bool SetMaterial( const std::shared_ptr< const Material >& mtl );
    virtual bool SetMaterial( const char* mtl );
    virtual bool SetMaterialToDefault( void );
    virtual void UnsetMaterial( void );
    virtual std::shared_ptr< const Material > GetMaterial( void ) const;
    
    bool HasAnimatedModel( void ) const;
    bool HasModel( void ) const;
    Vec3f GetOffset( void ) const;
    void SetOffset( const Vec3f& offset );

    void AddAttachment( const std::string& attachment_pos, const std::string& attachment_name, const AnimChannelT_BaseType channel );
    void RemoveAttachment(  const std::string& attachment_pos, const std::string& attachment_name  );
    bool HasAttachment(  const std::string& attachment_pos, const std::string& attachment_name  ) const;
    uint GetAttachmentIndexAtPos( const std::string& attachment_position ) const;
    
public:
    void BindEffectToBone( std::weak_ptr< Effect >& effect, const char* boneName, const AnimChannelT channel );
    
    BonePos GetBonePos( const std::string& boneName, const AnimChannelT channel ) const;
    Vec3f GetBonePos( const std::string& boneName, const AnimChannelT channel, const std::string& boneSpot ) const;
    
    void RepositionBoundEffects( void );
    void DoEffects( void );
    void DoPersistentEffects( void );
    void DoEffect_DamageFeedback( void );

    std::shared_ptr< FlowField > GetFlowField( void ); //!< gets the current flowfield to this entity. This is garunteed to never return null.

    bool IsDestructible( void ) const;
    void SetDestructible( const bool whether );
    
    bool IsMovable( void ) const; //!< Whether entity can be pushed by other entities
    void SetMovable( const bool to );

    void SetUseValue( const float to );
    float GetUseValue( void ) const;

    void SetHealth( const int to );
    int GetHealth( void ) const;
    
    void SetMaxHealth( const int to );
    int GetMaxHealth( void ) const;

    void SetLastTimeUsed( const uint to );
    uint GetLastTimeUsed( void ) const;
    
    int GetInfestValue( void ) const;
    void SetInfestValue( const int to );

    int GiveHealth( const int amount ); //!< sets health up to max, returns remainder
    void RestoreHealth( void );
    
    bool IsDamaged( void ) const;
    void SetGod( const bool whether);
    
private:
    virtual bool Damage( const DamageInfo& damageInfo, const std::weak_ptr< Entity >& inflictor );
protected:
    virtual bool ApplyDamage( const DamageInfo& damageInfo, const std::weak_ptr< Entity >& inflictor_unused, const float percent=1.0f );
private:
    void DoPersistentDamage( void );
    void SetGameIndex( const uint to );
    bool HasPersistentDamage( const DamageInfo& damageInfo ) const;
    
public:
    void AddDamageVuln( const std::string& damageName, const int damage );
    void RemoveDamageVuln( const std::string& damageName );

    bool CheckDie( void ); //!< checks whether health is <= 0, and then it calls Death(). Returns what Death() returns.
    virtual bool Death( void ); //!< any pre-removal stuff, play a death animation, then it calls Kill() when finished. Returns what Kill() returns.
    virtual bool Kill( void ); //!< The final nail in the coffin: marks entity for removal from the map, spawns loot, etc. Returns whether the entity has been marked for removal.
    
    void DropBodyParts( void );

    uint DropLoot( void ) const; //!< returns how many drops
    uint DropLoot( const uint min, const uint max, const float distance_velocity, const char* lootType ) const; //!< returns how many drops

    std::shared_ptr<Projectile> LaunchProjectile( const AnimCue& cue, const AnimChannelT channel );
    
    std::vector< EntityTree* > GetOccupiedVoxels( void ) const;

    float GetAnimSpeed( void ) const;
    void SetAnimSpeed( const float _speed );
    
private:
    void DrawBindLine( void ) const;
    EntityClipTypeT GetClipType( MatchOptsT_BaseType opts ) const;

protected:
    ModelInfo modelInfo;
    std::shared_ptr< FlowField > flowField;
private:
    LinkList< EntityTreeTicket > tickets; //!< voxels which this ent occupies
protected:
    LinkList< std::weak_ptr< Entity > > bindSlaves;
public:
    Dict entVals; //!< general use values for whatever we might need in game. these will automatically show up in editor (select mode)
    std::array<Ammo::Clip,static_cast<AmmoTypeT_BaseType>( AmmoTypeT::NUM_AMMO_TYPES )> ammo;
    std::vector< PersistentEvent< DamageEvent > > persistentDamages;
    std::vector< PersistentEvent< EffectEvent > > persistentEffects;
protected:
    std::shared_ptr< DamageVuln > damageVuln;
private:
    std::weak_ptr< Entity > my_ptr; 
    uint game_index;
    std::string touchDamage;
    std::string snd_impact;
    std::string snd_damage;
    protected:
    float moveSpeed;
    float moveSpeedBoost;
    float useValue;
    int health;
    int maxHealth;
    int infestValue;
    uint lastTimeUsed;
private:
    
    float animSpeed;
    float runSpeedMultiplier;
    SETTER_GETTER_BYVAL( Gravity, float, gravity );
    SETTER_GETTER_BYVAL( SightAngle, float, sightAngle );
    SETTER_GETTER_BYVAL( SightDistance, float, sightDistance );
    SETTER_GETTER_BYVAL( SightOffset, float, sightOffset );
    SETTER_GETTER_BYVAL( Opacity, float, ent_opacity );
    SETTER_GETTER_BYVAL( Friction, float, friction );
    SETTER_GETTER_BYVAL( LastDamagePercentOfHealth, float, lastDamagePercentOfHealth );
    SETTER_GETTER_BYVAL( TouchDamageCooldown, uint, touchDamageCooldown );
    SETTER_GETTER_BYVAL( PrevAttack1, uint, prevAttack1 );
    SETTER_GETTER_BYVAL( PrevAttack2, uint, prevAttack2 );
    SETTER_GETTER_BYVAL( TimeDamaged, uint, timeDamaged );
    SETTER_GETTER_BYVAL( PrevCostField, uint, prevCostField );
    uint turnSpeed;
    uint maxTurnSpeed;
    SETTER_GETTER_BYVAL( GiveHealth, int, giveHealth );
    SETTER_GETTER_BYVAL( DrawBounds, bool, drawBounds );
    SETTER_GETTER_BYVAL( NoAnimPause, bool, noAnimPause );
    SETTER_GETTER_BYVAL( NoTurn, bool, noTurn );
    SETTER_GETTER_BYVAL( NoMove, bool, noMove );
    SETTER_GETTER_BYVAL( CanMoveWhileTurning, bool, moveWhileTurning );
    SETTER_GETTER_BYVAL( NoCulling, bool, noCulling );
    SETTER_GETTER_BYVAL( ChargeForward, bool, chargeForward );
    SETTER_GETTER_BYVAL( ByPlayer, bool, byPlayer ); // for triggers, pickup_item
    SETTER_GETTER_BYVAL( ClipBoundsOnly, bool, clipBoundsOnly );
    SETTER_GETTER_BYVAL( GhostUs, bool, ghostUs );
    SETTER_GETTER_BYVAL( GhostThem, bool, ghostThem );
    SETTER_GETTER_BYVAL( OkayToDie, bool, okayToDie );
    SETTER_GETTER_BYVAL( TurnAnimOverride, bool, turnAnimOverride );
    SETTER_GETTER_BYVAL( Infested, bool, infested );
    SETTER_GETTER_BYVAL( SpawnTime, uint, spawnTime );
    bool movable;
    bool showName;
    bool destructible;
    SETTER_GETTER_BYVAL( Killed, bool, killed );
    bool markedForUse;
    void SetNoPhys( const bool whether );
    bool GetNoPhys( void ) const;
    
protected:
    bool usable;
    bool throwable;
protected:
    std::vector< std::weak_ptr< NavPoly > > occupied_nav_polies;
    std::vector< std::vector< ENTITY_EFFECT > > animCueEntityEffects;
    std::vector< EntityVoidMethodPtr > runComponents;
private:
    std::vector< EffectBind > boundEffects;
public:
    EntBounds bounds;
protected:
    std::string entname;
    std::string team; // for actor class
    std::weak_ptr< Entity > bindMaster;
    Color4f* colorization;
private:
    TurnDirT_BaseType turnDirection;
protected:
    Vec3f rotate_to;
    VBO<float>* vbo_name;
    VBO<float>* vbo_name_uv;
    VBO<float>* vbo_vert_normals;
protected:
    EntityFlagsT_BaseType entity_flags;
    EntityPhysicsFlagsT_BaseType phys_flags;
    AwakeStateT awake_state;

// ** Named Setters
private:
    static const uint Entity_SETTERS_START = 0;
protected:
    enum EntitySetterT : NamedSetterT_BaseType {
        Entity_SetTouchDamage = Entity_SETTERS_START
        , Entity_SetMoveSpeedBoost
        , Entity_SetHealth
        , Entity_SetMaxHealth
        , Entity_SetLastTimeUsed
        , Entity_SetInfestValue
        , Entity_SetRunSpeedMultiplier
        , Entity_SetTouchDamageCooldown
        , Entity_SetPrevAttack1
        , Entity_SetPrevAttack2
        , Entity_SetTimeDamaged
        , Entity_SetPrevCostField
        , Entity_SetTurnSpeed
        , Entity_SetMaxTurnSpeed
        , Entity_SetGiveHealth
        , Entity_SetDrawBounds
        , Entity_SetNoAnimPause
        , Entity_SetNoTurn
        , Entity_SetNoMove
        , Entity_SetCanMoveWhileTurning
        , Entity_SetNoCulling
        , Entity_SetGravity
        , Entity_SetSightAngle
        , Entity_SetSightDistance
        , Entity_SetSightOffset
        , Entity_SetOpacity
        , Entity_SetFriction
        , Entity_SetUseValue
        , Entity_SetLastDamagePercentOfHealth
        , Entity_SetUsable
        , Entity_SetThrowable
        , Entity_SetAlwaysShowName
        , Entity_SetDestructible
        , Entity_SetKilled
        , Entity_SetMovable
        , Entity_SetTeamName
        , Entity_SetByPlayer
        , Entity_SetMarkedForUse
        , Entity_SetClipBoundsOnly
        , Entity_SetGhostUs
        , Entity_SetGhostThem
        , Entity_SetMoveSpeed
        , Entity_SetChargeForward
        , Entity_SetNoGrav
        , Entity_SetOkayToDie
        , Entity_SetTurnAnimOverride
        , Entity_SetDamageSound
        , Entity_SetInfested
        , Entity_SetSpawnTime
        , Entity_SETTERS_END
        , Entity_NUM_SETTERS = Entity_SETTERS_END - Entity_SETTERS_START
        , Entity_INVALID_SETTER = INVALID_SETTER
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

public:
    static NamedSetterT GetSetterIndex( const std::string& key );
    static NamedSetterT GetSetterIndexIn( const std::vector< std::string >& entitySetters, const std::string& key );
    virtual bool Set( const NamedSetterT_BaseType setter, const std::string& value );
    virtual std::string Get( const NamedSetterT_BaseType setter ) const;
    static void InitSetterNames( void );
    
private:
    static std::vector< std::string > entitySetters;
    
protected:
    template< class CHILD, class PARENT >
    static void CheckSetterNames( const std::vector< std::string >& setters );

// ** ToolTips
public:
    static ToolTip< Entity, Entity > editor_tooltips;
    virtual void AddClassToolTip( const char* key, const char* val ) const { editor_tooltips.Add( key, val ); }
    virtual const std::string GetClassToolTip( const char* key ) const { return editor_tooltips.Get( key ); }
};

template< class CHILD, class PARENT >
void Entity::CheckSetterNames( const std::vector< std::string >& setters ) {
    static_assert( is_parent_of<CHILD>( get_typeid<PARENT>() ) );
    
    for ( auto & str : setters ) {
        ASSERT_MSG(!str.empty(), "There are undefined setters.");
        if ( str.empty() )
            continue;
        
        std::string assert_msg = "Overloading namedSetters is not allowed: overloading ";
        assert_msg += get_classname< PARENT >();
        assert_msg += "::";
        assert_msg += str;
        assert_msg += " by class ";
        assert_msg += get_classname<CHILD>();
        assert_msg += "\n";
        ASSERT_MSG( PARENT::GetSetterIndex( str ) == INVALID_SETTER, assert_msg );
    }
    
    using PARENTS_PARENT = typename std::remove_pointer< decltype( PARENT::typeinfo.get_base_type() ) >::type;
    
    if ( get_typeid< PARENT >() == get_typeid< PARENTS_PARENT >() )
        return;
        
    CheckSetterNames<PARENT, PARENTS_PARENT >(setters);
}

template<>
inline void Entity::CheckSetterNames<Entity, Entity>( [[maybe_unused]] const std::vector< std::string >& setters ) {
    return; // breaks the recursion
}

#endif  // SRC_ENTITIES_ENTITY_H_
