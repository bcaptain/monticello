// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "bugNest.h"
#include "../game.h"
#include "./entityInfoManager.h"
#include "./characters/forager.h"

const char* DEFAULT_BUG_ENTITY = "forager";

std::vector< std::string > BugNest::bugNestSetters;

void BugNest::InitSetterNames( void ) {
    if ( bugNestSetters.size() == BugNest_NUM_SETTERS )
        return; // already set
    bugNestSetters.resize(BugNest_NUM_SETTERS);
    
    const uint i = BugNest_SETTERS_START;
    bugNestSetters[BugNest_SetSpawnTimeDelay-i] = "spawnTimeDelay";
    bugNestSetters[BugNest_SetLastSpawnTime-i] = "lastSpawnTime";
    bugNestSetters[BugNest_SetBugsInNest-i] = "bugsInNest";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<BugNest,Entity>(bugNestSetters);
}

bool BugNest::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<BugNestSetterT>(setter) ) {
        case BugNest_SetSpawnTimeDelay: SetSpawnTimeDelay( String::ToUInt(value) ); return true;
        case BugNest_SetLastSpawnTime: SetLastSpawnTime( String::ToUInt(value) ); return true;
        case BugNest_SetBugsInNest: SetBugsInNest( String::ToUInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case BugNest_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case BugNest_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<BugNest>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string BugNest::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<BugNestSetterT>(setter) ) {
        case BugNest_SetSpawnTimeDelay: return String::ToString(GetSpawnTimeDelay());
        case BugNest_SetLastSpawnTime: return String::ToString(GetLastSpawnTime());
        case BugNest_SetBugsInNest: return String::ToString(GetBugsInNest());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case BugNest_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case BugNest_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<BugNest>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT BugNest::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( bugNestSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + BugNest_SETTERS_START;
        
    static_assert( is_parent_of<BugNest>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

BugNest::BugNest( void )
    : Entity()
    , spawnTimeDelay()
    , lastSpawnTime()
    , bugsInNest()
    {
}

BugNest::BugNest( const BugNest& other ) 
    : Entity( other )
    , spawnTimeDelay(other.spawnTimeDelay)
    , lastSpawnTime(other.lastSpawnTime)
    , bugsInNest(other.bugsInNest)
    {
}

BugNest& BugNest::operator=( const BugNest& other ) {
    Entity::operator=( other );
    
    spawnTimeDelay = other.spawnTimeDelay;
    lastSpawnTime = other.lastSpawnTime;
    bugsInNest = other.bugsInNest;
    
    return *this;
}

BugNest::~BugNest( void ) {
}

void BugNest::Think( void ) {
    
    if ( CheckBugSpawnTimer() )
        SpawnBug();
        
    Entity::Think();
}

void BugNest::Spawn( void ) {
    Entity::Spawn();
        
    SpawnAllBugs();
    
    WakeUp();
    return;
}

void BugNest::SpawnAllBugs( void ) {    
    if ( bugsInNest < 1 )
        return;
        
    while ( bugsInNest ) {
        SpawnBug();
        bugsInNest -= 1;
    }
    return;
}

bool BugNest::CheckBugSpawnTimer( void ) const {
    if ( game->GetGameTime() - lastSpawnTime > spawnTimeDelay )
        return true;
        
    return false;
}

void BugNest::SpawnBug( void ) {
    if ( bugsInNest < 1 )
        return;
         
    auto ent = entityInfoManager.LoadAndSpawn( DEFAULT_BUG_ENTITY, GetOrigin() );
    
    std::shared_ptr< Forager > bug = std::static_pointer_cast< Forager >( ent );
    if ( bug ) {
        bug->SetHomeNest( GetWeakPtr() );
        float radius = bug->bounds.GetRadius();
        
        bug->SetOrigin( ( GetFacingDir().GetNormalized() * Vec3f(0,radius,0) ) + GetOrigin() );
        
        lastSpawnTime = game->GetGameTime();
        bugsInNest -= 1;
    }
}

void BugNest::SetSpawnTimeDelay( const uint to ) {
    spawnTimeDelay = to;
}

uint BugNest::GetSpawnTimeDelay( void ) const {
    return spawnTimeDelay;
}

void BugNest::SetLastSpawnTime( const uint to ) {
    lastSpawnTime = to;
}

uint BugNest::GetLastSpawnTime( void ) const {
    return lastSpawnTime;
}

void BugNest::SetBugsInNest( const uint to ) {
    bugsInNest = to;
}

uint BugNest::GetBugsInNest( void ) const {
    return bugsInNest;
}

void BugNest::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<BugNest>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, bugNestSetters );
}

void BugNest::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<BugNest>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, bugNestSetters );
}
