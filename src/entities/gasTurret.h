// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_GAS_TURRET_H
#define SRC_ENTITIES_GAS_TURRET_H

#include "./projectile.h"
#include "./actor.h"


class GasTurret : public Entity {
public:
    static constexpr TypeInfo<GasTurret, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    GasTurret( void );
    ~GasTurret( void ) override;
    GasTurret( const GasTurret& other );

public:
    GasTurret& operator=( const GasTurret& other );

public:
    void Spawn( void ) override;
    void Think( void ) override;
    void Animate( void ) override;
    
    void SetDuration( const uint _duration );
    uint GetDuration( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void LaunchGas( void );
    float GetGasLaunchRotation( void ) const;
    
    SETTER_GETTER_BYVAL( GasLaunchPeriod, uint, gasLaunchPeriod );
    SETTER_GETTER_BYVAL( TimeNextGasLaunch, uint, timeNextGasLaunch );
    SETTER_GETTER_BYVAL( NumProjectilesPerLaunch, uint, numProjectilesPerLaunch );
    SETTER_GETTER_BYVAL( GasLaunchRotZPerSecond, uint, gasLaunchRotZPerSecond );
    
private:
    uint duration;
    Vec3f dir;
    
// ** Named Setters
private:
    static const uint GasTurret_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum GasTurretSetterT : NamedSetterT_BaseType {
        GasTurret_SetGasLaunchPeriod = GasTurret_SETTERS_START
        , GasTurret_SetTimeNextGasLaunch
        , GasTurret_SetNumProjectilesPerLaunch
        , GasTurret_SetGasLaunchRotZPerSecond
        , GasTurret_SetDuration
        , GasTurret_SETTERS_END
        , GasTurret_NUM_SETTERS = GasTurret_SETTERS_END - GasTurret_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > gasTurretSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<GasTurret>( get_typeid<Entity>() ) );
    static ToolTip< GasTurret, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_GAS_TURRET_H
