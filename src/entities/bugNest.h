// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_BUGNEST_H
#define SRC_ENTITIES_BUGNEST_H

#include "../base/main.h"
#include "./entity.h"


class BugNest : public Entity {
public:
    static constexpr TypeInfo<BugNest, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
    
public:
    BugNest( void );
    ~BugNest( void ) override;
    BugNest( const BugNest& other );

public:
    BugNest& operator=( const BugNest& other );

public:
    void Spawn( void ) override;
    void Think( void ) override;
    
    void SetSpawnTimeDelay( const uint to );
    uint GetSpawnTimeDelay( void ) const;
    
    void SetLastSpawnTime( const uint to );
    uint GetLastSpawnTime( void ) const;
    
    void SetBugsInNest( const uint to );
    uint GetBugsInNest( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void SpawnBug( void );
    void SpawnAllBugs( void );
    bool CheckBugSpawnTimer( void ) const;
    
private:
    uint spawnTimeDelay;
    uint lastSpawnTime;
    uint bugsInNest;
    
// ** Named Setters
private:
    static const uint BugNest_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum BugNestSetterT : NamedSetterT_BaseType {
        BugNest_SetSpawnTimeDelay = BugNest_SETTERS_START
        , BugNest_SetLastSpawnTime
        , BugNest_SetBugsInNest
        , BugNest_SETTERS_END
        , BugNest_NUM_SETTERS = BugNest_SETTERS_END - BugNest_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > bugNestSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<BugNest>( get_typeid<Entity>() ) );
    static ToolTip< BugNest, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_BUGNEST_H
