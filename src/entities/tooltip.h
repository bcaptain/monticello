// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_TOOLTIP_H_
#define SRC_ENTITIES_TOOLTIP_H_

#include "../base/main.h"

// A polymorphic set of tooltips for Entity class types.
// ENTITY_CLASS refers to the entity class type that is instantiating this class.
// ENTITY_CLASS and SUPER_CLASS must both have a public static ToolTip of the name "tooltip"
// Super class must set it's SUPER_CLASS to the same as it's ENTITY_CLASS

template< typename ENTITY_CLASS, typename SUPER_CLASS >
struct ToolTip {
    static void Add( const char* key, const char* val );
    static const std::string Get( const char* key );
    static bool HasParent( void );
    static std::vector< std::pair< std::string, std::string > > tips;
private:
    static uint FindIndex( const char* key );
};

template< typename ENTITY_CLASS, typename SUPER_CLASS >
std::vector< std::pair< std::string, std::string > > ToolTip<ENTITY_CLASS,SUPER_CLASS>::tips;
 
template< typename ENTITY_CLASS, typename SUPER_CLASS >
bool ToolTip<ENTITY_CLASS,SUPER_CLASS>::HasParent( void ) {
    return &SUPER_CLASS::editor_tooltips.tips != &ENTITY_CLASS::editor_tooltips.tips;
}

template< typename ENTITY_CLASS, typename SUPER_CLASS >
uint ToolTip<ENTITY_CLASS,SUPER_CLASS>::FindIndex( const char* key ) {
    for ( uint i=0; i<tips.size(); ++i )
        if ( tips[i].first == key )
            return i;
    
    return tips.size();
}

template< typename ENTITY_CLASS, typename SUPER_CLASS >
void ToolTip<ENTITY_CLASS,SUPER_CLASS>::Add( const char* key, const char* val ) {
    const uint i = FindIndex( key );
    if ( i < tips.size() ) {
        tips[i].second = val;
    } else {
        tips.emplace_back(key,val);
    }
}

template< typename ENTITY_CLASS, typename SUPER_CLASS >
const std::string ToolTip<ENTITY_CLASS,SUPER_CLASS>::Get( const char* key ) {
    const uint i = FindIndex(key);
    if ( i < tips.size() )
        return tips[i].second;
    
    if ( HasParent() )
        SUPER_CLASS::editor_tooltips.Get( key );
    
    return "";
}

#endif  // SRC_ENTITIES_TOOLTIP_H_
