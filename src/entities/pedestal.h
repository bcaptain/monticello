// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_PEDESTAL_H
#define SRC_ENTITIES_PEDESTAL_H

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./actor.h"
#include "../sprites/meter.h"


class Pedestal : public Entity {
public:
    static constexpr TypeInfo<Pedestal, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Pedestal( void );
    ~Pedestal( void ) override;
    Pedestal( const Pedestal& other );

public:
    Pedestal& operator=( const Pedestal& other );
    
public:
    void Think( void ) override;
    void Animate( void ) override;
    
    void SetMarkedForUse( const bool whether ) override;
    bool UseBy( Actor* user = nullptr ) override;
    
    bool IsUsable( void ) const override;
    bool IsUsableBy( const Actor* user ) const;

    void Spawn( void ) override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    bool IsWobbling( void ) const;
    
    void StartWobble( void );
    void SteadyWobble( void );
    
    void UpdateSteadyValue( void );
    void SpawnSteadyUI( void );
    void UpdateSteadyUIValue();
    
    void DropValuable( void );
    
    void SetSteadyValue( const int to );
    int GetSteadyValue( void ) const;
    float GetSteadyValue_Normalized( void ) const;
    
private:
    std::shared_ptr< Meter > useMeter;
    int steadyValue;
    
// ** Named Setters
private:
    static const uint Pedestal_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum PedestalSetterT : NamedSetterT_BaseType {
        Pedestal_SetSteadyValue = Pedestal_SETTERS_START
        , Pedestal_SETTERS_END
        , Pedestal_NUM_SETTERS = Pedestal_SETTERS_END - Pedestal_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > pedestalSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Pedestal>( get_typeid<Entity>() ) );
    static ToolTip< Pedestal, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_PEDESTAL_H
