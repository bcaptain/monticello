// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./grenade.h"

#include "../../game.h"
#include "../../rendering/renderer.h" //debug only
#include "../actor.h" //TODO : Included this just for the AnimStateT defines...

void Grenade::Hit( Entity& collision ) {
    if ( ReactsToPhysicsAgainst(collision) ) // todo: this code should be put into Grenade::BumpedIntoBy() and BumpInto(), which will call Explode()
        Explode();
}

bool Grenade::ReactsToPhysicsAgainst( const Entity& ent ) const {
    if ( derives_from< Grenade >( ent ) )
        return true;

    return Projectile::ReactsToPhysicsAgainst( ent );
}
