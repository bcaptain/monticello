// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_PROJECTILES_GASBOMB_H
#define SRC_ENTITIES_PROJECTILES_GASBOMB_H

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../projectile.h"
#include "../actor.h" //TODO : Included this just for the AnimStateT defines...


class Gasbomb : public Projectile {
public:
    static constexpr TypeInfo<Gasbomb, Projectile> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

protected:
    void Hit( Entity& entity ) override;
    void SpawnGasCloud( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Gasbomb>( get_typeid<Projectile>() ) );
    static ToolTip< Gasbomb, Projectile > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif // SRC_ENTITIES_PROJECTILES_GASBOMB_H
