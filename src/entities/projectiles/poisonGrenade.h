// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_POISONGRENADE_H
#define SRC_ENTITIES_POISONGRENADE_H

#include "../projectile.h"
#include "../actor.h"


class PoisonGrenade : public Projectile {
public:
    static constexpr TypeInfo<PoisonGrenade, Projectile> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    PoisonGrenade( void );
    ~PoisonGrenade( void ) override;
    PoisonGrenade( const PoisonGrenade& other );

public:
    PoisonGrenade& operator=( const PoisonGrenade& other );
    
public:
    void Spawn( void ) override;
    void Think( void ) override;
    void Animate( void ) override;
    
    void SetGasLaunchPeriod( const uint to );
    uint GetGasLaunchPeriod( void ) const;
    
    void SetTimeNextGasLaunch( const uint to );
    uint GetTimeNextGasLaunch( void ) const;
    
    void SetNumProjectiles( const uint to );
    uint GetNumProjectiles( void ) const;
    
    void SetEffectRotationDegPerSecond( const uint to );
    uint GetEffectRotationDegPerSecond( void ) const;   
 
    void SetGasLaunchRange( const float to );
    float GetGasLaunchRange( void ) const; 
    
    void SetProjectileArcOffsetActive( const bool whether );
    bool GetProjectileArcOffsetActive( void ) const;
    
    void SetProjectileName( const std::string& proj_name );
    std::string GetProjectileName( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void LaunchGas( void );
    float GetEffectRotation( void ) const;
    
private:
    Vec3f launchPoint;
    std::string projectileName;
    float gasLaunchRange;
    uint gasLaunchPeriod;
    uint timeNextGasLaunch;
    uint numProjectiles;
    uint effectRotationDegPerSecond;
    bool projectileArcOffsetActive;
    
// ** Named Setters
private:
    static const uint PoisonGrenade_SETTERS_START = Projectile_NUM_SETTERS;
protected:
    enum PoisonGrenadeSetterT : NamedSetterT_BaseType {
        PoisonGrenade_SetGasLaunchPeriod = PoisonGrenade_SETTERS_START
        , PoisonGrenade_SetTimeNextGasLaunch
        , PoisonGrenade_SetNumProjectiles
        , PoisonGrenade_SetEffectRotationDegPerSecond
        , PoisonGrenade_SetGasLaunchRange
        , PoisonGrenade_SetProjectileArcOffsetActive
        , PoisonGrenade_SetProjectileName
        , PoisonGrenade_SETTERS_END
        , PoisonGrenade_NUM_SETTERS = PoisonGrenade_SETTERS_END - PoisonGrenade_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > poisonGrenadeSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<PoisonGrenade>( get_typeid<Projectile>() ) );
    static ToolTip< PoisonGrenade, Projectile > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_POISONGRENADE_H
