// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./gasBomb.h"

#include "../../game.h"

void Gasbomb::Hit( Entity& ent ) {
    
    if ( !derives_from( TypeID_Flat, ent ) ) // todo: this code should be put into GasBomb::BumpedIntoBy() and BumpInto(), which will call Explode()
        SpawnGasCloud();
}

void Gasbomb::SpawnGasCloud( void ) {
    //todo
}

