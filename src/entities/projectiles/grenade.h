// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_PROJECTILES_GRENADE_H
#define SRC_ENTITIES_PROJECTILES_GRENADE_H

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../projectile.h"

class Grenade : public Projectile {
public:
    static constexpr TypeInfo<Grenade, Projectile> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

protected:
    void Hit( Entity& entity ) override;

protected:
    bool ReactsToPhysicsAgainst( const Entity& ent ) const override;
    
// ** ToolTips
public:
    static_assert( is_parent_of<Grenade>( get_typeid<Projectile>() ) );
    static ToolTip< Grenade, Projectile > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif // SRC_ENTITIES_PROJECTILES_GRENADE_H
