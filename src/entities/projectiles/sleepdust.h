// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_SLEEPDUST_H_
#define SRC_ENTITIES_SLEEPDUST_H_

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../projectile.h"

class SleepDust : public Projectile {
public:
    static constexpr TypeInfo<SleepDust, Projectile> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    SleepDust( void );
    ~SleepDust( void ) override;
    SleepDust( const SleepDust& other );

public:
    SleepDust& operator=( const SleepDust& other );

public:
    void Think( void ) override;
    void Spawn( void ) override;
    void Explode( void ) override;
    
    void SetEffectPeriod( const uint to );
    uint GetEffectPeriod( void ) const;
    
    void SetEffectAmount( const uint to );
    uint GetEffectAmount( void ) const;
    
    void SetTimeLastDustEffect( const uint to );
    uint GetTimeLastDustEffect( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
protected:
    bool ReactsToPhysicsAgainst( const Entity& ent ) const override;
    
private:
    void DoSleepDust( void );
    bool CanDoSleepDust( void ) const;
    
private:
    uint effectPeriod;
    uint effectAmount;
    uint timeLastDustEffect;

// ** Named Setters
private:
    static const uint SleepDust_SETTERS_START = Projectile_NUM_SETTERS;
protected:
    enum SleepDustSetterT : NamedSetterT_BaseType {
        SleepDust_SetEffectPeriod = SleepDust_SETTERS_START
        , SleepDust_SetEffectAmount
        , SleepDust_SetTimeLastDustEffect
        , SleepDust_SETTERS_END
        , SleepDust_NUM_SETTERS = SleepDust_SETTERS_END - SleepDust_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > sleepDustSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<SleepDust>( get_typeid<Projectile>() ) );
    static ToolTip< SleepDust, Projectile > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_SLEEPDUST_H_

