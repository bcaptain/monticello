// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./poisonGrenade.h"
#include "../entityInfoManager.h"
#include "../../game.h"
#include "../../rendering/animation/animInfo.h"

std::vector< std::string > PoisonGrenade::poisonGrenadeSetters;

void PoisonGrenade::InitSetterNames( void ) {
    if ( poisonGrenadeSetters.size() == PoisonGrenade_NUM_SETTERS )
        return; // already set
    poisonGrenadeSetters.resize(PoisonGrenade_NUM_SETTERS);
    
    const uint i = PoisonGrenade_SETTERS_START;
    poisonGrenadeSetters[PoisonGrenade_SetGasLaunchPeriod-i] = "gasLaunchPeriod";
    poisonGrenadeSetters[PoisonGrenade_SetTimeNextGasLaunch-i] = "timeNextGasLaunch";
    poisonGrenadeSetters[PoisonGrenade_SetNumProjectiles-i] = "numProjectiles";
    poisonGrenadeSetters[PoisonGrenade_SetEffectRotationDegPerSecond-i] = "effectRotationDegPerSecond";
    poisonGrenadeSetters[PoisonGrenade_SetGasLaunchRange-i] = "gasLaunchRange";
    poisonGrenadeSetters[PoisonGrenade_SetProjectileArcOffsetActive-i] = "projectileArcOffsetActive";
    poisonGrenadeSetters[PoisonGrenade_SetProjectileName-i] = "projectileName";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<PoisonGrenade,Projectile>(poisonGrenadeSetters);
}

bool PoisonGrenade::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<PoisonGrenadeSetterT>(setter) ) {
        case PoisonGrenade_SetGasLaunchPeriod: SetGasLaunchPeriod( String::ToUInt( value ) ); return true;
        case PoisonGrenade_SetTimeNextGasLaunch: SetTimeNextGasLaunch( String::ToUInt( value ) ); return true;
        case PoisonGrenade_SetNumProjectiles: SetNumProjectiles( String::ToUInt( value ) ); return true;
        case PoisonGrenade_SetEffectRotationDegPerSecond: SetEffectRotationDegPerSecond( String::ToUInt( value ) ); return true;
        case PoisonGrenade_SetGasLaunchRange: SetGasLaunchRange( String::ToFloat( value ) ); return true;
        case PoisonGrenade_SetProjectileArcOffsetActive: SetProjectileArcOffsetActive( String::ToBool( value ) ); return true;
        case PoisonGrenade_SetProjectileName: SetProjectileName( value ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case PoisonGrenade_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case PoisonGrenade_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<PoisonGrenade>( get_typeid<Projectile>() ) );
            return Projectile::Set( setter, value );
    } 
}
std::string PoisonGrenade::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<PoisonGrenadeSetterT>(setter) ) {
        case PoisonGrenade_SetGasLaunchPeriod: return String::ToString(GetGasLaunchPeriod());
        case PoisonGrenade_SetTimeNextGasLaunch: return String::ToString(GetTimeNextGasLaunch());
        case PoisonGrenade_SetNumProjectiles: return String::ToString(GetNumProjectiles());
        case PoisonGrenade_SetEffectRotationDegPerSecond: return String::ToString(GetEffectRotationDegPerSecond());
        case PoisonGrenade_SetGasLaunchRange: return String::ToString(GetGasLaunchRange());
        case PoisonGrenade_SetProjectileArcOffsetActive: return String::ToString(GetProjectileArcOffsetActive());
        case PoisonGrenade_SetProjectileName: return GetProjectileName();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case PoisonGrenade_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case PoisonGrenade_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<PoisonGrenade>( get_typeid<Projectile>() ) );
            return Projectile::Get( setter );
    } 
}

NamedSetterT PoisonGrenade::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( poisonGrenadeSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + PoisonGrenade_SETTERS_START;
    
    static_assert( is_parent_of<PoisonGrenade>( get_typeid<Projectile>() ) );
    return Projectile::GetSetterIndex( key );
}

PoisonGrenade::PoisonGrenade( void )
    : Projectile()
    , launchPoint()
    , projectileName()
    , gasLaunchRange()
    , gasLaunchPeriod()
    , timeNextGasLaunch()
    , numProjectiles()
    , effectRotationDegPerSecond()
    , projectileArcOffsetActive()
    {
}

PoisonGrenade::PoisonGrenade( const PoisonGrenade& other ) 
    : Projectile( other )
    , launchPoint(other.launchPoint)
    , projectileName(other.projectileName)
    , gasLaunchRange(other.gasLaunchRange)
    , gasLaunchPeriod(other.gasLaunchPeriod)
    , timeNextGasLaunch(other.timeNextGasLaunch)
    , numProjectiles(other.numProjectiles)
    , effectRotationDegPerSecond(other.effectRotationDegPerSecond)
    , projectileArcOffsetActive(other.projectileArcOffsetActive)
    {
}

PoisonGrenade& PoisonGrenade::operator=( const PoisonGrenade& other ) {
    Projectile::operator=( other );
    launchPoint = other.launchPoint;
    projectileName = other.projectileName;
    gasLaunchRange = other.gasLaunchRange;
    gasLaunchPeriod = other.gasLaunchPeriod;
    timeNextGasLaunch = other.timeNextGasLaunch;
    numProjectiles = other.numProjectiles;
    effectRotationDegPerSecond = other.effectRotationDegPerSecond;
    projectileArcOffsetActive = other.projectileArcOffsetActive;
    
    return *this;
}

PoisonGrenade::~PoisonGrenade( void ) {
}

void PoisonGrenade::Spawn( void ) {
    Projectile::Spawn();
    
    timeNextGasLaunch = game->GetGameTime() + gasLaunchPeriod;

    const float angVel_that_feels_right = Random::Float(300,500);
    const float angFriction_that_feels_right = Random::Float(200,250);
    const Vec3f rot_direction( 1.0f, 0.0f, 0.0f );
    bounds.SetAngularVelocity( rot_direction * angVel_that_feels_right );
    bounds.SetAngularFriction( angFriction_that_feels_right );
}

void PoisonGrenade::Think( void ) {
    if ( timeNextGasLaunch <= game->GetGameTime() ){
        LaunchGas();
        timeNextGasLaunch = game->GetGameTime() + gasLaunchPeriod;
        projectileArcOffsetActive = !projectileArcOffsetActive;
    }
    
    Projectile::Think();
}

void PoisonGrenade::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

void PoisonGrenade::LaunchGas( void ) {
    
    const uint rot_change = 360 / numProjectiles;
    const float start_rot = GetEffectRotation();
    
    Mat3f rot;
    rot.Rotate( start_rot, 0,0,1 );
        
    for ( uint i = 0; i < numProjectiles; i++ ) {
        
        auto ent = entityInfoManager.LoadAndSpawn( projectileName.c_str() );
        std::shared_ptr< Projectile > proj = std::static_pointer_cast< Projectile >( ent );
        if ( ! proj )
            continue;
            
        proj->SetOwner( GetWeakPtr() );
        proj->SetOrigin( GetOrigin() );
        
        Vec3f attack_dir = dir;
        
        rot.Rotate( rot_change * i, 0,0,1 );
        
        attack_dir *= rot;
    
        proj->SetDir( attack_dir );
        proj->Launch();
    }
    
}

float PoisonGrenade::GetEffectRotation( void ) const {
    
    const float timeAlive_seconds = ( game->GetGameTime() - bornTime ) / 1000.0f;
    const float degree_rotation_total = static_cast<float>(timeAlive_seconds) * effectRotationDegPerSecond;
    const float degree_rotation_normalized = fmodf( degree_rotation_total, 360 );
    
    return degree_rotation_normalized;
}

void PoisonGrenade::SetGasLaunchPeriod( const uint to ) {
    gasLaunchPeriod = to;
}

uint PoisonGrenade::GetGasLaunchPeriod( void ) const {
    return gasLaunchPeriod;
}

void PoisonGrenade::SetTimeNextGasLaunch( const uint to ) {
    timeNextGasLaunch = to;
}

uint PoisonGrenade::GetTimeNextGasLaunch( void ) const {
    return timeNextGasLaunch;
}

void PoisonGrenade::SetNumProjectiles( const uint to ) {
    numProjectiles = to;
}

uint PoisonGrenade::GetNumProjectiles( void ) const {
    return numProjectiles;
}

void PoisonGrenade::SetEffectRotationDegPerSecond( const uint to ) {
    effectRotationDegPerSecond = to;
}

uint PoisonGrenade::GetEffectRotationDegPerSecond( void ) const {
    return effectRotationDegPerSecond;
}

void PoisonGrenade::SetGasLaunchRange( const float to ) {
    gasLaunchRange = to;
}

float PoisonGrenade::GetGasLaunchRange( void ) const {
    return gasLaunchRange;
}
    
void PoisonGrenade::SetProjectileArcOffsetActive( const bool whether ) {
    projectileArcOffsetActive = whether;
}

bool PoisonGrenade::GetProjectileArcOffsetActive( void ) const {
    return projectileArcOffsetActive;
}

void PoisonGrenade::SetProjectileName( const std::string& proj_name ) {
    projectileName = proj_name;
}

std::string PoisonGrenade::GetProjectileName( void ) const {
    return projectileName;
}

void PoisonGrenade::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Projectile::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<PoisonGrenade>( get_typeid<Projectile>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, poisonGrenadeSetters );
}

void PoisonGrenade::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Projectile::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<PoisonGrenade>( get_typeid<Projectile>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, poisonGrenadeSetters );
}
