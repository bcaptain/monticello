// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "sleepdust.h"
#include "../../game.h"
#include "../actor.h"

std::vector< std::string > SleepDust::sleepDustSetters;

void SleepDust::InitSetterNames( void ) {
    if ( sleepDustSetters.size() == SleepDust_NUM_SETTERS )
        return; // already set
    sleepDustSetters.resize(SleepDust_NUM_SETTERS);
    
    const uint i = SleepDust_SETTERS_START;
    sleepDustSetters[SleepDust_SetEffectPeriod-i] = "effectPeriod";
    sleepDustSetters[SleepDust_SetEffectAmount-i] = "effectAmount";
    sleepDustSetters[SleepDust_SetTimeLastDustEffect-i] = "timeLastDustEffect";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<SleepDust,Projectile>(sleepDustSetters);
}

bool SleepDust::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<SleepDustSetterT>(setter) ) {
        case SleepDust_SetEffectPeriod: SetEffectPeriod( String::ToUInt(value) ); return true;
        case SleepDust_SetEffectAmount: SetEffectAmount( String::ToUInt(value) ); return true;
        case SleepDust_SetTimeLastDustEffect: SetTimeLastDustEffect( String::ToUInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case SleepDust_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case SleepDust_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<SleepDust>( get_typeid<Projectile>() ) );
            return Projectile::Set( setter, value );
    } 
}

std::string SleepDust::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<SleepDustSetterT>(setter) ) {
        case SleepDust_SetEffectPeriod: return String::ToString(GetEffectPeriod());
        case SleepDust_SetEffectAmount: return String::ToString(GetEffectAmount());
        case SleepDust_SetTimeLastDustEffect: return String::ToString(GetTimeLastDustEffect());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case SleepDust_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case SleepDust_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<SleepDust>( get_typeid<Projectile>() ) );
            return Projectile::Get( setter );
    } 
}

NamedSetterT SleepDust::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( sleepDustSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + SleepDust_SETTERS_START;
        
    static_assert( is_parent_of<SleepDust>( get_typeid<Projectile>() ) );
    return Projectile::GetSetterIndex( key );
}

SleepDust::SleepDust( void )
    : Projectile()
    , effectPeriod()
    , effectAmount()
    , timeLastDustEffect()
{ }

SleepDust::~SleepDust( void ) {
}

SleepDust::SleepDust( const SleepDust& other )
    : Projectile( other )
    , effectPeriod(other.effectPeriod)
    , effectAmount(other.effectAmount)
    , timeLastDustEffect(other.timeLastDustEffect)
{ }

void SleepDust::Spawn( void ) {
    Projectile::Spawn();
    Entity::Spawn();
    
    SetTarget( game->GetPlayerEntity() );
}

void SleepDust::Think( void ) {
    
    if ( CanDoSleepDust() )
        DoSleepDust();
        
    Projectile::Think();
}

bool SleepDust::ReactsToPhysicsAgainst( const Entity& ent ) const {
    
    if ( IsSameTeamAsOwner( &ent ) ) //todo:damageteams
        return true;
    
    if ( game->IsPlayer( ent ) ) //todo:damageteams
        return true;
        
    if ( derives_from( TypeID_Flat, ent ) )
        return true;
    
    return Projectile::ReactsToPhysicsAgainst( ent );
}

void SleepDust::Explode( void ) {
    return; //Do Nothing
}

void SleepDust::DoSleepDust( void ) {
    
    //Check player in range
    const std::shared_ptr< Actor > player = game->GetPlayer();
    
    if ( !player )
        return;
    
    if ( player->IsAsleep() )
        return;
    
    const float my_radius = bounds.GetRadius();
    const float player_radius = player->bounds.GetRadius();
    const Vec3f my_origin = GetOrigin();
    const Vec3f player_origin = player->GetOrigin();
    const float dist = ( player_origin - my_origin ).Len() - my_radius - player_radius;

    if ( dist <= 0 ) {
        player->AddSleepiness( effectAmount );       
        
        timeLastDustEffect = game->GetGameTime();
    }
    
}

bool SleepDust::CanDoSleepDust( void ) const {
    return game->GetGameTime() - timeLastDustEffect >= effectPeriod;
}

void SleepDust::SetEffectPeriod( const uint to ) {
    effectPeriod = to;
}

uint SleepDust::GetEffectPeriod( void ) const {
    return effectPeriod;
}

void SleepDust::SetEffectAmount( const uint to ) {
    effectAmount = to;
}

uint SleepDust::GetEffectAmount( void ) const {
    return effectAmount;
}

void SleepDust::SetTimeLastDustEffect( const uint to ) {
    timeLastDustEffect = to;
}

uint SleepDust::GetTimeLastDustEffect( void ) const {
    return timeLastDustEffect;
}

void SleepDust::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Projectile::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<SleepDust>( get_typeid<Projectile>() ) );
    static_assert( is_child_of<Projectile>( get_typeid<SleepDust>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, sleepDustSetters );
}

void SleepDust::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Projectile::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<SleepDust>( get_typeid<Projectile>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, sleepDustSetters );
}
