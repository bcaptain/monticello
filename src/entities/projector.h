// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_PROJECTOR_H_
#define SRC_ENTITIES_PROJECTOR_H_

#include "./clib/src/warnings.h"
#include "./entity.h"

typedef int ProjectionTypeT_BaseType;
enum class ProjectionTypeT : ProjectionTypeT_BaseType {
    Ortho = 0
    , Perspective = 1
};

typedef int ProjectorTargetsT_BaseType;
enum class ProjectorTargetsT : ProjectorTargetsT_BaseType {
    Nothing = 0
    , Modules = 1
    , Actors = 1<<1
    , Everything = 1<<2
};


class Projector : public Entity {
public:
    static constexpr TypeInfo<Projector, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Projector( void );
    Projector( const Projector& other );
    ~Projector( void ) override;    

public:
    Projector& operator=( const Projector& other ); 
    
public:
    bool IsVisible( void ) const override;
    
    using Entity::DrawVBO;
    void DrawVBO( const float opacity ) const override;
    void UpdateVBO( void ) override;
    
    void SetProjectionOpacity( const float to );
    float GetProjectionOpacity( void ) const;
    
    void SetSideLength( const float to );
    float GetSideLength( void ) const;

    void SetFOV( const float to );
    float GetFOV( void ) const;

    void SetWidth( const float to );
    float GetWidth( void ) const;

    void SetHeight( const float to );
    float GetHeight( void ) const;

    void SetNearPlane( const float to );
    float GetNearPlane( void ) const;

    void SetFarPlane( const float to );
    float GetFarPlane( void ) const;

    void SetType( ProjectionTypeT& to );
    ProjectionTypeT GetType( void ) const;
    
    void Spawn( void ) override;
    void PackVBO( void ) override;
    
    bool CanTarget( const Entity& entity ) const;
    void SendShaderData( void );
    
    bool SetProjectorModelMaterial( const std::shared_ptr< const Material >& mtl );
    bool SetProjectorModelMaterial( const char* mtl );
    
    bool SetMaterial( const std::shared_ptr< const Material >& mtl ) override;
    bool SetMaterial( const char* mtl ) override;
    bool SetMaterialToDefault( void ) override;
    void UnsetMaterial( void ) override;
    std::shared_ptr< const Material > GetMaterial( void ) const override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void ComputeMVPMatrix( void );
    void ComputeProjectionMatrix( void );
    void ComputeProjectionMatrix_Ortho( void );
    void ComputeProjectionMatrix_Perspective( void );

private:
    Mat4f matMVP;
    Mat4f projMatrix;
    float projection_opacity;
    float sideLength;
    float fov;
    float width;
    float height;
    float nearPlane;
    float farPlane;
    std::unique_ptr< VBO<float> > vbo_vert;
    std::unique_ptr< VBO<float> > vbo_color;
    std::shared_ptr< const Material > projectionMtl;
    ProjectionTypeT type;
    ProjectorTargetsT targets;
    
// ** Named Setters
private:
    static const uint Projector_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum ProjectorSetterT : NamedSetterT_BaseType {
        Projector_SetProjectionOpacity = Projector_SETTERS_START
        , Projector_SetSideLength
        , Projector_SetFOV
        , Projector_SetWidth
        , Projector_SetHeight
        , Projector_SetNearPlane
        , Projector_SetFarPlane
        , Projector_SETTERS_END
        , Projector_NUM_SETTERS = Projector_SETTERS_END - Projector_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > projectorSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Projector>( get_typeid<Entity>() ) );
    static ToolTip< Projector, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_PROJECTOR_H_
