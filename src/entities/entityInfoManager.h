// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_ENTITYINFOMANAGER_H_
#define SRC_ENTITIES_ENTITYINFOMANAGER_H_

#include "../base/main.h"
#include "../files/manifest.h"
#include "../files/assetManager.h"

class EntityInfo;

/*!

EntityInfoManager \n\n

holds all the entityInfos for the game

**/

class EntityInfoManager : public AssetManager< AssetData<EntityInfo>, EntityInfo > {
public:
    EntityInfoManager( void );

public:
    // ** these functions create entities based on their .ent files
    std::shared_ptr< Entity > LoadAndSpawn( const char* name, const Vec3f& origin = Vec3f(0,0,0) );
    std::shared_ptr< Entity > LoadAndSpawn( const EntityInfo& ent_info, const Vec3f& origin = Vec3f(0,0,0) );
    std::shared_ptr< Entity > Load( const char* name, const Vec3f& origin = Vec3f(0,0,0) );
    std::shared_ptr< Entity > Load( const EntityInfo& ent_info, const Vec3f& origin = Vec3f(0,0,0) );
    std::shared_ptr< Entity > Load_Unmanaged( const char* name, const Vec3f& origin = Vec3f(0,0,0) ); //!< will not be inserted into the game
    std::shared_ptr< Entity > Load_Unmanaged( const EntityInfo& ent_info, const Vec3f& origin = Vec3f(0,0,0) ); //!< will not be inserted into the game

    void LoadAllEntValToolTips( void );

private:
    void FinishEnt( Entity& ent, const Vec3f& origin ) const;
    bool Load( File& opened_ent_file ) override;
    using AssetManager< AssetData<EntityInfo>,EntityInfo >::Load;
    void BeingUnloaded( EntityInfo& asset_unused ) override;

private:
    static bool instantiated;
};

extern EntityInfoManager entityInfoManager;
#endif  // SRC_ENTITIES_ENTITYINFOMANAGER_H_
