// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./actor.h"
#include "./valuable.h"
#include "../game.h"
#include "./entityInfoManager.h"
#include "../particles/effectsManager.h"

std::vector< std::string > Valuable::valuableSetters;

void Valuable::InitSetterNames( void ) {
    if ( valuableSetters.size() == Valuable_NUM_SETTERS )
        return; // already set
    valuableSetters.resize(Valuable_NUM_SETTERS);

    const uint i = Valuable_SETTERS_START;    
    valuableSetters[Valuable_SetObjectValue-i] = "objectValue";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Valuable,Entity>(valuableSetters);
}

bool Valuable::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<ValuableSetterT>(setter) ) {
        case Valuable_SetObjectValue: SetObjectValue( String::ToInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Valuable_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Valuable_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Valuable>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Valuable::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<ValuableSetterT>(setter) ) {
        case Valuable_SetObjectValue: return String::ToString(GetObjectValue());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Valuable_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Valuable_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Valuable>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Valuable::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( valuableSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Valuable_SETTERS_START;
        
    static_assert( is_parent_of<Valuable>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Valuable::~Valuable( void ) {
}

void Valuable::BumpInto( Entity& ent ) {
    if ( !intact )
        return;
    
    if ( !derives_from( TypeID_Flat, ent ) )
        return;

    BreakValuable();
    MarkForDeletion();
}

Valuable::Valuable( void )
    : Entity()
    , breakEffect()
    , activeEffect()
    , objectValue()
    , intact(true)
{ }

Valuable::Valuable( const Valuable& other )
    : Entity( other )
    , breakEffect( other.breakEffect )
    , activeEffect( other.activeEffect )
    , objectValue( other.objectValue )
    , intact( other.intact )
{ }

Valuable& Valuable::operator=( const Valuable& other ) {
    Entity::operator=( other );
    breakEffect = other.breakEffect;
    activeEffect = other.activeEffect;
    objectValue = other.objectValue;
    intact = other.intact;
    return *this;
}


void Valuable::Spawn( void ) {
    Entity::Spawn();
    
    AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_GHOST_US );
}

void Valuable::Think( void ) {
    if ( IsVisible() )
        Entity::Think();
        
    if ( !activeEffect.lock() ) {
        game->SubtractLevelTime( objectValue );
        MarkForDeletion();
    }
}

void Valuable::BreakValuable( void ) {
    intact = false;
    SetVisible(false);
    game->SubtractLevelTime( objectValue );
    
    if ( breakEffect )
        activeEffect = effectsManager.Spawn( *breakEffect, GetOrigin() );
}

bool Valuable::ReactsToPhysicsAgainst( const Entity& ent ) const {
    if ( derives_from<Actor>( ent ) )
        return true;
    
    return Entity::ReactsToPhysicsAgainst( ent );
}

void Valuable::SetObjectValue( const int to ) {
    objectValue = to;
}

int Valuable::GetObjectValue( void ) const {
    return objectValue;
}

void Valuable::SetBreakEffect( const std::string& _Name ) {
    SetBreakEffect( effectsManager.Get( _Name.c_str() ) );
}

void Valuable::SetBreakEffect( const std::shared_ptr< const Effect >& fx ) {
    breakEffect = fx;
}

void Valuable::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Valuable>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, valuableSetters );
}

void Valuable::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Valuable>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, valuableSetters );
}
