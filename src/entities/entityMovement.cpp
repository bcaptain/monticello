// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entity.h"

const Vec3f ENTITY_FACING_DIRECTION_DEFAULT( 0, -1, 0 ); //!< Entities face down the negative y axis by default
const uint ACTOR_MAX_MS_FOR_180_TURN = 150; //!< miliseconds it takes to do a 180 degree turn
const float ACTOR_TURN_TRIGGER_ANGLE = 20; //!< Joystick input turn angles >= this value will trigger a turn animation
const float ACTOR_MIN_MOVE_SPEED = 0.02f;

void Entity::MoveBy( const Vec3f & by ) {
    // ** first we move bind slaves
    if ( bindSlaves.Num() != 0 )
        MoveBindSlavesBy( by );

    NudgeOrigin( by );
}

void Entity::MoveBy( const float x, const float y, const float z ) {
    MoveTo( GetOrigin() + Vec3f( x, y, z ) );
}

void Entity::MoveTo( const Vec3f & org ) {
    MoveBy( org - GetOrigin() );
}

void Entity::MoveTo( const float x, const float y, const float z ) {
    MoveTo( Vec3f(x,y,z) );
}

void Entity::TryQueueMovementCallback( void ) {
    if ( IsAwake() )
        return; // the Entity will do it's own migration in ::Think (unless it is put to sleep before it gets there)

    if ( ( entity_flags & ENTFLAG_MIGRATE_VOXELS ) == 0 ) {
        const bool rotated_nonsphere = ( entity_flags & ENTFLAG_ROTATED ) && !bounds.IsSphere();
        if ( ! rotated_nonsphere ) {
            return; // already queued
        }
    }

    entity_flags |= ENTFLAG_MIGRATE_VOXELS;
    game->QueueEvent_EntityMethod( *this, &Entity::DoMigration );
}

void Entity::TryQueueRotationCallback( void ) {
    if ( IsAwake() )
        return; // the Entity will do it's own migration in ::Think (unless it is put to sleep before it gets there)
        
    TryQueueMovementCallback(); // must do this as well

    if ( entity_flags & ENTFLAG_ROTATED )
        return; // already queued
        
    game->QueueEvent_EntityMethod( *this, &Entity::DoRotated );
}

void Entity::SetMoved( void ) {
    TryQueueMovementCallback();
    entity_flags |= ENTFLAG_MOVED | ENTFLAG_MIGRATE_VOXELS;
}

void Entity::SetRotated( void ) {
    // no need for basic entities to call TryQueueRotationCallback
    entity_flags |= ENTFLAG_ROTATED;
}

void Entity::NudgeOrigin( const Vec3f & amt ) {
    SetOrigin( GetOrigin() + amt );
}

void Entity::SetOrigin( const Vec3f & _origin ) {
    if ( GetOrigin() == _origin )
        return;
    
    bounds.SetOrigin( _origin );
}

void Entity::SetOrigin( const float x, const float y, const float z ) {
    SetOrigin( Vec3f(x, y, z ) );
}

void Entity::MoveBindSlavesBy( const Vec3f &by ) {
    Link< std::weak_ptr< Entity > >* link = bindSlaves.GetFirst();
    while ( link != nullptr ) {
        if ( auto ent = link->Data().lock() ) {
            ent->MoveBy( by );
            link = link->GetNext();
        } else {
            link = link->Del();
        }
    }
}

void Entity::BindToMaster( Entity& to ) {
      if ( auto m = bindMaster.lock() ) {
          if ( m->GetIndex() == to.GetIndex() ) {
              return; // already bound
          }
      }

    Link< std::weak_ptr< Entity > >* link = bindSlaves.GetFirst();
    while ( link != nullptr ) {
        if ( auto ent = link->Data().lock() ) {
            if ( GetIndex() == ent->GetIndex() ) {
                return; // cannot be a slave to your own slave
            }
        }
    }

    UnbindFromMaster();
    bindMaster = to.GetWeakPtr();
    to.bindSlaves.Append( my_ptr );
}

void Entity::UnbindSlaves( void ) {
    Link< std::weak_ptr< Entity > >* link = bindSlaves.GetFirst();
    while ( link != nullptr ) {
        auto ent = link->Data().lock();
        ASSERT( ent );

        link = link->GetNext(); // when we unbind the slave, it'll iterate through our (the master's) slave list to remove it's slave link. so we pre-emptively increment.
        ent->UnbindFromMaster();
    }
}

void Entity::UnbindFromMaster( void ) {
    auto sptr = bindMaster.lock();

    if ( sptr == nullptr )
        return;

    // ** we have to remove the link to ourselves from the master's slaves list
    Link< std::weak_ptr< Entity > >* link = sptr->bindSlaves.GetFirst();
    while ( link != nullptr ) {
        if ( auto ent = link->Data().lock() ) {
            if ( GetIndex() == ent->GetIndex() ) {
                link->Del();
                break; // once we've found it, we're done
            }
        }
        link = link->GetNext();
    }

    bindMaster.reset();
}

void Entity::TurnTo( const Entity& ent ) {
    TurnToFaceDir( Vec3f::GetNormalized( ent.GetOrigin() - GetOrigin() ) );
}

void Entity::TurnToFaceDir( const Vec3f& unit_vec ) {
    //todoGravDir: this needs to be updated to use globalVals.GetVec3f(gval_grav_dir)
    const float angle = 180.0f-atan2f( unit_vec.x, unit_vec.y ) * RAD_TO_DEG_F;
    rotate_to = bounds.primitive->GetAngles();
    rotate_to.z = angle;
}

void Entity::NudgeTurn( const float nudge_angle ) {
    rotate_to += globalVals.GetVec3f(gval_grav_dir) * nudge_angle;
}

void ApplyRotation_Helper( float& rotate_this, float& to_this, const float seconds_per_180degrees );
void ApplyRotation_Helper( float& rotate_this, float& to_this, const float seconds_per_180degrees ) {
    if ( Maths::Approxf( rotate_this, to_this ) )
        return;
    
    to_this = Maths::NormalizeAngle( to_this );

    const float degrees_per_second = 180.0f / seconds_per_180degrees;
    float degrees_per_frame = degrees_per_second / FPS_CAP;

    const float difference = to_this - rotate_this;
    const float fabs_difference = fabsf( difference );

    if ( fabs_difference < degrees_per_frame ) {
        rotate_this = to_this;
        return;
    }

    // if the rotation is on the opposite direction, we subtract degrees, not add them
    if ( difference < 0.0f )
        degrees_per_frame = -degrees_per_frame;

    // if the difference is greater than 180, turn the other direction since it is shorter
    if ( fabs_difference > 180 )
        degrees_per_frame = -degrees_per_frame;
    
    rotate_this += degrees_per_frame;
    rotate_this = Maths::NormalizeAngle( rotate_this );
}

uint Entity::GetTurnSpeed( void ) const {
    return turnSpeed;
}

void Entity::SetTurnSpeed( const uint to ) {
    turnSpeed = to;
}

uint Entity::GetMaxTurnSpeed( void ) const {
    return maxTurnSpeed;
}

void Entity::SetMaxTurnSpeed( const uint to ) {
    maxTurnSpeed = to;
}

float Entity::GetMoveSpeed( void ) const {
#define PLAYTEST_MOVESPEED 1  //DEBUG/PLAYTESTING
#ifdef PLAYTEST_MOVESPEED
    float moveSpeedAdjust = globalVals.GetFloat( gval_pt_moveSpeedAdjust, 0 );
    return moveSpeed + moveSpeedAdjust;
#endif // PLAYTEST_MOVESPEED

    return moveSpeed;
}

void Entity::SetMoveSpeed( const float to ) {
    moveSpeed = to;
}

float Entity::GetTopSpeed( void ) const {
    float topSpeed = moveSpeed + moveSpeedBoost;
    topSpeed *= runSpeedMultiplier;
    return topSpeed;
}

void Entity::SetMoveSpeedBoost( const float to ) {
    moveSpeedBoost = to;
}

float Entity::GetMoveSpeedBoost( void ) const {
    return moveSpeedBoost;
}

bool Entity::CanMove( void ) const {
    if (noMove)
        return false;
        
    if ( !IsDoneTurning() )
        if ( !moveWhileTurning )
            return false;
    return true;
}

bool Entity::CanTurn( void ) const {    
    return !noTurn;
}

bool Entity::IsDoneTurning( void ) const {
    return fabsf( rotate_to.z - bounds.primitive->GetAngles().z ) < ACTOR_TURN_TRIGGER_ANGLE;
}

void Entity::ApplyRotation( void ) {
    
    turnDirection = TURNING_NOT_TURNING;

    if ( !CanTurn() )
        return;
    
    Vec3f newAngles( bounds.primitive->GetAngles() );
    if ( newAngles == rotate_to )
        return;

    const float seconds_per_180degrees = static_cast< float >( GetTurnSpeed() ) / 1000;
    
    ApplyRotation_Helper( newAngles.x, rotate_to.x, seconds_per_180degrees );
    ApplyRotation_Helper( newAngles.y, rotate_to.y, seconds_per_180degrees );
    ApplyRotation_Helper( newAngles.z, rotate_to.z, seconds_per_180degrees );

    if ( CanTurn() ) { //Checked again for "Heavy Turning"
        bounds.primitive->SetAngle( newAngles ); //This is where the actual turn happens
    }
}

bool Entity::HasArrivedAt_2D( const Vec3f& destination, const float arrivalDist ) const {
    Vec3f position = GetOrigin();
    position.z = 0;
    
    const Vec3f dest(destination.x, destination.y, 0);
    
    const float dist_sq = ( dest - position ).SquaredLen();
    const float arrivalDist_sq = arrivalDist * arrivalDist;
        
    return dist_sq < arrivalDist_sq;
}

Vec3f Entity::GetFacingDir( void ) const {
    if ( ! bounds.primitive->IsRotated() )
        return ENTITY_FACING_DIRECTION_DEFAULT;

    const Mat3f rot( bounds.primitive->GetAngles() );
    return ENTITY_FACING_DIRECTION_DEFAULT * rot;
}

void Entity::SetFacingDir( const Vec3f& unit_vec ) {
    /*
        // this whole process involves
        // 1.) finding the axis of rotation, which is usually the cross between the two vectors you're trying to align.
        // 2.) Generating the angle of rotation between the two angles.. something along the lines of angle = atan2(length(cross(a, b)), dot(a, b)). (which I stole from a forum post and haven't tested yet)

        //const float deg = DegreesBetween( unit_vec, globalVals.GetVec3f(gval_grav_dir) );
        Vec3f cross = unit_vec.Cross( globalVals.GetVec3f(gval_grav_dir) );
        const float angle = atan2( cross.Len(), unit_vec.Dot(globalVals.GetVec3f(gval_grav_dir)) );
        angles.z = angle; // set yaw. this assumes globalVals.GetVec3f(gval_grav_dir) is 0,0,-1
    */

    /*
    x = r * sin(p) * cos(t)
    y = r * sin(p) * sin(t)
    z = r * cos(p)

    If you already have x,y,z and want to switch it back, this is the conversion:

    r = sqrt(x*x + y*y + z*z)
    t = arctan(y/x)
    p = arccos(z/r)
    */

    // todoGravDir: this should use globalVals.GetVec3f(gval_grav_dir)
    const float angle_z = 180.0f-atan2f( unit_vec.x, unit_vec.y ) * RAD_TO_DEG_F;
    bounds.primitive->SetYaw( angle_z );
}

void Entity::SnapOrigin( const float to_meters ) {
    Vec3f origin = GetOrigin();
    ::SnapOrigin( origin, to_meters );
    SetOrigin( origin );
}

void Entity::RotateTo( const Vec3f& angles ) {
    rotate_to = angles;
}

LinkList< std::weak_ptr< Entity > >& Entity::GetBindSlaveListRef( void ) {
    return bindSlaves;
}

TurnDirT_BaseType Entity::GetTurnDirection( void ) const {
    return turnDirection;
}

void Entity::SetTurnDirection( TurnDirT_BaseType turnDir ) {
    turnDirection = turnDir;
}

std::shared_ptr< Entity > Entity::GetBindMaster( void ) {
    return bindMaster.lock();
}

std::shared_ptr< const Entity > Entity::GetBindMaster( void ) const {
    return bindMaster.lock();
}

void Entity::SetRunSpeedMultiplier( const float _multiplier ) {
    runSpeedMultiplier = _multiplier;
}

float Entity::GetRunSpeedMultiplier( void ) const {
    return runSpeedMultiplier;
}

bool Entity::IsMovable( void ) const {
    return movable;
}

void Entity::SetMovable( const bool to ) {
    movable = to;
}

void Entity::DoRotated( void ) {
    entity_flags &= ~ENTFLAG_ROTATED;
}

void Entity::DoMigration( void ) {
    if ( entity_flags & ENTFLAG_MIGRATE_VOXELS )
        Migrate();
}

void Entity::Migrate( void ) {
    // re-insert into the object tree
    entity_flags &= ~( ENTFLAG_MIGRATE_VOXELS | ENTFLAG_MOVED );

    game->entTree->TryMigrate( *this );

    UpdateNavMeshOccupancy();

    if ( GetNumVoxelsOccupied() < 1 ) {
        WARN("Entity left the octree. Marking it for deletion.\n");
        MarkForDeletion();
    }
}
