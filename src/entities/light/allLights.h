// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_ALLLIGHTS_H_
#define SRC_ENTITIES_ALLLIGHTS_H_

#include "./light.h"
#include "./dirLight.h"
#include "./pointLight.h"

#endif //SRC_ENTITIES_ALLLIGHTS_H_
