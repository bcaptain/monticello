// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./light.h"
#include "../../rendering/renderer.h"
#include "../../game.h"

const Vec3f DEFAULT_LIGHT_COLOR = Vec3f(1,1,1);
const Vec3f DEFAULT_LIGHT_TARGETCOLOR = Vec3f(0,0,0);
const Vec3f DEFAULT_LIGHT_AMBIENT_STRENGTH = Vec3f(0.1f,0.1f,0.1f);
const uint NUM_EMISSION_SEGMENTS = 32;
const float LIGHT_ATTENUATION_FALLOFF = 0.2f; //Due to the attenuation formula light will be noticable at approx <= 20% of the lights distance from target
const float DEFAULT_LIGHT_ATTENUATION_DISTANCE = 1;
const float LIGHT_ATTENUATION_CONSTANT = 1; //Do Not Change
const float LIGHT_ATTENUATION_LINEAR_CALC = 4.5f; //Do Not Change
const float LIGHT_ATTENUATION_QUADRATIC_CALC = 75.0f; //Do Not Change

std::vector< std::string > Light::lightSetters;

void Light::InitSetterNames( void ) {
    if ( lightSetters.size() == Light_NUM_SETTERS )
        return; // already set
    lightSetters.resize(Light_NUM_SETTERS);
    
    const uint i = Light_SETTERS_START;
    lightSetters[Light_SetColor-i] = "color";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Light,Entity>(lightSetters);
}

bool Light::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<LightSetterT>(setter) ) {
        case Light_SetColor: SetColor( String::ToVec3f(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Light_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Light_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Entity,Light>() );
            return Entity::Set( setter, value );
    }
}
std::string Light::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<LightSetterT>(setter) ) {
        case Light_SetColor: return String::ToString(GetColor());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Light_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Light_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Entity,Light>() );
            return Entity::Get( setter );
    }
}

NamedSetterT Light::GetSetterIndex( const std::string& key ) {
    
    const NamedSetterT ret =  GetSetterIndexIn( lightSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Light_SETTERS_START;
        
    static_assert( is_parent_of<Light>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Light::Light( void )
    : ambientStrength( DEFAULT_LIGHT_AMBIENT_STRENGTH )
    , color( DEFAULT_LIGHT_COLOR )
    , targetColor( DEFAULT_LIGHT_TARGETCOLOR )
    , vbo_vert( new VBO<float>(3, VBOChangeFrequencyT::RARELY ) )
    , vbo_colors( new VBO<float>(4, VBOChangeFrequencyT::RARELY ) )
    , attenuation()
{
    SetNoPhys( true );
    SetVisible( false );
}

Light::Light( const Light& other )
    : Entity( other )
    , ambientStrength(other.ambientStrength)
    , color(other.color)
    , targetColor(other.targetColor)
    , vbo_vert( new VBO<float>(3, VBOChangeFrequencyT::RARELY ) )
    , vbo_colors( new VBO<float>(4, VBOChangeFrequencyT::RARELY ) )
    , attenuation(other.attenuation)
{ }

Light& Light::operator=( const Light& other ) {
    if ( this == &other )
        return *this;

    ambientStrength = other.ambientStrength;
    color = other.color;
    targetColor = other.targetColor;
    vbo_vert = other.vbo_vert;
    vbo_colors = other.vbo_colors;

    Entity::operator=( other );
    
    return *this;
}

Light::~Light( void ) {
    
    delete vbo_vert;
    delete vbo_colors;
}

void Light::DrawVBO( [[maybe_unused]] const float opacity_unused ) const {
    if ( globalVals.GetBool( gval_d_vertNormals ) )
        DrawVertexNormals();
}

void Light::Spawn( void ) {
    ASSERT_MSG( renderer, "Tried to spawn a light before the renderer was created" );
    renderer->AddLight( *this );
    AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_NO_PHYS );
    Entity::Spawn();
}

Vec3f Light::GetInterpColor( const float delta ) const {
    
    if ( Maths::Approxf( delta, 1.0f, EP_HUNDREDTH ) || delta > 1.0f )
        return targetColor;
            
    if ( Maths::Approxf( delta, 0.0f, EP_HUNDREDTH ) || delta < 0.0f )
        return color;
    
    Vec3f interpColor( color );
    Vec3f colorChangeDelta;
    
    colorChangeDelta.x = fabsf( targetColor.x - color.x ) * delta;
    colorChangeDelta.y = fabsf( targetColor.y - color.y ) * delta;
    colorChangeDelta.z = fabsf( targetColor.z - color.z ) * delta;
    
    ( targetColor.x <= color.x ) ? interpColor.x -= colorChangeDelta.x : interpColor.x += colorChangeDelta.x;
    ( targetColor.y <= color.y ) ? interpColor.y -= colorChangeDelta.y : interpColor.y += colorChangeDelta.y;
    ( targetColor.z <= color.z ) ? interpColor.z -= colorChangeDelta.z : interpColor.z += colorChangeDelta.z;

    return interpColor;
}

bool Light::IsVisible( void ) const {
    return globalVals.GetBool( gval_d_lights );
}

void Light::SetColor( const Vec3f& to ) {
    color = to;
}

void Light::SetTargetColor( const Vec3f& to ) {
    targetColor = to;
}

Vec3f Light::GetColor( void ) const {
    return color;
}

Vec3f Light::GetTargetColor( void ) const {
    return targetColor;
}

void Light::SetAmbientStrength( const Vec3f& to ) {
    entVals.SetVec3f( "ambientStrength", to ); //toNamedSetter
}

Vec3f Light::GetAmbientStrength( void ) const {
    return entVals.GetVec3f( "ambientStrength", DEFAULT_LIGHT_AMBIENT_STRENGTH ); //toNamedSetter
}

const LightAttenuation Light::Attenuation( void ) const {
    return attenuation;
}

void Light::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Light>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, lightSetters );
}

void Light::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Light>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, lightSetters );
}
