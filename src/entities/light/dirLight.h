// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_DIRLIGHT_H_
#define SRC_ENTITIES_DIRLIGHT_H_

#include "./clib/src/warnings.h"
#include "./light.h"

extern const Vec3f DEFAULT_DIRLIGHT_DIRECTION;
extern const float DIRLIGHT_EMISSION_LENGTH;
extern const float DIRLIGHT_EMISSION_RADIUS;

/*!

DirLight \n\n

Directional light (i.e. sunlight)

**/


class DirLight : public Light {
public:
    static constexpr TypeInfo<DirLight, Light> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    DirLight( void );
    DirLight( const DirLight& other );
    ~DirLight( void ) override { }

public:
    DirLight& operator=( const DirLight& other ); 
    
    using Light::DrawVBO;
    void DrawVBO( const float opacity ) const override;
    
public:
    Vec3f GetDirection( void ) const;
    
    void SetRotated( void ) override;
    void DoRotated( void ) override;
    void DrawEmission( void );
    
private:
    void UpdateDirection( void ); //!< updates direction variable.
    
private:
    void PackDirLightEmissionVBO( void );
    Vec3f direction; //!< this is updated automatically when the entity is rotated. Do not modify this manually.
    
public:
    
    static_assert( is_parent_of<DirLight>( get_typeid<Light>() ) );
    static ToolTip< DirLight, Light > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }


};

#endif //SRC_ENTITIES_DIRLIGHT_H_
