// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_LIGHT_H_
#define SRC_ENTITIES_LIGHT_H_

#include "../entity.h"

extern const Vec3f DEFAULT_LIGHT_COLOR;
extern const Vec3f DEFAULT_LIGHT_TARGETCOLOR;
extern const Vec3f DEFAULT_LIGHT_AMBIENT_STRENGTH;
extern const uint NUM_EMISSION_SEGMENTS;
extern const float LIGHT_ATTENUATION_FALLOFF;
extern const float DEFAULT_LIGHT_ATTENUATION_DISTANCE;
extern const float LIGHT_ATTENUATION_CONSTANT;
extern const float LIGHT_ATTENUATION_LINEAR_CALC;
extern const float LIGHT_ATTENUATION_QUADRATIC_CALC;

class PointLight;
class DirLight;

struct LightAttenuation {
    
    LightAttenuation( void )
        : distance(DEFAULT_LIGHT_ATTENUATION_DISTANCE)
        , linear()
        , quadratic()
        , constant(LIGHT_ATTENUATION_CONSTANT)
        { }

    LightAttenuation( const LightAttenuation& other )
        : distance(other.distance)
        , linear(other.linear)
        , quadratic(other.quadratic)
        , constant(other.constant)
        { }

    LightAttenuation& operator=( const LightAttenuation& other ) {
        distance = other.distance;
        linear = other.linear;
        quadratic = other.quadratic;
        return *this;
    }
    
    float distance;
    float linear;
    float quadratic;
    const float constant;
};


class Light : public Entity {
public:
    static constexpr TypeInfo<Light, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Light( void );
    Light( const Light& other );
    ~Light( void ) override;

public:
    Light& operator=( const Light& other ); 
    
public:
    void Spawn( void ) override;
    
    using Entity::DrawVBO;
    void DrawVBO( const float opacity_unused ) const override;
    
    const LightAttenuation Attenuation( void ) const;
    bool IsVisible( void ) const override;
    
    void SetColor( const Vec3f& to);
    Vec3f GetColor( void ) const;
    void SetTargetColor( const Vec3f& to);
    Vec3f GetTargetColor( void ) const;
    Vec3f GetInterpColor( const float delta ) const; //Returns linear interp between setColor and targetColor
    void SetAmbientStrength( const Vec3f& to );
    Vec3f GetAmbientStrength( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
protected:
    Vec3f ambientStrength; //Todo Jesse: This is more of a "world" setting than a per-light setting. (i.e. map.ambientLightLevel ? )
    Vec3f color;
    Vec3f targetColor;
    VBO<float>* vbo_vert;
    VBO<float>* vbo_colors;
    LightAttenuation attenuation;
    
// ** Named Setters
private:
    static const uint Light_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum LightSetterT : NamedSetterT_BaseType {
        Light_SetColor = Light_SETTERS_START
        , Light_SETTERS_END
        , Light_NUM_SETTERS = Light_SETTERS_END - Light_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > lightSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
    static_assert( is_parent_of<Light>( get_typeid<Entity>() ) );
    static ToolTip< Light, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif // SRC_ENTITIES_LIGHT_H_
