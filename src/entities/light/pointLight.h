// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_POINTLIGHT_H_
#define SRC_ENTITIES_POINTLIGHT_H_

#include "./light.h"

extern const float DEFAULT_POINTLIGHT_RADIUS;

/*!

PointLight \n\n

Light emitted evenly from a point (i.e. a lightbulb)

**/


class PointLight : public Light {
public:
    static constexpr TypeInfo<PointLight, Light> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    PointLight( void );
    PointLight( const PointLight& other );
    ~PointLight( void ) override { }

public:
    PointLight& operator=( const PointLight& other ); 
    
    using Light::DrawVBO;
    void DrawVBO( const float opacity ) const override;
    
public:
    void SetRadius( const float to );
    float GetRadius( void ) const;
    void Editor_Think( void );
    void DrawEmission( void );
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

private:
    void SetAttenuationDistance( const float dist);
    void PackPointLightEmissionVBO( void );
    float radius;

// ** Named Setters
private:
    static const uint PointLight_SETTERS_START = Light_NUM_SETTERS;
protected:
    enum PointLightSetterT : NamedSetterT_BaseType {
        PointLight_SetRadius = PointLight_SETTERS_START
        , PointLight_SETTERS_END
        , PointLight_NUM_SETTERS = PointLight_SETTERS_END - PointLight_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > pointLightSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<PointLight>( get_typeid<Light>() ) );
    static ToolTip< PointLight, Light > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif // SRC_ENTITIES_POINTLIGHT_H_
