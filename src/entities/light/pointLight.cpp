// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./pointLight.h"
#include "../../rendering/renderer.h"
#include "../../editor/editor.h"

const float DEFAULT_POINTLIGHT_RADIUS = 2.0f;

std::vector< std::string > PointLight::pointLightSetters;

void PointLight::InitSetterNames( void ) {
    if ( pointLightSetters.size() == PointLight_NUM_SETTERS )
        return; // already set
    pointLightSetters.resize(PointLight_NUM_SETTERS);
    
    const uint i = PointLight_SETTERS_START;
    pointLightSetters[PointLight_SetRadius-i] = "radius";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<PointLight,Light>(pointLightSetters);
}

bool PointLight::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<PointLightSetterT>(setter) ) {
        case PointLight_SetRadius: SetRadius( String::ToFloat( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case PointLight_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<PointLight>( get_typeid<Light>() ) );
            return Light::Set( setter, value );
    } 
}
std::string PointLight::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<PointLightSetterT>(setter) ) {
        case PointLight_SetRadius: return String::ToString(GetRadius());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case PointLight_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<PointLight>( get_typeid<Light>() ) );
            return Light::Get( setter );
    } 
}

NamedSetterT PointLight::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( pointLightSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + PointLight_SETTERS_START;
        
    static_assert( is_parent_of<PointLight>( get_typeid<Light>() ) );
    return Light::GetSetterIndex( key );
}

PointLight::PointLight( void )
    : Light()
    , radius( DEFAULT_POINTLIGHT_RADIUS )
{ }

PointLight::PointLight( const PointLight& other )
    : Light( other )
    , radius(other.radius)
{ }

PointLight& PointLight::operator=( const PointLight& other ) {
    Light::operator=( other );
    radius = other.radius;
    
    return *this;
}

void PointLight::SetAttenuationDistance( const float dist ) {
    //Determines how the light fades over distance.  Due to the formula light will be noticable at <= 20% of the range passed into this function.
    
    attenuation.distance = dist;
    attenuation.linear = LIGHT_ATTENUATION_LINEAR_CALC / dist;
    attenuation.quadratic = LIGHT_ATTENUATION_QUADRATIC_CALC / ( dist * dist );
}

void PointLight::SetRadius( const float to ) {
    radius = to;
    SetAttenuationDistance( to );
}

float PointLight::GetRadius( void ) const {
    return radius;
}

void PointLight::Editor_Think( void ) {

    float newRadius = entVals.GetFloat("radius", DEFAULT_POINTLIGHT_RADIUS ); //toNamedSetter

    if ( !Maths::Approxf( radius, newRadius ) ) {
        radius = newRadius;
        SetAttenuationDistance( radius );
    }

    if ( globalVals.GetBool( gval_d_light ) )
        PackPointLightEmissionVBO();
            
}

void PointLight::DrawVBO( const float opacity ) const {
    Light::DrawVBO( opacity );
    
    const Color4f green(0,1,0,1);
    
    const Vec3f org = GetOrigin();
    
    renderer->DrawLine( org-Vec3f(2,0,0), org+Vec3f(2,0,0), green, green );
    renderer->DrawLine( org-Vec3f(0,2,0), org+Vec3f(0,2,0), green, green );
    renderer->DrawLine( org-Vec3f(0,0,2), org+Vec3f(0,0,2), green, green );
}

void PointLight::DrawEmission( void ) {
    if( !vbo_vert->Finalized() )
        PackPointLightEmissionVBO();
        
    ASSERT( vbo_vert->Finalized() );
    
    shaders->UseProg(  GLPROG_GRADIENT  );
    
    renderer->PushMatrixMV();
        renderer->ModelView().Translate( GetOrigin() );
        shaders->SendData_Matrices();
    renderer->PopMatrixMV();
    
    shaders->SetAttrib( "vColor", *vbo_colors );
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->DrawArrays( GL_LINES, 0, vbo_vert->Num() );
}

void PointLight::PackPointLightEmissionVBO( void ) {
    
    vbo_vert->Clear();
    vbo_colors->Clear();
       
    float l_radius = radius * LIGHT_ATTENUATION_FALLOFF;
    
    //LIGHT RADIUS
    Color4f circleColor( GetColor().x, GetColor().y, GetColor().z ,1.0f );
    PackAlignedCircleVBO( NUM_EMISSION_SEGMENTS, l_radius, *vbo_vert, *vbo_colors, circleColor, Vec3f(0.0f,0.0f,-1.0f) );
    PackAlignedCircleVBO( NUM_EMISSION_SEGMENTS, l_radius, *vbo_vert, *vbo_colors, circleColor, Vec3f(1.0f,0.0f,0.0f) );
    PackAlignedCircleVBO( NUM_EMISSION_SEGMENTS, l_radius, *vbo_vert, *vbo_colors, circleColor, Vec3f(0.0f,1.0f,0.0f) );
    
    //CLICKABLE FOR REFERENCE
    float object_radius = 0.5f;
    circleColor = Color4f(DEFAULT_LIGHT_COLOR.x,DEFAULT_LIGHT_COLOR.y,DEFAULT_LIGHT_COLOR.z, 1.0f );
    PackAlignedCircleVBO( NUM_EMISSION_SEGMENTS, object_radius, *vbo_vert, *vbo_colors, circleColor, Vec3f(0.0f,0.0f,-1.0f) );
    PackAlignedCircleVBO( NUM_EMISSION_SEGMENTS, object_radius, *vbo_vert, *vbo_colors, circleColor, Vec3f(1.0f,0.0f,0.0f) );
    PackAlignedCircleVBO( NUM_EMISSION_SEGMENTS, object_radius, *vbo_vert, *vbo_colors, circleColor, Vec3f(0.0f,1.0f,0.0f) );
    
    vbo_vert->MoveToVideoCard();
    vbo_colors->MoveToVideoCard();
}

void PointLight::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Light::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<PointLight>( get_typeid<Light>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, pointLightSetters );
}

void PointLight::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Light::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<PointLight>( get_typeid<Light>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, pointLightSetters );
}
