// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./dirLight.h"
#include "../../rendering/renderer.h"

const Vec3f DEFAULT_DIRLIGHT_DIRECTION = Vec3f(0,0,-1);
const float DIRLIGHT_EMISSION_LENGTH = 1;
const float DIRLIGHT_EMISSION_RADIUS = 0.25f;

DirLight::DirLight( void )
    : Light()
    , direction( DEFAULT_DIRLIGHT_DIRECTION )
{ }

DirLight::DirLight( const DirLight& other )
    : Light( other )
    , direction( DEFAULT_DIRLIGHT_DIRECTION )
{
}

DirLight& DirLight::operator=( const DirLight& other ) {
    Light::operator=( other );
    direction = other.direction;
    return *this;
}

void DirLight::DrawVBO( const float opacity ) const {
    Light::DrawVBO( opacity );
    
    const Color4f green(0,1,0,1);
    
    const Vec3f arrowEnd( GetOrigin() + GetDirection() * 2 );
    renderer->DrawLine( GetOrigin(), arrowEnd, green, green );
}

void DirLight::DoRotated( void ) {
    UpdateDirection();
    entity_flags &= ~ENTFLAG_ROTATED;
}

void DirLight::DrawEmission( void ) {
    if( !vbo_vert->Finalized() )
        PackDirLightEmissionVBO();
        
    ASSERT( vbo_vert->Finalized() );
    
    shaders->UseProg(  GLPROG_GRADIENT  );
    
    
    renderer->PushMatrixMV();
        renderer->ModelView().Translate( GetOrigin() );
        renderer->ModelView().RotateZYX( bounds.primitive->GetAngles() );
        shaders->SendData_Matrices();
    renderer->PopMatrixMV();
    
    shaders->SetAttrib( "vColor", *vbo_colors );
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->DrawArrays( GL_TRIANGLE_STRIP, 0, vbo_vert->Num() );
}

void DirLight::PackDirLightEmissionVBO( void ) {
    
    vbo_vert->Clear();
    vbo_colors->Clear();
    
    Vec3f downward = Vec3f(0,0,-1);//debug
    PackVBO_Cylinder( NUM_EMISSION_SEGMENTS, DIRLIGHT_EMISSION_RADIUS, DIRLIGHT_EMISSION_LENGTH, Vec3f(0,0,0), downward, *vbo_vert, vbo_colors );
    
    vbo_vert->MoveToVideoCard();
    vbo_colors->MoveToVideoCard();
}

void DirLight::UpdateDirection( void ) {
    direction = DEFAULT_DIRLIGHT_DIRECTION * Quat::FromAngles_ZYX( bounds.primitive->GetAngles() );
}

void DirLight::SetRotated( void ) {
    TryQueueRotationCallback();
    entity_flags |= ENTFLAG_ROTATED;
}

Vec3f DirLight::GetDirection( void ) const {
    return direction;
}
