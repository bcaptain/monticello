// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./actor.h"
#include "./bugFood.h"
#include "../game.h"
#include "../nav/navMesh.h"
#include "../nav/flowField.h"
#include "../damage/damageManager.h"
#include "../rendering/renderer.h" //debug only
#include "../rendering/font.h" //debug only
#include "../rendering/fontManager.h" //debug only
#include "../rendering/animation/animInfo.h"

const Color4f AI_FACING_DIRECTION_START = Color4f(1,0,0,1);
const Color4f AI_FACING_DIRECTION_END = Color4f(0,1,0,1);
const float AI_DEFAULT_CHASE_RADIUS_LIMIT = 15;
const uint AI_MAX_CHASE_NAV_POLIES = 15;
const float AI_AVOID_PLAYER_ARC_ANGLE = 60;

std::shared_ptr< Entity > Actor::AI_SetPathToWaypoint( const std::shared_ptr< Entity >& waypoint, const uint max_poly_moves ) {
    return AI_SetPathTo( waypoint, max_poly_moves );
}

void Actor::ArrivedAtDestination( void ) {
    nav_state = NAV_STATE_ARRIVED;
}

std::shared_ptr< Entity > Actor::AI_SetPathTo( const std::shared_ptr< Entity >& ent, const uint max_poly_moves ) {
    ASSERT( bounds.IsSphere() );

    if ( ! ent ) {
        nav_state = NAV_STATE_NO_PATH;
        return nullptr;
    }

    // try to find a path (or a better path)
    navPath = NavMesh::GetPath( *this, *ent, max_poly_moves );
    if ( !navPath.HasCurDest() ) {
        WARN("Actor could not find a path to entity \"%s\"\n", ent->GetIdentifier().c_str() );
        nav_state = NAV_STATE_NO_PATH;
        return nullptr;
    }

    if ( navPath.GetNumDests() == 1 ) {
        // see if our view is blocked by anything
        const auto line = Line3f( GetOrigin(), ent->GetOrigin() );
        const auto primitive = Primitive_Line3f( line );
        IntersectData coldata;
        std::shared_ptr< Entity > blocking_ent = GlobalTrace( coldata, primitive );
        auto const uid = blocking_ent->GetIndex();
        if ( uid != ent->GetIndex() && uid != GetIndex() ) {
            nav_state = NAV_STATE_PATH_ACQUIRED | NAV_STATE_PATH_BLOCKED;
            return blocking_ent;
        } else {
            ASSERT( bounds.IsSphere() );
            if ( HasArrivedAt_2D( ent->GetOrigin(), bounds.GetRadius() ) ) {
                nav_state = NAV_STATE_PATH_ACQUIRED;
                ArrivedAtDestination();
                return ent;
            }
        }
    }

    nav_state = NAV_STATE_PATH_ACQUIRED;
    return ent;
}

bool Actor::AI_FollowPath( const float destination_radius ) {
    ASSERT( bounds.IsSphere() );

    if ( !navPath.HasCurDest() ) {
        if ( nav_state == NAV_STATE_ARRIVED ) {
            return true;
        } else {
            nav_state = NAV_STATE_NO_PATH;
            return false;
        }
    }

    // get the first stop
    const Vec3f destination( navPath.GetCurDest() );

    ASSERT( bounds.IsSphere() );
    if ( HasArrivedAt_2D( destination, bounds.GetRadius() + destination_radius ) ) {
        navPath.PrepareNextDest();

        if ( !navPath.HasCurDest() ) {
            ArrivedAtDestination();
            return true;
        }
    }

    nav_state = NAV_STATE_DEPARTED;

    std::weak_ptr< const NavPoly > curPoly_w;
    if ( navPath.HasCurDest() ) {
        curPoly_w = navPath.GetCurPoly();
    }

    const bool doFlowField = globalVals.GetBool( gval_ai_flowfield );

    auto enemy_ptr = pursued_entity.lock();
    if ( !enemy_ptr || !doFlowField ) {
        MoveToward( destination ); //navmesh move
        TurnToPos( destination );
        return true;
    }
    
    // **** FLOWFIELD
    
    std::shared_ptr< FlowField > enemy_field = enemy_ptr->GetFlowField();
    ASSERT( enemy_field );
    
    if ( !enemy_field->IsReady() )
        return false;
    
    // ** set up the destination
    const Vec3f origin3f = enemy_ptr->GetOrigin();
    Circle2f destCircle( Vec2f(origin3f.x, origin3f.y), NAV_DESTINATION_ARRIVAL_DISTANCE );
    if ( enemy_ptr->bounds.IsSphere() ) {
        destCircle.radius = enemy_ptr->bounds.GetRadius();
    }
    
    const float sight_radius = AI_DEFAULT_CHASE_RADIUS_LIMIT; //todotagstuff - see other todotagstuff tag
    
    // ** enemy in sight?
    const float my_radius_sq = bounds.GetRadius() * bounds.GetRadius();
    const float dest_radius_sq = destCircle.radius * destCircle.radius;
    const float distance_sq = ( Vec2f(GetOrigin().x, GetOrigin().y) - destCircle.origin ).SquaredLen() - my_radius_sq - dest_radius_sq;
    
    const float sight_radius_sq = sight_radius * sight_radius;
    
    if ( distance_sq < 0 || distance_sq > sight_radius_sq ) 
        return false; // the enemy is out of our sight

    // ** calculate the flowenemy_field
    const std::vector<uint> obstacle_ent_ids_to_ignore({GetIndex(), enemy_ptr->GetIndex()});
    destFlowField = enemy_field;

    Vec3f moveDirection = enemy_field->GetMoveDirection( *this, GetOrigin(), obstacle_ent_ids_to_ignore );
    SetMoveVec( moveDirection );
    TurnToFaceDir( moveDirection );
    return true;
}

void Actor::AI_FindEnemyInSight( void ) {
    // ** no need to do an enemy check every single frame
    const uint enemyCheckDelay = 250;
    if ( game->GetGameTime() - prevEnemyCheck < enemyCheckDelay )
        return;

    prevEnemyCheck = game->GetGameTime();

    // ** get enemy if intersects view cone and our view is not blocked
    std::vector< CollisionItem > collisions;
    GetEntitiesInSight( collisions, TypeID_Actor );
    auto player = GetPlayerIfInList( collisions );
    if ( !player || GetSquaredSightDistance( *player ) < 0 )
        return;

    pursued_entity = player;
}

std::shared_ptr< Entity > Actor::AI_ExecutePatrolScript( void ) {
    // ** already in transit?
    if ( nav_state & NAV_STATE_DEPARTED )
        return AI_SetPathToWaypoint( pursued_entity.lock(), maxRoomPatrol );

    // ** no patrolScript selected?
    if ( patrol.size() < 1 )
        return nullptr;

    // ** clear the current path
    navPath.Clear();

    // ** waiting on a delay?
    if ( game->GetGameTime() - prev_waypoint_check < waypoint_duration )
        return nullptr; // keep the path we have
        
    prev_waypoint_check = game->GetGameTime();

    // ** listify the patrolScript
    const std::vector< std::string > list =String::ParseStringList( patrol, ";" );
    if ( list.size() < 1 )
        return nullptr;

    // ** end of script?
    if ( cur_waypoint_index >= list.size() )
        return nullptr;

    return AI_ExecutePatrolScriptExpression( list[cur_waypoint_index] );
}

std::shared_ptr< Entity > Actor::AI_ExecutePatrolScriptExpression( const std::string& expression ) {
    SetTurnSpeed( GetMaxTurnSpeed() );

    // ** execute action
    const std::vector< std::string > action =String::ParseStringList( expression, "," );
    if ( action.size() < 1 ) {
        ERR("Invalid format for entVal \"patrol\" on entity %s, got \"%s\"\n", GetIdentifier().c_str(), expression.c_str() );
        RebootPatrolScript();
        return nullptr;
    }

    // ** parse!
    std::shared_ptr< Entity > ret;

    if ( action[0] == "waypoint" ) { // walk to waypoint
        ret = AI_ExecutePatrolScriptExpression_waypoint( action );
        ++cur_waypoint_index; // execute the next expression next frame

    } else if ( action[0] == "wait" ) {
        AI_ExecutePatrolScriptExpression_wait( action );
        ++cur_waypoint_index;

    } else if ( action[0] == "turn" ) {
        AI_ExecutePatrolScriptExpression_turn( action );
        ++cur_waypoint_index;

    } else if ( action[0] == "turnBy" ) {
        AI_ExecutePatrolScriptExpression_turnBy( action );
        ++cur_waypoint_index;

    } else if ( action[0] == "turnTo" ) {
        AI_ExecutePatrolScriptExpression_turnTo( action );
        ++cur_waypoint_index;

    } else if ( action[0] == "loop" ) {
        RebootPatrolScript();
        return ret;

    } else {
        ERR("Invalid format for entVal \"patrol\" on entity %s, unknown script expression: \"%s\"\n", GetIdentifier().c_str(), expression.c_str() );
        RebootPatrolScript();
        return nullptr;
    }

    return ret;
}

std::shared_ptr< Entity > Actor::AI_ExecutePatrolScriptExpression_waypoint( const std::vector< std::string >& action ) {
    const int args_expected = 2;
    if ( action.size() != args_expected ) {
        ERR("Invalid number of arguments for entVal \"patrol\" expression \"waypoint\" on entity %s, expected %i (waypoint,waypointName)\n", GetIdentifier().c_str(), args_expected );
        RebootPatrolScript();
        return nullptr;
    }

    auto & waypoint_name = action[1];
    /* todowaypoint: auto waypoint( game->FindEntity( waypoint_name, TypeID_Waypoint ) ); */ const std::shared_ptr< Entity > waypoint = nullptr;
    
    if ( !waypoint ) {
        ERR("Waypoint \"%s\" not found in map.\n", waypoint_name.c_str(), GetIdentifier().c_str() );
        RebootPatrolScript();
        return nullptr;
    }
    
    return AI_SetPathToWaypoint( waypoint, maxRoomPatrol );
}

void Actor::AI_ExecutePatrolScriptExpression_wait( const std::vector< std::string >& action ) {
    const int args_expected = 2;
    if ( action.size() != args_expected ) {
        ERR("Invalid number of arguments for entVal \"patrol\" expression \"wait\" on entity %s, expected %i (wait,timeInMS)\n", GetIdentifier().c_str(), args_expected );
        RebootPatrolScript();
        return;
    }

    const uint duration = String::ToUInt( action[1] );
    prev_waypoint_check = game->GetGameTime();
    waypoint_duration = duration;
}

void Actor::AI_ExecutePatrolScriptExpression_turnBy( const std::vector< std::string >& action ) {
    const int args_expected = 3;
    if ( action.size() != args_expected ) {
        ERR("Invalid number of arguments for entVal \"patrol\" expression \"turnBy\" on entity %s, expected %i (turnBy,angle,durationInMSPer180)\n", GetIdentifier().c_str(), args_expected );
        RebootPatrolScript();
        return;
    }

    AI_ExecutePatrolScriptExpression_turnBy_helper( action, GetFacingDir() );
}

void Actor::AI_ExecutePatrolScriptExpression_turnTo( const std::vector< std::string >& action ) {
    const int args_expected = 3;
    if ( action.size() != args_expected ) {
        ERR("Invalid number of arguments for entVal \"patrol\" expression \"turnTo\" on entity %s, expected %i (turnTo,angle,durationInMSPer180)\n", GetIdentifier().c_str(), args_expected );
        RebootPatrolScript();
        return;
    }

    AI_ExecutePatrolScriptExpression_turnBy_helper( action, Vec3f(1,0,0) );
}

void Actor::AI_ExecutePatrolScriptExpression_turnBy_helper( const std::vector< std::string >& action, const Vec3f& from_vec ) {
    const float deg = String::ToUInt( action[1] );
    const Quat rot( Quat::FromAngles_ZYX( 0,0,deg ) );
    const Vec3f newDir( from_vec * rot );

    const uint duration_ms = String::ToUInt( action[2] );

    AI_ExecutePatrolScriptExpression_turn_helper( newDir, duration_ms );
}

void Actor::AI_ExecutePatrolScriptExpression_turn( const std::vector< std::string >& action ) {
    const int args_expected = 3;
    if ( action.size() != args_expected ) {
        ERR("Invalid number of arguments for entVal \"patrol\" expression \"turn\" on entity %s, expected %i (turn,N/NW/S/etc,durationInMSPer180)\n", GetIdentifier().c_str(), args_expected );
        RebootPatrolScript();
        return;
    }

    const std::string& dir = action[1];
    const uint duration_ms = String::ToUInt( action[2] );
    prev_waypoint_check = game->GetGameTime();
    waypoint_duration = duration_ms;

    bool ret = false;
    Vec3f vec;

    switch ( dir.size() ) {

        case 1:
            switch ( dir[0] ) {
                case 'N': vec.Set(0,1,0); ret = true; break;
                case 'E': vec.Set(1,0,0); ret = true; break;
                case 'S': vec.Set(0,-1,0); ret = true; break;
                case 'W': vec.Set(-1,0,0); ret = true; break;
                default: break;
            }

        case 2:
            switch ( dir[0] ) {
                case 'N':
                    switch ( dir[1] ) {
                        case 'E': vec.SetNormalized( 1,1,0 ); ret = true; break;
                        case 'W': vec.SetNormalized(-1,1,0 ); ret = true; break;
                        default: break;
                    }
                    break;

                case 'S':
                    switch ( dir[1] ) {
                        case 'E': vec.SetNormalized(1,-1,0 ); ret = true; break;
                        case 'W': vec.SetNormalized(-1,-1,0 ); ret = true; break;
                        default: break;
                    }
                    break;

                default: break;
            }

        default: break;
    }

    if ( !ret ) {
        ERR("Invalid direction for entVal \"patrol\" expression \"turn\" on entity %s, expected one of: N,S,E,W,NE,NW,SE,SW\n", GetIdentifier().c_str() );
    } else {
        AI_ExecutePatrolScriptExpression_turn_helper( vec, duration_ms );
    }
}

void Actor::AI_ExecutePatrolScriptExpression_turn_helper( const Vec3f& to_vec, const uint duration_ms ) {
    const Vec3f cur( GetFacingDir() );
/*
    // determine if the direction we want to face is to the left of the right.
    // we do this by rotating both vectors by the amount it takes to move the current facing direction onto the +y axis.
    // from there, whether the second vector's x is positive or negative will tell us.

    const float rad = cur.RadiansBetween( Vec3f(1,0,0) );
    const Quat rot( Quat::FromRadians_ZYX( 0,0,-rad ) );
    Vec3f tmp( to_vec );
    tmp *= rot;
    const bool left_turn = tmp.x < 0;
    if ( left_turn ) {
    }
*/
    // ** Set turn speed - convert the duration into miliseconds per 180 degrees
    float degrees = cur.DegreesBetween( to_vec );

    // if it is a full 180
    if ( Maths::Approxf( degrees, 0 ) ) {
        auto signs_differ=[]( const float a, const float b ){ return ( a < -EP && b > EP ) || ( a > EP && b < -EP ); };

        if ( signs_differ( cur.x, to_vec.x ) || signs_differ( cur.y, to_vec.y ) ) {
            degrees = 180;
        }
    }
    const float ms_per_degree = duration_ms / degrees;
    const float ms_per_180 = ms_per_degree * 180;

    SetTurnSpeed( static_cast<uint>( ms_per_180 ) );

    // ** complete the turn before we continue with the script
    prev_waypoint_check = game->GetGameTime();
    waypoint_duration = duration_ms;

    // **
    TurnToFaceDir( to_vec );
}

void Actor::RebootPatrolScript( void ) {
    cur_waypoint_index = 0;
    SetTurnSpeed( GetMaxTurnSpeed() );
}

void Actor::AI_FindEnemy( void ) {
    if ( pursued_entity.lock() ) {
        return;
    }
    
    if ( globalVals.GetBool( gval_ai_autoFindPlayer ) ) {
        pursued_entity = game->GetPlayerEntity();
    } else {
        AI_FindEnemyInSight();
    }
}

bool Actor::AI_FindFoodInRoom( void ) {
    if ( auto pursue_ent = pursued_entity.lock() )
        return true;
        
    // ** no need to do a food check every single frame
    if ( game->GetGameTime() - prevFoodCheck < foodCheckDelay )
        return true;

/*todo:navmeshsearch    
    prevFoodCheck = game->GetGameTime();
    
    auto food_ents_in_room = game->GetEntitiesInCurRoomWithType( TypeID_BugFood );
    
    if ( food_ents_in_room.size() > 0 ) {
        pursued_entity = GetClosestEntityInVector( food_ents_in_room );
        
        if ( auto foodEnt = pursued_entity.lock() ) {
            const float dist_sq = ( foodEnt->GetOrigin() - GetOrigin() ).SquaredLen();
            const float sense_range_sq = powf( 2, aiSenseFoodRange );
            if ( dist_sq > sense_range_sq )
                pursued_entity.reset();
        }
        
        return true;
    }
  */  
    return false;
}

bool Actor::AI_FindFoodInSight( void ) {
    if ( auto pursue_ent = pursued_entity.lock() )
        return true;

    // ** no need to do a food check every single frame
    if ( game->GetGameTime() - prevFoodCheck < foodCheckDelay )
        return true;
        
    prevFindCheck = game->GetGameTime();

    std::vector< CollisionItem > items_in_sight;
    GetEntitiesInSight( items_in_sight, TypeID_BugFood );
    
    for ( auto & item : items_in_sight ) {
        std::shared_ptr< BugFood > food_sptr = std::static_pointer_cast< BugFood >( item.ent.lock() );
        if ( food_sptr ) {
            if ( !(food_sptr->IsFoodForEnt( *this )) || !food_sptr->IsUsable() ) {
                continue;
            }
        }
    }
    
    pursued_entity = GetClosestEntityInList( items_in_sight );

    if ( pursued_entity.lock() )
        return true;
    
    return false;
}

Vec3f Actor::AI_GetNewWanderDirection( void ) {
    wanderDur = Random::UInt( wanderMinDur, wanderMaxDur );

    prevWander = game->GetGameTime();

    Vec3f dir;
    if ( AI_IsAvoidingPlayer() && IsDestructible() ) {
        std::shared_ptr< Actor > player = game->GetPlayer();
        if ( player ) { //TODO JESSE/BRANDON :: Check if player and bug are in same room, otherwise ignore?
            const Vec3f player_pos = player->GetOrigin();
            
            const float fleeTriggerRange_sq = powf( 2, aiFleeTriggerRange );
            const float distFromPlayer_sq = Vec3f( player_pos - GetOrigin() ).SquaredLen();
            
            if ( distFromPlayer_sq < fleeTriggerRange_sq ) {
                const Vec3f awayFromPlayer = GetOrigin() - player_pos;
                Mat3f randomRotation;
                randomRotation.Rotate( Random::Float( -AI_AVOID_PLAYER_ARC_ANGLE, AI_AVOID_PLAYER_ARC_ANGLE ), 0, 0, 1 );
                dir = awayFromPlayer * randomRotation;     
            } else {
                dir = Random::DirectionXY();
            }
        }
    } else  {
        dir = Random::DirectionXY();
    }

    return dir;
}

bool Actor::AI_IsAvoidingPlayer( void ) const {
    return GetAIAvoidPlayer();
}

bool Actor::AI_Wander( void ) {
    Vec3f dir;
    auto ob = obstacle.lock();

    if ( ob || ( game->GetGameTime() - prevWander > wanderDur ) ) {
        dir = AI_GetNewWanderDirection();
        obstacle.reset();
    }

    SetMoveVec( dir );
    TurnToFaceDir( dir );
    return true;
}

bool Actor::AI_FindEscape( const TypeID desired_exit_type ) {
    if ( desired_exit_type == MAX_UINT )
        return false;
        
    if ( auto pursue_ent = pursued_entity.lock() )
        if ( derives_from( desired_exit_type, *pursue_ent ) )
            return true;

    // ** no need to do an escape check every single frame
    const uint escapeCheckDelay = 250;
    if ( game->GetGameTime() - prevEscapeCheck < escapeCheckDelay )
        return true;
        
/*todo:navmeshsearch        
    prevEscapeCheck = game->GetGameTime();
    
    auto exit_ents_in_room = game->GetEntitiesInCurRoomWithType( desired_exit_type );
    
    if ( exit_ents_in_room.size() > 0 ) {
        pursued_entity = GetClosestEntityInVector( exit_ents_in_room );
        return true;
    } 
    */
    return false;
}

void Actor::AI_BumpedObstacle( [[maybe_unused]] const std::shared_ptr< Entity > & ent ) {
}

void Actor::AI_HurtBy( const Entity& inflictor ) {
    if ( !GetIgnoreAgressor() ) {
        SetTurnSpeed( GetMaxTurnSpeed() );
        TurnTo( inflictor );
    }
}

void Actor::AI_Load( const AI_ZoneT zone, const std::vector< std::string >& actions ) {
    ASSERT( zone < AI_ZoneT::NUM_ZONES );
    for ( auto const & action : actions ) {
        const auto z = static_cast<AI_ZoneT_BaseType>(zone);
        if ( action == "idle" ) { ai_actions[z].emplace_back( AI_ActionT::Idle );
        } else if ( action == "attack1" ) { ai_actions[z].emplace_back( AI_ActionT::Attack1 );
        } else if ( action == "attack2" ) { ai_actions[z].emplace_back( AI_ActionT::Attack2 );
        } else if ( action == "flee" ) { ai_actions[z].emplace_back( AI_ActionT::Flee );
        } else if ( action == "chase" ) { ai_actions[z].emplace_back( AI_ActionT::Chase );
        } else if ( action == "stalk" ) { ai_actions[z].emplace_back( AI_ActionT::Stalk );
        } else if ( action == "interact" ) { ai_actions[z].emplace_back( AI_ActionT::Interact );
        } else if ( action == "special1" ) { ai_actions[z].emplace_back( AI_ActionT::Special_1 );
        } else { ERR("Unknown ai action: %s (%s)\n", action.c_str(), GetIdentifier().c_str() );
        }
    }
}

void Actor::AI_Load( void ) {
    AI_Load( AI_ZoneT::Near, ai_NearActions );
    AI_Load( AI_ZoneT::Mid, ai_MidActions );
    AI_Load( AI_ZoneT::Far, ai_FarActions );
}

AI_ZoneT Actor::AI_GetZone( const float distToTarget ) const {
    if ( distToTarget < ai_NearMax )
        return AI_ZoneT::Near;
            
    if ( distToTarget < ai_MidMax )
        return AI_ZoneT::Mid;
        
    if ( distToTarget < ai_FarMax )
        return AI_ZoneT::Far;
    
    return AI_ZoneT::Lost;
}

bool Actor::AI_Attack1( Entity& target ) {
    if ( !IsReadyToAttack1() )
        return false;
        
    if ( !CanSee( target ) )
        return false;
        
    const Vec3f dir = (target.GetOrigin() - GetOrigin() ).GetNormalized();
    TurnToFaceDir( dir );
    modelInfo.AddAnimState( AnimStateT::Attack1 );
    return true;
}

bool Actor::AI_Interact( Entity& target ) {      
    if ( !CanSee( target ) )
        return false;
        
    const Vec3f dir = (target.GetOrigin() - GetOrigin() ).GetNormalized();
    TurnToFaceDir( dir );
    modelInfo.AddAnimState( AnimStateT::Interact );
    return true;
}

bool Actor::AI_Special1( void ) {
    ERR("AI_Special 1 intended to be overridden by derived classes");
    return false;
}

bool Actor::AI_Stalk( [[maybe_unused]] Entity& target ) {
    return true;
}

bool Actor::AI_Idle( void ) {
    return true;
}

bool Actor::AI_Attack2( Entity& target ) {
    if ( !IsReadyToAttack2() )
        return false;
        
    if ( !CanSee( target ) )
        return false;
        
    const Vec3f dir = (target.GetOrigin() - GetOrigin() ).GetNormalized();
    TurnToFaceDir( dir );
    modelInfo.AddAnimState( AnimStateT::Attack2 );
    return true;
}

bool Actor::AI_Chase( Entity& target ) {
    const Vec3f enemy_origin( target.GetOrigin() );
    navPath = NavMesh::GetPath( *this, Vec3f(enemy_origin.x, enemy_origin.y, 0), maxPolyChase );
    AI_FollowPath( target.bounds.GetRadius() );
    return true;
}

bool Actor::AI_Flee( const Vec3f dir ) {
   
    const Vec3f abs_dir = Maths::Fabsf( dir );
    float max_val = Max3( abs_dir.x, abs_dir.y, abs_dir.z );
    
    if ( Maths::Approxf( max_val, 0, EP_TEN_THOUSANDTH ) ) 
        WARN("Actor \"%s\" flee direction not defined\n", GetIdentifier().c_str() );

    SetFacingDir( dir );
    SetMoveVec( dir );
    
    return true;
}

bool Actor::AI_DoState_Actions( Entity& target ) {
    SetTurnSpeed( GetMaxTurnSpeed() );

    const float my_radius = bounds.GetRadius();
    const float enemy_radius = target.bounds.GetRadius();
    const Vec3f my_origin = GetOrigin();
    const Vec3f enemy_origin = target.GetOrigin();
    const float dist = ( enemy_origin - my_origin ).Len() - my_radius - enemy_radius;
    
    const AI_ZoneT zone = AI_GetZone( dist );
    ASSERT( zone < AI_ZoneT::NUM_ZONES );
    
    for ( auto const & action : ai_actions[static_cast<AI_ZoneT_BaseType>(zone)] ) {
        switch ( action ) {
            case AI_ActionT::Attack1:
                if ( AI_Attack1( target ) )
                    return true;
                break;
            
            case AI_ActionT::Attack2:
                if ( AI_Attack2( target ) )
                    return true;
                break;

            case AI_ActionT::Stalk:
                if ( AI_Attack2( target ) )
                    return true;
                break;

            case AI_ActionT::Chase:
                if ( AI_Chase( target ) )
                    return true;
                break;
            
            case AI_ActionT::Flee:
                if ( AI_Flee() )
                    return true;
                break;
                
            case AI_ActionT::Idle:
                if ( AI_Idle() )
                    return true;
                break;
                
            case AI_ActionT::Wander:
                if ( AI_Wander() )
                    return true;
                break;
                
            case AI_ActionT::Interact:
                if ( AI_Interact( target ) )
                    return true;
                break;
                
            case AI_ActionT::Special_1:
                if ( AI_Special1() )
                    return true;
                break;
            default:
                ERR("Unknown ai action: %u (%s)\n", static_cast<AI_ZoneT_BaseType>(action), GetIdentifier() );
        }
    }
    
    return false;
}

void Actor::DoAI( void ) {
    const uint curFrame = game->GetGameFrame();
    if ( curFrame % 2 )
        return; // AI runs every-other-frame
            
    if ( IsStunned() )
        return;

    AI_FindEnemy();
    
    if ( auto enemy = pursued_entity.lock() ) {
        if ( AI_DoState_Actions( *enemy ) ) {
            return;
        }
    }
    
    //AI_ExecutePatrolScript();  //old, may bring back
    AI_FollowPath();
}
