// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entity.h"
#include "../game.h"
#include "../math/collision/collision.h"
#include "../rendering/renderer.h"
#include "../damage/damageManager.h"
#include "../misc.h"

EntityClipTypeT Entity::GetClipType( MatchOptsT_BaseType opts ) const {
    // MatchOptsT will override anything we have set on this entity, such as clipBoundsOnly

    if ( ( opts & MatchOptsT::MATCH_TEST_ONLY_BOUNDS ) || GetClipBoundsOnly() )
        return EntityClipTypeT::BOUNDS_ONLY;
    
    if ( modelInfo.HasClipModel() )
        if ( globalVals.GetBool(gval_d_clipModels) || ( ( opts & MATCH_IGNORE_INVISIBLE_CLIPMODEL ) == 0 ) )
            return EntityClipTypeT::CLIP_MODEL;
            
    if ( !bounds.IsSphere() ) // sphere entities will not test against draw model
        if ( modelInfo.HasDrawModel() )
            return EntityClipTypeT::DRAW_MODEL;
        
    return EntityClipTypeT::BOUNDS_ONLY;
}

bool Entity::ReactsToPhysicsAgainst( const Entity& ent ) const {
    if ( &ent == this )
        return false;

    if ( phys_flags & PHYSFLAG_NO_PHYS )
        return false;
        
    return true;
}

bool Entity::ReactsToPhysicsTogether( const Entity& ent ) const {
    return
        ( this != &ent ) &&
        ( ent.ReactsToPhysicsAgainst( *this ) ) && 
        ( this->ReactsToPhysicsAgainst( ent ) )
    ;
}

// SomeDerivedEntityClass::BumpedIntoBy() should handle calling this if it wants to
void Entity::Push( [[maybe_unused]] Entity& ent_unused, const Vec3f& frame_vel ) {
    if ( !IsMovable() )
        return;
        
    Vec3f velocity = bounds.GetVelocity();
    
    const float len = frame_vel.Len() * FPS_CAP;
    velocity += frame_vel * (len * FPS_CAP);
    const float add = velocity.Len();
    velocity += add;
    
    /*
        todo:
        if ( !IsAwake() ) {
            SetAwake( AwakeStateT::AwakeUntilVelocityStops )
        }
    */
    
    bounds.SetVelocity( velocity );
}

void Entity::BumpInto( [[maybe_unused]] Entity& ent ) {
}

void Entity::BumpedIntoBy( Entity& ent ) {
    Bumped_DoTouchDamage( ent );
    Bumped_DoFire( ent );
}

void Entity::Bumped_DoTouchDamage( Entity& ent ) {
    if ( touchDamage.size() == 0 )
        return;

    // player doesn't damage self, enemies don't damage enemies
    if ( GetTeamName() == ent.GetTeamName() )
        return;

    //TODO Brandon from Jesse (entvals) : This code below is creating a list of entities that have caused "touchDamage" using GetIndex() to refer to "unique" entities.
    // won't this be a problem considering that Game::RemoveEntity is constantly changing those indexes via PopSwap() ?  As soon as something dies that "unique" reference could be referring
    // to a whole new entity.
    std::string prevTouchDamageStr( "prevDmg_UniqueEnt" ); //toNamedSetter
    prevTouchDamageStr += std::to_string( GetIndex() );
    uint prevTouchDamage = entVals.GetUInt( prevTouchDamageStr.c_str() ); //toNamedSetter

    if ( game->GetGameTime() - prevTouchDamage < touchDamageCooldown )
        return;

    if ( const auto damageDef = damageManager.Get( touchDamage.c_str() ) )
        damageDef->Inflict( *this, ent.GetWeakPtr() );

    entVals.SetUInt( prevTouchDamageStr.c_str(), game->GetGameTime() ); //toNamedSetter
}

void Entity::Bumped_DoFire( Entity& ent ) {
    if ( IsOnFire() ) {
        ent.TrySetOnFire();
        return;
    }
    
    if ( ent.IsOnFire() ) {
        TrySetOnFire();
    }
}

bool Entity::Intersects( const Primitive& primitive, const IntersectData& coldata ) const {
    const EntityClipTypeT clipType = GetClipType(coldata.opts);
    return Intersects( primitive, clipType );
}

bool Entity::Intersects( const Primitive& primitive, const EntityClipTypeT clipType ) const {
    const bool boundsClips = bounds.primitive->Intersects( primitive );
    if ( ! boundsClips )
        return false;
    
    std::shared_ptr< const Model > model;
    switch ( clipType ) {
        case EntityClipTypeT::CLIP_MODEL:
            model = modelInfo.GetClipModel();
            break;
        case EntityClipTypeT::DRAW_MODEL:
            model = modelInfo.GetModel();
            break;
        case EntityClipTypeT::BOUNDS_ONLY: return true;
        default:
            DIE("Entity::Intersects: unknown EntityClipTypeT\n");
            return false;
    }
    
    if ( ! model )
        return false;
        
    return primitive.Intersects( *model, bounds.primitive->GetAngles(), GetOrigin() );
}

bool Entity::SweepWith( CollisionReport3D& report, const Primitive& primitive, const SweepData& coldata ) const {
    const EntityClipTypeT clipType = GetClipType(coldata.opts);

    if ( clipType != EntityClipTypeT::BOUNDS_ONLY ) {
        
        std::shared_ptr< const Model > model;
        
        if ( clipType == EntityClipTypeT::CLIP_MODEL ) {
            model = modelInfo.GetClipModel();
        } else {
            model = modelInfo.GetModel();
        }
    
        if ( model ) {
            return primitive.SweepTo( report, coldata.vel, *model, bounds.primitive->GetAngles(), GetOrigin() );
        }
    }
    
    return primitive.SweepTo( report, coldata.vel, *bounds.primitive );
}

std::shared_ptr< Entity > ClickSweep( const MatchOptsT_BaseType matchType, const TypeID desired_type ) {
    // when clicking, we ignore the collision model since we're clicking at the model that we can see
    SweepData coldata;
    coldata.opts = matchType | MatchOptsT::MATCH_NEAREST | MatchOptsT::MATCH_IGNORE_INVISIBLE_CLIPMODEL;

    // make sure we don't click entities without models unless we can actually see them
    const bool draw_bboxes = globalVals.GetBool( gval_d_bounds );
    if ( !draw_bboxes )
        coldata.opts |= MatchOptsT::MATCH_ENTS_WITH_MODELS_ONLY;
        
    CollisionReport3D report( CollisionCalcsT::Intersection | CollisionCalcsT::Normal );
    coldata.desired_type = desired_type;

    const Line3f ray( GetPickLine() );
    coldata.vel = ray.vert[1] - ray.vert[0];
    
    return GlobalSweep( report, coldata, Primitive_Point( ray.vert[0] ) );
}

std::shared_ptr< Entity > GlobalTrace( const IntersectData& coldata, const Primitive& primitive, std::vector< CollisionItem >* collisions_out ) {
    LinkList< EntityTree * > voxelList;
    game->entTree->GetTraceVoxels( primitive, voxelList, VoxelTypeT::Occupied );

    if ( voxelList.Num() < 1 )
        return nullptr;

    return Collision3D::TraceToEntityVoxelList( coldata, primitive, voxelList, collisions_out );
}

std::shared_ptr< Entity > GlobalSweep( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive ) {
    LinkList< EntityTree * > voxelList;
    game->entTree->GetSweepVoxels( primitive, coldata.vel, voxelList, VoxelTypeT::Occupied );

    if ( voxelList.Num() < 1 )
        return nullptr;
        
    if ( coldata.opts & MATCH_NEAREST )
        EntityTree::SortVoxelList( primitive.GetOrigin(), voxelList );

    return Collision3D::SweepToEntityVoxelList( report, coldata, primitive, voxelList );
}

std::shared_ptr< Entity > GlobalSweep_VerticalLine( CollisionReport3D& report, const Vec3f& point ) {
    const Vec3f add( globalVals.GetVec3f( gval_grav_dir , Vec3f(0,0,-1) ) * globalVals.GetVec3f( gval_g_octreeSize ) );
    const Vec3f start( point - add );
    
    SweepData coldata;
    coldata.vel = add * 2;
    return GlobalSweep( report, coldata, Primitive_Point(start) );
}

void Entity::RemovePhysFlag( const EntityPhysicsFlagsT_BaseType flag ) {
    phys_flags &= ~flag;
}

void Entity::AddPhysFlag( const EntityPhysicsFlagsT_BaseType flag ) {
    phys_flags |= flag;
}

bool Entity::GetPhysFlag( const EntityPhysicsFlagsT_BaseType flag ) const {
    return phys_flags | flag;
}

std::shared_ptr< const Model > Entity::GetClipModel( void ) const {
    return modelInfo.GetClipModel();
}

void Entity::SetClipModel( const std::shared_ptr< const Model >& to ) {
    modelInfo.SetClipModel( to );
}

bool Entity::HasClipModel( void ) const {
    return modelInfo.HasClipModel();
}

bool Entity::GetNoGrav( void ) const {
    return phys_flags & PHYSFLAG_NO_GRAV;
}
    
bool Entity::GetNoPhys( void ) const {
    return phys_flags & PHYSFLAG_NO_PHYS;
}

bool Entity::IsOnGround( void ) const {
    return phys_flags & PHYSFLAG_IS_ON_GROUND;
}

Vec3f Entity::GetVelocity( void ) const {
    return bounds.GetVelocity();
}

void Entity::SetVelocity( const Vec3f& vec ) {
    bounds.SetVelocity( vec );
}

void Entity::SetNoGrav( const bool whether ) {
    if ( whether ) {
        phys_flags |= PHYSFLAG_NO_GRAV;
    } else {
        phys_flags &= ~PHYSFLAG_NO_GRAV;
    }
}

void Entity::SetNoPhys( const bool whether ) {
    if ( whether ) {
        phys_flags |= PHYSFLAG_NO_PHYS;
    } else {
        phys_flags &= ~PHYSFLAG_NO_PHYS;
    }
}

bool Entity::GhostsThrough( const Entity& ent ) const {
    if ( GhostsThroughThem( ent ) )
        return true;
    
    if ( ent.GhostsThroughUs( * this ) )
        return true;
        
    return false;
}

bool Entity::GhostsThroughThem( const Entity& ent ) const {
    return ( phys_flags & PHYSFLAG_GHOST_THEM  ) || ( ent.phys_flags & PHYSFLAG_GHOST_US );
}

bool Entity::GhostsThroughUs( const Entity& ent ) const {
    return ent.GhostsThroughThem( *this );
}

void Entity::SetOnGround( const bool on_ground ) {
    if ( on_ground ) {
        phys_flags |= PHYSFLAG_IS_ON_GROUND;
    } else {
        phys_flags &= ~PHYSFLAG_IS_ON_GROUND;
    }
}

bool Entity::IsGravityEnabled( void ) const {
    if ( GetNoGrav() )
        return false;
        
    if ( GetNoPhys() )
        return false;

    const bool worldWideNoGrav = globalVals.GetBool( gval_grav_disable );
    if ( worldWideNoGrav )
        return false;

    return true;
}

void Entity::DoVelocity( void ) {
    bounds.DoVelocity_Slide();
}

void Entity::SetGravityRatio( const float to ) {
    entVals.SetFloat( "gravity", to ); //toNamedSetter
}
