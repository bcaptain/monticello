// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_SWEEP_DEBUG_H
#define SRC_ENTITIES_SWEEP_DEBUG_H

#include "./clib/src/warnings.h"
#include "../entity.h"


class SweepDebug : public Entity {
public:
    static constexpr TypeInfo<SweepDebug, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    SweepDebug( void ) : ent_start(), ent_end(), num_sweeps(5) {};
    virtual ~SweepDebug( void ) {};

public:
    void Spawn( void ) override;
    void RunFrame( void ) override;

protected:
    bool ReactsToPhysicsAgainst( const Entity& ent ) const override;
    
private:
    std::shared_ptr< Entity > ent_start;
    std::shared_ptr< Entity > ent_end;
    uint num_sweeps;

public:
    static_assert( is_parent_of<SweepDebug>( get_typeid<Entity>() ) );
    static ToolTip< SweepDebug, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_SWEEP_DEBUG_H
