// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./clipDebug.h"
#include "../../game.h"
#include "../../rendering/renderer.h"
#include "../../math/collision/collision.h"
#include "../entity.h"

const float DEFAULT_CLIPDEBUG_RADIUS = 1;
const float DEFAULT_CLIPDEBUG_LENGTH = 1;
const float DEFAULT_CLIPDEBUG_BOXSIZE = 1;

std::vector< std::weak_ptr< Entity > > ClipDebug::clipDebugs;

ClipDebug::ClipDebug( void ) 
    : color_noCollision(0,1,0,1)
    , color_collision(1,0,0,1)
    , color( &color_noCollision )
    , box_size(DEFAULT_CLIPDEBUG_BOXSIZE,DEFAULT_CLIPDEBUG_BOXSIZE,DEFAULT_CLIPDEBUG_BOXSIZE)
    , radius(DEFAULT_CLIPDEBUG_RADIUS)
    , length(DEFAULT_CLIPDEBUG_LENGTH)
{ }

ClipDebug::ClipDebug( const ClipDebug& other ) 
    : Entity(other)
    , color_noCollision( other.color_noCollision )
    , color_collision( other.color_collision )
    , color( &color_noCollision )
    , box_size(other.box_size)
    , radius(other.radius)
    , length(other.length)
{ }

void ClipDebug::Spawn( void ) {
    Entity::Spawn();
    
    for ( auto const & clipDebug : clipDebugs )
        if ( auto const & co = clipDebug.lock() )
            if ( co->GetIndex() == GetIndex() )
                return;
    clipDebugs.emplace_back( GetWeakPtr() );
    WakeUp();
}

void ClipDebug::RunFrame( void ) {
    Entity::RunFrame();
    
    color = CollidesWithOthers() ? &color_collision : &color_noCollision;
}

void ClipDebug::UpdatePrimitive( void ) {
    switch ( bounds.GetShape() ) {
        case PrimitiveShapeT::SPHERE: bounds.SetToSphere( radius ); break;
        case PrimitiveShapeT::AABOX3D: bounds.SetToAABox3D( box_size ); break;
        case PrimitiveShapeT::BOX3D: bounds.SetToBox3D( box_size ); break;
        case PrimitiveShapeT::CYLINDER: UpdateCyl(); break;
        case PrimitiveShapeT::LINE3F: FALLTHROUGH;
        case PrimitiveShapeT::CAPSULE: FALLTHROUGH;
        case PrimitiveShapeT::VEC3F: FALLTHROUGH;
        default:
            ERR("unimplemented clipDebug shape: %u, changing back to sphere\n", static_cast<uint>(bounds.GetShape()) );
            bounds.SetToSphere( radius );
    }
}

void ClipDebug::UpdateCyl( void ) {
    bounds.SetToCyl();
    
    Primitive_Cylinder* cyl = static_cast< Primitive_Cylinder* >( bounds.primitive.get() );
    assert( cyl );
    cyl->SetRadius( radius );
    cyl->SetLength( length );
}

void ClipDebug::DrawVBO( [[maybe_unused]] const ModelDrawInfo& drawInfo ) const {
    if ( !IsSpawned() )
        return;
        
    bounds.DrawVBO( *color );
    DrawNameVBO();
}

bool ClipDebug::CollidesWithOthers( void ) const {
    for ( auto const & clipDebug : clipDebugs ) {
        auto const & ent = clipDebug.lock();
        if ( !ent )
            continue;

        auto const & co = std::static_pointer_cast< ClipDebug >(ent);
        if ( !co )
            continue;
            
        if ( co->GetIndex() == GetIndex() )
            continue;
            
        if ( co->bounds.primitive->Intersects( *bounds.primitive ) )
            return true;
    }
    
    return false;
}
