// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./sweepDebug.h"
#include "../../game.h"
#include "../../math/geometry/3d/plane.h"
#include "../../rendering/renderer.h"

void SweepDebug::Spawn( void ) {
    Entity::Spawn();
    
    bounds.SetToSphere( 1 );

    ASSERT( ! ent_start );
    ASSERT( ! ent_end );

    ent_start = game->NewEnt( TypeID_Entity );
    ent_end = game->NewEnt( TypeID_Entity );
    ent_end = game->NewEnt( TypeID_Entity );

    ent_start->SetName("Start");
    ent_start->SetAlwaysShowName( true );
    ent_start->bounds.SetToAABox3D( 1 );
    ent_start->SetNoGrav( false );
    ent_start->SetOrigin( GetOrigin() - Vec3f(1,0,0) );
    ent_start->Spawn();
    ent_start->AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_GHOST_US );

    ent_end->SetName("End");
    ent_end->SetAlwaysShowName( true );
    ent_end->bounds.SetToAABox3D( 1 );
    ent_end->SetNoGrav( false );
    ent_end->SetOrigin( GetOrigin() - Vec3f(0,0,1) );
    ent_end->Spawn();
    ent_end->AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_GHOST_US );

    WakeUp();
}

bool SweepDebug::ReactsToPhysicsAgainst( const Entity& ent ) const {
    if ( ent.GetIndex() == ent_end->GetIndex() )
        return true;

    if ( ent.GetIndex() == ent_start->GetIndex() )
        return true;

    return Entity::ReactsToPhysicsAgainst( ent );
}

void SweepDebug::RunFrame( void ) {
    Entity::RunFrame();

    const Vec3f start_origin( ent_start->GetOrigin() );
    const Vec3f end_origin( ent_end->GetOrigin() );

    SetOrigin( start_origin );

    Vec3f velocity = end_origin - start_origin;

    for ( uint i=0; i<num_sweeps; ++i ) {
        CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
        if ( !bounds.Slide( velocity, 1, nullptr, &report ) )
            break; // nothing hit

        const Vec3f new_origin( GetOrigin() );

        const Color4f red(1,0,0,1);
        const Color4f green(0,1,0,1);
        const Color4f blue(0,0,1,1);
        const Color4f blue_transluscent(0,0,1,0.5f);

        renderer->DrawLine( start_origin, end_origin, green, green ); // draw original velocity
        renderer->DrawLine( new_origin, new_origin+velocity, red, red ); // draw resultant velocity
        renderer->DrawLine( report.point, report.point + report.norm * 5, blue, blue ); // draw collision's tangent plane normal

        // draw tangent plane ( in the form of a quad )
        std::vector< Vec3f > quad;
        quad.reserve(4);
        quad.emplace_back(-1,-1,0);
        quad.emplace_back(-1,1,0);
        quad.emplace_back(1,1,0);
        quad.emplace_back(1,-1,0);
        const Vec3f quad_normal = Plane3f::GetNormal( quad.data() );

        // rotate the quad's normal to match the collision's tangent plane normal
        const float angle = quad_normal.RadiansBetween( report.norm );
        const Vec3f axis = quad_normal.Cross( report.norm ).GetNormalized();
        const Quat rot( Quat::FromAxis( angle, axis ) );
        for ( auto & vert : quad ) {
            vert *= rot;
            vert += report.point;
        }

        renderer->DrawPoly( quad, blue_transluscent );
    }
}
