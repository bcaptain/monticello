// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_CLIP_DEBUG_H
#define SRC_ENTITIES_CLIP_DEBUG_H

#include "../../base/main.h"
#include "../../entities/entity.h"


class ClipDebug : public Entity {
public:
    static constexpr TypeInfo<ClipDebug, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    ClipDebug( void );
    ClipDebug( const ClipDebug& other );
    virtual ~ClipDebug( void ) {}
    
public:
    ClipDebug& operator=( const ClipDebug& other ) = delete;

public:
    void Spawn( void ) override;
    void RunFrame( void ) override;
    using Entity::DrawVBO;
    void DrawVBO( const ModelDrawInfo& drawInfo ) const override;
    
private:
    void UpdatePrimitive( void );
    void UpdateCyl( void );
    void SetShape( void );
    bool CollidesWithOthers( void ) const;

protected:
    static std::vector< std::weak_ptr< Entity > > clipDebugs;
    Color4f color_noCollision;
    Color4f color_collision;
    Color4f* color;
    Vec3f box_size;
    float radius;
    float length;
    
public:
    static_assert( is_parent_of<ClipDebug>( get_typeid<Entity>() ) );
    static ToolTip< ClipDebug, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_CLIP_DEBUG_H
