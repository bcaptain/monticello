// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_BUGFOOD_H
#define SRC_ENTITIES_BUGFOOD_H

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./entity.h"


class BugFood : public Entity {
public:
    static constexpr TypeInfo<BugFood, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    BugFood( void );
    ~BugFood( void ) override;
    BugFood( const BugFood& other );

public:
    BugFood& operator=( const BugFood& other );

public:
    void Spawn( void ) override;
    
public:
    bool UseBy( Actor* user = nullptr ) override;
    
    bool IsFoodForEnt( const Entity& ent ) const;
    void SetFoodForTypeID( const TypeID user_type );
    TypeID GetUserType( void ) const;
    bool IsUsable( void ) const override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    SETTER_GETTER( DefaultUserType, std::string, defaultUserType );
    SETTER_GETTER_BYVAL( FoodStock, uint, foodStock );
    TypeID userType;

// ** Named Setters
private:
    static const uint BugFood_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum BugFoodSetterT : NamedSetterT_BaseType {
        BugFood_SetFoodStock = BugFood_SETTERS_START
        , BugFood_SetDefaultUserType
        , BugFood_SETTERS_END
        , BugFood_NUM_SETTERS = BugFood_SETTERS_END - BugFood_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };
private:
    static std::vector< std::string > bugFoodSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );
    
// ** ToolTips
public:
    static_assert( is_parent_of<BugFood>( get_typeid<Entity>() ) );
    static ToolTip< BugFood, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_BUGFOOD_H
