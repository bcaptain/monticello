// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "./garbageBin.h"
#include "./entityInfoManager.h"
#include "../sprites/sprite.h"
#include "../rendering/animation/animInfo.h"

const auto ANIMSTATE_GARBAGEBIN_INFESTED = AnimStateT::Special1;
const float USE_LIMIT = 1.0f;
const Uint32 IN_USE_TIMER_THRESHOLD = 250;
const float USE_VALUE_PER_FRAME = 0.025f;

const float DEFAULT_GARBAGEBIN_UI_EDGE_LENGTH = 0.8f;

const float MINIMUM_DISPLAYED_USE_VALUE = 0.01f;

GarbageBin::GarbageBin( void )
    : Entity()
    , useMeter()
    {
}

GarbageBin::GarbageBin( const GarbageBin& other ) 
    : Entity( other )
    , useMeter()
    {
}

GarbageBin::~GarbageBin( void ) {
}

void GarbageBin::SetMarkedForUse( const bool whether ) {
    if ( whether ) {
        if ( !IsUsable() )
            return;
        SpawnInspectUI();
    }        
    Entity::SetMarkedForUse( whether );
}

bool GarbageBin::UseBy( [[maybe_unused]] Actor* user_unused ) {
    
    if ( !IsUsable() )
        return false;
        
    useValue += USE_VALUE_PER_FRAME;
    lastTimeUsed = game->GetGameTime();
    
    if ( useValue > USE_LIMIT ) {
        SetInfested( false );
        SetDestructible( true );
        modelInfo.AddAnimState( AnimStateT::Die );
        return false;
    }
    
    return true;
}

void GarbageBin::UpdateUseValue( void ) {
    if ( !IsUsable() )
        return;
        
    if ( useValue > 0 ) {
        const Uint32 gameTime = game->GetGameTime();
    
        if ( gameTime - lastTimeUsed > IN_USE_TIMER_THRESHOLD ) {
            useValue -= USE_VALUE_PER_FRAME;
            useValue = std::max( useValue, 0.0f );
        }
    }
    
    UpdateInspectUIValue( );
}

void GarbageBin::SpawnInspectUI( void ) {
    const auto & mdl = GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;
    
    if ( !useMeter ) {
        useMeter = std::make_shared< Meter >(DEFAULT_GARBAGEBIN_UI_EDGE_LENGTH,DEFAULT_GARBAGEBIN_UI_EDGE_LENGTH);
        useMeter->SetMaterial( "inspectUI" );
        useMeter->SetMaskMaterial( "radialMaskUI" );
        useMeter->SetupUV();
    }
    
    Vec3f useMeterOrigin = GetOrigin();
    useMeterOrigin.x -= useMeter->GetWidth() / 2;
    useMeterOrigin.z = height + useMeter->GetHeight();
    
    useMeter->SetOrigin( useMeterOrigin );
    
    auto sprite = std::static_pointer_cast< Sprite >(useMeter);
    game->AddWorldUI( sprite );
}

void GarbageBin::UpdateInspectUIValue( void ) {
    if ( !useMeter )
        return;
        
    if ( !IsMarkedForUse() && useValue < MINIMUM_DISPLAYED_USE_VALUE ) {
        useMeter.reset();
        return;
    }
    
    useMeter->progress = useValue;
}

void GarbageBin::Think( void ) {
    if ( modelInfo.HasAnimState( AnimStateT::Die ) ) {
        Death();
        return;
    }
        
    if ( IsUsable() ) {
        modelInfo.AddAnimState( ANIMSTATE_GARBAGEBIN_INFESTED );
        modelInfo.RemAnimState( AnimStateT::Idle );
    } else {
        modelInfo.AddAnimState( AnimStateT::Idle );
        modelInfo.RemAnimState( ANIMSTATE_GARBAGEBIN_INFESTED );
    }
    
    UpdateUseValue();
    
    Entity::Think();
}

void GarbageBin::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( !animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        {
          "infested", ANIMSTATE_GARBAGEBIN_INFESTED, AnimOptT::None,
            {
                { AnimChannelT::Legs, 8000 }
            }
        }
        , { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
        , { "death", AnimStateT::Die, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

bool GarbageBin::IsUsable( void ) const {
    return GetInfested();
}

