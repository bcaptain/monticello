// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./basicBugSign.h"
#include "../rendering/animation/animInfo.h"

BasicBugSign::BasicBugSign( void )
    : Actor()
    {
}

BasicBugSign::BasicBugSign( const BasicBugSign& other ) 
    : Actor( other )
    {
}

BasicBugSign& BasicBugSign::operator=( const BasicBugSign& other ) {    
    Actor::operator=( other );
    return *this;
}

BasicBugSign::~BasicBugSign( void ) {
}

void BasicBugSign::Spawn( void ) {
    SetNoPhys(true);
    Actor::Spawn();
}

void BasicBugSign::Think( void ) {
    if ( GetOkayToDie() )
        Kill();
}

void BasicBugSign::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}
