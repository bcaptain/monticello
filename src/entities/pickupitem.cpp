// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./pickupitem.h"
#include "../game.h"
#include "../sound.h"
#include "../misc.h"

std::vector< std::string > PickupItem::pickupItemSetters;

void PickupItem::InitSetterNames( void ) {
    if ( pickupItemSetters.size() == PickupItem_NUM_SETTERS )
        return; // already set
    pickupItemSetters.resize(PickupItem_NUM_SETTERS);
    
    const uint i = PickupItem_SETTERS_START;
    pickupItemSetters[PickupItem_SetPickupSound-i] = "snd_pickup";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<PickupItem,Entity>(pickupItemSetters);
}

bool PickupItem::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<PickupItemSetterT>(setter) ) {
        case PickupItem_SetPickupSound: SetPickupSound( value ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case PickupItem_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case PickupItem_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        
        default:
            static_assert( is_parent_of<PickupItem>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string PickupItem::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<PickupItemSetterT>(setter) ) {
        case PickupItem_SetPickupSound: return GetPickupSound();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case PickupItem_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case PickupItem_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        
        default:
            static_assert( is_parent_of<PickupItem>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT PickupItem::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( pickupItemSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + PickupItem_SETTERS_START;
        
    static_assert( is_parent_of<PickupItem>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

PickupItem::~PickupItem( void ) {
}

bool PickupItem::AddHealth( Actor& player ) {
    const int addHealth = GetGiveHealth();
    if ( addHealth == 0 )
        return false;
        
    player.GiveHealth( addHealth );
    return true;
}

bool PickupItem::GiveItems( Actor& player ) {
    const std::vector< std::string > _items; //todo
    
    if ( _items.size() < 1 )
        return false;
        
    for ( auto const& itemName : _items )
        player.inventory.Add( Item( itemName.c_str(), itemName.c_str(), ItemTypeT::NORMAL ) );

    return true;
}

void PickupItem::PlayPickupSound( void ) {
    if ( snd_pickup.size() > 0 )
        soundManager.Play( snd_pickup.c_str() );
}

void PickupItem::BumpedIntoBy( Entity& ent ) {
    if ( pickedUp )
        return;

    if ( !derives_from< Actor >( ent ) )
        return;

    // only the player controlled right now can pick up items
    std::shared_ptr< Actor > player = game->GetPlayer();
    if ( ! player )
        return;

    if ( player.get() != static_cast< const Actor* > ( &ent ) )
        return;

    // attempt to give anything the item affords
    int numThingsGiven = AddHealth( *player ) ? 1 : 0;
    numThingsGiven += GiveItems( *player ) ? 1 : 0;

    if ( numThingsGiven < 1 )
        return;

    pickedUp = true;
    PlayPickupSound();
    MarkForDeletion();

    return;
}

PickupItem::PickupItem( void )
    : snd_pickup()
    , pickedUp(false)
{
    SetByPlayer(true);
}

PickupItem::PickupItem( const PickupItem& other )
    : Entity( other )
    , snd_pickup( other.snd_pickup )
    , pickedUp( false )
{ }

void PickupItem::Spawn( void ) {
    Entity::Spawn();
    
    AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_GHOST_US );
}

PickupItem& PickupItem::operator=( const PickupItem& other ) {
    snd_pickup = other.snd_pickup;
    pickedUp = other.pickedUp;
    Entity::operator=( other );
    return *this;
}

void PickupItem::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<PickupItem>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteBool( pickedUp );
    
    MapSaveSetters( saveFile, string_pool, pickupItemSetters );
}

void PickupItem::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<PickupItem>( get_typeid<Entity>() ) );

    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    saveFile.parser.ReadBool( pickedUp );
    
    
    MapLoadSetters( saveFile, string_pool, pickupItemSetters );
}

void PickupItem::SetPickupSound( const std::string& pickup_snd ) {
    snd_pickup = pickup_snd;
}

std::string PickupItem::GetPickupSound( void ) const {
    return snd_pickup;
}
