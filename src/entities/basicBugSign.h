// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_BASICBUGSIGN_H
#define SRC_ENTITIES_BASICBUGSIGN_H

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./actor.h"


class BasicBugSign : public Actor {
public:
    static constexpr TypeInfo<BasicBugSign, Actor> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    BasicBugSign( void );
    ~BasicBugSign( void ) override;
    BasicBugSign( const BasicBugSign& other );

public:
    BasicBugSign& operator=( const BasicBugSign& other );
    
public:
    void Think( void ) override;
    void Animate( void ) override;
    void Spawn( void ) override;
    
// ** ToolTips
public:
    static_assert( is_parent_of<BasicBugSign>( get_typeid<Actor>() ) );
    static ToolTip< BasicBugSign, Actor > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_BASICBUGSIGN_H
