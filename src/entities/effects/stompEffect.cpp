// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./stompEffect.h"
#include "../../rendering/renderer.h"
#include "../../game.h"

const Uint DEFAULT_NUM_STOMP_CIRCLE_SEGMENTS = 32;
const float DEFAULT_STOMP_CIRCLE_MIN_RADIUS = 0.3f;
const Color4f DEFAULT_STOMP_CIRCLE_COLOR(1.0f,1.0f,1.0f,1.0f);

std::vector< std::string > StompEffect::stompEffectSetters;

void StompEffect::InitSetterNames( void ) {
    if ( stompEffectSetters.size() == StompEffect_NUM_SETTERS )
        return; // already set
    stompEffectSetters.resize(StompEffect_NUM_SETTERS);
    
    const uint i = StompEffect_SETTERS_START;
    stompEffectSetters[StompEffect_SetGameTimeOfDeath-i] = "timeOfDeath";
    stompEffectSetters[StompEffect_SetMaxLifetime-i] = "maxLifeTime";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<StompEffect,Entity>(stompEffectSetters);
}

bool StompEffect::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<StompEffectSetterT>(setter) ) {
        case StompEffect_SetGameTimeOfDeath: SetGameTimeOfDeath( String::ToUInt(value) ); return true;
        case StompEffect_SetMaxLifetime: SetMaxLifetime( String::ToUInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case StompEffect_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case StompEffect_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<StompEffect>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string StompEffect::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<StompEffectSetterT>(setter) ) {
        case StompEffect_SetGameTimeOfDeath: return String::ToString(GetGameTimeOfDeath());
        case StompEffect_SetMaxLifetime: return String::ToString(GetMaxLifetime());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case StompEffect_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case StompEffect_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<StompEffect>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT StompEffect::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( stompEffectSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + StompEffect_SETTERS_START;
        
    static_assert( is_parent_of<StompEffect>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

StompEffect::StompEffect( void )
    : Entity() 
    , vbo_vert( new VBO<float>(3, VBOChangeFrequencyT::RARELY ) )
    , vbo_color( new VBO<float>(4, VBOChangeFrequencyT::RARELY ) )
    , circles()
    , lifetime_delta()
    , max_radius()
    , timeOfDeath()
    , maxLifeTime()
{ }

StompEffect::StompEffect( const StompEffect& other )
    : Entity()
    , vbo_vert( new VBO<float>(3, VBOChangeFrequencyT::RARELY ) )
    , vbo_color( new VBO<float>(4, VBOChangeFrequencyT::RARELY ) )
    , circles(other.circles)
    , lifetime_delta(other.lifetime_delta)
    , max_radius(other.max_radius)
    , timeOfDeath(other.timeOfDeath)
    , maxLifeTime(other.maxLifeTime)
{ }

StompEffect& StompEffect::operator=( const StompEffect& other ) {
    if ( this == &other )
        return *this;
        
    vbo_vert = other.vbo_vert;
    vbo_color = other.vbo_color;
    circles = other.circles;
    lifetime_delta = other.lifetime_delta;
    max_radius = other.max_radius;
    timeOfDeath = other.timeOfDeath;
    maxLifeTime = other.maxLifeTime;

    return *this;
}

StompEffect::~StompEffect( void ) {
    delete vbo_vert;
    delete vbo_color;
}

void StompEffect::PackStompEffectVBO() {
    
    vbo_vert->Clear();
    vbo_color->Clear();
    
    Color4f circleColor( DEFAULT_STOMP_CIRCLE_COLOR );
    
    float radius, circleLifeTimeDelta;
    const float CIRCLE_RADIUS_RANGE = max_radius - DEFAULT_STOMP_CIRCLE_MIN_RADIUS;
    
    Uint gameTime = game->GetGameTime();
    
    for ( auto& circle : circles ) {
        circleLifeTimeDelta = ( maxLifeTime - ( circle.timeOfDeath - gameTime )) / static_cast<float>(maxLifeTime);
        radius = DEFAULT_STOMP_CIRCLE_MIN_RADIUS + ( circleLifeTimeDelta * CIRCLE_RADIUS_RANGE );
        circleColor.a = 1 - circleLifeTimeDelta;
        PackStompCircleVBO( DEFAULT_NUM_STOMP_CIRCLE_SEGMENTS, radius, circleColor, Vec3f(0.0f,0.0f,-1.0f) );
    }
    
    vbo_vert->MoveToVideoCard();
    vbo_color->MoveToVideoCard();
}

void StompEffect::PackStompCircleVBO( const uint num_subdivides, const float radius, const Color4f& color,  const Vec3f& normal ) {
    ASSERT( num_subdivides >= 3 );

    if ( normal == Vec3f(0.0f,0.0f,0.0f) ) {
        ERR( "PackStompCircle received a normal of zero length (i.e. no direction)\n" );
        return;
    }

    std::vector<Vec3f> verts;
    GenerateCircleVerts( num_subdivides, radius, verts );
    
    //calculate rotation quat to align this circle vertices to the requested axis
    Vec3f circAxisZ = Vec3f(0.0f,0.0f,1.0f);
    if ( circAxisZ != Maths::Fabsf( normal ) ) { //if target axis is parallel to the original axis, no rotation necessary, cross product will be 0
        Vec3f targetAxis = normal.GetNormalized();
        Vec3f rotAxis = circAxisZ.Cross( targetAxis ).GetNormalized();
        float rotRadians = acosf( circAxisZ.Dot( targetAxis ) );
        
        Quat alignmentQuat( Quat::FromAxis( rotRadians, rotAxis ) );
        
        for( auto& vert : verts ) //apply alignnment to all verts
            vert *= alignmentQuat;
    }
    
    //Pack circle for GL_LINES
    for( std::vector<Vec3f>::size_type i = 0; i < verts.size(); i++ ) {
        vbo_vert->Pack( verts[i] );
        if ( i != verts.size() - 1 ) {
            vbo_vert->Pack( verts[i+1] );
        } else {
            vbo_vert->Pack( verts[0] ); //if at the end, complete the circle
        }
        vbo_color->PackRepeatedly(2, color );
    }
}

void StompEffect::DrawVBO( [[maybe_unused]] const float opacity_unused ) const {
        
    ASSERT( vbo_vert->Finalized() );
    
    shaders->UseProg(  GLPROG_GRADIENT  );
    
    renderer->PushMatrixMV();
        shaders->SendData_Matrices();
    renderer->PopMatrixMV();
    
    shaders->SetAttrib( "vColor", *vbo_color );
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->DrawArrays( GL_LINES, 0, vbo_vert->Num() );
}

void StompEffect::Spawn( void ) {
    
    timeOfDeath = game->GetGameTime() + maxLifeTime;
    
    circles.emplace_back( game->GetGameTime() + maxLifeTime );
    
    PackStompEffectVBO();
    
    Entity::Spawn();
    
    WakeUp();
}

void StompEffect::Think( void ) {
    
    Uint gameTime = game->GetGameTime();
    if ( gameTime > timeOfDeath ) {
        Kill();
        return;
    }
    
    lifetime_delta = ( maxLifeTime - ( timeOfDeath - gameTime )) / static_cast<float>(maxLifeTime);
    
    if ( circles.size() < 3 ) {
        if ( lifetime_delta > 0.05f && lifetime_delta < 0.10f && circles.size() < 2 ) {
            circles.emplace_back( gameTime + maxLifeTime );
        } else if ( lifetime_delta > 0.15f && circles.size() < 3 ) {
            circles.emplace_back( gameTime + maxLifeTime );
        }
    }
    
    PackStompEffectVBO(); // todo:jesse: every frame??
}

void StompEffect::SetRadius( const float _radius ) {
    max_radius = _radius;
}

void StompEffect::SetGameTimeOfDeath( const uint to ) {
    timeOfDeath = to;
}

uint StompEffect::GetGameTimeOfDeath( void ) const {
    return timeOfDeath;
}

void StompEffect::SetMaxLifetime( const uint to ) {
    maxLifeTime = to;
}

uint StompEffect::GetMaxLifetime( void ) const {
    return maxLifeTime;
}

void StompEffect::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<StompEffect>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, stompEffectSetters );
}

void StompEffect::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<StompEffect>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, stompEffectSetters );
}
