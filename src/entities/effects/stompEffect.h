// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_STOMPEFFECT_H
#define SRC_ENTITIES_STOMPEFFECT_H

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../entity.h"

struct StompCircle {
    
    StompCircle( void )
        : timeOfDeath()
        , lifeTimeMultiplier()
        { }

    StompCircle( const StompCircle& other )
        : timeOfDeath(other.timeOfDeath)
        , lifeTimeMultiplier( other.lifeTimeMultiplier)
        { }

    StompCircle& operator=( const StompCircle& other ) {
        timeOfDeath = other.timeOfDeath;
        lifeTimeMultiplier = other.lifeTimeMultiplier;
        return *this;
    }
    
    StompCircle( Uint _timeOfDeath )
        : timeOfDeath( _timeOfDeath )
        , lifeTimeMultiplier()
        { }
    
    Uint timeOfDeath;
    float lifeTimeMultiplier;
};

//TODO : Brandon from Jesse, integrate into effectsManager ?


class StompEffect : public Entity {
public:
    static constexpr TypeInfo<StompEffect, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    StompEffect( void );
    StompEffect( const StompEffect& other );
    StompEffect& operator=( const StompEffect& other );
    ~StompEffect( void );
    
public:
    void Spawn() override;
    void Think() override;

    void SetRadius( const float _radius );
    
    void SetGameTimeOfDeath( const uint to );
    uint GetGameTimeOfDeath( void ) const;
    
    void SetMaxLifetime( const uint to );
    uint GetMaxLifetime( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    using Entity::DrawVBO;
    void DrawVBO( const float opacity_unused ) const override;
    void PackStompEffectVBO( void );
    void PackStompCircleVBO( const uint num_subdivides, const float radius, const Color4f& color, const Vec3f& normal );
    
    void AddCircle( const Uint spawnTime );
    
private:
    VBO<float>* vbo_vert;
    VBO<float>* vbo_color;
    std::vector< StompCircle > circles;
    float lifetime_delta;
    float max_radius;
    uint timeOfDeath;
    uint maxLifeTime;
    
// ** Named Setters
private:
    static const uint StompEffect_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum StompEffectSetterT : NamedSetterT_BaseType {
        StompEffect_SetGameTimeOfDeath = StompEffect_SETTERS_START
        , StompEffect_SetMaxLifetime
        , StompEffect_SETTERS_END
        , StompEffect_NUM_SETTERS = StompEffect_SETTERS_END - StompEffect_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > stompEffectSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<StompEffect>( get_typeid<Entity>() ) );
    static ToolTip< StompEffect, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_STOMPEFFECT_H
