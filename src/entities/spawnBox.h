// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_SPAWNBOX_H_
#define SRC_ENTITIES_SPAWNBOX_H_

#include "./entity.h"

/*!

SpawnBox \n\n

used simply to spawn an entity if the area is available \n\n

**/


class SpawnBox : public Entity {
public:
    static constexpr TypeInfo<SpawnBox, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    bool SpawnEntity( const std::string& entity_name ) const; //!< spawn the entity of the specified name if there is room to do so

private:
    bool SpawnEntity( const EntityInfo& ent_info ) const;
    bool IsAreaAvailable( const Primitive& primitive ) const;
    
// ** ToolTips
public:
    static_assert( is_parent_of<SpawnBox>( get_typeid<Entity>() ) );
    static ToolTip< SpawnBox, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_SPAWNBOX_H_
