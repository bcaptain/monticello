// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "../editor/editor.h"
#include "../editor/undo/undo_entityAdd.h"
#include "./entityInfoManager.h"
#include "./entityInfo.h"
#include "./entity.h"
#include "../filesystem.h" // for OverrideWarning()

EntityInfoManager entityInfoManager;

bool EntityInfoManager::instantiated(false);

EntityInfoManager::EntityInfoManager( void )
    : AssetManager( "EntityInfo" )
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

bool EntityInfoManager::Load( File& opened_file ) {
    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open entity file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    AssetData< EntityInfo >* manifest_s = manifest.Get( name.c_str() );
    if ( !manifest_s ) {
        ERR_DIALOG("Couldn't load entity file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }

    // ** parse the file
    ParserSSV parser( opened_file );
    parser.SetCommentIndicator('#');
    std::pair<std::string, std::string> entVal;
    bool got_classid = false;
    std::shared_ptr< EntityInfo > entInfo=nullptr;

    while ( parser.ReadWord( entVal.first ) ) {
        // class should always be listed first in the .ent file so that we know how to create the entity
        if ( (!got_classid) && (entVal.first != "class") ) {
            ERR_DIALOG("The first entry in %s (from %s) was not \"class\"\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
            return false;
        }

        if ( entVal.first == "class" ) {
            if ( got_classid ) {
                ERR_DIALOG("EntityInfoManager::Load: More than one class type specified for %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
                return false;
            }

            parser.ReadWord( entVal.second );

            entInfo = std::make_shared< EntityInfo >( entVal.second.c_str() );
            EntityInfo::EntityInfoManager_Attorney::SetEntName( *entInfo, name );
            got_classid = true;

        } else if ( entVal.first == "entval" ) {
            parser.ReadString( entVal.first );
            parser.ReadString( entVal.second );
            OverridePairByKey( entInfo->entVals, entVal );
            
            auto GetSetterIndex = ALL_GET_SETTER_INDEX_PTRS[entInfo->GetObjectTypeID()];
            auto const setter_index = (*GetSetterIndex)(entVal.first);
            if ( setter_index == INVALID_SETTER )  {
                //tododlgerr
                ERR("EntityInfoManager::Load: Setter method %s (value %s) could not be found for %s in %s from %s\n", entVal.first.c_str(), entVal.second.c_str(), entInfo->GetEntName().c_str(), opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
                // no return
            } else {
                entInfo->entSets.emplace_back( setter_index, entVal.second );
            }

        } else if ( entVal.first == "include" ) {
            parser.ReadWord( entVal.second );
            auto info = entityInfoManager.Get( entVal.second.c_str() );
            if ( ! info ) {
                ERR_DIALOG("EntityInfoManager::Load: Could not include .ent file \"%s\" in %s from %s\n", entVal.second.c_str(), opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
                return false;
            } else {
                OverridePairsByKey( entInfo->entVals, info->entVals );
                OverridePairsByKey( entInfo->entMembers, info->entMembers );
            }
        } else if ( entVal.first == "entval_tooltip" ) {
            // just skip the info
        } else {
            parser.ReadWord( entVal.second );
            OverridePairByKey( entInfo->entMembers, entVal );

        }

        parser.SkipToNextNonblankLine();
    }

    const bool asset_existed = manifest_s->data != nullptr;
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );
    manifest_s->data = entInfo;

    CMSG_LOG("Loaded ent_info: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );

    return true;
}

std::shared_ptr< Entity > EntityInfoManager::LoadAndSpawn( const char* name, const Vec3f& origin ) {
    auto ent = Load( name, origin );
    if ( ent )
        ent->Spawn();
    return ent;
}

std::shared_ptr< Entity > EntityInfoManager::LoadAndSpawn( const EntityInfo& ent_info, const Vec3f& origin ) {
    auto ent = Load( ent_info, origin );
    if ( ent )
        ent->Spawn();
    return ent;
}

std::shared_ptr< Entity > EntityInfoManager::Load( const char* name, const Vec3f& origin ) {
    const std::shared_ptr< const EntityInfo > ent_info = Get( name );
    if ( !ent_info ) {
        ERR("EntityInfoManager::Load: There is no entity by that name: %s\n", name);
        return nullptr;
    }

    return Load( *ent_info, origin );
}

std::shared_ptr< Entity > EntityInfoManager::Load( const EntityInfo& ent_info, const Vec3f& origin ) {
    std::shared_ptr< Entity > ent = game->NewEnt( ent_info.GetObjectTypeID() );
    if ( ent ) {
        ent->LoadDefaults( ent_info );
        FinishEnt(*ent, origin);
    } else {
        ERR("EntityInfoManager::Load: Could not create entity: %s\n", ent_info.GetEntName().c_str() );
    }
    return ent;
}

std::shared_ptr< Entity > EntityInfoManager::Load_Unmanaged( const char* name, const Vec3f& origin ) {
    const std::shared_ptr< const EntityInfo > ent_info = Get( name );
    if ( !ent_info ) {
        ERR("EntityInfoManager::Load_Unmanaged: There is no entity by that name: %s\n", name);
        return nullptr;
    }

    return Load( *ent_info, origin );
}

std::shared_ptr< Entity > EntityInfoManager::Load_Unmanaged( const EntityInfo& ent_info, const Vec3f& origin ) {
    std::shared_ptr< Entity > ent = game->NewUnmanagedEnt( ent_info.GetObjectTypeID() );
    if ( ent ) {
        ent->LoadDefaults( ent_info );
        FinishEnt(*ent, origin);
    } else {
        ERR("EntityInfoManager::Load_Unmanaged: Could not create entity: %s\n", ent_info.GetEntName().c_str() );
    }
    return ent;
}

void EntityInfoManager::FinishEnt( Entity& ent, const Vec3f& origin ) const {
    auto const & bounds = ent.bounds;
    if ( bounds.IsSphere() ) {
        const Vec3f offset(0,0,bounds.GetRadius());
        bounds.SetOrigin( origin + offset );
    } else {
        ent.SetOrigin( origin );
    }
}

void EntityInfoManager::LoadAllEntValToolTips( void ) {
    File file;

    std::pair<std::string, std::string> mval;
    TypeID classid = 0;
    bool got_classid;

    for ( const auto& data : manifest ) {
        file.Open( data.second.GetPath().c_str() );
        if ( !file.IsOpen() ) {
            WARN("LoadAllEntValToolTips: Couldn't open .ent file: %s\n", data.second.GetPath().c_str() );
            continue;
        }

        ParserSSV parser( file );
        parser.SetCommentIndicator('#');
        got_classid = false;

        while ( parser.ReadWord( mval.first ) ) {

            if ( mval.first == "class" ) {
                if ( got_classid ) {
                    DIE("LoadAllEntValToolTips:More than one class type specified for %s\n", file.GetFilePath().c_str() );
                    continue;
                }

                parser.ReadWord( mval.second );
                classid = get_typeid( mval.second );
                got_classid = true;

            } else if ( mval.first == "entval_tooltip" ) {
                if ( ! got_classid ) {
                    ERR_DIALOG("LoadAllEntValToolTips:Class type must be specified before specifying a tooltip in %s\n", file.GetFilePath().c_str() );
                    return;
                }

                parser.ReadString( mval.first );
                parser.ReadString( mval.second );
                if ( mval.first.empty() ) {
                    ERR("LoadAllEntValToolTips:Error reading entVal tooltip from %s\n", file.GetFilePath().c_str() );
                } else {
                    AddClassToolTip( classid, mval.first.c_str(), mval.second.c_str() );
                }
            }

            parser.SkipToNextNonblankLine();
        }
        //data_p->data = nullptr; // set pointer to null to unload
    }
}

void EntityInfoManager::BeingUnloaded( [[maybe_unused]] EntityInfo& asset_unused ) {

}
