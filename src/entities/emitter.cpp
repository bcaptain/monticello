// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./emitter.h"
#include "../particles/effectsManager.h"
#include "../game.h"

std::vector< std::string > Emitter::emitterSetters;

void Emitter::InitSetterNames( void ) {
    if ( emitterSetters.size() == Emitter_NUM_SETTERS )
        return; // already set
    emitterSetters.resize(Emitter_NUM_SETTERS);
    
    const uint i = Emitter_SETTERS_START;
    emitterSetters[Emitter_SetEffect-i] = "fx_effect";
    emitterSetters[Emitter_SetActive-i] = "active";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Emitter,Entity>(emitterSetters);
}

bool Emitter::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<EmitterSetterT>(setter) ) {
        case Emitter_SetEffect: SetEffect( value ); return true;
        case Emitter_SetActive: SetActive( String::ToBool( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Emitter_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Emitter_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Emitter>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Emitter::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<EmitterSetterT>(setter) ) {
        case Emitter_SetEffect: return GetEffectName();
        case Emitter_SetActive: return String::ToString(IsActive());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Emitter_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Emitter_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Emitter>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Emitter::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( emitterSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Emitter_SETTERS_START;
        
    static_assert( is_parent_of<Emitter>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Emitter::Emitter( const Emitter& other )
    : Entity( other )
    , prev_emit( other.prev_emit )
    , effect( other.effect )
    , spawned_infinite_effect( other.spawned_infinite_effect )
    , active( other.active )
{ }

Emitter::Emitter( void )
    : prev_emit( 0 )
    , effect(nullptr)
    , spawned_infinite_effect()
    , active(false)
{
    SetNoPhys( true );
}

void Emitter::Spawn( void ) {
    Entity::Spawn();
    AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_NO_PHYS );
    WakeUp();
}

Emitter& Emitter::operator=( const Emitter& other ) {
    Entity::operator=( other );

    effect = other.effect;
    prev_emit = other.prev_emit;
    spawned_infinite_effect = other.spawned_infinite_effect;
    active = other.active;

    return *this;
}

Emitter::~Emitter( void ) {
    KillInfiniteEffect();
}

void Emitter::KillInfiniteEffect( void ) {
    if ( auto fx = spawned_infinite_effect.lock() ) {
        fx->SetDuration( 1 ); // this will signal the effect to begin to die
        spawned_infinite_effect.reset();
    }
}

void Emitter::Think( void ) {
    Entity::Think();

    if ( !( IsSpawned()) )
        return;

    if ( !IsActive() )
        return;

    const std::string entval_effect_name( GetEffectName() );

    if ( entval_effect_name.size() > 0 ) {
        if ( effect == nullptr ) {
            // the effect has changed, so update it
            effect = effectsManager.Get( entval_effect_name.c_str() );
            prev_emit = 0;

            KillInfiniteEffect();
        }
    }

    if ( !effect )
        return;

    if ( spawned_infinite_effect.lock() )
        return;

    const uint duration_infinite = 0;
    if ( effect->GetDuration() == duration_infinite ) {
        Emit();
    } else {
        if ( game->GetGameTime() - prev_emit >= effect->GetDuration() ) {
            Emit();
            prev_emit = game->GetGameTime();
        }
    }
}

void Emitter::Emit( void ) {
    if ( !effect )
        return;

    KillInfiniteEffect();
    std::weak_ptr< Effect > emitted = effectsManager.Spawn( *effect, GetOrigin() );

    const uint duration_infinite = 0;
    if ( effect->GetDuration() == duration_infinite ) {
        spawned_infinite_effect = emitted;
    }
}

void Emitter::SetEffect( const std::shared_ptr< const Effect >& fx ) {
    effect = fx;
    if ( fx )
        prev_emit = game->GetGameTime() - effect->GetDuration(); // so it emits immediately
}

void Emitter::SetEffect( const std::string& _name ) {
    if ( _name.size() < 1 ) {
        SetEffect( std::shared_ptr< Effect >(nullptr) );
        prev_emit = 0;
    } else {
        SetEffect( effectsManager.Get( _name.c_str() ) );
    }
}

void Emitter::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Emitter>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteUInt( string_pool.AppendUnique( GetEffectName() ) );
    saveFile.parser.WriteUInt( prev_emit );
    
    MapSaveSetters( saveFile, string_pool, emitterSetters );

    // TODO: When we save to a GameSave() we will probably need to respawn the effect, especially if effect is infinite
}

void Emitter::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Emitter>( get_typeid<Entity>() ) );

    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    std::string effect_name( string_pool[ saveFile.parser.ReadUInt() ] );
    SetEffect( effect_name.c_str() );

    saveFile.parser.ReadUInt(prev_emit);
    
    
    MapLoadSetters( saveFile, string_pool, emitterSetters );
}

std::string Emitter::GetEffectName( void ) const {
    if ( !effect )
        return std::string();
    return effect->GetName();
}

void Emitter::SetActive( const bool whether ) {
    active = whether;
}

bool Emitter::IsActive( void ) const {
    return active;
}
