// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entity.h"
#include "../rendering/renderer.h"
#include "../rendering/fontManager.h"
#include "../nav/flowField.h"

const Color4f EDITOR_MODEL_SELECTION_COLORIZATION( 0,1,0,0.5f );
const uint DEFAULT_FIRE_DURATION = 5000_ms;
const Color4f BIND_LINE_SOURCE_COLOR(1,1,1,1); // white
const Color4f BIND_LINE_DEST_COLOR(1,0,1,1); // purple
const float DEFAULT_NORMAL_LENGTH = 1.0f;

void Entity::Colorize( const Color4f& color ) {
    if ( !colorization ) {
        colorization = new Color4f( color );
    } else {
        *colorization = color;
    }
}

Color4f Entity::GetColorization( void ) const {
    if ( colorization )
        return *colorization;

    return Color4f(0,0,0,0);
}

void Entity::Uncolorize( void ) {
    DELNULL(colorization);
}

void Entity::DrawBindLine( void ) const {
    if ( auto master = bindMaster.lock() )
        renderer->DrawLine( master->GetOrigin(), GetOrigin(), BIND_LINE_SOURCE_COLOR, BIND_LINE_DEST_COLOR );
}

void Entity::PackVBO( void ) {
    bounds.PackVBO();
}

void Entity::DrawVBO( void ) const {
    const float defaultModelOpacity = 1;
    DrawVBO( defaultModelOpacity );
}

void Entity::DrawVBO( const float opacity ) const {
    ModelDrawInfo drawInfo;

    drawInfo.colorize = (
        #ifdef MONTICELLO_EDITOR
            GetSelectedByEditor() ? EDITOR_MODEL_SELECTION_COLORIZATION :
        #endif // MONTICELLO_EDITOR
        
        GetColorization()
    );
    
    drawInfo.noAnimPause = noAnimPause;
    drawInfo.forceStaticDraw = false;
    drawInfo.entity = GetWeakPtr();
    drawInfo.opacity = opacity;
    drawInfo.animateThisFrame = ReadyToAnimate();
    
    if ( modelInfo.HasDrawModel() ) {
        drawInfo.opacity = GetOpacity() > EP_THOUSANDTH ? GetOpacity() : drawInfo.opacity;
    } else {
        drawInfo.opacity = 1;
    }
    
    DrawVBO( drawInfo );
}

void Entity::DrawVBO( const ModelDrawInfo& drawInfo ) const {
    if ( flowField && globalVals.GetBool( gval_ai_flowfield ) ) {
        flowField->DrawDebug();
    }

    if ( modelInfo.HasDrawModel() ) {
        bool culling;
        int culling_mode;

        if ( noCulling ) {
            // backup culling mode
            culling = glIsEnabled( GL_CULL_FACE );
            glGetIntegerv( GL_CULL_FACE_MODE, &culling_mode );
            glDisable( GL_CULL_FACE );
        }

        renderer->ModelView().Translate( modelInfo.GetOffset() );
        
            modelInfo.Draw( drawInfo );
            
        renderer->ModelView().Translate( -modelInfo.GetOffset() );

        if ( noCulling ) {
            // reenable culling
            glCullFace( static_cast< GLenum >( culling_mode ) );
            if ( !culling ) {
                glDisable( GL_CULL_FACE );
            } else {
                glEnable( GL_CULL_FACE );
            }
        }
    }
    
    
    if ( GetDrawBounds() || globalVals.GetBool( gval_d_bounds ) )
        bounds.DrawVBO();
    
    if ( globalVals.GetBool( gval_d_vertNormals) )
        DrawVertexNormals();

    DrawNameVBO();

    if ( globalVals.GetBool( gval_d_bindLines ) )
        DrawBindLine();
}

void Entity::DrawNameVBO( void ) const {
    if ( GetName().size() == 0 )
        return;

    if ( ! vbo_name || ! vbo_name->Finalized() || ! vbo_name_uv || ! vbo_name_uv->Finalized() ) {
        return;
    }

    glDisable( GL_DEPTH_TEST );

        const char* fontname = "courier";
        std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
        Font::AssertValid( fnt, fontname );

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SetUniform1f("fWeight", 1.0f );

        renderer->PushMatrixMV();
            // we must keep translation but discard rotation in order for text to always face the screen
            Vec3f translation( renderer->ModelView().GetTranslation() );
            renderer->ModelView().LoadIdentity();
            renderer->ModelView().SetTranslation( translation );

            // flip Y axis, since UI is usually for drawing with 0,0 as top-left of screen
            renderer->ModelView().Scale( 1.0f,-1.0f,1.0f );

            shaders->SendData_Matrices();
            shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );

        renderer->PopMatrixMV();

        shaders->SetUniform4f("vColor", Color4f(0,1,0,1) );
        shaders->SetAttrib( "vUV", *vbo_name_uv );
        shaders->SetAttrib( "vPos", *vbo_name );
        shaders->DrawArrays( GL_QUADS, 0, vbo_name->Num() );

    glEnable( GL_DEPTH_TEST );
}

void Entity::destroy_name_vbo( void ) {
    DELNULL( vbo_name );
    DELNULL( vbo_name_uv );
}

void Entity::pack_and_send_name_vbo( void ) {
    if ( vbo_name )
        return;

    DELNULL( vbo_name_uv );
    vbo_name = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
    vbo_name_uv = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
    FontInfo fi;
    fi.size_ui_px = 0.4f;
    vbo_name->PackText( *vbo_name_uv, GetName().c_str(), fi );
    vbo_name->MoveToVideoCard();
    vbo_name_uv->MoveToVideoCard();
}

void Entity::DrawVertexNormals( void ) const {
    
    if ( ! vbo_vert_normals || ! vbo_vert_normals->Finalized() ) {
        return;
    }

    shaders->UseProg(  GLPROG_MINIMAL  );
    shaders->SendData_Matrices();
    shaders->SetUniform4f("vColor", Color4f(0,1,0,1) );
    shaders->SetAttrib( "vPos", *vbo_vert_normals );
    renderer->PopMatrixMV();
    
    shaders->DrawArrays( GL_LINES, 0, vbo_vert_normals->Num() );
}

void Entity::pack_and_send_vert_normals_vbo( void ) {
    
    if ( vbo_vert_normals )
        return;
    
    std::shared_ptr< const Model> ent_model = GetModel();
    
    if ( !ent_model ) {
        WARN("Can't draw normals for entity %s, it has no model.\n", entname.c_str() );
        return;
    }
        
    const Mesh* model_mesh = ent_model->GetMesh(0); //TODO : Enable drawing normals for multiple meshes?
    
    if ( !model_mesh ) {
        WARN("Drawing normals : Invalid mesh for entity %s\n", entname.c_str() );
        return;
    } 
    
    vbo_vert_normals = new VBO<float>( 3,VBOChangeFrequencyT::FREQUENTLY );
    
    vbo_vert_normals->PackNormals( model_mesh->verts , globalVals.GetFloat( "d_vertNormalsLen" , DEFAULT_NORMAL_LENGTH ) );
    vbo_vert_normals->MoveToVideoCard();    

}

void Entity::destroy_vert_normals_vbo( void ) {
    DELNULL( vbo_vert_normals );
}

void Entity::DrawOccupiedNavPolies( void ) const {
    for ( auto const & wptr : occupied_nav_polies )
        if ( auto const poly = wptr.lock() )
            renderer->DrawPoly( poly->GetVerts3D(), Color4f(0,1,0,0.4f) );
}
