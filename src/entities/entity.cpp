// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entity.h"
#include "./entityInfoManager.h"
#include "../game.h"
#include "../particles/effectsManager.h"
#include "../nav/navMesh.h"
#include "./projectile.h"
#include "../sound.h"
#include "./pickupitem.h" // for DropLoot
#include "../damage/damageManager.h"
#include "../nav/flowField.h"
#include "../preload.h"
#include "../rendering/animation/animInfo.h"

extern const uint ANIMCUE_PROJECTILE_NAME_INDEX;
extern const uint ANIMCUE_ENTITY_NAME_INDEX;

const float DEFAULT_SIGHT_OFFSET = 1.0f;
const float DEFAULT_SIGHT_DISTANCE = 5.0f;
const float DEFAULT_SIGHT_ANGLE= 90.0f;

const float NAV_DESTINATION_ARRIVAL_DISTANCE = 0.3f; //!< a quarter of a meter
const uint FLOWFIELD_COSTFIELD_CALC_FREQUENCY = 1000;

const std::string Ammo::str_invalid("invalid");
const std::array< std::pair< std::string, AmmoTypeT >, static_cast<AmmoTypeT_BaseType>(AmmoTypeT::NUM_AMMO_TYPES) > Ammo::ammoTypes {{
    {"gas", AmmoTypeT::Gas}
    // dont forget to add new types to AmmoTypeT
}};

std::vector< std::string > Entity::entitySetters;

void Entity::InitSetterNames( void ) {
    if ( entitySetters.size() == Entity_NUM_SETTERS )
        return; // already set
    entitySetters.resize(Entity_NUM_SETTERS);
    
    const uint i = Entity_SETTERS_START;
    entitySetters[Entity_SetTouchDamage-i] = "touchDamage";
    entitySetters[Entity_SetMoveSpeedBoost-i] = "moveSpeedBoost";
    entitySetters[Entity_SetHealth-i] = "health";
    entitySetters[Entity_SetMaxHealth-i] = "maxHealth";
    entitySetters[Entity_SetInfestValue-i] = "infestValue";
    entitySetters[Entity_SetLastTimeUsed-i] = "lastTimeUsed";
    entitySetters[Entity_SetRunSpeedMultiplier-i] = "runSpeedMultiplier";
    entitySetters[Entity_SetDrawBounds-i] = "drawBounds";
    entitySetters[Entity_SetNoAnimPause-i] = "noAnimPause";
    entitySetters[Entity_SetNoTurn-i] = "noTurn";
    entitySetters[Entity_SetNoMove-i] = "noMove";
    entitySetters[Entity_SetCanMoveWhileTurning-i] = "moveWhileTurning";
    entitySetters[Entity_SetNoCulling-i] = "noCulling";
    entitySetters[Entity_SetChargeForward-i] = "chargeForward";
    entitySetters[Entity_SetByPlayer-i] = "byPlayer";
    entitySetters[Entity_SetClipBoundsOnly-i] = "clipBoundsOnly";
    entitySetters[Entity_SetGhostUs-i] = "ghostUs";
    entitySetters[Entity_SetGhostThem-i] = "ghostThem";
    entitySetters[Entity_SetAlwaysShowName-i] = "showName";
    entitySetters[Entity_SetDestructible-i] = "destructible";
    entitySetters[Entity_SetKilled-i] = "killed";
    entitySetters[Entity_SetGravity-i] = "gravity";
    entitySetters[Entity_SetSightAngle-i] = "sightAngle";
    entitySetters[Entity_SetSightDistance-i] = "sightDistance";
    entitySetters[Entity_SetSightOffset-i] = "sightOffset";
    entitySetters[Entity_SetOpacity-i] = "ent_opacity";
    entitySetters[Entity_SetFriction-i] = "friction";
    entitySetters[Entity_SetUseValue-i] = "useValue";
    entitySetters[Entity_SetLastDamagePercentOfHealth-i] = "lastDamagePercentOfHealth";
    entitySetters[Entity_SetUsable-i] = "usable";
    entitySetters[Entity_SetThrowable-i] = "throwable";
    entitySetters[Entity_SetMovable-i] = "movable";
    entitySetters[Entity_SetTouchDamageCooldown-i] = "touchDamageCooldown";
    entitySetters[Entity_SetPrevAttack1-i] = "prevAttack1";
    entitySetters[Entity_SetPrevAttack2-i] = "prevAttack2";
    entitySetters[Entity_SetTimeDamaged-i] = "timeDamaged";
    entitySetters[Entity_SetPrevCostField-i] = "prevCostField";
    entitySetters[Entity_SetTurnSpeed-i] = "turnSpeed";
    entitySetters[Entity_SetMaxTurnSpeed-i] = "maxTurnSpeed";
    entitySetters[Entity_SetMoveSpeed-i] = "moveSpeed";
    entitySetters[Entity_SetGiveHealth-i] = "giveHealth";
    entitySetters[Entity_SetTeamName-i] = "team";
    entitySetters[Entity_SetMarkedForUse-i] = "markedForUse";
    entitySetters[Entity_SetNoGrav-i] = "noGrav";
    entitySetters[Entity_SetOkayToDie-i] = "okayToDie";
    entitySetters[Entity_SetTurnAnimOverride-i] = "turnAnimOverride";
    entitySetters[Entity_SetDamageSound-i] = "snd_damage";
    entitySetters[Entity_SetInfested-i] = "infested";
    entitySetters[Entity_SetSpawnTime-i] = "spawnTime";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    for ( auto & str : entitySetters )
        ASSERT_MSG(!str.empty(), "There are undefined setters.");
}

bool Entity::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<EntitySetterT>(setter) ) {
        case Entity_SetSpawnTime: SetSpawnTime( String::ToUInt( value ) );
        case Entity_SetTouchDamage: SetTouchDamage( value ); return true;
        case Entity_SetMoveSpeedBoost: SetMoveSpeedBoost( String::ToFloat(value) ); return true;
        case Entity_SetHealth: SetHealth( String::ToInt(value) ); return true;
        case Entity_SetMaxHealth: SetMaxHealth( String::ToInt(value) ); return true;
        case Entity_SetInfestValue: SetInfestValue( String::ToInt(value) ); return true;
        case Entity_SetLastTimeUsed: SetLastTimeUsed( String::ToUInt(value) ); return true;
        case Entity_SetRunSpeedMultiplier: SetRunSpeedMultiplier( String::ToFloat(value) ); return true;
        case Entity_SetDrawBounds: SetDrawBounds( String::ToBool(value) ); return true;
        case Entity_SetNoAnimPause: SetNoAnimPause( String::ToBool(value) ); return true;
        case Entity_SetNoGrav: SetNoGrav( String::ToBool(value) ); return true;
        case Entity_SetNoTurn: SetNoTurn( String::ToBool(value) ); return true;
        case Entity_SetNoMove: SetNoMove( String::ToBool(value) ); return true;
        case Entity_SetCanMoveWhileTurning: SetCanMoveWhileTurning( String::ToBool(value) ); return true;
        case Entity_SetNoCulling: SetNoCulling( String::ToBool(value) ); return true;
        case Entity_SetChargeForward: SetChargeForward( String::ToBool(value) ); return true;
        case Entity_SetByPlayer: SetByPlayer( String::ToBool(value) ); return true;
        case Entity_SetClipBoundsOnly: SetClipBoundsOnly( String::ToBool(value) ); return true;
        case Entity_SetGhostUs: SetGhostUs( String::ToBool(value) ); return true;
        case Entity_SetGhostThem: SetGhostThem( String::ToBool(value) ); return true;
        case Entity_SetGravity: SetGravity( String::ToFloat(value) ); return true;
        case Entity_SetSightAngle: SetSightAngle( String::ToFloat(value) ); return true;
        case Entity_SetSightDistance: SetSightDistance( String::ToFloat(value) ); return true;
        case Entity_SetSightOffset: SetSightOffset( String::ToFloat(value) ); return true;
        case Entity_SetOpacity: SetOpacity( String::ToFloat(value) ); return true;
        case Entity_SetFriction: SetFriction( String::ToFloat(value) ); return true;
        case Entity_SetUseValue: SetUseValue( String::ToFloat(value) ); return true;
        case Entity_SetLastDamagePercentOfHealth: SetLastDamagePercentOfHealth( String::ToFloat(value) ); return true;
        case Entity_SetUsable: SetUsable( String::ToBool(value) ); return true;
        case Entity_SetThrowable: SetThrowable( String::ToBool(value) ); return true;
        case Entity_SetAlwaysShowName: SetAlwaysShowName( String::ToBool(value) ); return true;
        case Entity_SetDestructible: SetDestructible( String::ToBool(value) ); return true;
        case Entity_SetKilled: SetKilled( String::ToBool(value) ); return true;
        case Entity_SetMovable: SetMovable( String::ToBool(value) ); return true;
        case Entity_SetTouchDamageCooldown: SetTouchDamageCooldown( String::ToUInt(value) ); return true;
        case Entity_SetPrevAttack1: SetPrevAttack1( String::ToUInt(value) ); return true;
        case Entity_SetPrevAttack2: SetPrevAttack2( String::ToUInt(value) ); return true;
        case Entity_SetTimeDamaged: SetTimeDamaged( String::ToUInt(value) ); return true;
        case Entity_SetPrevCostField: SetPrevCostField( String::ToUInt(value) ); return true;
        case Entity_SetTurnSpeed: SetTurnSpeed( String::ToUInt(value) ); return true;
        case Entity_SetMaxTurnSpeed: SetMaxTurnSpeed( String::ToUInt(value) ); return true;
        case Entity_SetMoveSpeed: SetMoveSpeed( String::ToFloat(value) ); return true;
        case Entity_SetGiveHealth: SetGiveHealth( String::ToInt(value) ); return true;
        case Entity_SetTeamName: SetTeamName( value ); return true;
        case Entity_SetMarkedForUse: SetMarkedForUse( String::ToBool(value) ); return true;
        case Entity_SetOkayToDie: SetOkayToDie( String::ToBool(value) ); return true;
        case Entity_SetTurnAnimOverride: SetTurnAnimOverride( String::ToBool(value) ); return true;
        case Entity_SetDamageSound: SetDamageSound( value ); return true;
        case Entity_SetInfested: SetInfested( String::ToBool( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Entity_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Entity_INVALID_SETTER: FALLTHROUGH;
        default:
            ERR("Entity::Set: entity setter could not be found for \"%s\": ID_%u = %s.\n", GetIdentifier(), static_cast<NamedSetterT_BaseType>(setter), value.c_str() );
            return false;
    } 
}

std::string Entity::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<EntitySetterT>(setter) ) {
        case Entity_SetSpawnTime: return String::ToString( GetSpawnTime() );
        case Entity_SetInfested: return String::ToString( GetInfested() );
        case Entity_SetTouchDamage: return GetTouchDamage();
        case Entity_SetMoveSpeedBoost: return String::ToString( GetMoveSpeedBoost() );
        case Entity_SetHealth: return String::ToString( GetHealth() );
        case Entity_SetMaxHealth: return String::ToString( GetMaxHealth() );
        case Entity_SetInfestValue: return String::ToString( GetInfestValue() );
        case Entity_SetLastTimeUsed: return String::ToString( GetLastTimeUsed() );
        case Entity_SetRunSpeedMultiplier: return String::ToString( GetRunSpeedMultiplier() );
        case Entity_SetDrawBounds: return String::ToString( GetDrawBounds() );
        case Entity_SetNoAnimPause: return String::ToString( GetNoAnimPause() );
        case Entity_SetNoGrav: return String::ToString( GetNoGrav() );
        case Entity_SetNoTurn: return String::ToString( GetNoTurn() );
        case Entity_SetNoMove: return String::ToString( GetNoMove() );
        case Entity_SetCanMoveWhileTurning: return String::ToString( GetCanMoveWhileTurning() );
        case Entity_SetNoCulling: return String::ToString( GetNoCulling() );
        case Entity_SetChargeForward: return String::ToString( GetChargeForward() );
        case Entity_SetByPlayer: return String::ToString( GetByPlayer() );
        case Entity_SetClipBoundsOnly: return String::ToString( GetClipBoundsOnly() );
        case Entity_SetGhostUs: return String::ToString( GetGhostUs() );
        case Entity_SetGhostThem: return String::ToString( GetGhostThem() );
        case Entity_SetGravity: return String::ToString( GetGravity() );
        case Entity_SetSightAngle: return String::ToString( GetSightAngle() );
        case Entity_SetSightDistance: return String::ToString( GetSightDistance() );
        case Entity_SetSightOffset: return String::ToString( GetSightOffset() );
        case Entity_SetOpacity: return String::ToString( GetOpacity() );
        case Entity_SetFriction: return String::ToString( GetFriction() );
        case Entity_SetUseValue: return String::ToString( GetUseValue() );
        case Entity_SetLastDamagePercentOfHealth: return String::ToString( GetLastDamagePercentOfHealth() );
        case Entity_SetUsable: return String::ToString( IsUsable() );
        case Entity_SetThrowable: return String::ToString( IsThrowable() );
        case Entity_SetAlwaysShowName: return String::ToString( AlwaysShowName() );
        case Entity_SetDestructible: return String::ToString( IsDestructible() );
        case Entity_SetKilled: return String::ToString( GetKilled() );
        case Entity_SetMovable: return String::ToString( IsMovable() );
        case Entity_SetTouchDamageCooldown: return String::ToString( GetTouchDamageCooldown() );
        case Entity_SetPrevAttack1: return String::ToString( GetPrevAttack1() );
        case Entity_SetPrevAttack2: return String::ToString( GetPrevAttack2() );
        case Entity_SetTimeDamaged: return String::ToString( GetTimeDamaged() );
        case Entity_SetPrevCostField: return String::ToString( GetPrevCostField() );
        case Entity_SetTurnSpeed: return String::ToString( GetTurnSpeed() );
        case Entity_SetMaxTurnSpeed: return String::ToString( GetMaxTurnSpeed() );
        case Entity_SetMoveSpeed: return String::ToString( GetMoveSpeed() );
        case Entity_SetGiveHealth: return String::ToString( GetGiveHealth() );
        case Entity_SetTeamName: return GetTeamName();
        case Entity_SetMarkedForUse: return String::ToString( IsMarkedForUse() );
        case Entity_SetOkayToDie: return String::ToString( GetOkayToDie() );
        case Entity_SetTurnAnimOverride: return String::ToString( GetTurnAnimOverride() );
        case Entity_SetDamageSound: return GetDamageSound();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Entity_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Entity_INVALID_SETTER: FALLTHROUGH;
        default:
            ERR("Entity::Get: entity setter could not be found for \"%s\": ID_%u.\n", GetIdentifier(), static_cast<AmmoTypeT_BaseType>(setter) );
            return "";
    } 
}

NamedSetterT Entity::GetSetterIndexIn( const std::vector< std::string >& setters, const std::string& key ) {
    for ( uint i=0; i<setters.size(); ++i )
        if ( setters[i] == key )
            return i;
    
    return INVALID_SETTER;
}

NamedSetterT Entity::GetSetterIndex( const std::string& key ) {
    return GetSetterIndexIn( entitySetters, key );
}

EffectBind::EffectBind( void )
    : effect()
    , boneName()
    , channel()
{ }

EffectBind::EffectBind( const EffectBind& other )
    : effect( other.effect )
    , boneName( other.boneName )
    , channel( other.channel )
{ }

EffectBind::EffectBind( const std::weak_ptr< Effect >& fx, const char* nameOfBone, AnimChannelT _channel )
    : effect( fx )
    , boneName( nameOfBone )
    , channel( _channel )
{ }

EffectBind& EffectBind::operator=( const EffectBind& other ) {
    effect = other.effect;
    boneName = other.boneName;
    channel = other.channel;
    return *this;
}

Entity::Entity( void )
    : Object()
    , modelInfo( this )
    , flowField()
    , tickets()
    , bindSlaves()
    , entVals( ENTVAL_DICT_SIZE )
    , ammo{}
    , persistentDamages()
    , persistentEffects()
    , damageVuln()
    , my_ptr()
    , game_index()
    , touchDamage()
    , moveSpeed(1.0f)
    , moveSpeedBoost(0.0f)
    , useValue(0.0f)
    , health(1)
    , maxHealth(100)
    , infestValue(0)
    , lastTimeUsed(0)
    , animSpeed(1.0f)
    , runSpeedMultiplier(1.0f)
    , gravity(1.0f)
    , sightAngle(DEFAULT_SIGHT_ANGLE)
    , sightDistance(DEFAULT_SIGHT_DISTANCE)
    , sightOffset(DEFAULT_SIGHT_OFFSET)
    , ent_opacity(1.0f)
    , friction(1.0f)
    , lastDamagePercentOfHealth(0.0f)
    , touchDamageCooldown(1000)
    , prevAttack1(0)
    , prevAttack2(0)
    , timeDamaged(0)
    , prevCostField(0)
    , turnSpeed(ACTOR_MAX_MS_FOR_180_TURN)
    , maxTurnSpeed(ACTOR_MAX_MS_FOR_180_TURN)
    , giveHealth(0)
    , drawBounds(false)
    , noAnimPause(false)
    , noTurn(false)
    , noMove(false)
    , moveWhileTurning(false)
    , noCulling(false)
    , chargeForward(false)
    , byPlayer(false)
    , clipBoundsOnly(false)
    , ghostUs(false)
    , ghostThem(false)
    , infested(false)
    , spawnTime(false)
    , movable(false)
    , showName(false)
    , destructible(false)
    , killed(false)
    , markedForUse(false)
    , usable(false)
    , throwable(false)
    , occupied_nav_polies()
    , animCueEntityEffects()
    , runComponents()
    , boundEffects()
    , bounds( this )
    , entname()
    , team()
    , bindMaster()
    , colorization( nullptr )
    , turnDirection( TURNING_NOT_TURNING )
    , rotate_to( 0,0,0 )
    , vbo_name( nullptr )
    , vbo_name_uv( nullptr )
    , vbo_vert_normals( nullptr )
    , entity_flags(EntityFlagsT::ENTFLAG_NONE)
    , phys_flags(EntityPhysicsFlagsT::PHYSFLAG_EMPTY)
    , awake_state( AwakeStateT::Asleep )
{ }

Entity::Entity( const Entity& other )
    : Object( other )
    , modelInfo( this, other.modelInfo )
    , flowField() // this is created automatically
    , tickets() // set up automatically when this entity enters the voxel tree
    , bindSlaves()
    , entVals( other.entVals )
    , ammo{}
    , persistentDamages()
    , persistentEffects()
    , damageVuln()
    , my_ptr()
    , game_index()
    , touchDamage(other.touchDamage)
    , moveSpeed(other.moveSpeed)
    , moveSpeedBoost(other.moveSpeedBoost)
    , useValue(other.useValue)
    , health(other.health)
    , maxHealth(other.maxHealth)
    , infestValue(other.infestValue)
    , lastTimeUsed(other.lastTimeUsed)
    , animSpeed(other.animSpeed)
    , runSpeedMultiplier(other.runSpeedMultiplier)
    , gravity(other.gravity)
    , sightAngle(other.sightAngle)
    , sightDistance(other.sightDistance)
    , sightOffset(other.sightOffset)
    , ent_opacity(other.ent_opacity)
    , friction(other.friction)
    , lastDamagePercentOfHealth(other.lastDamagePercentOfHealth)
    , touchDamageCooldown(other.touchDamageCooldown)
    , prevAttack1(other.prevAttack1)
    , prevAttack2(other.prevAttack2)
    , timeDamaged(other.timeDamaged)
    , prevCostField(other.prevCostField)
    , turnSpeed(other.turnSpeed)
    , maxTurnSpeed(other.maxTurnSpeed)
    , giveHealth(other.giveHealth)
    , drawBounds(other.drawBounds)
    , noAnimPause(other.noAnimPause)
    , noTurn(other.noTurn)
    , noMove(other.noMove)
    , moveWhileTurning(other.moveWhileTurning)
    , noCulling(other.noCulling)
    , chargeForward(other.chargeForward)
    , byPlayer(other.byPlayer)
    , clipBoundsOnly(other.clipBoundsOnly)
    , ghostUs(other.ghostUs)
    , ghostThem(other.ghostThem)
    , spawnTime(other.spawnTime)
    , infested(other.infested)
    , movable(other.movable)
    , showName(other.showName)
    , destructible(other.destructible)
    , killed(other.killed)
    , markedForUse(other.markedForUse)
    , usable(other.usable)
    , throwable(other.throwable)
    , occupied_nav_polies()
    , animCueEntityEffects( other.animCueEntityEffects )
    , runComponents( other.runComponents )
    , boundEffects( other.boundEffects )
    , bounds( other.bounds )
    , entname( other.entname )
    , team( other.team )
    , bindMaster( other.bindMaster )
    , colorization(nullptr)
    , turnDirection( other.turnDirection )
    , rotate_to( other.rotate_to )
    , vbo_name( nullptr )
    , vbo_name_uv( nullptr )
    , vbo_vert_normals( nullptr )
    , entity_flags(EntityFlagsT::ENTFLAG_NONE)
    , phys_flags(EntityPhysicsFlagsT::PHYSFLAG_EMPTY)
    , awake_state( AwakeStateT::Asleep )
{
    LeaveAllVoxels();
    bounds.SetOwner( this );
}

void Entity::PreloadAssets( void ) {
    CMSG_LOG("Preloading Entity Assets: %s\n", GetIdentifier().c_str() );
    
    for ( const auto & pair : entVals ) {
        if ( String::LeftIs( pair.first, "dmg_" ) ) {
            Preload::PreloadDamage( pair.second );
            continue;
        }

        if ( String::LeftIs( pair.first, "fx_" ) ) {
            Preload::PreloadEffect( pair.second );
            continue;
        }

        if ( String::LeftIs( pair.first, "snd_" ) ) {
            Preload::PreloadSound( pair.second );
            continue;
        }

        if ( String::LeftIs( pair.first, "ent_" ) ) {
            Preload::PreloadEntity( pair.second );
            continue;
        }
    }
}

void Entity::Init( void ) {
    if ( entity_flags & ENTFLAG_INITIALIZED )
        return;
    
    // the entity was Loaded from somewhere (possible a map or saveGame),
    // and is has not been registered as awake in Game::
    switch ( awake_state ) {
        case AwakeStateT::Awake:
            // set us asleep and wake us via Game::
            awake_state = AwakeStateT::Asleep;
            WakeUp();
            break;
        case AwakeStateT::FallingAsleep: break;
            // since the game doesn't consider us awake, we aren't actually falling asleep
            awake_state = AwakeStateT::Asleep;
        case AwakeStateT::Asleep: break;
            // nothing to do
        default: break;
    }
        
    Init_Pathfinding();
    Init_Attachments();

    entity_flags |= ENTFLAG_INITIALIZED;
}

void Entity::Spawn( void ) {
    Init();

    auto wptr = GetWeakPtr();
    
    game->entTree->TryMigrate( *this );
    
    if ( ghostUs )
        phys_flags |= EntityPhysicsFlagsT::PHYSFLAG_GHOST_US;
        
    if ( ghostThem )
        phys_flags |= EntityPhysicsFlagsT::PHYSFLAG_GHOST_THEM;
        
    if ( GetNumVoxelsOccupied() < 1 )
        return;

    entity_flags |= ENTFLAG_SPAWNED;
    game->IncreaseInfestLevel( infestValue );
}

void Entity::Init_Attachments( void ) {
    if ( entity_flags & ENTFLAG_INITIALIZED )
        return; // already initialized

    const std::string attachments_str = entVals.GetString("attachments");
    std::vector< std::string > attachmentNames =String::ParseStringList( attachments_str, " " );
    for ( const auto & attachmentName : attachmentNames ) {
        std::vector< std::string > attachment =String::ParseStringList( attachmentName, " " );
        if ( attachment.size() != 2 ) {
            continue;
        }
        AddAttachment( attachment[0], attachment[1], 1 );
    }
}

void Entity::Init_Pathfinding( void ) {
    if ( entity_flags & ENTFLAG_INITIALIZED )
        return; // already initialized

    UpdateNavMeshOccupancy();
    if ( occupied_nav_polies.size() < 1 ) {
        WARN("Entity \"%s\" was not initialized on a navigation mesh.\n", GetIdentifier().c_str() );
    }
}

Entity& Entity::operator=( const Entity& other) {
    if ( this == &other )
        return *this;

    LeaveAllVoxels();

    Object::operator=( other );

    modelInfo = other.modelInfo; // owner stays the same
    bindSlaves = other.bindSlaves;
    ammo = other.ammo;
    entVals = other.entVals;
    bounds = other.bounds;
    bounds.SetOwner( this );
    runSpeedMultiplier = other.runSpeedMultiplier;
    animSpeed = other.animSpeed;
    drawBounds = other.drawBounds;
    noAnimPause = other.noAnimPause;
    noTurn = other.noTurn;
    noMove = other.noMove;
    noCulling = other.noCulling;
    phys_flags = other.phys_flags;
    entname = other.entname;
    bindMaster = other.bindMaster;
    colorization = other.colorization;
    turnDirection = other.turnDirection;
    rotate_to = other.rotate_to;
    awake_state = other.awake_state;
    occupied_nav_polies = other.occupied_nav_polies;
    animCueEntityEffects = other.animCueEntityEffects;
    runComponents = other.runComponents;
    boundEffects = other.boundEffects;

    DELNULL( vbo_name );
    DELNULL( vbo_name_uv );
    DELNULL( vbo_vert_normals );

    //todonow: we need to re-enter voxels if we're spawned and have changed bounds/model

    entity_flags = other.entity_flags;

    /* intentionally not copied, unique per object:
        my_ptr
        game_index
        unique_id
        tickets
        flowField
    */

    return *this;
}

Entity::~Entity( void ) {
    LeaveAllVoxels();

    delete vbo_name;
    delete vbo_name_uv;
    delete vbo_vert_normals;
    delete colorization;
}

bool Entity::IsInVoxel( const EntityTree& voxel ) const {
    for ( auto const& ticket : tickets )
        if ( ticket.IsForVoxel( voxel ) )
            return true;

    return false;
}

void Entity::SetName( const std::string& new_name ) {
    Object::SetName( new_name );

    // if the vbo is already packed, even if always_show_name is off, it means we have turned on the debug feature to show all names.
    if ( vbo_name || AlwaysShowName() ) {
        DELNULL( vbo_name );
        DELNULL( vbo_name_uv );
        pack_and_send_name_vbo();
   }
}

void Entity::SetAlwaysShowName( const bool whether ) {
    showName = whether;
    if ( whether ) {
        pack_and_send_name_vbo();
    } else {
        destroy_name_vbo();
    }
}

std::string Entity::GetIdentifier( void ) const {
    std::string identifier( get_classname(*this) );
    if ( GetName().size() > 0 ) {
        identifier += "/";
        identifier += GetName();
    }

    if ( entname.size() > 0 ) {
        identifier += "/";
        identifier += entname;
    }

    return identifier;
}

void Entity::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Object::MapSave( saveFile, string_pool );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteVec3f( rotate_to );

    bounds.MapSave( saveFile, string_pool );
    modelInfo.MapSave( saveFile, string_pool );

    auto sptr = bindMaster.lock();
    saveFile.WriteEntityRef( sptr.get() );

    // if there's a def file, don't save any identical values from it
    if ( auto info = entityInfoManager.Get_NoErr( entname.c_str() ) ) {
        saveFile.WriteDict_IgnoreFromVec( entVals, &info->entVals );
    } else {
        saveFile.WriteDict( entVals );
    }
    
    auto mdl = GetModel();
    const bool has_custom_model = mdl && String::LeftIs( mdl->GetName(), CUSTOM_MODEL_ );
    saveFile.parser.WriteBool( has_custom_model );
    if ( has_custom_model ) {
        mdl->MapSave( saveFile, string_pool );
    }
    
    saveFile.parser.WriteUInt8( static_cast<AwakeStateT_BaseType>(awake_state) );
    saveFile.parser.WriteUInt8( phys_flags );
    
    const auto numAmmo = static_cast<AmmoTypeT_BaseType>(AmmoTypeT::NUM_AMMO_TYPES);
    saveFile.parser.WriteUInt( numAmmo );
    for ( uint i=0; i<numAmmo; ++i ) {
        const auto ammoName = Ammo::GetTypeName(static_cast<AmmoTypeT>(i) );
        saveFile.parser.WriteUInt( string_pool.AppendUnique( ammoName ) );
        saveFile.parser.WriteUInt( GetAmmoQuantity(static_cast<AmmoTypeT>(i) ) );
    }

    MapSaveSetters( saveFile, string_pool, entitySetters );
    
    // entity_flags intentionally not saved
    // todo: damageVuln
    // todo: persistentDamages
    // todo: persistentEffects
}

void Entity::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Object::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    saveFile.parser.ReadVec3f( rotate_to );

    bounds.MapLoad( saveFile, string_pool );
    modelInfo.MapLoad( saveFile, string_pool );

    std::shared_ptr< Entity > ent = saveFile.ReadEntityRef();
    if ( ent ) {
        bindMaster = ent;
        ent->bindSlaves.Append( my_ptr );
    }

    // load default entvals if there's a def file
    if ( auto info = entityInfoManager.Get_NoErr( entname.c_str() ) )
        entVals = info->entVals;

    saveFile.ReadDict( entVals );
    
    const bool has_custom_model = saveFile.parser.ReadBool();
    if ( has_custom_model ) {
        std::shared_ptr< Model > mdl( std::make_shared< Model >() );
        mdl->MapLoad( saveFile, string_pool );
        mdl->PackVBO();
        modelInfo.SetModel( mdl );
    }

    awake_state = static_cast<AwakeStateT>( saveFile.parser.ReadUInt8() ); // entity will be set to this state at spawn
    saveFile.parser.ReadUInt8( phys_flags ); 

    const uint numAmmo = saveFile.parser.ReadUInt();

    for ( uint i=0; i<numAmmo; ++i ) {
        const std::string ammoName = string_pool[saveFile.parser.ReadUInt()].c_str();
        const uint ammoQty = saveFile.parser.ReadUInt();

        if ( !GiveAmmo( ammoName, ammoQty ) ) {
            WARN("Entity::MapLoad: Could not give ammo, ammo type was probably removed from engine: %s\n", ammoName.c_str() );
        }
    }

    
    MapLoadSetters( saveFile, string_pool, entitySetters );
    
    //todo: damageVuln
    //todo: persistentDamages
    //todo: persistentEffects
    PreloadAssets();
    UpdateVBO();
}

void Entity::MapSaveSetters( FileMap& saveFile, LinkList< std::string >& string_pool, const std::vector< std::string >& setters ) const {
    saveFile.parser.WriteUInt( setters.size() );
    for ( uint i=0; i<setters.size(); ++i ) {
        saveFile.parser.WriteUInt( string_pool.AppendUnique( setters[i] ) ); // key
        saveFile.parser.WriteUInt( string_pool.AppendUnique( Get(i) ) ); // value
    }
}

void Entity::MapLoadSetters( FileMap& saveFile, const std::vector< std::string >& string_pool, const std::vector< std::string >& setters ) {
    const uint numSetters = saveFile.parser.ReadUInt();
    for ( uint i=0; i<numSetters; ++i ) {
        const std::string key = string_pool[saveFile.parser.ReadUInt()].c_str();
        const std::string value = string_pool[saveFile.parser.ReadUInt()].c_str();
        
        Set( GetSetterIndex(key), value );
    }
}

void Entity::SetUsable( const bool whether ) {
    usable = whether;
}

void Entity::SetThrowable( const bool whether ) {
    throwable = whether;
}

void Entity::SetSelectedByEditor( const bool whether ) {
    if ( whether ) {
        entity_flags |= ENTFLAG_SELECTED_BY_EDITOR;
    } else {
        entity_flags &= ~ENTFLAG_SELECTED_BY_EDITOR;
    }
}

bool Entity::CheckDie( void ) {
    if ( !IsDestructible() )
        return false;
        
    if ( health > 0 )
        return false;
    
    return Death();
}

bool Entity::Death( void ) {
    return Kill();
}

bool Entity::Kill( void ) {
    if ( !IsDestructible() )
        return false;

    if ( killed )
        return true; // already been killed
        
    destructible = false;
    health = 0;
    killed = true;
    
    const std::string broken_model = entVals.GetString( "model_broken" ); //toNamedSetter
    if ( broken_model.size() > 0 ) {
        if ( SetModel( broken_model.c_str() ) ) {
            modelInfo.SetMaterialToDefault();
            return true;
        }
    }
    
    game->DecreaseInfestLevel( infestValue );

    MarkForDeletion();

    return true;
}

void Entity::DropBodyParts( void ) {
    const float distance_velocity = 5;
    const float height_velocity = 20;

    if ( !modelInfo.HasDrawModel() )
        return;
    const uint numAttachments = modelInfo.NumAttachments();
    
    for ( uint i = 0; i < numAttachments; ++i ) {
        auto mdl = modelInfo.GetAttachment(i)->GetModel();
        if ( ! mdl )
            continue;

        std::shared_ptr< Entity > bodyPart = entityInfoManager.Load( "bodyPart" );

        bodyPart = entityInfoManager.Load( "bodyPart" );
        bodyPart->SetMaterial( GetMaterial() );
        bodyPart->SetModel( mdl );
        bodyPart->modelInfo.SetMaterialToDefault();
        bodyPart->SetOrigin(GetOrigin());

        // get minimum bounds
        auto b = mdl->GetBounds();
        const float x = b.max.x - b.min.x;
        const float y = b.max.y - b.min.y;
        const float z = b.max.z - b.min.z;

        const float abs_x = fabsf( x );
        const float abs_y = fabsf( y );
        const float abs_z = fabsf( z );

        const float min = std::fminf( std::fminf(abs_x,abs_y), abs_z );

        const Vec3f offset(
            -( b.min.x + b.max.x ) / 2,
            -( b.min.y + b.max.y ) / 2,
            -( b.min.z + b.max.z ) / 2
        );

        bodyPart->bounds.SetToSphere( min );
        bodyPart->modelInfo.SetOffset( offset );
        const float randomStartPosAroundCircle = Random::Float(0,360);
        bodyPart->bounds.SetVelocity(
            Vec3f(
                sinf( ( ( (static_cast<float>(i)/(numAttachments+1) ) * 360 ) + randomStartPosAroundCircle ) * DEG_TO_RAD_F ) * distance_velocity
                , cosf( ( ( (static_cast<float>(i)/(numAttachments+1) ) * 360 ) + randomStartPosAroundCircle ) * DEG_TO_RAD_F ) * distance_velocity
                , height_velocity
            )
        );

        const float angVel_that_feels_right = Random::Float(300,500);
        const float angFriction_that_feels_right = Random::Float(200,250);
        bodyPart->bounds.SetAngularVelocity( Random::Direction() * angVel_that_feels_right );
        bodyPart->bounds.SetAngularFriction( angFriction_that_feels_right );
        bodyPart->Spawn();
    }
}

uint Entity::DropLoot( void ) const {

    std::string lootMin_s;
    std::string lootMax_s;
    std::string lootType_s;

    uint tot = 0;
    for ( uint i=0; ; ++i ) {
        const std::string index( std::to_string( i ) );

        lootMax_s = "lootMax";
        lootMax_s += index;
        const uint lootMax = entVals.GetUInt(lootMax_s.c_str());
        if ( lootMax == 0 )
            break;

        lootMin_s = "lootMin";
        lootMin_s += index;
        const uint lootMin = entVals.GetUInt(lootMin_s.c_str());

        lootType_s = "loot";
        lootType_s += index;
        const std::string loot = entVals.GetString(lootType_s.c_str() );

        tot += DropLoot( lootMin, lootMax, 5+ static_cast<float>(i*2), loot.c_str() );
    }

    return tot;
}

uint Entity::DropLoot( const uint min, const uint max, const float distance_velocity, const char* lootType ) const {
    if ( max < min )
        return 0;

    if ( lootType == nullptr || lootType[0] == '\0' ) {
        ERR("Entity::DropLoot(): no loot specified on entity \"%s\"\n", GetIdentifier().c_str() );
        return 0;
    }

    const Vec3f drop_origin( GetOrigin() );
    const uint num_drops = Random::UInt(min,max);
    const float height_velocity = Random::UInt(17,25);

    uint tot=0;
    // spawn them all in a circular direction
    for ( uint i=0; i<num_drops; ++i ) {
        auto ent = entityInfoManager.Load( lootType );
        auto loot = std::static_pointer_cast< PickupItem >( ent );
        if ( ! loot ) {
            ERR("Entity::DropLoot(): no pickup item defined (\"%s\") on actor %s\n", lootType, GetIdentifier().c_str() );
            return tot;
        }
        auto & drop = *loot;
        
        drop.AddPhysFlag( EntityPhysicsFlagsT::PHYSFLAG_GHOST_US );
        drop.SetOrigin(drop_origin);
        const float randomStartPosAroundCircle = Random::Float(0,360);
        drop.bounds.SetVelocity(
            Vec3f(
                sinf( ( ( (static_cast<float>(i)/(num_drops+1) ) * 360 ) + randomStartPosAroundCircle ) * DEG_TO_RAD_F ) * distance_velocity
                , cosf( ( ( (static_cast<float>(i)/(num_drops+1) ) * 360 ) + randomStartPosAroundCircle ) * DEG_TO_RAD_F ) * distance_velocity
                , height_velocity
            )
        );
        drop.Spawn();
        ++tot;
    }

    return tot;
}

void Entity::RunFrame( void ) {
    if ( !IsSpawned() )
        return;
        
    if ( !game->IsPaused() ) {
        Think();
        
        for ( uint i=0; i<runComponents.size(); ++i )
            CALL_METHOD(*this,runComponents[i])();
    }
    
    if ( !game->IsPaused() || GetNoAnimPause() ) {
        if ( ReadyToAnimate() ) {
            Animate();
        }
    }
    
    DoMigration();
}

std::vector< EntityTree* > Entity::GetOccupiedVoxels( void ) const {
    std::vector< EntityTree* > voxels;
    
    for ( auto const & ticket : tickets )
        voxels.push_back( ticket.GetTree() );
        
    return voxels;
}

void Entity::Think( void ) {
    DoPersistentDamage();
    
    if ( CheckDie() )
        return;

    if ( AlwaysShowName() && ! vbo_name )
        pack_and_send_name_vbo();

    if ( globalVals.GetBool( gval_d_navOccupied ) )
        DrawOccupiedNavPolies();
    
    ApplyRotation();
    
    if ( GetNumVoxelsOccupied() > 0 ) {
        if ( IsGravityEnabled() ) {
            bounds.ApplyGravityForce( gravity );
        }

        DoVelocity();
    }
        
    bounds.DoAngularVelocity();
    RepositionBoundEffects();
    DoEffects();
}

void Entity::DoEffects( void ) {
    if ( globalVals.GetBool( gval_d_damageFeedback ) )
        DoEffect_DamageFeedback();
    
    DoPersistentEffects();
}

void Entity::DoEffect_DamageFeedback( void ) {
    const Uint32 DAMAGED_TIME_PERIOD = 1500;
    const Uint32 curTime = game->GetGameTime();
    Color4f col;
    bool colorize = false;
    
    // visual damage feedback
    if ( timeDamaged > 0 ) {
        Uint32 timeSinceDamaged = curTime - timeDamaged;
        if ( timeSinceDamaged > DAMAGED_TIME_PERIOD ) {
            SetTimeDamaged(0);
        } else {
            float damageAlpha = static_cast<float>(DAMAGED_TIME_PERIOD - timeSinceDamaged) / static_cast<float>(DAMAGED_TIME_PERIOD);
            col.Set( 0.8f,0,0, damageAlpha );
            colorize = true;
        }
    }

    if ( !colorize ) {
        Uncolorize();
    } else {
        Colorize( col );
    }
}

void Entity::DoPersistentEffects( void ) {
    const Uint32 curTime = game->GetGameTime();
    
    for ( uint i=0; i<persistentEffects.size(); ++i ) {
        auto & event = persistentEffects[i];
        
        if ( !event.data.effect || event.IsExpired( curTime ) ) {
            PopSwap( persistentEffects, i--);
            continue;
        }
        
        if ( !event.IsTimeUp( curTime ) )
            continue;
            
        event.UpdateTime( curTime );
        effectsManager.Spawn( *event.data.effect, GetOrigin() + event.data.origin_offset );
    }
}

bool Entity::IsOnFire( void ) const {
    const uint fireDur = entVals.GetUInt(entval_fireDur);
    const uint infinity = static_cast< uint >( -1 );
    if ( entVals.GetUInt(entval_fireDur) == infinity ) {
        return true;
    }

    return ( game->GetGameTime() - entVals.GetUInt(entval_prevFire) < fireDur );
}

bool Entity::IsFlamable( void ) const {
    return entVals.GetBool(entval_flammable );
}

bool Entity::HasDamageType( const std::string& type ) const {
    for ( auto const & pdam : persistentDamages ) {
        if ( ! pdam.data.damage )
            continue;
        
        auto const & types = pdam.data.damage->types;
        
        if ( std::find_if( std::cbegin(types), std::cend(types), [&type](auto& pair){return pair.first == type;} ) == std::cend(types) )
            return true;
    }
    
    return false;
}

void Entity::TrySetOnFire( const uint duration ) {
    if ( IsFlamable() ) {
        entVals.SetUInt(entval_prevFire, game->GetGameTime() );
        entVals.SetUInt(entval_fireDur, duration);
    }
}

bool Entity::SetMemberVariable( const std::string& entry, const std::string& val ) {

    if ( entry == "origin" ) {
        Vec3f vec;
        if (! String::ToVec3f( val, vec ) )
            ERR("Bad Vec3f value %s for entity %s\n", entry.c_str(), entname.c_str() );
        SetOrigin( vec );
        return true;
    }

    if ( entry == "modelOffset" ) {
        Vec3f vec;
        if (! String::ToVec3f( val, vec ) )
            ERR("Bad Vec3f value %s for entity %s\n", entry.c_str(), entname.c_str() );
        modelInfo.SetOffset( vec );
        return true;
    }

    if ( entry == "name" ) {
        SetName( val );
        return true;
    }

    if ( entry == "radius" ) {
        const float radius = String::ToFloat( val );
        bounds.SetToSphere( radius );
        return true;
    }

    if ( entry == "yaw" ) {
        bounds.primitive->SetYaw( String::ToFloat( val ) );
        return true;
    }

    if ( entry == "roll" ) {
        bounds.primitive->SetRoll( String::ToFloat( val ) );
        return true;
    }

    if ( entry == "pitch" ) {
        bounds.primitive->SetPitch( String::ToFloat( val ) );
        return true;
    }

    if ( entry == "bounds" ) {
        bounds.SetToShape( val );
        return true;
    }

    if ( entry == "boundsMaterial" ) {
        bounds.SetMaterial( val );
        return true;
    }
    
    if ( entry == "model" ) {
        SetModel( val.c_str() );
        return true;
    }

    if ( entry == "mtl" ) {
        modelInfo.SetMaterial( val.c_str() );
        return true;
    }
    
    return false;
}

void NotEnoughArgsError( const char* command, const char* entname );
void NotEnoughArgsError( const char* command, const char* entname ) {
    ERR("Not enough arguments for \"entVal onActivate %s\" on entity %s\n", command, entname );
}

void Entity::Activate( Actor* actor ) {
    std::vector< std::string > list =String::ListifyString( entVals.GetString( "onActivate" ).c_str() );  //toNamedSetter
    DoActivate( list, actor );
}

void Entity::DoActivate( const std::vector< std::string >& list, Actor* actor ) {
    for ( std::vector<std::string>::size_type i=0; i<list.size(); ++i ) {
        if ( list[i] == "model" ) {
            if ( ++i >= list.size() ) {
                NotEnoughArgsError("model",entname.c_str());
                break;
            }

            SetModel( list[i].c_str() );
            SetMaterialToDefault();
            continue;
        }

        if ( list[i] == "damage" ) {
            if ( ! actor )
                continue;

            if ( i+1 >= list.size() ) {
                NotEnoughArgsError("damage",entname.c_str());
                break;
            }

            const std::shared_ptr< const DamageInfo > dmg = damageManager.Get( list[++i].c_str() );
            if ( dmg ) {
                dmg->Inflict( *actor, GetWeakPtr() );
            }

            continue;
        }

        if ( list[i] == "set" ) {
            if ( i+2 >= list.size() ) {
                NotEnoughArgsError("set",entname.c_str());
                break;
            }

            const char* key = list[++i].c_str();
            const char* val = list[++i].c_str();
            entVals.Set( key, val );
            continue;
        }

        if ( list[i] == "aset" ) {
            if ( ! actor )
                continue;

            if ( i+2 >= list.size() ) {
                NotEnoughArgsError("aset",entname.c_str());
                break;
            }

            const char* key = list[++i].c_str();
            const char* val = list[++i].c_str();
            actor->entVals.Set( key, val );
            continue;
        }

        if ( list[i] == "nudgeTurn" ) {
            if ( i >= list.size() ) {
                NotEnoughArgsError("nudgeTurn",entname.c_str());
                break;
            }

            NudgeTurn( String::ToFloat( list[++i] ) );
            continue;
        }
    }
}

void Entity::LoadDefaults( const EntityInfo& info ) {
    if ( info.GetEntName().size() == 0 ) {
        ERR_DIALOG("Entity::LoadDefaults: (%u:%s): no entname: (%u:%s).\n", GetObjectTypeID(), get_classname(*this), info.GetObjectTypeID(), info.GetTypeName().c_str() );
        return;
    }
        
    if ( !derives_from( info.GetObjectTypeID(), GetObjectTypeID() ) ) {
        ERR_DIALOG("Entity::LoadDefaults: (%u:%s) does not derive from type (%u:%s).\n", GetObjectTypeID(), get_classname(*this), info.GetObjectTypeID(), info.GetTypeName().c_str() );
        return;
    }

    entname = info.GetEntName();

    for ( const auto& data : info.entMembers ) {
        if ( SetMemberVariable( data.first, data.second ) ) {
            continue;
        }
        //todostrongerr:
        ERR("Entity::LoadDefaults: Unknown entity value for \"%s\": %s = %s.\n", entname.c_str(), data.first.c_str(), data.second.c_str() );
    }
    
    for ( auto const & item : info.entSets )
        if ( !Set( item.first, item.second ) )
            // todostrongerr
            ERR("Entity::LoadDefaults: Could not set entity value for \"%s\": ID_%u = %s.\n", entname.c_str(), item.first, item.second.c_str() );
    
    entVals.Merge( info.entVals );
}

bool Entity::UseBy( [[maybe_unused]] Actor* user_unused ) {
    return false;
}

bool Entity::IsUsable( void ) const {
    return usable;
}

bool Entity::IsThrowable( void ) const {
    return throwable;
}

bool Entity::CanSee( const Entity& entity ) const {
    return GetSquaredSightDistance( entity ) > 0;
}

float Entity::GetSquaredSightDistance( const Entity& entity ) const {
    
    // ** check to see if there are obstacles in between the entities
    
    CollisionReport3D report;
    IntersectData coldata;
    coldata.opts = MatchOptsT::MATCH_NORMAL;
    coldata.desired_type = TypeID_Entity;
    std::vector< CollisionItem > all_collisions;
    
    const auto line = Line3f( GetOrigin(), entity.GetOrigin() );
    auto const primitive = Primitive_Line3f( line ); //todo: ConstLineSegCollision and friends
    GlobalTrace( coldata, primitive, &all_collisions );

    for ( const auto item : all_collisions ) {
        auto hit = item.ent.lock();
        if ( !hit )
            continue;

        // ignore the thing we are trying to see
        if ( hit->GetIndex() == entity.GetIndex() )
            continue;

        // ignore self
        if ( hit->GetIndex() == GetIndex() )
            continue;

        // ignore projectiles
        if ( derives_from( TypeID_Projectile, *hit ) )
            continue;

        return -1; // not in sight
    }

    const Vec3f dir( line.vert[1] - line.vert[0] );
    return dir.SquaredLen();
}

void Entity::UpdateVBO( void )  {
    bounds.UpdateVBO();
}

bool Entity::ReadyToAnimate( void ) const {
    if ( game->IsPaused() && !noAnimPause )
        return false;
        
    // this is slow but looks better
    if ( globalVals.GetBool( gval_d_animBlendFull ) ) {
        if ( !modelInfo.IsAnimated() )
            return false;
        
        // if any anims are blending, let it animate every frame
        for ( uint c=0; c<MAX_ANIM_CHANNELS; ++c ) {
            auto const anim = modelInfo.GetArmatureInfo( static_cast< AnimChannelT >(c) );
            if ( ! anim )
                continue;
            
            if ( anim->IsBlendingFrames() )
                return true; // animate blend frames at full frame rate
        }
    }
    
    const uint curFrame = game->GetGameFrame();
    const bool frameIsOdd = curFrame % 2;
    if ( !frameIsOdd )
        return true;
    
    return false;
}

Vec3f Entity::GetAttackDir( void ) const {
    return GetFacingDir();
}

std::string Entity::GetTouchDamage( void ) const {
    return touchDamage;
}

void Entity::SetTouchDamage( const std::string& damage_def ) {
    touchDamage = damage_def;
}

std::string Entity::GetImpactSound( void ) const {
    return snd_impact;
}

void Entity::SetDamageSound( const std::string& snd_name ) {
    snd_damage = snd_name;
}


std::string Entity::GetDamageSound( void ) const {
    return snd_damage;
}

void Entity::SetImpactSound( const std::string& snd_name ) {
    snd_impact = snd_name;
}

void Entity::MarkForDeletion( void ) {
    auto ptr = GetWeakPtr().lock();
    if ( !ptr )
        return;
        
    ptr->GoToSleep();
    game->MarkEntityForDeletion( ptr );
}

void Entity::UpdateNavMeshOccupancy( void ) {
    if ( !bounds.IsSphere() )
        return;

    std::vector< std::weak_ptr< NavPoly > > new_occupied_nav_polies;

    const Vec3f org = GetOrigin();
    const Vec2f org2d(org.x, org.y);
    const float rad = bounds.GetRadius();

    // check if we are still in our previous nav polies, or are entering any of their immediate neighbors
    occupied_nav_polies = NavPoly::ReinsertEntity( my_ptr.lock(), org2d, rad, occupied_nav_polies );

    // if we are in no nav polies, brute force until we find one
    if ( occupied_nav_polies.size() < 1 ) {
        for ( auto & mesh : NavMesh::meshes ) {
            if ( ! mesh )
                continue;
            mesh->BruteInsertEntity( my_ptr.lock(), new_occupied_nav_polies );
        }

        occupied_nav_polies = std::move( new_occupied_nav_polies );
    }
}

int Entity::GiveHealth( const int amount ) {
    const int prevHealth = health;

    if ( health >= maxHealth )
        return 0;

    health += amount;
    if ( health > maxHealth )
        health = maxHealth;

    return health - prevHealth;
}

void Entity::DoPersistentDamage( void ) {
    const auto curTime = game->GetGameTime();
    
    for ( uint i=0; i<persistentDamages.size(); ++i ) {
        auto & pdam = persistentDamages[i];
        auto const & damage = persistentDamages[i].data.damage;
        
        if ( !damage || pdam.IsExpired( curTime ) ) {
            PopSwap( persistentDamages, i--);
            continue;
        }
            
        if ( pdam.IsTimeUp(curTime) ) {
            pdam.UpdateTime( curTime );
            ApplyDamage( *damage, pdam.data.inflictor );
        }
    }
}

bool Entity::HasPersistentDamage( const DamageInfo& damageInfo ) const {
    for ( auto const & damageEvent : persistentDamages ) {
        auto const dmg = damageEvent.data.damage;
        if ( ! dmg )
            continue;
        if ( dmg->GetName() == damageInfo.GetName() )
            return true;
    }
    return false;
}

bool Entity::Damage( const DamageInfo& damageInfo, const std::weak_ptr< Entity >& inflictor ) {
    if ( !IsDestructible() )
        return false;
        
    if ( damageInfo.dur > 0 ) {
        auto damage = damageManager.Get( damageInfo.GetName().c_str() );
        if ( !damage ) {
            ERR("Could not find persistent damage: %s.\n", damageInfo.GetName().c_str() );
            return false;
        }
        
        if ( damageInfo.noStack && HasPersistentDamage( damageInfo ) )
            return false;
        
        persistentDamages.emplace_back( DamageEvent(damage, my_ptr, inflictor) );
        auto & event = persistentDamages[persistentDamages.size()-1];
        event.SetDelay( damageInfo.delay );
        event.SetDuration( damageInfo.dur );
        return true;
    }

    return ApplyDamage( damageInfo, inflictor );
}

bool Entity::ApplyDamage( const DamageInfo& damageInfo, [[maybe_unused]] const std::weak_ptr< Entity >& inflictor, const float percent ) {
    WakeUp();
    
    if ( !damageInfo.noAnim )
        modelInfo.AddAnimState( AnimStateT::Hurt );
        
    if ( damageInfo.fx_hurt ) {
        effectsManager.Spawn( *damageInfo.fx_hurt, GetOrigin() ); //todo:fxOffset
    }
    
    std::string sound = entVals.GetString( "snd_damage" );
    if ( sound.size() > 0 )
        soundManager.PlayAt( GetOrigin(), sound.c_str() );

    auto oldHealth = health;
    int damageAmount;
    if ( damageVuln ) {
        damageAmount = damageVuln->Result( damageInfo );
    } else {
        damageAmount = damageInfo.GetTotalDamage();
    }
    
    health -= damageAmount;
    
    SetTimeDamaged( game->GetGameTime() );
    SetLastDamagePercentOfHealth( static_cast< float >( damageAmount ) / GetMaxHealth() );
    
    if ( health >= oldHealth )
        return false;
    
    SetTimeDamaged( game->GetGameTime() );
    return true;
    
}

void Entity::AddDamageVuln( const std::string& damageName, const int damage ) {
    if (!damageVuln)
        damageVuln = std::make_shared<DamageVuln>();
    
    damageVuln->AddType( damageName, damage );
}

void Entity::RemoveDamageVuln( const std::string& damageName ) {
    if ( !damageVuln )
        return;
        
    damageVuln->RemoveType( damageName );
}

void Entity::SetDestructible( const bool whether ) {
    destructible = whether;

    if ( whether && health <= 0 )
        SetHealth( 1 );// set up a default health so it doesn't just die if we forgot to set it.
}

std::shared_ptr<Projectile> Entity::LaunchProjectile( const AnimCue& cue, const AnimChannelT channel ) {
    
    std::shared_ptr< Projectile > proj;

    auto fail = [*this]( const char* hint){
        ERR("In cue file for entity %s, launchProjectile has invalid format (hint: %s), please check the log.\n", GetIdentifier().c_str(), hint);
        constexpr const char* format_msg(
            "Projectile cue format:.\n"
            "  Guide: [] denotes variable, {} denotes optional, * denotes argument can be used multiple times\n"
            "  Format: onFrame [uint] ofChannelName [name] launchProjectile [projName] {reqAmmo [uint] [name]}* {onBone [boneName] [head/center/tail]} {rot_z [-180 to 180]} {entval [projEntValToSet] [value]}*\n"
            "  [reqAmmo] must preceed [entity].\n"
            "  [entity] must preceed everything else.\n"
        );
        LOG("%s", format_msg);
        DIE("Shutting down.\n");
        return nullptr;
    };
    
    auto const & args=cue.arguments;
    if ( args.size() < 2 )
        fail("no arguments");
        
    auto dir = GetFacingDir();
    for ( uint arg=1; arg<cue.arguments.size(); ++arg ) {
        const uint args_left = args.size() - arg;
        
        if ( !proj && args[arg] != "reqAmmo" ) {
            if ( proj || args_left < 1 )
                fail("entity");
            
            auto ent = entityInfoManager.Load_Unmanaged( cue.arguments[ANIMCUE_PROJECTILE_NAME_INDEX].c_str() );
            if ( ! ent ) {
                ERR("Couldn't create projectile \"%s\" on entity \"%s\"\n", cue.arguments[0].c_str(), GetIdentifier().c_str() );
                return nullptr;
            }

            if ( !derives_from( TypeID_Projectile, *ent ) ) {
                ERR("Projectile \"%s\" isn't of Projectile class on entity \"%s\"\n", cue.arguments[0].c_str(), GetIdentifier().c_str() );
                return nullptr;
            }
            
            proj = std::static_pointer_cast< Projectile >( ent );
        }
        
        if ( args[arg] == "onBone" ) {
            if ( ! proj || args_left < 2 )
                fail("onBone");

            const char* boneName = args[++arg].c_str();
            proj->SetOrigin( GetBonePos( boneName, channel, args[++arg].c_str() ) );
            continue;
        }

        if ( args[arg] == "reqAmmo" ) {
            if ( args_left < 2 )
                fail("reqAmmo");
                
            if ( proj )
                fail("reqAmmo should precede other optional arguments");
                
            const uint ammoNum = String::ToUInt( args[++arg] );
            const char* ammoName = args[++arg].c_str();

            if ( !HasAmmo(Ammo::GetAmmoTypeFromName(ammoName), ammoNum) )
                return nullptr;
                
            RemoveAmmo(Ammo::GetAmmoTypeFromName(ammoName), ammoNum);
                
            continue;
        }

        if ( args[arg] == "rot_z" ) {
            int rot_z = 0;
            if ( args_left >= 1 )
                rot_z = String::ToInt( args[++arg] );
            
            constexpr const int MAX_PROJECTILE_LAUNCH_ROT_Z = 180;
            if ( ! proj || rot_z == 0 || rot_z < -MAX_PROJECTILE_LAUNCH_ROT_Z || rot_z > MAX_PROJECTILE_LAUNCH_ROT_Z ) {
                fail("rot_z");
            }
            
            Mat3f rot;
            rot.Rotate( rot_z, 0, 0, 1 );
            dir *= rot;
            dir.Normalize();
            continue;
        }
        
        if ( args[arg] == "entval" ) {
            if ( ! proj || args_left < 2 )
                fail("entval");
            
            const auto key = args[++arg];
            const auto val = args[++arg];
            
            proj->entVals.Set( key.c_str(), val );  //toNamedSetter
            continue;
        }
        
        // unknown cue arg
        fail( args[arg].c_str() );
    }
    
    if ( !proj )
        fail("no projectile created.\n");
    
    proj->SetOwner( my_ptr );
    proj->SetDir( dir );
    game->InsertEntity( proj );
    proj->Spawn();
    proj->Launch();
    
    return proj;
}

void Entity::BindEffectToBone( std::weak_ptr< Effect >& effect, const char* boneName, const AnimChannelT channel ) {
    boundEffects.emplace_back( effect, boneName, channel );
}

void Entity::RepositionBoundEffects( void ) {
    uint i=0;
    while ( i<boundEffects.size() ) {
        auto fx = boundEffects[i].effect.lock();
        if ( fx ) {
            Vec3f boneSpot = GetBonePos( boundEffects[i].boneName.c_str(), boundEffects[i].channel, "center" );
            fx->SetOrigin( boneSpot );
            ++i;
            continue;
        }

        const uint last = boundEffects.size()-1;

        if ( i == last ) {
            boundEffects.resize(0);
            break;
        }

        // overrite this element wih the last one and resize the list down by one
        std::swap( boundEffects[i].boneName, boundEffects[last].boneName );
        std::swap( boundEffects[i].effect, boundEffects[last].effect );
        boundEffects[i].channel = boundEffects[last].channel;
        boundEffects.resize( boundEffects.size()-1 );
    }
}

BonePos Entity::GetBonePos( const std::string& boneName, const AnimChannelT channel ) const {
    const ArmatureInfo *armInfo = nullptr;
    
    if ( HasAnimatedModel() ) {
        bool boneFoundInAttachment = false;
        for ( uint i=0; i<modelInfo.NumAttachments(); ++i ) {
            armInfo = modelInfo.GetAttachment(i)->GetArmatureInfo(channel);
            if ( armInfo ) {
                if ( armInfo->IsBoneNameInArmature( boneName ) ) {
                    boneFoundInAttachment = true;
                    break;
                }
            }
        }
        if ( !boneFoundInAttachment )
            armInfo = modelInfo.GetArmatureInfo(channel);
    }
    
    if ( ! armInfo ) {
        ERR("GetBonePos: couldn't get armInfo for entity %s.\n", GetIdentifier().c_str());
        
        // return a sane default
        const Vec3f tail = GetOrigin();
        const Vec3f head = tail + Vec3f(0,0,1);
        return {head,tail};
    }
    
    
    const Mat3f rot( bounds.primitive->GetAngles() );
    
    auto pos = armInfo->GetBonePos( boneName );
    pos.head *= rot;
    pos.tail *= rot;
    pos.head += GetOrigin() + modelInfo.GetOffset();
    pos.tail += GetOrigin() + modelInfo.GetOffset();
    
    return pos;
}

Vec3f Entity::GetBonePos( const std::string& boneName, const AnimChannelT channel, const std::string& boneSpot ) const {
    auto bonePos = GetBonePos( boneName, channel );

    if ( boneSpot == "head" )
        return bonePos.head;
    
    if ( boneSpot == "tail" )
        return bonePos.tail;

    if ( boneSpot != "center" )
        WARN("GetBonePos_TPose: boneSpot was not defined for an action, i.e.\"head,center, or tail\", BS_CENTER is assumed.\n");
    
    return (bonePos.tail + bonePos.head) / 2;
}

void Entity::AnimationCompleted( [[maybe_unused]] const AnimStateT completedAnimState, [[maybe_unused]] const AnimChannelT channel ) {
}

void Entity::AnimationInterrupted( [[maybe_unused]] const AnimStateT interruptedAnimState, [[maybe_unused]] const AnimChannelT channel ) {
}

std::shared_ptr< FlowField > Entity::GetFlowField( void ) {
    if ( ! flowField ) {
        flowField = std::make_shared<FlowField>();
        prevCostField = game->GetGameTime() - FLOWFIELD_COSTFIELD_CALC_FREQUENCY - 1;
    }
    
    std::shared_ptr< const NavPoly > curPoly;
    
    // we only need one poly
    for ( uint i=0; i < occupied_nav_polies.size() && !curPoly; ++i )
        curPoly = occupied_nav_polies[i].lock(); 
        
    if ( game->GetGameTime() - prevCostField > FLOWFIELD_COSTFIELD_CALC_FREQUENCY ) {
        prevCostField = game->GetGameTime();
        flowField->Build( GetOrigin() );

        Circle2f destCircle( Vec2f(GetOrigin().x, GetOrigin().y), NAV_DESTINATION_ARRIVAL_DISTANCE );
        if ( bounds.IsSphere() ) {
            destCircle.radius = bounds.GetRadius();
        }
        
        flowField->ComputeCostField( curPoly, destCircle );
        
    }

    return flowField;
}

bool Entity::SetMaterial( const std::shared_ptr< const Material >& mtl ) {
    return modelInfo.SetMaterial( mtl );
}

bool Entity::SetMaterial( const char* mtl ) {
    return modelInfo.SetMaterial( mtl );
}

std::shared_ptr< const Material > Entity::GetMaterial( void ) const {
    return modelInfo.GetMaterial();
}

void Entity::UnsetMaterial( void ) {
    modelInfo.UnsetMaterial();
}

bool Entity::SetMaterialToDefault( void ) {
    return modelInfo.SetMaterialToDefault();
}

bool Entity::SetModel( const std::shared_ptr< const Model >& model ) {
    return modelInfo.SetModel( model );
}

std::string Entity::GetTeamName( void ) const {
    return team;
}

void Entity::SetTeamName( const std::string& teamName ) {
    team = teamName;
}

void Entity::WakeUp( void ) {
    auto ptr = GetWeakPtr().lock();
    if ( !ptr ) {
        ERR("Could not set entity awake_state, the object is not managed.\n");
        return;
    }
    game->AwakenEntity( ptr );
}

bool Entity::IsPlayer( void ) const {
    return game->IsPlayer( *this );
}

bool Entity::RemoveAmmo( const AmmoTypeT _type, const uint amount ) {
    if ( _type == AmmoTypeT::Invalid ) {
        ERR("Entity::RemoveAmmo: Unknown ammo type: %u\n", static_cast<AmmoTypeT_BaseType>(_type) );
        return false;
    }
    
    uint& clipAmount = ammo[static_cast<AmmoTypeT_BaseType>(_type)].amount;
    
    if ( amount > clipAmount ) {
        clipAmount = 0;
    } else {
        clipAmount -= amount;
    }
    return true;
}

bool Entity::GiveAmmo( const AmmoTypeT _type, const uint amount ) {
    if ( _type == AmmoTypeT::Invalid ) {
        ERR("Entity::GiveAmmo: Unknown ammo type: %u\n", static_cast<AmmoTypeT_BaseType>(_type));
        return false;
    }

    auto& clip = ammo[static_cast<AmmoTypeT_BaseType>(_type)];
    
    clip.amount += amount;
    if ( clip.amount > clip.capacity )
        clip.amount = clip.capacity;
        
    return true;
}

uint Entity::GetAmmoQuantity( const AmmoTypeT _type ) const {
    if ( _type == AmmoTypeT::Invalid ) {
        ERR("Entity::HasAmmo: Unknown ammo type: %u\n", static_cast<AmmoTypeT_BaseType>(_type));
        return 0;
    }
    
    return ammo[static_cast<AmmoTypeT_BaseType>(_type)].amount;
}

uint Entity::GetAmmoCapacity( const AmmoTypeT _type ) const {
    if ( _type == AmmoTypeT::Invalid ) {
        ERR("Entity::HasAmmo: Unknown ammo type: %u\n", static_cast<AmmoTypeT_BaseType>(_type));
        return 0;
    }
    
    return ammo[static_cast<AmmoTypeT_BaseType>(_type)].capacity;
}

void Entity::SetAmmoCapacity( const AmmoTypeT _type, const uint amount ) {
    if ( _type == AmmoTypeT::Invalid ) {
        ERR("Entity::HasAmmo: Unknown ammo type: %u\n", static_cast<AmmoTypeT_BaseType>(_type));
        return;
    }
    
    ammo[static_cast<AmmoTypeT_BaseType>(_type)].capacity = amount;
}

float Entity::GetAnimSpeed( void ) const {
    return animSpeed;
}

void Entity::SetAnimSpeed( const float _speed ) {
    animSpeed = _speed;
}

void Entity::SetGameIndex( const uint to ) {
    game_index = to;
}

void Entity::GoToSleep( void ) {
    awake_state = AwakeStateT::FallingAsleep;
}

bool Entity::IsAwake( void ) const {
    return awake_state == AwakeStateT::Awake;
}

Vec3f Entity::GetOrigin( void ) const {
    return bounds.GetOrigin();
}

Uint32 Entity::GetIndex( void ) const {
    return game_index;
}

const std::weak_ptr< const Entity > Entity::GetWeakPtr( void ) const {
    return my_ptr;
}

std::weak_ptr< Entity > Entity::GetWeakPtr( void ) {
    return my_ptr;
}

uint Entity::GetNumVoxelsOccupied( void ) const {
    return tickets.Num();
}

bool Entity::IsSpawned( void ) const {
    return entity_flags & ENTFLAG_SPAWNED;
}

bool Entity::GetSelectedByEditor( void ) const {
    return entity_flags & ENTFLAG_SELECTED_BY_EDITOR;
}

void Entity::SetEntName( const std::string& def_nam ) {
    entname = def_nam;
}

void Entity::SetEntName( const char *def_nam ) {
    entname = def_nam;
}

std::string Entity::GetEntName( void ) const {
    return entname;
}

Vec3f Entity::GetOffset( void ) const {
    return modelInfo.GetOffset();
}

void Entity::SetOffset( const Vec3f& offset ) {
    modelInfo.SetOffset( offset );
}

std::shared_ptr< const Model > Entity::GetModel( void ) const {
    return modelInfo.GetModel();
}

bool Entity::HasModel( void ) const {
    return modelInfo.HasDrawModel();
}

void Entity::LeaveAllVoxels( void ) {
    tickets.DelAll();
}

bool Entity::IsDamaged( void ) const {
    return GetHealth() < GetMaxHealth();
}

void Entity::RemoveAttachment( const std::string& attachment_pos, const std::string& attachment_name ) {
    modelInfo.RemoveAttachment( attachment_pos, attachment_name );
}

void Entity::AddAttachment( const std::string& attachment_pos, const std::string& attachment_name, const AnimChannelT_BaseType channel ) {
    modelInfo.AddAttachment( attachment_pos, attachment_name, channel );
}

bool Entity::HasAttachment( const std::string& attachment_pos, const std::string& attachment_name ) const {
    return modelInfo.HasAttachment( attachment_pos, attachment_name );
}

uint Entity::GetAttachmentIndexAtPos( const std::string& attachment_pos ) const {
    return modelInfo.GetAttachmentIndexAtPos( attachment_pos );
}

Vec3f Entity::GetModelOffset( void ) const {
    return modelInfo.GetOffset();
}

void Entity::SetModelOffset( const Vec3f _offset ) {
    modelInfo.SetOffset( _offset );
}

bool Entity::SetModel( const char* model_name ) {
    return modelInfo.SetModel( model_name );
}

void Entity::GameAttorney::SetGameIndex( Entity& ent, const uint index ) {
    ent.SetGameIndex( index );
}

void Entity::GameAttorney::SetAwakeState( Entity& ent, const AwakeStateT state ) {
    ent.awake_state = state;
}

void Entity::GameAttorney::SetWeakPtr( Entity& ent, const std::weak_ptr<Entity>& wptr ) {
    ent.my_ptr = wptr;
}

LinkList< EntityTreeTicket >& Entity::EntityTreeAttorney::TicketsRef( Entity& ent ) {
    return ent.tickets;
}

bool Entity::DamageAttorney::ApplyDamage( const DamageInfo& damageInfo, Entity& ent, const std::weak_ptr< Entity >& inflictor, const float percent ) {
    return ent.ApplyDamage( damageInfo, inflictor, percent );
}

bool Entity::RemoveAmmo( const std::string& ammoName, const uint amount ) {
    return RemoveAmmo( Ammo::GetAmmoTypeFromName( ammoName ), amount);
}

bool Entity::GiveAmmo( const std::string& ammoName, const uint amount ) {
    return GiveAmmo( Ammo::GetAmmoTypeFromName( ammoName ), amount);
}

bool Entity::HasAmmo( const AmmoTypeT _type, const uint amount ) const {
    return GetAmmoQuantity( _type ) > amount;
}

uint Entity::GetAmmoQuantity( const std::string& ammoName ) const {
    return GetAmmoQuantity( Ammo::GetAmmoTypeFromName( ammoName ) );
}

uint Entity::GetAmmoCapacity( const std::string& ammoName ) const {
    return GetAmmoCapacity( Ammo::GetAmmoTypeFromName( ammoName ) );
}

void Entity::SetAmmoCapacity( const std::string& ammoName, const uint amount ) {
    return SetAmmoCapacity( Ammo::GetAmmoTypeFromName( ammoName ), amount );
}

void Entity::SetMaxHealth( const int to ) {
    maxHealth = to;
}

int Entity::GetMaxHealth( void ) const {
    return maxHealth;
}

int Entity::GetHealth( void ) const {
    return health;
}

void Entity::SetHealth( const int to ) {
    health = to;
}

bool Entity::IsDestructible( void ) const {
    return destructible;
}

bool Entity::AlwaysShowName( void ) const {
    return showName;
}

bool Entity::IsMarkedForUse( void ) const {
    return markedForUse;
}

void Entity::SetUseValue( const float to ) {
    useValue = to;
}

float Entity::GetUseValue( void ) const {
    return useValue;
}
    
void Entity::SetLastTimeUsed( const uint to ) {
    lastTimeUsed = to;
}

uint Entity::GetLastTimeUsed( void ) const {
    return lastTimeUsed;
}

void Entity::SetMarkedForUse( const bool whether ) {
    markedForUse = whether;
}

int Entity::GetInfestValue( void ) const {
    return infestValue;
}

void Entity::SetInfestValue( const int to ) {
    infestValue = to;
}

void Entity::RestoreHealth( void ) {
    health = maxHealth;
}
