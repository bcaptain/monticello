// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_EMITTER_H_
#define SRC_ENTITIES_EMITTER_H_

#include "./entity.h"
#include "../particles/effect.h"

/*!

Emitter \n\n

Emits effects \n
WARNING: once an emitter is set to emit a particle with an infinite duration, it will emit only one and never again.

**/


class Emitter : public Entity {
public:
    static constexpr TypeInfo<Emitter, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Emitter( void );
    Emitter( const Emitter& other );
    ~Emitter( void ) override;

public:
    Emitter& operator=( const Emitter& other );

public:
    void Spawn( void ) override;
    void SetEffect( const std::string& fx );
    void SetEffect( const std::shared_ptr< const Effect >& fx );

    std::string GetEffectName( void ) const;

    void SetActive( const bool whether );
    bool IsActive( void ) const;

    void Think( void ) override;
    void Emit( void );

    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void KillInfiniteEffect( void );

private:
    Uint32 prev_emit;
    std::shared_ptr< const Effect > effect; //!< a link to the "master" effect
    std::weak_ptr< Effect > spawned_infinite_effect; //!< if the effect is infinite, this is us keepign track of it so we can kill it off if the emitter gets deleted or something. there can be only one emit of an infinite effect.
    bool active;

// ** Named Setters
private:
    static const uint Emitter_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum EmitterSetterT : NamedSetterT_BaseType {
        Emitter_SetEffect = Emitter_SETTERS_START
        , Emitter_SetActive
        , Emitter_SETTERS_END
        , Emitter_NUM_SETTERS = Emitter_SETTERS_END - Emitter_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > emitterSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Emitter>( get_typeid<Entity>() ) );
    static ToolTip< Emitter, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_EMITTER_H_
