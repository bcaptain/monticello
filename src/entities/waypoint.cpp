// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./waypoint.h"
#include "../rendering/materialManager.h"

Waypoint::Waypoint( void ) {
}

Waypoint::Waypoint( const Waypoint& other )
    : Entity( other )
{ }

void Waypoint::Spawn( void ) {
    Entity::Spawn();
    SetNoGrav(false);
}

bool Waypoint::SetModel( [[maybe_unused]] const std::shared_ptr< const Model >& mod ) {
    ERR("Waypoints cannot have models.\n");
    return false;
}
