// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_ACTOR_H_
#define SRC_ENTITIES_ACTOR_H_

#include "../base/main.h"

#include "./light/allLights.h" //debug only

extern const int ACTOR_DEFAULT_MAX_HEALTH;
extern const Box3D ACTOR_DEFAULT_VIEW_CONE;
extern const float ACTOR_DEFAULT_VIEW_WIDTH;
extern const float ACTOR_DEFAULT_ACTOR_DEFAULT_VIEW_HEIGHT;
extern const float ACTOR_DEFAULT_VIEW_DIST;

extern const Color4f HUD_HEALTH_FONT_COLOR;
extern const float HUD_HEALTH_FONT_SIZE;

extern const Vec3f ENTITY_FACING_DIRECTION_DEFAULT;
extern const Color4f AI_FACING_DIRECTION_START;
extern const Color4f AI_FACING_DIRECTION_END;
extern const float AI_DEFAULT_CHASE_RADIUS_LIMIT;
extern const uint AI_MAX_CHASE_NAV_POLIES;

constexpr const bool attackCollisionDebug = true;

#include "./entity.h"
#include "../damage/damage.h"
#include "../inventory.h"
#include "../nav/navMesh.h"
#include "../input/input.h"

typedef Uint32 AI_ZoneT_BaseType;
enum class AI_ZoneT : AI_ZoneT_BaseType {
    Near
    , Mid
    , Far
    , Lost
    , NUM_ZONES
};

typedef Uint32 AI_ActionT_BaseType;
enum class AI_ActionT : AI_ActionT_BaseType {
    Idle
    , Attack1
    , Attack2
    , Chase
    , Stalk
    , Flee
    , Wander
    , Interact
    , Special_1
};

typedef Uint32 NavStateT_BaseType;
enum NavStateT : NavStateT_BaseType {
    NAV_STATE_NO_PATH=0
    , NAV_STATE_PATH_ACQUIRED=1
    , NAV_STATE_PATH_BLOCKED=1<<1
    , NAV_STATE_DEPARTED=1<<2
    , NAV_STATE_ARRIVED=1<<3
};

class Navmesh_Poly;
class Sprite;
class Navmesh_Neighbor;
class PickupItem;

struct ViewCone {
    ViewCone( void ) = delete;
    ViewCone( const ViewCone& other ) = delete;
    ViewCone& operator=( const ViewCone& other ) = delete;
    
    explicit ViewCone( const Entity& ent );
    bool IsWithinAngle( Entity& ent, const Vec3f& seer_origin, const Vec3f& seer_facing_dir ) const;
    
    const float offset;
    const float dist;
    const float angle;
};

/*!

Actor \n\n

Entities that have AI and inventory . \n\n

**/


class Actor : public Entity {
public:
    static constexpr TypeInfo<Actor, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Actor( void );
    ~Actor( void ) override;
    Actor( const Actor& other );

public:
    Actor& operator=( const Actor& other );
public:
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
    bool ApplyDamage( const DamageInfo& damageInfo, const std::weak_ptr< Entity >& inflictor_unused, const float percent=1.0f ) override;
    
    bool Death( void ) override;
    bool Kill( void ) override;
    void DeathExplode( void );
    void Spawn( void ) override;
    void Think( void ) override;
    void DoSpawnProtection( void );
    void DoVelocity( void ) override;
    virtual void PutToSleep( void ) {}

    bool IsStunned( void ) const;
    bool IsAsleep( void ) const;

    virtual void AddSleepiness( const int val );
    void SetSleepLevel( const int to );
    int GetSleepLevel( void ) const;
    float GetSleepLevel_Normalized( void ) const;
    
    void DoMigration( void ) override;
    std::shared_ptr< Entity > GetClosestEntityInList( const std::vector< CollisionItem >& list ) const;
    std::shared_ptr< Entity > GetClosestEntityInVector( std::vector< std::weak_ptr< Entity > > vector ) const;

    bool SetMemberVariable( const std::string& entry, const std::string& val ) override;

    bool IsUsable( void ) const override;
    void Stun( const Uint32 miliseconds );

    void GetEntitiesInSight( std::vector< CollisionItem >& collisions_out, const TypeID specific_type = TypeID_Entity );

    virtual void Interact( void );
    int ActivateManualTriggers( void ); //!< returns how many triggers were successfully activated

// **** Input
public:
    virtual bool ExecuteActionJoyAxis_State( const std::string& action, const float axis_val );
    virtual bool ExecuteInputAction( const std::string& action, const InputTypeT type );
    virtual void PressUse( void );

// **** Drawing
protected:
    using Entity::DrawVBO;
    void DrawVBO( const float opacity ) const override;
public:
    void DrawHUD( void );

private:
    void DrawHUD_StatusBar( void );
    void DrawHUD_StatusBar_Health( void );
    void DrawHUD_Inventory( void );
    void PackHUD_StatusBar_Health( void );
    
    void DrawDebug( void ) const;
    void DrawFlowFieldDebug( void ) const;

// ***** AI
    /*! both entities must be in a navmesh to get a path.
      On success, the returned shared pointer will be the same as ent.
      If no path is available, nullptr will be returned.
      If some other entity is blocking our path, it will be returned.
    */
public:
    std::shared_ptr< Entity > AI_SetPathToWaypoint( const std::shared_ptr< Entity >& waypoint, const uint max_poly_moves ); //!< on success, sets path and returns the waypoint. failure may return a blocking entity or null Note: this works even if the entity is not of class Waypoint.
    std::shared_ptr< Entity > AI_SetPathTo( const std::shared_ptr< Entity >& ent, const uint max_poly_moves = AI_MAX_CHASE_NAV_POLIES ); //!< on success, sets and returns the enemy. failure may return a blocking entity or null
    std::shared_ptr< Entity > AI_ExecutePatrolScript( void ); //!< updates the path if needed and if so, returns the new waypoint. otherwise it will return nullptr, or in case of a failure will return a blocking entity
    virtual void AI_BumpedObstacle( const std::shared_ptr< Entity > & ent );
private:
    std::shared_ptr< Entity > AI_ExecutePatrolScriptExpression( const std::string& expression );
    std::shared_ptr< Entity > AI_ExecutePatrolScriptExpression_waypoint( const std::vector< std::string >& action );
    void AI_ExecutePatrolScriptExpression_wait( const std::vector< std::string >& action );
    void AI_ExecutePatrolScriptExpression_turn( const std::vector< std::string >& action );
    void AI_ExecutePatrolScriptExpression_turn_helper( const Vec3f& to_vec, const uint duration_ms );
    void AI_ExecutePatrolScriptExpression_turnTo( const std::vector< std::string >& action );
    void AI_ExecutePatrolScriptExpression_turnBy( const std::vector< std::string >& action );
    void AI_ExecutePatrolScriptExpression_turnBy_helper( const std::vector< std::string >& action, const Vec3f& from_vec );
public:
    virtual void DoAI( void );
#ifdef MONTICELLO_DEBUG
    void AI_Debug( void );
#endif //MONTICELLO_DEBUG
protected:
    void AI_FindEnemyInSight( void ); //!< returns the current enemy, or finds a new one. nullptr on failure
    void AI_FindEnemy( void ); //!< returns the current enemy, or finds, sets, and returns a new one. returns nullptr on failure
    bool AI_FindFoodInRoom( void );
    bool AI_FindFoodInSight( void );
    
    AI_ZoneT AI_GetZone( const float distToTarget ) const;
    
    bool AI_DoState_Actions( Entity& target );
    
    bool AI_Attack1( Entity& target );
    bool AI_Attack2( Entity& target );
    bool AI_Stalk( Entity& target );
    bool AI_Flee( Vec3f dir = Vec3f() );
    bool AI_Chase( Entity& target );
    bool AI_Idle( void );
    bool AI_Wander( void );
    bool AI_Interact( Entity& target );
    virtual bool AI_Special1( void );
    bool AI_FindEscape( const TypeID desired_type = MAX_UINT );
    Vec3f AI_GetNewWanderDirection( void );
    bool AI_IsAvoidingPlayer( void ) const;
    virtual bool AI_FollowPath( const float destination_radius = 1.0f );
    void AI_HurtBy( const Entity& inflictor );
    
private:
    void AI_Load( const AI_ZoneT zone, const std::vector< std::string >& actions );
    void AI_Load( void );
    
public:
    bool SetMoveVec( const Vec3f& move_dir );
    bool MoveToward( const Vec3f& origin_to );
    void TurnToWorldMousePos( void );
    void TurnToPos( const Vec3f& pos );

    virtual void Attack1( void );
    virtual void Attack2( void );
    
    virtual bool IsReadyToAttack1( void ) const;
    bool IsReadyToAttack2( void ) const;
    virtual void DoSpecial1( void );
    virtual void DoSpecial2( void );

    void FaceDir( const Vec3f& vec );
    void FaceSprite( const Sprite* sp );
    //bool CanTurn( void ) const override;
    
public:
    std::vector< std::string > GetAINearActions( void ) const;
    void SetAINearActions( const std::vector< std::string >& nearActions );
    void SetAINearActions( const std::string& nearActions_csv );

    std::vector< std::string > GetAIMidActions( void ) const;
    void SetAIMidActions( const std::vector< std::string >& midActions );
    void SetAIMidActions( const std::string& midActions_csv );

    std::vector< std::string > GetAIFarActions( void ) const;
    void SetAIFarActions( const std::vector< std::string >& farActions );
    void SetAIFarActions( const std::string& farActions_csv );

public:
    void ArrivedAtDestination( void );
    void RebootPatrolScript( void ); //!< start patrol script over from the beginning

    Vec3f GetAttackDir( void ) const override;
    
protected:
    void UnmarkUsableEntity( std::weak_ptr<Entity> ent_wkptr );
    void FindTargetedEntity( void );
    Sphere GetSphereForReachableArea( void ) const; //!< return a collision data for the area in which the actor can reach things (based on box_size, positioned directly in front of the player an centered)










public:
    Inventory inventory;

protected:
    NavPath navPath;
    std::vector< std::string > ai_NearActions;
    std::vector< std::string > ai_MidActions;
    std::vector< std::string > ai_FarActions;
    std::weak_ptr< Entity > targeted_entity; //!< entity the player is facing which he may "use" (will also be colorized_
    Box3D view_cone;
    
    std::vector< AI_ActionT > ai_actions[ static_cast<AI_ZoneT_BaseType>(AI_ZoneT::NUM_ZONES) ];
    std::weak_ptr< FlowField > destFlowField; //!< the destination field of the entity that we are pursuing
    
    std::weak_ptr< Entity > pursued_entity;
    std::weak_ptr< Entity > obstacle; //!< whatever the entity might be bumping into while walking
    Vec3f movingDir; //!< the current normalized direction the player is attempting to move
    Vec3f move; //!< same as movingDir except gets zeroed after it is applied to velocity (evaluated every frame for movement)
    VBO<float>* vbo_hud_health;
    VBO<float>* vbo_hud_health_uv;

protected:
    int32 last_packed_health;
    NavStateT_BaseType nav_state; //TOGAMESAVE

private:
    SETTER_GETTER( DamageAttack1, std::string, damageAttack1 );
    SETTER_GETTER( DamageAttack2, std::string, damageAttack2 );
    SETTER_GETTER( DamageDeath, std::string, damageDeath );
    SETTER_GETTER( DeathExplodeEffect, std::string, fx_deathExplode );
    SETTER_GETTER( Patrol, std::string, patrol );
    SETTER_GETTER_BYVAL( AINearMax, float, ai_NearMax );
    SETTER_GETTER_BYVAL( AIMidMax, float, ai_MidMax );
    SETTER_GETTER_BYVAL( AIFarMax, float, ai_FarMax );
    SETTER_GETTER_BYVAL( AIFleeTriggerRange, float, aiFleeTriggerRange );
    SETTER_GETTER_BYVAL( AISenseFoodRange, float, aiSenseFoodRange );
    SETTER_GETTER_BYVAL( DeathExplodeRadius, float, deathExplodeRadius );
    SETTER_GETTER_BYVAL( PrevThink, uint, prevThink );
    SETTER_GETTER_BYVAL( ThinkDelay, uint, thinkDelay );
    SETTER_GETTER_BYVAL( Attack1Cooldown, uint, attack1Cooldown );
    SETTER_GETTER_BYVAL( Attack2Cooldown, uint, attack2Cooldown );
    SETTER_GETTER_BYVAL( MaxRoomPatrol, uint, maxRoomPatrol );
    SETTER_GETTER_BYVAL( PrevEnemyCheck, uint, prevEnemyCheck );
    SETTER_GETTER_BYVAL( PrevFoodCheck, uint, prevFoodCheck );
    SETTER_GETTER_BYVAL( PrevEscapeCheck, uint, prevEscapeCheck );
    SETTER_GETTER_BYVAL( PrevFindCheck, uint, prevFindCheck );
    SETTER_GETTER_BYVAL( WanderMinDur, uint, wanderMinDur );
    SETTER_GETTER_BYVAL( WanderMaxDur, uint, wanderMaxDur );
    SETTER_GETTER_BYVAL( WanderDur, uint, wanderDur );
    SETTER_GETTER_BYVAL( SpawnImmunityDuration, uint, spawnImmunityDuration );
    SETTER_GETTER_BYVAL( AIWanderDelayPeriod, uint, aiWanderDelayPeriod );
    SETTER_GETTER_BYVAL( AIWanderActivePeriod, uint, aiWanderActivePeriod );
    SETTER_GETTER_BYVAL( AIWanderDelayedTime, uint, aiWanderDelayedTime );
    SETTER_GETTER_BYVAL( AIWanderActiveTime, uint, aiWanderActiveTime );
    SETTER_GETTER_BYVAL( PrevWander, uint, prevWander );
    SETTER_GETTER_BYVAL( FoodCheckDelay, uint, foodCheckDelay );
    SETTER_GETTER_BYVAL( LastTimeSleepAdded, uint, lastTimeSleepAdded );
    SETTER_GETTER_BYVAL( WayPointDuration, uint, waypoint_duration );
    SETTER_GETTER_BYVAL( PrevWaypointCheck, uint, prev_waypoint_check );
    SETTER_GETTER_BYVAL( CurWaypointIndex, uint, cur_waypoint_index );
    SETTER_GETTER_BYVAL( MaxPolyChase, uint, maxPolyChase );
    int sleepLevel;
    SETTER_GETTER_BYVAL( AIAvoidPlayer, bool, ai_AvoidPlayer );
    SETTER_GETTER_BYVAL( IgnoreAgressor, bool, ignoreAgressor );
    SETTER_GETTER_BYVAL( Running, bool, running );

// ** Named Setters
private:
    static const uint Actor_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum ActorSetterT : NamedSetterT_BaseType {
        Actor_SetAIAvoidPlayer = Actor_SETTERS_START
        , Actor_SetAINearMax
        , Actor_SetAIMidMax
        , Actor_SetAIFarMax
        , Actor_SetAIFleeTriggerRange
        , Actor_SetAISenseFoodRange
        , Actor_SetAINearActions
        , Actor_SetAIMidActions
        , Actor_SetAIFarActions
        , Actor_SetIgnoreAgressor
        , Actor_SetDamageAttack1
        , Actor_SetDamageAttack2
        , Actor_SetAttack1Cooldown
        , Actor_SetAttack2Cooldown
        , Actor_SetMaxRoomPatrol
        , Actor_SetPrevEnemyCheck
        , Actor_SetPrevFoodCheck
        , Actor_SetPrevEscapeCheck
        , Actor_SetPrevFindCheck
        , Actor_SetPatrol
        , Actor_SetRunning
        , Actor_SetWanderMinDur
        , Actor_SetWanderMaxDur
        , Actor_SetWanderDur
        , Actor_SetSpawnImmunityDuration
        , Actor_SetAIWanderDelayPeriod
        , Actor_SetAIWanderActivePeriod
        , Actor_SetAIWanderDelayedTime
        , Actor_SetAIWanderActiveTime
        , Actor_SetPrevWander
        , Actor_SetFoodCheckDelay
        , Actor_SetSleepLevel
        , Actor_SetLastTimeSleepAdded
        , Actor_SetDamageDeath
        , Actor_SetDeathExplodeEffect
        , Actor_SetPrevThink
        , Actor_SetThinkDelay
        , Actor_SetDeathExplodeRadius
        , Actor_SetWayPointDuration
        , Actor_SetPrevWaypointCheck
        , Actor_SetCurWaypointIndex
        , Actor_SetMaxPolyChase
        , Actor_SETTERS_END
        , Actor_NUM_SETTERS = Actor_SETTERS_END - Actor_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > actorSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
    static ToolTip< Actor, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_ACTOR_H_
