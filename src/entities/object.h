// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_OBJECTS_OBJECT_H_
#define SRC_OBJECTS_OBJECT_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"

template< typename TYPE >
class VBO;

class Object {
public:
    Object( void );
    virtual ~Object( void );
    Object( const Object& other );

public:
    Object& operator=( const Object& other );

public:
    virtual void SetName( const std::string& _name );
    std::string GetName( void ) const;

    void SetLastDrawFrame( const Uint32 frame );
    Uint32 GetLastDrawFrame( void ) const;

    virtual bool IsVisible( void ) const;
    virtual void SetVisible( const bool whether );

    virtual void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    virtual void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool );

    virtual void DrawVBO( void ) const {};

private:
    std::string name; // private because if it changes, we must modify game::all_entities list

protected:
    Uint32 lastDrawFrame;
    bool visible;
};

#endif  // SRC_OBJECTS_OBJECT_H_
