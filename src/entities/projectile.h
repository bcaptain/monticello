// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_PROJECTILE_H_
#define SRC_ENTITIES_PROJECTILE_H_

#include "./entity.h"
#include "../damage/damage.h"
#include "../game.h"

extern const int PROJECTILE_DEFAULT_DURATION;
extern const int PROJECTILE_TRAIL_DELAY;
extern const Uint32 HUGE_DELAY;

class Effect;

typedef Uint32 ProjectileOptsT_BaseType;
enum class ProjectileOptsT : ProjectileOptsT_BaseType {
    Empty = 0
    , ExplodeOnImpact = 1
    , ExplodeOnExpire = 1 << 1
    , IntersectDamage = 1 << 2
};
BITWISE_OPERATORS_FOR_ENUM_CLASS( ProjectileOptsT, ProjectileOptsT_BaseType )

typedef Uint8 ProjectileStatusT_BaseType;
enum class ProjectileStatusT : ProjectileStatusT_BaseType {
    NONE = 0,
    LAUNCHED = 1,
    EXPLODED = 2
};

typedef void(Projectile::*EFFECT_SETTER_METHOD_PTR)(const char*);


class Projectile : public Entity {
public:
    static constexpr TypeInfo<Projectile, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Projectile( void );
    ~Projectile( void ) override;
    Projectile( const Projectile& other );

public:
    Projectile& operator=( const Projectile& other );

    void Spawn( void ) override;

    void Think( void ) override;
    void RunFrame( void ) override;
    void Launch( void );
    void BumpedIntoBy( Entity& ent ) override;
    void BumpInto( Entity& ent ) override;

    virtual void Animate( void );

    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

    void SetSpeed( const float units_per_sec );
    float GetSpeed( void ) const;
    
    void SetBornTime( const uint to );
    uint GetBornTime( void ) const;

    void SetSpeedMod( const float units_per_sec );
    float GetSpeedMod( void ) const;

    virtual void Hit( Entity& ent );
    virtual void Explode( void );
    virtual void Expire( void );
    
public:
    void SetOwner( const std::weak_ptr< Entity >& _owner );
    const std::weak_ptr< Entity > GetOwner( void ) const;
    
    void SetTarget( const std::weak_ptr< Entity >& _target );
    
    void SetEffectFromGVal( const EntVal& entvalHash, EFFECT_SETTER_METHOD_PTR effectSetter ); //unused?

    void SetLaunchEffect( const std::string& fx );
    void SetLaunchEffect( const std::shared_ptr< const Effect >& fx );
    std::string GetLaunchEffectName( void ) const;
    
    void SetAttachedEffect( const std::string& fx );
    void SetAttachedEffect( const std::shared_ptr< const Effect >& fx );
    std::string GetAttachedEffectName( void ) const;

    void SetTrailEffect( const std::string& fx );
    void SetTrailEffect( const std::shared_ptr< const Effect >& fx );
    std::string GetTrailEffectName( void ) const;
    
    void SetTrailDelay( const uint to );
    uint GetTrailDelay( void ) const;

    void SetExplodeEffect( const std::string& fx );
    void SetExplodeEffect( const std::shared_ptr< const Effect >& fx );
    std::string GetExplodeEffectName( void ) const;
    
    void SetDir( const Vec3f& newDir );
    const Vec3f GetDir( void ) const;

    void SetHitDamage( const std::string& to );
    void SetHitDamage( const std::shared_ptr< const DamageInfo >& to );
    std::string GetHitDamageName( void ) const;
    
    void SetExplodeDamage( const std::string& to );
    void SetExplodeDamage( const std::shared_ptr< const DamageInfo >& to );
    std::string GetExplodeDamageName( void ) const;
    
    void SetLaunchSound( const std::string& snd_name );
    const std::string SetLaunchSound( void ) const;
    std::string GetLaunchSoundName( void ) const;

    void SetExplodeSound( const std::string& snd_name );
    const std::string SetExplodeSound( void ) const;
    std::string GetExplodeSoundName( void ) const;
    
    std::shared_ptr< const DamageInfo > GetExplodeDamage( void ) const;
    std::shared_ptr< const DamageInfo > GetHitDamage( void ) const;

    void SetDuration( const Uint _duration );
    Uint GetDuration( void ) const;

    bool IsSameTeamAsOwner( const Entity* collider ) const;
    
    void SetExplodeOnImpact( const bool whether );
    bool GetExplodeOnImpact( void ) const;
    void SetExplodeOnExpire( const bool whether );
    bool GetExplodeOnExpire( void ) const;
    void SetDamageIntersectors( const bool whether );
    bool GetDamageIntersectors( void ) const;
    void SetIntersectDelay( const Uint32 delay ); //!< will not affect entities already intersecting the projectile, only new ones
    Uint32 GetIntersectDelay( void ) const;
    
    void AddIntersector( Entity& ent );
    
protected:
    bool ReactsToPhysicsAgainst( const Entity& ent ) const override;
    void DamageIntersectingEntities( void );

protected:
    std::shared_ptr< const DamageInfo > damage_explode;
    std::shared_ptr< const DamageInfo > damage_hit;
    Vec3f launchPoint;
    Vec3f dir;
    std::string snd_launch;
    std::string snd_explode;
    std::shared_ptr< const Effect > fx_launch;
    std::shared_ptr< const Effect > fx_explode;
    std::shared_ptr< const Effect > fx_trail;
    std::shared_ptr< const Effect > fx_attached;
    std::weak_ptr< Entity > owner;
    std::weak_ptr< Entity > target;
    std::vector< PersistentEvent< DamageEvent > > intersectingEntities;
    uint prevTrail;
    uint trailDelay;
    uint duration;
    uint bornTime;
    float speed;
    float speedMod;
    ProjectileStatusT status;
    Uint32 intersect_delay;
    ProjectileOptsT opts;

// ** Named Setters
private:
    static const uint Projectile_SETTERS_START = Entity_NUM_SETTERS;
protected:
    enum ProjectileSetterT : NamedSetterT_BaseType {
        Projectile_SetLaunchEffect = Projectile_SETTERS_START
        , Projectile_SetAttachedEffect
        , Projectile_SetTrailEffect
        , Projectile_SetExplodeEffect
        , Projectile_SetTrailDelay
        , Projectile_SetDuration
        , Projectile_SetSpeed
        , Projectile_SetSpeedMod
        , Projectile_SetExplodeDamage
        , Projectile_SetHitDamage
        , Projectile_SetExplodeOnImpact
        , Projectile_SetExplodeOnExpire
        , Projectile_SetDamageIntersectors
        , Projectile_SetIntersectDelay
        , Projectile_SetLaunchSound
        , Projectile_SetExplodeSound
        , Projectile_SETTERS_END
        , Projectile_NUM_SETTERS = Projectile_SETTERS_END - Projectile_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > projectileSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Projectile>( get_typeid<Entity>() ) );
    static ToolTip< Projectile, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_PROJECTILE_H_
