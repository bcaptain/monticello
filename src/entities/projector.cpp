// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./projector.h"
#include "../rendering/renderer.h"
#include "../rendering/materialManager.h"
#include "../game.h"

const float DEFAULT_PROJECTOR_FOV = 60.0f;
const float DEFAULT_PROJECTOR_SIDELENGTH = 1.0f;
const float DEFAULT_PROJECTOR_WIDTH = 50.0f;
const float DEFAULT_PROJECTOR_HEIGHT = 50.0f;
const float DEFAULT_PROJECTOR_NEARPLANE = 1.0f;
const float DEFAULT_PROJECTOR_FARPLANE = 5.0f;

std::vector< std::string > Projector::projectorSetters;

void Projector::InitSetterNames( void ) {
    if ( projectorSetters.size() == Projector_NUM_SETTERS )
        return; // already set
    projectorSetters.resize(Projector_NUM_SETTERS);
    
    const uint i = Projector_SETTERS_START;
    projectorSetters[Projector_SetProjectionOpacity-i] = "projection_opacity";
    projectorSetters[Projector_SetSideLength-i] = "sideLength";
    projectorSetters[Projector_SetFOV-i] = "fov";
    projectorSetters[Projector_SetWidth-i] = "width";
    projectorSetters[Projector_SetHeight-i] = "height";
    projectorSetters[Projector_SetNearPlane-i] = "nearPlane";
    projectorSetters[Projector_SetFarPlane-i] = "farPlane";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Projector,Entity>(projectorSetters);
}

bool Projector::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<ProjectorSetterT>(setter) ) {
        case Projector_SetProjectionOpacity: SetProjectionOpacity( String::ToFloat( value ) ); return true;
        case Projector_SetSideLength: SetSideLength( String::ToFloat( value ) ); return true;
        case Projector_SetFOV: SetFOV( String::ToFloat( value ) ); return true;
        case Projector_SetWidth: SetWidth( String::ToFloat( value ) ); return true;
        case Projector_SetHeight: SetHeight( String::ToFloat( value ) ); return true;
        case Projector_SetNearPlane: SetNearPlane( String::ToFloat( value ) ); return true;
        case Projector_SetFarPlane: SetFarPlane( String::ToFloat( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Projector_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Projector_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Projector>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Projector::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<ProjectorSetterT>(setter) ) {
        case Projector_SetProjectionOpacity: return String::ToString(GetProjectionOpacity());
        case Projector_SetSideLength: return String::ToString(GetSideLength());
        case Projector_SetFOV: return String::ToString(GetFOV());
        case Projector_SetWidth: return String::ToString(GetWidth());
        case Projector_SetHeight: return String::ToString(GetHeight());
        case Projector_SetNearPlane: return String::ToString(GetNearPlane());
        case Projector_SetFarPlane: return String::ToString(GetFarPlane());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Projector_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Projector_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Projector>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Projector::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( projectorSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Projector_SETTERS_START;
        
    static_assert( is_parent_of<Projector>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Projector::Projector( void )
    : matMVP()
    , projMatrix()
    , projection_opacity(1.0f)
    , sideLength(DEFAULT_PROJECTOR_SIDELENGTH)
    , fov(DEFAULT_PROJECTOR_FOV)
    , width(DEFAULT_PROJECTOR_WIDTH)
    , height(DEFAULT_PROJECTOR_HEIGHT)
    , nearPlane(DEFAULT_PROJECTOR_NEARPLANE)
    , farPlane(DEFAULT_PROJECTOR_FARPLANE)
    , vbo_vert( std::make_unique<VBO<float> >( 3, VBOChangeFrequencyT::RARELY ) )
    , vbo_color( std::make_unique<VBO<float> >( 3, VBOChangeFrequencyT::RARELY ) )
    , projectionMtl()
    , type( ProjectionTypeT::Ortho )
    , targets( ProjectorTargetsT::Modules )
{ }

Projector::Projector( const Projector& other )
    : Entity( other )
    , matMVP(other.matMVP)
    , projMatrix(other.projMatrix)
    , projection_opacity(other.projection_opacity)
    , sideLength(other.sideLength)
    , fov(other.fov)
    , width(other.width)
    , height(other.height)
    , nearPlane(other.nearPlane)
    , farPlane(other.farPlane)
    , vbo_vert( std::make_unique<VBO<float> >( 3, VBOChangeFrequencyT::RARELY ) )
    , vbo_color( std::make_unique<VBO<float> >( 3, VBOChangeFrequencyT::RARELY ) )
    , projectionMtl( other.projectionMtl )
    , type(other.type)
    , targets(other.targets)
{ }

Projector& Projector::operator=( const Projector& other ) {
    vbo_vert->Clear();
    vbo_color->Clear();
    projection_opacity = other.projection_opacity;
    sideLength = other.sideLength;
    fov = other.fov;
    width = other.width;
    height = other.height;
    nearPlane = other.nearPlane;
    farPlane = other.farPlane;
    matMVP = other.matMVP;
    projMatrix = other.projMatrix;
    projectionMtl = other.projectionMtl;
    type = other.type;
    targets = other.targets;
    
    Entity::operator=( other );
    UpdateVBO();
    return *this;
}

Projector::~Projector( void ) {
    if ( renderer )
        renderer->RemoveProjector( GetWeakPtr().lock() );
}

void Projector::Spawn( void ) {
    ASSERT_MSG( renderer, "Tried to spawn a projector before the renderer was created" );
    renderer->AddProjector( *this );
    Entity::Spawn();
}

bool Projector::SetProjectorModelMaterial( const char* mtl ) {
    return Entity::SetMaterial( materialManager.Get( mtl ) );
}

bool Projector::SetProjectorModelMaterial( const std::shared_ptr< const Material >& mtl ) {
    return Entity::SetMaterial( mtl );
}

bool Projector::SetMaterial( const std::shared_ptr< const Material >& mtl ) {
    projectionMtl = mtl;
    return true;
}

bool Projector::SetMaterial( const char* mtl ) {
    projectionMtl = materialManager.Get( mtl );
    return true;
}

std::shared_ptr< const Material > Projector::GetMaterial( void ) const {
    return projectionMtl;
}

void Projector::UnsetMaterial( void ) {
    projectionMtl.reset();
}

bool Projector::SetMaterialToDefault( void ) {
    projectionMtl.reset(); // default is none
    return true;
}

void Projector::UpdateVBO( void ) {
    vbo_vert->Clear();
    vbo_color->Clear();
    
    Entity::UpdateVBO();
}

void Projector::DrawVBO( const float opacity ) const {
    if ( !globalVals.GetBool( gval_d_projectors ) )
        return;
        
    Entity::DrawVBO( opacity );
        
    ASSERT( vbo_vert->Finalized() );

    shaders->UseProg(  GLPROG_GRADIENT  );
    shaders->SendData_Matrices();
    shaders->SetAttrib( "vColor", *vbo_color );
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->DrawArrays( GL_LINES, 0, vbo_vert->Num() );
}

void Projector::PackVBO( void ) {
    if ( !globalVals.GetBool( gval_d_projectors ) )
        return;
        
    bounds.PackVBO();
        
    ASSERT( vbo_vert );
    
    if ( vbo_vert->Finalized() )
        return;

    const AABox3D two_unit_cube(
      -1.0f,1.0f
      ,-1.0f,1.0f
      ,-1.0f,1.0f
    );
    
    Box3D drawFrustum(
          two_unit_cube.GetVert_FarLeftTop()
        , two_unit_cube.GetVert_FarRightTop()
        , two_unit_cube.GetVert_NearLeftTop()
        , two_unit_cube.GetVert_NearRightTop()
        , two_unit_cube.GetVert_NearLeftBottom()
        , two_unit_cube.GetVert_NearRightBottom()
        , two_unit_cube.GetVert_FarLeftBottom()
        , two_unit_cube.GetVert_FarRightBottom()
    );
    
    Mat4f mat;
    mat.RotateZYX( bounds.primitive->GetAngles() );
    
    
    // ** Pack Orientation Lines
    const Vec3f drawFrustumCenter = drawFrustum.GetCenter();
    const Box3D tmpDrawFrustum = drawFrustum * mat;
    vbo_vert->Pack( drawFrustumCenter );
    vbo_vert->Pack( tmpDrawFrustum.GetRightNormal() * globalVals.GetFloat( gval_d_axisLineLength, 1 ) ); //X Positive Axis
    vbo_color->PackRepeatedly( 2, Vec3f(1,0,0) );
    
    vbo_vert->Pack( drawFrustumCenter );
    vbo_vert->Pack( tmpDrawFrustum.GetTopNormal() * globalVals.GetFloat( gval_d_axisLineLength, 1 ) ); //Y Positive Axis
    vbo_color->PackRepeatedly( 2, Vec3f(0,1,0) );

    vbo_vert->Pack( drawFrustumCenter );
    vbo_vert->Pack( tmpDrawFrustum.GetNearNormal() * globalVals.GetFloat( gval_d_axisLineLength, 1 ) ); //Z Positive Axis
    vbo_color->PackRepeatedly( 2, Vec3f(0,0,1) );
    
    Mat4f persp( projMatrix );
    persp.Invert();
    mat *= persp;
       
    drawFrustum *= mat;
/*
    const float x = Vec3f::Len( tmpDrawFrustum.GetVert_FarLeftTop() - tmpDrawFrustum.GetVert_FarRightTop() ) / 2;
    const float y = Vec3f::Len( tmpDrawFrustum.GetVert_NearLeftTop() - tmpDrawFrustum.GetVert_FarLeftTop() ) / 2;
    const float z = Vec3f::Len( tmpDrawFrustum.GetVert_FarLeftTop() - tmpDrawFrustum.GetVert_FarRightBottom() ) / 2;
    bounds.SetToAABox3D(-x,x,-y,y,-z,z);
    */
    // ** Pack Outline
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_LEFT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_LEFT_TOP_INDEX) );

    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );

    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_LEFT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo_vert->Pack( drawFrustum.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    
    vbo_vert->MoveToVideoCard();
    
    const Vec3f purple(1,0,1);
    vbo_color->PackRepeatedly( 24, purple );
    vbo_color->MoveToVideoCard();
}

bool Projector::CanTarget( const Entity& entity ) const {
    switch ( targets ) {
        case ProjectorTargetsT::Modules: return derives_from( TypeID_Module, entity );
        case ProjectorTargetsT::Actors: return derives_from( TypeID_Actor, entity );
        case ProjectorTargetsT::Everything: return true;
        case ProjectorTargetsT::Nothing:
            FALLTHROUGH;
        default: return false;
    }
}

void Projector::SendShaderData( void ) {
    if ( ! projectionMtl )
        return;

    ComputeMVPMatrix();
    
    shaders->SetUniform1f( "opacity", projection_opacity );
    shaders->SetUniformMatrix4fv( "texMat", 1, GL_FALSE, matMVP.data );
    shaders->SetUniformMatrix4fv( "invViewMat", 1, GL_FALSE, renderer->InverseViewMatrix().data );
    shaders->SetTexture( projectionMtl->GetDiffuse()->GetID(), 1, "tex1" );
    return;
}

void Projector::ComputeMVPMatrix( void ) {
    ComputeProjectionMatrix();
    
    const float bias_mat[16]{0.5,0,0,0,0,0.5,0,0,0,0,0.5,0,0.5,0.5,0.5,1};
    const Mat4f bias( bias_mat );
    
    Mat4f mvMat;
    mvMat.RotateXYZ( -bounds.primitive->GetAngles() );
    mvMat.Translate( -GetOrigin() );
    
    matMVP = bias * projMatrix * mvMat;
}

void Projector::ComputeProjectionMatrix_Ortho( void ) {    
    //TODO : skip calculations if nothing changed
    
    projMatrix.LoadIdentity();
    projMatrix.Orthographic(-sideLength,sideLength,-sideLength,sideLength,-sideLength,sideLength);
}

void Projector::ComputeProjectionMatrix_Perspective( void ) {
    //TODO: skip calculations if nothing changed
        
    projMatrix.LoadIdentity();
    projMatrix.Perspective(
          fov
        , width
        , height
        , nearPlane
        , farPlane
    );
}

void Projector::ComputeProjectionMatrix( void ) {
    switch( type ) {
        case ProjectionTypeT::Ortho: ComputeProjectionMatrix_Ortho(); break;
        case ProjectionTypeT::Perspective: ComputeProjectionMatrix_Perspective(); break;
        default: ERR("Projector type not defined..\n"); break;
    }
}

bool Projector::IsVisible( void ) const {
    return globalVals.GetBool( gval_d_projectors );
}

void Projector::SetType( ProjectionTypeT& to ) {
    type = to;
}

ProjectionTypeT Projector::GetType( void ) const {
    return type;
}

void Projector::SetProjectionOpacity( const float to ) {
    projection_opacity = to;
}

float Projector::GetProjectionOpacity( void ) const {
    return projection_opacity;
}

void Projector::SetSideLength( const float to ) {
    sideLength = to;
}

float Projector::GetSideLength( void ) const {
    return sideLength;
}

void Projector::SetFOV( const float to ) {
    fov = to;
}

float Projector::GetFOV( void ) const {
    return fov;
}

void Projector::SetWidth( const float to ) {
    width = to;
}

float Projector::GetWidth( void ) const {
    return width;
}

void Projector::SetHeight( const float to ) {
    height = to;
}

float Projector::GetHeight( void ) const {
    return height;
}

void Projector::SetNearPlane( const float to ) {
    nearPlane = to;
}

float Projector::GetNearPlane( void ) const {
    return nearPlane;
}

void Projector::SetFarPlane( const float to ) {
    farPlane = to;
}

float Projector::GetFarPlane( void ) const {
    return farPlane;
}

void Projector::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Projector>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, projectorSetters );
}

void Projector::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Projector>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, projectorSetters );
}
