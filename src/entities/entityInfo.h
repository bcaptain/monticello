// Copyright 2010-2019 Brandon Captain. You may not copy this work.

/*
Note on reinterpret_cast:
    C++17 standard [expr.reinterpret.cast]/10) guarantees that method pointers
    of differing classes can be casted back and forth without issue:
    
    "A prvalue of type “pointer to member of X of type T1” can be explicitly
    converted to a prvalue of a different type “pointer to member of Y of
    type T2” if T1 and T2 are both function types or both object types. The
    null member pointer value is converted to the null member pointer value
    of the destination type. The result of this conversion is unspecified,
    except in the following cases:

    * converting a prvalue of type “pointer to member function” to a different
    pointer to member function type and back to its original type yields the
    original pointer to member value.
    [...]"
    
    Url: https://stackoverflow.com/q/56938785
    
Note: MSVC may not like this as it violates the standard for optimizations:
    Url: https://stackoverflow.com/q/30923296
*/

#include "./clib/src/warnings.h"
#ifndef SRC_OBJECTS_EntityInfo_H_
#define SRC_OBJECTS_EntityInfo_H_

#include "../base/main.h"

class Entity;

extern const float SPHERE_RADIUS_DEFAULT;

class EntityInfoManager;

class EntityInfo {
public:
    class EntityInfoManager_Attorney {
        friend class EntityInfoManager;
    private:
        static void SetEntName( EntityInfo& entInfo, const std::string& _name );
        
    };
public:
    EntityInfo( void ) = delete;
    explicit EntityInfo( const uint _type );
    EntityInfo( const char* _classname );
    
    EntityInfo( const EntityInfo& other );
    EntityInfo( EntityInfo&& other_rref );
    
    ~EntityInfo( void ) = default;

    EntityInfo& operator=( const EntityInfo& other );
    EntityInfo& operator=( EntityInfo&& other_rref );

    uint GetObjectTypeID( void ) const;
    std::string GetTypeName( void ) const;
    std::string GetEntName( void ) const;
    std::string GetMemberValue( const std::string& key ) const;

    std::vector< std::pair< uint, std::string > > entSets;
    std::vector< std::pair< std::string, std::string > > entVals;  //toNamedSetter: remove
    std::vector< std::pair< std::string, std::string > > entMembers;
    
private:
    void SetEntName( const std::string& _name );

private:
    std::string entname;
    uint type;
};

#endif  // SRC_OBJECTS_EntityInfo_H_
