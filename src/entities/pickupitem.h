// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_PICKUPITEM_H_
#define SRC_ENTITIES_PICKUPITEM_H_

#include "../base/main.h"
#include "./actor.h"

class Effect;

/*!

PickupItem \n\n

An item that can be picked up by actors \n\n

**/


class PickupItem : public Entity {
public:
    static constexpr TypeInfo<PickupItem, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    PickupItem( void );
    PickupItem( const PickupItem& other );
    ~PickupItem( void ) override;

public:
    PickupItem& operator=( const PickupItem& other );

public:
    void Spawn( void ) override;
    void BumpedIntoBy( Entity& ent ) override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    void SetPickupSound( const std::string& pickup_snd );
    std::string GetPickupSound( void ) const;
    
private:
    bool GiveItems( Actor& player );
    bool AddHealth( Actor& player );
    void PlayPickupSound( void );

public:
    std::string snd_pickup;
    bool pickedUp;

// ** Named Setters
protected:
    enum PickupItemSetterT : NamedSetterT_BaseType {
        PickupItem_SETTERS_START = Entity_NUM_SETTERS
        , PickupItem_SetPickupSound = PickupItem_SETTERS_START
        , PickupItem_SETTERS_END
        , PickupItem_NUM_SETTERS = PickupItem_SETTERS_END - PickupItem_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > pickupItemSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
    static_assert( is_parent_of<PickupItem>( get_typeid<Entity>() ) );
    static ToolTip< PickupItem, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_PICKUPITEM_H_
