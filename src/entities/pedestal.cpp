// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "pedestal.h"
#include "../game.h"
#include "entityInfoManager.h"
#include "../sprites/sprite.h"
#include "../rendering/animation/animInfo.h"

const auto ANIMSTATE_PEDESTAL_WOBBLE = AnimStateT::Special1;

const Uint32 USE_STEADY_TIMER_THRESHOLD = 250;
const int STEADY_AMOUNT_THRESHOLD = 1000;
const int STEADY_AMOUNT_PER_FRAME = 40;
const int WOBBLE_AMOUNT_PER_FRAME = 3;

const float DEFAULT_PEDESTAL_UI_EDGE_LENGTH = 0.8f;

const int FALLOVER_THRESHOLD = 0;

std::vector< std::string > Pedestal::pedestalSetters;

void Pedestal::InitSetterNames( void ) {
    if ( pedestalSetters.size() == Pedestal_NUM_SETTERS )
        return; // already set
    pedestalSetters.resize(Pedestal_NUM_SETTERS);
    
    const uint i = Pedestal_SETTERS_START;
    pedestalSetters[Pedestal_SetSteadyValue-i] = "steadyValue";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Pedestal,Entity>(pedestalSetters);
}

bool Pedestal::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<PedestalSetterT>(setter) ) {
        case Pedestal_SetSteadyValue: SetSteadyValue( String::ToInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Pedestal_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Pedestal_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Pedestal>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}
std::string Pedestal::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<PedestalSetterT>(setter) ) {
        case Pedestal_SetSteadyValue: return String::ToString(GetSteadyValue());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Pedestal_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Pedestal_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Pedestal>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}
    
NamedSetterT Pedestal::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( pedestalSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Pedestal_SETTERS_START;
        
    static_assert( is_parent_of<Pedestal>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

Pedestal::Pedestal( void )
    : Entity()
    , useMeter()
    , steadyValue()
    {
}

Pedestal::Pedestal( const Pedestal& other ) 
    : Entity( other )
    , useMeter()
    , steadyValue(other.steadyValue)
    {
}

Pedestal& Pedestal::operator=( const Pedestal& other ) {
    Entity::operator=( other );
    
    steadyValue = other.steadyValue;
    return *this;
}

Pedestal::~Pedestal( void ) {
}

void Pedestal::Think( void ) {
    Entity::Think();
    
    if ( IsWobbling() )
        UpdateSteadyValue();
        
}

void Pedestal::Spawn( void ) {
    Entity::Spawn();
    
    const std::string attach_pos = "onTop";
    const std::string attach_name = "urn";
    AddAttachment( attach_pos, attach_name, 0 ); //todo: attachmentChannel
}

bool Pedestal::IsUsable( void ) const {
    return IsWobbling();
}

bool Pedestal::IsUsableBy( const Actor* user ) const {
    
    if ( !user )
        return false;
    
    switch ( user->GetObjectTypeID() ) {
        case TypeID_Beetle : return modelInfo.NumAttachments() > 0 && !IsWobbling() ; break;
        case TypeID_Eugene : return IsWobbling(); break;
        default :
            WARN("Interaction not defined : %s -with- %s.\n", get_classname( GetObjectTypeID() ).c_str(), get_classname( user->GetObjectTypeID() ).c_str() );
            return false;
    }

    return true;
}

void Pedestal::SetMarkedForUse( const bool whether ) {
    if ( whether && !IsUsable() )
        return;
    Entity::SetMarkedForUse( whether );
}

bool Pedestal::UseBy( Actor* user ) {
    
    if ( !user )
        return false;
    
    switch ( user->GetObjectTypeID() ) {
        case TypeID_Beetle : StartWobble(); return true;
        case TypeID_Eugene : SteadyWobble(); return true;
        default :
            WARN("Interaction not defined : %s -with- %s.\n", get_classname( GetObjectTypeID() ).c_str(), get_classname( user->GetObjectTypeID() ).c_str() );
            return false;
    }
}

void Pedestal::StartWobble( void ) {
    modelInfo.AddAnimState( ANIMSTATE_PEDESTAL_WOBBLE );
    SpawnSteadyUI();
}

void Pedestal::SteadyWobble( void ) {

    steadyValue += STEADY_AMOUNT_PER_FRAME;
    
    if ( steadyValue >= STEADY_AMOUNT_THRESHOLD ) {
        modelInfo.ClearAnimState();
        useMeter.reset();
        steadyValue = STEADY_AMOUNT_THRESHOLD;
    }
    
    useValue = steadyValue;
    lastTimeUsed = game->GetGameTime();
}

bool Pedestal::IsWobbling( void ) const {
    return modelInfo.HasAnimState( ANIMSTATE_PEDESTAL_WOBBLE );
}

void Pedestal::Animate( void ) {

    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "wobble", ANIMSTATE_PEDESTAL_WOBBLE, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
        , { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

void Pedestal::UpdateSteadyValue( void ) {

    const Uint32 gameTime = game->GetGameTime();

    if ( gameTime - lastTimeUsed > USE_STEADY_TIMER_THRESHOLD ) {
        steadyValue -= WOBBLE_AMOUNT_PER_FRAME;
        useValue = steadyValue;
    }
    
    if ( steadyValue < FALLOVER_THRESHOLD ) {
        DropValuable();
        modelInfo.RemAnimState( ANIMSTATE_PEDESTAL_WOBBLE );
        steadyValue = STEADY_AMOUNT_THRESHOLD;
    }
    
    UpdateSteadyUIValue();
}

void Pedestal::UpdateSteadyUIValue() {
    if ( !useMeter )
        return;
    
    useMeter->progress = GetSteadyValue_Normalized();
}

float Pedestal::GetSteadyValue_Normalized( void ) const {
    return static_cast<float>(steadyValue) / STEADY_AMOUNT_THRESHOLD;
}

void Pedestal::SpawnSteadyUI( void ) {
    const auto & mdl = GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;
    
    if ( !useMeter ) {
        useMeter = std::make_shared< Meter >(DEFAULT_PEDESTAL_UI_EDGE_LENGTH,DEFAULT_PEDESTAL_UI_EDGE_LENGTH);
        useMeter->SetMaterial( "steadyPedestal" );
        useMeter->SetMaskMaterial( "radialMaskUI" );
        useMeter->SetupUV();
    }
    
    Vec3f useMeterOrigin = GetOrigin();
    useMeterOrigin.x -= useMeter->GetWidth() / 2;
    useMeterOrigin.z = height + useMeter->GetHeight();
    
    useMeter->SetOrigin( useMeterOrigin );
    
    auto sprite = std::static_pointer_cast< Sprite >(useMeter);
    game->AddWorldUI( sprite );
}

void Pedestal::DropValuable( void ) {
    
    useMeter.reset();
    
    if ( modelInfo.NumAttachments() < 1 )
        return;
    
    const ModelInfo* attachment = modelInfo.GetAttachment(0);
    
    if ( !attachment )
        return;
        
    const ArmatureInfo* arm_info = attachment->GetArmatureInfo( AnimChannelT::Channel_0 );
    
    Vec3f attach_pos;
    arm_info->GetAttachmentPos( attach_pos );
    
    auto ent = entityInfoManager.LoadAndSpawn( "urn", attach_pos + GetOrigin() );
    ent->WakeUp();
        
    modelInfo.RemoveAttachmentAtIndex( 0 );
}

void Pedestal::SetSteadyValue( const int to ) {
    steadyValue = to;
}

int Pedestal::GetSteadyValue( void ) const {
    return steadyValue;
}

void Pedestal::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Pedestal>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, pedestalSetters );
}

void Pedestal::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Pedestal>( get_typeid<Entity>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, pedestalSetters );
}
