// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./actor.h"
#include "../game.h"
#include "../math/collision/collision.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../rendering/models/model.h"
#include "../rendering/models/modelManager.h"
#include "../rendering/materialManager.h"
#include "../rendering/fontManager.h"
#include "../rendering/models/modelInfo.h"
#include "../rendering/font.h"
#include "../nav/navMesh.h"
#include "../damage/damageManager.h"
#include "../input/input.h"
#include "../particles/effectsManager.h"
#include "../sound.h"
#include "./triggers/trigger.h"
#include "./projectile.h"
#include "./entityInfoManager.h"
#include "../entities/modules/flat.h"
#include "../nav/flowField.h"
#include "../input/playerInput.h"
#include "../rendering/animation/animInfo.h"

constexpr const bool moveSpeedDebug = 0;

const float ACTOR_DEFAULT_MOVE_SPEED = 3.5; // units/meters per second
const float ACTOR_MAX_SPEED_FOR_CREEP = 0.5;
const float ACTOR_DEFAULT_VIEW_WIDTH = 10;
const float ACTOR_DEFAULT_VIEW_HEIGHT = 1.5;
const float ACTOR_DEFAULT_VIEW_DIST = 10;
const float ACTOR_DEFAULT_VIEW_PERIPHERAL_WIDTH=1;
const float ACTOR_DEFAULT_VIEW_PERIPHERAL_HEIGHT=0.2f;

const uint ACTOR_DEFAULT_MAX_ROOM_PATROL = 4;

const float DEFAULT_SIGHT_OFFSET = 1;
const float DEFAULT_SIGHT_DISTANCE = 5;
const float DEFAULT_SIGHT_ANGLE = 90;

const uint ACTOR_DEFAULT_WANDER_MIN = 2000;
const uint ACTOR_DEFAULT_WANDER_MAX = 4000;

const Color4f HUD_HEALTH_FONT_COLOR(1,0,0,1);
const float HUD_HEALTH_FONT_SIZE(48);

const int SLEEP_THRESHOLD = 100;

const Box3D ACTOR_DEFAULT_VIEW_CONE(
    Vec3f(ACTOR_DEFAULT_VIEW_PERIPHERAL_WIDTH,0,-ACTOR_DEFAULT_VIEW_PERIPHERAL_HEIGHT),
    Vec3f(ACTOR_DEFAULT_VIEW_PERIPHERAL_WIDTH,0,ACTOR_DEFAULT_VIEW_PERIPHERAL_HEIGHT),
    Vec3f(ACTOR_DEFAULT_VIEW_WIDTH,-ACTOR_DEFAULT_VIEW_DIST,-ACTOR_DEFAULT_VIEW_HEIGHT),
    Vec3f(ACTOR_DEFAULT_VIEW_WIDTH,-ACTOR_DEFAULT_VIEW_DIST,ACTOR_DEFAULT_VIEW_HEIGHT),
    Vec3f(-ACTOR_DEFAULT_VIEW_WIDTH,-ACTOR_DEFAULT_VIEW_DIST,-ACTOR_DEFAULT_VIEW_HEIGHT),
    Vec3f(-ACTOR_DEFAULT_VIEW_WIDTH,-ACTOR_DEFAULT_VIEW_DIST,ACTOR_DEFAULT_VIEW_HEIGHT),
    Vec3f(-ACTOR_DEFAULT_VIEW_PERIPHERAL_WIDTH,0,-ACTOR_DEFAULT_VIEW_PERIPHERAL_HEIGHT),
    Vec3f(-ACTOR_DEFAULT_VIEW_PERIPHERAL_WIDTH,0,ACTOR_DEFAULT_VIEW_PERIPHERAL_HEIGHT)
);

std::vector< std::string > Actor::actorSetters;

void Actor::InitSetterNames( void ) {
    if ( actorSetters.size() == Actor_NUM_SETTERS )
        return; // already set
    actorSetters.resize(Actor_NUM_SETTERS);
    
    const uint i = Actor_SETTERS_START;
    actorSetters[Actor_SetAIAvoidPlayer-i] = "aiAvoidPlayer";
    actorSetters[Actor_SetAINearMax-i] = "aiNearMax";
    actorSetters[Actor_SetAIMidMax-i] = "aiMidMax";
    actorSetters[Actor_SetAIFarMax-i] = "aiFarMax";
    actorSetters[Actor_SetAIFleeTriggerRange-i] = "aiFleeTriggerRange";
    actorSetters[Actor_SetAISenseFoodRange-i] = "aiSenseFoodRange";
    actorSetters[Actor_SetAINearActions-i] = "aiNearActions";
    actorSetters[Actor_SetAIMidActions-i] = "aiMidActions";
    actorSetters[Actor_SetAIFarActions-i] = "aiFarActions";
    actorSetters[Actor_SetIgnoreAgressor-i] = "ignoreAgressor";
    actorSetters[Actor_SetDamageAttack1-i] = "damageAttack1";
    actorSetters[Actor_SetDamageAttack2-i] = "damageAttack2";
    actorSetters[Actor_SetAttack1Cooldown-i] = "attack1Cooldown";
    actorSetters[Actor_SetAttack2Cooldown-i] = "attack2Cooldown";
    actorSetters[Actor_SetMaxRoomPatrol-i] = "maxRoomPatrol";
    actorSetters[Actor_SetPatrol-i] = "patrol";
    actorSetters[Actor_SetPrevEnemyCheck-i] = "prevEnemyCheck";
    actorSetters[Actor_SetPrevFoodCheck-i] = "prevFoodCheck";
    actorSetters[Actor_SetPrevEscapeCheck-i] = "prevEscapeCheck";
    actorSetters[Actor_SetPrevFindCheck-i] = "prevFindCheck";
    actorSetters[Actor_SetRunning-i] = "running";
    actorSetters[Actor_SetWanderMinDur-i] = "wanderMinDur";
    actorSetters[Actor_SetWanderMaxDur-i] = "wanderMaxDur";
    actorSetters[Actor_SetWanderDur-i] = "wanderDur";
    actorSetters[Actor_SetAIWanderDelayPeriod-i] = "aiWanderDelayPeriod";
    actorSetters[Actor_SetAIWanderActivePeriod-i] = "aiWanderActivePeriod";
    actorSetters[Actor_SetAIWanderDelayedTime-i] = "aiWanderDelayedTime";
    actorSetters[Actor_SetAIWanderActiveTime-i] = "aiWanderActiveTime";
    actorSetters[Actor_SetPrevWander-i] = "prevWander";
    actorSetters[Actor_SetFoodCheckDelay-i] = "foodCheckDelay";
    actorSetters[Actor_SetSleepLevel-i] = "sleepLevel";
    actorSetters[Actor_SetLastTimeSleepAdded-i] = "lastTimeSleepAdded";
    actorSetters[Actor_SetSpawnImmunityDuration-i] = "spawnImmunityDuration";
    actorSetters[Actor_SetDamageDeath-i] = "damageDeath";
    actorSetters[Actor_SetDeathExplodeEffect-i] = "fx_deathExplode";
    actorSetters[Actor_SetDeathExplodeRadius-i] = "deathExplodeRadius";
    actorSetters[Actor_SetPrevThink-i] = "prevThink";
    actorSetters[Actor_SetThinkDelay-i] = "thinkDelay";
    actorSetters[Actor_SetWayPointDuration-i] = "waypoint_duration";
    actorSetters[Actor_SetPrevWaypointCheck-i] = "prev_waypoint_check";
    actorSetters[Actor_SetCurWaypointIndex-i] = "cur_waypoint_index";
    actorSetters[Actor_SetMaxPolyChase-i] = "maxPolyChase";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Actor,Entity>(actorSetters);
}

bool Actor::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<ActorSetterT>(setter) ) {
        case Actor_SetAIAvoidPlayer: SetAIAvoidPlayer( String::ToBool(value) ); return true;
        case Actor_SetAINearMax: SetAINearMax( String::ToFloat(value) ); return true;
        case Actor_SetAIMidMax: SetAIMidMax( String::ToFloat(value) ); return true;
        case Actor_SetAIFarMax: SetAIFarMax( String::ToFloat(value) ); return true;
        case Actor_SetAIFleeTriggerRange: SetAIFleeTriggerRange( String::ToFloat(value) ); return true;
        case Actor_SetAISenseFoodRange: SetAISenseFoodRange( String::ToFloat(value) ); return true;
        case Actor_SetAINearActions: SetAINearActions( value ); return true;
        case Actor_SetAIMidActions: SetAIMidActions( value ); return true;
        case Actor_SetAIFarActions: SetAIFarActions( value ); return true;
        case Actor_SetIgnoreAgressor: SetIgnoreAgressor( String::ToBool(value) ); return true;
        case Actor_SetDamageAttack1: SetDamageAttack1( value ); return true;
        case Actor_SetDamageAttack2: SetDamageAttack2( value ); return true;
        case Actor_SetAttack1Cooldown: SetAttack1Cooldown( String::ToUInt( value ) ); return true;
        case Actor_SetAttack2Cooldown: SetAttack2Cooldown( String::ToUInt( value ) ); return true;
        case Actor_SetMaxRoomPatrol: SetMaxRoomPatrol( String::ToUInt( value ) ); return true;
        case Actor_SetPatrol: SetPatrol( value ); return true;
        case Actor_SetPrevEnemyCheck: SetPrevEnemyCheck( String::ToUInt( value ) ); return true;
        case Actor_SetPrevFoodCheck: SetPrevFoodCheck( String::ToUInt( value ) ); return true;
        case Actor_SetPrevEscapeCheck: SetPrevEscapeCheck( String::ToUInt( value ) ); return true;
        case Actor_SetPrevFindCheck: SetPrevFindCheck( String::ToUInt( value ) ); return true;
        case Actor_SetWanderMinDur: SetWanderMinDur( String::ToUInt( value ) ); return true;
        case Actor_SetWanderMaxDur: SetWanderMaxDur( String::ToUInt( value ) ); return true;
        case Actor_SetWanderDur: SetWanderDur( String::ToUInt( value ) ); return true;
        case Actor_SetSpawnImmunityDuration: SetSpawnImmunityDuration( String::ToUInt( value ) ); return true;
        case Actor_SetAIWanderDelayPeriod: SetAIWanderDelayPeriod( String::ToUInt( value ) ); return true;
        case Actor_SetAIWanderActivePeriod: SetAIWanderActivePeriod( String::ToUInt( value ) ); return true;
        case Actor_SetAIWanderDelayedTime: SetAIWanderDelayedTime( String::ToUInt( value ) ); return true;
        case Actor_SetAIWanderActiveTime: SetAIWanderActiveTime( String::ToUInt( value ) ); return true;
        case Actor_SetPrevWander: SetPrevWander( String::ToUInt( value ) ); return true;
        case Actor_SetFoodCheckDelay: SetFoodCheckDelay( String::ToUInt( value ) ); return true;
        case Actor_SetLastTimeSleepAdded: SetLastTimeSleepAdded( String::ToUInt( value ) ); return true;
        case Actor_SetSleepLevel: SetSleepLevel( String::ToInt( value ) ); return true;
        case Actor_SetRunning: SetRunning( String::ToBool( value ) ); return true;
        case Actor_SetDamageDeath: SetDamageDeath( value ); return true;
        case Actor_SetDeathExplodeEffect: SetDeathExplodeEffect( value ); return true;
        case Actor_SetDeathExplodeRadius: SetDeathExplodeRadius( String::ToFloat( value ) ); return true;
        case Actor_SetThinkDelay: SetThinkDelay( String::ToUInt( value ) ); return true;
        case Actor_SetPrevThink: SetPrevThink( String::ToUInt( value ) ); return true;
        case Actor_SetWayPointDuration: SetWayPointDuration( String::ToUInt( value ) ); return true;
        case Actor_SetPrevWaypointCheck: SetPrevWaypointCheck( String::ToUInt( value ) ); return true;
        case Actor_SetCurWaypointIndex: SetCurWaypointIndex( String::ToUInt( value ) ); return true;
        case Actor_SetMaxPolyChase: SetMaxPolyChase( String::ToUInt( value ) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Actor_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Actor_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
            return Entity::Set( setter, value );
    } 
}

std::string Actor::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<ActorSetterT>(setter) ) {
        case Actor_SetAIAvoidPlayer: return String::ToString( GetAIAvoidPlayer() );
        case Actor_SetAINearMax: return String::ToString( GetAINearMax() );
        case Actor_SetAIMidMax: return String::ToString( GetAIMidMax() );
        case Actor_SetAIFarMax: return String::ToString( GetAIFarMax() );
        case Actor_SetAIFleeTriggerRange: return String::ToString( GetAIFleeTriggerRange() );
        case Actor_SetAISenseFoodRange: return String::ToString( GetAISenseFoodRange() );
        case Actor_SetAINearActions: return String::FromVector( GetAINearActions() );
        case Actor_SetAIMidActions: return String::FromVector( GetAIMidActions() );
        case Actor_SetAIFarActions: return String::FromVector( GetAIFarActions() );
        case Actor_SetIgnoreAgressor: return String::ToString( GetIgnoreAgressor() );
        case Actor_SetDamageAttack1: return GetDamageAttack1();
        case Actor_SetDamageAttack2: return GetDamageAttack2();
        case Actor_SetAttack1Cooldown: return String::ToString( GetAttack1Cooldown() );
        case Actor_SetAttack2Cooldown: return String::ToString( GetAttack2Cooldown() );
        case Actor_SetMaxRoomPatrol: return String::ToString( GetMaxRoomPatrol() );
        case Actor_SetPatrol: return GetPatrol();
        case Actor_SetPrevEnemyCheck: return String::ToString( GetPrevEnemyCheck() );
        case Actor_SetPrevFoodCheck: return String::ToString( GetPrevFoodCheck() );
        case Actor_SetPrevEscapeCheck: return String::ToString( GetPrevEscapeCheck() );
        case Actor_SetPrevFindCheck: return String::ToString( GetPrevFindCheck() );
        case Actor_SetWanderMinDur: return String::ToString( GetWanderMinDur() );
        case Actor_SetWanderMaxDur: return String::ToString( GetWanderMaxDur() );
        case Actor_SetWanderDur: return String::ToString( GetWanderDur() );
        case Actor_SetSpawnImmunityDuration: return String::ToString( GetSpawnImmunityDuration() );
        case Actor_SetAIWanderDelayPeriod: return String::ToString( GetAIWanderDelayPeriod() );
        case Actor_SetAIWanderActivePeriod: return String::ToString( GetAIWanderActivePeriod() );
        case Actor_SetAIWanderDelayedTime: return String::ToString( GetAIWanderDelayedTime() );
        case Actor_SetAIWanderActiveTime: return String::ToString( GetAIWanderActiveTime() );
        case Actor_SetPrevWander: return String::ToString( GetPrevWander() );
        case Actor_SetFoodCheckDelay: return String::ToString( GetFoodCheckDelay() );
        case Actor_SetLastTimeSleepAdded: return String::ToString( GetLastTimeSleepAdded() );
        case Actor_SetSleepLevel: return String::ToString( GetSleepLevel() );
        case Actor_SetRunning: return String::ToString( GetRunning() );
        case Actor_SetDamageDeath: return GetDamageDeath();
        case Actor_SetDeathExplodeEffect: return String::ToString( GetDeathExplodeEffect() );
        case Actor_SetDeathExplodeRadius: return String::ToString( GetDeathExplodeRadius() );
        case Actor_SetThinkDelay: return String::ToString( GetThinkDelay() );
        case Actor_SetPrevThink: return String::ToString( GetPrevThink() );
        case Actor_SetWayPointDuration: return String::ToString( GetWayPointDuration() );
        case Actor_SetPrevWaypointCheck: return String::ToString( GetPrevWaypointCheck() );
        case Actor_SetCurWaypointIndex: return String::ToString( GetCurWaypointIndex() );
        case Actor_SetMaxPolyChase: return String::ToString( GetMaxPolyChase() );
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Actor_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Actor_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
            return Entity::Get( setter );
    } 
}

NamedSetterT Actor::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( actorSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Actor_SETTERS_START;
        
    static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
    return Entity::GetSetterIndex( key );
}

ViewCone::ViewCone( const Entity& ent )
    : offset( ent.GetSightOffset() )
    , dist( ent.GetSightDistance() )
    , angle( ent.GetSightAngle() )
{    
}

bool ViewCone::IsWithinAngle( Entity& ent, const Vec3f& seer_origin, const Vec3f& seer_facing_dir ) const {
    const Vec3f cone_tip = seer_origin - seer_facing_dir*offset;
    const Vec3f dir_seer_to_ent = (cone_tip - ent.GetOrigin()).GetNormalized();
    float degrees = fabsf( seer_facing_dir.DegreesBetween( dir_seer_to_ent ) );
    return ( degrees <= (angle/2) ); // half angle because we're comparing from center of cone
}

Actor::Actor( void )
    : Entity()
    , inventory()
    , navPath()
    , ai_NearActions()
    , ai_MidActions()
    , ai_FarActions()
    , targeted_entity()
    , view_cone(ACTOR_DEFAULT_VIEW_CONE)
    , ai_actions()
    , destFlowField()
    , pursued_entity()
    , obstacle()
    , movingDir()
    , move()
    , vbo_hud_health(nullptr)
    , vbo_hud_health_uv(nullptr)
    , last_packed_health(0)
    , nav_state(NAV_STATE_NO_PATH)
    , damageAttack1()
    , damageAttack2()
    , damageDeath()
    , fx_deathExplode()
    , patrol()
    , ai_NearMax(0.5f)
    , ai_MidMax(2.0f)
    , ai_FarMax(20.0f)
    , aiFleeTriggerRange(3.0f)
    , aiSenseFoodRange(5.0f)
    , deathExplodeRadius()
    , prevThink()
    , thinkDelay()
    , attack1Cooldown(0)
    , attack2Cooldown(0)
    , maxRoomPatrol(ACTOR_DEFAULT_MAX_ROOM_PATROL)
    , prevEnemyCheck()
    , prevFoodCheck()
    , prevEscapeCheck()
    , prevFindCheck()
    , wanderMinDur(ACTOR_DEFAULT_WANDER_MIN)
    , wanderMaxDur(ACTOR_DEFAULT_WANDER_MAX)
    , wanderDur()
    , spawnImmunityDuration()
    , aiWanderDelayPeriod()
    , aiWanderActivePeriod()
    , aiWanderDelayedTime()
    , aiWanderActiveTime()
    , prevWander()
    , foodCheckDelay()
    , lastTimeSleepAdded()
    , waypoint_duration()
    , prev_waypoint_check()
    , cur_waypoint_index()
    , maxPolyChase(AI_MAX_CHASE_NAV_POLIES)
    , sleepLevel()
    , ai_AvoidPlayer(false)
    , ignoreAgressor(false)
    , running(false)
{ }

Actor::Actor( const Actor& other )
    : Entity( other )
    , inventory( other.inventory)
    , navPath( other.navPath)
    , ai_NearActions( other.ai_NearActions)
    , ai_MidActions( other.ai_MidActions)
    , ai_FarActions( other.ai_FarActions)
    , targeted_entity( other.targeted_entity )
    , view_cone( other.view_cone )
    , ai_actions()
    , destFlowField()
    , pursued_entity(other.pursued_entity)
    , obstacle(other.obstacle)
    , movingDir(other.move)
    , move(other.move)
    , vbo_hud_health(nullptr)
    , vbo_hud_health_uv(nullptr)
    , last_packed_health(0)
    , nav_state(other.nav_state)
    , damageAttack1(other.damageAttack1)
    , damageAttack2(other.damageAttack2)
    , damageDeath(other.damageDeath)
    , fx_deathExplode(other.fx_deathExplode)
    , patrol(other.patrol)
    , ai_NearMax(other.ai_NearMax)
    , ai_MidMax(other.ai_MidMax)
    , ai_FarMax(other.ai_FarMax)
    , aiFleeTriggerRange(other.aiFleeTriggerRange)
    , aiSenseFoodRange(other.aiSenseFoodRange)
    , deathExplodeRadius(other.deathExplodeRadius)
    , prevThink(other.prevThink)
    , thinkDelay(other.deathExplodeRadius)
    , attack1Cooldown(other.attack1Cooldown)
    , attack2Cooldown(other.attack2Cooldown)
    , maxRoomPatrol(other.maxRoomPatrol)
    , prevEnemyCheck(other.prevEnemyCheck)
    , prevFoodCheck(other.prevFoodCheck)
    , prevEscapeCheck(other.prevEscapeCheck)
    , prevFindCheck(other.prevFindCheck)
    , wanderMinDur(other.wanderMinDur)
    , wanderMaxDur(other.wanderMaxDur)
    , wanderDur(other.wanderDur)
    , spawnImmunityDuration(other.spawnImmunityDuration)
    , aiWanderDelayPeriod(other.aiWanderDelayPeriod)
    , aiWanderActivePeriod(other.aiWanderActivePeriod)
    , aiWanderDelayedTime(other.aiWanderDelayedTime)
    , aiWanderActiveTime(other.aiWanderActiveTime)
    , prevWander(other.prevWander)
    , foodCheckDelay(other.foodCheckDelay)
    , lastTimeSleepAdded(other.lastTimeSleepAdded)
    , waypoint_duration(other.waypoint_duration)
    , prev_waypoint_check(other.prev_waypoint_check)
    , cur_waypoint_index(other.cur_waypoint_index)
    , maxPolyChase(other.maxPolyChase)
    , sleepLevel(other.sleepLevel)
    , ai_AvoidPlayer(other.ai_AvoidPlayer)
    , ignoreAgressor(other.ignoreAgressor)
    , running(other.running)
{
    for ( AI_ZoneT_BaseType zone=0; zone<static_cast<AI_ZoneT_BaseType>( AI_ZoneT::NUM_ZONES ); ++zone ) {
        ai_actions[zone] = other.ai_actions[zone];
    }
}

Actor& Actor::operator=( const Actor& other ) {
    Entity::operator=( other );
    inventory = other.inventory;
    navPath = other.navPath;
    ai_NearActions = other.ai_NearActions;
    ai_MidActions = other.ai_MidActions;
    ai_FarActions = other.ai_FarActions;
    targeted_entity = other.targeted_entity;
    view_cone = other.view_cone;
    pursued_entity = other.pursued_entity;
    obstacle = other.obstacle;
    movingDir = other.movingDir;
    move = other.move;
    DELNULL( vbo_hud_health );
    DELNULL( vbo_hud_health_uv );
    last_packed_health=0;

    nav_state = other.nav_state;
    damageAttack1 = other.damageAttack1;
    damageAttack2 = other.damageAttack2;
    damageDeath = other.damageDeath;
    fx_deathExplode = other.fx_deathExplode;
    patrol = other.patrol;
    ai_NearMax = other.ai_NearMax;
    ai_MidMax = other.ai_MidMax;
    ai_FarMax = other.ai_FarMax;
    aiFleeTriggerRange = other.aiFleeTriggerRange;
    aiSenseFoodRange = other.aiSenseFoodRange;
    deathExplodeRadius = other.deathExplodeRadius;
    prevThink = other.prevThink;
    thinkDelay = other.thinkDelay;
    attack1Cooldown = other.attack1Cooldown;
    attack2Cooldown = other.attack2Cooldown;
    maxRoomPatrol = other.maxRoomPatrol;
    prevEnemyCheck = other.prevEnemyCheck;
    prevFoodCheck = other.prevFoodCheck;
    prevEscapeCheck = other.prevEscapeCheck;
    prevFindCheck = other.prevFindCheck;
    wanderMinDur = other.wanderMinDur;
    wanderMaxDur = other.wanderMaxDur;
    wanderDur = other.wanderDur;
    spawnImmunityDuration = other.spawnImmunityDuration;
    aiWanderDelayPeriod = other.aiWanderDelayPeriod;
    aiWanderActivePeriod = other.aiWanderActivePeriod;
    aiWanderDelayedTime = other.aiWanderDelayedTime;
    aiWanderActiveTime = other.aiWanderActiveTime;
    prevWander = other.prevWander;
    foodCheckDelay = other.foodCheckDelay;
    lastTimeSleepAdded = other.lastTimeSleepAdded;
    waypoint_duration = other.waypoint_duration;
    prev_waypoint_check = other.prev_waypoint_check;
    cur_waypoint_index = other.cur_waypoint_index;
    maxPolyChase = other.maxPolyChase;
    sleepLevel = other.sleepLevel;
    ai_AvoidPlayer = other.ai_AvoidPlayer;
    ignoreAgressor = other.ignoreAgressor;
    running = other.running;

    for ( AI_ZoneT_BaseType zone=0; zone<static_cast<AI_ZoneT_BaseType>( AI_ZoneT::NUM_ZONES ); ++zone ) {
        ai_actions[zone] = other.ai_actions[zone];
    }
    
    return *this;
}

Actor::~Actor( void ) {
    delete vbo_hud_health;
    delete vbo_hud_health_uv;
}

void Actor::Spawn( void ) {
    Entity::Spawn();
    AI_Load();
    WakeUp();
}

bool Actor::MoveToward( const Vec3f& origin_to ) {
    Vec3f move_dir( origin_to );
    move_dir -= GetOrigin();
    return SetMoveVec( move_dir );
}

void Actor::TurnToWorldMousePos( void ) {
    TurnToPos( Input::GetWorldMousePos() );
}

void Actor::TurnToPos( const Vec3f& pos ) {
    const auto dir( Vec3f::GetNormalized( pos - GetOrigin() ) );
    TurnToFaceDir( dir );
}

bool Actor::SetMoveVec( const Vec3f& move_dir ) {
    movingDir = move_dir;
    const bool creep = globalVals.GetBool(gval_g_creep);
    if ( !creep || move_dir.SquaredLen() > 1 ) {
        movingDir.Normalize();
    }
    
    if ( creep ) {
        SetAnimSpeed( move.Len() );
    }
    
    if ( CanTurn() ) {
        move = movingDir;
    } else {
        move = GetFacingDir();
    }
    
    if ( !CanMove() )
        return false;

    return true;
}

void Actor::DoVelocity( void ) {
    if ( GetNumVoxelsOccupied() < 1 )
        return;
   
    const float topSpeed = GetTopSpeed();
    const float myMoveSpeed = GetMoveSpeed();
    
    if constexpr ( moveSpeedDebug )
        Printf("F|TS|MS : %u|%f.3|%f.3\n", game->GetGameFrame(), topSpeed, myMoveSpeed );
        
    const Vec3f vel( move.x * myMoveSpeed, move.y * myMoveSpeed, 0 );
    const float minSpeed_sq = EP_ONE_CENTIMETER * EP_ONE_CENTIMETER;
    move.Zero(); // movement accounted for, zero it
    
    Vec3f velocity = bounds.GetVelocity();
    
    if ( CanMove() && vel.SquaredLen() > minSpeed_sq ) {
        const float prev_z = velocity.z;
        const float prevSpeed_sq = velocity.SquaredLen();
        
        velocity = vel;

        if ( running ) {
            modelInfo.AddAnimState( AnimStateT::RunForward );
            velocity *= GetRunSpeedMultiplier();
        } else {
            modelInfo.AddAnimState( AnimStateT::WalkForward );
        }
        
        // we don't want the player to add speed to a direction if he's moving faster than maximum speed in that dir,
        // but we want to keep movement in any other direction
        const float topSpeed_sq = topSpeed * topSpeed;
        const float newSpeed_sq = velocity.SquaredLen();
        if ( newSpeed_sq > topSpeed_sq ) {
            if ( newSpeed_sq > prevSpeed_sq ) {
                velocity.Normalize();
                velocity *= topSpeed;
            }
        }

        velocity.z = prev_z;
    } else {
        modelInfo.RemAnimState( AnimStateT::RunForward);
        modelInfo.RemAnimState( AnimStateT::WalkForward);
    }
    
    bounds.SetVelocity( velocity );

    std::vector< CollisionItem > all_collisions;
    bounds.DoVelocity_Slide( DEFAULT_SLIDE_NUM, &all_collisions );

    for ( auto & item : all_collisions ) {
        if ( auto ent = item.ent.lock() ) {
            if ( !derives_from< Flat >( *ent ) ) {
                obstacle = ent;
                break;
            }
        }
    }
}

void Actor::DoSpecial1( void ) {
}

void Actor::DoSpecial2 ( void ) {
}

void Actor::Interact ( void ) {
}

bool Actor::IsReadyToAttack1( void ) const {
    if ( game->GetGameTime() - GetPrevAttack1() < attack1Cooldown )
        return false;
        
    if ( !HasAnimatedModel() )
        return false;
        
    if ( modelInfo.HasAnimState( AnimStateT::Attack1 ) )
        return false;

    if ( modelInfo.HasAnimState( AnimStateT::Attack2 ) )
        return false;
        
    return true;
}

bool Actor::IsReadyToAttack2( void ) const {
    if ( !HasAnimatedModel() )
        return false;

    if ( game->GetGameTime() - GetPrevAttack1() < attack2Cooldown )
        return false;

    if ( modelInfo.HasAnimState( AnimStateT::Attack2 ) )
        return false;
        
    return true;
}

void Actor::Attack2( void ) {
    SetPrevAttack2( game->GetGameTime() );
}

void Actor::Attack1( void ) {
    const std::shared_ptr< const DamageInfo > damage = damageManager.Get( damageAttack1.c_str() );
    if ( !damage ) {
        return;
    }
    
    SetPrevAttack1( game->GetGameTime() );
    
    const float ent_radius = bounds.IsSphere() ? bounds.GetRadius() : bounds.GetAABox3D_Local().GetSize().z;
    const float attack_space = 0.5f;
    const float cyl_radius = ent_radius + attack_space;
    const float cyl_height = ent_radius;
    const Primitive_Cylinder prim( Cylinder( GetOrigin(), Vec3f(0,0,1), cyl_radius, cyl_height ) );
    
    const float max_targets = 5;
    
    DamageZone( prim, max_targets, *damage, GetWeakPtr(), DamageFalloffT::Linear );
}

void Actor::Think( void ) {
    if ( !( IsSpawned()) )
        return;

    if ( GetChargeForward() ) {
        MoveToward( GetOrigin() + GetFacingDir() );
        Entity::Think();
        return;
    }
    
    DoSpawnProtection();
    
    if ( !game->IsPlayer( *this ) ) {
        if ( !globalVals.GetBool( gval_ai_disable ) ) {
            DoAI();
        }
        Entity::Think();
        return;
    }
    
    move.z = 0;
    if ( !Maths::Approxf( move, Vec3f(0,0,0) ) ) {
        // since we check and apply each axis individually
        float speed = move.Len();
        
        if ( speed > ACTOR_MAX_SPEED_FOR_CREEP ) {
            move.Normalize();
            speed = 1.0f;
        }
    
        SetAnimSpeed( speed );

        PlayerInput::ExecuteMoveVector( *this, move );
    }

    Entity::Think();
}

void Actor::DoSpawnProtection( void ) {
    if ( IsDestructible() )
        return;
    
    if ( spawnImmunityDuration <= 0 )
        return;
        
    if ( game->GetGameTime() - GetSpawnTime() > spawnImmunityDuration )
        SetDestructible( true );
}

bool Actor::ExecuteInputAction( const std::string& action, const InputTypeT type ) {
    if ( type == InputTypeT::Hold ) {
        if ( action == "left" ) {
            move.x -= 1;
            return true;
        }

        if ( action == "right" ) {
            move.x += 1;
            return true;
        }

        if ( action == "forward" ) {
            move.y += 1;
            return true;
        }

        if ( action == "backward" ) {
            move.y -= 1;
            return true;
        }
            
        if ( action == "use" ) {
            PressUse();
            return true;
        }

        if ( action == "attack1" ) {
            if ( IsReadyToAttack1() ) {
                modelInfo.AddAnimState( AnimStateT::Attack1 );
                return true;
            }
        }

        if ( action == "attack2" ) {
            if ( IsReadyToAttack2() ) {
                modelInfo.AddAnimState( AnimStateT::Attack2 );
                return true;
            }
        }

        return false;
    }
    
    if ( action == "use" ) {
        if ( type == InputTypeT::Press ) {
            PressUse();
        }
        return true;
    }
    
    if ( action == "run" ) {
        if ( type == InputTypeT::Press ) {
            running = !globalVals.GetBool( gval_g_autorun );
        } else {
            running = globalVals.GetBool( gval_g_autorun );
        }
        return true;
    }
    
    return false;
}

bool Actor::ExecuteActionJoyAxis_State( const std::string& action, const float axis_val ) {
    if ( action == "movex" ) {
        move.x = axis_val;

        if ( move.x > 1 )
            move.x = 1;

        return true;
    }

    if ( action == "movey" ) {
        move.y = axis_val;

        if ( move.y > 1 )
            move.y = 1;
        return true;
    }
    
    return false;
}

bool Actor::ApplyDamage( const DamageInfo& damageInfo, const std::weak_ptr< Entity >& inflictor, const float percent ) {
    if ( auto player = game->GetPlayer() ) {
        if ( player->GetIndex() == GetIndex() ) {
            if ( globalVals.GetBool("god") ) {
                return false;
            }
        }
    }
    
    if (!Entity::ApplyDamage( damageInfo, inflictor, percent ) )
        return false;

    if ( auto ent = inflictor.lock() )
        AI_HurtBy( *ent );

    return true;
}

bool Actor::Death( void ) {
    if ( !IsDestructible() )
        return false;
    
    SetNoMove( true );
    SetNoTurn( true );
    
    auto animInfo = modelInfo.GetAnimInfo();
    if ( animInfo && animInfo->GetAnim_NoErr( "death", AnimChannelT::Legs ) ) {
        modelInfo.AddAnimState( AnimStateT::Die );
            
        if ( !GetOkayToDie() )
            return false;
    }

    return Kill();
}

bool Actor::Kill( void ) {
    if ( !Entity::Kill() )
        return false;
    
    DropBodyParts();
    DeathExplode();

    return true;
}

void Actor::DeathExplode( void ) {
    if ( damageDeath.empty() )
        return;

    const Vec3f explodeOrigin( GetOrigin() );

    const auto deathExplosion = damageManager.Get( damageDeath.c_str() );
    if ( deathExplosion ) {
        const float defaultExplosionRadius = bounds.IsSphere() ? bounds.GetRadius() : 2;
        const float explodeRadius = Maths::Approxf( deathExplodeRadius, 0 ) ? defaultExplosionRadius : deathExplodeRadius;
        DamageZone( Primitive_Sphere( Sphere( explodeOrigin, explodeRadius ) ), 0, *deathExplosion, GetWeakPtr(), DamageFalloffT::Linear );
    }

    if ( fx_deathExplode.empty() )  {
        ERR("Effect entVal fx_deathExplode was not set on entity %s\n", GetIdentifier().c_str() );
        return;
    }

    const auto fx_explode = effectsManager.Get( fx_deathExplode.c_str() );
    if ( fx_explode ) {
        effectsManager.Spawn( *fx_explode, explodeOrigin );
    }
}

void Actor::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Entity::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    saveFile.parser.WriteVec3f( move );
    inventory.MapSave( saveFile, string_pool );

    MapSaveSetters( saveFile, string_pool, actorSetters );
    
    // TOGAMESAVE: next_nav_poly intentionally not saved, it is regenerated automatically
}

void Actor::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Entity::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );

    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    saveFile.parser.ReadVec3f( move );
    inventory.MapLoad( saveFile, string_pool );
    
    
    MapLoadSetters( saveFile, string_pool, actorSetters );
}

#include "../commandsys.h"

void Actor::DrawDebug( void ) const {
}


void Actor::DrawHUD( void ) {
    DrawHUD_Inventory();
    if ( globalVals.GetBool( gval_d_playerStatusBar ) )
        DrawHUD_StatusBar();
}

void Actor::PackHUD_StatusBar_Health( void ) {

    if ( vbo_hud_health ) {
        if ( last_packed_health == health ) {
            return;
        } else {
            ASSERT( vbo_hud_health_uv );
            vbo_hud_health->Clear();
            vbo_hud_health_uv->Clear();
        }
    } else {
        ASSERT( ! vbo_hud_health_uv );
        vbo_hud_health = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
        vbo_hud_health_uv = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
    }

    FontInfo fi;
    fi.size_ui_px = HUD_HEALTH_FONT_SIZE;
    fi.pos.Set(0,600-HUD_HEALTH_FONT_SIZE);
    vbo_hud_health->PackText( *vbo_hud_health_uv, std::to_string(health).c_str(), fi, &fi.pos );
    vbo_hud_health->MoveToVideoCard();
    vbo_hud_health_uv->MoveToVideoCard();
    last_packed_health = health;
}

void Actor::DrawHUD_StatusBar( void ) {
    DrawHUD_StatusBar_Health();
}

void Actor::DrawHUD_StatusBar_Health( void ) {
    PackHUD_StatusBar_Health();

    ASSERT( vbo_hud_health );
    ASSERT( vbo_hud_health_uv );
    ASSERT( vbo_hud_health->Finalized() );
    ASSERT( vbo_hud_health_uv->Finalized() );

    glDisable( GL_DEPTH_TEST );
        const char* fontname = "courier";
        std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
        Font::AssertValid( fnt, fontname );

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SendData_Matrices();
        shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
        shaders->SetUniform1f("fWeight", 1.0f );
        shaders->SetUniform4f("vColor", HUD_HEALTH_FONT_COLOR );
        shaders->SetAttrib( "vUV", *vbo_hud_health_uv );
        shaders->SetAttrib( "vPos", *vbo_hud_health );
        shaders->DrawArrays( GL_QUADS, 0, vbo_hud_health->Num() );
    glEnable( GL_DEPTH_TEST );
}

void Actor::DrawHUD_Inventory( void ) {
    // ** draw the model of each inventory item across the top right of the screen (going left)

    Vec3f item_position( 750,0,10 );
    const float rot = game->GetRenderFrame();
    Link< Item > *link = inventory.GetFirst();
    while ( link != nullptr ) {
        renderer->PushMatrixMV();
            renderer->ModelView().Translate( item_position );
            renderer->ModelView().Rotate( rot, 0,0,1 );
            renderer->ModelView().Scale(50, 50, 50);
            link->Data().Draw();
        renderer->PopMatrixMV();

        item_position.x -= 50;

        link = link->GetNext();
    }
}

void Actor::DrawVBO( const float opacity ) const {
    DrawDebug();

    Entity::DrawVBO( opacity );

    const bool draw_view_cones = globalVals.GetBool( gval_d_viewCones );
    if ( draw_view_cones ) {
        const ViewCone sight( *this );
            
        const Vec3f facing_dir = GetFacingDir().GetNormalized(); // making sure we're perfectly normalized for the quaternian inversion below
        const Vec3f bounds_origin = GetOrigin();
        const Vec3f cone_tip = bounds_origin - facing_dir*sight.offset;
        
        // half radius, since we're rotating out from center
        const Quat rot( Quat::FromAxis( (sight.angle/2) * DEG_TO_RAD_F, Vec3f(0,0,1) ) );
        const Vec3f line1_dir = rot * facing_dir;
        
        const Quat rot2( rot.GetInverse() );
        const Vec3f line2_dir = rot2 * facing_dir;
        
        renderer->DrawLine( cone_tip, cone_tip + line1_dir * sight.dist, Color4f(1,0,0), Color4f(1,0,0) );
        renderer->DrawLine( cone_tip, cone_tip + line2_dir * sight.dist, Color4f(1,0,0), Color4f(1,0,0) );
        renderer->DrawPoint( cone_tip, Color4f(1,0,0), 5 );
        renderer->DrawLine( bounds_origin, bounds_origin + facing_dir * sight.dist, Color4f(1,0,0), Color4f(1,0,0) );
    }

    navPath.DrawPathDebug( this );
}

void Actor::DoMigration( void ) {
    Entity::DoMigration();

    if ( game->IsPaused() )
        return;

    // only highlight things if we are the entity the player is currently using
    std::shared_ptr< Actor > player = game->GetPlayer();
    if ( ! player || this != player.get() )
        return;

    FindTargetedEntity();
}

void Actor::FindTargetedEntity( void ) {
    // note: "use-activated triggers" aren't activated in this way, they are activated elsewhere
    
    bool unmarkPreviousEntity = true;
    std::weak_ptr< Entity > previous_targeted_ent = targeted_entity;

    std::vector< CollisionItem > entList;

    const Primitive_Sphere primitive( GetSphereForReachableArea() );
    IntersectData coldata;
    coldata.opts |= MATCH_TEST_ONLY_BOUNDS;
    coldata.opts |= MATCH_VISIBLE_ONLY;
    coldata.opts |= MATCH_ENTS_WITH_MODELS_ONLY;
    
    game->entTree->GetEntityTraceCollisions( coldata, primitive, &entList );

    // cast this player down to an Entity to compare Entity pointers to us (since the addresses could feasibly be different depending on what it's casted to)
    
    const auto usableColor = Color4f( 0, 1, 1, 0.5f );
    const auto throwableColor = Color4f( 1, 0, 0, 0.5f );

    for ( auto & item : entList ) {
        auto ent = item.ent.lock();
        if ( ! ent )
            continue;

        if ( ent.get() == this ) 
            continue;
            
        if( !ent->IsVisible() )
            continue;
            
        if( !ent->HasModel() )
            continue;
            
        if( ent == previous_targeted_ent.lock() ) {
            unmarkPreviousEntity = false;
            break;
        }
            
        if ( usable ) {
            ent->SetMarkedForUse(true);
            ent->Colorize( usableColor );
            targeted_entity = ent;
            break;
        }
        
        if ( throwable ) {
            ent->Colorize( throwableColor );
            targeted_entity = ent;
            break;
        }
    }

    if ( unmarkPreviousEntity && previous_targeted_ent.lock() )
        UnmarkUsableEntity( previous_targeted_ent );
        
    return;
}

Sphere Actor::GetSphereForReachableArea( void ) const {
    ASSERT( bounds.IsSphere() );
    Sphere sphere( GetOrigin(), bounds.GetRadius() );
    sphere.origin += GetFacingDir() * (sphere.radius/2); // move the sphere forward a little
    sphere.radius *= 1.1f; // make the sphere a smidge bigger
    return sphere;
}

bool Actor::SetMemberVariable( const std::string& entry, const std::string& val ) {
    return Entity::SetMemberVariable( entry, val );
}

int Actor::ActivateManualTriggers( void ) {
    int ret = 0;
    
    IntersectData coldata;
    coldata.desired_type = TypeID_Trigger;
    
    std::vector< CollisionItem > trigs;
    
    const auto primitive = Primitive_Sphere( Sphere(Vec3f(0,0,0),1) ); //todo Primitive_Point
    
    GlobalTrace( coldata, primitive, &trigs );
    for ( auto & item : trigs )
        if ( auto trig = item.ent.lock() )
            if ( trig->IsUsable() )
                if ( std::static_pointer_cast< Trigger >( trig )->TryToActivate( this ) )
                    ++ret;

    return ret;
}

void Actor::PressUse( void ) {
    auto ent = targeted_entity.lock();
    if ( ent ) {
        
        //ent->Uncolorize();
        //targeted_entity.reset();

        if ( ent->IsUsable() ) {
            ent->UseBy( this );
            return;
        }
    }
    
    ActivateManualTriggers();
}

void Actor::Stun( const Uint32 miliseconds ) {
    prevThink = game->GetGameTime();
    thinkDelay = miliseconds;
    pursued_entity.reset();
}

bool Actor::IsUsable( void ) const {
    return Entity::IsUsable();
}

bool Actor::IsStunned( void ) const {
    if ( thinkDelay <= 0 )
        return false;

    if ( game->GetGameTime() - prevThink < thinkDelay )
        return true;

    return false;
}

bool Actor::IsAsleep( void ) const {
    return sleepLevel >= SLEEP_THRESHOLD;
}

void Actor::UnmarkUsableEntity( std::weak_ptr<Entity> ent_wkptr ) {
    if ( auto ent = ent_wkptr.lock() ) {
        ent->SetMarkedForUse(false);
        ent->Uncolorize();
        targeted_entity.reset();
    }
}

void Actor::GetEntitiesInSight( std::vector< CollisionItem >& collisions_out, const TypeID desired_type ) {
    ASSERT( bounds.IsSphere() );
    
    const Vec3f bounds_origin = GetOrigin();
    const Vec3f facing_dir = GetFacingDir();
    const ViewCone sight( *this );
    const Primitive_Sphere primitive( Sphere(bounds_origin, sight.dist) );
    
    IntersectData coldata;
    coldata.opts |= MATCH_TEST_ONLY_BOUNDS;
    coldata.opts |= MATCH_VISIBLE_ONLY;
    coldata.opts |= MATCH_ENTS_WITH_MODELS_ONLY;
    coldata.desired_type = desired_type;

    GlobalTrace( coldata, primitive, &collisions_out );
    
    // prune out stuff outside of our sight
    for ( uint i=0; i<collisions_out.size(); ++i ) {
        auto ent = collisions_out[i].ent.lock();
        if ( ent ) {
            if ( ent->GetIndex() == GetIndex() || sight.IsWithinAngle( *ent, bounds_origin, facing_dir ) ) {
                PopSwap( collisions_out, i-- );
                continue;
            }
        }
    }
}

Vec3f Actor::GetAttackDir( void ) const {
    if ( auto sptr = pursued_entity.lock() )
        return Vec3f::GetNormalized( sptr->GetOrigin() - GetOrigin() );

    return GetFacingDir();
}

std::shared_ptr< Entity > Actor::GetClosestEntityInList( const std::vector< CollisionItem >& list ) const {
    std::shared_ptr< Entity > ret = nullptr;
    float retDist;
    
    for ( auto& item : list ) {
        auto ent = item.ent.lock();
        if ( !ent )
            continue;

        const float dist = ( ent->GetOrigin() - GetOrigin() ).SquaredLen();

        if ( !ret || ( dist < retDist ) ) {
            ret = ent;
            retDist = dist;
        }
    }
    
    return ret;
}

std::shared_ptr< Entity > Actor::GetClosestEntityInVector( std::vector< std::weak_ptr< Entity > > vector ) const {
    std::shared_ptr< Entity > ret = nullptr;
    float retDist;
    
    for ( auto& vectorEntity : vector ) {
        auto ent = vectorEntity.lock();
        if ( !ent )
            continue;

        const float dist = ( ent->GetOrigin() - GetOrigin() ).SquaredLen();

        if ( !ret || ( dist < retDist ) ) {
            ret = ent;
            retDist = dist;
        }
    }
    
    return ret;
}
   
void Actor::AddSleepiness( const int val ) {
    sleepLevel += val;
    SetLastTimeSleepAdded( game->GetGameTime() );
    if ( sleepLevel >= SLEEP_THRESHOLD )
        PutToSleep(); 
}

void Actor::SetSleepLevel( const int to ) {
    sleepLevel = to;
}

int Actor::GetSleepLevel( void ) const {
    return sleepLevel;
}

float Actor::GetSleepLevel_Normalized( void ) const {
    return sleepLevel / SLEEP_THRESHOLD;
}

std::vector< std::string > Actor::GetAINearActions( void ) const {
    return ai_NearActions;
}

void Actor::SetAINearActions( const std::vector< std::string >& nearActions ) {
    ai_NearActions = nearActions;
}

void Actor::SetAINearActions( const std::string& nearActions_csv ) {
    ai_NearActions =String::ParseStringList( nearActions_csv, "," );
}

std::vector< std::string > Actor::GetAIMidActions( void ) const {
    return ai_MidActions;
}

void Actor::SetAIMidActions( const std::vector< std::string >& midActions ) {
    ai_MidActions = midActions;
}

void Actor::SetAIMidActions( const std::string& midActions_csv ) {
    ai_MidActions =String::ParseStringList( midActions_csv, "," );
}

std::vector< std::string > Actor::GetAIFarActions( void ) const {
    return ai_FarActions;
}

void Actor::SetAIFarActions( const std::string& farActions_csv ) {
    ai_FarActions =String::ParseStringList( farActions_csv, "," );
}

void Actor::SetAIFarActions( const std::vector< std::string >& farActions ) {
    ai_FarActions = farActions;
}

