// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./forager.h"
#include "../../rendering/animation/animInfo.h"
#include "../../game.h"

const float ESCAPE_RANGE = 1.0f;
const Uint32 DEFAULT_FLEE_DURATION_MS = 2000;
const Uint32 DEFAULT_STOP_FLEE_RANGE = 5;
const Uint32 DEFAULT_STOP_FLEE_RANGE_SQ = DEFAULT_STOP_FLEE_RANGE * DEFAULT_STOP_FLEE_RANGE;
const Uint32 MINIMUM_WANDER_PERIOD_OFFSET = 100;

std::vector< std::string > Forager::foragerSetters;

void Forager::InitSetterNames( void ) {
    if ( foragerSetters.size() == Forager_NUM_SETTERS )
        return; // already set
    foragerSetters.resize(Forager_NUM_SETTERS);
    
    [[maybe_unused]] const uint i = Forager_SETTERS_START;
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Forager,BasicBug>(foragerSetters);
}

bool Forager::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<ForagerSetterT>(setter) ) {
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Forager_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Forager_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Forager>( get_typeid<BasicBug>() ) );
            return BasicBug::Set( setter, value );
    } 
}
std::string Forager::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<ForagerSetterT>(setter) ) {
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Forager_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Forager_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Forager>( get_typeid<BasicBug>() ) );
            return BasicBug::Get( setter );
    } 
}

NamedSetterT Forager::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( foragerSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Forager_SETTERS_START;
        
    static_assert( is_parent_of<Forager>( get_typeid<BasicBug>() ) );
    return BasicBug::GetSetterIndex( key );
}

Forager::Forager( void )
    : BasicBug()
    , homeNest()
    {
}

Forager::Forager( const Forager& other ) 
    : BasicBug( other )
    , homeNest(other.homeNest)
    {
}

Forager& Forager::operator=( const Forager& other ) {
    BasicBug::operator=( other );
    homeNest = other.homeNest;
    return *this;
}

Forager::~Forager( void ) {
}

void Forager::Spawn( void ) {
    BasicBug::Spawn();
        
    uint _wanderActivePeriod = GetAIWanderActivePeriod();
    const uint randomTimeOffset( Random::UInt( MINIMUM_WANDER_PERIOD_OFFSET, _wanderActivePeriod / 2 ));
    
    SetAIWanderActivePeriod( _wanderActivePeriod - randomTimeOffset );
    SetSpawnTime( game->GetGameTime() );
    SetDestructible( false );
    
}

void Forager::Think( void ) { 
    BasicBug::Think();
}

void Forager::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( !animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        {
          "damage", AnimStateT::Hurt, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "walk", AnimStateT::WalkForward, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 7000 }
            }
        }
        , { "run", AnimStateT::RunForward, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 7000 }
            }
        }
        , { "idle", AnimStateT::Idle, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
        , { "interact", AnimStateT::Interact, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 8000 }
            }
        }
        , { "death", AnimStateT::Die, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

void Forager::DoAI( void ) {
        
    const uint curFrame = game->GetGameFrame();
    if ( curFrame % 2 )
        return;
            
    if ( IsStunned() )
        return;
        
    if ( hasFood ) {
        if ( !AI_FindEscape( TypeID_BugNest ) ) {
            if ( !CheckFleeFromPlayer() )
                AI_Wander();
            return;
        }
    } else {
        //todo:navmeshEntitySearch
    }
    
    auto pursue_target = pursued_entity.lock();

    if ( !pursue_target )  {
        if ( !CheckFleeFromPlayer() && CheckWander() ) {
            AI_Wander();
        }
        return;
    }

    AI_DoState_Actions( *pursue_target );
}

void Forager::Interact( void ) {
    auto ent = pursued_entity.lock();
    
    if ( !ent )
        return;
        
    switch ( ent->GetObjectTypeID() ) {
        case TypeID_BugFood : TryHarvest( ent ); break;
        case TypeID_BugNest : TryEscape(); break;
        default :
            WARN("Interaction not defined : %s -with- %s.\n", GetIdentifier().c_str(), ent->GetIdentifier().c_str() );
    }
    
    return;
}

bool Forager::CheckFleeFromPlayer( void ) {
    auto player_ent = game->GetPlayer();
    Vec3f dir = GetFacingDir();
    if ( !player_ent )
        return false;
    
    const float fleeTriggerRange_sq = powf( 2, GetAIFleeTriggerRange() );
    const float distFromPlayer_sq = Vec3f( player_ent->GetOrigin() - GetOrigin() ).SquaredLen();
    
    if ( activelyFleeing ) {
        bool stopFleeing = false;
        bool fleeTimerExpired = false;
        fleeTimerExpired = game->GetGameTime() - fleeTimeStart > DEFAULT_FLEE_DURATION_MS;
        stopFleeing = distFromPlayer_sq >= DEFAULT_STOP_FLEE_RANGE_SQ || fleeTimerExpired;
        
        if ( stopFleeing ) {
            activelyFleeing = false;
            return false;
        } 
        
        if ( fleeTimerExpired ) {
            fleeTimeStart = game->GetGameTime();
            dir = AI_GetNewWanderDirection();
        }

        AI_Flee( dir );
        return true;
    }
    
    if ( distFromPlayer_sq < fleeTriggerRange_sq ) {
        activelyFleeing = true;
        fleeTimeStart = game->GetGameTime();
        dir = AI_GetNewWanderDirection();
        AI_Flee( dir );
        return true;
    }

    return false;
}

void Forager::TryEscape( void ) {
    
    auto exit = pursued_entity.lock();
    
    if( !exit )
        return;
    
    const float my_radius = bounds.GetRadius();
    const float exit_radius = exit->bounds.GetRadius();
    const Vec3f my_origin = GetOrigin();
    const Vec3f exit_origin = exit->GetOrigin();
    const float dist = ( exit_origin - my_origin ).Len() - my_radius - exit_radius;
    
    //TODO.. despawn bug as it escapes into nest.. then the nest will respawn it later.  For now the bug just turns around
    //to go collect more "food"
        //Temporary
        if ( dist < ESCAPE_RANGE ) {
            hasFood = false;
            pursued_entity.reset();
            modelInfo.RemAnimState( AnimStateT::Interact );
            harvestAttempts = 0;
        }
    //END TODO
    
    //if ( dist < ESCAPE_RANGE )
    //  //TODO : Escape (simply despawn / mark for removal?)
}

void Forager::SetHomeNest( const std::weak_ptr< Entity >& nest ) {
    homeNest = nest;
}

void Forager::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    BasicBug::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Forager>( get_typeid<BasicBug>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, foragerSetters );
}

void Forager::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    BasicBug::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Forager>( get_typeid<BasicBug>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, foragerSetters );
}
