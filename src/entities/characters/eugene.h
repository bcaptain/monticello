// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_BOTS_EUGENE_H
#define SRC_ENTITIES_BOTS_EUGENE_H

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../actor.h"

class Meter;
class AnimSelection;

extern const int SLEEP_THRESHOLD;

class Eugene : public Actor {
public:
    static constexpr TypeInfo<Eugene, Actor> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Eugene( void );
    ~Eugene( void ) override;
    Eugene( const Eugene& other );
    
public:
    Eugene& operator=( const Eugene& other );

public:
    void Spawn( void ) override;
    void Animate( void ) override;
    AnimStateT GetAnimStateForAttachment( const AnimStateT state ) const override;
    AnimStateT GetAnimStateFromName( const std::string& animStateName ) const override;
    std::size_t GetAnimIndex( const AnimStateT state ) const override;
    void Think( void ) override;
    bool ExecuteInputAction( const std::string& action, const InputTypeT type ) override;
    
    void DoSpecial1( void ) override { DoStomp(); };
    void DoSpecial2( void ) override { DoMelee(); };
    
    bool IsReadyToAttack1( void ) const override;
    void SpawnStompEffect( const Vec3f& effectOrigin ) const;
    void PutToSleep( void ) override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    bool IsRecoveringFromMeleeLeftAttack( void ) const;
    bool IsNotAttacking( void ) const;

    void DoStomp( void );
    void DoMelee( void );
    void EquipNextAttachment( void );
    bool IsReadyToSpecial2( void ) const;
    
    void SpawnMeleeEffect( const std::shared_ptr< const Entity> target );
    void SpawnSleepMeterUI( void );
    void UpdateSleepMeterUI( void );
    void UpdateSleepLevel( void );
    
private:
    SETTER_GETTER_BYVAL( StompOffset, Vec3f, stompOffset );
    SETTER_GETTER_BYVAL( MeleeOffset, Vec3f, meleeOffset );
    SETTER_GETTER( StompDamage, std::string, dmg_eugeneStomp );
    SETTER_GETTER( MeleeDamage, std::string, dmg_eugeneMelee );
    SETTER_GETTER( StompEffect, std::string, fx_stomp );
    SETTER_GETTER_BYVAL( StompRadius, float, stompRadius );
    SETTER_GETTER_BYVAL( MeleeRadius, float, meleeRadius );
    SETTER_GETTER_BYVAL( SleepRecoveryPerFrame, float, sleepRecoveryPerFrame );
    SETTER_GETTER_BYVAL( StompMaxTargets, uint, stompMaxTargets );
    SETTER_GETTER_BYVAL( MeleeMaxTargets, uint, meleeMaxTargets );

    void SpawnSprayerFuelMeterUI( void );
    void UpdateSprayerFuelMeterUI( void );
    
private:
    std::shared_ptr< Sprite > melee_effect;
    std::shared_ptr< Meter > sleep_meter;
    std::shared_ptr< Meter > sprayerFuel_meter;
    static const std::vector< AnimSelection > anims; //todoanimb: this needs to not be static if we're to allow entities to adopt behavior/anim/model of another type    

// ** Named Setters
private:
    static const uint Eugene_SETTERS_START = Actor_NUM_SETTERS;
protected:
    enum EugeneSetterT : NamedSetterT_BaseType {
        Eugene_SetStompDamage = Eugene_SETTERS_START
        , Eugene_SetMeleeDamage
        , Eugene_SetStompMaxTargets
        , Eugene_SetStompRadius
        , Eugene_SetMeleeRadius
        , Eugene_SetStompOffset
        , Eugene_SetMeleeMaxTargets
        , Eugene_SetMeleeOffset
        , Eugene_SetSleepRecoveryPerFrame
        , Eugene_SetStompEffect
        , Eugene_SETTERS_END
        , Eugene_NUM_SETTERS = Eugene_SETTERS_END - Eugene_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > eugeneSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Eugene>( get_typeid<Actor>() ) );
    static ToolTip< Eugene, Actor > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_BOTS_EUGENE_H
