// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_MOTH_H
#define SRC_ENTITIES_MOTH_H

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "./basicBug.h"

PrecomputeEntVal( flyHeight )  //todo Jesse:: Maybe unneeded?  Can use modeloffset to simulate flying or just set noGrav and have the bounds actually be mid-air.


class Moth : public BasicBug {
public:
    static constexpr TypeInfo<Moth, BasicBug> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Moth( void );
    ~Moth( void ) override;
    Moth( const Moth& other );

public:
    Moth& operator=( const Moth& other );
    
public:
    void Think( void ) override;
    void DoAI( void ) override;
    void Animate( void ) override;
    void Spawn( void ) override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void Interact() override;

// ** Named Setters
private:
    static const uint Moth_SETTERS_START = Actor_NUM_SETTERS;
protected:
    enum MothSetterT : NamedSetterT_BaseType {
        Moth_SETTERS_END = Moth_SETTERS_START
        , Moth_NUM_SETTERS = Moth_SETTERS_END - Moth_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > mothSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Moth>( get_typeid<BasicBug>() ) );
    static ToolTip< Moth, BasicBug > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_MOTH_H
