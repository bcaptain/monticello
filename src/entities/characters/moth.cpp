// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./moth.h"
#include "../../game.h"
#include "../../rendering/animation/animInfo.h"

std::vector< std::string > Moth::mothSetters;

void Moth::InitSetterNames( void ) {
    if ( mothSetters.size() == Moth_NUM_SETTERS )
        return; // already set
    mothSetters.resize(Moth_NUM_SETTERS);
    
    [[maybe_unused]] const uint i = Moth_SETTERS_START;
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Moth,BasicBug>(mothSetters);
}

bool Moth::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<MothSetterT>(setter) ) {
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Moth_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Moth_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Moth>( get_typeid<BasicBug>() ) );
            return BasicBug::Set( setter, value );
    } 
}
std::string Moth::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<MothSetterT>(setter) ) {
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Moth_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Moth_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Moth>( get_typeid<BasicBug>() ) );
            return BasicBug::Get( setter );
    } 
}

NamedSetterT Moth::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( mothSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Moth_SETTERS_START;
        
    static_assert( is_parent_of<Moth>( get_typeid<BasicBug>() ) );
    return BasicBug::GetSetterIndex( key );
}

Moth::Moth( void )
    : BasicBug()
    {
}

Moth::Moth( const Moth& other ) 
    : BasicBug( other )
    {
}

Moth& Moth::operator=( const Moth& other ) {
    BasicBug::operator=( other );
    return *this;
}

Moth::~Moth( void ) {
}

void Moth::Think( void ) {

    const uint curFrame = game->GetGameFrame();
    if ( curFrame % 2 )
        return;
        
    if ( IsStunned() )
        return;
        
    BasicBug::Think();
}

void Moth::DoAI( void ) {
    if ( IsStunned() ) {
        return;
    }
    
    auto pursue_target = pursued_entity.lock();
    if ( pursue_target ) {
        AI_DoState_Actions( *pursue_target );
        return;
    }
    
    if ( AI_FindFoodInSight() )
            return;

    if ( !pursue_target )  {
        if ( CheckWander() ) {
            AI_Wander();
        }
        return;
    }
}

void Moth::Spawn( void ) {
    BasicBug::Spawn();
    AddDamageVuln( "groundImpact", 1 );
}

void Moth::Animate( void ) {
    
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
        , { "idle", AnimStateT::WalkForward, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 7000 }
            }
        }
        , { "interact", AnimStateT::Interact, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 7000 }
            }
        } 
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

void Moth::Interact( void ) {
    auto ent = pursued_entity.lock();
    
    if ( !ent )
        return;
        
    switch ( ent->GetObjectTypeID() ) {
        case TypeID_BugFood : TryHarvest( ent ); break;
        default :
            WARN("Interaction not defined : %s -with- %s.\n", GetIdentifier().c_str(), ent->GetIdentifier().c_str() );
    }
    
    return;
}

void Moth::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    BasicBug::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Moth>( get_typeid<BasicBug>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, mothSetters );
}

void Moth::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    BasicBug::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Moth>( get_typeid<BasicBug>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, mothSetters );
}
