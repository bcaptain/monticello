// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_BEDBUG_H
#define SRC_ENTITIES_BEDBUG_H

#include "../../base/main.h"
#include "./basicBug.h"


class Bedbug : public BasicBug {
public:
    static constexpr TypeInfo<Bedbug, BasicBug> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Bedbug( void );
    ~Bedbug( void ) override;
    Bedbug( const Bedbug& other );

public:
    Bedbug& operator=( const Bedbug& other );

public:
    void Think( void ) override;
    void Animate( void ) override;
    void DoAI( void ) override;
    
    AnimStateT GetAnimStateFromName( const std::string& animStateName ) const override;

    void Spawn( void ) override;
    
    bool AI_Special1( void ) override;
    
    void SetTimeLastSleepAttack( const uint to );
    uint GetTimeLastSleepAttack( void ) const;
    
    void SetSleepAttackCooldown( const uint to );
    uint GetSleepAttackCooldown( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    bool CanSleepAttack( void ) const;
    bool IsDoingSleepAttack( void ) const;
    void DoSleepDust( void );
    
private:
    uint timeLastSleepAttack;
    uint sleepAttackCooldown;
        
// ** Named Setters
private:
    static const uint Bedbug_SETTERS_START = BasicBug_NUM_SETTERS;
protected:
    enum BedbugSetterT : NamedSetterT_BaseType {
        Bedbug_SetTimeLastSleepAttack = Bedbug_SETTERS_START
        , Bedbug_SetSleepAttackCooldown
        , Bedbug_SETTERS_END
        , Bedbug_NUM_SETTERS = Bedbug_SETTERS_END - Bedbug_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > bedBugSetters;
        
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
    static_assert( is_parent_of<Bedbug>( get_typeid<BasicBug>() ) );
    static ToolTip< Bedbug, BasicBug > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_BEDBUG_H
