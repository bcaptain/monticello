// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_CHARACTERS_FORAGER_H
#define SRC_ENTITIES_CHARACTERS_FORAGER_H

#include "../../base/main.h"
#include "./basicBug.h"
#include "../bugNest.h"


class Forager : public BasicBug {
public:
    static constexpr TypeInfo<Forager, BasicBug> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    Forager( void );
    ~Forager( void ) override;
    Forager( const Forager& other );

public:
    Forager& operator=( const Forager& other );

public:
    void Animate( void ) override;
    void DoAI( void ) override;
    
    void SetHomeNest( const std::weak_ptr< Entity >& nest );
    void Spawn( void ) override;
    void Think( void ) override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    void Interact() override;
    void TryEscape( void );
    bool CheckFleeFromPlayer( void );
    
    std::weak_ptr< Entity > homeNest;
    
// ** Named Setters
private:
    static const uint Forager_SETTERS_START = BasicBug_NUM_SETTERS;
protected:
    enum ForagerSetterT : NamedSetterT_BaseType {
        Forager_SETTERS_END = Forager_SETTERS_START
        , Forager_NUM_SETTERS = Forager_SETTERS_END - Forager_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > foragerSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Forager>( get_typeid<BasicBug>() ) );
    static ToolTip< Forager, BasicBug > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_CHARACTERS_FORAGER_H
