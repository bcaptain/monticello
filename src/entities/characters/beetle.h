// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_Beetle_H
#define SRC_ENTITIES_Beetle_H

#include "../../base/main.h"
#include "./basicBug.h"


class Beetle : public BasicBug {
public:
    static constexpr TypeInfo<Beetle, BasicBug> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Beetle( void );
    ~Beetle( void ) override;
    Beetle( const Beetle& other );

public:
    Beetle& operator=( const Beetle& other );

public:
    void Think( void ) override;
    void Animate( void ) override;
    void DoAI( void ) override;
    void Spawn( void ) override;
    bool AI_Special1( void ) override;
    AnimStateT GetAnimStateFromName( const std::string& animStateName ) const override;
    void BumpInto( Entity& ent ) override;
    bool IsCharging( void ) const;
    bool ApplyDamage( const DamageInfo& damageInfo, const std::weak_ptr< Entity >& inflictor_unused, const float percent=1.0f ) override;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;
    
private:
    bool AI_FindValuable( void );
    bool CanCharge( void ) const;
    uint TimeSpentCharging( void ) const;
    void DelayNextCharge( const Uint delay_ms );
    
private:
    SETTER_GETTER_BYVAL( TimeLastChargeCollision, uint, timeLastChargeCollision );
    SETTER_GETTER_BYVAL( ChargeCooldown, uint, chargeCooldown );
    SETTER_GETTER_BYVAL( CheckForValuableDelay, uint, checkForValuableDelay );
    SETTER_GETTER_BYVAL( TimeStartOfCharge, uint, timeStartOfCharge );
    SETTER_GETTER_BYVAL( MaxChargeDuration, uint, maxChargeDuration );
    SETTER_GETTER_BYVAL( ChargeDelay, uint, chargeDelay );
    SETTER_GETTER_BYVAL( HatHealth, uint, hatHealth );
    SETTER_GETTER_BYVAL( MaxHatHealth, uint, maxHatHealth );
    
// ** Named Setters
private:
    static const uint Beetle_SETTERS_START = BasicBug_NUM_SETTERS;
protected:
    enum BeetleSetterT : NamedSetterT_BaseType {
        Beetle_SetTimeLastChargeCollision = Beetle_SETTERS_START
        , Beetle_SetChargeCooldown
        , Beetle_SetCheckForValuableDelay
        , Beetle_SetTimeStartOfCharge
        , Beetle_SetMaxChargeDuration
        , Beetle_SetChargeDelay
        , Beetle_SetHatHealth
        , Beetle_SetMaxHatHealth
        , Beetle_SETTERS_END
        , Beetle_NUM_SETTERS = Beetle_SETTERS_END - Beetle_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > beetleSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );

// ** ToolTips
public:
    static_assert( is_parent_of<Beetle>( get_typeid<BasicBug>() ) );
    static ToolTip< Beetle, BasicBug > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_Beetle_H
