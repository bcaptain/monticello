// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./basicBug.h"
#include "../../sprites/meter.h"
#include "../../rendering/animation/animInfo.h"
#include "../../game.h"

const float ESCAPE_RANGE = 1.0f;
const Uint32 DEFAULT_FLEE_DURATION_MS = 2000;
const Uint32 DEFAULT_STOP_FLEE_RANGE = 5;
const Uint32 DEFAULT_STOP_FLEE_RANGE_SQ = DEFAULT_STOP_FLEE_RANGE * DEFAULT_STOP_FLEE_RANGE;
const Uint32 MINIMUM_WANDER_PERIOD_OFFSET = 100;

const float DEFAULT_HEALTHBAR_UI_WIDTH = 0.8f;
const float DEFAULT_HEALTHBAR_UI_HEIGHT = DEFAULT_HEALTHBAR_UI_WIDTH / 5;
const float DEFAULT_HEALTHBAR_UI_UPDATE_DURATION_MS = 350;

std::vector< std::string > BasicBug::basicBugSetters;

void BasicBug::InitSetterNames( void ) {
    if ( basicBugSetters.size() == BasicBug_NUM_SETTERS )
        return; // already set
    basicBugSetters.resize(BasicBug_NUM_SETTERS);
    
    const uint i = BasicBug_SETTERS_START;
    basicBugSetters[BasicBug_SetHasFood-i] = "hasFood";
    basicBugSetters[BasicBug_SetHarvestAttempts-i] = "harvestAttempts";
    basicBugSetters[BasicBug_SetHarvestAttemptsNeeded-i] = "harvestAttemptsNeeded";
    basicBugSetters[BasicBug_SetActivelyFleeing-i] = "activelyFleeing";
    basicBugSetters[BasicBug_SetFleeTimeStart-i] = "fleeTimeStart";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<BasicBug,Actor>(basicBugSetters);
}

bool BasicBug::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    
    switch ( static_cast<BasicBugSetterT>(setter) ) {
        case BasicBug_SetHasFood: SetHasFood( String::ToBool(value) ); return true;
        case BasicBug_SetHarvestAttempts: SetHarvestAttempts( String::ToUInt(value) ); return true;
        case BasicBug_SetHarvestAttemptsNeeded: SetHarvestAttemptsNeeded( String::ToUInt(value) ); return true;
        case BasicBug_SetActivelyFleeing: SetActivelyFleeing( String::ToBool(value) ); return true;
        case BasicBug_SetFleeTimeStart: SetFleeTimeStart( String::ToUInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case BasicBug_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case BasicBug_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<BasicBug>( get_typeid<Actor>() ) );
            return Actor::Set( setter, value );
    } 
}
std::string BasicBug::Get( const NamedSetterT_BaseType setter ) const {
    
    switch ( static_cast<BasicBugSetterT>(setter) ) {
        case BasicBug_SetHasFood: String::ToString( GetHasFood() );
        case BasicBug_SetHarvestAttempts: return String::ToString(GetHarvestAttempts());
        case BasicBug_SetHarvestAttemptsNeeded: return String::ToString(GetHarvestAttemptsNeeded());
        case BasicBug_SetActivelyFleeing: return String::ToString(IsActivelyFleeing());
        case BasicBug_SetFleeTimeStart: return String::ToString(GetFleeTimeStart());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case BasicBug_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case BasicBug_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<BasicBug>( get_typeid<Actor>() ) );
            return Actor::Get( setter );
    } 
}

NamedSetterT BasicBug::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( basicBugSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + BasicBug_SETTERS_START;
        
    static_assert( is_parent_of<BasicBug>( get_typeid<Actor>() ) );
    return Actor::GetSetterIndex( key );
}

BasicBug::BasicBug( void )
    : Actor()
    , healthbarUI()
    , fleeTimeStart()
    , hasFood()
    , harvestAttempts()
    , harvestAttemptsNeeded()
    , activelyFleeing()
    {
}

BasicBug::BasicBug( const BasicBug& other ) 
    : Actor( other )
    , healthbarUI( other.healthbarUI )
    , fleeTimeStart( other.fleeTimeStart )
    , hasFood( other.hasFood )
    , harvestAttempts( other.harvestAttempts )
    , harvestAttemptsNeeded( other.harvestAttemptsNeeded )
    , activelyFleeing( other.activelyFleeing )
    {
}

BasicBug& BasicBug::operator=( const BasicBug& other ) {
    Actor::operator=( other );
    
    healthbarUI = other.healthbarUI;
    fleeTimeStart = other.fleeTimeStart;
    hasFood = other.hasFood;
    harvestAttempts = other.harvestAttempts;
    harvestAttemptsNeeded = other.harvestAttemptsNeeded;
    activelyFleeing = other.activelyFleeing;
    return *this;
}

BasicBug::~BasicBug( void ) {
}

void BasicBug::Spawn( void ) {
    Actor::Spawn();    
}

void BasicBug::Think( void ) {
    
    if ( IsDamaged() )
        UpdateHealthBarUI();
        
    Actor::Think();
}

bool BasicBug::CheckWander( void ) {
        
    const Uint32 gameTime = game->GetGameTime();
    
    if ( GetAIWanderActiveTime() + GetAIWanderActivePeriod() > gameTime )
        return true;
    
    if ( GetAIWanderDelayedTime() + GetAIWanderDelayPeriod() > gameTime )
        return false;
        
    if ( GetAIWanderActiveTime() <= GetAIWanderDelayedTime() ) {
        SetAIWanderActiveTime( gameTime );
        return true;
    } else {
        SetAIWanderDelayedTime( gameTime );
        return false;
    }
        
}

void BasicBug::SpawnHealthBarUI( void ) {
    
    if ( healthbarUI )
        return;
        
    const auto & mdl = GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;
        
    healthbarUI = std::make_shared< Meter >( DEFAULT_HEALTHBAR_UI_WIDTH, DEFAULT_HEALTHBAR_UI_HEIGHT );
    healthbarUI->SetMaterial( "sprayMeterFullUI" );
    healthbarUI->SetMaskMaterial( "progressBarMask_horizontal" );
    healthbarUI->SetMaskType( MeterMaskType::DIMMER );
    healthbarUI->SetupUV();
    
    Vec3f healthBarMeterOrigin;
    healthBarMeterOrigin.x -= healthbarUI->GetWidth() / 2;
    healthBarMeterOrigin.z = height + healthbarUI->GetHeight();
    healthBarMeterOrigin += GetModelOffset();
    
    healthbarUI->SetOriginOffset( healthBarMeterOrigin );
    
    healthBarMeterOrigin += GetOrigin();
    
    healthbarUI->SetOrigin( healthBarMeterOrigin );
    
    auto sprite = std::static_pointer_cast< Sprite >(healthbarUI);
    game->AddWorldUI( sprite );
}

void BasicBug::UpdateHealthBarUI( void ) {
    if (!healthbarUI)
        SpawnHealthBarUI();
        
    float healthPercentage = static_cast< float >( GetHealth() ) / GetMaxHealth();
    
    if ( Maths::Approxf( healthPercentage, 1.0f ) ) {
        healthbarUI.reset();
        return;
    }
    
    healthbarUI->progress = healthPercentage;
    healthbarUI->sub_progress = 0;
    
    const uint timeSinceDamaged = game->GetGameTime() - GetTimeDamaged();
    
    if ( timeSinceDamaged < DEFAULT_HEALTHBAR_UI_UPDATE_DURATION_MS )
        healthbarUI->sub_progress = ( 1 - ( static_cast<float>( timeSinceDamaged ) / DEFAULT_HEALTHBAR_UI_UPDATE_DURATION_MS ) ) * GetLastDamagePercentOfHealth();

    healthbarUI->SetOrigin( GetOrigin() + healthbarUI->GetOriginOffset() );
}

void BasicBug::TryHarvest( const std::shared_ptr<Entity> target_ent ) {
    if ( !target_ent )
        return;
    
    if ( harvestAttempts >= harvestAttemptsNeeded ) {        
        if ( target_ent->UseBy( this ) )
            harvestAttempts = 0;
            
        modelInfo.RemAnimState( AnimStateT::Interact );
    } else {
        harvestAttempts += 1;
    }
}

void BasicBug::SetHasFood( const bool whether ) {
    hasFood = whether;
}

bool BasicBug::GetHasFood( void ) const {
    return hasFood;
}

void BasicBug::SetHarvestAttempts( const uint to ) {
    harvestAttempts = to;
}

uint BasicBug::GetHarvestAttempts( void ) const {
    return harvestAttempts;
}

void BasicBug::SetHarvestAttemptsNeeded( const uint to ) {
    harvestAttemptsNeeded = to;
}

uint BasicBug::GetHarvestAttemptsNeeded( void ) const {
    return harvestAttemptsNeeded;
}

void BasicBug::SetActivelyFleeing( const bool whether ) {
    activelyFleeing = whether;
}

bool BasicBug::IsActivelyFleeing( void ) const {
    return activelyFleeing;
}
    
void BasicBug::SetFleeTimeStart( const uint to ) {
    fleeTimeStart = to;
}

uint BasicBug::GetFleeTimeStart( void ) const {
    return fleeTimeStart;
}

void BasicBug::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Actor::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<BasicBug>( get_typeid<Actor>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, basicBugSetters );
}

void BasicBug::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Actor::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<BasicBug>( get_typeid<Actor>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, basicBugSetters );
}
