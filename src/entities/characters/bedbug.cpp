// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "bedbug.h"
#include "../../game.h"
#include "../../rendering/animation/animInfo.h"

const auto ANIMSTATE_BEDBUG_SLEEP_ATTACK = AnimStateT::Special1;
const auto ANIMSTATE_BEDBUG_REST = AnimStateT::Special2;

const Uint32 MINIMUM_WANDER_PERIOD_OFFSET = 100;

std::vector< std::string > Bedbug::bedBugSetters;

void Bedbug::InitSetterNames( void ) {
    if ( bedBugSetters.size() == Bedbug_NUM_SETTERS )
        return; // already set
        
    bedBugSetters.resize(Bedbug_NUM_SETTERS);
    
    const uint i = Bedbug_SETTERS_START;
    bedBugSetters[Bedbug_SetSleepAttackCooldown-i] = "sleepAttackCooldown";
    bedBugSetters[Bedbug_SetTimeLastSleepAttack-i] = "timeLastSleepAttack";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Bedbug,BasicBug>(bedBugSetters);
}

bool Bedbug::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<BedbugSetterT>(setter) ) {
        case Bedbug_SetTimeLastSleepAttack: SetTimeLastSleepAttack( String::ToUInt(value) ); return true;
        case Bedbug_SetSleepAttackCooldown: SetSleepAttackCooldown( String::ToUInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Bedbug_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Bedbug_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Bedbug>( get_typeid<BasicBug>() ) );
            return BasicBug::Set( setter, value );
    } 
}
std::string Bedbug::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<BedbugSetterT>(setter) ) {
        case Bedbug_SetTimeLastSleepAttack: return String::ToString(GetTimeLastSleepAttack());
        case Bedbug_SetSleepAttackCooldown: return String::ToString(GetSleepAttackCooldown());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Bedbug_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Bedbug_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Bedbug>( get_typeid<BasicBug>() ) );
            return BasicBug::Get( setter );
    } 
}

NamedSetterT Bedbug::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( bedBugSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Bedbug_SETTERS_START;
        
    static_assert( is_parent_of<Bedbug>( get_typeid<BasicBug>() ) );
    return BasicBug::GetSetterIndex( key );
}

Bedbug::Bedbug( void )
    : BasicBug()
    , timeLastSleepAttack()
    , sleepAttackCooldown()
    {
}

Bedbug::Bedbug( const Bedbug& other ) 
    : BasicBug( other )
    , timeLastSleepAttack( other.timeLastSleepAttack)
    , sleepAttackCooldown( other.sleepAttackCooldown)
    {
}

Bedbug& Bedbug::operator=( const Bedbug& other ) {
    BasicBug::operator=( other );

    timeLastSleepAttack = other.timeLastSleepAttack;
    sleepAttackCooldown = other.sleepAttackCooldown;
    
    return *this;
}

Bedbug::~Bedbug( void ) {
}

void Bedbug::Think( void ) {
    BasicBug::Think();
}

void Bedbug::DoAI( void ) {

    if ( IsStunned() ) {
        return;
    }
    
    if ( !CanSleepAttack() ) {
        if ( CheckWander() ) {
            AI_Wander();
        }
        return;
    }
        
    AI_FindEnemy();
    
    auto pursue_target = pursued_entity.lock();
    if ( pursue_target ) {
        AI_DoState_Actions( *pursue_target );
        return;
    }
    
    if ( !pursue_target )  {
        if ( CheckWander() ) {
            AI_Wander();
        }
        return;
    }
}

bool Bedbug::AI_Special1( void ) {
    
    if ( !CanSleepAttack() )
        return false;
    
    if ( !IsDoingSleepAttack() )
        modelInfo.AddAnimState( ANIMSTATE_BEDBUG_SLEEP_ATTACK );
        
    timeLastSleepAttack = game->GetGameTime();
    
    return true;
        
    //Launch SleepDust projectile entities in cue file
}

void Bedbug::Spawn( void ) {
    BasicBug::Spawn();
    
    //todojesse: what is wanderActivePeriod? a delay? in miliseconds? The name doesn't help to understand it. It doesn't look like it is used anywhere in the code, just set in a few different places.
    //todojesse: Also, don't bother using Uint16 for most cases. Just use a normal int
    const Uint _wanderActivePeriod = GetAIWanderActivePeriod();
    
    const Uint randomTimeOffset( Random::UInt( MINIMUM_WANDER_PERIOD_OFFSET, _wanderActivePeriod / 2 ));
    
    SetAIWanderActivePeriod( _wanderActivePeriod - randomTimeOffset );
    SetSpawnTime( game->GetGameTime() );
    SetDestructible( false );
}

void Bedbug::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "walk", AnimStateT::WalkForward, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 7000 }
            }
        }
        , { "rest", ANIMSTATE_BEDBUG_REST, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 8000 }
            }
        }
        , { "sleepAttack", ANIMSTATE_BEDBUG_SLEEP_ATTACK, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 8000 }
            }
        }
        , { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

bool Bedbug::CanSleepAttack( void ) const {
    return game->GetGameTime() - timeLastSleepAttack >= sleepAttackCooldown;
}

bool Bedbug::IsDoingSleepAttack( void ) const {
    return modelInfo.HasAnimState( ANIMSTATE_BEDBUG_SLEEP_ATTACK );
}

AnimStateT Bedbug::GetAnimStateFromName( const std::string& animStateName ) const {
    if ( animStateName == "rest" )
        return ANIMSTATE_BEDBUG_REST;

    if ( animStateName == "sleepAttack" )
        return ANIMSTATE_BEDBUG_SLEEP_ATTACK;

    return Actor::GetAnimStateFromName( animStateName );
}

void Bedbug::SetTimeLastSleepAttack( const uint to ) {
    timeLastSleepAttack = to;
}

uint Bedbug::GetTimeLastSleepAttack( void ) const {
    return timeLastSleepAttack;
}

void Bedbug::SetSleepAttackCooldown( const uint to ) {
    sleepAttackCooldown = to;
}

uint Bedbug::GetSleepAttackCooldown( void ) const {
    return sleepAttackCooldown;
}

void Bedbug::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    BasicBug::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Bedbug>( get_typeid<BasicBug>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, bedBugSetters );
}

void Bedbug::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    BasicBug::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Bedbug>( get_typeid<BasicBug>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, bedBugSetters );
}
