// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./beetle.h"
#include "../../game.h"
#include "../../damage/damageManager.h"
#include "../pedestal.h"
#include "../../rendering/animation/animInfo.h"

const auto ANIMSTATE_BEETLE_READY_CHARGE = AnimStateT::Special1;
const auto ANIMSTATE_BEETLE_CHARGE = AnimStateT::Special2;
const auto ANIMSTATE_BEETLE_COLLIDE = AnimStateT::Special3;
const auto ANIMSTATE_BEETLE_STUNNED = AnimStateT::Special4;
const auto ANIMSTATE_BEETLE_READY_CHARGE_WITH_PARTYHAT = AnimStateT::Special5;
const auto ANIMSTATE_BEETLE_CHARGE_WITH_PARTYHAT = AnimStateT::Special6;
const auto ANIMSTATE_BEETLE_COLLIDE_WITH_PARTYHAT = AnimStateT::Special7;
const auto ANIMSTATE_BEETLE_STUNNED_WITH_PARTYHAT = AnimStateT::Special8;
const auto ANIMSTATE_BEETLE_IDLE_WITH_PARTYHAT = AnimStateT::Special9;
const auto ANIMSTATE_BEETLE_WALK_WITH_PARTYHAT = AnimStateT::Special10;
const auto ANIMSTATE_BEETLE_HATDAMAGED = AnimStateT::Special11;

const float MINIMUM_WANDER_PERIOD_MULTIPLIER = 0.8f;

std::vector< std::string > Beetle::beetleSetters;

void Beetle::InitSetterNames( void ) {
    if ( beetleSetters.size() == Beetle_NUM_SETTERS )
        return; // already set
    beetleSetters.resize(Beetle_NUM_SETTERS);
    
    const uint i = Beetle_SETTERS_START;
    beetleSetters[Beetle_SetTimeLastChargeCollision-i] = "timeLastChargeCollision";
    beetleSetters[Beetle_SetChargeCooldown-i] = "chargeCooldown";
    beetleSetters[Beetle_SetCheckForValuableDelay-i] = "checkForValuableDelay";
    beetleSetters[Beetle_SetTimeStartOfCharge-i] = "timeStartOfCharge";
    beetleSetters[Beetle_SetMaxChargeDuration-i] = "maxChargeDuration";
    beetleSetters[Beetle_SetChargeDelay-i] = "chargeDelay";
    beetleSetters[Beetle_SetHatHealth-i] = "hatHealth";
    beetleSetters[Beetle_SetMaxHatHealth-i] = "maxHatHealth";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Beetle,BasicBug>(beetleSetters);
}

bool Beetle::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<BeetleSetterT>(setter) ) {
        case Beetle_SetTimeLastChargeCollision: SetTimeLastChargeCollision( String::ToUInt(value) ); return true;
        case Beetle_SetChargeCooldown: SetChargeCooldown( String::ToUInt(value) ); return true;
        case Beetle_SetCheckForValuableDelay: SetCheckForValuableDelay( String::ToUInt(value) ); return true;
        case Beetle_SetTimeStartOfCharge: SetTimeStartOfCharge( String::ToUInt(value) ); return true;
        case Beetle_SetMaxChargeDuration: SetMaxChargeDuration( String::ToUInt(value) ); return true;
        case Beetle_SetChargeDelay: SetChargeDelay( String::ToUInt(value) ); return true;
        case Beetle_SetHatHealth: SetHatHealth( String::ToUInt(value) ); return true;
        case Beetle_SetMaxHatHealth: SetMaxHatHealth( String::ToUInt(value) ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Beetle_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        default: return BasicBug::Set( setter, value );
    } 
}
std::string Beetle::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<BeetleSetterT>(setter) ) {
        case Beetle_SetTimeLastChargeCollision: return String::ToString(GetTimeLastChargeCollision());
        case Beetle_SetChargeCooldown: return String::ToString(GetChargeCooldown());
        case Beetle_SetCheckForValuableDelay: return String::ToString(GetCheckForValuableDelay());
        case Beetle_SetTimeStartOfCharge: return String::ToString(GetTimeStartOfCharge());
        case Beetle_SetMaxChargeDuration: return String::ToString(GetMaxChargeDuration());
        case Beetle_SetChargeDelay: return String::ToString(GetChargeDelay());
        case Beetle_SetHatHealth: return String::ToString(GetHatHealth());
        case Beetle_SetMaxHatHealth: return String::ToString(GetMaxHatHealth());
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Beetle_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        default: return BasicBug::Get( setter );
    } 
}

NamedSetterT Beetle::GetSetterIndex( const std::string& key ) {
    const NamedSetterT ret =  GetSetterIndexIn( beetleSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Beetle_SETTERS_START;
    return BasicBug::GetSetterIndex( key );
}

Beetle::Beetle( void )
    : BasicBug()
    , timeLastChargeCollision()
    , chargeCooldown()
    , checkForValuableDelay()
    , timeStartOfCharge()
    , maxChargeDuration()
    , chargeDelay()
    , hatHealth()
    , maxHatHealth()
    {
}

Beetle::Beetle( const Beetle& other ) 
    : BasicBug( other )
    , timeLastChargeCollision(other.timeLastChargeCollision)
    , chargeCooldown(other.chargeCooldown)
    , checkForValuableDelay(other.checkForValuableDelay)
    , timeStartOfCharge(other.timeStartOfCharge)
    , maxChargeDuration(other.maxChargeDuration)
    , chargeDelay(other.chargeDelay)
    , hatHealth(other.hatHealth)
    , maxHatHealth(other.maxHatHealth)
    {
}

Beetle& Beetle::operator=( const Beetle& other ) {
    BasicBug::operator=( other );
    
    timeLastChargeCollision = other.timeLastChargeCollision;
    chargeCooldown = other.chargeCooldown;
    checkForValuableDelay = other.checkForValuableDelay;
    timeStartOfCharge = other.timeStartOfCharge;
    maxChargeDuration = other.maxChargeDuration;
    chargeDelay = other.chargeDelay;
    hatHealth = other.hatHealth;
    maxHatHealth = other.maxHatHealth;
    
    return *this;
}

Beetle::~Beetle( void ) {
}

void Beetle::Think( void ) {  
    BasicBug::Think();
    
    auto pursue_target = pursued_entity.lock();
    
    if ( pursue_target )
        SetRunning( IsCharging() );
}

bool Beetle::IsCharging( void ) const {
    return modelInfo.HasAnimState( ANIMSTATE_BEETLE_CHARGE );
}

bool Beetle::AI_Special1( void ) {
    auto pursue_target = pursued_entity.lock();
    if ( !pursue_target )
        return false;
        
    AI_Chase( *pursue_target );
    
    if ( !IsCharging() ) {
        modelInfo.AddAnimState( ANIMSTATE_BEETLE_READY_CHARGE );
        timeStartOfCharge = game->GetGameTime();
        chargeDelay = 0;
    } else if ( TimeSpentCharging() >= maxChargeDuration ) {
            pursued_entity.reset();
            modelInfo.RemAnimState( ANIMSTATE_BEETLE_CHARGE );
            modelInfo.RemAnimState( ANIMSTATE_BEETLE_CHARGE_WITH_PARTYHAT );
            DelayNextCharge( chargeCooldown * 2 );
    }
    
    return true;
}

uint Beetle::TimeSpentCharging( void ) const {
    return game->GetGameTime() - timeStartOfCharge;
}

void Beetle::DelayNextCharge( const Uint delay_ms ) {
    timeLastChargeCollision = game->GetGameTime();
    chargeDelay = delay_ms;
}

void Beetle::BumpInto( Entity& ent ) {
    auto pursue_target = pursued_entity.lock();
    if ( pursue_target ) {
        if ( pursue_target.get() == &ent ) {
            pursue_target->UseBy( this );
            modelInfo.AddAnimState( ANIMSTATE_BEETLE_COLLIDE );
            pursued_entity.reset();
            //TESTING, I want the beetle to bounce back a bit after colliding
                bounds.SetVelocity(0,10,0);
                timeLastChargeCollision = game->GetGameTime();
            //END TESTING
        }
    }
        
    Actor::BumpInto( ent );
}

void Beetle::DoAI( void ) {
   
    if ( IsStunned() ) {
        return;
    }
    
    auto pursue_target = pursued_entity.lock();
    if ( pursue_target ) {
        AI_DoState_Actions( *pursue_target );
        return;
    }
    
    if ( CanCharge() ) {
        if ( AI_FindValuable() )
            return;
    }
        
    //AI_FindEnemy();

    if ( !pursue_target )  {
        if ( CheckWander() ) {
            AI_Wander();
        }
        return;
    }
}

bool Beetle::CanCharge( void ) const {
    return game->GetGameTime() - timeLastChargeCollision >= chargeCooldown + chargeDelay;
}

void Beetle::Spawn( void ) {
    Actor::Spawn();

    const float randomTimeOffsetMult( Random::Float( MINIMUM_WANDER_PERIOD_MULTIPLIER , 1.0f ));
    
    SetAIWanderActivePeriod( static_cast< uint >( GetAIWanderActivePeriod() * randomTimeOffsetMult ) );
    SetSpawnTime( game->GetGameTime() );
    SetDestructible( false );
    
    const std::string attach_pos = "hatAttach";
    const std::string attach_name = "partyHat";
    AddAttachment( attach_pos, attach_name, 0 ); //todo: attachmentChannel
    
    AddDamageVuln( "groundImpact", 1 );
    AddDamageVuln( "poison", 1 );
}


bool Beetle::ApplyDamage( const DamageInfo& damageInfo, [[maybe_unused]] const std::weak_ptr< Entity >& inflictor, const float percent ) {
    if ( !HasAttachment( "hatAttach", "partyHat" ) )
        Actor::ApplyDamage( damageInfo, inflictor, percent );
        
    int damageAmount;
    if ( damageVuln ) {
        damageAmount = damageVuln->Result( damageInfo );
    } else {
        damageAmount = damageInfo.GetTotalDamage();
    }
    
    if ( !damageAmount )
        return false;
        
    hatHealth -= damageAmount;
    if ( hatHealth < 1 ) {
        modelInfo.RemoveAttachmentAtIndex( 0 );
        RemoveDamageVuln( "groundImpact" );
        RemoveDamageVuln( "poison" );
    }
    
    modelInfo.AddAnimState( ANIMSTATE_BEETLE_HATDAMAGED );
    return true;
}

void Beetle::Animate( void ) {
    
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;
    
    // order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
    const static std::vector< AnimSelection > anims(
    {
        { "hatDamaged", ANIMSTATE_BEETLE_HATDAMAGED, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "stunned", ANIMSTATE_BEETLE_STUNNED, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "stunnedWithPartyHat", ANIMSTATE_BEETLE_STUNNED_WITH_PARTYHAT, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "collide", ANIMSTATE_BEETLE_COLLIDE, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "collideWithPartyHat", ANIMSTATE_BEETLE_COLLIDE_WITH_PARTYHAT, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "charge", ANIMSTATE_BEETLE_CHARGE, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "chargeWithPartyHat", ANIMSTATE_BEETLE_CHARGE_WITH_PARTYHAT, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "readyCharge", ANIMSTATE_BEETLE_READY_CHARGE, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "readyChargeWithPartyHat", ANIMSTATE_BEETLE_READY_CHARGE_WITH_PARTYHAT, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 9000 }
            }
        }
        , { "walk", AnimStateT::WalkForward, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 7000 }
            }
        }
        , { "walkWithPartyHat", ANIMSTATE_BEETLE_WALK_WITH_PARTYHAT, AnimOptT::BlendInto,
            {
                { AnimChannelT::Legs, 7000 }
            }
        }
        , { "idle", AnimStateT::Idle, AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
        , { "idleWithPartyHat", ANIMSTATE_BEETLE_IDLE_WITH_PARTYHAT, AnimOptT::BlendInto | AnimOptT::Loop,
            {
                { AnimChannelT::Legs, 1000 }
            }
        }
    }
    );
    
    animInfo->Animate( anims, GetAnimSpeed() );
}

bool Beetle::AI_FindValuable( void ) {
    if ( auto pursue_ent = pursued_entity.lock() )
        if ( derives_from( TypeID_Pedestal, *pursue_ent ) )
            return true;

    if ( game->GetGameTime() - GetPrevFindCheck() < checkForValuableDelay )
        return false;
        
    SetPrevFindCheck( game->GetGameTime() );

    std::vector< CollisionItem > collisions;
    GetEntitiesInSight( collisions, TypeID_Pedestal );
    
    for ( uint i=0; i<collisions.size(); ++i ) {
        auto ent = collisions[i].ent.lock();
        if ( !ent || !derives_from<Pedestal>(*ent) ) {
            PopSwap( collisions, i-- );
            continue;
        }
            
        auto pedestal = std::static_pointer_cast< Pedestal >( ent );
        if ( pedestal && !pedestal->IsUsableBy(this) ) {
            PopSwap( collisions, i-- );
            continue;
        }
    }
    
    pursued_entity = GetClosestEntityInList( collisions );

    if ( pursued_entity.lock() )
        return true;
    
    return false;
}

AnimStateT Beetle::GetAnimStateFromName( const std::string& animStateName ) const {
    
    if ( animStateName == "readyCharge" )
        return ANIMSTATE_BEETLE_READY_CHARGE;

    if ( animStateName == "charge" )
        return ANIMSTATE_BEETLE_CHARGE;
    
    if ( animStateName == "collide" )
        return ANIMSTATE_BEETLE_COLLIDE;
        
    if ( animStateName == "stunned" )
        return ANIMSTATE_BEETLE_STUNNED;     

    return Actor::GetAnimStateFromName( animStateName );
}

void Beetle::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    BasicBug::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Beetle>( get_typeid<BasicBug>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, beetleSetters );
}

void Beetle::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    BasicBug::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Beetle>( get_typeid<BasicBug>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, beetleSetters );
}
