// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_ENTITIES_CHARACTERS_BASICBUG_H
#define SRC_ENTITIES_CHARACTERS_BASICBUG_H

#include "../../base/main.h"
#include "../actor.h"
#include "../bugNest.h"

class Meter;


class BasicBug : public Actor {
public:
    static constexpr TypeInfo<BasicBug, Actor> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }

public:
    BasicBug( void );
    ~BasicBug( void ) override;
    BasicBug( const BasicBug& other );

public:
    BasicBug& operator=( const BasicBug& other );
    
public:
    void Spawn( void ) override;
    void Think( void ) override;
    
    void SetHasFood( const bool whether );
    bool GetHasFood( void ) const;
    
    void SetActivelyFleeing( const bool whether );
    bool IsActivelyFleeing( void ) const;
    
    void SetHarvestAttempts( const uint to );
    uint GetHarvestAttempts( void ) const;
    
    void SetHarvestAttemptsNeeded( const uint to );
    uint GetHarvestAttemptsNeeded( void ) const;
    
    void SetFleeTimeStart( const uint to );
    uint GetFleeTimeStart( void ) const;
    
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) override;

protected:
    bool CheckWander( void );
    void UpdateHealthBarUI( void );
    
    void TryHarvest( const std::shared_ptr<Entity> target_ent );
    
private:
    void SpawnHealthBarUI( void );
    
private:
    std::shared_ptr< Meter > healthbarUI;

protected:
    uint fleeTimeStart;
    bool hasFood;
    bool harvestAttempts;
    bool harvestAttemptsNeeded;
    bool activelyFleeing;

// ** Named Setters
private:
    static const uint BasicBug_SETTERS_START = Actor_NUM_SETTERS;
protected:
    enum BasicBugSetterT : NamedSetterT_BaseType {
        BasicBug_SetHasFood = BasicBug_SETTERS_START
        , BasicBug_SetHarvestAttempts
        , BasicBug_SetHarvestAttemptsNeeded
        , BasicBug_SetActivelyFleeing
        , BasicBug_SetFleeTimeStart
        , BasicBug_SETTERS_END
        , BasicBug_NUM_SETTERS = BasicBug_SETTERS_END - BasicBug_SETTERS_START
        // NOTE: don't forget to add new entries to ThisClass::InitSetterNames() and ThisClass::Set()
    };

private:
    static std::vector< std::string > basicBugSetters;
public:
    bool Set( const NamedSetterT_BaseType setter, const std::string& value ) override;
    std::string Get( const NamedSetterT_BaseType setter ) const override;
    static NamedSetterT GetSetterIndex( const std::string& key );
    static void InitSetterNames( void );
    
// ** ToolTips
public:
    static_assert( is_parent_of<BasicBug>( get_typeid<Actor>() ) );
    static ToolTip< BasicBug, Actor > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif  // SRC_ENTITIES_CHARACTERS_BASICBUG_H
