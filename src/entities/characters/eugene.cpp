// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./eugene.h"
#include "./clib/src/warnings.h"
#include "../../game.h"
#include "../../damage/damageManager.h"
#include "../../rendering/renderer.h"
#include "../entityInfoManager.h"
#include "../effects/stompEffect.h"
#include "../actor.h"
#include "../../sprites/sprite.h"
#include "../../sprites/meter.h"
#include "../modules/flat.h"
#include "../../rendering/animation/animInfo.h"

const auto ANIMSTATE_EUGENE_ATTACK_WITH_SPRAYER = AnimStateT::Special1;
const auto ANIMSTATE_EUGENE_IDLE_WITH_SPRAYER = AnimStateT::Special2;
const auto ANIMSTATE_EUGENE_WALK_WITH_SPRAYER = AnimStateT::Special3;
const auto ANIMSTATE_EUGENE_RUN_WITH_SPRAYER = AnimStateT::Special4;
const auto ANIMSTATE_EUGENE_STOMP_WITH_SPRAYER = AnimStateT::Special5;
const auto ANIMSTATE_EUGENE_ATTACK_LEFT_WITH_NEWSPAPER = AnimStateT::Special6;
const auto ANIMSTATE_EUGENE_ATTACK_LEFT_RECOVER_WITH_NEWSPAPER = AnimStateT::Special7;
const auto ANIMSTATE_EUGENE_ATTACK_RIGHT_WITH_NEWSPAPER = AnimStateT::Special8;
const auto ANIMSTATE_EUGENE_ATTACK_RIGHT_RECOVER_WITH_NEWSPAPER = AnimStateT::Special9;
const auto ANIMSTATE_EUGENE_STOMP_WITH_NEWSPAPER = AnimStateT::Special10;
const auto ANIMSTATE_EUGENE_RUN_WITH_NEWSPAPER = AnimStateT::Special11;
const auto ANIMSTATE_EUGENE_IDLE_WITH_NEWSPAPER = AnimStateT::Special12;
const auto ANIMSTATE_EUGENE_WALK_WITH_NEWSPAPER = AnimStateT::Special13;
const auto ANIMSTATE_EUGENE_CONSTRUCT = AnimStateT::Special14;
const auto ANIMSTATE_EUGENE_READY_ATTACK_WITH_SPRAYER = AnimStateT::Special15;
const auto ANIMSTATE_EUGENE_SLEEP = AnimStateT::Special16;
const auto ANIMSTATE_EUGENE_SLEEP_WITH_SPRAYER = AnimStateT::Special17;
const auto ANIMSTATE_EUGENE_SLEEP_WITH_NEWSPAPER = AnimStateT::Special18;

const float DEFAULT_STOMP_EFFECT_HEIGHT = 0.05f;
const float DEFAULT_MELEE_EFFECT_EDGE_LENGTH = 0.8f;
const float DEFAULT_SLEEPMETER_UI_EDGE_LENGTH = 0.8f;
const float DEFAULT_SPRAYERFUEL_UI_WIDTH = 1.0f;
const float DEFAULT_SPRAYERFUEL_UI_HEIGHT = DEFAULT_SPRAYERFUEL_UI_WIDTH / 4;
const float DEFAULT_SLEEP_RECOVERY_PER_FRAME_ASLEEP = 0.005f;
const int DEFAULT_SLEEP_RECOVERY_PER_FRAME_AWAKE = 2;
const Uint32 SLEEP_ADD_TIMER_THRESHOLD = 250;

// order is important. if interrupt value is equal, the animation closest to the beginning of the list will take precedence
const std::vector< AnimSelection > Eugene::anims {
    { "sleep", ANIMSTATE_EUGENE_SLEEP, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9800 }
            , { AnimChannelT::Torso, 9800 }
          }
    }
    , { "sleepWithSprayer", ANIMSTATE_EUGENE_SLEEP_WITH_SPRAYER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9800 }
            , { AnimChannelT::Torso, 9800 }
          }
    }
    , { "sleepWithNewspaper", ANIMSTATE_EUGENE_SLEEP_WITH_NEWSPAPER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9800 }
            , { AnimChannelT::Torso, 9800 }
          }
    }
    , {
      "damage", AnimStateT::Hurt, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 2500 }
            , { AnimChannelT::Torso, 9700 }
          }
    }
    , { "construct", ANIMSTATE_EUGENE_CONSTRUCT, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 3000 }
            , { AnimChannelT::Torso, 3000 }
          }
    }
    , { "stomp", AnimStateT::Attack2, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9500 }
            , { AnimChannelT::Torso, 9500 }
          }
    }
    , { "stompWithNewspaper", ANIMSTATE_EUGENE_STOMP_WITH_NEWSPAPER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9500 }
            , { AnimChannelT::Torso, 9500 }
          }
    }
    , { "stompWithSprayer", ANIMSTATE_EUGENE_STOMP_WITH_SPRAYER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9500 }
            , { AnimChannelT::Torso, 9500 }
          }
    }
    , { "attackLoopWithSprayer", ANIMSTATE_EUGENE_ATTACK_WITH_SPRAYER, AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 9000 }
            , { AnimChannelT::Torso, 9000 }
          }
    }
    , { "readyAttackWithSprayer", ANIMSTATE_EUGENE_READY_ATTACK_WITH_SPRAYER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 9000 }
            , { AnimChannelT::Torso, 9000 }
          }
    }
    , { "attackLeftWithNewspaper", ANIMSTATE_EUGENE_ATTACK_LEFT_WITH_NEWSPAPER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 3000 }
            , { AnimChannelT::Torso, 9000 }
          }
    }
    , { "attackLeftRecoverWithNewspaper", ANIMSTATE_EUGENE_ATTACK_LEFT_RECOVER_WITH_NEWSPAPER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 3000 }
            , { AnimChannelT::Torso, 9000 }
          }
    }
    , { "attackRightWithNewspaper", ANIMSTATE_EUGENE_ATTACK_RIGHT_WITH_NEWSPAPER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 3000 }
            , { AnimChannelT::Torso, 9000 }
          }
    }
    , { "attackRightRecoverWithNewspaper", ANIMSTATE_EUGENE_ATTACK_RIGHT_RECOVER_WITH_NEWSPAPER, AnimOptT::BlendInto,
          {
              { AnimChannelT::Legs, 3000 }
            , { AnimChannelT::Torso, 9000 }
          }
    }
    , { "walk", AnimStateT::WalkForward, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7000 }
            , { AnimChannelT::Torso, 7000 }
          }
    }
    , { "walkWithSprayer", ANIMSTATE_EUGENE_WALK_WITH_SPRAYER, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7000 }
            , { AnimChannelT::Torso, 7000 }
          }
    }
    , { "walkWithNewspaper", ANIMSTATE_EUGENE_WALK_WITH_NEWSPAPER, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7000 }
            , { AnimChannelT::Torso, 7000 }
          }
    }
    , { "run", AnimStateT::RunForward, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7500 }
            , { AnimChannelT::Torso, 7500 }
          }
    }
    , { "runWithNewspaper", ANIMSTATE_EUGENE_RUN_WITH_NEWSPAPER, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7500 }
            , { AnimChannelT::Torso, 7500 }
          }
    }
    , { "runWithSprayer", ANIMSTATE_EUGENE_RUN_WITH_SPRAYER, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7500 }
            , { AnimChannelT::Torso, 7500 }
          }
    }
    , { "run", AnimStateT::RunForward, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 7500 }
            , { AnimChannelT::Torso, 7500 }
          }
    }
    , { "idleWithNewspaper", ANIMSTATE_EUGENE_IDLE_WITH_NEWSPAPER, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 1000 }
            , { AnimChannelT::Torso, 1000 }
          }
    }
    , { "idleWithSprayer", ANIMSTATE_EUGENE_IDLE_WITH_SPRAYER, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 1000 }
            , { AnimChannelT::Torso, 1000 }
          }
    }
    , { "idle", AnimStateT::Idle, AnimOptT::BlendInto | AnimOptT::Loop,
          {
              { AnimChannelT::Legs, 1000 }
            , { AnimChannelT::Torso, 1000 }
          }
    }
};

//Todo ammo : when ammo moves to another class / attached object we can set these values from there
const uint DEFAULT_GAS_AMMO_CAPACITY = 100;

std::vector< std::string > Eugene::eugeneSetters;

void Eugene::InitSetterNames( void ) {
    if ( eugeneSetters.size() == Eugene_NUM_SETTERS )
        return; // already set
    eugeneSetters.resize(Eugene_NUM_SETTERS);
    
    const uint i = Eugene_SETTERS_START;
    eugeneSetters[Eugene_SetStompDamage-i] = "dmg_eugeneStomp";
    eugeneSetters[Eugene_SetMeleeDamage-i] = "dmg_eugeneMelee";
    eugeneSetters[Eugene_SetStompMaxTargets-i] = "stompMaxTargets";
    eugeneSetters[Eugene_SetMeleeMaxTargets-i] = "meleeMaxTargets";
    eugeneSetters[Eugene_SetStompRadius-i] = "stompRadius";
    eugeneSetters[Eugene_SetStompOffset-i] = "stompOffset";
    eugeneSetters[Eugene_SetMeleeRadius-i] = "meleeRadius";
    eugeneSetters[Eugene_SetMeleeOffset-i] = "meleeOffset";
    eugeneSetters[Eugene_SetStompEffect-i] = "fx_stomp";
    eugeneSetters[Eugene_SetSleepRecoveryPerFrame-i] = "sleepRecoveryPerFrame";
    // NOTE: don't forget to add new entries to ThisClass::Set()
    
    CheckSetterNames<Eugene,Actor>(eugeneSetters);
}

bool Eugene::Set( const NamedSetterT_BaseType setter, const std::string& value ) {
    switch ( static_cast<EugeneSetterT>(setter) ) {
        case Eugene_SetStompDamage: SetStompDamage( value ); return true;
        case Eugene_SetMeleeDamage: SetMeleeDamage( value ); return true;
        case Eugene_SetStompMaxTargets: SetStompMaxTargets( String::ToUInt(value) ); return true;
        case Eugene_SetMeleeMaxTargets: SetMeleeMaxTargets( String::ToUInt(value) ); return true;
        case Eugene_SetStompRadius: SetStompRadius( String::ToFloat(value) ); return true;
        case Eugene_SetMeleeRadius: SetMeleeRadius( String::ToFloat(value) ); return true;
        case Eugene_SetSleepRecoveryPerFrame: SetSleepRecoveryPerFrame( String::ToFloat(value) ); return true;
        case Eugene_SetStompOffset: SetStompOffset( String::ToVec3f(value) ); return true;
        case Eugene_SetMeleeOffset: SetMeleeOffset( String::ToVec3f(value) ); return true;
        case Eugene_SetStompEffect: SetStompEffect( value ); return true;
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Eugene_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Eugene_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
            return Actor::Set( setter, value );
    } 
}

std::string Eugene::Get( const NamedSetterT_BaseType setter ) const {
    switch ( static_cast<EugeneSetterT>(setter) ) {
        case Eugene_SetStompDamage: return GetStompDamage();
        case Eugene_SetMeleeDamage: return GetMeleeDamage();
        case Eugene_SetStompMaxTargets: return String::ToString( GetStompMaxTargets() );
        case Eugene_SetMeleeMaxTargets: return String::ToString( GetMeleeMaxTargets() );
        case Eugene_SetStompRadius: return String::ToString( GetStompRadius() );
        case Eugene_SetMeleeRadius: return String::ToString( GetMeleeRadius() );
        case Eugene_SetSleepRecoveryPerFrame: return String::ToString( GetSleepRecoveryPerFrame() );
        case Eugene_SetStompOffset: return String::ToString( GetStompOffset() );
        case Eugene_SetMeleeOffset: return String::ToString( GetMeleeOffset() );
        case Eugene_SetStompEffect: return GetStompEffect();
        // NOTE: don't forget to add new entries names to ThisClass::InitSetterNames()
        case Eugene_SETTERS_END: FALLTHROUGH; // included for -Wswitch-enum
        case Eugene_NUM_SETTERS: FALLTHROUGH; // included for -Wswitch-enum
        default:
            static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
            return Actor::Get( setter );
    } 
}

NamedSetterT Eugene::GetSetterIndex( const std::string& key ) {
    NamedSetterT ret = Entity::GetSetterIndexIn( eugeneSetters, key );
    if ( ret != INVALID_SETTER )
        return ret + Eugene_SETTERS_START;
        
    static_assert( is_parent_of<Actor>( get_typeid<Entity>() ) );
    return Actor::GetSetterIndex( key );
}

Eugene::Eugene( void )
    : Actor()
    , stompOffset()
    , meleeOffset()
    , dmg_eugeneStomp()
    , dmg_eugeneMelee()
    , fx_stomp()
    , stompRadius()
    , meleeRadius()
    , sleepRecoveryPerFrame()
    , stompMaxTargets()
    , meleeMaxTargets()
    , melee_effect()
    , sleep_meter()
    , sprayerFuel_meter()
    {
}

Eugene::Eugene( const Eugene& other ) 
    : Actor( other )
    , stompOffset( other.stompOffset )
    , meleeOffset( other.meleeOffset )
    , dmg_eugeneStomp( other.dmg_eugeneStomp )
    , dmg_eugeneMelee( other.dmg_eugeneMelee )
    , fx_stomp( other.fx_stomp )
    , stompRadius( other.stompRadius )
    , meleeRadius( other.meleeRadius )
    , sleepRecoveryPerFrame( other.sleepRecoveryPerFrame )
    , stompMaxTargets( other.stompMaxTargets )
    , meleeMaxTargets( other.meleeMaxTargets )
    , melee_effect( other.melee_effect )
    , sleep_meter( other.sleep_meter )
    , sprayerFuel_meter( other.sprayerFuel_meter )
    {
}

Eugene& Eugene::operator=( const Eugene& other ) {
    if ( this == &other )
        return *this;

    stompOffset = other.stompOffset;
    meleeOffset = other.meleeOffset;
    dmg_eugeneStomp = other.dmg_eugeneStomp;
    dmg_eugeneMelee = other.dmg_eugeneMelee;
    fx_stomp = other.fx_stomp;
    stompRadius = other.stompRadius;
    meleeRadius = other.meleeRadius;
    sleepRecoveryPerFrame = other.sleepRecoveryPerFrame;
    stompMaxTargets = other.stompMaxTargets;
    meleeMaxTargets = other.meleeMaxTargets;
    melee_effect = other.melee_effect;
    sleep_meter = other.sleep_meter;
    sprayerFuel_meter = other.sprayerFuel_meter;
    
    Actor::operator=( other );
    
    return *this;
}

Eugene::~Eugene( void ) {
}

void Eugene::Spawn( void ) {
    ASSERT( Maths::Approxf( FPS_CAP, 60.0f ) ); //If FPS_CAP changes we'll need to adjust the calculation below
    const float EXPORT_ANIM_FPS = 30.0f;
    const float frameMultiplier = FPS_CAP / EXPORT_ANIM_FPS;
    sleepRecoveryPerFrame = 1.0f / ( modelInfo.GetNumAnimFrames( "sleep", AnimChannelT::Channel_0 ) * frameMultiplier );
    
    //Todo ammo : when ammo moves to another class / attached object we can set these values from there
        SetAmmoCapacity( AmmoTypeT::Gas, DEFAULT_GAS_AMMO_CAPACITY );
        GiveAmmo( AmmoTypeT::Gas, DEFAULT_GAS_AMMO_CAPACITY );
    //end todo
    
    Actor::Spawn();
}

void Eugene::Think( void ) {
    Actor::Think();
            
    if ( GetSleepLevel() > 0 ) {
        if ( !IsAsleep() )
            UpdateSleepLevel();
        UpdateSleepMeterUI();
    }
}

bool Eugene::IsNotAttacking( void ) const {
    if ( modelInfo.HasAnimState( AnimStateT::Attack1 ) )
        return true;
        
    if ( modelInfo.HasAnimState( AnimStateT::Attack2 ) )
        return true;
        
    return false;
}

bool Eugene::IsReadyToAttack1( void ) const {
    if ( !Actor::IsReadyToAttack1() )
        return false;

    if ( HasAttachment( "rHandItem", "sprayer" ) ) {
        // we let eugene attack with the sprayer even if it does a sad harmless puff
        return true;
    }

    if ( HasAttachment( "rHandItem", "newspaper" ) ) {
        return true;
    }

    return false;
}

bool Eugene::IsRecoveringFromMeleeLeftAttack( void ) const {
    return modelInfo.HasAnimState( ANIMSTATE_EUGENE_ATTACK_LEFT_RECOVER_WITH_NEWSPAPER );
}

bool Eugene::ExecuteInputAction( const std::string& action, const InputTypeT type ) {
    if ( type == InputTypeT::Hold ) {
        if ( action == "attack1" ) {
            
            if ( IsRecoveringFromMeleeLeftAttack() ) {
                if ( HasAttachment( "rHandItem", "newspaper" ) ) {
                    modelInfo.AddAnimState( ANIMSTATE_EUGENE_ATTACK_RIGHT_WITH_NEWSPAPER );
                    return true;
                }
            }
            
            if ( IsReadyToAttack1() ) {
                if ( HasAttachment( "rHandItem", "sprayer" ) ) {
                    if ( IsNotAttacking() ) {
                        modelInfo.AddAnimState( ANIMSTATE_EUGENE_READY_ATTACK_WITH_SPRAYER );
                        return true;
                    }
                    UpdateSprayerFuelMeterUI();
                }
                if ( HasAttachment( "rHandItem", "newspaper" ) ) {
                    modelInfo.AddAnimState( ANIMSTATE_EUGENE_ATTACK_LEFT_WITH_NEWSPAPER );
                    return true;
                }
            }
        }
        
        if ( action == "attack2" ) {
            if ( IsReadyToAttack2() ) {
                modelInfo.AddAnimState( AnimStateT::Attack2 );
                return true;
            }
        }
        
        if ( action == "special2" ) {
            if ( IsReadyToSpecial2() ) {
                modelInfo.AddAnimState( ANIMSTATE_EUGENE_CONSTRUCT );
                modelInfo.SetAttachmentVisibility( false );
            }
            return true;
        }
        
        return Actor::ExecuteInputAction( action, type );
    }
    
    if ( action == "swapWeapons" ) {
        if ( type == InputTypeT::Press ) {
            if ( modelInfo.NumAttachments() > 0 ) {
                EquipNextAttachment();
            }
        }
        return true;
    }
    
    if ( action == "special2" ) {
        if ( type == InputTypeT::Release ) {
            modelInfo.SetAttachmentVisibility( true );
            modelInfo.RemAnimState(ANIMSTATE_EUGENE_CONSTRUCT);
        }
        return true;
    }
    
    if ( action == "attack1" ) {
        if ( type == InputTypeT::Release ) {
            sprayerFuel_meter.reset();
            modelInfo.RemAnimState(ANIMSTATE_EUGENE_READY_ATTACK_WITH_SPRAYER);
            modelInfo.RemAnimState(ANIMSTATE_EUGENE_ATTACK_WITH_SPRAYER);
        }
        return true;
    }
    
    return Actor::ExecuteInputAction( action, type );
}

void Eugene::EquipNextAttachment( void ) {
    //TODO: Inventory / Equipment list for character that we can reference during this swap function?
    std::string nextAttachmentName = "sprayer";
                
    if ( HasAttachment( "rHandItem", "sprayer" ) )
        nextAttachmentName = "newspaper";
        
    uint attachment_index = modelInfo.GetAttachmentIndexAtPos( "rHandItem" );
    if ( attachment_index < modelInfo.NumAttachments() )
        modelInfo.RemoveAttachmentAtIndex( attachment_index ); //todocycle: weapon cycling code
    
    AddAttachment( "rHandItem", nextAttachmentName, 1 );
}

void Eugene::Animate( void ) {
    auto animInfo = modelInfo.GetAnimInfo();
    if ( ! animInfo )
        return;

    animInfo->Animate( anims, GetAnimSpeed() );
}

void Eugene::DoStomp( void ) {
             
    const std::shared_ptr< const DamageInfo > damage = damageManager.Get( dmg_eugeneStomp.c_str() );
    if ( !damage ) {
        return;
    }
    
    Vec3f angles = bounds.primitive->GetAngles();
    Mat3f rot( angles );
    
    Vec3f stomp_origin = stompOffset * rot;
    stomp_origin += GetOrigin();
    
    DamageZone( Primitive_Sphere( Sphere( stomp_origin, stompRadius ) ), stompMaxTargets, *damage, GetWeakPtr(), DamageFalloffT::Uniform );
    
    stomp_origin.z = DEFAULT_STOMP_EFFECT_HEIGHT;
    SpawnStompEffect( stomp_origin );

    return;
}

void Eugene::DoMelee( void ) {
    
    const std::shared_ptr< const DamageInfo > damage = damageManager.Get( dmg_eugeneMelee.c_str() );
    
    if ( !damage ) {
        return;
    }
    
    Vec3f angles = bounds.primitive->GetAngles();
    Mat3f rot( angles );
    Vec3f melee_origin = ( meleeOffset - ( meleeRadius * 2 ) ) * rot;
    melee_origin += GetOrigin();
    
    std::vector< CollisionItem > items;    
    DamageZone( Primitive_Sphere( Sphere( melee_origin, meleeRadius ) ), meleeMaxTargets, *damage, GetWeakPtr(), DamageFalloffT::Uniform, &items );
    if ( items.size() <= 1 )
        return;
        
    if ( auto ent = items[0].ent.lock() ) {
        melee_effect.reset();
        SpawnMeleeEffect( ent );
    }

    if constexpr ( attackCollisionDebug ) {
        if ( globalVals.GetBool( gval_d_attackCollision ) ) {
            globalVals.SetBool( gval_v_pauseAfterRender, true );
            renderer->DrawSphere( 12, meleeRadius, melee_origin );
        }
    }
}

void Eugene::SpawnStompEffect( const Vec3f& effectOrigin ) const {
    //TODO: Replace this with an actual effect (particles, etc)?
    /* todo:jesse:entsets:fixit
    if ( fx_stomp == "stompEffect" ) {
    if ( stompEffect_ptr ) {
        stompEffect_ptr->SetRadius( stompRadius );
        std::shared_ptr< StompEffect > stompEffect_ptr = std::static_pointer_cast< StompEffect > (entityInfoManager.LoadAndSpawn( fx_stomp.c_str(), effectOrigin ));
        
        if ( stompEffect_ptr )
            stompEffect_ptr->SetRadius( stompRadius );
    }
    */
}

void Eugene::SpawnMeleeEffect( const std::shared_ptr< const Entity > target ) {
    if ( !melee_effect ) {
        melee_effect = std::make_shared<Sprite>(DEFAULT_MELEE_EFFECT_EDGE_LENGTH,DEFAULT_MELEE_EFFECT_EDGE_LENGTH);
        melee_effect->SetMaterial( "eugeneNewspaperSplat" );
        melee_effect->SetupUV();
    }
    
    const auto & mdl = target->GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;
    
    Vec3f melee_effect_origin;
    melee_effect_origin.x -= melee_effect->GetWidth() / 2;
    melee_effect_origin.z = height + melee_effect->GetHeight();
    melee_effect_origin += target->GetModelOffset();
    
    melee_effect->SetOriginOffset( melee_effect_origin );
    
    melee_effect_origin += target->GetOrigin();
    
    melee_effect->SetOrigin( melee_effect_origin );
        
    game->AddWorldUI( melee_effect );
}

bool Eugene::IsReadyToSpecial2( void ) const {
    return true;
}

AnimStateT Eugene::GetAnimStateFromName( const std::string& animStateName ) const {
    if ( animStateName == "readyAttackWithSprayer" )
        return ANIMSTATE_EUGENE_READY_ATTACK_WITH_SPRAYER;

    if ( animStateName == "attackLoopWithSprayer" )
        return ANIMSTATE_EUGENE_ATTACK_WITH_SPRAYER;
      
    if ( animStateName == "attackLeftRecoverWithNewspaper" )
        return ANIMSTATE_EUGENE_ATTACK_LEFT_RECOVER_WITH_NEWSPAPER;

    if ( animStateName == "attackRightRecoverWithNewspaper" )
        return ANIMSTATE_EUGENE_ATTACK_RIGHT_RECOVER_WITH_NEWSPAPER;

    if ( animStateName == "attackLeftWithNewspaper" )
        return ANIMSTATE_EUGENE_ATTACK_LEFT_WITH_NEWSPAPER;

    if ( animStateName == "attackRightWithNewspaper" )
        return ANIMSTATE_EUGENE_ATTACK_RIGHT_WITH_NEWSPAPER;

    return Actor::GetAnimStateFromName( animStateName );
}

void Eugene::UpdateSleepMeterUI( void ) {
    if (!sleep_meter)
        SpawnSleepMeterUI();
        
    if ( GetSleepLevel() == 0 ) {
        sleep_meter.reset();
        return;
    }
    
    if ( IsAsleep() ) {
        sleep_meter->progress -= sleepRecoveryPerFrame;
    } else {
        sleep_meter->progress = GetSleepLevel_Normalized();
    }
    
    sleep_meter->SetOrigin( GetOrigin() + sleep_meter->GetOriginOffset() );
}

void Eugene::UpdateSprayerFuelMeterUI( void ) {
    if (!sprayerFuel_meter)
        SpawnSprayerFuelMeterUI();

    sprayerFuel_meter->progress = static_cast< float >( GetAmmoQuantity( AmmoTypeT::Gas ) ) / GetAmmoCapacity( AmmoTypeT::Gas );
        
    sprayerFuel_meter->SetOrigin( GetOrigin() + sprayerFuel_meter->GetOriginOffset() );
}

void Eugene::SpawnSleepMeterUI( void ) {
    
    if ( sleep_meter )
        return;
        
    const auto & mdl = GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;
        
    sleep_meter = std::make_shared< Meter >(DEFAULT_SLEEPMETER_UI_EDGE_LENGTH,DEFAULT_SLEEPMETER_UI_EDGE_LENGTH);
    sleep_meter->SetMaterial( "sleepEffectUI" );
    sleep_meter->SetMaskMaterial( "progressBarMask_vertical" );
    sleep_meter->SetMaskType( MeterMaskType::HARD_ALPHA );
    sleep_meter->SetupUV();
    
    Vec3f sleepMeterOrigin;
    sleepMeterOrigin.x -= sleep_meter->GetWidth() / 2;
    sleepMeterOrigin.z = height + sleep_meter->GetHeight();
    sleepMeterOrigin += GetModelOffset();
    
    sleep_meter->SetOriginOffset( sleepMeterOrigin );
    
    sleepMeterOrigin += GetOrigin();
    
    sleep_meter->SetOrigin( sleepMeterOrigin );
    
    auto sprite = std::static_pointer_cast< Sprite >(sleep_meter);
    game->AddWorldUI( sprite );
}

void Eugene::UpdateSleepLevel( void ) {

    const Uint32 gameTime = game->GetGameTime();
    
    int _sleepLevel = GetSleepLevel();
    
    if ( gameTime - GetLastTimeSleepAdded() > SLEEP_ADD_TIMER_THRESHOLD ) {
        _sleepLevel -= DEFAULT_SLEEP_RECOVERY_PER_FRAME_AWAKE;
        _sleepLevel = std::max( sleepLevel, 0 );
        SetSleepLevel( _sleepLevel );
    }
}

void Eugene::SpawnSprayerFuelMeterUI( void ) {
    if ( sprayerFuel_meter )
        return;
        
    if ( sprayerFuel_meter )
        return;
        
    const auto & mdl = GetModel();
    const float default_height = 1;
    const float height = mdl ? mdl->GetHeight() : default_height;

    sprayerFuel_meter = std::make_shared< Meter >(DEFAULT_SPRAYERFUEL_UI_WIDTH,DEFAULT_SPRAYERFUEL_UI_HEIGHT);
    sprayerFuel_meter->SetMaterial( "sprayMeterFullUI" );
    sprayerFuel_meter->SetMaskMaterial( "progressBarMask_horizontal" );
    sprayerFuel_meter->SetMaskType( MeterMaskType::DIMMER );
    sprayerFuel_meter->SetupUV();
    
    Vec3f sprayerFuelMeterOrigin;
    sprayerFuelMeterOrigin.x -= sprayerFuel_meter->GetWidth() / 2;
    sprayerFuelMeterOrigin.z = height + sprayerFuel_meter->GetHeight();
    sprayerFuelMeterOrigin += GetModelOffset();
    
    sprayerFuel_meter->SetOriginOffset( sprayerFuelMeterOrigin );
    
    sprayerFuelMeterOrigin += GetOrigin();
    
    sprayerFuel_meter->SetOrigin( sprayerFuelMeterOrigin );
    
    auto sprite = std::static_pointer_cast< Sprite >( sprayerFuel_meter );
    game->AddWorldUI( sprite );
}

AnimStateT Eugene::GetAnimStateForAttachment( const AnimStateT state ) const {
    // here we will add temporary animState values that we weren't able to do so elsewhere
    
    //todoanimb: be nice if we could switch ( attachment )
    
    //todoanimb: I think it's possible that if we switch attachments on the very last frame of an attack,
    // that the animstate will be set from attack to attack_with_whatever and won't unset the real attach
    // anim. test this? //todoanimb:checkmetoo
    
    
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wswitch"
    #pragma GCC diagnostic ignored "-Wswitch-enum"
    
        if ( HasAttachment( "rHandItem", "sprayer" ) ) {
            switch ( state ) {
                case AnimStateT::Idle: return ANIMSTATE_EUGENE_IDLE_WITH_SPRAYER;
                case AnimStateT::WalkForward: return ANIMSTATE_EUGENE_WALK_WITH_SPRAYER;
                case AnimStateT::RunForward: return ANIMSTATE_EUGENE_RUN_WITH_SPRAYER; 
                case ANIMSTATE_EUGENE_SLEEP: return ANIMSTATE_EUGENE_SLEEP_WITH_SPRAYER;
                case AnimStateT::Attack1: return ANIMSTATE_EUGENE_ATTACK_WITH_SPRAYER;
                case AnimStateT::Attack2: return ANIMSTATE_EUGENE_STOMP_WITH_SPRAYER;
                default: return state;
            }
        }
        
        else if ( HasAttachment( "rHandItem", "newspaper" ) ) {
            switch ( state ) {
                case AnimStateT::Idle: return ANIMSTATE_EUGENE_IDLE_WITH_NEWSPAPER;
                case AnimStateT::WalkForward: return ANIMSTATE_EUGENE_WALK_WITH_NEWSPAPER;
                case AnimStateT::RunForward: return ANIMSTATE_EUGENE_RUN_WITH_NEWSPAPER;
                case ANIMSTATE_EUGENE_SLEEP: return ANIMSTATE_EUGENE_SLEEP_WITH_NEWSPAPER;
                case AnimStateT::Attack1: return ANIMSTATE_EUGENE_ATTACK_LEFT_WITH_NEWSPAPER;
                case AnimStateT::Attack2: return ANIMSTATE_EUGENE_STOMP_WITH_NEWSPAPER;
                default: return state;
            }
        }
        
    #pragma GCC diagnostic pop
    
    return state;
}

std::size_t Eugene::GetAnimIndex( const AnimStateT state ) const {
    for ( uint i=0; i<anims.size(); ++i )
        if ( anims[i].animState == state )
            return i;
    return MAX_INT32;
}

void Eugene::PutToSleep( void ) {
    modelInfo.AddAnimState( ANIMSTATE_EUGENE_SLEEP );
}

void Eugene::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Actor::MapSave( saveFile, string_pool );
    static_assert( is_parent_of<Eugene>( get_typeid<Actor>() ) );

    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    MapSaveSetters( saveFile, string_pool, eugeneSetters );
}

void Eugene::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Actor::MapLoad( saveFile, string_pool );
    static_assert( is_parent_of<Eugene>( get_typeid<Actor>() ) );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    
    MapLoadSetters( saveFile, string_pool, eugeneSetters );
}
