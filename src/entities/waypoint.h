// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_ENTITIES_WAYPOINT_H
#define SRC_ENTITIES_WAYPOINT_H

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./entity.h"

/*!

Waypoint \n\n

Used for Actors' patrol routes

*/


class Waypoint : public Entity {
public:
    static constexpr TypeInfo<Waypoint, Entity> typeinfo{};
    TypeID GetObjectTypeID( void ) const override { return typeinfo.get_typeid(); }
    std::shared_ptr< Entity > CloneShared( void ) const override { using ret = std::remove_cv< std::remove_pointer<decltype(this)>::type >::type; return std::make_shared<ret>( *this ); }
    
public:
    Waypoint( void );
    Waypoint( const Waypoint& other );
    ~Waypoint( void ) override { };

public:
    Waypoint& operator=( const Waypoint& other ) = default;

public:
    void Spawn( void ) override;
    bool SetModel( const std::shared_ptr< const Model >& mod ) override;

// ** ToolTips
public:
    static_assert( is_parent_of<Waypoint>( get_typeid<Entity>() ) );
    static ToolTip< Waypoint, Entity > editor_tooltips;
    void AddClassToolTip( const char* key, const char* val ) const override { editor_tooltips.Add( key, val ); }
    const std::string GetClassToolTip( const char* key ) const override { return editor_tooltips.Get( key ); }
};

#endif // SRC_ENTITIES_WAYPOINT_H
