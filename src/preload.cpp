// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./preload.h"

#include "./game.h"
#include "./sound.h"
#include "./damage/damageManager.h"
#include "./particles/effectsManager.h"
#include "./rendering/models/modelManager.h"
#include "./entities/entityInfoManager.h"
#include "./entities/entityInfo.h"
#include "./entities/entity.h"

//scaleme
HashList< std::shared_ptr< const DamageInfo > > Preload::preloaded_damage;
HashList< std::shared_ptr< const Effect > > Preload::preloaded_effects;
HashList< std::shared_ptr< const EntityInfo > > Preload::preloaded_entityInfo;
HashList< std::shared_ptr< const Entity > > Preload::preloaded_entities;
HashList< std::shared_ptr< const SoundData > > Preload::preloaded_sounds;
HashList< std::shared_ptr< const Model > > Preload::preloaded_models;

void Preload::PreloadDamage( const std::string& name ) {
    PreloadAsset( preloaded_damage, damageManager, name );
}

void Preload::PreloadEffect( const std::string& name ) {
    PreloadAsset( preloaded_effects, effectsManager, name );
}

void Preload::PreloadModel( const std::string& name ) {
    PreloadAsset( preloaded_models, modelManager, name );
}

void Preload::PreloadSound( const std::string& name ) {
    PreloadAsset( preloaded_sounds, soundManager, name );
}

void Preload::PreloadEntity( const std::string& name ) {
    if ( name.size() < 1 )
        return;

    auto const ent_info = entityInfoManager.Get( name.c_str() );
    if ( ! ent_info )
        return;
        
    PreloadEntityInfo( ent_info );
}

void Preload::PreloadEntityInfo( const std::shared_ptr< const EntityInfo >& ent_info ) {
    if ( preloaded_entityInfo.Update( ent_info->GetEntName().c_str(), ent_info ) )
        PreloadEntity( ent_info );
}

void Preload::PreloadEntity( const std::shared_ptr< const EntityInfo >& ent_info ) {
    std::shared_ptr< Entity > ent = game->NewUnmanagedEnt( ent_info->GetObjectTypeID() );
    if ( !ent )
        return;
    
    std::shared_ptr< const Entity > cast = std::static_pointer_cast< const Entity >( ent );
    ASSERT( cast );
    if ( preloaded_entities.Update( ent_info->GetEntName().c_str(), cast ) ) {
        ent->LoadDefaults( *ent_info );
        ent->PreloadAssets();
    }
}

void Preload::ClearForShutdown( void ) {
    Clear();
}

void Preload::Clear( void ) {
    preloaded_damage.Clear();
    preloaded_effects.Clear();
    preloaded_entityInfo.Clear();
    preloaded_entities.Clear();
    preloaded_sounds.Clear();
    preloaded_models.Clear();
}
