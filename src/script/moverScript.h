// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_SCRIPT_MOVERSCRIPT_H_
#define SRC_SCRIPT_MOVERSCRIPT_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"

typedef uchar MoveOpTypeT_BaseType;
enum class MoveOpTypeT : MoveOpTypeT_BaseType {
    NONE=0,
    MOVE=1,
    MOVE_LOCAL=2,
    ROTATE_OWN_ORIGIN=3, //!< rotate around own origin
    ROTATE_SPECIFIED_ORIGIN=4,
    RESET=5 //!< re-execute the script
};

struct MoveOp {
    MoveOp( void );
    MoveOp( const MoveOp& other );
    MoveOp( MoveOp&& other_rref );
    ~MoveOp( void );

    MoveOp& operator=( const MoveOp& other );
    MoveOp& operator=( MoveOp&& other_rref );

    void SetMove( const Uint32 depart, const Uint32 arrive, const Vec3f& mv );
    void SetMoveLocal( const Uint32 depart, const Uint32 arrive, const Vec3f& mv );
    void SetRotate( const Uint32 depart, const Uint32 arrive, const Vec3f& axis );
    void SetRotate( const Uint32 depart, const Uint32 arrive, const Vec3f& axis, const Vec3f& origin );
    void SetReset( const Uint32 depart, const Uint32 time );

    void Clear( void );

    Uint32 time_depart;
    Uint32 time_arrive;

    MoveOpTypeT GetMoveOpType( void ) const;
    void GetMoveOrigin( Vec3f& out ) const;
    void GetRotationAxis( Vec3f& out ) const;
    void GetRotationOrigin( Vec3f& out ) const;

private:
    MoveOpTypeT type;
    Vec3f *data;
};

class MoverScript : public File {
public:
    bool Load( const char* path, LinkList< MoveOp >& moveOps );
    bool Load( LinkList< MoveOp >& moveOps );
};

#endif // SRC_SCRIPT_MOVERSCRIPT_H_
