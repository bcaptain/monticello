// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./moverScript.h"

MoveOp::MoveOp( void )
    : time_depart(0)
    , time_arrive(0)
    , type(MoveOpTypeT::NONE)
    , data(nullptr)
{ }

MoveOp::MoveOp( const MoveOp& other )
    : time_depart(other.time_depart)
    , time_arrive(other.time_arrive)
    , type(other.type)
    , data(nullptr)
{
    switch ( type ) {
        case MoveOpTypeT::NONE:
            break;
        case MoveOpTypeT::RESET: FALLTHROUGH;
        case MoveOpTypeT::MOVE: FALLTHROUGH;
        case MoveOpTypeT::MOVE_LOCAL: FALLTHROUGH;
        case MoveOpTypeT::ROTATE_OWN_ORIGIN:
            data = new Vec3f;
            *data = *other.data; // rotation axis (or in the case of MOVE, the move-to origin)
            break;
        case MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN:
            data = new Vec3f[2];
            data[0] = other.data[0]; // rotation axis
            data[1] = other.data[1]; // rotation origin
            break;
        default:
            ASSERT( false );
            break;
    }
}

MoveOp::MoveOp( MoveOp&& other_rref )
    : time_depart(other_rref.time_depart)
    , time_arrive(other_rref.time_arrive)
    , type(other_rref.type)
    , data(other_rref.data)
{
    other_rref.data = nullptr;
}

MoveOp& MoveOp::operator=( MoveOp&& other_rref ) {
    type = other_rref.type;
    time_depart = other_rref.time_depart;
    time_arrive = other_rref.time_arrive;
    Vec3f* tmp = data;
    data = other_rref.data;
    other_rref.data = tmp;

    return *this;
}

MoveOp& MoveOp::operator=( const MoveOp& other ) {
    //todo: set data? timeArrive/Depart?
    switch ( other.type ) {
        case MoveOpTypeT::NONE:
            Clear();
            break;
        case MoveOpTypeT::MOVE_LOCAL:
            FALLTHROUGH;
        case MoveOpTypeT::MOVE:
            SetMove( other.time_depart, other.time_arrive, *other.data );
            break;
        case MoveOpTypeT::RESET:
            SetReset( other.time_depart, static_cast< Uint32 >( other.data->x ) );
            break;
        case MoveOpTypeT::ROTATE_OWN_ORIGIN:
            SetRotate( other.time_depart, other.time_arrive, *other.data );
            break;
        case MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN:
            SetRotate( other.time_depart, other.time_arrive, other.data[0], other.data[1] );
            break;
        default:
            ASSERT( false );
            break;
    }

    return *this;
}

MoveOp::~MoveOp( void ) {
    Clear();
}

void MoveOp::Clear( void ) {
    time_depart = 0;
    time_arrive = 0;
    switch ( type ) {
        case MoveOpTypeT::NONE:
            return;
        case MoveOpTypeT::MOVE_LOCAL: FALLTHROUGH;
        case MoveOpTypeT::MOVE: FALLTHROUGH;
        case MoveOpTypeT::RESET: FALLTHROUGH;
        case MoveOpTypeT::ROTATE_OWN_ORIGIN:
            DELNULL( data );
            break;
        case MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN:
            DELNULLARRAY( data );
            break;
        default:
            ASSERT( false );
            break;
    }
    type = MoveOpTypeT::NONE;
}

void MoveOp::SetMove( const Uint32 depart, const Uint32 arrive, const Vec3f& mv ) {
    if ( type == MoveOpTypeT::MOVE ) {
        *data = mv;
    } else {
        Clear();
        data = new Vec3f( mv );
        type = MoveOpTypeT::MOVE;
    }

    time_depart = depart;
    time_arrive = arrive;
}

void MoveOp::SetMoveLocal( const Uint32 depart, const Uint32 arrive, const Vec3f& mv ) {
    if ( type == MoveOpTypeT::MOVE_LOCAL ) {
        *data = mv;
    } else {
        Clear();
        data = new Vec3f( mv );
        type = MoveOpTypeT::MOVE_LOCAL;
    }

    time_depart = depart;
    time_arrive = arrive;
}

void MoveOp::SetRotate( const Uint32 depart, const Uint32 arrive, const Vec3f& axis ) {
    if ( type == MoveOpTypeT::ROTATE_OWN_ORIGIN ) {
        *data = axis;
    } else {
        Clear();
        data = new Vec3f( axis );
        type = MoveOpTypeT::ROTATE_OWN_ORIGIN;
    }

    time_depart = depart;
    time_arrive = arrive;
}

void MoveOp::SetRotate( const Uint32 depart, const Uint32 arrive, const Vec3f& axis, const Vec3f& origin ) {
    if ( type == MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN ) {
        data[0] = axis;
        data[1] = origin;
    } else {
        Clear();
        data = new Vec3f[2]{ axis, origin };
        type = MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN;
    }

    time_depart = depart;
    time_arrive = arrive;
}

void MoveOp::SetReset( const Uint32 depart, const Uint32 time ) {
    if ( type == MoveOpTypeT::RESET ) {
        data->x = time;
    } else {
        Clear();
        data = new Vec3f{ static_cast<float>( time ), 0, 0 };
        type = MoveOpTypeT::RESET;
    }

    time_depart = depart;
    time_arrive = depart; // intentional, arrival time is ignored
}

MoveOpTypeT MoveOp::GetMoveOpType( void ) const {
    return type;
}

void MoveOp::GetMoveOrigin( Vec3f& out ) const {
    ASSERT( data );
    ASSERT( type == MoveOpTypeT::MOVE || type == MoveOpTypeT::MOVE_LOCAL );
    out = *data;
}

void MoveOp::GetRotationAxis( Vec3f& out ) const {
    ASSERT( data );
    ASSERT( type == MoveOpTypeT::ROTATE_OWN_ORIGIN || type == MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN );
    out = *data;
}

void MoveOp::GetRotationOrigin( Vec3f& out ) const {
    ASSERT( data );
    ASSERT( type == MoveOpTypeT::ROTATE_SPECIFIED_ORIGIN );
    out = data[1];
}

bool MoverScript::Load( LinkList< MoveOp >& moveOps ) {
    if ( ! IsOpen() ) {
        ERR("Couldn't open map script: %s\n", GetFilePath().c_str() );
        return false;
    }

    moveOps.DelAll();

    ParserSSV parser( *this );
    parser.SetCommentIndicator('#');

    std::string tmp;
    std::string action;
    Vec3f vec_val, vec_val2;
    uint time_depart;
    uint time_arrive;
    MoveOp* op;

    while ( true ) {
        if ( ! parser.ReadWord( tmp, true ) ) {
            break; // end of file
        }
        time_depart = String::ToUInt( tmp );

        if ( ! parser.ReadWord( tmp, true ) ) {
            break; // end of file
        }
        time_arrive = String::ToUInt( tmp );

        if ( ! parser.ReadWordOnLine( action ) ) {
            Close();
            ERR("Mapscript: Parse Error: Couldn't read/find action string in script %s:%u at time '%s'\n", GetFilePath().c_str(), parser.GetLineNumber(), tmp.c_str() );
            return false;
        }

        if ( action == "move" ) {
            parser.ReadVec3f( vec_val );
            op = &moveOps.Append()->Data();
            op->SetMove( time_depart, time_arrive, vec_val );
        } else if ( action == "move_local" ) {
            parser.ReadVec3f( vec_val );
            op = &moveOps.Append()->Data();
            op->SetMoveLocal( time_depart, time_arrive, vec_val );
        } else if ( action == "rotate_local" ) {
            parser.ReadVec3f( vec_val );
            op = &moveOps.Append()->Data();
            op->SetRotate( time_depart, time_arrive, vec_val );
        } else if ( action == "rotate" ) {
            parser.ReadVec3f( vec_val );
            parser.ReadVec3f( vec_val2 );
            op = &moveOps.Append()->Data();
            op->SetRotate( time_depart, time_arrive, vec_val, vec_val2 );
        } else if ( action == "reset" ) {
            op = &moveOps.Append()->Data();
            parser.ReadFloat( vec_val2.x );
            op->SetReset( time_depart, static_cast< Uint32 >( vec_val2.x ) );
        } else {
            ERR("Mapscript: Unknown action (%s) in script %s:%u\n", action.c_str(), GetFilePath().c_str(), parser.GetLineNumber() );
            Close();
            return false;
        }

        parser.SkipWhitespace( true );
        if ( EndOfFile() ) {
            break;
        }

        // detect trailing garbage
        if ( !parser.AtBeginningOfLine() ) {
            uint line = parser.GetLineNumber();
            std::string garbage;
            parser.ReadUnquotedString( garbage );
            ERR("Mapscript: Trailing garbage at end of line in script %s:%u, garbag is: %s\n", GetFilePath().c_str(), line, garbage.c_str() );
            Close();
            return false;
        }
    }

    CMSG_LOG("Loaded mover_script: %s from %s\n", GetFilePath().c_str(), GetFileContainerName_Full().c_str() );

    return true;
}

bool MoverScript::Load( const char* path, LinkList< MoveOp >& moveOps ) {
    return Open( path ) && Load( moveOps );
}
