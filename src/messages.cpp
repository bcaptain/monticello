// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./messages.h"

uint Messages::errors = 0;
uint Messages::warnings = 0;

void Messages::Error( const char* str ) {
    Message< HistoryDisplayT::MSG_ERROR >( str );
}

void Messages::Warning( const char* str ) {
    Message< HistoryDisplayT::WARNING >( str );
}

void Messages::Message( const char* str ) {
    Message< HistoryDisplayT::MESSAGE >( str );
}
