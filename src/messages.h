// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MESSAGES_H_
#define SRC_MESSAGES_H_

#include "./base/main.h"
#include "./console.h"

class Game;
class Console;
extern Game* game;
extern Console* console;

typedef Uint8 HistoryDisplayT_BaseType;
enum class HistoryDisplayT : HistoryDisplayT_BaseType;

//! hook functions, and variables, for putting messages into the game console. They hook to CLib's ERR, WARN, etc
namespace Messages {
    extern uint errors;
    extern uint warnings;
    
    template< HistoryDisplayT MESSAGE_TYPE >
    void Message( const char* str ) {
        ++errors;
        if ( console ) {
            console->Append( str, MESSAGE_TYPE );
        } else if ( game ) {
            game->preConsoleMessages.emplace_back( str, MESSAGE_TYPE );
        }
    }
    
    void Error( const char* str );
    void Warning( const char* str );
    void Message( const char* str );
}

#endif  // SRC_MESSAGES_H_
