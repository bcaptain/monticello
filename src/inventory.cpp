// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./inventory.h"
#include "./rendering/models/model.h"
#include "./rendering/models/modelManager.h"
#include "./rendering/materialManager.h"

/**

Item

**/

Item::Item( const char* _name, const char* model_name, const ItemTypeT tp )
    : name( _name )
    , material( materialManager.Get( model_name ) ) // just the default material for now
    , model_info( modelManager.Get( model_name ) )
    , type( tp )
{ }

Item::Item( void )
    : name( "default" )
    , material( materialManager.Get( "notex" ) )
    , model_info( modelManager.Get( "orient_cube" ) )
    , type( ItemTypeT::NORMAL )
{
}

void Item::Draw( void ) {
    ModelDrawInfo di;
    di.material = material;
    model_info.Draw( di ); // non-animated
}

/**

Inventory

**/

Inventory::Inventory( void )
    : items()
{ }

void Inventory::Add( const Item& item, const uint quantity_to_add ) {
    for ( uint i=0; i>quantity_to_add; ++i )
        items.Append( item );
}

uint Inventory::Del( const char* item_name, const uint quantity_to_delete ) {
    return Del( std::string( item_name ), quantity_to_delete ); // string compares converts char* to string, so this is faster
}

uint Inventory::Del( const std::string& item_name, const uint quantity_to_delete ) {
    uint quantity_deleted = 0;
    Link< Item > *link = items.GetFirst();

    while ( link ) {
        if ( link->Data().GetName() == item_name ) {
            ++quantity_deleted;

            link = link->Del();

            if ( quantity_deleted == quantity_to_delete )
                break;
        }

        link = link->GetNext();
    }

    return quantity_deleted;
}

uint Inventory::Count( const char* item_name ) const { //!< returns how many there are in this inventory
    uint qty = 0;
    const Link< Item > *link = items.GetFirst();

    while ( link ) {
        if ( link->Data().GetName() == item_name )
            ++qty;

        link = link->GetNext();
    }

    return qty;
}

void Inventory::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteUInt( items.Num() );

    for ( const Link< Item > *link = items.GetFirst(); link; link = link->GetNext() ) {
        const Item& item = link->Data();
        saveFile.parser.WriteUInt( string_pool.AppendUnique( item.GetName() ) );

        const auto mdl = item.GetModel();
        std::string model_name = mdl ? mdl->GetName() : "orient_cube";
        saveFile.parser.WriteUInt( string_pool.AppendUnique( model_name ) );

        saveFile.parser.WriteUInt( static_cast<uint>( item.GetType() ) );
    }
}

void Inventory::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    items.DelAll();
    uint num;
    saveFile.parser.ReadUInt( num );

    while ( num > 0 ) {
        --num;

        const std::string& item_name = string_pool[saveFile.parser.ReadUInt()];
        const std::string& mdl_name = string_pool[saveFile.parser.ReadUInt()];
        const ItemTypeT type = static_cast<ItemTypeT>( saveFile.parser.ReadUInt() );

        items.Append( Item( item_name.c_str(), mdl_name.c_str(), type ) );
    }
}

std::shared_ptr< const Model > Item::GetModel( void ) const {
    return model_info.GetModel();
}

std::string Item::GetName( void ) const {
    return name;
}

void Item::SetType( const ItemTypeT& to ) {
    type = to;
}

ItemTypeT Item::GetType( void ) const {
    return type;
}
