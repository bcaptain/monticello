// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MISC_H_
#define SRC_MISC_H_

#include "./base/main.h"

class Entity;
struct CollisionItem;

Line3f GetPickLine( void );
Line3f GetPickLine( Vec3f mouse_pos );

std::shared_ptr<Entity> GetPlayerIfInList( const std::vector< CollisionItem >& list );

#ifdef MONTICELLO_EDITOR
void debugOutput( void );
void debug_shatter_flat( void );
void debug_merge_models( void );
void debug_print_entity_indices( void );

void GetGroupOrigin( const LinkList< std::weak_ptr< Entity> > &ent_list, Vec3f& origin_out ); //!< retrieves the center of all objects (based on their origins, not bounds)
#endif // MONTICELLO_EDITOR

void SnapOrigin( Vec3f& origin, const float to_meters );

bool VerifyArgNum( const char* func_name, const std::vector< std::string >& args, const uint expected_num ); // returns false and prints error if the number of arguments is not expected_num

void SortVerts( std::vector< Vec3f >& verts, const Vec3f& axis ); //!< Sorts counter-clockwise around the given axis. negate axis for clockwise.

void DrawLine( const Vec3f& start, const Vec3f& end, const char* name );
void DrawPoint( const Vec3f& p, const char* name );
void DrawPoint( const float x, const float y, const float z, const char* name );

bool Unproject( const Vec3f& winCoords, const Mat4f& modelMatrix, const Mat4f& projMatrix, const int viewport[4], Vec3f& point_out );

#endif  // SRC_MISC_H_
