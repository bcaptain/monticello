// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_INVENTORY_H
#define SRC_INVENTORY_H

#include "./clib/src/warnings.h"
#include "./base/main.h"
#include "./rendering/models/modelInfo.h"

class Model;

typedef Uint8 ItemTypeT_BaseType;
enum class ItemTypeT : ItemTypeT_BaseType {
    NORMAL
};

/*!

Item \n\n

A generic class for items to give to actors

**/

class Item {
public:
    Item( void ); //!< Do not call this function. It is not deleted because a template class depends on it for compilation, but will ASSERT(false) if called
    Item( const char* _name, const char* model_name, const ItemTypeT tp = ItemTypeT::NORMAL );
    ~Item( void ) = default;
    Item( const Item& other ) = default;
    //todo: move ctor/op=

public:
    std::string GetName( void ) const;
    std::shared_ptr< const Model > GetModel( void ) const;
    void Draw( void );
    void SetType( const ItemTypeT& to );
    ItemTypeT GetType( void ) const;

public:
    Item& operator=( const Item& other ) = default;

private:
    std::string name;
    std::shared_ptr< const Material > material;
    ModelInfo model_info;
    ItemTypeT type;
};

/*!

Inventory \n\n

A nicely encapsulated linklist of items an actor has

**/

class Inventory {
public:
    Inventory( void );
    ~Inventory( void ) = default;
    Inventory( const Inventory& other ) = default;

public:
    Inventory& operator=( const Inventory& other ) = default;

public:
    void Add( const Item& item );
    void Add( const Item& item, const uint quantity_to_add );
    uint Del( const std::string& item_name, const uint quantity_to_delete ); //!< returns how many were deleted
        uint Del( const char* item_name, const uint quantity_to_delete ); //!< Less efficient overlaod of Del, must construct string from item_name. returns how many were deleted
    uint Count( const char* item_name ) const;

    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool );

    Link< Item >* GetFirst( void );
    const Link< Item >* GetFirst( void ) const;

private:
    LinkList< Item > items;
};

// **
//
// Inventory
//
// **

inline const Link< Item >* Inventory::GetFirst( void ) const {
    return items.GetFirst();
}

inline Link< Item >* Inventory::GetFirst( void ) {
    return items.GetFirst();
}

inline void Inventory::Add( const Item& item ) {
    items.Append( item );
}

#endif  // SRC_INVENTORY_H
