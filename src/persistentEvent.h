// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_PERSISTENT_EVENT_H_
#define SRC_PERSISTENT_EVENT_H_

#include "./base/main.h"
#include "./game.h"

template< typename TYPE >
class PersistentEvent {
public:
    PersistentEvent( void );
    PersistentEvent( const TYPE& _thing );
    PersistentEvent( TYPE&& _thing );
    ~PersistentEvent( void ) = default;
    
public:
    PersistentEvent( const PersistentEvent& other ) = default;
    PersistentEvent( PersistentEvent&& other_uref );
    
public:
    PersistentEvent& operator=( const PersistentEvent& other ) = default;
    PersistentEvent& operator=(  PersistentEvent&& other_uref );

public:
    void SetDelay( const Uint32 _delay ) { delay = _delay; }
    void SetDuration( const Uint32 _duration ) { duration = _duration; }
    void UpdateTime( const Uint32 cur_time ) { prev = cur_time; }
    bool IsTimeUp( const Uint32 cur_time ) const { return cur_time - prev > delay; }
    bool IsExpired( const Uint32 cur_time ) const { return ( duration > 0 ) && ( cur_time - start > duration ); }
    Uint32 GetPrev( void ) const { return prev; }
    Uint32 GetDelay( void ) const { return delay; }
    Uint32 GetDuration( void ) const { return duration; }
    
public:
    TYPE data;
    
private:
    Uint32 start;
    Uint32 prev;
    Uint32 delay;
    Uint32 duration;
};

template< typename TYPE >
PersistentEvent<TYPE>::PersistentEvent( void )
    : data()
    , start( game->GetGameTime() )
    , prev( start )
    , delay(0)
    , duration(0)
{ }

template< typename TYPE >
PersistentEvent<TYPE>::PersistentEvent( const TYPE& _data )
    : data(_data)
    , start( game->GetGameTime() )
    , prev( start )
    , delay(0)
    , duration(0)
{ }

template< typename TYPE >
PersistentEvent<TYPE>::PersistentEvent( TYPE&& _data_uref )
    : data( std::forward<TYPE>( _data_uref ) )
    , start( game->GetGameTime() )
    , prev( start )
    , delay(0)
    , duration(0)
{ }

template< typename TYPE >
PersistentEvent<TYPE>::PersistentEvent( PersistentEvent&& other_uref )
    : data( std::forward<TYPE>( other_uref.data ) )
    , start( other_uref.start )
    , prev( other_uref.prev )
    , delay( other_uref.delay )
    , duration( other_uref.duration )
{ }

template< typename TYPE >
PersistentEvent<TYPE>& PersistentEvent<TYPE>::operator=( PersistentEvent&& other_uref ) {
    data = std::forward<TYPE>( other_uref.data );
    start = other_uref.start;
    prev = other_uref.prev;
    delay = other_uref.delay;
    duration = other_uref.duration;
    return *this;
}

#endif  // SRC_PERSISTENT_EVENT_H_
