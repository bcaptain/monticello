// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_LASER_H
#define SRC_LASER_H

#include "./clib/src/warnings.h"
#include "./base/main.h"

extern const Vec3f DEFAULT_LASER_DIRECTION;

template< typename TYPE >
class VBO;

class Laser {
public:
    Laser( void );
    Laser( const float _length, const float _width, const Vec3f& _color );
    ~Laser( void );
    Laser( const Laser& other );
private:
    void Construct( void );

public:
    Laser& operator=( const Laser& other );

public:
    void SetLength( const float to );
    float GetLength( void ) const;
    void SetOrigin( const Vec3f& to );
    void SetOrigin( const float x, const float y, const float z );

    void DrawVBO( void ) const;
    void PackVBO( const float what_length );

    void SetFade( const bool whether );
    void UpdateVBO( void );

    void SetIntensity( const float to );

private:
    static void PackCoordVBO( void );

private:
    Vec3f origin;
    Vec3f dir;
    Vec3f color;
    VBO<float>* vbo_vert;
    float length;
    float cap_length;
    float width;
    float intensity; //!< scale from 0 to 1

    bool fade;

    static VBO<float>* vbo_coord;
    static uint num_lasers_in_world;
};

#endif  // SRC_LASER_H
