// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./ballistic.h"
#include "./game.h"
#include "./entities/entity.h"
#include "./rendering/camera.h"
#include "./rendering/renderer.h"
#include "./entities/actor.h"
#include "./commandsys.h"
#include "./damage/damage.h"
#include "./damage/damageManager.h"

std::shared_ptr< Entity > Ballistic::Fire( const Vec3f& origin, const Vec3f& direction_normalized, const std::string& damageInfo, const float range, const std::weak_ptr< Entity >& originator ) {
    if ( range <= 0 )
        return nullptr;

    std::shared_ptr< const DamageInfo > damage = damageManager.Get( damageInfo.c_str() );
    if ( ! damage ) {
        ERR("Ballistic::Fire: Unable to find damageInfo: %s\n", damageInfo.c_str());
        return nullptr;
    }

    auto primitive = Primitive_Point(origin);
    CollisionReport3D report;
    SweepData coldata;
    coldata.opts |= MATCH_NEAREST;
    coldata.vel = direction_normalized * range;
    
    std::shared_ptr< Entity > hit = GlobalSweep( report, coldata, primitive );
    if ( hit ) {
        damage->Inflict( *hit, originator.lock() );
    }

    constexpr const bool ballistic_debug = false;
    if constexpr ( ballistic_debug ) {
        const Vec3f destination( origin + coldata.vel );
        
        CommandSys::Execute( "delallshapes" );
        CommandSys::Execute( {"point", "sstart", String::VecToString( origin )} );
        CommandSys::Execute( {"point", "end", String::VecToString( destination )} );
        CommandSys::Execute( {"line", String::VecToString( origin ), String::VecToString( destination )} );
    }

    return hit;
}

std::shared_ptr< Entity > Ballistic::PlayerFire( const std::string& damageInfo, const float range, const std::weak_ptr< Entity >& originator ) {
    if ( range <= 0 )
        return nullptr;

    std::shared_ptr<Actor> player = game->GetPlayer();
    if ( ! player ) {
        ERR("Ballistic::PlayerFire: no player in the game\n");
        return nullptr;
    }

    const Vec3f camera_origin = renderer->camera.GetOriginAtMaximumZoom();
    const Vec3f bullet_origin = renderer->GetCrosshairOrigin();
    const Vec3f bullet_dir = Vec3f::GetNormalized( bullet_origin - camera_origin );
    return Fire( bullet_origin, bullet_dir, damageInfo, range, originator );
}

