// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CONSOLE_H_
#define SRC_CONSOLE_H_

#include "./ui/ui.h"

class Console;
extern Console* console;

extern const uint CONSOLE_DISPLAY_LINES;
extern const uint CONOLE_HISTORY_LINES;
extern const Vec3f CONSOLE_HISTORY_COMMAND_COLOR;
extern const Vec3f CONSOLE_HISTORY_MESSAGE_COLOR;
extern const Vec3f CONSOLE_HISTORY_WARNING_COLOR;
extern const Vec3f CONSOLE_HISTORY_ERROR_COLOR;
extern const Vec3f CONSOLE_FONT_COLOR;

typedef Uint8 HistoryDisplayT_BaseType;
enum class HistoryDisplayT : HistoryDisplayT_BaseType {
    COMMAND=0, //default
    MESSAGE=1,
    WARNING=2,
    MSG_ERROR=3
};

class HistoryElement {
public:
    HistoryElement( void );
    explicit HistoryElement( const HistoryDisplayT display_type );
    HistoryElement( const std::string& str, const HistoryDisplayT display_type );
    HistoryElement( const char* str, const HistoryDisplayT display_type );

public:
    HistoryDisplayT display;
    std::string string;
};

class Console : public TextBox {
public:
    Console( void );
    ~Console( void );
    Console( const Console& other ) = delete;

public:
    Console& operator=( const Console& other ) = delete;

public:
    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;
protected:
    void pack_vbo( void ) const;

public:
    void Init( void );
    void ClearHistory( void );
    void Append( const std::string& str, const HistoryDisplayT display_type = HistoryDisplayT::COMMAND );
    void Append( const char* str, const HistoryDisplayT display_type = HistoryDisplayT::COMMAND );

    void PressEnter( void ) override;
    void PressUp( void ) override;
    void PressDown( void ) override;
    void PressPageUp( void ) override;
    void PressPageDown( void ) override;
    void PressLeft( void ) override;
    void PressRight( void ) override;

    static void PrintFloats( const float* flt, const std::size_t num_floats, const char* postfix_message = "" );
    static void PrintFloat( const float flt, const char* postfix_message = "" );

    void SetVisible( const bool whether ) override;

private:
    LinkList< HistoryElement > displayhistory;
    Link< HistoryElement >* firstDisplayLine;
    Link< HistoryElement >* historySelection;
    VBO<float>* vbo_textcolor;
    VBO<float>* vbo_textuv;
    VBO<float>* vbo_background;
    
    bool just_opened;
};

#endif  // SRC_CONSOLE_H_
