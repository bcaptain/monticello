// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>

#include <string>
#include <vector>

#include "./clib/src/warnings.h"
#include "./sdl.h"
#include "../sound.h"
#include "../rendering/renderer.h"

#include "../game.h"

// VBO funcs
PFNGLGENBUFFERSPROC glGenBuffers = nullptr;
PFNGLBINDBUFFERPROC glBindBuffer = nullptr;
PFNGLBUFFERDATAPROC glBufferData = nullptr;
PFNGLBUFFERSUBDATAPROC glBufferSubData = nullptr;
PFNGLDELETEBUFFERSPROC glDeleteBuffers = nullptr;
PFNGLMAPBUFFERPROC glMapBuffer = nullptr;
PFNGLUNMAPBUFFERPROC glUnmapBuffer = nullptr;

// Shader funcs
PFNGLCREATESHADERPROC glCreateShader = nullptr;
PFNGLSHADERSOURCEPROC glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC glCompileShader = nullptr;
PFNGLATTACHSHADERPROC glAttachShader = nullptr;
PFNGLCREATEPROGRAMPROC glCreateProgram = nullptr;
PFNGLLINKPROGRAMPROC glLinkProgram = nullptr;
PFNGLUSEPROGRAMPROC glUseProgram = nullptr;
PFNGLGETSHADERIVPROC glGetShaderiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog = nullptr;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog = nullptr;

OpenGLInfo::OpenGLInfo( void )
    : vendor(nullptr)
    , renderer(nullptr)
    , version(nullptr)
    , extensions()
    , redBits(0)
    , greenBits(0)
    , blueBits(0)
    , alphaBits(0)
    , depthBits(0)
    , stencilBits(0)
    , maxTextureSize(0)
    , maxLights(0)
    , maxAttribStacks(0)
    , maxClipPlanes(0)
    , maxTextureStacks(0)
    , maxFragUniform(0)
    , maxVertUniform(0)
    , vid_ram_total(0)
    , vid_ram_available_at_start(0)
    , video_card_brand( VideoCardBrandT::UNKNOWN )
    {
}

void OpenGLInfo::Init( void ) {
    CMSG_LOG("---- OpenGL Init ----\n");

    version = reinterpret_cast<const char*>( glGetString( GL_VERSION ) );

    if ( !version ) {
        DIE("Could not determine OpenGL version.\n");
        return;
    }

    if ( version[1] == '.' ) { // if OpenGL is not in the double digits
        char major[2];
        major[0] = version[0];
        major[1] = '\0';
        char minor[2];
        minor[0] = version[2];
        minor[1] = '\0';
        
        const int min_gl_major = 3;
        const int min_gl_minor = 0;
        
        if ( String::ToInt( major ) >= min_gl_major ) {
            ; // good
        } else if ( String::ToInt( major ) == min_gl_major ) {
            if ( String::ToInt( minor ) < min_gl_minor ) {
                DIE("You need OpenGL %i.%i or later. Yours is %s\n", min_gl_major, min_gl_minor, version);
                return;
            }
        }
    }

    vendor = reinterpret_cast<const char*>( glGetString( GL_VENDOR ) );
    renderer = reinterpret_cast<const char*>( glGetString( GL_RENDERER ) );
    char* allExtentions = new char[ strlen( reinterpret_cast<const char*>( glGetString( GL_EXTENSIONS ) ) ) + 1 ];
    strcpy( allExtentions, reinterpret_cast<const char*>( glGetString( GL_EXTENSIONS ) ) );

    const char* ext=strtok( allExtentions, " ");
    while ( ext != nullptr ) {
        extensions.Append( ext );
        ext = strtok( nullptr, " ");
    }

    DELNULLARRAY( allExtentions );

    extensions.SortData();

    // TODO: do we have to free/delete these?
    glGetIntegerv( GL_RED_BITS, &redBits );
    glGetIntegerv( GL_GREEN_BITS, &greenBits );
    glGetIntegerv( GL_BLUE_BITS, &blueBits );
    glGetIntegerv( GL_ALPHA_BITS, &alphaBits );
    glGetIntegerv( GL_DEPTH_BITS, &depthBits );
    glGetIntegerv( GL_STENCIL_BITS, &stencilBits );
    glGetIntegerv( GL_MAX_LIGHTS, &maxLights );
    glGetIntegerv( GL_MAX_TEXTURE_SIZE, &maxTextureSize );
    glGetIntegerv( GL_MAX_CLIP_PLANES, &maxClipPlanes );
    glGetIntegerv( GL_MAX_ATTRIB_STACK_DEPTH, &maxAttribStacks );
    glGetIntegerv( GL_MAX_TEXTURE_STACK_DEPTH, &maxTextureStacks );
    glGetIntegerv( GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &maxFragUniform );
    glGetIntegerv( GL_MAX_VERTEX_UNIFORM_COMPONENTS, &maxVertUniform );

    const std::string ati("ATI");
    const std::size_t ati_len = ati.size();

    const std::string nvidia("NVIDIA");
    const std::size_t nvidia_len = nvidia.size();

    const std::size_t len = String::Len( vendor );

    std::string extraction;

    for ( std::size_t i=0; i<len; ++i ) {
        if ( i < len-ati_len ) {
            extraction = std::string( vendor ).substr( i, ati_len );
            if ( extraction.compare( ati ) == 0 ) { // case Sensitive on purpose - ati will always be uppercase
                video_card_brand = VideoCardBrandT::ATI;
                break;
            }
        } else {
            break; // string is too short to be ATI or NVIDIA
        }

        if ( i < len-nvidia_len ) {
            extraction = std::string( vendor ).substr( i, nvidia_len );
            String::ToUpper( extraction );
            if ( extraction.compare( nvidia ) == 0 ) { // case INsensitive on purpose - unsure of capitalization but "nvidia" is long enough to not be accidental
                video_card_brand = VideoCardBrandT::NVIDIA;
                break;
            }
        }
    }

    // ** get video memory info
    if ( video_card_brand ==  VideoCardBrandT::UNKNOWN ) {
        WARN("Couldn't determine video hardware vendor, will not try to determine video ram.\n");

    } else if ( video_card_brand == VideoCardBrandT::ATI ) {
        // ** https://www.opengl.org/registry/specs/AMD/wgl_gpu_association.txt

        typedef uint(*wglGetGPUIDsAMDT_T)(uint maxCount, uint *ids);
        typedef int(*wglGetGPUInfoAMD_T)(uint id, int property, GLenum dataType, uint size, void *data);

        wglGetGPUIDsAMDT_T wglGetGPUIDsAMD = union_cast< wglGetGPUIDsAMDT_T >( SDL_GL_GetProcAddress("wglGetGPUIDsAMD") );
        wglGetGPUInfoAMD_T wglGetGPUInfoAMD = union_cast< wglGetGPUInfoAMD_T >( SDL_GL_GetProcAddress("wglGetGPUInfoAMD") );

        if ( wglGetGPUIDsAMD == nullptr || wglGetGPUInfoAMD == nullptr ){
            WARN("Couldn't determine video ram for this ATI card, wglGetGPUIDsAMD or wglGetGPUInfoAMD extentions not available.\n");
            return;
        }

        const int WGL_GPU_RAM_AMD = 0x21A3;
        const uint ids_len = wglGetGPUIDsAMD(0, 0);
        uint *ids = new uint[ids_len];
        wglGetGPUIDsAMD(ids_len, ids);
        wglGetGPUInfoAMD(ids[0], WGL_GPU_RAM_AMD, GL_UNSIGNED_INT, sizeof(size_t), &vid_ram_total);
        delete[] ids;

    } else if ( video_card_brand == VideoCardBrandT::NVIDIA ) {
        // ** http://developer.download.nvidia.com/opengl/specs/GL_NVX_gpu_memory_info.txt

        const int GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX = 0x9048;
        const int GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX = 0x9049;

        int tmp;

        glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX, &tmp);
        ASSERT( tmp >= 0 );
        vid_ram_total = static_cast< uint >( tmp );

        glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX, &tmp);
        ASSERT( tmp >= 0 );
        vid_ram_available_at_start = static_cast< uint >( tmp );
    }
}

void OpenGLInfo::Print( void ) {
    CMSG_LOG("Vendor: %s\n", vendor);
    CMSG_LOG("Version: %s\n", version);
    CMSG_LOG("Renderer: %s\n\n", renderer);

    CMSG_LOG("Video Ram Total: %u \n", vid_ram_total );
    CMSG_LOG("Video Ram Avail: %u \n\n", vid_ram_available_at_start );

    CMSG_LOG("RGBA Color Bits: %i, %i, %i, %i\n", redBits, greenBits, blueBits, alphaBits);
    CMSG_LOG("Depth Bits: %i\n", depthBits);
    CMSG_LOG("Stencil Bits: %i\n\n", stencilBits);

    CMSG_LOG("Max Texture Size: %ix%i\n", maxTextureSize, maxTextureSize);
    CMSG_LOG("Max Lights: %i\n", maxLights);
    CMSG_LOG("Max Clip Planes: %i\n", maxClipPlanes);
    CMSG_LOG("Max Attribute Stacks: %i\n", maxAttribStacks);
    CMSG_LOG("Max Texture Stacks: %i\n\n", maxTextureStacks);

    CMSG_LOG("Max Vertex Uniforms: %i\n", maxVertUniform);
    CMSG_LOG("Max Fragment Uniforms: %i\n\n", maxFragUniform);

    CMSG_LOG( "Extensions: %u \n\n", extensions.Num() );

    if ( globalVals.GetBool("i_glext") )
        for ( auto const& extName : extensions )
            CMSG_LOG( "%s\n", extName.c_str() );
}

bool OpenGLInfo::CheckExtentionSupport( const std::string& ext ) {
    return extensions.Contains( ext );
}

SDL::SDL( void )
    : sdl_renderer(nullptr)
    , sdl_window(nullptr)
    , sdl_glcontext(nullptr)
    , videoFlags(0)
    , isInitialized(false)
    {
}

SDL::~SDL( void ) {
    SDL_CloseAudio();
    Mix_CloseAudio();

    DestroyWindow();

    ASSERT( ! sdl_window );
    ASSERT( ! sdl_renderer );
    ASSERT( ! sdl_glcontext );

    Mix_Quit();
    SDL_Quit();
}

void SDL::DestroyWindow() {
    if ( sdl_renderer ) {
        SDL_DestroyRenderer(sdl_renderer);
        sdl_renderer = nullptr;
    }
    if ( sdl_window ) {
        SDL_DestroyWindow(sdl_window);
        sdl_window = nullptr;
    }
    if ( sdl_glcontext ) {
        SDL_GL_DeleteContext(sdl_glcontext);
        sdl_glcontext = nullptr;
    }
}

void SDL::Init_Video( const uint flags ) {
    CMSG_LOG("---- SDL Video Init ----\n");

    DestroyWindow();

    ASSERT( ! sdl_window );
    ASSERT( ! sdl_renderer );
    ASSERT( ! sdl_glcontext );

    videoFlags |= flags;

    if ( SDL_Init( SDL_INIT_VIDEO ) != 0 ) {
        DIE( "error: Could not initialize SDL video.\n");
        return;
    }

    CreateGameWindow();

    isInitialized = true;
}

void SDL::Init_VBOs( void ) {
    CMSG_LOG("---- SDL VBO Init ----\n");

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wpedantic"
    glBindBuffer = union_cast< PFNGLBINDBUFFERPROC >( SDL_GL_GetProcAddress("glBindBuffer") );
    glGenBuffers = union_cast< PFNGLGENBUFFERSPROC >( SDL_GL_GetProcAddress("glGenBuffers") );
    glBufferData = union_cast< PFNGLBUFFERDATAPROC >( SDL_GL_GetProcAddress("glBufferData") );
    glBufferSubData = union_cast< PFNGLBUFFERSUBDATAPROC >( SDL_GL_GetProcAddress("glBufferSubData") );
    glDeleteBuffers = union_cast< PFNGLDELETEBUFFERSPROC >( SDL_GL_GetProcAddress("glDeleteBuffers") );
    glMapBuffer = union_cast< PFNGLMAPBUFFERPROC >( SDL_GL_GetProcAddress("glMapBuffer") );
    glUnmapBuffer = union_cast< PFNGLUNMAPBUFFERPROC >( SDL_GL_GetProcAddress("glUnmapBuffer") );

    #pragma GCC diagnostic pop
    if ( glBindBuffer && glGenBuffers && glBufferData && glBufferSubData &&
        glDeleteBuffers && glMapBuffer && glUnmapBuffer ) {
        return;
    } else {
        DIE( "Your video card does not seem to support VBOs.\n Updating your video drivers may fix this.\n");
    }
}

void SDL::Init_Shaders( void ) {
    CMSG_LOG("---- SDL Shaders Init ----\n");

    glCreateShader = union_cast< PFNGLCREATESHADERPROC >( SDL_GL_GetProcAddress("glCreateShader") );
    glShaderSource = union_cast< PFNGLSHADERSOURCEPROC >( SDL_GL_GetProcAddress("glShaderSource") );
    glCompileShader = union_cast< PFNGLCOMPILESHADERPROC >( SDL_GL_GetProcAddress("glCompileShader") );
    glAttachShader = union_cast< PFNGLATTACHSHADERPROC >( SDL_GL_GetProcAddress("glAttachShader") );
    glCreateProgram = union_cast< PFNGLCREATEPROGRAMPROC >( SDL_GL_GetProcAddress("glCreateProgram") );
    glLinkProgram = union_cast< PFNGLLINKPROGRAMPROC >( SDL_GL_GetProcAddress("glLinkProgram") );
    glUseProgram = union_cast< PFNGLUSEPROGRAMPROC >( SDL_GL_GetProcAddress("glUseProgram") );
    glGetShaderiv = union_cast< PFNGLGETSHADERIVPROC >( SDL_GL_GetProcAddress("glGetShaderiv") );
    glGetProgramInfoLog = union_cast< PFNGLGETPROGRAMINFOLOGPROC >( SDL_GL_GetProcAddress("glGetProgramInfoLog") );
    glGetShaderInfoLog = union_cast< PFNGLGETSHADERINFOLOGPROC >( SDL_GL_GetProcAddress("glGetShaderInfoLog") );

    if ( glCreateShader && glShaderSource && glCompileShader && glAttachShader &&
        glCreateProgram && glLinkProgram && glUseProgram && glGetShaderiv &&
        glGetProgramInfoLog && glGetShaderInfoLog ) {
        return;
    } else {
        DIE( "Your video card does not support Shaders.\n");
    }
}

void SDL::Init( void ) {
    SetFullScreenFlag( globalVals.GetBool( gval_v_fullscreen ) );
    SetBorderlessWindowFlag( globalVals.GetBool( gval_v_borderlessWindow ) );

    Init_Video();
    Init_VBOs();
    Init_Shaders();

    if ( glewInit() != GLEW_OK )
        DIE("Could not initialize Glew.\n");

    Init_Audio();
    Init_Input();
}

void SDL::Init_Input( void ) {

    CMSG_LOG("---- SDL Input Init ----\n");
    

/*
    // if we don't do this, sometimes joybutton-release events aren't captured after hitting "accept" on loading  a map
    // update: instead of allowing this, for now, we're just going to clear all inputs after a map is loaded
    SDL_SetHint( SDL_HINT_JOYSTICK_ALLOW_BACKGROUND_EVENTS, "1" );
*/
    if ( SDL_Init( SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER ) != 0 ) {
        ERR( "error: Could not initialize SDL joysticks: %s\n", SDL_GetError() );
        return;
    }

    SDL_JoystickEventState( SDL_ENABLE );
    
    LoadControllerDatabase();
}

void SDL::Init_Audio( void ) {
    CMSG_LOG("---- SDL Audio Init ----\n");

    if ( SDL_Init( SDL_INIT_AUDIO ) != 0
    /*    || SDL_AudioInit("openal") != 0 */
        || Mix_OpenAudio(MONTICELLO_SOUND_FREQUENCY, MONTICELLO_SOUND_FORMAT, MONTICELLO_SOUND_AUDIOCHANNELS, MONTICELLO_SOUND_CHUNKSIZE) != 0 )
    {
        ERR("Could not initialize SDL audio: %s\n", SDL_GetError() );
    }

    Mix_AllocateChannels( MONTICELLO_SOUND_CHANNELS );
}

void SDL::SetWindowSize( const int x, const int y ) const {
    ASSERT( sdl_window );
    if ( sdl_window )
        SDL_SetWindowSize( sdl_window, x, y );
}

void SDL::CreateGameWindow( void ) {
    DestroyWindow();

    ASSERT( ! sdl_window );
    ASSERT( ! sdl_renderer );
    ASSERT( ! sdl_glcontext );
 
    uint width = globalVals.GetUInt(gval_v_width);
    uint height = globalVals.GetUInt(gval_v_height);

    const int numVidDisplays = SDL_GetNumVideoDisplays();
    ASSERT( SDL_GetNumVideoDisplays() >= 0 );
    
    std::vector<SDL_DisplayMode> desktopDisplays_vec( static_cast<uint>( numVidDisplays ) );

    //gets desktop display information for multiple displays, assume desktop ID = 0 is main
    for( uint i=0; i<desktopDisplays_vec.size(); ++i ) {
        SDL_GetDesktopDisplayMode(static_cast<int>(i), &desktopDisplays_vec[i]);
    }
    
    //gets desktop display bounds
    std::vector<SDL_Rect> desktopDisplayBounds_vec( static_cast<uint>( numVidDisplays ) );
    for( uint i=0; i<desktopDisplays_vec.size(); ++i ) {
        SDL_GetDisplayBounds(static_cast<int>(i), &desktopDisplayBounds_vec[i]);
    }
        
    // fallback resolution   
    const uint defaultWidth = 800;
    const uint defaultHeight = 600;

    if ( width < defaultWidth || height < defaultHeight ) {
        WARN("Invalid screen size %ux%u, resizing to %ux%u\n", width, height, defaultWidth, defaultHeight);
        width = defaultWidth;
        height = defaultHeight;

        globalVals.SetInt( gval_v_width, defaultWidth );
        globalVals.SetInt( gval_v_height, defaultHeight );
    }

    const uint displayID = GetLaunchDisplayID();
    
    const int h_pos = desktopDisplayBounds_vec[displayID].x + ((desktopDisplayBounds_vec[displayID].w - static_cast<int>( width ) ) / 2); //centered
    const int v_pos = desktopDisplayBounds_vec[displayID].y + ((desktopDisplayBounds_vec[displayID].h - static_cast<int>( height ) ) / 2); //centered

    sdl_window = SDL_CreateWindow(
        "Monticello",
        h_pos,
        v_pos,
        globalVals.GetInt( gval_v_width ),
        globalVals.GetInt( gval_v_height ),
        videoFlags
    );

    if ( sdl_window == nullptr ) {
        DIE("Could not create SDL Window: %s\n", SDL_GetError()  );
        return;
    }

    sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
    if ( sdl_renderer == nullptr ) {
        DIE("Could not create SDL Renderer: %s\n", SDL_GetError()  );
        return;
    }

    sdl_glcontext = SDL_GL_CreateContext( sdl_window );
    if ( sdl_renderer == nullptr ) {
        DIE("Could not create SDL GL Context: %s\n", SDL_GetError()  );
        return;
    }
}

uint SDL::GetLaunchDisplayID( void ) const {
    uint configDisplayID = globalVals.GetUInt( gval_v_launchOnDisplay );
    if ( configDisplayID > 2 ) {
        CMSG_LOG("Invalid DisplayID:\"%u\", in config file. Should be 0 for PRIMARY, 1 for SECONDARY, or 2 for THIRD..defaulting to PRIMARY\n", configDisplayID );
        configDisplayID = 0;
    }
    
    return configDisplayID;
}

Vec3f SDL::GetResolution( void ) const {
    int w=0, h=0;
    SDL_GetWindowSize( sdl_window, &w, &h );
    return {
        static_cast<float>(w),
        static_cast<float>(h),
        0
    };
}

Vec3f SDL::GetPosition( void ) const {
    int x=0, y=0;
    SDL_GetWindowPosition( sdl_window, &x, &y );
    return {
        static_cast<float>(x),
        static_cast<float>(y),
        0
    };
}

void SDL::WarpMouse( const Uint16 x, const Uint16 y ) const {
    if ( !sdl_window ) {
        DIE("Cannot warp mouse, there is no window.\n");
    } else {
        SDL_WarpMouseInWindow( sdl_window, x, y );
    }
}

void SDL::SwapBuffers( void ) const {
    if ( !sdl_window ) {
        DIE("Cannot swap buffers, there is no window.\n");
    }

    SDL_GL_SwapWindow( sdl_window );
}

bool SDL::GetFullScreen( void ) const {
    return videoFlags & SDL_WINDOW_FULLSCREEN;
}

void SDL::ToggleFullScreen( void ) {
    SetFullScreen( !GetFullScreen() );
}

void SDL::SetFullScreen( const bool whether ) {
    ASSERT( sdl_window );
    SetFullScreenFlag( whether );

    if ( sdl_window )
        SDL_SetWindowFullscreen(sdl_window, whether);
        
    game->UpdateCursor();
        
}

void SDL::SetFullScreenFlag( const bool whether ) {
    if ( !whether ) {
        videoFlags &= ~static_cast<uint>(SDL_WINDOW_FULLSCREEN);
    } else {
        videoFlags |= SDL_WINDOW_FULLSCREEN;
    }
}

void SDL::SetBorderlessWindowFlag( const bool whether ) {
    if ( !whether ) {
        videoFlags &= ~static_cast<uint>(SDL_WINDOW_BORDERLESS);
    } else {
        videoFlags |= SDL_WINDOW_BORDERLESS;
    } 
}

bool SDL::LoadControllerDatabase( void ) {
    const std::string filename( DataDir + "gamecontrollerdb.txt" );
    if ( !FileSystem::Exists( filename.c_str() ) ) {
        ERR("SDL Controller database not found in %s\n", filename.c_str() );
        return false;
    }

    const int pads_added = SDL_GameControllerAddMappingsFromFile( filename.c_str() );

    if ( pads_added < 0 ) {
        ERR("SDL Controller database not loaded: %s\n", SDL_GetError() );
        return false;
    }
    
    CMSG_LOG("SDL Controller database loaded %i new controllers\n", pads_added );
    return true;
}
