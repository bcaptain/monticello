// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_BASE_SDLMAIN_H_
#define SRC_BASE_SDLMAIN_H_

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include "./clib/src/warnings.h"

#ifdef _WIN32
    #include <windows.h>
#endif //_WIN32


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wswitch-default" // complains about AABox3D not having virutal destructor, which it doesn't need for this class
#pragma GCC diagnostic ignored "-Wpadded" // complains about AABox3D not having virutal destructor, which it doesn't need for this class
    #include "./SDL2/SDL.h"
    #include "./SDL2/SDL_mixer.h"
#pragma GCC diagnostic pop

#include "../base/main.h"

// VBO funcs
extern PFNGLGENBUFFERSPROC glGenBuffers;
extern PFNGLBINDBUFFERPROC glBindBuffer;
extern PFNGLBUFFERDATAPROC glBufferData;
extern PFNGLBUFFERSUBDATAPROC glBufferSubData;
extern PFNGLDELETEBUFFERSPROC glDeleteBuffers;
extern PFNGLMAPBUFFERPROC glMapBuffer;
extern PFNGLUNMAPBUFFERPROC glUnmapBuffer;

// Shader funcs
extern PFNGLCREATESHADERPROC glCreateShader;
extern PFNGLSHADERSOURCEPROC glShaderSource;
extern PFNGLCOMPILESHADERPROC glCompileShader;
extern PFNGLATTACHSHADERPROC glAttachShader;
extern PFNGLCREATEPROGRAMPROC glCreateProgram;
extern PFNGLLINKPROGRAMPROC glLinkProgram;
extern PFNGLUSEPROGRAMPROC glUseProgram;
extern PFNGLGETSHADERIVPROC glGetShaderiv;
extern PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
extern PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;

typedef Uint8 VideoCardBrandT_BaseType;
enum class VideoCardBrandT : VideoCardBrandT_BaseType {
    UNKNOWN = 0
    , ATI = 1
    , NVIDIA = 2
};

typedef Uint8 DisplayID_BaseType;
enum DisplayID : DisplayID_BaseType {
    PRIMARY = 0
    , SECONDARY = 1
    , THIRD = 2
};

class SDL {
public:
    SDL( void );
    ~SDL( void );
    SDL( const SDL& other ) = delete;

public:
    SDL& operator=( const SDL& other ) = delete;

private:
    void DestroyWindow( void );

public:
    void Init( void );
    Vec3f GetResolution( void ) const; //!< Z will be zero
    Vec3f GetPosition( void ) const;
    
    void SwapBuffers( void ) const;
    void WarpMouse( const Uint16 x, const Uint16 y ) const;
    void SetWindowSize( const int x, const int y ) const;

    void ToggleFullScreen( void );
    void SetFullScreen( const bool whether );
    bool GetFullScreen( void ) const;

    bool IsInitialized( void ) const;
    
    static bool LoadControllerDatabase( void );

private:
    void SetFullScreenFlag( const bool whether );
    void SetBorderlessWindowFlag( const bool whether );
    void CreateGameWindow( void );
    void Init_Video( const uint flags = SDL_WINDOW_OPENGL );
    void Init_Shaders( void );
    void Init_VBOs( void );
    void Init_Audio( void );
    void Init_Input( void );
    uint GetLaunchDisplayID( void ) const;

private:
    SDL_Renderer *sdl_renderer;
    SDL_Window *sdl_window;
    SDL_GLContext sdl_glcontext;
    uint videoFlags;
    bool isInitialized;
};

inline bool SDL::IsInitialized( void ) const {
    return isInitialized;
}

class OpenGLInfo {
public:
    OpenGLInfo( void );
    OpenGLInfo( const OpenGLInfo& other ) = delete;

public:
    OpenGLInfo& operator=( const OpenGLInfo& other ) = delete;

public:
    void Print( void );
    void Init( void );
    bool CheckExtentionSupport( const std::string& name );

public:
    const char* vendor;
    const char* renderer;
    const char* version;
    LinkList<std::string> extensions;

    int redBits;
    int greenBits;
    int blueBits;
    int alphaBits;
    int depthBits;
    int stencilBits;
    int maxTextureSize;
    int maxLights;
    int maxAttribStacks;
    int maxClipPlanes;
    int maxTextureStacks;
    int maxFragUniform;
    int maxVertUniform;
    std::size_t vid_ram_total;
    std::size_t vid_ram_available_at_start;

    VideoCardBrandT video_card_brand;
};

#endif  // SRC_BASE_SDLMAIN_H_

