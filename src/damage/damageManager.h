// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_DAMAGEMANAGER_H_
#define SRC_DAMAGEMANAGER_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../files/manifest.h"
#include "../files/assetManager.h"

class DamageInfo;

/*!

DamageManager \n\n

holds all the DamageInfos for the game

**/

class DamageManager : public AssetManager< AssetData<DamageInfo>, DamageInfo > {
public:
    DamageManager( void );

private:
    bool Load( File& opened_damage_file ) override;
    using AssetManager< AssetData<DamageInfo>,DamageInfo >::Load;

private:
    static bool instantiated;
};

extern DamageManager damageManager;

#endif  // SRC_DAMAGEMANAGER_H_
