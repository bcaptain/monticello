// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_DAMAGE_H_
#define SRC_DAMAGE_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../persistentEvent.h"

class IntersectData;
class Primitive;
class DamageInfo;
class Entity;
class Effect;
struct CollisionItem;

struct EffectEvent;

struct DamageEvent {
    DamageEvent( void );
    explicit DamageEvent( const std::shared_ptr< const DamageInfo > _damage, const std::weak_ptr< Entity >& _ent, const std::weak_ptr< Entity >& _inflictor ) : damage(_damage), ent(_ent), inflictor(_inflictor) { }

    DamageEvent( const DamageEvent& other ) = default;
    ~DamageEvent( void ) = default;
    
    DamageEvent& operator=( const DamageEvent& other ) = default;
    
    std::shared_ptr< const DamageInfo > damage;
    std::weak_ptr< Entity > ent;
    std::weak_ptr< Entity > inflictor;
};

typedef bool DamageFalloffT_BaseType;
enum class DamageFalloffT : DamageFalloffT_BaseType {
    Linear
    , Uniform
};

//! describes how much of each type of damage something does
class DamageInfo {
public:
    DamageInfo( void );
    ~DamageInfo( void ) = default;
    DamageInfo( const DamageInfo& other ) = default;

public:
    DamageInfo& operator=( const DamageInfo& other ) = default;

public:
    int GetTotalDamage( void ) const;

    void SetDamagesPlayer( const bool whether );
    void SetDamagesEnemy( const bool whether );
    bool Inflict( Entity& ent, const std::weak_ptr< Entity >& inflictor, const float percentOfDamage=1.0f ) const;

    bool HasType( const std::vector< std::pair< std::string, int > >& v_types ) const;
    bool HasType( const std::string& dmg_type ) const;

    void PlayHitSound( void ) const;
    
    void SetName( const std::string to );
    std::string GetName( void ) const;
    
    void PreloadAssets( void ) const;

private:
    std::string name;
public:
    std::vector< std::pair< std::string, int > > types;
    std::string snd_hit;
    std::shared_ptr< const Effect > fx_hurt;
    std::shared_ptr< PersistentEvent< EffectEvent > > fx_persist; //todo: a list of these
    float critChance;
    float critMultiplier;
    uint dur; //!< if greater than zero, how many miliseconds to continue doing damage.
    uint delay; //!< MS to wait before doing damage (until duration is up_;
    bool noAnim;
    bool noStack;
};

class DamageVuln {
public:
    DamageVuln( void );
    ~DamageVuln( void ) = default;
    DamageVuln( const DamageVuln& other ) = default;

public:
    DamageVuln& operator=( const DamageVuln& other ) = default;

public:
    int Result( const DamageInfo& damageInfo, const float percent=1.0f ); //!< return how much damage will be taken based on our vulnerabilities. Percentage values from 0 to 1, 1 being 100% vulnerable to a damage type
    void AddType( const std::string& damageName, const int damage );
    void RemoveType( const std::string& damageName );
    
public:
    std::vector< std::pair< std::string, int > > types;
};

uint DamageZone( const Primitive& primitive, const uint max_targets, const DamageInfo& damage, const std::weak_ptr< Entity >& inflictor, const DamageFalloffT falloff = DamageFalloffT::Uniform, std::vector< CollisionItem >* damaged_ents_out = nullptr );

bool IsAWallBetweenPoints( const Vec3f& start, const Vec3f& end, const Entity* ent_to_ignore_one, const Entity* ent_to_ignore_two );

void Damage_GetTargets( const IntersectData& coldata, const Primitive& primitive, const std::weak_ptr< Entity >& inflictor, std::vector< CollisionItem >& ents );

uint DamageEnts( const std::vector< CollisionItem >& ents, const uint max_targets, const DamageInfo& damage, const std::weak_ptr< Entity >& inflictor, std::vector< CollisionItem >* damaged_ents_out = nullptr );
uint DamageEnts_Linear( const std::vector< CollisionItem >& ents, const uint max_targets, const DamageInfo& damage, const std::weak_ptr< Entity >& inflictor, const Vec3f& damage_center, const float damage_dist );

#endif  // SRC_DAMAGE_H_
