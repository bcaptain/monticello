// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "./damageManager.h"
#include "../particles/effectsManager.h"
#include "../sound.h"
#include "./damage.h"
#include "../filesystem.h" // for OverrideWarning()

bool DamageManager::instantiated(false);
DamageManager damageManager;

DamageManager::DamageManager( void )
    : AssetManager( "Damage" )
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

bool DamageManager::Load( File& opened_file ) {
    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open damage file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    AssetData< DamageInfo >* manifest_s = manifest.Get( name.c_str() );
    if ( !manifest_s ) {
        ERR_DIALOG("Couldn't load damage file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
    }

    auto dam = std::make_shared<DamageInfo>();
    dam->SetName( name );

    // ** parse the file
    ParserSSV parser( opened_file );
    parser.SetCommentIndicator('#');
    std::string entry;
    
    bool setDelay = false;
    bool setDur = false;

    while ( parser.ReadWord( entry ) ) {
        if ( entry == "snd_hit" ) {
            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR("Malformed damage file %s, \"snd_hit\" should specify a sound\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->snd_hit = entry;
            soundManager.Load( entry.c_str() );
            continue;
        }

        if ( entry == "type" ) {
            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR("Malformed damage file %s, \"type\" should specify a type, then an integer value\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->types.emplace_back( entry, parser.ReadInt() );
            continue;
        }

        if ( entry == "crit" ) {
            std::string mult;
            if ( !parser.ReadWordOnLine( entry ) || !parser.ReadWordOnLine( mult ) ) {
                ERR("Malformed damage file %s, \"crit\" should specify a uint 0-100 chance to crit, then a floating-point percentage multiplier\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->critChance = String::ToFloat( entry );
            dam->critMultiplier = String::ToFloat( mult );
            continue;
        }
        
        if ( entry == "dur" ) {
            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR("Malformed damage file %s, \"dur\" should specify a time in miliseconds\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->dur = String::ToUInt( entry );
            setDur = true;
            continue;
        }
        
        if ( entry == "delay" ) {
            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR("Malformed damage file %s, \"delay\" should specify a time in miliseconds\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->delay = String::ToUInt( entry );
            setDelay = true;
            continue;
        }
        
        if ( entry == "fx_hurt" ) {
            std::string ms;
            if ( !parser.ReadWordOnLine( entry ) || !parser.ReadWordOnLine( ms ) ) {
                ERR("Malformed damage file %s, \"fx_hurt\" should specify an effect name and a time in miliseconds\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->fx_hurt = effectsManager.Get( entry.c_str() );
            continue;
        }
        
        if ( entry == "fx_persist" ) {
            std::string dur, delay;
            if ( !parser.ReadWordOnLine( entry ) || !parser.ReadWordOnLine( delay ) || !parser.ReadWordOnLine( dur ) ) {
                ERR("Malformed damage file %s, \"fx_hurt\" should specify an effect name and a time in miliseconds\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            auto fx = effectsManager.Get( entry.c_str() );
            if ( ! fx )
                continue;
            
            dam->fx_persist = std::make_shared< PersistentEvent<EffectEvent> >( EffectEvent(fx) );
            dam->fx_persist->SetDuration( String::ToUInt( dur ) );
            dam->fx_persist->SetDelay( String::ToUInt( delay ) );
            continue;
        }
        
        if ( entry == "noAnim" ) {
            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR("Malformed damage file %s, \"noAnim\" should specify a bool\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->noAnim = String::ToBool( entry );
            continue;
        }
        
        if ( entry == "noStack" ) {
            if ( !parser.ReadWordOnLine( entry ) ) {
                ERR("Malformed damage file %s, \"noStack\" should specify a bool\n", opened_file.GetFilePath() );
                parser.SkipToNextLine();
                continue;
            }

            dam->noStack = String::ToBool( entry );
            continue;
        }

        ERR("Malformed damage file %s, unknown entry: %s\n", opened_file.GetFilePath(), entry );
        parser.SkipToNextLine();
    }
    
    if ( setDelay && !setDur ) {
        WARN("Delay set without Dur (Duration): damage file %s, unknown entry: %s\n", opened_file.GetFilePath(), entry );
    }

    const bool asset_existed = manifest_s->data != nullptr;
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );
        
    manifest_s->data = dam;
    
    CMSG_LOG("Loaded damage_info: %s from %s\n", opened_file.GetFilePath(), opened_file.GetFileContainerName_Full() );

    return true;
}
