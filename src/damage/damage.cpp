// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./damage.h"
#include "./damageManager.h"
#include "../particles/effectsManager.h"
#include "../game.h"
#include "../math/collision/collision.h"
#include "../entities/entity.h"
#include "../sound.h"
#include "../entities/actor.h"

DamageInfo::DamageInfo( void )
    : name()
    , types()
    , snd_hit()
    , fx_hurt()
    , fx_persist()
    , critChance(0)
    , critMultiplier(0)
    , dur(0)
    , delay(0)
    , noAnim(false)
    , noStack(true)
{ }

bool DamageInfo::Inflict( Entity& ent, const std::weak_ptr< Entity >& inflictor, const float percent ) const {
    if ( !ent.IsDestructible() )
        return false;
        
    if ( dur > 0 ) {
        auto damage = damageManager.Get( GetName().c_str() );
        if ( damage ) {
            ent.persistentDamages.emplace_back( DamageEvent(damage, ent.GetWeakPtr(), inflictor) );
            auto & event = ent.persistentDamages[ent.persistentDamages.size()-1];
            event.SetDelay( delay );
            event.SetDuration( dur );
        } else {
            ERR("Could not find persistent damage: %s.\n", GetName().c_str() );
            return false;
        }
    } else {
        if ( ! Entity::DamageAttorney::ApplyDamage( *this, ent, inflictor, percent ) )
            return false;
    }
    
    if ( fx_persist ) {
        auto & data = fx_persist->data;
        if ( data.effect ) {
            ent.persistentEffects.emplace_back( PersistentEvent< EffectEvent >( EffectEvent(data.effect) ) );
            auto & event = ent.persistentEffects[ent.persistentEffects.size()-1];
            event.SetDelay( fx_persist->GetDelay() );
            event.SetDuration( fx_persist->GetDuration() );
        }
    }
    return true;
}

DamageVuln::DamageVuln( void )
    : types()
{}

int DamageVuln::Result( const DamageInfo& damage, const float percent ) { //tododamage:ugly
    float ret = 0;
    for ( auto& type : damage.types ) {
        auto found = std::find_if(
            std::cbegin(types)
            , std::cend(types) 
            , [&type](const auto& elem) {return elem.first == type.first;}
        );
        
        const float non_immunity = (found!=std::cend(types)) ? (1.0f-found->second) : 1.0f;
        ret += type.second * non_immunity * percent;
    }
    
    return static_cast<int>( ret );
}

void DamageVuln::AddType( const std::string& damageName, const int damage ) {
    
    for ( auto& v : types )
        if ( damageName == v.first && damage == v.second )
            return;
            
    types.push_back( {damageName,damage} );
}

void DamageVuln::RemoveType( const std::string& damageName ) {
    for ( std::size_t i = 0, e = types.size(); i != e; ++i) {
        if ( damageName == types[i].first ) {
            types.erase(std::cbegin(types) + static_cast<std::ptrdiff_t>( i ) );
            return;
        }
    }
}

int DamageInfo::GetTotalDamage( void ) const {
    int ret = 0;
    for ( auto& t : types )
        ret += t.second;

    if ( critChance > 0 ) {
        float crit = Random::Float( 0, 1 );
        if ( crit <= critChance ) {
            CMSG("Critical Hit.\n");
            return static_cast<int>(
                static_cast<float>(ret) * critMultiplier
            );
        }
    }

    return ret;
}

bool DamageInfo::HasType( const std::string& dmg_type ) const {
    for ( auto& t : types ) {
        if ( t.first == dmg_type )
            return true;
    }
    
    return false;
}

bool DamageInfo::HasType( const std::vector< std::pair< std::string, int > >& v_types ) const {
    for ( auto& t : v_types ) {
        if ( HasType( t.first ) )
            return true;
    }
    
    return false;
}

uint DamageZone( const Primitive& primitive, const uint max_targets, const DamageInfo& damage, const std::weak_ptr< Entity >& inflictor, const DamageFalloffT falloff /* todo:damageteams */, std::vector< CollisionItem >* damaged_ents_out ) {
    std::vector< CollisionItem > entList;
    IntersectData coldata;
    coldata.opts |= MATCH_VISIBLE_ONLY;
    coldata.opts |= MATCH_ENTS_WITH_MODELS_ONLY;
    coldata.opts |= MATCH_TEST_ONLY_BOUNDS;

    std::vector< CollisionItem > ents;
    Damage_GetTargets( coldata, primitive, inflictor, ents );
    
    return DamageEnts( ents, max_targets, damage, inflictor, damaged_ents_out );
}

void Damage_GetTargets( const IntersectData& coldata, const Primitive& primitive, const std::weak_ptr< Entity >& inflictor, std::vector< CollisionItem >& ents ) {
    game->entTree->GetEntityTraceCollisions( coldata, primitive, &ents );

    for ( uint i=0; i<ents.size(); ++i ) {
        auto ent = ents[i].ent.lock();

        if ( !ent ) {
            PopSwap( ents, i-- );
            continue;
        }

        if ( auto ient = inflictor.lock() ) {
            if ( ent->GetIndex() == ient->GetIndex() ) {
                PopSwap( ents, i-- );
                continue;
            }
        }
    }
}

uint DamageEnts_Linear( const std::vector< CollisionItem >& ents, const uint max_targets, const DamageInfo& damage, const std::weak_ptr< Entity >& inflictor, const Vec3f& damage_center, const float damage_dist ) {
    uint hits=0;
    
    for ( auto & item : ents ) {
        auto ent = item.ent.lock();
        if ( !ent )
            continue;
                
        float damage_fraction=1;
        if ( ent->bounds.IsSphere() ) {
            
            // get the distance to the blast origin from the closest point on the entity's bounding sphere
            const float dist_between_origins = Vec3f::Len( damage_center - ent->GetOrigin() );
            const float entRadius = ent->bounds.GetRadius();
            const float sumRadii = entRadius + damage_dist;

            // if the spheres do not intersect ( this is an edge case )
            if ( dist_between_origins > sumRadii )
                return false;

            if ( dist_between_origins > EP ) {
                damage_fraction = 1 - (dist_between_origins/sumRadii);
            }
        }

        damage.Inflict( *ent, inflictor, damage_fraction );
        
        if ( damage.snd_hit.size() > 0 ) {
            soundManager.PlayAt( ent->GetOrigin(), damage.snd_hit.c_str() );
        }

        ++hits;
        if ( max_targets > 0 && hits >= max_targets )
            break;
    }

    return hits;
}

uint DamageEnts( const std::vector< CollisionItem >& ents, const uint max_targets, const DamageInfo& damage, const std::weak_ptr< Entity >& inflictor, std::vector< CollisionItem >* damaged_ents_out ) {
    uint hits=0;
    
    for ( auto & item : ents ) {
        auto ent = item.ent.lock();
        if ( !ent )
            continue;
            
        // player doesn't damage self, enemies don't damage enemies
        auto ient = inflictor.lock();
        if ( ient ) {
            if ( ent->GetIndex() == ient->GetIndex() )
                continue;
            
            if ( derives_from< Actor >( *ent ) ) {
                // enemy flag only works for actors
                if ( ient->GetTeamName() == ent->GetTeamName() )
                    continue;
            } else {
                // only player can destory walls, pots, etc
                if ( ient->GetTeamName() != "player" )
                    continue;
            }
        }
        
        if ( !damage.Inflict( *ent, inflictor ) ) {
            continue;
        }
        
        if ( damage.snd_hit.size() > 0 ) {
            soundManager.PlayAt( ent->GetOrigin(), damage.snd_hit.c_str() );
        }

        ++hits;
        if ( damaged_ents_out )
            damaged_ents_out->emplace_back( item );
        if ( max_targets > 0 && hits >= max_targets )
            break;
    }

    return hits;
}

void DamageInfo::PreloadAssets( void ) const {
}

void DamageInfo::SetName( const std::string to ) {
    name = to;
}

std::string DamageInfo::GetName( void ) const {
    return name;
}
