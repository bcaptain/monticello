// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MATH_PRIMITIVE_H
#define SRC_MATH_PRIMITIVE_H

#include "../base/main.h"
#include "../rendering/vbo.h"
#include "../rendering/shapes.h"
#include "../rendering/drawInfo.h"

#include "../math/geometry/3d/cone.h"
#include "../math/geometry/3d/cylinder.h"
#include "../math/geometry/3d/capsule.h"
#include "../math/geometry/3d/poly.h"
#include "../math/collision/collision.h"

extern const float SPHERE_RADIUS_DEFAULT;

class Model;
class Material;
struct DrawInfo;

class CollisionReport3D;

using PrimitiveDrawModeT_BaseType = uint;
enum class PrimitiveDrawModeT : PrimitiveDrawModeT_BaseType {
    Wireframe
    , Solid
    , Gradient
};

// mimicking partial function template specialization
template< typename TYPE, PrimitiveShapeT SHAPE >
struct CheckShapeToModel {
    static constexpr bool Trace( const TYPE& shape, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) {
        return TYPE::ToModel( shape, _model, _model_angles, _model_origin );
    }
    static constexpr bool Sweep( CollisionReport3D& report, const TYPE& shape, const Vec3f& toDest, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin, const ModelCollisionT clipType ) {
        return TYPE::SweepToModel( report, shape, toDest, _model, _model_angles, _model_origin, clipType );
    }
};
 
// specialization of CheckShapeToModel for Vec3f, since we keep the function in Collision3D::TestPoint instead of in TYPE:: like the rest of the shapes
template< PrimitiveShapeT SHAPE >
struct CheckShapeToModel<Vec3f, SHAPE> {
    static constexpr bool Trace( const Vec3f& shape, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) {
        return Collision3D::TestPoint::ToModel( shape, _model, _model_angles, _model_origin );
    }
    static constexpr bool Sweep( CollisionReport3D& report, const Vec3f& point, const Vec3f& vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin, const ModelCollisionT clipType ) {
        return Collision3D::TestPoint::SweepToModel( report, point, point+vel, _model, _model_angles, _model_origin, clipType );
    }
};

struct PrimitiveDrawInfo {
    PrimitiveDrawInfo( void );
    PrimitiveDrawInfo( const PrimitiveDrawInfo& other );
    PrimitiveDrawInfo& operator=( const PrimitiveDrawInfo& other );

    PrimitiveDrawInfo( PrimitiveDrawInfo && other_rref );
    PrimitiveDrawInfo& operator=( PrimitiveDrawInfo && other_rref );
    
    friend class Primitive;
    friend class Primitive_Sphere;
    friend class Primitive_Box3D;
    friend class Primitive_AABox3D;
    friend class Primitive_Capsule;
    friend class Primitive_Cylinder;
    friend class Primitive_Line3f;
    friend class Primitive_Point;

private:
    void Clear( void );
    
private:
    std::unique_ptr< VBO<float> > vbo_vert;
    std::unique_ptr< VBO<float> > vbo_uv;
    std::unique_ptr< VBO<float> > vbo_color;
    PrimitiveDrawModeT prevMode;
};

class Primitive {
public:
    Primitive( void );
    Primitive( const Primitive& other );
    virtual ~Primitive( void ) {}
    
public:
    Primitive& operator=( const Primitive& other );
    virtual std::shared_ptr< Primitive > Clone_Shared( void ) const = 0;

public:
    void SetOwner( Entity* _owner );
    void UpdateOwnerAngles( void ) const;
    Entity* GetOwner( void ) const;
    
    void DrawVBO( const DrawInfo& drawInfo, const PrimitiveDrawInfo& primDrawInfo ) const;
    
    void UpdateVBO( PrimitiveDrawInfo& drawInfo );
    void PackVBO( PrimitiveDrawInfo& drawInfo ) const;
    virtual void PackVBO_color( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const;
    virtual void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const = 0;
    virtual void PackVBO_uv( PrimitiveDrawInfo& drawInfo ) const;
    
    virtual PrimitiveShapeT GetShapeType( void ) const = 0;
    
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wunused-parameter"
        virtual bool Intersects( const Vec3f& _point ) const { ERR("Not Implemented: virtual Intersects( Vec3f ).\n"); return false; }
        virtual bool Intersects( const Line3f& _line ) const { ERR("Not Implemented: virtual Intersects( Line3f ).\n"); return false; }
        virtual bool Intersects( const Sphere& _sphere ) const { ERR("Not Implemented: virtual Intersects( Sphere ).\n"); return false; }
        virtual bool Intersects( const Box3D& _box ) const { ERR("Not Implemented: virtual Intersects( Box3D ).\n"); return false; }
        virtual bool Intersects( const AABox3D& _aabox ) const { ERR("Not Implemented: virtual Intersects( AABox3D ).\n"); return false; }
        virtual bool Intersects( const Capsule& _capsule ) const { ERR("Not Implemented: virtual Intersects( Capsule ).\n"); return false; }
        virtual bool Intersects( const SemiCylinder& _semiCyl ) const { ERR("Not Implemented: virtual Intersects( SemiCylinder ).\n"); return false; }
        virtual bool Intersects( const Cylinder& _cyl ) const { ERR("Not Implemented: virtual Intersects( Cylinder ).\n"); return false; }
        virtual bool Intersects( const Polygon3D& _polygon ) const { ERR("Not Implemented: virtual Intersects( Polygon3D ).\n"); return false; }
        virtual bool Intersects( const Primitive& _primitive ) const { ERR("Not Implemented: virtual Intersects( Primitive ).\n"); return false; }
        virtual bool Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const { ERR("Not Implemented: virtual Intersects( Model ).\n"); return false; }
        
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Vec3f& _point ) const { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Line3f ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Line3f& _line ) const { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Line3f ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Sphere& _sphere ) const { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Sphere ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Capsule& _capsule ) const { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Capsule ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Cylinder& _cyl ) const  { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Cylinder ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const SemiCylinder& _semiCyl ) const  { ASSERT_MSG( false, "Not Implemented: virtual Intersects( SemiCylinder ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Box3D& _box ) const  { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Box3D ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const AABox3D& _aabox ) const  { ASSERT_MSG( false, "Not Implemented: virtual Intersects( AABox3D ).\n"); return false; }
        virtual bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Polygon3D& _polygon ) const  { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Polygon3D ).\n"); return false; }
        virtual bool SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Primitive& _primitive ) const  { ASSERT_MSG( false, "Not Implemented: virtual Intersects( Primitive ).\n"); return false; }
        virtual bool SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const  { ASSERT_MSG( false, "Not Model.\n"); return false; }
    #pragma GCC diagnostic pop
        
    virtual void SetBoundsFromModel( const Model& model ) = 0;

    virtual void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool );
    virtual void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    
    Vec3f GetOrigin( void ) const;
    void SetOrigin( const Vec3f& _origin );

    bool IsRotated( void ) const; //!< if the object's angle is not default
    
    Vec3f GetAngles( void ) const;
    float GetYaw( void ) const;
    float GetRoll( void ) const;
    float GetPitch( void ) const;

    void SetYaw( const float ang );
    void SetRoll( const float ang );
    void SetPitch( const float ang );
    
    void SetAngle( const float amtx, const float amty, const float amtz );
    void SetAngle( const Vec3f& ang );

    void NudgeYaw( const float amt );
    void NudgeRoll( const float amt );
    void NudgePitch( const float amt );
    void NudgeAngle( const Vec3f& amt ); //!< sets angle instantly
    void NudgeAngle( const float amtx, const float amty, const float amtz ); //!< sets angle instantly

    void SnapYaw( const float to_angle );
    void SnapRoll( const float to_angle );
    void SnapPitch( const float to_angle );
    
protected:
    void SetOwnerStatusMovedAndUpdateVBO( void );

protected:
    Entity* owner;
    Vec3f origin;
    Vec3f angles; // in degrees
};

template< typename TYPE, PrimitiveShapeT SHAPE >
class Primitive_Derived : public Primitive {
public:
    Primitive_Derived( void );
    explicit Primitive_Derived( const TYPE& _shape );
    ~Primitive_Derived( void ) override {}
    
public:
    bool Intersects( const Vec3f& _point ) const override;
    bool Intersects( const Line3f& _line ) const override;
    bool Intersects( const Sphere& _shape ) const override;
    bool Intersects( const Box3D& _box ) const override;
    bool Intersects( const AABox3D& _aabox ) const override;
    bool Intersects( const Capsule& _capsule ) const override;
    bool Intersects( const SemiCylinder& _semiCyl ) const override;
    bool Intersects( const Cylinder& _cyl ) const override;
    bool Intersects( const Polygon3D& _polygon ) const override;
    bool Intersects( const Primitive& _primitive ) const override;
    bool Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Vec3f& _point ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Line3f& _line ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Sphere& _shape ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Capsule& _capsule ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Cylinder& _cyl ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const SemiCylinder& _semiCyl ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Box3D& _box ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const AABox3D& _aabox ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Polygon3D& _polygon ) const override;
    bool SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Primitive& _primitive ) const override;
    bool SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    PrimitiveShapeT GetShapeType( void ) const override { return SHAPE; }
    
    TYPE GetShape_Local( void ) const;
    virtual void SetShape( const TYPE& _shape );
    virtual TYPE GetShape( void ) const = 0;
    
protected:
    TYPE shape;
};

template< typename TYPE, PrimitiveShapeT SHAPE >
Primitive_Derived<TYPE, SHAPE>::Primitive_Derived( const TYPE& _shape )
    : shape( _shape )
{ }

template< typename TYPE, PrimitiveShapeT SHAPE >
TYPE Primitive_Derived<TYPE, SHAPE>::GetShape_Local( void ) const {
    return shape;
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Vec3f& _point ) const {
    return Collision3D::TestPoint::Intersects( _point, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Sphere& _sphere ) const {
    return Sphere::Intersects( _sphere, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Box3D& _box ) const {
    return Box3D::Intersects( _box, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const AABox3D& _aabox ) const {
    return AABox3D::Intersects( _aabox, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Capsule& _capsule ) const {
    return Capsule::Intersects( _capsule, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Line3f& _line ) const {
    CollisionReport3D report_unused;
    return Line3f::Intersects( report_unused, _line, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const SemiCylinder& _semiCyl ) const {
    return Collision3D::TestSemiCylinder::Intersects( _semiCyl, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Cylinder& _cyl ) const {
    return Cylinder::Intersects( _cyl, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Polygon3D& _polygon ) const {
    return Polygon3D::Intersects( _polygon, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Primitive& _primitive ) const {
    return _primitive.Intersects( GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
    return CheckShapeToModel<TYPE, SHAPE>::Trace( GetShape(), _model, _model_angles, _model_origin );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Vec3f& _point ) const {
    return Collision3D::TestPoint::SweepTo( report, _point, _point + _vel, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Sphere& _sphere ) const {
    return Sphere::SweepTo( report, _sphere, _sphere.origin+_vel, GetShape() );
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Box3D& _box ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::SweepToBox3D\n"); //toimplement
        return false;
    }

    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const AABox3D& _aabox ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::SweepToBox3D\n"); //toimplement
        return false;
    }

    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Capsule& _capsule ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::SweepToCapsule\n"); //toimplement
        return false;
    }

    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Line3f& _line ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::Line3f\n"); //toimplement
        return false;
    }

    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const SemiCylinder& _semiCyl ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::SweepToSemiCylinder\n"); //toimplement
        return false;
    }

    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Cylinder& _cyl ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::SweepToCylinder\n"); //toimplement
        return false;
    }

    template< typename TYPE, PrimitiveShapeT SHAPE >
    bool Primitive_Derived<TYPE, SHAPE>::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Polygon3D& _polygon ) const {
        ERR("Not Implemented: Primitive_Derived<TYPE, SHAPE>::SweepToPolygon3D\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Primitive& _primitive ) const {
    return _primitive.SweepWith( report, _vel, GetShape() );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
bool Primitive_Derived<TYPE, SHAPE>::SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
    return CheckShapeToModel<TYPE, SHAPE>::Sweep( report, GetShape(), GetOrigin()+_vel, _model, _model_angles, _model_origin, ModelCollisionT::CLOSEST_POLY );
}

template< typename TYPE, PrimitiveShapeT SHAPE >
void Primitive_Derived<TYPE, SHAPE>::SetShape( const TYPE& _shape ) {
    shape = _shape;
    SetOwnerStatusMovedAndUpdateVBO();
}

class Primitive_Sphere : public Primitive_Derived< Sphere, PrimitiveShapeT::SPHERE > {
public:
    Primitive_Sphere( void );
    explicit Primitive_Sphere( const Sphere& _sphere );
    explicit Primitive_Sphere( const Model& _modelToEncompass );
    explicit Primitive_Sphere( const float _radius );
    ~Primitive_Sphere( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    void PackVBO_color( [[maybe_unused]] PrimitiveDrawInfo& drawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const override {}
    
public:
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    float GetRadius( void ) const;
    void SetRadius( const float _radius );
    
    void SetShape( const Sphere& _sphere ) override;
    Sphere GetShape( void ) const override;
};

class Primitive_Box3D : public Primitive_Derived< Box3D, PrimitiveShapeT::BOX3D > {
public:
    Primitive_Box3D( void );
    explicit Primitive_Box3D( const Box3D& _box );
    Primitive_Box3D( Box3D && box_rref );
    explicit Primitive_Box3D( const float side_lengths );
    ~Primitive_Box3D( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    void PackVBO_color( [[maybe_unused]] PrimitiveDrawInfo& drawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const override {}
    
public:
    void PackVBO_uv( PrimitiveDrawInfo& drawInfo ) const override;
    
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    Box3D GetShape( void ) const override;
};

class Primitive_AABox3D : public Primitive_Derived< AABox3D, PrimitiveShapeT::AABOX3D > {
public:
    Primitive_AABox3D( void );
    explicit Primitive_AABox3D( const AABox3D& _aabox );
    explicit Primitive_AABox3D( const Model& _modelToEncompass );
    explicit Primitive_AABox3D( const float bounds );
    ~Primitive_AABox3D( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    void PackVBO_color( [[maybe_unused]] PrimitiveDrawInfo& drawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const override {}
public:
    void PackVBO_uv( PrimitiveDrawInfo& drawInfo ) const override;
    
    bool Intersects( const Vec3f& _point ) const override;
    bool Intersects( const Line3f& _line ) const override;
    bool Intersects( const Sphere& _sphere ) const override;
    bool Intersects( const Box3D& _box ) const override;
    bool Intersects( const AABox3D& _aabox ) const override;
    bool Intersects( const Capsule& _capsule ) const override;
    bool Intersects( const SemiCylinder& _semiCyl ) const override;
    bool Intersects( const Cylinder& _cyl ) const override;
    bool Intersects( const Polygon3D& _polygon ) const override;
    bool Intersects( const Primitive& _primitive ) const override;
    bool Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Vec3f& _point ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Line3f& _line ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Sphere& _sphere ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Capsule& _capsule ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Cylinder& _cyl ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const SemiCylinder& _semiCyl ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Box3D& _box ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const AABox3D& _aabox ) const override;
    bool SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Polygon3D& _polygon ) const override;
    bool SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Primitive& _primitive ) const override;
    bool SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    void SetShape( const AABox3D& _aabox ) override;
    void SetShape( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z );
    void SetShape( const float bounds );
    
    AABox3D GetShape( void ) const override { ASSERT_MSG(false, "This class should be calling GetOrientedBox3D()"); return shape; }
    
    Box3D GetOrientedBox3D( void ) const;
    AABox3D GetTranslatedAABox3D( void ) const;
};

class Primitive_Point : public Primitive_Derived< Vec3f, PrimitiveShapeT::VEC3F > {
public:
    Primitive_Point( void ) = delete;
    explicit Primitive_Point( const Vec3f& _vec );
    ~Primitive_Point( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    
public:
    using Primitive_Derived::Intersects;
    bool Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    using Primitive_Derived::SweepTo;
    bool SweepTo( CollisionReport3D& report, const Vec3f& _vel_ignored, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    Vec3f GetShape( void ) const override;
};

class Primitive_Line3f : public Primitive_Derived< Line3f, PrimitiveShapeT::LINE3F > {
public:
    Primitive_Line3f( void ) = delete;
    explicit Primitive_Line3f( const LineTypeT _line_type );
    explicit Primitive_Line3f( const Line3f& _line );
    ~Primitive_Line3f( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    
public:
    using Primitive_Derived::Intersects;
    bool Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    using Primitive_Derived::SweepTo;
    bool SweepTo( CollisionReport3D& report, const Vec3f& _vel_ignored, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const override;
    
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    Line3f GetShape( void ) const override;
};

class Primitive_Cylinder : public Primitive_Derived< Cylinder, PrimitiveShapeT::CYLINDER > {
public:
    Primitive_Cylinder( void );
    explicit Primitive_Cylinder( const Cylinder& _cyl );
    ~Primitive_Cylinder( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    void PackVBO_color( [[maybe_unused]] PrimitiveDrawInfo& drawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const override {}
    
public:
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    void SetRadius( const float _radius );
    void SetLength( const float _length );
    
    void SetShape( const Cylinder& _cyl ) override;
    Cylinder GetShape( void ) const override;
};

class Primitive_Capsule : public Primitive_Derived< Capsule, PrimitiveShapeT::CAPSULE > {
public:
    Primitive_Capsule( void );
    explicit Primitive_Capsule( const Capsule& _cyl );
    ~Primitive_Capsule( void ) override {}
    
    std::shared_ptr< Primitive > Clone_Shared( void ) const override;
    
protected:
    void PackVBO_vert( PrimitiveDrawInfo& drawInfo, const PrimitiveDrawModeT drawMode ) const override;
    void PackVBO_color( [[maybe_unused]] PrimitiveDrawInfo& drawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const override {}
    
public:
    void SetBoundsFromModel( const Model& model ) override;
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool )  override;
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const override;
    
    void SetRadius( const float _radius );
    void SetLength( const float _length );
    
    void SetShape( const Capsule& _cap ) override;
    Capsule GetShape( void ) const override;
};

#endif  // SRC_MATH_PRIMITIVE_H
