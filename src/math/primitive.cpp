// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./primitive.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../math/collision/collision.h"

Primitive::Primitive( void ) 
    : owner()
    , origin()
    , angles( 0,0,0 )
{ }

Primitive::Primitive( const Primitive& other ) 
    : owner() // owner stays the same
    , origin( other.origin )
    , angles( other.angles )
{ }

Primitive& Primitive::operator=( const Primitive& other ) {
    // owner stays the same
    origin = other.origin;
    angles = other.angles;
    UpdateOwnerAngles();
    return *this;
}

void Primitive::SetOwner( Entity* _owner ) {
    owner = _owner;
    UpdateOwnerAngles();
}

void Primitive::UpdateOwnerAngles( void ) const {
    if ( !owner )
        return;
        
    owner->RotateTo( angles );
    owner->SetMoved();
}

Entity* Primitive::GetOwner( void ) const {
    return owner;
}

void Primitive::NudgeYaw( const float amt ) {
    SetYaw( angles.z + amt );
}

void Primitive::NudgeRoll( const float amt ) {
    SetRoll( angles.x + amt );
}

void Primitive::NudgePitch( const float amt ) {
    SetPitch( angles.y + amt );
}

void Primitive::NudgeAngle( const Vec3f& amt ) {
    SetAngle( angles.x + amt.x, angles.y + amt.y, angles.z + amt.z );
}

void Primitive::NudgeAngle( const float amtx, const float amty, const float amtz ) {
    SetAngle( angles.x + amtx, angles.y + amty, angles.z + amtz );
}

void Primitive::SetAngle( const Vec3f& ang ) {
    SetAngle( ang.x, ang.y, ang.z );
}

float Primitive::GetYaw( void ) const {
    return angles.z;
}

float Primitive::GetRoll( void ) const {
    return angles.y;
}

float Primitive::GetPitch( void ) const {
    return angles.x;
}

Vec3f Primitive::GetAngles( void ) const {
    return angles;
}

PrimitiveDrawInfo::PrimitiveDrawInfo( void )
    : vbo_vert()
    , vbo_uv()
    , vbo_color()
    , prevMode( PrimitiveDrawModeT::Wireframe )
{ }

PrimitiveDrawInfo::PrimitiveDrawInfo( [[maybe_unused]] const PrimitiveDrawInfo& other )
    : vbo_vert() // no need to set these, they will be automatically generated when needed
    , vbo_uv()
    , vbo_color()
    , prevMode( PrimitiveDrawModeT::Wireframe )
{ }

PrimitiveDrawInfo::PrimitiveDrawInfo( PrimitiveDrawInfo && other_rref )
    : vbo_vert( std::move( other_rref.vbo_vert ) )
    , vbo_uv( std::move( other_rref.vbo_uv ) )
    , vbo_color( std::move( other_rref.vbo_color ) )
    , prevMode( other_rref.prevMode )
{ }

PrimitiveDrawInfo& PrimitiveDrawInfo::operator=( PrimitiveDrawInfo && other_rref ) {
    vbo_vert = std::move( other_rref.vbo_vert );
    vbo_uv = std::move( other_rref.vbo_uv );
    vbo_color = std::move( other_rref.vbo_color );
    prevMode = other_rref.prevMode;
    
    return *this;
}
    
PrimitiveDrawInfo& PrimitiveDrawInfo::operator=( [[maybe_unused]] const PrimitiveDrawInfo& other ) {
    // no need to set these, they'll be automatically generated when needed
    Clear();
    return *this;
}

void PrimitiveDrawInfo::Clear( void ) {
    if ( vbo_vert )
        vbo_vert->Clear();
        
    if ( vbo_uv )
        vbo_uv->Clear();
        
    if ( vbo_color )
        vbo_color->Clear();
}

void Primitive::UpdateVBO( PrimitiveDrawInfo& primDrawInfo ) {
    if ( primDrawInfo.vbo_vert )
        primDrawInfo.vbo_vert->Clear();
}

void Primitive::DrawVBO( const DrawInfo& drawInfo, const PrimitiveDrawInfo& primDrawInfo ) const {
    if ( !primDrawInfo.vbo_vert || !primDrawInfo.vbo_vert->Finalized() )
        return;

    const bool textured = primDrawInfo.vbo_uv && primDrawInfo.vbo_uv->Finalized() && drawInfo.material != nullptr && drawInfo.material->GetDiffuse() != nullptr;
    
    const PrimitiveDrawModeT drawmode = static_cast< PrimitiveDrawModeT >( globalVals.GetUInt( gval_d_boundsMode ) );
    
    if ( textured ) {
        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE  );
        shaders->SetTexture( drawInfo.material->GetDiffuse()->GetID() );
        shaders->SetUniform1f("fWeight", 0.1f );
        shaders->SetAttrib("vUV", *primDrawInfo.vbo_uv );
        shaders->SetUniform4f("vColor", drawInfo.colorize.r, drawInfo.colorize.g, drawInfo.colorize.b, drawInfo.opacity );
    } else {
        switch ( drawmode ) {
            case PrimitiveDrawModeT::Gradient:
                if ( primDrawInfo.vbo_color ) {
                    shaders->UseProg(  GLPROG_GRADIENT  );
                    shaders->SetAttrib( "vColor", *primDrawInfo.vbo_color );
                    break;
                }
                FALLTHROUGH;
            case PrimitiveDrawModeT::Wireframe:
                shaders->UseProg(  GLPROG_MINIMAL  );
                shaders->SetUniform4f("vColor", drawInfo.colorize.r, drawInfo.colorize.g, drawInfo.colorize.b, drawInfo.opacity );
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                break;
            case PrimitiveDrawModeT::Solid:
                FALLTHROUGH;
            default:
                shaders->UseProg(  GLPROG_MINIMAL  );
                shaders->SetUniform4f("vColor", drawInfo.colorize.r, drawInfo.colorize.g, drawInfo.colorize.b, drawInfo.opacity );
                break;
        }
        
    }

    shaders->SendData_Matrices();
    shaders->SetAttrib("vPos", *primDrawInfo.vbo_vert );
    shaders->DrawArrays( GL_TRIANGLE_STRIP, 0, primDrawInfo.vbo_vert->Num() );
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

Vec3f Primitive::GetOrigin( void ) const {
    return origin;
}

void Primitive::MapLoad( FileMap& saveFile, [[maybe_unused]] const std::vector< std::string >& string_pool ){
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    SetOrigin( saveFile.parser.ReadVec3f() );
    saveFile.parser.ReadVec3f( angles );
}

void Primitive::MapSave( FileMap& saveFile, [[maybe_unused]] LinkList< std::string >& string_pool ) const {
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteVec3f( GetOrigin() );
    saveFile.parser.WriteVec3f( angles );
}
    
void Primitive::SetYaw( const float ang ) {
    angles.z = Maths::NormalizeAngle( ang );
    
    if ( owner ) {
        owner->RotateTo( angles );
        owner->SetRotated();
    }
}

void Primitive::SetRoll( const float ang ) {
    angles.x = Maths::NormalizeAngle( ang );

    if ( owner ) {
        owner->RotateTo( angles );
        owner->SetRotated();
    }
}

void Primitive::SetPitch( const float ang ) {
    angles.y = Maths::NormalizeAngle( ang );

    if ( owner ) {
        owner->RotateTo( angles );
        owner->SetRotated();
    }
}

void Primitive::SetAngle( const float amtx, const float amty, const float amtz ) {
    angles.x = Maths::NormalizeAngle( amtx );
    angles.y = Maths::NormalizeAngle( amty );
    angles.z = Maths::NormalizeAngle( amtz );

    if ( owner ) {
        owner->RotateTo( angles );
        owner->SetRotated();
    }
}

void Primitive::SetOrigin( const Vec3f& _origin ) {
    origin = _origin;

    if ( owner ) {
        owner->SetMoved();
    }
}

void Primitive::SnapYaw( const float to_angle ) {
    SetYaw( Round( angles.z / to_angle ) * to_angle );
}

void Primitive::SnapRoll( const float to_angle ) {
    SetRoll( Round( angles.x / to_angle ) * to_angle );
}

void Primitive::SnapPitch( const float to_angle ) {
    SetPitch( Round( angles.y / to_angle ) * to_angle );
}

bool Primitive::IsRotated( void ) const {
    return !Maths::Approxf( angles, Vec3f(0,0,0), EP_THOUSANDTH );
}

void Primitive::PackVBO( PrimitiveDrawInfo& primDrawInfo ) const {
    if ( !primDrawInfo.vbo_vert ) {
        primDrawInfo.vbo_vert = std::make_unique< VBO<float> >( 3, VBOChangeFrequencyT::RARELY );
    }
    
    const PrimitiveDrawModeT drawMode = static_cast< PrimitiveDrawModeT >( globalVals.GetUInt( gval_d_boundsMode ) );
    
    // we must have color VBO instantiated prior to packing verts, because PackVBO_vert may use it.
    if ( drawMode != PrimitiveDrawModeT::Wireframe ) {
        if ( !primDrawInfo.vbo_color ) {
            primDrawInfo.vbo_color = std::make_unique< VBO<float> >( 4, VBOChangeFrequencyT::RARELY );
        }
    }
    
    // we must pack verts before color, so we know how many vertices to pack colors for
    if ( primDrawInfo.prevMode != drawMode || !primDrawInfo.vbo_vert->Finalized() ) {
        PackVBO_vert( primDrawInfo, drawMode );
    }
    
    // finally, we have one last chance to pack the colors
    if ( drawMode != PrimitiveDrawModeT::Wireframe ) {
        if ( primDrawInfo.prevMode != drawMode || !primDrawInfo.vbo_color->Finalized() ) {
            PackVBO_color( primDrawInfo, drawMode );
        }
    }
    
    primDrawInfo.prevMode = drawMode;
}

void Primitive::PackVBO_color( PrimitiveDrawInfo& primDrawInfo, const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_color );
    
    if ( drawMode != PrimitiveDrawModeT::Solid )
        return;
        
    if ( primDrawInfo.vbo_vert->Num() < 1 )
        return;
        
    primDrawInfo.vbo_color->PackRepeatedly( primDrawInfo.vbo_vert->Num(), Color4f(0,1,0) );
    primDrawInfo.vbo_color->MoveToVideoCard();
}

void Primitive::PackVBO_uv( [[maybe_unused]] PrimitiveDrawInfo& primDrawInfo ) const {
    ERR("Primitive::PackVBO_uv: No uv pack for shape %u", static_cast<PrimitiveShapeT_BaseType>(GetShapeType()) );
}

void Primitive::SetOwnerStatusMovedAndUpdateVBO( void ) {
    if ( owner ) {
        owner->bounds.UpdateVBO();
        owner->SetMoved();
    }
}

Primitive_Sphere::Primitive_Sphere( void )
    : Primitive_Derived< Sphere, PrimitiveShapeT::SPHERE >( Sphere( Vec3f(0,0,0), SPHERE_RADIUS_DEFAULT ) )
{ }

Primitive_Sphere::Primitive_Sphere( const Model& _modelToEncompass )
    : Primitive_Derived< Sphere, PrimitiveShapeT::SPHERE >( Sphere( _modelToEncompass ) )
{ }

Primitive_Sphere::Primitive_Sphere( const Sphere& _sphere )
    : Primitive_Derived< Sphere, PrimitiveShapeT::SPHERE >( Sphere( Vec3f(0,0,0), _sphere.radius ) )
{
    origin = _sphere.origin;
}

Primitive_Sphere::Primitive_Sphere( const float _radius )
    : Primitive_Derived< Sphere, PrimitiveShapeT::SPHERE >( Sphere( Vec3f(0,0,0), _radius ) )
{ }

void Primitive_Sphere::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    
    PackVBO_Sphere( 15, shape.radius, Vec3f(0,0,0), *primDrawInfo.vbo_vert, primDrawInfo.vbo_color.get() );
}

void Primitive_Sphere::SetRadius( const float _radius ) {
    shape.radius = _radius;
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_Sphere::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ){
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    saveFile.parser.ReadFloat( shape.radius );
}

void Primitive_Sphere::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteFloat( shape.radius );
}

void Primitive_Sphere::SetBoundsFromModel( [[maybe_unused]]const Model& model ) {
    shape.SetRadiusFromModel( model );
}

void Primitive_Sphere::SetShape( const Sphere& _shape ) {
    origin = _shape.origin;
    shape.origin = _shape.origin;
    shape.radius = _shape.radius;
    if ( owner ) {
        owner->bounds.UpdateVBO();
    }
}

Sphere Primitive_Sphere::GetShape( void ) const {
    return { GetOrigin(), shape.radius };
}

std::shared_ptr< Primitive > Primitive_Sphere::Clone_Shared( void ) const {
    return std::make_shared< Primitive_Sphere >(*this);
}

float Primitive_Sphere::GetRadius( void ) const {
    return shape.radius;
}

Primitive_Box3D::Primitive_Box3D( void )
    : Primitive_Derived< Box3D, PrimitiveShapeT::BOX3D >( Box3D( BOX_BOUNDS_DEFAULT ) )
{ }

Primitive_Box3D::Primitive_Box3D( const float side_lengths )
    : Primitive_Derived< Box3D, PrimitiveShapeT::BOX3D >( Box3D( side_lengths ) )
{ }

Primitive_Box3D::Primitive_Box3D( const Box3D& _box ) 
    : Primitive_Derived< Box3D, PrimitiveShapeT::BOX3D >( Box3D( _box ) )
{ }

Primitive_Box3D::Primitive_Box3D( Box3D && box_rref ) 
    : Primitive_Derived< Box3D, PrimitiveShapeT::BOX3D >( Box3D( box_rref ) )
{ }

void Primitive_Box3D::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    
    PackVBO_BoxVerts( GetShape_Local(), *primDrawInfo.vbo_vert, primDrawInfo.vbo_color.get() );
}

void Primitive_Box3D::PackVBO_uv( PrimitiveDrawInfo& primDrawInfo ) const {
    if ( !primDrawInfo.vbo_uv ) {
        primDrawInfo.vbo_uv = std::make_unique< VBO<float> >( 2, VBOChangeFrequencyT::RARELY );
    }
    
    if ( primDrawInfo.vbo_uv->Finalized() )
        return;

    PackVBO_BoxUVs( *primDrawInfo.vbo_uv );
}

void Primitive_Box3D::SetBoundsFromModel( const Model& model ) {
    if ( model.NumMeshes() < 1 ) {
        ERR("Primitive_Box3D::SetBoundsFromModel: model has no meshmodel has no mesh\n");
        return;
    }
    
    AABox3D aabox( model.GetBounds() );

    // extend box outward just a tad, this helps with collision and also ensures that it is a valid box shape for flat surfaces
    aabox.min.x -= 0.01f; // one centimeter
    aabox.max.x += 0.01f;
    aabox.min.y -= 0.01f;
    aabox.max.y += 0.01f;
    aabox.min.z -= 0.01f;
    aabox.max.z += 0.01f;
    
    shape.Set( aabox );
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_Box3D::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    saveFile.ReadBox3D( shape );
}


void Primitive_Box3D::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    saveFile.WriteBox3D( shape );
}

Box3D Primitive_Box3D::GetShape( void ) const {
    if ( !IsRotated() ) {
        return shape + origin;
    } else {
        return shape * Quat::FromAngles_ZYX( GetAngles() ) + origin;
    }
}

std::shared_ptr< Primitive > Primitive_Box3D::Clone_Shared( void ) const {
    return std::make_shared< Primitive_Box3D >(*this);
}

Primitive_AABox3D::Primitive_AABox3D( void )
    : Primitive_Derived< AABox3D, PrimitiveShapeT::AABOX3D >( AABox3D( BOX_BOUNDS_DEFAULT ) )
{ }

Primitive_AABox3D::Primitive_AABox3D( const AABox3D& _aabox )
    : Primitive_Derived< AABox3D, PrimitiveShapeT::AABOX3D >( AABox3D( _aabox ) )
{ }

Primitive_AABox3D::Primitive_AABox3D( const Model& _modelToEncompass )
    : Primitive_Derived< AABox3D, PrimitiveShapeT::AABOX3D >( AABox3D( _modelToEncompass ) )
{ }

Primitive_AABox3D::Primitive_AABox3D( const float bounds )
    : Primitive_Derived< AABox3D, PrimitiveShapeT::AABOX3D >( AABox3D( bounds ) )
{ }

void Primitive_AABox3D::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    
    PackVBO_BoxVerts( Box3D( GetShape_Local() ), *primDrawInfo.vbo_vert, primDrawInfo.vbo_color.get() );
}

void Primitive_AABox3D::PackVBO_uv( PrimitiveDrawInfo& primDrawInfo ) const {
    if ( !primDrawInfo.vbo_uv ) {
        primDrawInfo.vbo_uv = std::make_unique< VBO<float> >( 2, VBOChangeFrequencyT::RARELY );
    }
    
    if ( primDrawInfo.vbo_uv->Finalized() )
        return;
        
    PackVBO_BoxUVs( *primDrawInfo.vbo_uv );
}

bool Primitive_AABox3D::Intersects( const Vec3f& _point ) const {
    if ( IsRotated() ) {
        return Collision3D::TestPoint::Intersects( _point, GetOrientedBox3D() );
    } else {
        return Collision3D::TestPoint::Intersects( _point, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const Box3D& _box ) const {
    if ( IsRotated() ) {
        return Box3D::Intersects( _box, GetOrientedBox3D() );
    } else {
        return Box3D::Intersects( _box, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const AABox3D& _aabox ) const {
    if ( IsRotated() ) {
        return AABox3D::Intersects( _aabox, GetOrientedBox3D() );
    } else {
        return AABox3D::Intersects( _aabox, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const Sphere& _sphere ) const {
    if ( IsRotated() ) {
        return Sphere::Intersects( _sphere, GetOrientedBox3D() );
    } else {
        return Sphere::Intersects( _sphere, GetTranslatedAABox3D() );
    }
}
bool Primitive_AABox3D::Intersects( const Capsule& _capsule ) const {
    if ( IsRotated() ) {
        return Capsule::Intersects( _capsule, GetOrientedBox3D() );
    } else {
        return Capsule::Intersects( _capsule, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const Line3f& _line ) const {
    if ( IsRotated() ) {
        return Line3f::Intersects( _line, GetOrientedBox3D() );
    } else {
        return Line3f::Intersects( _line, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const Primitive& _primitive ) const {
    if ( IsRotated() ) {
        return _primitive.Intersects( GetOrientedBox3D() );
    } else {
        return _primitive.Intersects( GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const SemiCylinder& _semiCyl ) const {
    if ( IsRotated() ) {
        return Collision3D::TestSemiCylinder::Intersects( _semiCyl, GetOrientedBox3D() );
    } else {
        return Collision3D::TestSemiCylinder::Intersects( _semiCyl, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const Cylinder& _cyl ) const {
    if ( IsRotated() ) {
        return Cylinder::Intersects( _cyl, GetOrientedBox3D() );
    } else {
        return Cylinder::Intersects( _cyl, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::Intersects( const Polygon3D& _polygon ) const {
    if ( IsRotated() ) {
        return Polygon3D::Intersects( _polygon, GetOrientedBox3D() );
    } else {
        return Polygon3D::Intersects( _polygon, GetTranslatedAABox3D() );
    }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Primitive_AABox3D::Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
        ERR("Not Implemented: Primitive_AABox3D::Intersects( const Model )\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const AABox3D& _aabox ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepToAABox3D\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Box3D& _box ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepToBox3D\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Capsule& _capsule ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepToCapsule( re\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const SemiCylinder& _semiCyl ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepToSemiCylinder( re\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Cylinder& _cyl ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepToCylinder\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Polygon3D& _polygon ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepToPolygon3D\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Line3f& _line ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepWithLine3f\n"); //toimplement
        return false;
    }

    bool Primitive_AABox3D::SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
        ERR("Not Implemented: Primitive_AABox3D::SweepTo( model )\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Sphere& _sphere ) const {
    if ( IsRotated() ) {
        return Sphere::SweepTo( report, _sphere, _sphere.origin+_vel, GetOrientedBox3D() );
    } else {
        return Sphere::SweepTo( report, _sphere, _sphere.origin+_vel, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::SweepWith( CollisionReport3D& report, const Vec3f& _vel, const Vec3f& _point ) const {
    if ( IsRotated() || ( report.calc != CollisionCalcsT::None) ) {
        return Collision3D::TestPoint::SweepTo( report, _point, _point+_vel, GetOrientedBox3D() );
    } else {
        return Collision3D::TestPoint::SweepTo( report, _point, _point+_vel, GetTranslatedAABox3D() );
    }
}

bool Primitive_AABox3D::SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Primitive& _primitive ) const {
    if ( IsRotated() || ( report.calc != CollisionCalcsT::None) ) {
        return _primitive.SweepWith( report, _vel, GetOrientedBox3D() );
    } else {
        return _primitive.SweepWith( report, _vel, GetTranslatedAABox3D() );
    }
}

void Primitive_AABox3D::SetBoundsFromModel( const Model& model ) {
    shape.SetBoundsFromModel( model );
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_AABox3D::SetShape( const AABox3D& _aabox ) {
    shape = _aabox;
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_AABox3D::SetShape( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z ) {
    shape.SetBounds( min_x, max_x, min_y, max_y, min_z, max_z );
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_AABox3D::SetShape( const float bounds ) {
    shape.SetBounds( bounds );
    SetOwnerStatusMovedAndUpdateVBO();
}

AABox3D Primitive_AABox3D::GetTranslatedAABox3D( void ) const {
    return shape + origin;
}

Box3D Primitive_AABox3D::GetOrientedBox3D( void ) const {
    if ( IsRotated() ) {
        return Box3D( shape ) * Quat::FromAngles_ZYX( GetAngles() ) + origin;
    } else {
        return Box3D( shape + origin );
    }
}

void Primitive_AABox3D::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    saveFile.ReadAABox3D( shape );
}


void Primitive_AABox3D::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    saveFile.WriteAABox3D( shape );
}

std::shared_ptr< Primitive > Primitive_AABox3D::Clone_Shared( void ) const {
    return std::make_shared< Primitive_AABox3D >(*this);
}


Primitive_Point::Primitive_Point( const Vec3f& _vec )
    : Primitive_Derived< Vec3f, PrimitiveShapeT::VEC3F >( _vec )
{ }

void Primitive_Point::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    
    primDrawInfo.vbo_vert->Pack( GetShape() );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
bool Primitive_Point::Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
    ERR( "Primitive_Line3f::SweepTo: Not Implemented.\n");
    return false;
}
#pragma GCC diagnostic pop

bool Primitive_Point::SweepTo( CollisionReport3D& report, const Vec3f& _vel, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
    return Collision3D::TestPoint::SweepToModel( report, GetShape(), GetShape()+_vel, _model, _model_angles, _model_origin, ModelCollisionT::CLOSEST_POLY );
}

void Primitive_Point::SetBoundsFromModel( [[maybe_unused]] const Model& model ) {
    ERR( "Vec3f::SetBoundsFromModel: A line cannot encapsellate a model.\n" );
    return;
}

Vec3f Primitive_Point::GetShape( void ) const {
    return shape + origin;
}

void Primitive_Point::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    ParserBinary parser( saveFile );
    parser.ReadVec3f( shape );
}

void Primitive_Point::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    ParserBinary parser( saveFile );
    parser.WriteVec3f( shape );
}

std::shared_ptr< Primitive > Primitive_Point::Clone_Shared( void ) const {
    return std::make_shared< Primitive_Point >(*this);
}


Primitive_Line3f::Primitive_Line3f( const LineTypeT _line_type )
    : Primitive_Derived< Line3f, PrimitiveShapeT::LINE3F >( Line3f( _line_type ) )
{ }

Primitive_Line3f::Primitive_Line3f( const Line3f& _line )
    : Primitive_Derived< Line3f, PrimitiveShapeT::LINE3F >( _line )
{ }

void Primitive_Line3f::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    
    const auto _line = GetShape();
    primDrawInfo.vbo_vert->Pack( _line.vert[0] );
    primDrawInfo.vbo_vert->Pack( _line.vert[1] );
}

bool Primitive_Line3f::Intersects( const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
    return Line3f::ToModel( GetShape(), _model, _model_angles, _model_origin );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
bool Primitive_Line3f::SweepTo( CollisionReport3D& report, const Vec3f& _vel_ignored, const Model& _model, const Vec3f& _model_angles, const Vec3f& _model_origin ) const {
    ERR( "Primitive_Line3f::SweepTo: Not Implemented.\n");
    return false;
}
#pragma GCC diagnostic pop

void Primitive_Line3f::SetBoundsFromModel( [[maybe_unused]] const Model& model ) {
    ERR( "Line3f::SetBoundsFromModel: A line cannot encapsellate a model.\n" );
    return;
}

Line3f Primitive_Line3f::GetShape( void ) const {
    if ( !IsRotated() ) {
        return shape + origin;
    } else {
        return shape * Quat::FromAngles_ZYX( GetAngles() ) + origin;
    }
}

void Primitive_Line3f::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    ParserBinary parser( saveFile );
    parser.ReadVec3f( shape.vert[0] );
}


void Primitive_Line3f::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    ParserBinary parser( saveFile );
    parser.WriteVec3f( shape.vert[1] );
}

std::shared_ptr< Primitive > Primitive_Line3f::Clone_Shared( void ) const {
    return std::make_shared< Primitive_Line3f >(*this);
}

Primitive_Cylinder::Primitive_Cylinder( void )
    : Primitive_Derived< Cylinder, PrimitiveShapeT::CYLINDER >( Cylinder() )
{ }

Primitive_Cylinder::Primitive_Cylinder( const Cylinder& _cyl )
    : Primitive_Derived< Cylinder, PrimitiveShapeT::CYLINDER >( _cyl )
{
    origin = _cyl.origin;
}

void Primitive_Cylinder::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    primDrawInfo.vbo_vert->Clear();
    if ( primDrawInfo.vbo_color )
        primDrawInfo.vbo_color->Clear();
    
    PackVBO_Cylinder( 15, shape.radius, shape.length, Vec3f(0,0,0), shape.normal, *primDrawInfo.vbo_vert, primDrawInfo.vbo_color.get() );
    
    primDrawInfo.vbo_vert->MoveToVideoCard();
    if ( primDrawInfo.vbo_color )
        primDrawInfo.vbo_color->MoveToVideoCard();
}

void Primitive_Cylinder::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ){
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    saveFile.parser.ReadFloat( shape.radius );
    saveFile.parser.ReadFloat( shape.length );
    saveFile.parser.ReadVec3f( shape.normal );
}

void Primitive_Cylinder::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteFloat( shape.radius );
    saveFile.parser.WriteFloat( shape.length );
    saveFile.parser.WriteVec3f( shape.normal );
}

void Primitive_Cylinder::SetBoundsFromModel( [[maybe_unused]]const Model& model ) {
    const AABox3D bounds = model.GetBounds();
    const float minbound = std::fminf( bounds.min.x, bounds.min.y );
    const float maxbound = std::fminf( bounds.max.x, bounds.max.y );
    
    shape.radius = (maxbound - minbound) / 2;
    shape.length = bounds.max.z - bounds.min.z;
}

void Primitive_Cylinder::SetShape( const Cylinder& _shape ) {
    origin = _shape.origin;
    shape.origin = _shape.origin;
    shape.radius = _shape.radius;
    shape.length = _shape.length;
    shape.normal = _shape.normal;
    if ( owner ) {
        owner->bounds.UpdateVBO();
    }
}

Cylinder Primitive_Cylinder::GetShape( void ) const {
    return { GetOrigin(), shape.normal, shape.radius, shape.length };
}

void Primitive_Cylinder::SetRadius( const float _radius ) {
    shape.radius = _radius;
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_Cylinder::SetLength( const float _length ) {
    shape.length = _length;
    SetOwnerStatusMovedAndUpdateVBO();
}

std::shared_ptr< Primitive > Primitive_Cylinder::Clone_Shared( void ) const {
    return std::make_shared< Primitive_Cylinder >(*this);
}


Primitive_Capsule::Primitive_Capsule( void )
    : Primitive_Derived< Capsule, PrimitiveShapeT::CAPSULE >( Capsule() )
{ }

Primitive_Capsule::Primitive_Capsule( const Capsule& _cap )
    : Primitive_Derived< Capsule, PrimitiveShapeT::CAPSULE >( _cap )
{
    origin = _cap.origin;
}

void Primitive_Capsule::PackVBO_vert( PrimitiveDrawInfo& primDrawInfo, [[maybe_unused]] const PrimitiveDrawModeT drawMode ) const {
    ASSERT( primDrawInfo.vbo_vert );
    primDrawInfo.vbo_vert->Clear();
    if ( primDrawInfo.vbo_color )
        primDrawInfo.vbo_color->Clear();
    
    PackVBO_Capsule( 15, shape.radius, shape.length, Vec3f(0,0,0), shape.normal, *primDrawInfo.vbo_vert, primDrawInfo.vbo_color.get() );
    
    primDrawInfo.vbo_vert->MoveToVideoCard();
    if ( primDrawInfo.vbo_color )
        primDrawInfo.vbo_color->MoveToVideoCard();
}

void Primitive_Capsule::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ){
    Primitive::MapLoad( saveFile, string_pool );
    
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    saveFile.parser.ReadFloat( shape.radius );
    saveFile.parser.ReadFloat( shape.length );
    saveFile.parser.ReadVec3f( shape.normal );
}

void Primitive_Capsule::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    Primitive::MapSave( saveFile, string_pool );
    
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );

    saveFile.parser.WriteFloat( shape.radius );
    saveFile.parser.WriteFloat( shape.length );
    saveFile.parser.WriteVec3f( shape.normal );
}

void Primitive_Capsule::SetBoundsFromModel( [[maybe_unused]]const Model& model ) {
    const AABox3D bounds = model.GetBounds();
    const float minbound = std::fminf( bounds.min.x, bounds.min.y );
    const float maxbound = std::fminf( bounds.max.x, bounds.max.y );
    
    shape.radius = (maxbound - minbound) / 2;
    shape.length = bounds.max.z - bounds.min.z;
}

void Primitive_Capsule::SetShape( const Capsule& _shape ) {
    origin = _shape.origin;
    shape.origin = _shape.origin;
    shape.radius = _shape.radius;
    shape.length = _shape.length;
    shape.normal = _shape.normal;
    if ( owner ) {
        owner->bounds.UpdateVBO();
    }
}

Capsule Primitive_Capsule::GetShape( void ) const {
    return { GetOrigin(), shape.normal, shape.radius, shape.length };
}

void Primitive_Capsule::SetRadius( const float _radius ) {
    shape.radius = _radius;
    SetOwnerStatusMovedAndUpdateVBO();
}

void Primitive_Capsule::SetLength( const float _length ) {
    shape.length = _length;
    SetOwnerStatusMovedAndUpdateVBO();
}

std::shared_ptr< Primitive > Primitive_Capsule::Clone_Shared( void ) const {
    return std::make_shared< Primitive_Capsule >(*this);
}
