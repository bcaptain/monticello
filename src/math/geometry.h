// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_MATH_GEOMETRY_H_
#define SRC_MATH_GEOMETRY_H_

#include "./geometry/2d/point.h"
#include "./geometry/2d/lineseg.h"
#include "./geometry/2d/circle.h"
#include "./geometry/2d/poly.h"
#include "./geometry/2d/aabb.h"
#include "./geometry/3d/aabox.h"
#include "./geometry/3d/box.h"
#include "./geometry/3d/capsule.h"
#include "./geometry/3d/cone.h"
#include "./geometry/3d/cylinder.h"
#include "./geometry/3d/ellipse.h"
#include "./geometry/3d/line.h"
#include "./geometry/3d/plane.h"
#include "./geometry/3d/point.h"
#include "./geometry/3d/poly.h"
#include "./geometry/3d/semiCylinder.h"
#include "./geometry/3d/sphere.h"
#include "./geometry/3d/tri.h"

#endif  // SRC_MATH_GEOMETRY_H_
