// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./bounds.h"
#include "../game.h"
#include "../sound.h"
#include "../rendering/models/model.h"
#include "../entities/entity.h"
#include "../rendering/materialManager.h"
#include "../math/collision/collision.h"
#include "../entities/actor.h"

const Color4f BBOX_COLOR_UNSELECTED( 0,0,1,1 ); // blue
const Color4f BBOX_COLOR_SELECTED( 0.5f,0.7f,0.9f, 1.0f );
const uint DEFAULT_SLIDE_NUM = 4;

const float MINIMUM_FRICTION_TO_APPLY = 0.05f;

extern const float FPS_CAP;
extern const float EP_ONE_CM_PER_FRAME;
extern const float EP_ONE_CM_PER_FRAME_SQ;
extern const float EP_HALF_CM_PER_FRAME;
extern const float EP_HALF_CM_PER_FRAME_SQ;

const float MIN_SPEED_CM_PER_SEC = 0.01f;
const float MIN_SPEED_CM_PER_FRAME = MIN_SPEED_CM_PER_SEC/60.0f;
const float MIN_SPEED_CM_PER_FRAME_SQ = MIN_SPEED_CM_PER_FRAME * MIN_SPEED_CM_PER_FRAME;

EntBounds::EntBounds( void )
    : primitive( std::make_shared< Primitive_Sphere >( SPHERE_RADIUS_DEFAULT ) )
    , primDrawInfo()
    , owner()
    , material()
    , velocity()
    , angularVelocity()
    , angularFriction()
{
}

EntBounds::EntBounds( Entity* _owner )
    : primitive( std::make_shared< Primitive_Sphere >( SPHERE_RADIUS_DEFAULT ) )
    , primDrawInfo()
    , owner(_owner)
    , material()
    , velocity()
    , angularVelocity()
    , angularFriction()
{
    primitive->SetOwner( _owner );
}

EntBounds::EntBounds( const EntBounds& other )
    : primitive( other.primitive->Clone_Shared() )
    , primDrawInfo() // this will be generated automatically
    , owner() // owner stays the same
    , material( other.material )
    , velocity( other.velocity )
    , angularVelocity( other.angularVelocity )
    , angularFriction( other.angularFriction )
{
}

EntBounds::EntBounds( EntBounds&& other_rref )
    : primitive( std::move( other_rref.primitive ) )
    , primDrawInfo( std::move( other_rref.primDrawInfo ) )
    , owner() // owner stays the same
    , material( other_rref.material )
    , velocity( other_rref.velocity )
    , angularVelocity( other_rref.angularVelocity )
    , angularFriction( other_rref.angularFriction )
{
}

void EntBounds::SetOwner( Entity* _owner ) {
    owner = _owner;
    primitive->SetOwner( _owner );
}

EntBounds& EntBounds::operator=( EntBounds&& other_rref ) {
    primitive = std::move( other_rref.primitive );
    primitive->SetOwner( owner );
    primDrawInfo = std::move( other_rref.primDrawInfo );
    // owner stays the same
    material = other_rref.material;
    velocity = other_rref.velocity;
    angularVelocity = other_rref.angularVelocity;
    angularFriction = other_rref.angularFriction;
    return *this;
}

EntBounds& EntBounds::operator=( const EntBounds& other ) {
    ASSERT( primitive );
    ASSERT( other.primitive );
    *primitive = *other.primitive; // primitive's owner hasn't changed
    // primDrawInfo will be generated automatically when needed
    // owner staus the same
    material = other.material;
    velocity = other.velocity;
    angularVelocity = other.angularVelocity;
    angularFriction = other.angularFriction;
    return *this;
}

void EntBounds::DrawVBO( void ) const {
    DrawVBO( GetDrawColor() );
}

void EntBounds::DrawVBO( const Color4f& color ) const {
    DrawInfo drawInfo;
    const float DEFAULT_BOUNDS_OPACITY = 0.33f;
    drawInfo.opacity = globalVals.GetFloat( gval_d_boundsAlpha, DEFAULT_BOUNDS_OPACITY );
    drawInfo.material = material;
    drawInfo.colorize = color;
    
    primitive->DrawVBO( drawInfo, primDrawInfo );
}

void EntBounds::PackVBO( void ) {
    primitive->PackVBO( primDrawInfo );

    if ( material ) {
        primitive->PackVBO_uv( primDrawInfo );
    }
}

Color4f EntBounds::GetDrawColor( void ) const {
    #ifdef MONTICELLO_EDITOR
        if ( owner ) {
            if ( owner->GetSelectedByEditor() ) {
                return BBOX_COLOR_SELECTED;
            }
        }
    #endif // MONTICELLO_EDITOR
    
    return BBOX_COLOR_UNSELECTED;
}

Vec3f EntBounds::GetBoundsFromSideLengths( const Vec3f& side_lengths ) const {
    Vec3f bounds( side_lengths );
    bounds.x = fabsf( bounds.x / 2 );
    bounds.y = fabsf( bounds.y / 2 );
    bounds.z = fabsf( bounds.z / 2 );
    
    bool invalidDimensions = false;
    if ( bounds.z < EP_ONE_CENTIMETER ) {
        invalidDimensions = true;
        bounds.z = EP_ONE_CENTIMETER;
    }
    
    if ( bounds.y < EP_ONE_CENTIMETER ) {
        invalidDimensions = true;
        bounds.y = EP_ONE_CENTIMETER;
    }
    
    if ( bounds.z < EP_ONE_CENTIMETER ) {
        invalidDimensions = true;
        bounds.z = EP_ONE_CENTIMETER;
    }
    
    if ( invalidDimensions ) {
        std::string err = "Invalid entity bounds: \"";
        err += std::to_string( side_lengths.x );
        err += ",";
        err += std::to_string( side_lengths.y );
        err += ",";
        err += std::to_string( side_lengths.z );
        err += "\"";
        
        if ( owner ) {
            err += " for entity";
            err += owner->GetEntName();
        }
        
        ERR("%s. Settimg to mimumums.", err.c_str() );
    }
    
    return bounds;
}

void EntBounds::SetToBox3D( const Vec3f& side_lengths ) {
    const Vec3f half_size = GetBoundsFromSideLengths( side_lengths );
    SetToBox3D( Box3D( AABox3D( -half_size.x, half_size.x, -half_size.y, half_size.y, -half_size.z, half_size.z ) ) );
}

void EntBounds::SetToBox3D( const float side_lengths ) {
    const float half_size = side_lengths;
    SetToBox3D( Box3D(AABox3D( -half_size, half_size, -half_size, half_size, -half_size, half_size ) ) );
}

void EntBounds::SetToBox3D( const Box3D& _box ) {
    if ( !IsBox3D() ) {
        const Vec3f& origin = primitive->GetOrigin();
        primitive = std::make_shared< Primitive_Box3D >( _box );
        primitive->SetOwner( owner );
        primitive->SetOrigin( origin );
    } else {
        Primitive_Box3D* box = static_cast< Primitive_Box3D* >( primitive.get() );
        box->SetShape( _box );
    }

    if ( owner ) {
        owner->SetModelOffset( Vec3f(0,0,0) );
    }
    UpdateVBO();
}

void EntBounds::SetToCyl( void ) {
    SetToCyl( Cylinder() );
}

void EntBounds::SetToCyl( const Cylinder& _cyl ) {
    if ( !IsCyl() ) {
        const Vec3f& origin = primitive->GetOrigin();
        primitive = std::make_shared< Primitive_Cylinder >( _cyl );
        primitive->SetOwner( owner );
        primitive->SetOrigin( origin );
    } else {
        Primitive_Cylinder* cyl = static_cast< Primitive_Cylinder* >( primitive.get() );
        cyl->SetShape( _cyl );
    }

    if ( owner ) {
        owner->SetModelOffset( Vec3f(0,0,0) );
    }
    UpdateVBO();
}

void EntBounds::SetToBox3D( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z ) {
    SetToBox3D( Box3D( AABox3D( min_x, max_x, min_y, max_y, min_z, max_z ) ) );
}

void EntBounds::SetToAABox3D( const AABox3D& aabox ) {
    SetToAABox3D( aabox.min.x, aabox.max.x, aabox.min.y, aabox.max.y, aabox.min.z, aabox.max.z );
}

void EntBounds::SetToAABox3D( const Vec3f& side_lengths ) {
    const Vec3f half_size = GetBoundsFromSideLengths( side_lengths );
    SetToAABox3D( -half_size.x, half_size.x, -half_size.y, half_size.y, -half_size.z, half_size.z );

}

void EntBounds::SetToAABox3D( const float side_lengths ) {
    const float half_size = side_lengths;
    SetToAABox3D( -half_size, half_size, -half_size, half_size, -half_size, half_size );
}

void EntBounds::SetToAABox3D( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z ) {
    if ( !IsAABox3D() ) {
        if ( owner && IsSphere() ) {
            auto sphere = std::static_pointer_cast< Primitive_Sphere >( primitive );
            // since we raise spheres off the ground, we need to remove that height
            
            owner->NudgeOrigin( Vec3f(0,0,-sphere->GetRadius()) );
        }
        
        const Vec3f& origin = primitive->GetOrigin();
        primitive = std::make_shared< Primitive_AABox3D >( AABox3D( min_x, max_x, min_y, max_y, min_z, max_z ) );
        primitive->SetOwner( owner );
        primitive->SetOrigin( origin );
    } else {
        Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( primitive.get() );
        box->SetShape( min_x, max_x, min_y, max_y, min_z, max_z );
    }

    if ( owner ) {
        owner->SetModelOffset( Vec3f(0,0,0) );
    }
    UpdateVBO();
}

void EntBounds::SetToSphere( const ModelInfo& modelInfo ) {
    auto const clipModel( modelInfo.GetClipModel() );
    if ( clipModel ) {
        SetToSphere( *clipModel );
    } else {
        std::string err("EntBounds::SetToSphere(ModelInfo): no model");
        if ( owner ) {
            err += " for entity ";
            err += owner->GetEntName();
        }
        ERR( "%s", err.c_str() );
    }
}

void EntBounds::SetToSphere( const Model& model_to_encompass ) {
    std::shared_ptr< Primitive_Sphere > sphere;
    
    if ( !IsSphere() ) {
        const Vec3f& origin = primitive->GetOrigin();
        sphere = std::make_shared< Primitive_Sphere >( model_to_encompass );
        sphere->SetOwner( owner );
        sphere->SetOrigin( origin );
        if ( owner ) {
            // since origin of a sphere is the center, we need to raise it off the ground
            owner->NudgeOrigin( Vec3f(0,0,sphere->GetRadius()) );
        }
        primitive = sphere;
    } else {
        sphere = std::static_pointer_cast< Primitive_Sphere >( primitive );
        sphere->SetBoundsFromModel( model_to_encompass );
    }
    
    if ( owner ) {
        owner->SetModelOffset( Vec3f(0,0,-sphere->GetRadius()) );
    }
    UpdateVBO();
}

void EntBounds::SetToAABox3D( const ModelInfo& modelInfo ) {
    auto const clipModel( modelInfo.GetClipModel() );
    if ( clipModel ) {
        SetToAABox3D( *clipModel );
    } else {
        std::string err("EntBounds::SetToAABox3D(ModelInfo): no model");
        if ( owner ) {
            err += " for entity ";
            err += owner->GetEntName();
        }
        ERR( "%s", err.c_str() );
    }
}

void EntBounds::SetToAABox3D( const Model& model_to_encompass ) {
    if ( !IsAABox3D() ) {
        if ( owner && IsSphere() ) {
            auto sphere = std::static_pointer_cast< Primitive_Sphere >( primitive );
        
            // since we raise spheres off the ground, we need to remove that height
            owner->NudgeOrigin( Vec3f(0,0,-sphere->GetRadius()) );
        }
        
        const Vec3f& origin = primitive->GetOrigin();
        primitive = std::make_shared< Primitive_AABox3D >( model_to_encompass );
        primitive->SetOwner( owner );
        primitive->SetOrigin( origin );
    } else {
        Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( primitive.get() );
        box->SetBoundsFromModel( model_to_encompass );
    }

    if ( owner ) {
        owner->SetModelOffset( Vec3f(0,0,0) );
    }
    UpdateVBO();
}

void EntBounds::SetToSphere( const float _radius ) {
    float radius = fabsf(_radius);
    if ( radius < SPHERE_RADIUS_MINIMUM )
        radius = SPHERE_RADIUS_MINIMUM;
        
    if ( !IsSphere() ) {
        if ( owner ) {
            // since origin of a sphere is the center, we need to raise it off the ground
            owner->NudgeOrigin( Vec3f(0,0,_radius) );
        }

        const Vec3f& origin = primitive->GetOrigin();
        primitive = std::make_shared< Primitive_Sphere >( radius );
        primitive->SetOwner( owner );
        primitive->SetOrigin( origin );
    } else {
        Primitive_Sphere* sphere = static_cast< Primitive_Sphere* >( primitive.get() );
        sphere->SetRadius( radius );
    }
    
    if ( owner ) {
        owner->SetModelOffset( Vec3f(0,0,-_radius) );
    }
    UpdateVBO();
}

void EntBounds::SetMaterial( const std::string& _material ) {
    SetMaterial( materialManager.GetRandom( _material.c_str() ) );
}

void EntBounds::SetMaterial( const std::shared_ptr< const Material >& _material ) {
    material = _material;
    primitive->PackVBO_uv( primDrawInfo );
}

AABox3D EntBounds::GetAABox3D_Local( void ) const {
    ASSERT( IsAABox3D() );
    const Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( primitive.get() );
    return box->GetShape_Local();
}

Sphere EntBounds::GetSphere( void ) const {
    ASSERT( IsSphere() );
    const Primitive_Sphere* box = static_cast< Primitive_Sphere* >( primitive.get() );
    return box->GetShape();
}

Box3D EntBounds::GetOrientedBox3D( void ) const {
    ASSERT( IsAABox3D() );
    const Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( primitive.get() );
    return box->GetOrientedBox3D();
}

float EntBounds::GetHeight( void ) const {
    if ( IsSphere() )
        return GetRadius();

    ASSERT( IsAABox3D() );
    const Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( primitive.get() );
    const auto & aabox = box->GetShape_Local();
    return aabox.max.z - aabox.min.z;
}

float EntBounds::GetRadius( void ) const {
    ASSERT( IsSphere() );
    const Primitive_Sphere* sphere = static_cast< Primitive_Sphere* >( primitive.get() );
    return sphere->GetRadius();
}

void EntBounds::SetToShape( const PrimitiveShapeT shape ) {
    switch ( shape ) {
        case PrimitiveShapeT::SPHERE: SetToSphere(); break;
        case PrimitiveShapeT::AABOX3D: SetToAABox3D(); break;
        case PrimitiveShapeT::BOX3D: FALLTHROUGH;
        case PrimitiveShapeT::LINE3F: FALLTHROUGH;
        case PrimitiveShapeT::CAPSULE: FALLTHROUGH;
        case PrimitiveShapeT::CYLINDER: FALLTHROUGH;
        case PrimitiveShapeT::VEC3F: FALLTHROUGH;
        default:
            ERR_DIALOG("EntBounds: invalid shape: %u\n.", static_cast<uint>( shape ));
    }
}

bool EntBounds::SetToShape( const std::string& shapeType ) {
    if ( String::LeftIs( shapeType, "auto" ) ) {
        if ( ! owner ) {
            ERR("Cannot auto set bounds, not attached to an entity.\n" );
            return false;
        }
        
        auto model = owner->GetClipModel();
        if ( ! model ) {
            ERR("Cannot auto set bounds for entity %s, it has no model.\n", owner->GetEntName().c_str() );
            return false;
        }
        
        if ( shapeType == "autoBox" ) {
            SetToAABox3D( *model );
            return true;
        }
        
        if ( shapeType == "autoSphere" ) {
            SetToSphere( *model );
            return true;
        }
    }
    
    Vec3f bounds;
    String::ToVec3f( shapeType, bounds );
    bounds = GetBoundsFromSideLengths( bounds );
    
    SetToAABox3D( -bounds.x, bounds.x, -bounds.y, bounds.y, -bounds.z, bounds.z );
    return true;
}

void EntBounds::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    
    const PrimitiveShapeT shape = static_cast< PrimitiveShapeT >( saveFile.parser.ReadUChar() );
    SetToShape( shape );
    primitive->MapLoad( saveFile, string_pool );
    
    material = MapLoadMaterial( saveFile, string_pool );
    saveFile.parser.ReadVec3f( angularVelocity );
    saveFile.parser.ReadFloat( angularFriction );
    saveFile.parser.ReadVec3f( velocity );
}

void EntBounds::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    saveFile.parser.WriteUInt8( static_cast< PrimitiveShapeT_BaseType >( GetShape() ) );
    primitive->MapSave( saveFile, string_pool );
    
    MapSaveMaterial( saveFile, string_pool, material );
    saveFile.parser.WriteVec3f( angularVelocity );
    saveFile.parser.WriteFloat( angularFriction );
    saveFile.parser.WriteVec3f( velocity );
}

bool EntBounds::IsSphere( void ) const {
    return primitive->GetShapeType() == PrimitiveShapeT::SPHERE;
}

bool EntBounds::IsBox3D( void ) const {
    return primitive->GetShapeType() == PrimitiveShapeT::BOX3D;
}

bool EntBounds::IsCyl( void ) const {
    return primitive->GetShapeType() == PrimitiveShapeT::CYLINDER;
}

bool EntBounds::IsAABox3D( void ) const {
    return primitive->GetShapeType() == PrimitiveShapeT::AABOX3D;
}

void EntBounds::SetOrigin( const Vec3f& _origin ) const {
    primitive->SetOrigin( _origin );
}

Vec3f EntBounds::GetOrigin( void ) const {
    return primitive->GetOrigin();
}

bool EntBounds::Intersects( const EntBounds& other_bounds ) const {
    return primitive->Intersects( *other_bounds.primitive );
}

void EntBounds::ApplyGravityForce( const float gravity_percent ) {
    
    // don't apply gravity if the entity is on the ground
    // if the entity is moving, we don't care whether it's on the ground becaue he might be walking down a slope or off a cliff.
    if ( Maths::Approxf( velocity.SquaredLen(), 0 )  ) {
        if ( owner ) {
            if ( owner->IsOnGround() ) {
                return;
            }
        }
    }
    
    // so things don't fall through the map (worldwide noclip)
    if ( globalVals.GetBool( gval_g_noClip ) )
        return;

    const float max_vel = globalVals.GetFloat(gval_grav_terminal) * gravity_percent;

    if ( fabsf( velocity.z ) >= fabsf( max_vel ) )
        return;

    const float meters_per_sec = globalVals.GetFloat(gval_grav_force);
    const float meters_per_frame = (meters_per_sec / FPS_CAP) * gravity_percent;
    
    velocity += globalVals.GetVec3f(gval_grav_dir) * meters_per_frame;

}

void EntBounds::DoVelocity_Slide( const uint numSlides, std::vector< CollisionItem >* all_collisions_out ) {
    float obstacle_friction = 1;
    const float original_speed = velocity.Len();

    // not going fast enough for movement
    if ( original_speed <= EP_HALF_CENTIMETER )
        return;

    // calculate the amount of velocity to apply for this frame
    const Vec3f frame_vel = velocity / FPS_CAP;
    
    // Slide will perform the move and set the resulting velocity for the frame
    Vec3f new_frame_vel = frame_vel;
    std::vector< CollisionItem > all_collisions;

    if ( velocity.z >= 0 ) {
        // if we're not being pulled down to the ground, just apply all velocity at once
        if ( owner ) {
            owner->SetOnGround( false );
        }
        Slide( new_frame_vel, numSlides, &all_collisions );
    } else {
        /* downward velocity needs to be calculated separately because:
            1.) so that we can figure out if we are "on the ground"
            2.) so we can more easily climb stairs with a perfectly lateral vector instead of one that always points into the ground
        */

        // ** do downward velocity

        Vec3f down_vel( 0, 0, new_frame_vel.z );
        // todo: Entity* downward_collision =
        Slide( down_vel, numSlides, &all_collisions );

        new_frame_vel.x += down_vel.x; // keep any lateral velocity gained
        new_frame_vel.y += down_vel.y; // keep any lateral velocity gained
        new_frame_vel.z = down_vel.z;

        bool onGroundState = false;
        //CMSG("collisions: \n");
        for ( const auto& item : all_collisions ) {
            if ( auto collisionEnt = item.ent.lock() ) {
                //CMSG("-     %i:%s\n",collisionEnt->GetIndex(),collisionEnt->GetEntName().c_str() );
                if ( collisionEnt->GetEntName() == "floor" ) {
                    onGroundState = true;
                    obstacle_friction = collisionEnt->GetFriction();
                    break;
                }
            }
        }

        if ( owner ) {
            owner->SetOnGround( onGroundState );
        }

        // ** Do lateral velocity

        Vec3f lateral_vel( new_frame_vel.x, new_frame_vel.y, 0 );
        Slide( lateral_vel, numSlides, &all_collisions );

        new_frame_vel.x = lateral_vel.x;
        new_frame_vel.y = lateral_vel.y;
        new_frame_vel.z += lateral_vel.z; // keep any vertical velocity gaines
    }

    // update overall velocity (keeping the new direction but compensated for speed loss)
    const float new_frame_speed = new_frame_vel.Len();
    const float frame_speed_lost = frame_vel.Len() - new_frame_vel.Len();
    const float new_speed = original_speed - frame_speed_lost;
    const Vec3f new_dir = new_frame_vel / new_frame_speed;

    velocity = new_dir * new_speed;

    if ( owner && all_collisions.size() > 0 ) {
        const float impact_velocity = original_speed;
        const float impact_velocity_sound_min = 10;
        const float impact_velocity_sound_max = globalVals.GetFloat(gval_grav_terminal);

        if ( impact_velocity > impact_velocity_sound_min ) {
            
            const float volume_percent = impact_velocity / impact_velocity_sound_max;
            const float volume = volume_percent * MAX_VOLUME;

            const std::string snd_impact( owner->GetImpactSound() );
            if ( ! snd_impact.empty() ) {
                soundManager.Play( snd_impact.c_str(), static_cast<int>(volume) );
            }
        }
    }

    if ( all_collisions_out ) {
        std::swap( all_collisions, *all_collisions_out );
    }

    if ( owner ) {
        //if ( owner->IsOnGround() )
        DoFriction( owner->GetFriction() * obstacle_friction );
    }
   
    if ( globalVals.GetBool( gval_d_playerSpeed ) ) {
        if ( game->IsPlayer( owner ) ) {
            Printf("Player Speed : %1.3f\n", static_cast<double>( velocity.Len() ) );
        }
    }
}

void EntBounds::DoFriction( const float friction_amount ) {
    
    if ( friction_amount < MINIMUM_FRICTION_TO_APPLY )
        return;
    
    const float speed_loss_per_sec = globalVals.GetFloat(gval_g_friction);
    const float new_speed_loss_per_sec = speed_loss_per_sec * friction_amount;
    const float speed_loss = new_speed_loss_per_sec / FPS_CAP;

    if ( velocity.x < speed_loss && velocity.x > -speed_loss ) {
        velocity.x = 0;
    } else if ( velocity.x > 0 ) {
        velocity.x -= speed_loss;
    } else if ( velocity.x < 0 ) {
        velocity.x += speed_loss;
    }

    if ( velocity.y < speed_loss && velocity.y > -speed_loss ) {
        velocity.y = 0;
    } else if ( velocity.y > 0 ) {
        velocity.y -= speed_loss;
    } else if ( velocity.y < 0 ) {
        velocity.y += speed_loss;
    }
    
}

// ** put the sphere next to the collision point unless we're already really close
inline Vec3f GetOriginMovedNextToCollisionPlane( const SweepData& coldata, const Vec3f& origin, const CollisionReport3D& report );
inline Vec3f GetOriginMovedNextToCollisionPlane( const SweepData& coldata, const Vec3f& origin, const CollisionReport3D& report ) {
    float time = report.time;
    
    // this math sets the sphere's origin based on the collision normal and the normal of movement.
    // at least EP_HALF_CM_PER_FRAME away from the collision point will always be applied.
    const float vel_len = coldata.vel.Len();
    const float time_backup_half_centimeter_per_frame = EP_HALF_CM_PER_FRAME / vel_len;
    time -= time_backup_half_centimeter_per_frame;
    
    // A right-angle trajectory will set it no more.
    // As the angle steepes from 90 degrees, we take away more of the move distance.
    const Vec3f move_trajectory = coldata.vel.GetNormalized();
    const float radian = fabsf( move_trajectory.Dot( report.norm ) );
    ASSERT( radian >= 0 );
    time -= radian * time_backup_half_centimeter_per_frame;
    
    if ( time < 0 )
        return origin;
    
    return origin + ( coldata.vel * time );
}

Vec3f CheckIncline( Vec3f& vel, const CollisionReport3D& report );
Vec3f CheckIncline( Vec3f& vel, const CollisionReport3D& report ) {
    const float maxIncline = fabsf( globalVals.GetFloat( gval_g_maxIncline ) );
    if ( Maths::Approxf( maxIncline, 0 ) )
        return vel;
        
    const Vec3f gravDir = globalVals.GetVec3f( gval_grav_dir, Vec3f(0,0,-1) );
    if ( fabsf( report.norm.DegreesBetween( -gravDir ) ) <= maxIncline )
        return vel;
        
    /* todoGravDir: this should use globalVals.GetVec3f(gval_grav_dir)
     * this would involve:
     * making a plane from the gravity direction,
     * getting the distance from the plane and the velocity point, divying up that float amount somehow, along the plane
     * projecting he velocity point onto the plane and using that for the new velocity
    */
    
    return Vec3f(
        vel.x + vel.x * fabsf( vel.z ),
        vel.y + vel.y * fabsf( vel.z ), 
        0
    );
}

inline Vec3f GetAdjustedSlideVelocity( const SweepData& coldata, const Vec3f& origin, const CollisionReport3D& report, const Entity& us, const Entity& them );
inline Vec3f GetAdjustedSlideVelocity( const SweepData& coldata, const Vec3f& origin, const CollisionReport3D& report, const Entity& us, const Entity& them ) {
    // new velocity is [ collision point ] to [ the original destination projected onto tangent plane ]
    Vec3f vel( coldata.vel );
    const Vec3f original_destination( origin + coldata.vel );
    Vec3f norm(report.norm);
    
    const bool noTrample = them.bounds.IsSphere() && derives_from<Actor>(us) && globalVals.GetBool( gval_g_noTrample );
    
    if ( noTrample ) {
        norm.x = origin.x - report.point.x;
        norm.y = origin.y - report.point.y;
        norm.z = 0.0f; //todo: this should be updated to use any gravity, not jut 0,0,-1
        norm.Normalize();
    }
    
    vel = Plane3f::GetProjectedPoint( original_destination, report.point, norm );
    vel -= report.point;
    
    if ( them.bounds.IsSphere() ) {
        if ( noTrample ) {
            vel.z = 0; //todo: this should be updated to use any gravity, not jut 0,0,-1
        }
    } else {
        vel = CheckIncline( vel, report );
    }

    return vel;
}

bool EntBounds::Slide( Vec3f &frame_vel, const uint num_slides, std::vector< CollisionItem >* all_collisions, CollisionReport3D* report_optional ) {
    // Reference Material: collision.pdf
    
    if ( num_slides < 1 )
        return false;
        
    if ( Maths::Approxf( frame_vel, Vec3f(0,0,0) ) )
        return false;

    if ( owner ) {
        if ( globalVals.GetBool( gval_g_noClip ) ) {
            owner->NudgeOrigin( frame_vel );
            return false;
        }
        
        if ( !owner->IsAwake() ) {
            return false;
        }
    }
    
    SweepData coldata;
    coldata.vel = frame_vel;
    coldata.do_collide = true;
    coldata.opts |= MatchOptsT::MATCH_IGNORE_INVISIBLE_CLIPMODEL;
    coldata.opts |= MatchOptsT::MATCH_NEAREST;
    
    // ** set up report
    
    CollisionReport3D rp( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
    CollisionReport3D *report;
    if ( report_optional ) {
        ASSERT( report_optional->ComputeNormal() );
        ASSERT( report_optional->ComputeIntersection() );
        report = report_optional;
    } else {
        report = &rp;
    }

    std::vector< CollisionItem > cols;
    if ( all_collisions ) {
        report->all_collisions_out = all_collisions;
    } else {
        report->all_collisions_out = &cols;
    }

    // ** do the slides
    
    bool collision_occured = false;
    for ( uint i=0; i<num_slides; ++i ) {
        // ** If we aren't moving, stop recursing
        if ( coldata.vel.SquaredLen() < MIN_SPEED_CM_PER_FRAME_SQ ) {
            coldata.vel.Zero();
            break;
        }
        
        // ** Sweep against the entities
        const std::shared_ptr< Entity > nonghost_collision = game->entTree->GetEntitySweepCollisions( *report, coldata, *primitive );

        // ** Bump into all of the things we hit ( could be >=0 ghosts and <=1 non-ghost )
        if ( coldata.do_collide && owner ) {
            for ( auto & item : *report->all_collisions_out ) {
                if ( auto collision = item.ent.lock() ) {
                    owner->BumpInto( *collision );
                    collision->BumpedIntoBy( *owner );
                }
            }
        }
        
        // ** if no collision, move and we're done
        if ( !nonghost_collision ) {
            primitive->SetOrigin( primitive->GetOrigin() + coldata.vel );
            frame_vel = coldata.vel; // what's left of the velocity
            break; // no non-ghost collision means we're done
        }
        
        collision_occured = true;
        
        // ** Move to the new location
        const Vec3f velToCollision = coldata.vel * report->time;
        if ( velToCollision.SquaredLen() < MIN_SPEED_CM_PER_FRAME_SQ ) {
            // we don't move if the movement vector is really small
            coldata.vel.Zero();
            frame_vel.Zero();
            break;
        }
            
        primitive->SetOrigin( GetOriginMovedNextToCollisionPlane( coldata, primitive->GetOrigin(), *report ) );
        if ( owner )
            coldata.vel = GetAdjustedSlideVelocity( coldata, primitive->GetOrigin(), *report, *owner, *nonghost_collision );
        frame_vel = coldata.vel; // what's left of the velocity
        
        // ** if we went to sleep during the slide, we're done.
        // This can happen, for example, if a projectile has hit something and wants to explode,
        // since it would be pointless to continue looping here
        if ( owner && !owner->IsAwake() ) {
            break;
        }
    }

    return collision_occured;
}

void EntBounds::DoAngularVelocity( void ) {
    if ( Maths::Approxf( angularVelocity, Vec3f(0,0,0), EP_THOUSANDTH ) )
        return;
        
    primitive->NudgeAngle( angularVelocity/FPS_CAP );

    const float speed_loss_per_second = fabsf( angularFriction );
    const float speed_loss = speed_loss_per_second / FPS_CAP;

    int axes_stationary=0;

    for ( uint i=0; i<3; ++i ) {
        if ( angularVelocity[i] > EP ) {
            angularVelocity[i] -= std::fminf( angularVelocity[i], speed_loss );
        } else if ( angularVelocity[i] < EP ) {
            angularVelocity[i] -= std::fmaxf( angularVelocity[i], -speed_loss );
        } else {
            angularVelocity[i] = 0;
            ++axes_stationary;
        }
    }
}

PrimitiveShapeT EntBounds::GetShape( void ) const {
    return primitive->GetShapeType();
}

void EntBounds::UpdateVBO( void )  {
    primitive->UpdateVBO( primDrawInfo );
}

void EntBounds::SetVelocity( const Vec3f& _velocity ) {
    velocity = _velocity;
}

void EntBounds::SetVelocity( const float x, const float y, const float z ) {
    velocity.Set( x, y, z );
}

Vec3f EntBounds::GetVelocity( void ) const {
    return velocity;
}

void EntBounds::SetAngularVelocity( const Vec3f& _angularVelocity ) {
    angularVelocity = _angularVelocity;
}

void EntBounds::SetAngularFriction( const float _angularFriction ) {
    angularFriction = _angularFriction;
}
