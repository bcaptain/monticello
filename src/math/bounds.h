// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_BOUNDS_H_
#define SRC_BOUNDS_H_

#include "../base/main.h"
#include "../math/collision/collision.h"
#include "./primitive.h"
#include "../rendering/shapes.h"

extern const Color4f BOX_COLOR_UNSELECTED;
extern const Color4f BOX_COLOR_SELECTED;
extern const uint DEFAULT_SLIDE_NUM;

class Model;
class Material;
struct ModelDrawInfo;

template< typename TYPE >
class VBO;

class EntBounds {
public:
    EntBounds( void );
    ~EntBounds( void ) = default;
    explicit EntBounds( const EntBounds& other );
    explicit EntBounds( Entity* _owner );
    EntBounds( EntBounds&& other_rref );
    
public:
    void SetOwner( Entity* _owner );
    
public:
    EntBounds& operator=( const EntBounds& other );
    EntBounds& operator=( EntBounds&& other_rref );

public:
    void SetToSphere( const float _radius = SPHERE_RADIUS_DEFAULT );
    void SetToSphere( const ModelInfo& modelInfo );
    void SetToSphere( const Model& model_to_encompass );

    void SetToShape( const PrimitiveShapeT shape );
    bool SetToShape( const std::string& shapeType );
    void SetToAABox3D( const AABox3D& aabox );
    void SetToAABox3D( const float side_lengths = BOX_BOUNDS_DEFAULT*2 );
    void SetToAABox3D( const Vec3f& size );
    void SetToAABox3D( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z );
    void SetToAABox3D( const ModelInfo& modelInfo );
    void SetToAABox3D( const Model& model_to_encompass );
    
    void SetToBox3D( const Box3D& box );
    void SetToBox3D( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z );
    void SetToBox3D( const float side_lengths = BOX_BOUNDS_DEFAULT*2 );
    void SetToBox3D( const Vec3f& size );

    void SetToCyl( void );
    void SetToCyl( const Cylinder& cyl );

    bool IsSphere( void ) const;
    bool IsBox3D( void ) const;
    bool IsAABox3D( void ) const;
    bool IsCyl( void ) const;
    PrimitiveShapeT GetShape( void ) const;

    float GetRadius( void ) const;
    float GetHeight( void ) const;
    Vec3f GetOrigin( void ) const;
    void SetOrigin( const Vec3f& _origin ) const;
    
    Sphere GetSphere( void ) const;
    AABox3D GetAABox3D_Local( void ) const;
    Box3D GetOrientedBox3D( void ) const;
    
    void UpdateVBO( void ); //!< flags the VBOs to be repacked before the next draw call
    void PackVBO( void );
    void DrawVBO( void ) const;
    void DrawVBO( const Color4f& color ) const;
    
    Color4f GetDrawColor( void ) const;

    void SetMaterial( const std::shared_ptr< const Material >& _material );
    void SetMaterial( const std::string& _material );

    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool );
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    
    void SetVelocity( const Vec3f& _velocity );
    void SetVelocity( const float x, const float y, const float z );
    Vec3f GetVelocity( void ) const;
    void DoVelocity_Slide( const uint numSlides = DEFAULT_SLIDE_NUM, std::vector< CollisionItem >* all_collisions = nullptr ); //!< all_collisions may contain duplicates if numSlides is greater than 1
    bool Slide( Vec3f &vel, const uint slide_times = 0, std::vector< CollisionItem >* all_collisions = nullptr, CollisionReport3D* report_optional = nullptr ); //! sweeps against other EntBounds until either a max recursion number is reached, the velocity is zero, or there are no more collisions. returns true if anything was hit
    void ApplyGravityForce( const float gravity_percent );
    void DoFriction( const float friction_amount );
    
    void DoAngularVelocity( void );
    void SetAngularVelocity( const Vec3f& _angularVelocity );
    void SetAngularFriction( const float _angularFriction );

    bool Intersects( const EntBounds& other_bounds ) const;
    
private:
    Vec3f GetBoundsFromSideLengths( const Vec3f& bounds ) const;
    
public:
    std::shared_ptr< Primitive > primitive;
    
private:
    PrimitiveDrawInfo primDrawInfo;
    Entity* owner;
    std::shared_ptr< const Material > material;
    Vec3f velocity; //!< in units per sec
    Vec3f angularVelocity; //!< in degrees
    float angularFriction; //!< in defrees
};

#endif  // SRC_BOUNDS_H_
