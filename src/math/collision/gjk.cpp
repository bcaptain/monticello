// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./gjk.h"
#include "../../commandsys.h" //debugging

//Note: Research the Expanding Polytope Algorithm (EPA) for getting contact point from GJK (if ever needed)

// #define GJK_DEBUG 1;

#ifdef GJK_DEBUG
void debug_DrawShape( const Vec3f *verts, const std::size_t num_verts );
void debug_DrawShape( const Vec3f *verts, const std::size_t num_verts ) {
    if ( num_verts < 2 )
        return;

    std::string str;

    // draw every line to every other, without redundancy
    for ( uint i=0; i<num_verts; ++i ) {
        for ( uint k=i+1; k<num_verts; ++k ) {
            str = "line ";
            str += std::to_string(verts[i], ',');
            str += " ";
            str += std::to_string(verts[k], ',');
            str += '\n';
            CommandSys::SendDebug(str);
        }
    }
}

void debug_DrawMinkowsky( const Vec3f *Averts_unwound, const std::size_t Anum_verts, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts );
void debug_DrawMinkowsky( const Vec3f *Averts_unwound, const std::size_t Anum_verts, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts ) {
    std::string str;
    for ( uint a=0; a<Anum_verts; ++a ) {
        for ( uint b=0; b<Bnum_verts; ++b ) {
            str = "point ";
            str += std::to_string(Averts_unwound[a] - Bverts_unwound[b], ',');
            str += '\n';
            CommandSys::SendDebug(str);
        }
    }
}
#endif // GJK_DEBUG

Vec3f Collision3D::TestGJK::GetSupport( const Vec3f& unnormalized_dir, const Vec3f *Averts_unwound, const std::size_t Anum_verts, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts ) {
    const std::size_t Aindex( Polyhedron::GetFarthestVertIndex( Averts_unwound, Anum_verts, unnormalized_dir ) );
    const std::size_t Bindex( Polyhedron::GetFarthestVertIndex( Bverts_unwound, Bnum_verts, -unnormalized_dir ) ); // opposite direction to get the maximum value
    return Averts_unwound[Aindex] - Bverts_unwound[Bindex];
}

bool Collision3D::TestGJK::Intersects( const Vec3f *Averts_unwound, const std::size_t Anum_verts, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts ) {
    // the smallest shape in 3D that we can enclose an origin with is a tetrahedron, which has 4 vertices
    std::vector< Vec3f > simplex;
    simplex.reserve(4);

    // we start with any random point in the MD
    const Vec3f any_dir( Vec3f(0,0,1) );
    simplex.emplace_back( GetSupport( any_dir, Averts_unwound, Anum_verts, Bverts_unwound, Bnum_verts ) );

    // start with -support cuz that's a dir toward the origin (doesn't need to be normalized)
    Vec3f dir( -simplex[0] );

    int cycles=0;
    do {
        #ifdef GJK_DEBUG
            CommandSys::ClearDebug();
            CommandSys::SendDebug("delallshapes\n");
            debug_DrawShape( Averts_unwound, Anum_verts );
            debug_DrawShape( Bverts_unwound, Bnum_verts );
            debug_DrawMinkowsky( Averts_unwound, Anum_verts, Bverts_unwound, Bnum_verts );
        #endif // GJK_DEBUG
        // find the vertex in the MD which is farthest in the direction
        // it becomes the next part of our enclosure shape
        simplex.emplace_back( GetSupport( dir, Averts_unwound, Anum_verts, Bverts_unwound, Bnum_verts ) );

        // if the farthest vert does not reach the origin, we can't enclose it
        if ( simplex[simplex.size()-1].Dot( dir ) < 0 )
            return false;

        if ( cycles++ >= 20 )
            return false;
    } while ( ! CheckSimplex( simplex, dir ) );

    return true;
}

bool Collision3D::TestGJK::CheckSimplex( std::vector<Vec3f>& simplex, Vec3f& search_dir_out ) {
    switch ( simplex.size() ) {
        case 2:
            CheckSimplex_LineSeg( simplex, search_dir_out );
            return false;
        case 3:
            CheckSimplex_Triangle( simplex, search_dir_out );
            return false;
        case 4:
            return CheckSimplex_Tetrahedron( simplex, search_dir_out );
        default:
            DIE("Invalid GJK simplex size.\n");
            return false; // if user wishes to not abort the program
    }
}

void Collision3D::TestGJK::CheckSimplex_LineSeg( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out ) {
    ASSERT( simplex_inout.size() == 2 );

    /* our line is BACKWARD from how we put it in. the lineseg starts at the last added position and
       ends at the position we just added: */
    Vec3f& A( simplex_inout[1] ); // the new point just added
    Vec3f& B( simplex_inout[0] );

    /* What we are doing is checking if the origin is closest to the line or one of it's endpoints.
       We know the origin can't be behind the line's end (B in the opposite direction as A) because we
       already know from the callee CheckGJKCollision() that the origin lies somewhere in the direction
       from the B to A. So we only need to check two regions: around the line, and beore A.*/

    const Vec3f AB( B - A );
    const Vec3f AO( -A );

    const bool origin_closest_to_line( AB.Dot( AO ) > 0 );
    if ( origin_closest_to_line ) {
        std::swap( A,B ); // our new simplex is a line from from A to B
        search_dir_out = AB.Cross( AO ).Cross( AB ); // new search direction is perpendicular to the edge of the line AB pointing toward the origin
    } else {
        // the new simplex is a single point at A
        B = A;
        simplex_inout.resize(1);

        search_dir_out = AO; // the origin is closer to A than the line, so we use AO as the search_dir_out
    }
}

void Collision3D::TestGJK::CheckSimplex_Triangle( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out ) {
    ASSERT( simplex_inout.size() == 3 );
    /* here, we try to find the closest part of the triangle to the origin and hope that it is fully within
       out triangle extended outward as a prism. if it's outside of that, we'll have to go backward and
       check different lines.

       the origin will lie in one of 8 sections of space:
        3 on the far side of any one of the edges as line segs.
        3 in-between those spaces.
        2 (if none of the above are true) either above the triangle's plane or below.
    */

    // always in reverse order:
    Vec3f& A( simplex_inout[2] ); // the new point just added
    Vec3f& B( simplex_inout[1] );
    Vec3f& C( simplex_inout[0] );

#ifdef GJK_DEBUG
    std::string str;

    str = "point origin 0,0,0\n";

    str += "point A ";
    str += std::to_string(simplex_inout[2], ',');
    str += '\n';

    str += "point B ";
    str += std::to_string(simplex_inout[1], ',');
    str += '\n';

    str += "point C ";
    str += std::to_string(simplex_inout[0], ',');
    str += '\n';

    str += "tri tri ";
    str += std::to_string(simplex_inout[2], ','); str += ' ';
    str += std::to_string(simplex_inout[1], ','); str += ' ';
    str += std::to_string(simplex_inout[0], ',');
    str += '\n';

    CommandSys::SendDebug(str);
#endif // GJK_DEBUG

    /* because we know that the line from C to B was already checked by CheckSimplex_LineSeg,
    we also know that the origin exists around that line in the direction of search_dir_out (which
    can be either inside the tri or not), and this means that we can immediately rule out the space
    that exists in the opposite direction the line. That rules out 3 sections out of 8, leaving us
    with only 5 sections of space to check. */

    const Vec3f AB( B - A );
    const Vec3f AC( C - A );
    const Vec3f AO( -A );
    const Vec3f ABC_NORMAL( AB.Cross( AC ) ); // triangle's plane normal (doesn't need to be normalized)

#ifdef GJK_DEBUG // lines for normals
    std::string str;

    str += "line ABC_NORMAL ";
    Vec3f norm = ABC_NORMAL.GetNormalized();
    Vec3f middle = (A+B+C) / 3;
    Vec3f b = middle + norm * 5;
    str += std::to_string(middle, ','); str += ' ';
    str += std::to_string(b, ','); str += ' ';
    str += '\n';

    str += "line AB ";
    norm = Vec3f::GetNormalized( AB.Cross(AO).Cross(AB) );
    middle = (A+B) / 2;
    b = middle + norm * 5;
    str += std::to_string(middle, ','); str += ' ';
    str += std::to_string(b, ','); str += ' ';
    str += '\n';

    str += "line AC ";
    norm = Vec3f::GetNormalized( AC.Cross(AO).Cross(AC) );
    middle = (A+C) / 2;
    b = middle + norm * 5;
    str += std::to_string(middle, ','); str += ' ';
    str += std::to_string(b, ','); str += ' ';
    str += '\n';

    CommandSys::SendDebug( str );
#endif // GJK_DEBUG

    // we test the edges first in order to elliminate the most possibilities
    const bool outsideOfAC( ( ABC_NORMAL.Cross( AC ).Dot( AO ) > 0 ) );
    if ( outsideOfAC ) {
        /* the origin is on the outside of this line, which means we have to go back and try a different line,
           we still have to narrow down what space the origin lies in so that we can search with a better point */

        const bool upLineAC( AC.Dot( AO ) > 0 );
        if ( upLineAC ) {
            // new simplex is the line AC
            B = C;
            C = A;
            simplex_inout.resize(2);

            // new search direction is from that line toward the origin
            search_dir_out = AC.Cross( AO ).Cross( AC );
            return;
        }
    } else {
        const bool insideOfAB( AB.Cross( ABC_NORMAL ).Dot( AO ) <= 0 );
        if ( insideOfAB ) {
            // the origin is closest to the triangle than any edge or vert

            const bool above_tri = ABC_NORMAL.Dot( AO ) > 0;
            if ( above_tri ) {
                std::swap( A, C ); // the new simplex is the triangle ABC_NORMAL
                search_dir_out = ABC_NORMAL; // origin is in the direction of ABC_NORMAL
                return;
            } else {
                // the new simplex is the same triangle but wound in reverse.
                Vec3f tmp( C );
                C = A;
                A = B;
                B = tmp;
                search_dir_out = -ABC_NORMAL; // origin is in the opposite direction of ABC_NORMAL
                return;
            }
        }
    }

    /* the origin was neither above/below the plane nor outside of line AC, so we now must check
       the direction AB to see if it's closer to line AB or to vert A.
    */

    C = A; // in either case below, A will be at index zero
    if ( AB.Dot( AO ) > 0 ) {
        simplex_inout.resize(2); // our new simplex is the line AB
        search_dir_out = AB.Cross( AO ).Cross( AB ); // new search direction is from AB to the origin
    } else {
        simplex_inout.resize(1); // the point A is our new simplex
        search_dir_out = AO; // new search direction is A to the origin;
    }
}
/*
inline void Simplex_TetDCBA_To_PointA( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[0] = simplex_inout[3];
    simplex_inout.resize(1);
}
*/
inline void Simplex_TetDCBA_To_LineAC( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[0] = simplex_inout[3];
    simplex_inout.resize(2);
}

inline void Simplex_TetDCBA_To_LineAD( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[1] = simplex_inout[0];
    simplex_inout[0] = simplex_inout[3];
    simplex_inout.resize(2);
}

inline void Simplex_TetDCBA_To_TriADC( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[2] = simplex_inout[1];
    simplex_inout[1] = simplex_inout[0];
    simplex_inout[0] = simplex_inout[3];
    simplex_inout.resize(3);
}

inline void Simplex_TetDCBA_To_LineAB( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[0] = simplex_inout[3];
    simplex_inout[1] = simplex_inout[2];
    simplex_inout.resize(2);
}

inline void Simplex_TetDCBA_To_TriACB( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[0] = simplex_inout[3];
    simplex_inout.resize(3);
}

inline void Simplex_TetDCBA_To_TriABD( std::vector<Vec3f>& simplex_inout ) {
    simplex_inout[1] = simplex_inout[2];
    simplex_inout[2] = simplex_inout[0];
    simplex_inout[0] = simplex_inout[3];
    simplex_inout.resize(3);
}

bool Collision3D::TestGJK::CheckSimplex_Tetrahedron( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out ) {
    ASSERT( simplex_inout.size() == 4 );

    // in reverse, as usual:
    Vec3f& A( simplex_inout[3] ); // the new point just added
    Vec3f& B( simplex_inout[2] );
    Vec3f& C( simplex_inout[1] );
    Vec3f& D( simplex_inout[0] );

    #ifdef GJK_DEBUG
        std::string str;
        str = "point origin 0,0,0\n";

        str += "point A ";
        str += std::to_string(simplex_inout[3], ',');
        str += '\n';

        str += "point B ";
        str += std::to_string(simplex_inout[2], ',');
        str += '\n';

        str += "point C ";
        str += std::to_string(simplex_inout[1], ',');
        str += '\n';

        str += "point D ";
        str += std::to_string(simplex_inout[0], ',');
        str += '\n';

        str += "tet tet ";
        str += std::to_string(simplex_inout[3], ','); str += ' ';
        str += std::to_string(simplex_inout[2], ','); str += ' ';
        str += std::to_string(simplex_inout[1], ','); str += ' ';
        str += std::to_string(simplex_inout[0], ',');
        str += '\n';

        CommandSys::SendDebug( str );
    #endif // GJK_DEBUG

    /* at this point, we know that the origin lies somewhere on the front side of the triancle BCD, within it's edges,
       and before point A. That makes a prism shape. What we have to do is check whether the point is inside the
       tetrahedron by checking if it is behind the other three faces. If it is in front one face, then we use that face
       as the new simplex. If it is in front of two of them, then we know it is closest to the edge formed by those two
       faces ( part of the line is hte new point we just added - which is the peak of the tetrahedron). If the origin is
       in front of all three faces, then we know the origin is closr to the peak of the new tetrahedron than any other
       part of it, and we must use that point as the new simpex.
    */

    const Vec3f AO( -A );
    const Vec3f AC( C - A );
    const Vec3f AD( D - A );
    const Vec3f AB( B - A );
    const Vec3f ADC( AD.Cross(AC) );
    const Vec3f ACB( AC.Cross(AB) );
    const Vec3f ABD( AB.Cross(AD) );

    const bool inFrontOfACB( ACB.Dot( AO ) > 0 );
    const bool inFrontOfADC( ADC.Dot( AO ) > 0 );

    #ifdef GJK_DEBUG // lines for normals
        std::string str;

        str += "line ADC ";
        Vec3f norm = ADC.GetNormalized();
        Vec3f middle = (A+D+C) / 3;
        Vec3f b = middle + norm * 5;
        str += std::to_string(middle, ','); str += ' ';
        str += std::to_string(b, ','); str += ' ';
        str += '\n';

        str += "line ACB ";
        norm = ACB.GetNormalized();
        middle = (A+C+B) / 3;
        b = middle + norm * 5;
        str += std::to_string(middle, ','); str += ' ';
        str += std::to_string(b, ','); str += ' ';
        str += '\n';

        str += "line ABD ";
        norm = ABD.GetNormalized();
        middle = (A+B+D) / 3;
        b = middle + norm * 5;
        str += std::to_string(middle, ','); str += ' ';
        str += std::to_string(b, ','); str += ' ';
        str += '\n';

        str += "line AB ";
        norm = Vec3f::GetNormalized( AB.Cross(AO).Cross(AB) );
        middle = (A+B) / 2;
        b = middle + norm * 5;
        str += std::to_string(middle, ','); str += ' ';
        str += std::to_string(b, ','); str += ' ';
        str += '\n';

        str += "line AC ";
        norm = Vec3f::GetNormalized( AC.Cross(AO).Cross(AC) );
        middle = (A+C) / 2;
        b = middle + norm * 5;
        str += std::to_string(middle, ','); str += ' ';
        str += std::to_string(b, ','); str += ' ';
        str += '\n';

        str += "line AD ";
        norm = Vec3f::GetNormalized( AD.Cross(AO).Cross(AD) );
        middle = (A+D) / 2;
        b = middle + norm * 5;
        str += std::to_string(middle, ','); str += ' ';
        str += std::to_string(b, ','); str += ' ';
        str += '\n';

        CommandSys::SendDebug( str );
    #endif // GJK_DEBUG

    if ( inFrontOfADC ) {
        if ( inFrontOfACB ) {

            #ifndef NDEBUG
                const bool inFrontOfABD( ABD.Dot( AO ) > 0 );
                ASSERT( ! inFrontOfABD ); /* this shouldn't happen
                Simplex_TetDCBA_To_A( simplex_inout );
                search_dir_out = AO; // direction is peak to the origin
                */
            #endif // NDEBUG

            Simplex_TetDCBA_To_LineAC( simplex_inout );
            search_dir_out = AC.Cross(AO).Cross(AC); // direction is AC to the origin

        } else {
            const bool inFrontOfABD( ABD.Dot( AO ) > 0 );

            if ( inFrontOfABD ) {
                Simplex_TetDCBA_To_LineAD( simplex_inout );
                search_dir_out = AD.Cross(AO).Cross(AD); // direction is AD to the origin
            } else {
                Simplex_TetDCBA_To_TriADC( simplex_inout );
                search_dir_out = ADC;
            }
        }
    } else {
        const bool inFrontOfABD( ABD.Dot( AO ) > 0 );

        if ( inFrontOfACB ) {
            if ( inFrontOfABD ) {
                Simplex_TetDCBA_To_LineAB( simplex_inout );
                search_dir_out = AB.Cross(AO).Cross(AB); // direction is AB to the origin
            } else {
                Simplex_TetDCBA_To_TriACB( simplex_inout );
                search_dir_out = ACB;
            }

        } else {
            if ( inFrontOfABD ) {
                Simplex_TetDCBA_To_TriABD( simplex_inout );
                search_dir_out = ABD;
            } else {
                return true; // the origin is encapselated within the tetrahedron
            }
        }
    }

    return false;
}

bool Collision3D::TestGJK::Intersects( const Box3D& box1, const Box3D& box2 ) {
    return Collision3D::TestGJK::Intersects( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8 );
}
