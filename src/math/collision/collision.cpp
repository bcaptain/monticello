// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./collision.h"
#include "./gjk.h"
#include "../../entities/entity.h"
#include "../../entityTree/entityTree.h"

IntersectData::IntersectData( void )
    : opts()
    , desired_type( TypeID_Entity )
    {
}

IntersectData::IntersectData( const SweepData& sweepData )
    : opts( sweepData.opts )
    , desired_type( sweepData.desired_type )
    {
}

IntersectData& IntersectData::operator=( const SweepData& sweepData ) {
    opts = sweepData.opts;
    desired_type = sweepData.desired_type;
    
    return *this;
}

SweepData::SweepData( void )
    : IntersectData()
    , vel()
    , do_collide( false )
    {
        opts |= MatchOptsT::MATCH_NEAREST;
}

SweepData::SweepData( const IntersectData& traceData )
    : IntersectData( traceData )
    , vel()
    , do_collide( false )
    {
}

std::shared_ptr< Entity > Collision3D::SweepToEntityVoxelList( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive, const LinkList< EntityTree * >& voxelList ) {
    CollisionReportTemp3D closest;
    bool found = false;
    std::shared_ptr< Entity > ret;

    CollisionReport3D tempReport( report.calc & ~CollisionCalcsT::Sorted );
    tempReport = report;
    
    std::vector< CollisionItem > ghost_collisions; // an accumulation of collisions against entities that we would ghost through along our intended sweep
    std::vector< const Entity* > checked_ents;

    for ( auto & voxel : voxelList ) {
        if ( ! voxel )
            continue;

        auto collision = SweepToEntityList( tempReport, coldata, primitive, voxel->GetEntityListRef(), ghost_collisions, checked_ents );

        if ( collision ) {
            if ( !found || tempReport.time < closest.time ) {
                // if we want the closest collision or want all collisions, we need to keep checking
                if ( coldata.opts & MatchOptsT::MATCH_NEAREST || report.all_collisions_out ) {
                    found = true;
                    closest = tempReport;
                    ret = collision;
                } else {
                    report = tempReport;
                    // we're not concerned about the order in which collisions happened (no MATCH_NEAREST) so we just return the one collision
                    return collision;
                }
            }
        }
    }

    if ( report.all_collisions_out ) {
        
        // add any ghost collisions that happen spacially before our closest real collision to all_collisions_out
        for ( const auto & item : ghost_collisions ) {
            
            if ( !ret || item.time <= report.time )
                report.all_collisions_out->emplace_back( item.ent, report.time );
        }
        
        if ( ret )
            report.all_collisions_out->emplace_back( ret, report.time );
    }
    
    if ( report.all_collisions_out && report.SortCollisions() ) {
        std::sort( std::begin( *report.all_collisions_out ), std::end( *report.all_collisions_out ) );
    }

    report = closest;
    return ret;
}

std::shared_ptr< Entity > Collision3D::SweepToEntityList( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive, const LinkList< std::weak_ptr< Entity > >& list, std::vector< CollisionItem >& ghost_collisions, std::vector< const Entity* >& checked_ents ) {
    CollisionReport3D tempReport( report.calc );
    std::shared_ptr< Entity > collision=nullptr;

    for ( auto & item : list ) {
        auto ent = item.lock();
        if ( ! ent )
            continue;

        // if the ent ends up not being what we are looking for, it won't be checked again from another voxel during this check
        if ( !AppendUnique( checked_ents, ent.get() ) )
            continue;

        if ( coldata.opts & MatchOptsT::MATCH_VISIBLE_ONLY )
            if ( !ent->IsVisible() )
                continue;

        if ( coldata.opts & MatchOptsT::MATCH_ENTS_WITH_MODELS_ONLY )
            if ( !ent->HasModel() )
                continue;

        if ( coldata.desired_type != TypeID_Entity ) { // ignore default base type Entity because everything is an entity
            const bool isType = derives_from( coldata.desired_type, *ent );
            const bool notWanted = coldata.opts & MatchOptsT::MATCH_INVERT_ENTITY_TYPE;
            if ( isType == notWanted )
                continue;
        }

        auto moving_ent = primitive.GetOwner();
        if ( moving_ent && !moving_ent->ReactsToPhysicsTogether( *ent ) )
            continue;
            
        if ( ! ent->SweepWith( tempReport, primitive, coldata ) )
            continue;
            
        if ( moving_ent && moving_ent->GhostsThrough( *ent ) ) {
            if ( report.all_collisions_out ) {
                ghost_collisions.emplace_back( ent, tempReport.time );
            }
            continue;
        } else {
            if ( report.all_collisions_out ) {
                report.all_collisions_out->emplace_back( ent, tempReport.time );
            }
        }

        if ( collision ) {
            // if we already have a closer collision, skip
            if ( report.time <= tempReport.time )
                continue;
        }
        
        collision = ent;
        report.time = tempReport.time;
        report.point = tempReport.point;
        report.norm = tempReport.norm;

        if ( ! ( coldata.opts & MatchOptsT::MATCH_NEAREST ) && !report.all_collisions_out )
            break;
    }
    
    if ( report.all_collisions_out && report.SortCollisions() ) {
        std::sort( std::begin( *report.all_collisions_out ), std::end( *report.all_collisions_out ) );
    }

    return collision;
}

/*! note that MatchOptsT::MATCH_NEAREST has no effect here. Getting the "closest collision" with trace collision makes no sense.
update: todo: I don't really remember how this works, but I think line trace was supposed to be renamed to point-sweep.
*/
std::shared_ptr< Entity > Collision3D::TraceToEntityVoxelList( const IntersectData& coldata, const Primitive& primitive, const LinkList< EntityTree * >& voxelList, std::vector< CollisionItem >* collisions_out ) {
    std::shared_ptr< Entity > collision, ret;
    std::vector< const Entity* > checked_ents;

    for ( auto & item : voxelList ) {
        if ( !item ) {
            continue;
        }

        collision = TraceToEntityList( coldata, primitive, item->GetEntityListRef(), checked_ents, collisions_out );

        if ( !ret && collision ) {

            // if we didn't ask for a list, just return this one instead of getting all collisions
            if ( ! collisions_out ) {
                return collision;
            } else {
                ret = collision;
            }
        }
    }

    return ret;
}

std::shared_ptr< Entity > Collision3D::TraceToEntityList( const IntersectData& coldata, const Primitive& primitive, const LinkList< std::weak_ptr< Entity > >& list, std::vector< const Entity* >& checked_ents, std::vector< CollisionItem >* collisions_out ) {
    std::shared_ptr< Entity > ret;
    auto moving_ent = primitive.GetOwner();
    
    ASSERT_MSG( ! ( coldata.opts & MATCH_NEAREST ), "Cannot judge distance with trace collision. You probably want a sweep instead.\n");

    for ( auto & item : list ) {
        std::shared_ptr< Entity > ent = item.lock();
        if ( ! ent )
            continue;

        // don't collide with self
        if ( moving_ent && moving_ent->GetIndex() == ent->GetIndex() )
            continue;

        // the ent may be in multiple voxels. make sure we only check it once
        if ( ! AppendUnique( checked_ents, ent.get() ) )
            continue;

        if ( coldata.opts & MatchOptsT::MATCH_INVERT_ENTITY_TYPE ) {
            if ( derives_from( coldata.desired_type, *ent ) ) {
                continue;
            }
        } else if ( coldata.desired_type != TypeID_Entity ) { // ignore default base type
            if ( !derives_from( coldata.desired_type, *ent ) ) {
                continue;
            }
        }
        
        if ( moving_ent && !ent->ReactsToPhysicsTogether( *moving_ent ) )
            continue;

        if ( !ent->Intersects( primitive ) )
            continue;

        if ( moving_ent && !ent->GhostsThrough( *moving_ent ) )
            continue;
            
        if ( collisions_out ) {
            collisions_out->emplace_back( ent, 0 );
            if ( ! ret ) {
                ret = ent;
            }
        } else {
            return ent;
        }
    }

    return ret;
}

#ifdef CLIB_UNIT_TEST
    void Collision3D::UnitTest( void ) {
        Collision3D::TestPoint::UnitTest();
        Line3f::UnitTest();
        Sphere::UnitTest();
        Box3D::UnitTest();
        Capsule::UnitTest();
        Collision3D::TestGJK::UnitTest();
        AABox3D::UnitTest();
        Collision3D::TestSemiCylinder::UnitTest();
        Cylinder::UnitTest();
    }

    void Collision2D::UnitTest( void ) {
        Collision2D::TestPoint::UnitTest();
        Collision2D::TestPoly::UnitTest();
        Collision2D::TestCircle::UnitTest();
    }
#endif //CLIB_UNIT_TEST

//
//
// CollisionReport3D
//
//

CollisionReport3D::CollisionReport3D( CollisionCalcsT calcs )
    : CollisionReportT( calcs )
    , all_collisions_out( nullptr )
{ }

CollisionReport3D::CollisionReport3D( void )
    : all_collisions_out( nullptr )
{ }

CollisionReport3D::CollisionReport3D( const CollisionReport3D &other )
    : CollisionReportT(other)
    , all_collisions_out( other.all_collisions_out )
{ }

CollisionReport3D::CollisionReport3D( const CollisionReportTempT<Vec3f> &temp )
    : CollisionReportT(temp)
    , all_collisions_out( nullptr )
{ }

CollisionReport3D& CollisionReport3D::operator=( const CollisionReport3D &other ) {
    CollisionReportT::operator=( other );
    all_collisions_out = other.all_collisions_out;
    return *this;
}

CollisionReport3D& CollisionReport3D::operator=( const CollisionReportTempT<Vec3f>& temp ) {
    CollisionReportT::operator=( temp );
    return *this;
}
