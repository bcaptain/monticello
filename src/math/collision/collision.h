// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_COLLISION_H_
#define SRC_COLLISION_COLLISION_H_

#include "../../base/main.h"
#include "../../math/geometry.h"

class EntityTree;
class Entity;
class Primitive;

template< typename TYPE>
class CollisionReportT;

class CollisionReport3D;

struct CollisionItem {
    CollisionItem( const CollisionItem& other ) = default;
    CollisionItem( void ) : ent(), time(0) {}
    ~CollisionItem( void ) = default;
    CollisionItem( const std::weak_ptr<Entity>& _ent, const float _time ) :  ent(_ent), time(_time) {}
    CollisionItem& operator=( const CollisionItem& other ) = default;
    bool operator>(const CollisionItem& other) const { return time > other.time; }
    bool operator<(const CollisionItem& other) const { return time < other.time; }
    bool operator>=(const CollisionItem& other) const { return !operator<(other); }
    bool operator<=(const CollisionItem& other) const { return !operator>(other); }
    std::weak_ptr< Entity > ent;
    float time;
};

// for specifying which calculations are needed (some will be calculated anyway when necessary)
typedef Uint8 CollisionCalcsT_BaseType;
enum class CollisionCalcsT : CollisionCalcsT_BaseType {
    None=0
    , Sorted=1 // !< will sort all_collisions_out
    , Intersection=1<<1
    , Normal=1<<2
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( CollisionCalcsT, CollisionCalcsT_BaseType )

// Tells collision functions what type of collisions should be performed
typedef Uint8 EntityClipTypeT_BaseType;
enum class EntityClipTypeT : EntityClipTypeT_BaseType {
    CLIP_MODEL = 0
    , DRAW_MODEL = 1
    , BOUNDS_ONLY = 1<<1
};

// options for checking collision against lists of entities
typedef Uint8 MatchOptsT_BaseType;
enum MatchOptsT : MatchOptsT_BaseType {
    MATCH_NORMAL = 0
    , MATCH_NEAREST = 1
    , MATCH_INVERT_ENTITY_TYPE = 1<<1
    , MATCH_VISIBLE_ONLY = 1<<2
    , MATCH_ENTS_WITH_MODELS_ONLY = 1<<3
    , MATCH_IGNORE_INVISIBLE_CLIPMODEL = 1<<4
    , MATCH_TEST_ONLY_BOUNDS = 1<<5
};

class SweepData;

// Tells collision system how to trace collide with things, what things to collide with, and what to do when collisions happen
class IntersectData {
public:
    IntersectData( void );
    IntersectData( const IntersectData& other ) = default;
    IntersectData& operator=( const IntersectData& other ) = default;
    
    IntersectData( const SweepData& sweepData );
    IntersectData& operator=( const SweepData& sweepData );
    
    virtual ~IntersectData( void ) {}

public:
    MatchOptsT_BaseType opts;
    TypeID desired_type; //!< What kind of entity type we are looking for. The default is the base Entity class so it will catch everything.
};

// Tells collision system how to sweep collide with things, what things to collide with, and what to do when collisions happen
class SweepData : public IntersectData {
public:
    SweepData( void );
    SweepData( const SweepData& other ) = default;
    SweepData& operator=( const SweepData& other ) = default;
    
    SweepData( const IntersectData& other );
    
    ~SweepData( void ) override {}

public:
    Vec3f vel; //!< velocity vector. if ent is set, this is used to push entities if they are movables. If shape is a sweepsphere, this is it's sweep vector.
    bool do_collide; //!< whether to call Collide() on collision entity if 'ent' above is set. defaults to true
};

// Information about collision occurences, and what info to request
template< typename TYPE >
struct CollisionReportTempT;

template< typename TYPE >
class CollisionReportT {

public:
    CollisionReportT( void );
    explicit CollisionReportT( CollisionCalcsT calcs );
    explicit CollisionReportT( const TYPE& thepoint, CollisionCalcsT calcs = CollisionCalcsT::None );
    CollisionReportT( const CollisionReportT<TYPE> &other );
    CollisionReportT( const CollisionReportTempT<TYPE> &other );
    ~CollisionReportT( void ) = default;

public:
    CollisionReportT<TYPE>& operator=( const CollisionReportT<TYPE> &other );
    CollisionReportT<TYPE>& operator=( const CollisionReportTempT<TYPE>& temp );

    bool SortCollisions( void ) const;
    bool ComputeNormal( void ) const;
    bool ComputeIntersection( void ) const;
    
public:
    TYPE norm; //! normal of the tangent plane beteen us and the primitive we collided with. The normal points toward us from the primitive we collided with
    TYPE point; //! intersection point
    float time; //! time 'up the velocity vector' that a collision occurs

    const CollisionCalcsT calc; //!< what calculations to make
};

template< typename TYPE >
inline bool CollisionReportT<TYPE>::ComputeNormal( void ) const {
    return BitwiseAnd( calc, CollisionCalcsT::Normal );
}

template< typename TYPE >
inline bool CollisionReportT<TYPE>::SortCollisions( void ) const {
    return BitwiseAnd( calc, CollisionCalcsT::Sorted );
}

template< typename TYPE >
inline bool CollisionReportT<TYPE>::ComputeIntersection( void ) const {
    return BitwiseAnd( calc, CollisionCalcsT::Intersection );
}

typedef CollisionReportT<Vec2f> CollisionReport2D;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++" // private inheritance doesn't need virtual destructor!
class CollisionReport3D : private CollisionReportT<Vec3f> {
public:
#pragma GCC diagnostic pop

    CollisionReport3D( void );
    CollisionReport3D( const CollisionReport3D &other );
    CollisionReport3D( const CollisionReportTempT<Vec3f> &other );
    CollisionReport3D& operator=( const CollisionReport3D &other );
    CollisionReport3D& operator=( const CollisionReportTempT<Vec3f>& temp );
    ~CollisionReport3D( void ) = default;

    explicit CollisionReport3D( CollisionCalcsT calcs );

public:
    using CollisionReportT<Vec3f>::norm;
    using CollisionReportT<Vec3f>::point;
    using CollisionReportT<Vec3f>::time;
    using CollisionReportT<Vec3f>::calc;

    using CollisionReportT::ComputeNormal;
    using CollisionReportT::ComputeIntersection;
    using CollisionReportT::SortCollisions;
    
public:
    std::vector< CollisionItem > *all_collisions_out; //!< If this is not null will scan for all collisions (slower). defaults to null. does NOT get freed.
};

template< typename TYPE >
struct CollisionReportTempT {
    CollisionReportTempT( void );
    ~CollisionReportTempT( void ) = default;
    CollisionReportTempT( const CollisionReportTempT& other ) = default;
    CollisionReportTempT( const CollisionReportT<TYPE>& other );
    CollisionReportTempT( const CollisionReport3D& other );

    CollisionReportTempT& operator=( const CollisionReportTempT& other ) = default;

    CollisionReportTempT& operator=( const CollisionReport3D& other );
    CollisionReportTempT& operator=( const CollisionReportT<TYPE>& other );

    TYPE norm;
    TYPE point;
    float time;
};

typedef CollisionReportTempT<Vec3f> CollisionReportTemp3D;

template< typename TYPE >
CollisionReportT<TYPE>::CollisionReportT( const CollisionReportTempT<TYPE>& other )
    : norm( other.norm )
    , point( other.point )
    , time( other.time )
    , calc( CollisionCalcsT::None )
{ }

template< typename TYPE >
CollisionReportT<TYPE>::CollisionReportT( const TYPE& thepoint, CollisionCalcsT calcs )
    : norm()
    , point(thepoint)
    , time(0)
    , calc(calcs)
{ }

template< typename TYPE >
CollisionReportT<TYPE>::CollisionReportT( CollisionCalcsT calcs )
    : norm()
    , point()
    , time(0)
    , calc(calcs)
{ }

template< typename TYPE >
CollisionReportT<TYPE>::CollisionReportT( void )
    : norm()
    , point()
    , time(0)
    , calc(CollisionCalcsT::None)
{ }

template< typename TYPE >
CollisionReportT<TYPE>::CollisionReportT( const CollisionReportT<TYPE> &other )
    : norm(other.norm)
    , point(other.point)
    , time(other.time)
    , calc(other.calc)
{ }

template< typename TYPE >
CollisionReportT<TYPE>& CollisionReportT<TYPE>::operator=( const CollisionReportT<TYPE> &other ) {
    time = other.time;
    norm = other.norm;
    point = other.point;
    return *this;
}

template< typename TYPE >
CollisionReportT<TYPE>& CollisionReportT<TYPE>::operator=( const CollisionReportTempT<TYPE>& temp ) {
    norm = temp.norm;
    point = temp.point;
    time = temp.time;
    return *this;
}

template< typename TYPE >
CollisionReportTempT<TYPE>::CollisionReportTempT( void )
    : norm()
    , point()
    , time(0.0f)
{ }

template< typename TYPE >
CollisionReportTempT<TYPE>::CollisionReportTempT( const CollisionReportT<TYPE>& other )
    : norm( other.norm )
    , point( other.point )
    , time( other.time )
{ }

template< typename TYPE >
CollisionReportTempT<TYPE>::CollisionReportTempT( const CollisionReport3D& other )
    : norm( other.norm )
    , point( other.point )
    , time( other.time )
{ }

template< typename TYPE >
CollisionReportTempT<TYPE>& CollisionReportTempT<TYPE>::operator=( const CollisionReportT<TYPE>& report ) {
    norm = report.norm;
    point = report.point;
    time = report.time;
    return *this;
}

template< typename TYPE >
CollisionReportTempT<TYPE>& CollisionReportTempT<TYPE>::operator=( const CollisionReport3D& report ) {
    norm = report.norm;
    point = report.point;
    time = report.time;
    return *this;
}

namespace Collision2D {
    #ifdef CLIB_UNIT_TEST
        void UnitTest( void );
    #endif //CLIB_UNIT_TEST
}

namespace Collision3D {
    #ifdef CLIB_UNIT_TEST
        void UnitTest( void );
    #endif //CLIB_UNIT_TEST

    std::shared_ptr< Entity > TraceToEntityVoxelList( const IntersectData& coldata, const Primitive& primitive, const LinkList< EntityTree * >& voxelList, std::vector< CollisionItem >* collisions_out = nullptr ); //!< returns the first collision, collisions_out is packed with all collisions if set (but is slower)
    std::shared_ptr< Entity > TraceToEntityList( const IntersectData& coldata, const Primitive& primitive, const LinkList< std::weak_ptr< Entity > >& list, std::vector< const Entity* >& checked_ents, std::vector< CollisionItem >* collisions_out = nullptr ); //!< returns the first collision, collisions_out is packed with all collisions if set (but is slower)

    std::shared_ptr< Entity > SweepToEntityVoxelList( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive, const LinkList< EntityTree * >& voxelList );
    std::shared_ptr< Entity > SweepToEntityList( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive, const LinkList< std::weak_ptr< Entity > >& list, std::vector< CollisionItem >& ghost_collisions, std::vector< const Entity* >& checked_ents );
    
    namespace TestColData {
        bool ToAABox3D( const IntersectData& coldata, const AABox3D& aabox );
    }
}

#endif  // SRC_COLLISION_COLLISION_H_
