// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_GJK
#define SRC_COLLISION_GJK

#include "../../base/main.h"
#include "../geometry.h"

namespace Collision3D {
    namespace TestGJK {

        #ifdef CLIB_UNIT_TEST
            void UnitTest( void );
        #endif //CLIB_UNIT_TEST

        //! GJK checks whether the MD ("Minkowski Difference") of two convex polyhedron/polyhedra encloses the origin. If so, the two shapes collide.
        //! NOTE: It is hypothetically possible for very-far-apart objects to cause float overflows. Always do a quick circumsphere pre-test on both objects to make sure they are close.
        bool Intersects( const Vec3f *Averts_unwound, const std::size_t Anum_verts, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts );

        template< typename SHAPE_DATA >
        bool Intersects( const SHAPE_DATA& shape, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts );

        bool Intersects( const Box3D& box1, const Box3D& box2 );

        template< typename SHAPE_DATA >
        bool Intersects( const SHAPE_DATA& shape, const Box3D& box );

        // **** the following aren't for general consumption

        //! returns the Minkowski Difference of the two farthest points of the two shapes along the given direction
        template< typename TYPE >
        Vec3f GetSupport( const Vec3f& unnormalized_dir, const TYPE& shape, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts );
        Vec3f GetSupport( const Vec3f& unnormalized_dir, const Vec3f *Averts_unwound, const std::size_t Anum_verts, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts );

        //! chooses the proper function to get the simplex and dir for whatever part of it is closest to the origin
        bool CheckSimplex( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out );

        //! update the simplex based on what section of space the origin lies in
        void CheckSimplex_LineSeg( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out );

        //! update the simplex based on what section of space the origin lies in
        void CheckSimplex_Triangle( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out );

        //! update the simplex based on what section of space the origin lies in
        bool CheckSimplex_Tetrahedron( std::vector<Vec3f>& simplex_inout, Vec3f& search_dir_out );
    }
}

template< typename SHAPE_DATA >
inline bool Collision3D::TestGJK::Intersects( const SHAPE_DATA& shape, const Box3D& box ) {
    return Collision3D::TestGJK::Intersects( shape, box.GetVertsPtr(), 8 );
}

template< typename SHAPE_DATA >
bool Collision3D::TestGJK::Intersects( const SHAPE_DATA& shape, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts ) {
    // the smallest shape in 3D that we can enclose an origin with is a tetrahedron, which has 4 vertices
    std::vector< Vec3f > simplex;
    simplex.reserve(4);

    // we start with any random point in the MD
    const Vec3f any_dir( Vec3f(0,0,1) );
    simplex.emplace_back( GetSupport( any_dir, shape, Bverts_unwound, Bnum_verts ) );

    // start with -support cuz that's a dir toward the origin (doesn't need to be normalized)
    Vec3f dir( -simplex[0] );

    int i=0;
    do {
        // find the vertex in the MD which is farthest in the direction
        // it becomes the next part of our enclosure shape
        simplex.emplace_back( GetSupport( dir, shape, Bverts_unwound, Bnum_verts ) );

        // if the farthest vert does not reach the origin, we can't enclose it
        if ( simplex[simplex.size()-1].Dot( dir ) < 0 )
            return false;

        if ( ++i == 20 )
            return false;

    } while ( ! CheckSimplex( simplex, dir ) );

    return true;
}

template< typename TYPE >
Vec3f Collision3D::TestGJK::GetSupport( const Vec3f& unnormalized_dir, const TYPE& shape, const Vec3f *Bverts_unwound, const std::size_t Bnum_verts ) {
    const std::size_t Bindex( Polyhedron::GetFarthestVertIndex( Bverts_unwound, Bnum_verts, -unnormalized_dir ) ); // opposite direction to get the maximum value
    const Vec3f vert( shape.GetFarthestVert( unnormalized_dir.GetNormalized() ) );
    return vert - Bverts_unwound[Bindex];
}

#endif // SRC_COLLISION_GJK
