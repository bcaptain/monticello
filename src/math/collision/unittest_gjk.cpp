// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./collision.h"
#include "./gjk.h"

#ifdef CLIB_UNIT_TEST
void Collision3D::TestGJK::UnitTest( void ) {

    {
        Box3D one(-10,10,-10,10,-10,10);
        Box3D two(-10,10,-10,10,-10,10);

        // completely coincide
        ASSERT( Collision3D::TestGJK::Intersects( one, two ) );

        // intersect
        two += Vec3f( 10,0,0 );
        ASSERT( Collision3D::TestGJK::Intersects( one, two ) );

        // touches
        two += Vec3f( 10,0,0 );
        ASSERT( Collision3D::TestGJK::Intersects( one, two ) );

        // no touch
        two += Vec3f( 0.01f,0,0 );
        ASSERT( !Collision3D::TestGJK::Intersects( one, two ) );
    }

    { // two tetrahedron touching at their tips
        Vec3f tet1[4]{
            Vec3f(-1,-1,-1) // base
            , Vec3f(1,-1,-1) // base
            , Vec3f(0,1,-1) // base
            , Vec3f(0,0,0) // the tip at the top
        };
        Vec3f tet2[4]{
            Vec3f(-1,-1,1) // base
            , Vec3f(1,-1,1) // base
            , Vec3f(0,1,1) // base
            , Vec3f(0,0,0) // the tip at the bottom
        };

        ASSERT( Collision3D::TestGJK::Intersects( tet1, 4, tet2, 4 ) );

        // now move the top up just at touch so they don't collide anymore
        tet2[3].z+=0.01f;

        ASSERT( ! Collision3D::TestGJK::Intersects( tet1, 4, tet2, 4 ) );
    }
}

#endif // CLIB_UNIT_TEST
