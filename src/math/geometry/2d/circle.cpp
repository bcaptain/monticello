// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./circle.h"

bool Collision2D::TestCircle::ToAASquare( const Vec2f& center, const float radius, const Vec2f& aabb_min, const Vec2f& aabb_max ) {
    const Vec2f center_box( (aabb_min + aabb_max) / 2 );

    // CHECK INSPHERE
    const float box_inshere_radius = fabsf( aabb_max.x - aabb_min.x ) / 2;
    if ( Collision2D::TestCircle::ToCircle( center, radius, center_box, box_inshere_radius ) )
        return true;

    // CHECK CIRCUMSPHERE
    const float box_circumsphere_radius = (aabb_max - aabb_min).SquaredLen() / 2;
    const float radius_squared = radius * radius;
    if ( !Collision2D::TestCircle::ToCircle( center, radius_squared, center_box, box_circumsphere_radius ) )
        return false;

    // CHECK VERTICES
    const Vec2f verts[4] {
        Vec2f( aabb_min.x, aabb_min.y ),
        Vec2f( aabb_min.x, aabb_max.y ),
        Vec2f( aabb_max.x, aabb_min.y ),
        Vec2f( aabb_max.x, aabb_max.y )
    };

    // an edge case where the circle intersects a line but not the insphere
    for ( uint a=3,b=0; b<4; a=b++ )
        if ( Collision2D::TestLineSeg::ToCircle( verts[a], verts[b], center, radius ) )
            return true;

    // a rare edge case where the circle is inside a corner of the box but not touching the insphere ot any lines
    if ( Collision2D::TestPoint::ToAASquare4v( center, AASquare4v( aabb_max, aabb_min ) ) )
        return true;

    return false;
}

bool Collision2D::TestCircle::ToCircle( const Vec2f& circle1, const float radius1, const Vec2f& circle2, const float radius2 ) {
    return Vec2f::SquaredLen( circle1 - circle2 ) <= (radius1 + radius2) * (radius1 + radius2);
}

bool Collision2D::TestCircle::ToPoly( const std::vector<Vec2f>& clockwise_poly, const Vec2f& center, const float radius ) {
    if ( clockwise_poly.size() < 3 )
        return false;

    ASSERT( clockwise_poly.size() >= 3 );

    uint numZeroDists = 0;

    // ** first check that ths circle exists on or behind all infinite lines
    for ( std::size_t a=clockwise_poly.size()-1, b=0; b<clockwise_poly.size(); a=b++ ) {
        const float dist = Line2f::GetDistToPoint( LineTypeT::LINE, clockwise_poly[a], clockwise_poly[b], center );
        if ( dist > radius ) {
            return false;
        }

        if ( dist <= 0 )
            ++numZeroDists;
    }

    // ** if all the distances are less than zero, then the center of the circle is inside the triangle
    if ( numZeroDists == clockwise_poly.size() )
        return true;

    // ** now we just have to make sure that at least one of the line segments intersect the circle
    for ( std::size_t a=clockwise_poly.size()-1, b=0; b<clockwise_poly.size(); a=b++ ) {
        if ( Collision2D::TestLineSeg::ToCircle( clockwise_poly[a], clockwise_poly[b], center, radius ) )
            return true;
    }

    return false;
}

#ifdef CLIB_UNIT_TEST
    void Collision2D::TestCircle::UnitTest( void ) {
        UnitTest_ToCircle();
        UnitTest_ToPoly();
        UnitTest_ToAASquare();
    }

    void Collision2D::TestCircle::UnitTest_ToAASquare( void ) {
        const Vec2f aabb_min( -0.5f, -0.5f );
        const Vec2f aabb_max( 0.5f, 0.5f );
        const float sphere_radius = 1;
        const Vec2f sphere_center_yes1(0,0); // coincide
        const Vec2f sphere_center_yes2(1.4f,0); // hits insphere
        const Vec2f sphere_center_no2(1.5f,0); // hits insphere but only touches
        const Vec2f sphere_center_no3(1.6f,0); // misses insphere, hits outsphere, no intersection
        const Vec2f sphere_center_no1(2.5f,0); // misses circumsphere
        ASSERT( Collision2D::TestCircle::ToAASquare( sphere_center_yes1, sphere_radius, aabb_min, aabb_max ) );
        ASSERT( Collision2D::TestCircle::ToAASquare( sphere_center_yes2, sphere_radius, aabb_min, aabb_max ) );
        ASSERT( !Collision2D::TestCircle::ToAASquare( sphere_center_no1, sphere_radius, aabb_min, aabb_max ) );

        // todo: other circle collision tests say that touching is not a collision. we should be consistent.
        ASSERT( Collision2D::TestCircle::ToAASquare( sphere_center_no2, sphere_radius, aabb_min, aabb_max ) );

        ASSERT( Collision2D::TestCircle::ToAASquare( sphere_center_no2, sphere_radius+0.01f, aabb_min, aabb_max ) );
        ASSERT( !Collision2D::TestCircle::ToAASquare( sphere_center_no2, sphere_radius-0.01f, aabb_min, aabb_max ) );

        // doesn't hit insphere, but hits vertice
        const Vec2f sphere_center_A(1.5,1.5); 
        const float sphere_radius_A1_yes = 0.01f + Vec2f(1,1).Len(); // doesn't hit insphere, but hits vertice
        //const float sphere_radius_A1_touch = Vec2f(1,1).Len(); // doesn't hit insphere, but hits vertice
        const float sphere_radius_A2_no = 1; // doesn't hit insphere, but hits vertice
        ASSERT( Collision2D::TestCircle::ToAASquare( sphere_center_A, sphere_radius_A1_yes, aabb_min, aabb_max ) );
        ASSERT( !Collision2D::TestCircle::ToAASquare( sphere_center_A, sphere_radius_A2_no, aabb_min, aabb_max ) );
    }

    void Collision2D::TestCircle::UnitTest_ToPoly( void ) {
        std::vector< Vec2f > verts;
        verts.emplace_back(0,0);
        verts.emplace_back(0,1);
        verts.emplace_back(1,1);
        verts.emplace_back(1,0);

        ASSERT( ToPoly( verts, {0,0}, 1) ); // penetrate
        ASSERT( ToPoly( verts, {0.5f,0.5f}, 1) ); // penetrate
        ASSERT( ToPoly( verts, {1,1}, 1) ); // penetrate
        ASSERT( ToPoly( verts, {0,1}, 1) ); // penetrate
        ASSERT( ToPoly( verts, {1,0}, 1) ); // penetrate

        ASSERT( !ToPoly( verts, {0.5f,2.5f}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {0.5f,-2.5f}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {2.5f,2.5f}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {2.5f,-2.5f}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {-2.5f,2.5f}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {-2.5f,-2.5f}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {2.5f,0}, 1) ); // no touch
        ASSERT( !ToPoly( verts, {-2.5f,0}, 1) ); // no touch

        ASSERT( !ToPoly( verts, {0,2}, 1) ); // touch
        ASSERT( !ToPoly( verts, {1,2}, 1) ); // touch
        ASSERT( !ToPoly( verts, {-1,1}, 1) ); // touch
        ASSERT( !ToPoly( verts, {-1,0}, 1) ); // touch
        ASSERT( !ToPoly( verts, {2,0}, 1) ); // touch
        ASSERT( !ToPoly( verts, {2,1}, 1) ); // touch
        ASSERT( !ToPoly( verts, {0,-1}, 1) ); // touch
        ASSERT( !ToPoly( verts, {1,-1}, 1) ); // touch

        verts.clear();
        verts.emplace_back( 2, 2 );
        verts.emplace_back( 4, 0 );
        verts.emplace_back( 1,0 );

        ASSERT( !ToPoly( verts, {2, -2}, 1.8f) ); // miss

        verts.clear();
        verts.emplace_back( 4, 0 );
        verts.emplace_back( 1,0 );
        verts.emplace_back( 2, 2 );

        ASSERT( !ToPoly( verts, {2, -2}, 1.8f) ); // miss

        verts.clear();
        verts.emplace_back( 1,0 );
        verts.emplace_back( 2, 2 );
        verts.emplace_back( 4, 0 );

        ASSERT( !ToPoly( verts, {2, -2}, 1.8f) ); // miss
        ASSERT( ToPoly( verts, {2, -1}, 1.5f) ); // penetrate

        verts.clear();
        verts.emplace_back( 19.03f,-13.16f );
        verts.emplace_back( 7.08f, -12.4f );
        verts.emplace_back( 13.24f, -5.17f );

        ASSERT( ToPoly( verts, {14.38f,-13.58f}, 2) ); // penetrate


        verts.clear();
        verts.emplace_back( 0,0 );
        verts.emplace_back( 10,10 );
        verts.emplace_back( 20,0 );

        ASSERT( !ToPoly( verts, {25,-2.5f}, 4) ); // miss
    }

    void Collision2D::TestCircle::UnitTest_ToCircle( void ) {
        // coiincide
        ASSERT( ToCircle( Vec2f(0,0), 1, Vec2f(0,0), 1 ) );
        ASSERT( ToCircle( Vec2f(0,0), 1, Vec2f(0,0), 1 ) );

        // encapselation
        ASSERT( ToCircle( Vec2f(0,0), 1, Vec2f(0,0), 0.5f ) );
        ASSERT( ToCircle( Vec2f(0,0), 0.5f, Vec2f(0,0), 1 ) );

        // intersection
        ASSERT( ToCircle( Vec2f(0,0), 1, Vec2f(0.5f,0), 1 ) );
        ASSERT( ToCircle( Vec2f(0.5f,0), 1, Vec2f(0,0), 1 ) );

        // intersection
        ASSERT( ToCircle( Vec2f(0,0), 1, Vec2f(1,0), 1 ) );
        ASSERT( ToCircle( Vec2f(0,0), 1, Vec2f(1,0), 1 ) );

        // touch
        ASSERT( ToCircle( Vec2f(-1,0), 1, Vec2f(1,0), 1 ) );
        ASSERT( ToCircle( Vec2f(-1,0), 1, Vec2f(1,0), 1 ) );

        // no touch
        ASSERT( ! ToCircle( Vec2f(-1.01f,0), 1, Vec2f(1,0), 1 ) );
        ASSERT( ! ToCircle( Vec2f(1,0), 1, Vec2f(-1.01f,0), 1 ) );
    }

#endif // CLIB_UNIT_TEST
