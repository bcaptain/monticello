// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_2D_AABB_H_
#define SRC_COLLISION_2D_AABB_H_

#include "../../../base/main.h"
#include "../../geometry.h"

namespace Collision2D {
    namespace TestAASquare {
        bool BehindLine( const Vec2f& A, const Vec2f& B, const Vec2f& C, const Vec2f& D, const Line2f& line, const float epsilon = EP );
        bool BeforeLine( const Vec2f& A, const Vec2f& B, const Vec2f& C, const Vec2f& D, const Line2f& line, const float epsilon = EP );

        float SignedDistToLine( const Vec2f& A, const Vec2f& B, const Vec2f& C, const Vec2f& D, const Line2f& line ); //!< this is for infinite lines, not linesegs
    }
}

#endif // SRC_COLLISION_2D_AABB_H_
