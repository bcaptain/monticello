// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./lineseg.h"
#include "./point.h"
#include "../../../rendering/face.h"
#include "../../collision/collision.h"

bool Collision2D::TestLineSeg::ToLineSeg( const Vec2f &A, const Vec2f& B, const Vec2f &U, const Vec2f& V ) { // there has to be a faster way of doing this
    float d = (A.x - B.x) * (U.y - V.y) - (A.y - B.y) * (U.x - V.x);

    // parallel
    if ( Maths::Approxf( d, 0 ) )
        return ( Maths::Approxf( A.x - B.x, 0 ) );

    float pre = (A.x*B.y - A.y*B.x), post = (U.x*V.y - U.y*V.x);
    Vec2f point;
    point.x = ( pre * (U.x - V.x) - (A.x - B.x) * post ) / d;
    point.y = ( pre * (U.y - V.y) - (A.y - B.y) * post ) / d;

    // *** only a collision if point exists within both line segments

    if ( point.x+EP < std::fminf(A.x, B.x) || point.x-EP > std::fmaxf(A.x, B.x) ||
         point.y+EP < std::fminf(A.y, B.y) || point.y-EP > std::fmaxf(A.y, B.y) ||
         point.x+EP < std::fminf(U.x, V.x) || point.x-EP > std::fmaxf(U.x, V.x) ||
         point.y+EP < std::fminf(U.y, V.y) || point.y-EP > std::fmaxf(U.y, V.y) )
        return false;

    return true;
}

bool Collision2D::TestLineSeg::ToLineSeg( CollisionReport2D& report, const Vec2f &A, const Vec2f& B, const Vec2f &U, const Vec2f& V ) {
    float d = (A.x - B.x) * (U.y - V.y) - (A.y - B.y) * (U.x - V.x);

    // parallel
    if ( Maths::Approxf( d, 0 ) )
        return ( Maths::Approxf( A.x - B.x, 0 ) );

    float pre = (A.x*B.y - A.y*B.x), post = (U.x*V.y - U.y*V.x);
    report.point.x = ( pre * (U.x - V.x) - (A.x - B.x) * post ) / d;
    report.point.y = ( pre * (U.y - V.y) - (A.y - B.y) * post ) / d;

    // *** only a collision if point exists within both line segments

    if ( report.point.x+EP < std::fminf(A.x, B.x) || report.point.x-EP > std::fmaxf(A.x, B.x) ||
         report.point.y+EP < std::fminf(A.y, B.y) || report.point.y-EP > std::fmaxf(A.y, B.y) ||
         report.point.x+EP < std::fminf(U.x, V.x) || report.point.x-EP > std::fmaxf(U.x, V.x) ||
         report.point.y+EP < std::fminf(U.y, V.y) || report.point.y-EP > std::fmaxf(U.y, V.y) )
    {
        return false;
    }

    if ( report.ComputeNormal() ) {
        report.norm = B;
        report.norm -= A;
        report.norm.Normalize();
    }

/* I'm not sure report.point is even the point of collision.

    if ( report.calc & COMPUTE_TIME ) {
        // ** manually calculate time up line AB
        const float AB_sq = Vec3f::SquaredLen(B-A);
        if ( Maths::Approxf( AB_sq, 0 ) ) {
            report.time = 0;
            return true;
        }

        const float AP_sq = Vec3f::SquaredLen(report.point - A);
        if ( Maths::Approxf( AB_sq, 0 ) ) {
            report.time = 0;
            return true;
        }

        report.time = AP_sq / AB_sq;
    }
*/

    return true;
}

// SAT would be better here
bool Collision2D::TestLineSeg::ToFace( const Vec2f &point_A, const Vec2f &point_B, const Face2D& face, const Vec2f& origin ) {
    auto const num_verts = face.NumVert();
    ASSERT( num_verts > 2 );

    for ( std::size_t a=num_verts-1, b=0; b<num_verts; a=b++ )
        if ( Collision2D::TestLineSeg::ToLineSeg( point_A, point_B, origin + face.vert[b].co, origin + face.vert[a].co ) )
            return false;

    return true;
}

bool Collision2D::TestLineSeg::ToAASquare4v_TopAndBottom( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.tl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.br, square.bl ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_Sides( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_AllButLeft( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.tl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.bl, square.br ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_AllButRight( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.tl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.bl, square.br ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_AllButTop( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.bl, square.br ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_TopAndRight( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.tr ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_BottomAndRight( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.bl, square.br ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_TopAndLeft( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.tr ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_BottomAndLeft( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.bl, square.br ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_AllButBottom( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.tr ) )
        return true;

    return false;
}

bool Collision2D::TestLineSeg::ToAASquare4v_All( const Vec2f& A, const Vec2f& B, const AASquare4v& square ) {
    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tr, square.br ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.bl ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.tl, square.tr ) )
        return true;

    if ( Collision2D::TestLineSeg::ToLineSeg( A,B, square.bl, square.br ) )
        return true;

    return false;
}

/*!
Information from: http://stackoverflow.com/questions/99353/how-to-test-if-a-line-segment-intersects-an-axis-aligned-rectange-in-2d \n
Use the Cohen-Sutherland algorithm. \n\n

It's used for clipping but can be slightly tweaked for this task. It divides 2D space up into a tic-tac-toe board with your rectangle as the "center square". \n
then it checks to see which of the nine regions each of your line's two points are in. \n\n

    If both points are left, right, top, or bottom, you trivially reject. \n
    If either point is inside, you trivially accept. \n
    In the rare remaining cases you can do the math to intersect with whichever sides of the rectangle are possible to intersect with, based on which regions they're in. \n
**/

bool Collision2D::TestLineSeg::ToAASquare( const Vec2f& A, const Vec2f& B, const AASquare2v& aasquare ) {
    // is this the Cohen-Sutherland algorithm?
    
    const AASquare4v square( aasquare );

// i is our square
//  a | b | c
// -----------
//  g | i | h
// -----------
//  d | e | f

    // pre-emptively test whether B is inside the square
    if ( TestPoint::ToAASquare4v( B, square ) )
        return true;

    if ( TestPoint::AboveAASquare( A, aasquare ) ) {
        if ( TestPoint::AboveAASquare( B, aasquare ) ) // both points above square
            return false;

        if ( TestPoint::LeftOfAASquare( A, aasquare ) ) { // a
            if ( TestPoint::LeftOfAASquare( B, aasquare ) ) // both points left of square
                return false;

            if ( ! TestPoint::BelowAASquare( B, aasquare ) ) { // a-h
                return TestLineSeg::ToAASquare4v_AllButBottom( A,B, square );

            } else if ( ! TestPoint::RightOfAASquare( B, aasquare ) ) { // a-e
                return TestLineSeg::ToAASquare4v_AllButRight( A,B, square );

            } else { // a-f
                return TestLineSeg::ToAASquare4v_All( A,B, square );
            }
        } else if ( TestPoint::RightOfAASquare( A, aasquare ) ) { // c
            if ( TestPoint::RightOfAASquare( B, aasquare ) ) // both right of square
                return false;

            if ( ! TestPoint::BelowAASquare( B, aasquare ) ) { // c-g
                return TestLineSeg::ToAASquare4v_AllButBottom( A,B, square );

            } else if ( ! TestPoint::LeftOfAASquare( B, aasquare ) ) { // c-e
                return TestLineSeg::ToAASquare4v_AllButLeft( A,B, square );

            } else { // c-d
                return TestLineSeg::ToAASquare4v_All( A,B, square );
            }
        } else { // b
            if ( ! TestPoint::BelowAASquare( B, aasquare ) ) {
                if ( TestPoint::RightOfAASquare( B, aasquare ) ) { // b-h
                    return TestLineSeg::ToAASquare4v_TopAndRight( A, B, square );

                } else { // b-g
                    return TestLineSeg::ToAASquare4v_TopAndLeft( A, B, square );
                }
            } else {
                if ( TestPoint::RightOfAASquare( B, aasquare ) ) { // b-f
                    return TestLineSeg::ToAASquare4v_AllButLeft( A, B, square );

                } else if ( TestPoint::LeftOfAASquare( B, aasquare ) ) { // b-d
                    return TestLineSeg::ToAASquare4v_AllButRight( A, B, square );

                } else { // b-e
                    return TestLineSeg::ToAASquare4v_TopAndBottom( A, B, square );
                }
            }
        }
    } else if ( TestPoint::BelowAASquare( A, aasquare ) ) {
        if ( TestPoint::BelowAASquare( B, aasquare ) ) // both points below square
            return false;

        if ( TestPoint::LeftOfAASquare( A, aasquare ) ) { // d
            if ( TestPoint::LeftOfAASquare( B, aasquare ) ) // both points left of square
                return false;

            if ( ! TestPoint::AboveAASquare( B, aasquare ) ) { // d-h
                return TestLineSeg::ToAASquare4v_AllButTop( A,B, square );

            } else if ( ! TestPoint::RightOfAASquare( B, aasquare ) ) { // d-b
                return TestLineSeg::ToAASquare4v_AllButRight( A,B, square );

            } else { // d-c
                return TestLineSeg::ToAASquare4v_All( A,B, square );
            }
        } else if ( TestPoint::RightOfAASquare( A, aasquare ) ) { // f
            if ( TestPoint::RightOfAASquare( B, aasquare ) ) // both points right of square
                return false;

            if ( ! TestPoint::AboveAASquare( B, aasquare ) ) { // f-g
                return TestLineSeg::ToAASquare4v_AllButTop( A,B, square );

            } else if ( ! TestPoint::LeftOfAASquare( B, aasquare ) ) { // f-b
                return TestLineSeg::ToAASquare4v_AllButLeft( A,B, square );

            } else { // f-a
                return TestLineSeg::ToAASquare4v_All( A,B, square );
            }
        } else { // e
            if ( ! TestPoint::AboveAASquare( B, aasquare ) ) {
                if ( TestPoint::RightOfAASquare( B, aasquare ) ) { // e-h
                    return TestLineSeg::ToAASquare4v_TopAndRight( A, B, square );

                } else { // e-g
                    return TestLineSeg::ToAASquare4v_TopAndLeft( A, B, square );
                }
            } else {
                if ( TestPoint::RightOfAASquare( B, aasquare ) ) { // e-c
                    return TestLineSeg::ToAASquare4v_AllButLeft( A, B, square );

                } else if ( TestPoint::LeftOfAASquare( B, aasquare ) ) { // e-a
                    return TestLineSeg::ToAASquare4v_AllButRight( A, B, square );

                } else { // e-b
                    return TestLineSeg::ToAASquare4v_TopAndBottom( A, B, square );
                }
            }
        }
    } else if ( TestPoint::LeftOfAASquare( A, aasquare ) ) { // g
        if ( TestPoint::LeftOfAASquare( B, aasquare ) ) // both points left of square
            return false;

        if ( ! TestPoint::RightOfAASquare( B, aasquare ) ) {
            if ( TestPoint::AboveAASquare( B, aasquare ) ) { // g-b
                return TestLineSeg::ToAASquare4v_TopAndLeft( A, B, square );

            } else { // g-e
                return TestLineSeg::ToAASquare4v_BottomAndLeft( A, B, square );
            }
        } else {
            if ( TestPoint::AboveAASquare( B, aasquare ) ) { // g-c
                return TestLineSeg::ToAASquare4v_AllButBottom( A, B, square );

            } else if ( TestPoint::BelowAASquare( B, aasquare ) ) { // g-f
                return TestLineSeg::ToAASquare4v_AllButTop( A, B, square );

            } else { // g-h
                return TestLineSeg::ToAASquare4v_Sides( A, B, square );
            }
        }
    } else if ( TestPoint::RightOfAASquare( A, aasquare ) ) { // h
        if ( TestPoint::RightOfAASquare( B, aasquare ) ) // both points right of square
            return false;

        if ( ! TestPoint::LeftOfAASquare( B, aasquare ) ) {
            if ( TestPoint::AboveAASquare( B, aasquare ) ) { // h-b
                return TestLineSeg::ToAASquare4v_TopAndRight( A, B, square );

            } else { // h-e
                return TestLineSeg::ToAASquare4v_BottomAndRight( A, B, square );
            }
        } else {
            if ( TestPoint::AboveAASquare( B, aasquare ) ) { // h-a
                return TestLineSeg::ToAASquare4v_AllButBottom( A, B, square );

            } else if ( TestPoint::BelowAASquare( B, aasquare ) ) { // h-d
                return TestLineSeg::ToAASquare4v_AllButTop( A, B, square );

            } else { // h-g
                return TestLineSeg::ToAASquare4v_Sides( A, B, square );
            }
        }
    } else { // i
        return true;
    }
}

bool Collision2D::TestLineSeg::ToLineSeg( CollisionReport2D& report, const Line2f& line, const Line2f& line2 ) {
    return Collision2D::TestLineSeg::ToLineSeg( report, line.vert[0], line.vert[1], line2.vert[0], line2.vert[1] );
}

bool Collision2D::TestLineSeg::ToLineSeg( CollisionReport2D& report, const Line2f& line, const Vec2f &x, const Vec2f& y ) {
    return Collision2D::TestLineSeg::ToLineSeg( report, line.vert[0], line.vert[1], x, y );
}

bool Collision2D::TestLineSeg::ToLineSeg( CollisionReport2D& report, const Vec2f &a, const Vec2f& b, const Line2f& line2 ) {
    return Collision2D::TestLineSeg::ToLineSeg( report, a, b, line2.vert[0], line2.vert[1] );
}

bool Collision2D::TestLineSeg::ToAASquare( const Line2f& line, const AASquare2v& square ) {
    return Collision2D::TestLineSeg::ToAASquare( line.vert[0], line.vert[1], square );
}

bool Collision2D::TestLineSeg::ToCircle( const Vec2f& A, const Vec2f& B, const Vec2f& center, const float radius ) {
    Vec2f closest_point;
    Line2f::GetProjectedPoint( LineTypeT::SEG, A, B, center, &closest_point );
    return Collision2D::TestPoint::ToCircle( closest_point, center, radius);
}

bool Collision2D::TestLineSeg::ToCircle( const Line2f& line, const Vec2f& center, const float radius ) {
    return Collision2D::TestLineSeg::ToCircle( line.vert[0], line.vert[1], center, radius );
}

bool Collision2D::TestLineSeg::ToFace( const Line2f& line, const Face2D& face, const Vec2f& origin ) {
    return Collision2D::TestLineSeg::ToFace( line.vert[0], line.vert[1], face, origin );
}

bool Collision2D::TestLineSeg::ToAASquare( const Vec2f& A, const Vec2f& B, const Vec2f& aabox_min, const Vec2f& aabox_max ) {
    return Collision2D::TestLineSeg::ToAASquare( A, B, AASquare2v( aabox_min, aabox_max ) );
}
