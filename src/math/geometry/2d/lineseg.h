// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_2D_LINESEG_H_
#define SRC_COLLISION_2D_LINESEG_H_

#include "../../../base/main.h"
#include "../../geometry.h"

template< typename TYPE >
class FaceT;

template< typename TYPE >
class VertexT;

typedef VertexT<Vec2f> Vertex2D;

typedef FaceT<Vertex2D> Face2D;

class Sprite;

template< typename TYPE >
class CollisionReportT;

typedef CollisionReportT<Vec2f> CollisionReport2D;

namespace Collision2D {
    namespace TestLineSeg {
        bool PointsOnSameSideOfLine( const Vec2f &pointOne, const Vec2f &pointTwo, const Vec2f &point_on_line, const Vec2f &lineNormal); //!< todo: unit test

        bool ToAASquare4v_TopAndBottom( const Vec2f& A, const Vec2f& B, const AASquare4v& square );
        bool ToAASquare4v_Sides( const Vec2f& A, const Vec2f& B, const AASquare4v& square );
        bool ToAASquare4v_AllButLeft( const Vec2f& A, const Vec2f& B, const AASquare4v& square );
        bool ToAASquare4v_AllButRight( const Vec2f& A, const Vec2f& B, const AASquare4v& square );
        bool ToAASquare4v_AllButTop( const Vec2f& A, const Vec2f& B, const AASquare4v& square );
        bool ToAASquare4v_TopAndRight( const Vec2f& A, const Vec2f& B, const AASquare4v& aasquare );
        bool ToAASquare4v_BottomAndRight( const Vec2f& A, const Vec2f& B, const AASquare4v& aasquare );
        bool ToAASquare4v_TopAndLeft( const Vec2f& A, const Vec2f& B, const AASquare4v& aasquare );
        bool ToAASquare4v_BottomAndLeft( const Vec2f& A, const Vec2f& B, const AASquare4v& aasquare );
        bool ToAASquare4v_AllButBottom( const Vec2f& A, const Vec2f& B, const AASquare4v& square );
        bool ToAASquare4v_All( const Vec2f& A, const Vec2f& B, const AASquare4v& square );

        bool ToAASquare( const Vec2f& A, const Vec2f& B, const Vec2f& aabb_min, const Vec2f& aabb_max );
        bool ToAASquare( const Vec2f& A, const Vec2f& B, const AASquare2v& aasquare );

        bool ToAASquare( const Line2f& line, const Vec2f& aabb_min, const Vec2f& aabb_max );
        bool ToAASquare( const Line2f& line, const AASquare2v& aasquare );

        bool ToLineSeg( const Vec2f &A, const Vec2f& B, const Vec2f &U, const Vec2f& V ); //!< todo: unit test
        bool ToLineSeg( CollisionReport2D& report, const Line2f& line, const Line2f& line2 );
        bool ToLineSeg( CollisionReport2D& report, const Line2f& line, const Vec2f &x, const Vec2f& y );
        bool ToLineSeg( CollisionReport2D& report, const Vec2f &a, const Vec2f& b, const Line2f& line2 );
        bool ToLineSeg( CollisionReport2D& report, const Vec2f &a, const Vec2f& b, const Vec2f &x, const Vec2f& y );
        bool ToCircle( const Line2f& line, const Vec2f& center, const float radius );
        bool ToCircle( const Vec2f& A, const Vec2f& B, const Vec2f& center, const float radius );
        bool ToFace( const Line2f& line, const Face2D& face, const Vec2f& origin );
        bool ToFace( const Vec2f &point_A, const Vec2f &point_B, const Face2D& face, const Vec2f& origin );
    }
}

#endif // SRC_COLLISION_2D_LINESEG_H_
