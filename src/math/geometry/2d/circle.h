// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_2D_CIRCLE_H_
#define SRC_COLLISION_2D_CIRCLE_H_

#include "../../../base/main.h"
#include "../../geometry.h"

template< typename TYPE >
class FaceT;

template< typename TYPE >
class VertexT;

typedef VertexT<Vec2f> Vertex2D;

typedef FaceT<Vertex2D> Face2D;

class Sprite;

namespace Collision2D {
    namespace TestCircle {
        void UnitTest( void );

        bool ToCircle( const Vec2f& c1, const float r1, const Vec2f& c2, const float r2 );
        void UnitTest_ToCircle( void );
        
        bool ToPoly( const std::vector<Vec2f>& clockwise_poly, const Vec2f& center, const float radius );
        void UnitTest_ToPoly( void );
        
        bool ToAASquare( const Vec2f& center, const float radius, const Vec2f& aabb_min, const Vec2f& aabb_max );
        void UnitTest_ToAASquare( void );
        
    }
}

#endif // SRC_COLLISION_2D_CIRCLE_H_
