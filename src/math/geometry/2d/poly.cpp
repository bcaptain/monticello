// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./poly.h"

#ifdef CLIB_UNIT_TEST

template bool Collision2D::TestPoly::SAT::GetCollision_Helper( const Vec2f* poly1_verts, const std::size_t poly1_num_verts, const Vec2f* poly2_verts, const std::size_t poly2_num_verts, const Vec2f& axis );
template bool Collision2D::TestPoly::SAT::GetCollision_Helper( const Vec3f* poly1_verts, const std::size_t poly1_num_verts, const Vec3f* poly2_verts, const std::size_t poly2_num_verts, const Vec3f& axis );

bool Collision2D::TestPoly::ToPoly_Convex( const Vec2f* poly1_ordered_verts, const std::size_t poly1_num_verts, const Vec2f* poly2_ordered_verts, const std::size_t poly2_num_verts ) {
    if ( !SAT::GetCollision( poly1_ordered_verts, poly1_num_verts, poly2_ordered_verts, poly2_num_verts ) )
        return false;

    return SAT::GetCollision( poly2_ordered_verts, poly2_num_verts, poly1_ordered_verts, poly1_num_verts );
}

bool Collision2D::TestPoly::SAT::GetCollision( const Vec2f* poly1_verts, const std::size_t poly1_num_verts, const Vec2f* poly2_verts, const std::size_t poly2_num_verts ) {
    ASSERT( poly1_num_verts > 2 );
    ASSERT( poly2_num_verts > 2 );

    for ( std::size_t a=poly1_num_verts-1, b=0; b < poly1_num_verts; a=b++ ) {
        auto const norm = Line2f::GetNormal( poly1_verts[b], poly1_verts[a] );
        if ( ! GetCollision_Helper( poly1_verts, poly1_num_verts, poly2_verts, poly2_num_verts, norm ) ) {
            return false; // no collision
        }
    }

    return true; // collision
}

void Collision2D::TestPoly::UnitTest( void ) {
    UnitTest_ToPoly_Convex();
}

void Collision2D::TestPoly::UnitTest_ToPoly_Convex( void ) {
    const Vec2f tri1[3]{
        Vec2f(0,0),
        Vec2f(-1,0),
        Vec2f(0,1)
    };

    const Vec2f tri2[3]{
        Vec2f(0.5f,0),
        Vec2f(-0.5f,1),
        Vec2f(0.5f,1)
    };

    // tri1 and two collide
    ASSERT( ToPoly_Convex( tri1, 3, tri2, 3 ));

    const Vec2f tri3[3]{
        Vec2f(0,0),
        Vec2f(-1,0),
        Vec2f(-1,1)
    };

    // tri3 and tri2 do not collide
    ASSERT( ! ToPoly_Convex( tri3, 3, tri2, 3 ));
}

#endif // CLIB_UNIT_TEST
