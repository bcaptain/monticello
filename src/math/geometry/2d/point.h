// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_2D_POINT_H_
#define SRC_COLLISION_2D_POINT_H_

#include "../../../base/main.h"
#include "../../geometry.h"

template< typename TYPE >
class FaceT;

template< typename TYPE >
class VertexT;

typedef VertexT<Vec2f> Vertex2D;

typedef FaceT<Vertex2D> Face2D;

class Sprite;
class SweepData;
class IntersectData;

namespace Collision2D {
    namespace TestPoint {
        bool LeftOfAASquare( const Vec2f& A, const AASquare2v& square );
        bool RightOfAASquare( const Vec2f& A, const AASquare2v& square );
        bool AboveAASquare( const Vec2f& A, const AASquare2v& square );
        bool BelowAASquare( const Vec2f& A, const AASquare2v& square );
        bool ToCircle( const Vec2f& point, const Vec2f& center, const float radius );

        // ** Face/Poly

        bool ToFace( const Vec2f& point, const Face2D& face, const Vec2f& geo_origin );
        void UnitTest_ToFace( void );
        
        bool ToPoly( const Vec2f& point, const Vec2f* clockwise_verts, const std::size_t num_verts );
        void UnitTest_ToPoly( void );

        bool BehindLine( const Vec2f& point, const Line2f& line, const float epsilon = EP );
        bool BehindLine( const Vec2f& point, const Vec2f& line_start, const Vec2f& line_end, const float epsilon = EP );

        bool OnLine( const Vec2f& point, const Line2f& line, const float epsilon = EP );
        bool OnLine( const Vec2f& point, const Vec2f& line_start, const Vec2f& line_end, const float epsilon = EP );

        bool BeforeLine( const Vec2f& point, const Line2f& line, const float epsilon = EP );
        bool BeforeLine( const Vec2f& point, const Vec2f& line_start, const Vec2f& line_end, const float epsilon = EP );

        bool ToAASquare4v( const Vec2f& point, const AASquare4v& square );
        bool ToSprite( const Vec2f& point, Sprite& sprite );
        Sprite* ToSpriteList( const Vec2f& point, std::vector< Sprite* >& sprite_list );

        void UnitTest( void );
    }
}

#endif // SRC_COLLISION_2D_POINT_H_
