// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./aabb.h"

bool Collision2D::TestAASquare::BehindLine( const Vec2f& A, const Vec2f& B, const Vec2f& C, const Vec2f& D, const Line2f& line, const float epsilon ) {
    if ( Collision2D::TestPoint::BeforeLine( A, line, epsilon ) )
        return false;

    if ( Collision2D::TestPoint::BeforeLine( B, line, epsilon ) )
        return false;

    if ( Collision2D::TestPoint::BeforeLine( C, line, epsilon ) )
        return false;

    if ( Collision2D::TestPoint::BeforeLine( D, line, epsilon ) )
        return false;

    return true;
}

bool Collision2D::TestAASquare::BeforeLine( const Vec2f& A, const Vec2f& B, const Vec2f& C, const Vec2f& D, const Line2f& line, const float epsilon ) {
    if ( Collision2D::TestPoint::BehindLine( A, line, epsilon ) )
        return false;

    if ( Collision2D::TestPoint::BehindLine( B, line, epsilon ) )
        return false;

    if ( Collision2D::TestPoint::BehindLine( C, line, epsilon ) )
        return false;

    if ( Collision2D::TestPoint::BehindLine( D, line, epsilon ) )
        return false;

    return true;
}


float Collision2D::TestAASquare::SignedDistToLine( const Vec2f& A, const Vec2f& B, const Vec2f& C, const Vec2f& D, const Line2f& line ) {
    // if we ever find two points that exist on opposite sides of the line, there is intersection
    bool less=false,more=false;

    const float dist1 = Line2f::GetDistToPoint( line, A );
    if ( dist1 < 0 ) less = true;
    else if (dist1 > 0 ) more = true;


    const float dist2 = Line2f::GetDistToPoint( line, B );
    if ( dist2 < 0 ) less = true;
    else if (dist2 > 0 ) more = true;
    
    if ( less && more )
        return 0;

    const float dist3 = Line2f::GetDistToPoint( line, C );

    if ( dist3 < 0 ) less = true;
    else if (dist3 > 0 ) more = true;
    
    if ( less && more )
        return 0;

    const float dist4 = Line2f::GetDistToPoint( line, D );

    if ( dist4 < 0 ) less = true;
    else if (dist4 > 0 ) more = true;
    
    if ( less && more )
        return 0;

    // **

    float least = dist1;

    if ( fabsf(dist2) < fabsf(dist1) )
        least = dist2;

    if ( fabsf(dist3) < fabsf(dist2) )
        least = dist3;

    if ( fabsf(dist4) < fabsf(dist3) )
        least = dist4;

    return least;
}
