// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./point.h"
#include "../../collision/collision.h"
#include "../../../sprites/sprite.h"
#include "../../../sprites/spriteList.h"

bool Collision2D::TestPoint::OnLine( const Vec2f& point, const Vec2f& line_start, const Vec2f& line_end, const float epsilon ) {
    const float dist = Line2f::GetDistToPoint( LineTypeT::LINE, line_start, line_end, point );
    return dist >= -epsilon && dist <= epsilon;
}

bool Collision2D::TestPoint::ToCircle( const Vec2f& point, const Vec2f& center, const float radius ) {
    return ( Vec2f::Len( point - center ) - radius ) < 0;
}

bool Collision2D::TestPoint::ToAASquare4v( const Vec2f& A, const AASquare4v& square ) {
    if ( A.x > square.tr.x + EP) return false;
    if ( A.x < square.bl.x - EP ) return false;
    if ( A.y > square.tr.y + EP ) return false;
    if ( A.y < square.bl.y - EP ) return false;
    return true;
}

bool Collision2D::TestPoint::ToFace( const Vec2f& point, const Face2D& face, const Vec2f& origin ) {
    // add origin and reverse the order of facevertices (they are wound counter-clockwise and drawn with y axis flipped)
    Vec2f verts[4]{
        Vec2f( face.vert[3].co.x + origin.x, face.vert[3].co.y + origin.y ),
        Vec2f( face.vert[2].co.x + origin.x, face.vert[2].co.y + origin.y ),
        Vec2f( face.vert[1].co.x + origin.x, face.vert[1].co.y + origin.y ),
        Vec2f( face.vert[0].co.x + origin.x, face.vert[0].co.y + origin.y ),
    };
    return ToPoly( point, verts, 4 );
}

bool Collision2D::TestPoint::ToPoly( const Vec2f& point, const Vec2f* verts, const std::size_t num_verts ) {
    if ( num_verts < 3 )
        return false; // room not constructed yet

    for ( std::size_t a=num_verts-1, b=0; b<num_verts; a=b++ )
        if ( Collision2D::TestPoint::BeforeLine( point, verts[a], verts[b] ) )
            return false;

    return true;
}

bool Collision2D::TestPoint::ToSprite( const Vec2f& point, Sprite& sprite ) {
    if ( sprite.IsVisible() == false )
        return false;

    return TestPoint::ToFace( point, *sprite.GetFace(), sprite.GetOrigin().ToVec2() );
}

Sprite * Collision2D::TestPoint::ToSpriteList( const Vec2f& point, std::vector< Sprite* >& sprite_list ) {
    Sprite *collision;
    Vec2f adjusted_point;

    // The last sprite in the list is rendered last. so if we check the list backward
    for ( uint i=sprite_list.size(); i>0; ) {
        --i;

        Sprite* sprite = sprite_list[i];

        if ( ! sprite )
            continue;

        if ( sprite->IsVisible() == false  )
            continue;

        // we must check all elements of all windows until a match is found
        if ( sprite->children ) {
            // window element origins are relative to the window
            adjusted_point = point;
            Vec3f sprite_origin( sprite->GetOrigin() );
            adjusted_point.x -= sprite_origin.x;
            adjusted_point.y -= sprite_origin.y;

            collision = ToSpriteList( adjusted_point, sprite->children->list );
        } else {
            collision = nullptr;
        }

        // check collision on this sprite itself if this is not a parent or one if it's children were not collided with
        if ( ! collision )
            if ( Collision2D::TestPoint::ToSprite( point, *sprite ) )
                collision = sprite; // yes, we collide with this sprite

        // if something inside the parent was clicked, or this sprite itself was clicked.
        // This also has the effect that windows absorb clicks if they are on top of any elements. That's a good thing.
        if ( collision ) {
            return collision;
        }
    }

    return nullptr;
}

bool Collision2D::TestPoint::BehindLine( const Vec2f& point, const Line2f& line, const float epsilon ) {
    return BehindLine( point, line.vert[0], line.vert[1], epsilon );
}

bool Collision2D::TestPoint::BehindLine( const Vec2f& point, const Vec2f& line_start, const Vec2f& line_end, const float epsilon ) {
    return Line2f::GetDistToPoint( LineTypeT::LINE, line_start, line_end, point ) < epsilon;
}

bool Collision2D::TestPoint::OnLine( const Vec2f& point, const Line2f& line, const float epsilon ) {
    return OnLine( point, line.vert[0], line.vert[1], epsilon );
}

bool Collision2D::TestPoint::BeforeLine( const Vec2f& point, const Line2f& line, const float epsilon ) {
    return BeforeLine( point, line.vert[0], line.vert[1], epsilon );
}

bool Collision2D::TestPoint::BeforeLine( const Vec2f& point, const Vec2f& line_start, const Vec2f& line_end, const float epsilon ) {
    return Line2f::GetDistToPoint( LineTypeT::LINE, line_start, line_end, point ) > epsilon;
}

bool Collision2D::TestPoint::LeftOfAASquare( const Vec2f& A, const AASquare2v& square ) {
    return ( A.x < square.mins.x - EP );
}

bool Collision2D::TestPoint::RightOfAASquare( const Vec2f& A, const AASquare2v& square ) {
    return ( A.x > square.maxs.x + EP );
}

bool Collision2D::TestPoint::AboveAASquare( const Vec2f& A, const AASquare2v& square ) {
    return ( A.y > square.maxs.y + EP );
}

bool Collision2D::TestPoint::BelowAASquare( const Vec2f& A, const AASquare2v& square ) {
    return ( A.y < square.mins.y - EP );
}

#ifdef CLIB_UNIT_TEST
    void Collision2D::TestPoint::UnitTest( void ) {
        UnitTest_ToFace();
        UnitTest_ToPoly();
    }

    void Collision2D::TestPoint::UnitTest_ToFace( void ) {
        Vec2f point;
        Vec2f origin(0,0);

        Face2D face;
        face.Resize( 4 );

        // faces are built counter-clockwise since they are drawn with the y axis flipped
        face.vert[3].co.Set(-1.05f,-1.05f); // bottom left
        face.vert[2].co.Set(-1.05f,1.05f); // top left
        face.vert[1].co.Set(1.05f,1.05f); // top right
        face.vert[0].co.Set(1.05f,-1.05f); // bottom right

        Vec2f succeed_left_edge(-1,0);
        Vec2f succeed_right_edge(1,0);
        Vec2f succeed_top_edge(0,1);
        Vec2f succeed_top_right_corner(1,1);
        Vec2f succeed_top_left_corner(-1,1);
        Vec2f succeed_bottom_edge(0,-1);
        Vec2f succeed_bottom_right_corner(1,-1);
        Vec2f succeed_bottom_left_corner(-1,-1);

        Vec2f fail_left(-1.1f,0);
        Vec2f fail_right(1.1f,0);
        Vec2f fail_top(0,1.1f);
        Vec2f fail_top_right(1.1f,1.1f);
        Vec2f fail_top_left(-1.1f,1.1f);
        Vec2f fail_bottom(0,-1.1f);
        Vec2f fail_bottom_right(1.1f,-1.1f);
        Vec2f fail_bottom_left(-1.1f,-1.1f);
        Vec2f fail_inside(0.2f,0.2f);

        point = succeed_left_edge;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_right_edge;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_top_edge;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_top_right_corner;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_top_left_corner;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_bottom_edge;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_bottom_right_corner;
        ASSERT(   ToFace( point, face, origin ) );

        point = succeed_bottom_left_corner;
        ASSERT(   ToFace( point, face, origin ) );

        point = fail_left;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_right;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_top;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_top_right;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_top_left;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_bottom;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_bottom_right;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_bottom_left;
        ASSERT( ! ToFace( point, face, origin ) );

        point = fail_inside;
        ASSERT( ToFace( point, face, origin ) );
    }

    void Collision2D::TestPoint::UnitTest_ToPoly( void ) {
        Vec2f verts[4]{
            Vec2f(-1.05f,-1.05f), // bottom left
            Vec2f(-1.05f,1.05f), // top left
            Vec2f(1.05f,1.05f), // top right
            Vec2f(1.05f,-1.05f) // bottom right
        };

        Vec2f succeed_left_edge(-1,0);
        Vec2f succeed_right_edge(1,0);
        Vec2f succeed_top_edge(0,1);
        Vec2f succeed_top_right_corner(1,1);
        Vec2f succeed_top_left_corner(-1,1);
        Vec2f succeed_bottom_edge(0,-1);
        Vec2f succeed_bottom_right_corner(1,-1);
        Vec2f succeed_bottom_left_corner(-1,-1);

        Vec2f fail_left(-1.1f,0);
        Vec2f fail_right(1.1f,0);
        Vec2f fail_top(0,1.1f);
        Vec2f fail_top_right(1.1f,1.1f);
        Vec2f fail_top_left(-1.1f,1.1f);
        Vec2f fail_bottom(0,-1.1f);
        Vec2f fail_bottom_right(1.1f,-1.1f);
        Vec2f fail_bottom_left(-1.1f,-1.1f);
        Vec2f fail_inside(0.2f,0.2f);

        ASSERT(   ToPoly( succeed_left_edge, verts, 4 ) );
        ASSERT(   ToPoly( succeed_right_edge, verts, 4 ) );
        ASSERT(   ToPoly( succeed_top_edge, verts, 4 ) );
        ASSERT(   ToPoly( succeed_top_right_corner, verts, 4 ) );
        ASSERT(   ToPoly( succeed_top_left_corner, verts, 4 ) );
        ASSERT(   ToPoly( succeed_bottom_edge, verts, 4 ) );
        ASSERT(   ToPoly( succeed_bottom_right_corner, verts, 4 ) );
        ASSERT(   ToPoly( succeed_bottom_left_corner, verts, 4 ) );
        ASSERT( ! ToPoly( fail_left, verts, 4 ) );
        ASSERT( ! ToPoly( fail_right, verts, 4 ) );
        ASSERT( ! ToPoly( fail_top, verts, 4 ) );
        ASSERT( ! ToPoly( fail_top_right, verts, 4 ) );
        ASSERT( ! ToPoly( fail_top_left, verts, 4 ) );
        ASSERT( ! ToPoly( fail_bottom, verts, 4 ) );
        ASSERT( ! ToPoly( fail_bottom_right, verts, 4 ) );
        ASSERT( ! ToPoly( fail_bottom_left, verts, 4 ) );
        ASSERT( ToPoly( fail_inside, verts, 4 ) );

        Vec2f point;

        point = succeed_left_edge;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_right_edge;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_top_edge;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_top_right_corner;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_top_left_corner;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_bottom_edge;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_bottom_right_corner;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = succeed_bottom_left_corner;
        ASSERT(   ToPoly( point, verts, 4 ) );

        point = fail_left;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_right;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_top;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_top_right;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_top_left;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_bottom;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_bottom_right;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_bottom_left;
        ASSERT( ! ToPoly( point, verts, 4 ) );

        point = fail_inside;
        ASSERT( ToPoly( point, verts, 4 ) );
    }
#endif // CLIB_UNIT_TEST
