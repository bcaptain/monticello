// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_2D_POLY_H_
#define SRC_COLLISION_2D_POLY_H_

#include "../../../base/main.h"
#include "../../geometry.h"

namespace Collision2D {
    namespace TestPoly {

        // uses the Separating Axis Theorem (SAT) explained here: http://www.codezealot.org/archives/55
        bool ToPoly_Convex( const Vec2f* poly1_ordered_verts, const std::size_t poly1_num_verts, const Vec2f* poly2_ordered_verts, const std::size_t poly2_num_verts );

        namespace SAT {
            template< typename VEC_TYPE >
            bool GetCollision_Helper( const VEC_TYPE* poly1_verts, const std::size_t poly1_num_verts, const VEC_TYPE* poly2_verts, const std::size_t poly2_num_verts, const VEC_TYPE& axis ); //!< not for general consumption. vertices can be in clockwise or counter, doesn't matter as long as they are ordered in one of them.

            bool GetCollision( const Vec2f* poly1_verts, const std::size_t poly1_num_verts, const Vec2f* poly2_verts, const std::size_t poly2_num_verts );
        }

        #ifdef CLIB_UNIT_TEST
            void UnitTest( void );
            void UnitTest_ToPoly_Convex( void );
        #endif //CLIB_UNIT_TEST
    }
}

template< typename VEC_TYPE >
bool Collision2D::TestPoly::SAT::GetCollision_Helper( const VEC_TYPE* poly1_verts, const std::size_t poly1_num_verts, const VEC_TYPE* poly2_verts, const std::size_t poly2_num_verts, const VEC_TYPE& axis ) {
    ASSERT( poly1_num_verts > 0 );
    ASSERT( poly2_num_verts > 0 );
    std::size_t v;
    float p_dot;

    // ** project all the points from poly1 onto the axis
    float poly1_min, poly1_max;
    poly1_min = poly1_max = axis.Dot( poly1_verts[0] );

    for ( v = 1; v < poly1_num_verts; ++v ) {
        p_dot = axis.Dot( poly1_verts[v] );
        poly1_min = std::fminf( poly1_min, p_dot );
        poly1_max = std::fmaxf( poly1_max, p_dot );
    }

    // ** project each point from poly2 onto the axis and test for collision
    typedef int8 SideOfPoly1ProjectionT_BaseType;
    enum class SideOfPoly1ProjectionT : SideOfPoly1ProjectionT_BaseType {
        INSIDE=0 // default
        , LEFT=-1
        , RIGHT=1
    };

    SideOfPoly1ProjectionT prev_side = SideOfPoly1ProjectionT::INSIDE;
    for ( v = 0; v < poly2_num_verts; ++v ) {
        p_dot = axis.Dot( poly2_verts[v] );

        if ( p_dot < poly1_min ) { // point is on the LEFT side

            // if any two points are on opposite sides of poly1's projections, there is collision on this axis
            if ( prev_side == SideOfPoly1ProjectionT::RIGHT )
                return true;

            prev_side = SideOfPoly1ProjectionT::LEFT;

        } else if ( p_dot > poly1_max ) { // point is on the RIGHT side

            // if any two points are on opposite sides of poly1's projections, there is collision on this axis
            if ( prev_side == SideOfPoly1ProjectionT::LEFT )
                return true;

            prev_side = SideOfPoly1ProjectionT::RIGHT;

        } else { // the point is INSIDE of the poly1's projection
            return true;
        }
    }

    return false;
}

#endif // SRC_COLLISION_2D_POLY_H_
