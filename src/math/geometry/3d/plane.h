// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CLIB_MATH_PLANE_H_
#define SRC_CLIB_MATH_PLANE_H_

#include <mutex>
#include "../../../base/main.h"
#include "../../geometry.h"

/*!
    Note: This container intentionally does not encapsellate operations
    All operations are for clockwise winding.
    Plane normals are positive and point away from the front face.
*/

#ifdef CLIB_UNIT_TEST
    void UnitTest_Plane3f( void );
#endif // CLIB_UNIT_TEST

class Plane3f {
public:
    Plane3f( void );
    Plane3f( const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC );
    Plane3f( const Vec3f& planeA, const Vec3f& norm );
    Plane3f( const Plane3f& other );
    ~Plane3f( void ) = default;

public:
    Plane3f& operator=( const Plane3f& other );
    void Set( const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC );
    void Set( const Vec3f& planeA, const Vec3f& norm );

public:
    static Vec3f GetUnnormalizedNormal( const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC );
    
    static Vec3f GetNormal( const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC );
    Vec3f GetNormal( void ) const;
    static Vec3f GetNormal( const Vec3f* three_clockwise_verts );
    
    #ifdef CLIB_UNIT_TEST
        static void UnitTest_Normal( void );
    #endif // CLIB_UNIT_TEST

    //! Distance is positive when in front of plane, negative when behind
    static float GetDistToPoint( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C );
    static float GetDistToPoint( const Vec3f& point, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToPoint( const Vec3f& point ) const;
    
    #ifdef CLIB_UNIT_TEST
        static void UnitTest_GetDistToPoint( void );
    #endif // CLIB_UNIT_TEST

    static float GetDistToLine( const Line3f& lineseg, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToLine( const Line3f& lineseg ) const;
    
    #ifdef CLIB_UNIT_TEST
        static void UnitTest_GetDistToLine( void );
    #endif // CLIB_UNIT_TEST

    static float GetDistToAABox3D( const AABox3D& lineseg, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToAABox3D( const AABox3D& lineseg ) const;
    
    static float GetDistToBox3D( const Box3D& lineseg, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToBox3D( const Box3D& lineseg ) const;
    
    static float GetDistToCylinder( const Cylinder& cyl, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToCylinder( const Cylinder& cyl ) const;
    
    #ifdef CLIB_UNIT_TEST
        static void UnitTest_GetDistToCylinder( void );
    #endif // CLIB_UNIT_TEST

    static float GetDistToCone3D( const Cone3D& cone, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToCone3D( const Cone3D& cone ) const;
    
    #ifdef CLIB_UNIT_TEST
        static void UnitTest_GetDistToCone3D( void );
    #endif // CLIB_UNIT_TEST

    //!< This will return the shortest distance to any of the vertices. It can give the closest distance to any polytope. If it intersects, will return zero
    static float GetDistToVertGroup( const Vec3f* poly_verts, const std::size_t num_verts, const Vec3f& planeA, const Vec3f& plane_normal );
    float GetDistToVertGroup( const Vec3f* poly_verts, const std::size_t num_verts ) const;

    Vec3f GetProjectedPoint( const Vec3f& point );

    //! return the point moved toward the plane by it's distance from it, along the plane's normal
    static Vec3f GetProjectedPoint( const Vec3f& point, const Vec3f& planeA, const Vec3f& planeNormal );
        static Vec3f GetProjectedPoint( const Vec3f& point, const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC ); //!< Less efficient verload of GetProjectedPoint: has to calculate normal

    Vec3f GetPoint( void ) const;

public:
    Vec3f vert;
    Vec3f norm;
};

#endif  //SRC_CLIB_MATH_PLANE
