// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./capsule.h"
#include "./plane.h"
#include "./aabox.h"

#ifdef CLIB_UNIT_TEST
    void Capsule::UnitTest( void ) {
        Capsule::UnitTest_ToPlane();
        Capsule::UnitTest_ToAABox3D();
    }

    void Capsule::UnitTest_ToAABox3D( void ) {

        //todo: this isn't very extensive for now, since Capsule::ToAABox is just early outs before calling Sphere::SweepToAABox
        const AABox3D aabox( -1, 1, -1, 1, -1, 1 );

        Capsule graze_edge( Vec3f(-4,0,0), Vec3f(1,1,0).GetNormalized(), sqrtf(2)+0.01f, 8 );
        ASSERT( Capsule::Intersects( graze_edge, aabox ) );
        
        Capsule penetrate_edge( Vec3f(-4,0,0), Vec3f(1,1,0).GetNormalized(), sqrtf(2)+0.1f, 8 );
        ASSERT( Capsule::Intersects( penetrate_edge, aabox ) );
        
        Capsule miss_edge( Vec3f(-4,0,0), Vec3f(1,1,0).GetNormalized(), sqrtf(2)-0.1f, 8 );
        ASSERT( ! Capsule::Intersects( miss_edge, aabox ) );
        
        Capsule inside( Vec3f(-0.1f,0,0), Vec3f(1,0,0), 0.1f, 0.025f );
        ASSERT( Capsule::Intersects( inside, aabox ) );
        
        Capsule encompass( Vec3f(-100,0,0), Vec3f(1,0,0), 10, 200 );
        ASSERT( Capsule::Intersects( encompass, aabox ) );
        
        Capsule graze_side( Vec3f(-2,0,0), Vec3f(0,1,0), 1, 5 );
        ASSERT( Capsule::Intersects( graze_side, aabox ) );
        
        Capsule miss_side( Vec3f(-2,0,0), Vec3f(0,1,0), 0.9f, 5  );
        ASSERT( !Capsule::Intersects( miss_side, aabox ) );
        
        Capsule hit_corner( Vec3f(-3,-3,0), Vec3f(1,1,1).GetNormalized(), 1.1f, 9  );
        ASSERT( Capsule::Intersects( hit_corner, aabox ) );
        
        Capsule miss_corner( Vec3f(-3,-3,0), Vec3f(1,1,1).GetNormalized(), 0.8f, 9  );
        ASSERT( ! Capsule::Intersects( miss_corner, aabox ) );
        
        Capsule graze_corner( Vec3f(-3,-3,0), Vec3f(1,1,1).GetNormalized(), 1.0f, 9  );
        ASSERT( Capsule::Intersects( graze_corner, aabox ) );

    }

    void Capsule::UnitTest_ToPlane( void ) {
        const Vec3f cap_origin(-2,0,0);
        const Vec3f cap_normal(1,0,0);
        const float cap_radius=1;
        const float cap_length=4;
        const Capsule cap_around_x_axis(
            cap_origin,
            cap_normal,
            cap_radius,
            cap_length
        );

        const float half_cap_length = cap_length / 2;

        Plane3f slice_through_origin_xy( Vec3f(0,0,0), Vec3f(2,0,0), Vec3f(0,2,0) );
        Plane3f slice_through_origin_xz( Vec3f(0,0,0), Vec3f(2,0,0), Vec3f(0,0,2) );
        Plane3f slice_through_origin_yz( Vec3f(0,0,0), Vec3f(0,2,0), Vec3f(0,0,2) );

        ASSERT( Capsule::Intersects( cap_around_x_axis, slice_through_origin_xy ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, slice_through_origin_xz ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, slice_through_origin_yz ) );

        const Vec3f little_after_end( cap_normal*(half_cap_length+(cap_radius/2)) );
        Plane3f slice_end  ( Vec3f(0,0,0)+little_after_end, Vec3f(0,0,2)+little_after_end, Vec3f(0,2,0)+little_after_end );

        const Vec3f little_before_start( -cap_normal*(cap_radius/2) );
        Plane3f slice_start( Vec3f(0,0,0)+little_before_start, Vec3f(0,0,2)+little_before_start, Vec3f(0,2,0)+little_before_start );

        ASSERT( Capsule::Intersects( cap_around_x_axis, slice_end ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, slice_start ) );


        const Vec3f at_end( cap_normal*(half_cap_length+cap_radius) );
        Plane3f touch_end( Vec3f(0,0,0)+at_end, Vec3f(0,0,2)+at_end, Vec3f(0,2,0)+at_end );

        const Vec3f at_start( -at_end );
        Plane3f touch_start( Vec3f(0,0,0)+at_start, Vec3f(0,0,2)+at_start, Vec3f(0,2,0)+at_start );

        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_end ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_start ) );


        const Vec3f just_beyond_end( cap_normal*(half_cap_length+cap_radius+0.01f) );
        Plane3f just_after_end( Vec3f(0,0,0)+just_beyond_end, Vec3f(0,0,2)+just_beyond_end, Vec3f(0,2,0)+just_beyond_end );

        const Vec3f just_before_start( -just_beyond_end );
        Plane3f right_before_start( Vec3f(0,0,0)+just_before_start, Vec3f(0,0,2)+just_before_start, Vec3f(0,2,0)+just_before_start );

        ASSERT( ! Capsule::Intersects( cap_around_x_axis, just_after_end ) );
        ASSERT( ! Capsule::Intersects( cap_around_x_axis, right_before_start ) );

        Plane3f touch_on_xy_zPos( Vec3f(0,0,cap_radius), Vec3f(2,0,cap_radius), Vec3f(0,2,cap_radius) );
        Plane3f touch_on_xy_zNeg( Vec3f(0,0,-cap_radius), Vec3f(2,0,-cap_radius), Vec3f(0,2,-cap_radius) );
        Plane3f touch_on_xz_yPos( Vec3f(0,cap_radius,0), Vec3f(2,cap_radius,0), Vec3f(0,cap_radius,2) );
        Plane3f touch_on_xz_yNeg( Vec3f(0,-cap_radius,0), Vec3f(2,-cap_radius,0), Vec3f(0,-cap_radius,2) );
        Plane3f touch_on_yz_xPos( Vec3f(half_cap_length+cap_radius,2,0), Vec3f(half_cap_length+cap_radius,0,0), Vec3f(half_cap_length+cap_radius,0,2) );
        Plane3f touch_on_yz_xNeg( Vec3f(-half_cap_length+cap_radius,2,0), Vec3f(-half_cap_length+cap_radius,0,0), Vec3f(-half_cap_length+cap_radius,0,2) );

        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_on_xy_zPos ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_on_xy_zNeg ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_on_xz_yPos ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_on_xz_yNeg ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_on_yz_xPos ) );
        ASSERT( Capsule::Intersects( cap_around_x_axis, touch_on_yz_xNeg ) );

        Plane3f NOtouch_on_xy_zPos( Vec3f(0,0,(cap_radius+0.01f)), Vec3f(2,0,(cap_radius+0.01f)), Vec3f(0,2,(cap_radius+0.01f)) );
        Plane3f NOtouch_on_xy_zNeg( Vec3f(0,0,-(cap_radius+0.01f)), Vec3f(2,0,-(cap_radius+0.01f)), Vec3f(0,2,-(cap_radius+0.01f)) );
        Plane3f NOtouch_on_xz_yPos( Vec3f(0,(cap_radius+0.01f),0), Vec3f(2,(cap_radius+0.01f),0), Vec3f(0,(cap_radius+0.01f),2) );
        Plane3f NOtouch_on_xz_yNeg( Vec3f(0,-(cap_radius+0.01f),0), Vec3f(2,-(cap_radius+0.01f),0), Vec3f(0,-(cap_radius+0.01f),2) );
        Plane3f NOtouch_on_yz_xPos( Vec3f((half_cap_length+cap_radius+0.01f),2,0), Vec3f((half_cap_length+cap_radius+0.01f),0,0), Vec3f((half_cap_length+cap_radius+0.01f),0,2) );
        Plane3f NOtouch_on_yz_xNeg( Vec3f(-(half_cap_length+cap_radius+0.01f),2,0), Vec3f(-(half_cap_length+cap_radius+0.01f),0,0), Vec3f(-(half_cap_length+cap_radius+0.01f),0,2) );

        ASSERT( ! Capsule::Intersects( cap_around_x_axis, NOtouch_on_xy_zPos ) );
        ASSERT( ! Capsule::Intersects( cap_around_x_axis, NOtouch_on_xy_zNeg ) );
        ASSERT( ! Capsule::Intersects( cap_around_x_axis, NOtouch_on_xz_yPos ) );
        ASSERT( ! Capsule::Intersects( cap_around_x_axis, NOtouch_on_xz_yNeg ) );
        ASSERT( ! Capsule::Intersects( cap_around_x_axis, NOtouch_on_yz_xPos ) );
        ASSERT( ! Capsule::Intersects( cap_around_x_axis, NOtouch_on_yz_xNeg ) );

    }

#endif //CLIB_UNIT_TEST
