// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CLIB_MATH_TRI_H_
#define SRC_CLIB_MATH_TRI_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Circle2f;
class Circle3f;

class Triangle3f {
public:
    Triangle3f( void ) = delete;
    Triangle3f( const Triangle3f& other ) = delete;

public:
    Triangle3f& operator=( const Triangle3f& other ) = delete;

public:
    static Circle3f GetIncircle( const Vec3f& A, const Vec3f& B, const Vec3f& C );

    static void UnitTest( void );

    static void GetIncircle_UnitTest( void );
};

#endif  //SRC_CLIB_MATH_TRI_H_
