// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include <cfloat>
#include "./cylinder.h"

Vec3f Cylinder::GetEnd( void ) const {
    return {
          normal.x * length + origin.x
        , normal.y * length + origin.y
        , normal.z * length + origin.z
    };
}

Vec3f Cylinder::GetFarthestVert( const Vec3f& normalized_dir ) const {
    const Vec3f cyl_top = GetEnd();
    const Vec3f start( Sphere::GetFarthestVert( normalized_dir, origin, radius ) );
    const Vec3f end( Sphere::GetFarthestVert( normalized_dir, cyl_top, radius ) );

    if ( start.Dot( normalized_dir ) > end.Dot( normalized_dir ) ) {
        // ** if the distance of the point is before the start of the cylinder, it exists on the sphere and we need to move it onto the cylinder
        const float dist = Plane3f::GetDistToPoint( start, origin, normal );
        if ( dist < 0 ) {
            const Vec3f projectedToBottom = start + normal * fabsf( dist );
            const Vec3f dir_from_center = (projectedToBottom - origin).GetNormalized();
            const Vec3f moved_to_edge = origin + dir_from_center * radius;
            return moved_to_edge;
        }
        return start;
    } else {
        // ** if the distance of the point is farther than the end of the cylinder, it exists on the sphere and we need to move it
        const float dist = Plane3f::GetDistToPoint( end, origin, normal );
        if ( dist > length ) {
            const Vec3f projectedToTop = end - normal * ( fabsf(dist) - length);
            const Vec3f dir_from_center = (projectedToTop - cyl_top).GetNormalized();
            const Vec3f moved_to_edge = cyl_top + dir_from_center * radius;
            return moved_to_edge;
        }
        return end;
    }
}

bool Cylinder::Intersects( const Cylinder& cyl, const Sphere& sphere ) {
    return Sphere::Intersects( sphere, cyl );
}

bool Cylinder::ToPlane( const Cylinder& cyl, const Vec3f& planeA, const Vec3f& plane_normal ) {
    return Maths::Approxf( Plane3f::GetDistToCylinder( cyl, planeA, plane_normal ), 0 );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Cylinder::SweepToModel( CollisionReport3D& report, const Cylinder& cyl, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {
        ERR("Not Implemented: Cylinder::SweepToModel\n"); //toimplement
        return false;
    }

    bool Cylinder::ToModel( const Cylinder& cyl, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
        ERR("Not Implemented: Cylinder::ToModel\n"); //toimplement
        return false;
    }

    bool Cylinder::Intersects( const Cylinder& cyl, const SemiCylinder& semicyl ) {
        ERR("Not Implemented: Cylinder::Intersects( const Cylinder& cyl, const SemiCylinder& semicyl )\n"); //toimplement
        return false;
    }

    bool Cylinder::Intersects( const Cylinder& cyl, const Cylinder& other ) {
        ERR("Not Implemented: Cylinder::Intersects( const Cylinder& cyl, const Cylinder& cyl )\n"); //toimplement
        return false;
    }

    bool Cylinder::Intersects( const Cylinder& cyl, const Capsule& capsule ) {
        ERR("Not Implemented: Cylinder::Intersects( const Cylinder& cyl, const Capsule& capsule )\n"); //toimplement
        return false;
    }

    bool Cylinder::Intersects( const Cylinder& cyl, const Polygon3D& polygon ) {
        ERR("Not Implemented: Cylinder::Intersects( const Cylinder& cyl, const Polygon3D& polygon )\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Cylinder::Intersects( const Cylinder& cyl, const Box3D& box ) {
    int points_behind=0;
    
    for ( uint face=0; face<6; ++face) {
        const Vec3f face_norm = box.GetFaceNormal( face );
        const Vec3f face_vert = box.GetFaceVert( face );
        
        const Vec3f vert_behind = cyl.GetFarthestVert( -face_norm );
        const float dist_behind = Plane3f::GetDistToPoint( vert_behind, face_vert, face_norm );
        
        if ( dist_behind > 0 ) {
            // todo: profile for our cases, whether testing for this early-out saves cycles or adds them:
            const Vec3f vert_before = cyl.GetFarthestVert( face_norm );
            const float dist_before = Plane3f::GetDistToPoint( vert_before, face_vert, face_norm );
            
            if ( dist_before > 0 ) {
                return false; // cylindar exists entirely before of the face
            } else {
                ++points_behind;
            }
        } else {
            ++points_behind;
        }
    }
    
    return points_behind == 6; // if any points were found behind all planes, there is intersection
}

bool Cylinder::Intersects( const Cylinder& cyl, const AABox3D& aabox ) {
    if ( ! Capsule::Intersects( Capsule( cyl.origin, cyl.normal, cyl.radius, cyl.length ), aabox ) )
        return false;

    const Box3D box( aabox );

    // planes at ends of cylinder, both facing outward from cylinder
    const Plane3f bottom( cyl.origin, cyl.normal );
    if ( bottom.GetDistToBox3D( box ) < 0 )
        return false;

    const Plane3f top( cyl.origin + (cyl.normal * cyl.length), -cyl.normal );
    if ( top.GetDistToBox3D( box ) < 0 )
        return false;

    return true;
}

bool Cylinder::Intersects( const Cylinder& cyl, const Line3f& line ) {
    const Vec3f cyl_end = cyl.GetEnd();
    // lineseg totally beyond the end?
    if ( Plane3f::GetDistToLine(line, cyl_end, cyl.normal) > 0 && Plane3f::GetDistToLine(line, cyl_end, cyl.normal ) > 0 )
        return false;

    // lineseg totally before the start?
    if ( Plane3f::GetDistToLine(line, cyl.origin, cyl.normal) < 0 && Plane3f::GetDistToLine(line, cyl.origin, cyl.normal) < 0 )
        return false;
        
    const float lines_dist = Line3f::SquaredDistToLine( line.vert[0], line.vert[1], LineTypeT::SEG, cyl.origin, cyl_end, LineTypeT::SEG );
    return lines_dist <= cyl.radius; // touching is considered an intersection
}

bool Cylinder::Intersects( const Cylinder& cyl, const Vec3f& point ) {
    const Vec3f cyl_end = cyl.GetEnd();
    // point totally beyond the end?
    if ( Plane3f::GetDistToPoint(point, cyl_end, cyl.normal) > 0 )
        return false;

    // point totally before the start?
    if ( Plane3f::GetDistToPoint(point, cyl.origin, cyl.normal) < 0 )
        return false;
        
    const float point_dist = Line3f::GetDistToPoint( LineTypeT::SEG, cyl.origin, cyl_end, point );
    return point_dist <= cyl.radius; // touching is considered an intersection
}

#ifdef CLIB_UNIT_TEST
    void Cylinder::UnitTest( void ) {
        Cylinder::UnitTest_ToAABox3D();
        Cylinder::UnitTest_ToLineSeg();
        Cylinder::UnitTest_ToBox3D();
    }

    void Cylinder::UnitTest_ToLineSeg( void ) {
        const Cylinder cyl(Vec3f(2,0,0), Vec3f(0,1,0), 1, 3 );
        
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(0,1,0), Vec3f(2,-1,0), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(4,1,0), Vec3f(2,-5,0), LineTypeT::SEG ) ) );
        
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(4,2,0), LineTypeT::SEG ) ) );
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(1,2,0), LineTypeT::SEG ) ) ); // touches surface
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(1,1,0), LineTypeT::SEG ) ) );
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(1,0,0), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(0.99f,0,0), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(0.99f,1,0), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(0,2,0), Vec3f(0.99f,2,0), LineTypeT::SEG ) ) );
        
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(4,0,0), Vec3f(4,4,0), LineTypeT::SEG ) ) );
        
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(2,10,0), Vec3f(2,-10,0), LineTypeT::SEG ) ) );
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(2,10,0), Vec3f(2,3,0), LineTypeT::SEG ) ) );
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(2,10,0), Vec3f(2,2.99f,0), LineTypeT::SEG ) ) );
        
        ASSERT( Cylinder::Intersects( cyl, Line3f( Vec3f(0,1.5f,0), Vec3f(2.5f,0,-1), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(3,5,1), Vec3f(3,-5,1), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(5,2,-1), Vec3f(1,-2,-1), LineTypeT::SEG ) ) );
        ASSERT( !Cylinder::Intersects( cyl, Line3f( Vec3f(2,10,0), Vec3f(2,3.01f,0), LineTypeT::SEG ) ) );
        
        
    }
    
    void Cylinder::UnitTest_ToBox3D( void ) {
        const Box3D box( -Vec3f(1,1,1), Vec3f(1,1,1));
        const Cylinder cyl_inside( Vec3f(0,0,-0.2f), Vec3f(0,0,1), 0.2f, 0.2f );
        const Cylinder cyl_intersect_face( Vec3f(0,0,0), Vec3f(0,0,1), 0.2f, 4 );
        const Cylinder cyl_intersect_face_crooked( Vec3f(1.1f,1.1f,1.1f), Vec3f(0,1,1).GetNormalized(), 0.5f, 4 );
        const Cylinder cyl_intersect_edge( Vec3f(-2,1.1f,0), Vec3f(1,0,0), 0.2f, 4 );
        const Cylinder cyl_outside_f( Vec3f(20,0,0), Vec3f(1,0,0), 0.2f, 4 );
        const Cylinder cyl_outside_a( Vec3f(0,10,0), Vec3f(0,1,0), 0.2f, 4 );
        const Cylinder cyl_outside_b( Vec3f(0,0,-15), Vec3f(0,0,1), 0.2f, 4 );
        const Cylinder cyl_outside_c( Vec3f(-20,0,0), Vec3f(1,0,0), 0.2f, 4 );
        const Cylinder cyl_outside_d( Vec3f(0,-10,0), Vec3f(0,1,0), 0.2f, 4 );
        const Cylinder cyl_outside_e( Vec3f(0,0,-15), Vec3f(0,0,1), 0.2f, 4 );
        
        ASSERT( Cylinder::Intersects( cyl_inside, box ) );
        ASSERT( Cylinder::Intersects( cyl_intersect_face, box ) );
        ASSERT( Cylinder::Intersects( cyl_intersect_face_crooked, box ) );
        ASSERT( Cylinder::Intersects( cyl_intersect_edge, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_outside_f, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_outside_a, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_outside_b, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_outside_c, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_outside_d, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_outside_e, box ) );

        const Cylinder cyl_flipped_inside( -Vec3f(0,0,-0.2f), -Vec3f(0,0,1), 0.2f, 0.2f );
        const Cylinder cyl_flipped_intersect_face( -Vec3f(0,0,0), -Vec3f(0,0,1), 0.2f, 4 );
        const Cylinder cyl_flipped_intersect_face_crooked( -Vec3f(1.1f,1.1f,1.1f), -Vec3f(0,1,1).GetNormalized(), 0.5f, 4 );
        const Cylinder cyl_flipped_intersect_edge( -Vec3f(-2,1.1f,0), -Vec3f(1,0,0), 0.2f, 4 );
        const Cylinder cyl_flipped_outside_f( -Vec3f(20,0,0), -Vec3f(1,0,0), 0.2f, 4 );
        const Cylinder cyl_flipped_outside_a( -Vec3f(0,10,0), -Vec3f(0,1,0), 0.2f, 4 );
        const Cylinder cyl_flipped_outside_b( -Vec3f(0,0,-15), -Vec3f(0,0,1), 0.2f, 4 );
        const Cylinder cyl_flipped_outside_c( -Vec3f(-20,0,0), -Vec3f(1,0,0), 0.2f, 4 );
        const Cylinder cyl_flipped_outside_d( -Vec3f(0,-10,0), -Vec3f(0,1,0), 0.2f, 4 );
        const Cylinder cyl_flipped_outside_e( -Vec3f(0,0,-15), -Vec3f(0,0,1), 0.2f, 4 );
        
        ASSERT( Cylinder::Intersects( cyl_flipped_inside, box ) );
        ASSERT( Cylinder::Intersects( cyl_flipped_intersect_face, box ) );
        ASSERT( Cylinder::Intersects( cyl_flipped_intersect_face_crooked, box ) );
        ASSERT( Cylinder::Intersects( cyl_flipped_intersect_edge, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_flipped_outside_f, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_flipped_outside_a, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_flipped_outside_b, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_flipped_outside_c, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_flipped_outside_d, box ) );
        ASSERT( ! Cylinder::Intersects( cyl_flipped_outside_e, box ) );
    }
    
    void Cylinder::UnitTest_ToAABox3D( void ) {
        const Cylinder cyl( Vec3f(0,0,-4), Vec3f(0,0,1), 4, 4 );

        const AABox3D inside( Vec3f(-1,-1,-1), Vec3f( 1,1,1 ) );
        ASSERT( Cylinder::Intersects( cyl, inside ) );

        const AABox3D outside( Vec3f(-100,-100,-100), Vec3f( 100,100,100 ) );
        ASSERT( Cylinder::Intersects( cyl, outside ) );

        const AABox3D corner( Vec3f( 2,2,-2), Vec3f( 10, 10, 2 ) );
        ASSERT( Cylinder::Intersects( cyl, corner ) );

        const AABox3D side( Vec3f( -6,-100,-2), Vec3f( -3, 100, 2 ) );
        ASSERT( Cylinder::Intersects( cyl, side ) );

        const AABox3D outside1( Vec3f(-5,-5,-5), Vec3f(-4.5f,-4.5f,-4.5f) );
        ASSERT( !Cylinder::Intersects( cyl, outside1 ) );

        const AABox3D outside2( Vec3f(5,5,5), Vec3f(45,45,45) );
        ASSERT( !Cylinder::Intersects( cyl, outside2 ) );

        const Vec3f one(1,1,1);
        const AABox3D above( Vec3f(0,0,5.01f)-one, Vec3f(0,0,5.01f)+one );
        const AABox3D below( Vec3f(0,0,-5.01f)-one, Vec3f(0,0,-5.01f)+one );
        ASSERT( !Cylinder::Intersects( cyl, above ) );
        ASSERT( !Cylinder::Intersects( cyl, below ) );
    }

#endif //CLIB_UNIT_TEST
