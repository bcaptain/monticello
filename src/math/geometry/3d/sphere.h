// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_SPHERE_H_
#define SRC_COLLISION_3D_SPHERE_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Model;
class SweepData;
class IntersectData;

class Sphere {
public:
    Sphere( void )
        : origin(), radius(0) { }

    Sphere( const Vec3f& org, const float rad )
        : origin( org ), radius( rad ) { }
        
    explicit Sphere( const Model& modelToEncompass );

public:
    void SetRadiusFromModel( const Model& model );
    
    Vec3f GetFarthestVert( const Vec3f& normalized_dir ) const;
    static Vec3f GetFarthestVert( const Vec3f& normalized_dir, const Vec3f& origin, const float radius );
    
    static bool Intersects( CollisionReport3D& report, const Sphere& sphere, const Line3f& line );
    
    static bool Intersects( const Sphere& sphere, const Vec3f& line );
    static bool Intersects( const Sphere& sphere, const Line3f& line );
    static bool Intersects( const Sphere& sphere, const Box3D& box );
    static bool Intersects( const Sphere& sphere1, const Sphere& sphere2 );
    static bool Intersects( const Sphere& sphere, const SemiCylinder& semiCyl );
    static bool Intersects( const Sphere& sphere, const Plane3f& plane );
    static bool Intersects( const Sphere& sphere, const Polygon3D& polygon );
    static bool Intersects( const Sphere& sphere, const AABox3D& aabox, Vec3f* norm_out = nullptr );
    
    static bool Intersects( Sphere sphere, Cylinder cyl ); //!< intentionally sent by value
    static bool Intersects( Sphere sphere, Capsule cap ); //!< intentionally sent by value
    
    static float DistToAABox3D( const Sphere& sphere, const AABox3D& aabox, Vec3f* norm_out = nullptr );
    static float SquaredDistToAABox3D( const Sphere& sphere, const AABox3D& aabox, Vec3f* norm_out = nullptr );
    
    static bool ToPlane( const Sphere& sphere, const Vec3f& planeA, const Vec3f& planeNormal, float* signed_dist_out = nullptr );
    static bool ToPlane( const Sphere& sphere, const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC, float* signed_dist_out = nullptr ); //! Less efficient overload to ToPlane, has to compute normal
    static bool ToPlane( const Sphere& sphere, const Plane3f& plane, float* signed_dist_out = nullptr );
    
    static bool ToPoly( const Sphere& sphere, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& polyNormal );
    static bool ToPoly( const Sphere& sphere, const Vec3f* clockwise_verts, const std::size_t vert_count );

    static bool BeforePlane( const Sphere& sphere, const Plane3f& plane );
    static bool BeforePlane( const Sphere& sphere, const Vec3f &A, const Vec3f& B, const Vec3f& C );
    static bool BeforePlane( const Sphere& sphere, const Vec3f& point_on_plane, const Vec3f& plane_normal );

    static bool BehindPlane( const Sphere& sphere, const Vec3f &A, const Vec3f& B, const Vec3f& C );
    static bool BehindPlane( const Sphere& sphere, const Plane3f& plane );
    static bool BehindPlane( const Sphere& sphere, const Vec3f& point_on_plane, const Vec3f& plane_normal );

    static bool ToModel( const Sphere& sphere, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );

    /*! For seep functions, the distance upon non-collision is not calculated since it doesn't seem useful \n\n

    Note: SweepToPlane only works when sweeping from the FRONT of the plane to the BACK, and only if the first sphere_origin does not already collide with the plane
    if there is a collision, report.dist will be the time from origin to destination where the first collision occurs \n\n

    sweepint spheres to planes, will not collide against back-facing planes \n
    **/
    
    static bool SweepToPlane( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& plane_vert1, const Vec3f& plane_vert2, const Vec3f& plane_vert3 );
    static bool SweepToPlane( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& point_on_plane, const Vec3f& plane_normal ); //!< Less efficient overlaod to SweepToPlane, has to compute normal.

    static bool SweepToModel( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );

    static bool SweepToPoly( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f* clockwise_verts, const std::size_t numVerts ); //!< less efficient overload to SweepToPoly, must compute normal
    static bool SweepToPoly( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f* clockwise_verts, const std::size_t numVerts, const Vec3f& poly_normal );

    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const AABox3D& aabox );
    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Box3D& box );
    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& check_point );
    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Sphere& obstacle_sphere );
    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Line3f& line );
    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Cylinder& cyl ); 
    static bool SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Capsule& cyl ); 
    static bool SweepTo( const Sphere& sphere, const Vec3f& sphere_dest, const AABox3D& aabox );
    static bool SweepTo( const Sphere& sphere, const Vec3f& sphere_dest, const Box3D& box );
    
    static bool SweepToBox3D( const SweepData& ssc, const Box3D& box ); //!< Overload is same function, but creates its own CollisionReport3D with no calcs

    static bool SweepToLine( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end );
    static bool SweepToLineSeg( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& line_start, const Vec3f& line_end );
    static bool SweepToRay( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& line_start, const Vec3f& line_end );

    static bool GetSATCollision_Helper( const Sphere& sphere, const Vec3f* verts, const std::size_t num_verts, const Vec3f& axis ); //!< Not intended for general consumption!
    
    static void UnitTest( void );
    static void UnitTest_ToSphere( void );
    static void UnitTest_ToCylinder( void );
    static void UnitTest_ToCapsule( void );
    static void UnitTest_ToBox( void );
    static void UnitTest_ToPlane( void );
    static void UnitTest_ToAABox3D( void );
    static void UnitTest_SweepToPlane( void );
    static void UnitTest_ToPoly( void );
    static void UnitTest_SweepToPoly( void );
    static void UnitTest_SweepToBox3D( void );
    static void UnitTest_SweepToPoint( void );
    static void UnitTest_SweepToSphere( void );
    static void UnitTest_SweepToLine( void );
    static void UnitTest_SweepToLineSeg( void );
    static void UnitTest_SweepToRay( void );

public:
    Vec3f origin;
    float radius;
};

#endif // SRC_COLLISION_3D_SPHERE_H_
