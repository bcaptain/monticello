// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./ellipse.h"

void Ellipse::SurfacePoint( const Vec2f& radiusVector, const Vec2f& unitVector, Vec2f& out ) {
    out = radiusVector;
    out *= unitVector;
}

// todo: check
float Ellipsoid::GetArea( const Vec3f& radiusVector ) {
    return ( radiusVector.x / 2 ) * ( radiusVector.y / 2 ) * ( radiusVector.z / 2 ) * PI_F;
}

inline Vec3f Ellipsoid::SurfacePoint( const Vec3f& radiusVector, const Vec3f& unitVector ) {
    return radiusVector * unitVector;
}
