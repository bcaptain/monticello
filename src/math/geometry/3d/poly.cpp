// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./poly.h"
#include "../../collision/collision.h"

float Polygon3D::GetClosestVert( const Vec3f& to, const Vec3f* unsorted_verts, const std::size_t num_verts, Vec3f& out ) {
    std::size_t index;
    float dist = GetClosestVertIndex( to, unsorted_verts, num_verts, index );
    out = unsorted_verts[index];
    return dist;
}
    
float Polygon3D::GetClosestVertIndex( const Vec3f& to, const Vec3f* unsorted_verts, const std::size_t num_verts, std::size_t& out ) {
    ASSERT( num_verts > 0 );
    ASSERT( unsorted_verts );

    float closest = Vec3f::SquaredLen( to - unsorted_verts[0] );
    out = 0;
    
    // ** Moving One Vert?
    for ( std::size_t v=1; v<num_verts; ++v ) {
        float dist = Vec3f::SquaredLen( to - unsorted_verts[v] );
        if ( dist < closest ) {
            closest = dist;
            out = v;
        }
    }
    
    return sqrtf( closest );
}

Sphere Polygon3D::GetCircumsphere( const Vec3f* unsorted_vert_list, const std::size_t num_verts ) {
    if ( num_verts < 1 )
        return {};

    const Vec3f center = Polygon3D::GetCenter( unsorted_vert_list, num_verts );
    float radius_sq = Vec3f::SquaredLen(unsorted_vert_list[0] - center);

    for ( std::size_t v=1; v<num_verts; ++v ) {
        float len_sq = Vec3f::SquaredLen(unsorted_vert_list[v] - center);
        if ( len_sq > radius_sq ) {
            radius_sq = len_sq;
        }
    }

    // add just at tiny bit more just so we don't cause a problem with the corners
    return Sphere{ center, sqrtf( radius_sq ) + EP };
}

std::size_t Polygon3D::GetFarthestVertIndex( const Vec3f* unsorted_vert_list, const std::size_t num_verts, const Vec3f& dir ) {
    ASSERT( unsorted_vert_list && num_verts > 0 );

    // start off with first vert
    std::size_t farthest = 0;
    float dist( unsorted_vert_list[0].Dot(dir) );

    for ( std::size_t i=1; i<num_verts; ++i ) {
        float tmp( unsorted_vert_list[i].Dot( dir ) );
        if ( tmp > dist ) {
            dist = tmp;
            farthest = i;
        }
    }

    return farthest;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Polygon3D::Intersects( const Polygon3D& polygon, const Box3D& box ) {
        ERR("Not Implemented: Polygon3D to Box3D.\n"); //toimplement
        return false;
    }

    bool Polygon3D::Intersects( const Polygon3D& polygon, const Line3f& line ) {
        ERR("Not Implemented: Polygon3D to Line3f.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Polygon3D::Intersects( const Polygon3D& polygon, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, polygon );
}

bool Polygon3D::Intersects( const Polygon3D& polygon, const Capsule& cap ) {
    return Capsule::Intersects( cap, polygon );
}

bool Polygon3D::Intersects( const Polygon3D& polygon, const AABox3D& aabox ) {
    return AABox3D::Intersects( aabox, polygon );
}

bool Polygon3D::Intersects( const Polygon3D& polygon, const Sphere& sphere ) {
    return Sphere::ToPoly( sphere, polygon.verts.data(), polygon.verts.size(), Plane3f::GetNormal( polygon.verts.data() ) );
}

bool Polygon3D::Intersects( const Polygon3D& polygon, const Vec3f& point, const float epsilon ) {
    return Collision3D::TestPoint::ToPoly( point, polygon.verts.data(), polygon.verts.size(), Plane3f::GetNormal( polygon.verts.data() ), epsilon );
}

namespace Polyhedron {
    std::size_t GetFarthestVertIndex( const Vec3f* unsorted_vert_list, const std::size_t num_verts, const Vec3f& dir ) {
        return Polygon3D::GetFarthestVertIndex( unsorted_vert_list, num_verts, dir );
    }
}

namespace VertCloud {
    std::size_t GetFarthestVertIndex( const Vec3f* unsorted_vert_list, const std::size_t num_verts, const Vec3f& dir ) {
        return Polygon3D::GetFarthestVertIndex( unsorted_vert_list, num_verts, dir );
    }
}

Vec3f Polygon3D::GetCenter( const Vec3f* unsorted_vert_list, const std::size_t num_verts ) {
    ASSERT( num_verts > 2 );
    return GetAverage(unsorted_vert_list, num_verts);
}
