// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ELLIPSE_H_
#define SRC_CLIB_ELLIPSE_H_

#include "./clib/src/warnings.h"
#include "../../../base/main.h"
#include "../../geometry.h"

class Ellipse {
public:
    static void SurfacePoint( const Vec2f& radiusVector, const Vec2f& unitVector, Vec2f& out ); //!< get the point on the ellipse's surface given a unit vector
    static float GetArea( const Vec2f& radiusVector );
};

class Ellipsoid {
public:
    static Vec3f SurfacePoint( const Vec3f& radiusVector, const Vec3f& unitVector ); //!< get the point on the ellipse's surface given a unit vector
    static float GetArea( const Vec3f& radiusVector );
};

#endif  //__CLIB_ELLIPSE
