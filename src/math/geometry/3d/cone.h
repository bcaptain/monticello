// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_CONE_H_
#define SRC_COLLISION_3D_CONE_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Cone3D {
public:
    Cone3D( const Vec3f& _origin, const Vec3f& _normal, const float _length, const float _radius );
    Cone3D( void );
    ~Cone3D( void ) = default;
    Cone3D( const Cone3D& other ) = default;

public:
    Cone3D& operator=( const Cone3D& other ) = default;

public:
    Vec3f GetFarthestVert( const Vec3f& normalized_dir ) const;
    Vec3f GetTip( void ) const;

#ifdef CLIB_UNIT_TEST
    static void UnitTest( void );
#endif //CLIB_UNIT_TEST
    
    float GetAngle( void ) const;
    #ifdef CLIB_UNIT_TEST
        static void UnitTest_GetAngle( void );
    #endif //CLIB_UNIT_TEST
//        static bool ToPoly( const Cone3D& cone, const AABox3D& aabox );
//        static bool ToAABox3D( const Cone3D& cone, const AABox3D& aabox );
//        static bool ToBox3D( const Cone3D& cone, const AABox3D& aabox );
        static bool Intersects( const Cone3D& cone, const Plane3f& plane );
        static bool ToPlane( const Cone3D& cone, const Vec3f& planeA, const Vec3f& plane_normal );

public:
    Vec3f origin;
    Vec3f normal;
    float length;
    float radius;
};

#endif // SRC_COLLISION_3D_CONE_H_
