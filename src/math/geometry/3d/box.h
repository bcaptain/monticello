// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_BOX_H_
#define SRC_CLIB_BOX_H_

#include <cstring>
#include <mutex>
#include "./clib/src/warnings.h"
#include "../../../base/main.h"
#include "../../geometry.h"

extern const std::size_t BOX_FAR_LEFT_TOP_INDEX;
extern const std::size_t BOX_FAR_RIGHT_TOP_INDEX;
extern const std::size_t BOX_NEAR_LEFT_TOP_INDEX;
extern const std::size_t BOX_NEAR_RIGHT_TOP_INDEX;
extern const std::size_t BOX_NEAR_LEFT_BOTTOM_INDEX;
extern const std::size_t BOX_NEAR_RIGHT_BOTTOM_INDEX;
extern const std::size_t BOX_FAR_LEFT_BOTTOM_INDEX;
extern const std::size_t BOX_FAR_RIGHT_BOTTOM_INDEX;

class Model;
class Sphere;
class AABox3D;
class CollisionReport3D;

typedef Uint8 ModelCollisionT_BaseType;
enum class ModelCollisionT : ModelCollisionT_BaseType;

Box3D operator*=( Box3D& box, const Mat3f& mat );
Box3D operator*( const Box3D& box, const Mat3f& mat );

Box3D& operator*=( Box3D& box, const Mat4f& mat );
Box3D operator*( const Box3D& box, const Mat4f& mat );

Box3D operator*( const Box3D& box, const Quat& quat );
Box3D& operator*=( Box3D& box, const Quat& quat );

// ** clockwise from the outside
#define BOX_BOTTOM_PLANE_VERTS(thebox) \
    (thebox).VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX), (thebox).VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX), \
    (thebox).VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX)

#define BOX_TOP_PLANE_VERTS(thebox) \
    (thebox).VertAt(BOX_FAR_LEFT_TOP_INDEX), (thebox).VertAt(BOX_FAR_RIGHT_TOP_INDEX), \
    (thebox).VertAt(BOX_NEAR_RIGHT_TOP_INDEX)

#define BOX_RIGHT_PLANE_VERTS(thebox) \
    (thebox).VertAt(BOX_NEAR_RIGHT_TOP_INDEX), (thebox).VertAt(BOX_FAR_RIGHT_TOP_INDEX), \
    (thebox).VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX)

#define BOX_LEFT_PLANE_VERTS(thebox) \
    (thebox).VertAt(BOX_FAR_LEFT_TOP_INDEX), (thebox).VertAt(BOX_NEAR_LEFT_TOP_INDEX), \
    (thebox).VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX)

#define BOX_NEAR_PLANE_VERTS(thebox) \
    (thebox).VertAt(BOX_NEAR_LEFT_TOP_INDEX), (thebox).VertAt(BOX_NEAR_RIGHT_TOP_INDEX), \
    (thebox).VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX)

#define BOX_FAR_PLANE_VERTS(thebox) \
    (thebox).VertAt(BOX_FAR_RIGHT_TOP_INDEX), (thebox).VertAt(BOX_FAR_LEFT_TOP_INDEX), \
    (thebox).VertAt(BOX_FAR_LEFT_BOTTOM_INDEX)



// ** clockwise from the outside
#define BOX_BOTTOM_FACE_VERTS(thebox) \
    (thebox).VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX), (thebox).VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX), \
    (thebox).VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX), (thebox).VertAt(BOX_FAR_LEFT_BOTTOM_INDEX)

#define BOX_TOP_FACE_VERTS(thebox) \
    (thebox).VertAt(BOX_FAR_LEFT_TOP_INDEX), (thebox).VertAt(BOX_FAR_RIGHT_TOP_INDEX), \
    (thebox).VertAt(BOX_NEAR_RIGHT_TOP_INDEX), (thebox).VertAt(BOX_NEAR_LEFT_TOP_INDEX)

#define BOX_RIGHT_FACE_VERTS(thebox) \
    (thebox).VertAt(BOX_NEAR_RIGHT_TOP_INDEX), (thebox).VertAt(BOX_FAR_RIGHT_TOP_INDEX), \
    (thebox).VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX), (thebox).VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX)

#define BOX_LEFT_FACE_VERTS(thebox) \
    (thebox).VertAt(BOX_FAR_LEFT_TOP_INDEX), (thebox).VertAt(BOX_NEAR_LEFT_TOP_INDEX), \
    (thebox).VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX), (thebox).VertAt(BOX_FAR_LEFT_BOTTOM_INDEX)

#define BOX_NEAR_FACE_VERTS(thebox) \
    (thebox).VertAt(BOX_NEAR_LEFT_TOP_INDEX), (thebox).VertAt(BOX_NEAR_RIGHT_TOP_INDEX), \
    (thebox).VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX), (thebox).VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX)

#define BOX_FAR_FACE_VERTS(thebox) \
    (thebox).VertAt(BOX_FAR_RIGHT_TOP_INDEX), (thebox).VertAt(BOX_FAR_LEFT_TOP_INDEX), \
    (thebox).VertAt(BOX_FAR_LEFT_BOTTOM_INDEX), (thebox).VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX)

#include "./aabox.h"

extern const std::size_t BOX_FAR_LEFT_TOP_INDEX;
extern const std::size_t BOX_FAR_RIGHT_TOP_INDEX;
extern const std::size_t BOX_NEAR_LEFT_TOP_INDEX;
extern const std::size_t BOX_NEAR_RIGHT_TOP_INDEX;
extern const std::size_t BOX_NEAR_LEFT_BOTTOM_INDEX;
extern const std::size_t BOX_NEAR_RIGHT_BOTTOM_INDEX;
extern const std::size_t BOX_FAR_LEFT_BOTTOM_INDEX;
extern const std::size_t BOX_FAR_RIGHT_BOTTOM_INDEX;

extern const std::size_t BOX_RIGHT_FACE_INDEX;
extern const std::size_t BOX_LEFT_FACE_INDEX;
extern const std::size_t BOX_TOP_FACE_INDEX;
extern const std::size_t BOX_BOTTOM_FACE_INDEX;
extern const std::size_t BOX_NEAR_FACE_INDEX;
extern const std::size_t BOX_FAR_FACE_INDEX;

typedef Uint16 BoxNormCalculationsT_BaseType;
enum BoxNormCalculationsT : BoxNormCalculationsT_BaseType {
    NO_NORMS_COMPUTED = 0
    , RIGHT_NORM_COMPUTED = 1
    , LEFT_NORM_COMPUTED = 1<<1
    , TOP_NORM_COMPUTED = 1<<2
    , BOTTOM_NORM_COMPUTED = 1<<3
    , NEAR_NORM_COMPUTED = 1<<4
    , FAR_NORM_COMPUTED = 1<<5

    , ALL_NORMS_COMPUTED = (
        RIGHT_NORM_COMPUTED |
        LEFT_NORM_COMPUTED |
        TOP_NORM_COMPUTED |
        BOTTOM_NORM_COMPUTED |
        NEAR_NORM_COMPUTED |
        FAR_NORM_COMPUTED
    )
};

/*!

Box3D \n\n

 In World Space (Left Handed, Z is up):
  *     Near Face = +Z, Far Face = -Z
  *     Right Face = +X, Left Face = -X
  *     Top Face = +Y, Bottom Face = -Y

**/
   

class Box3D {
public:
    Box3D( void );
    ~Box3D( void );
    explicit Box3D( const AABox3D& aabb );
    Box3D( const Vec3f& mins, const Vec3f& maxs );
    Box3D( const Box3D& other );
    Box3D( const float side_lengths );
    Box3D( Box3D&& other_rref );
    Box3D( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z );
    Box3D( const Vec3f (&verts)[8] );
    Box3D( const Vec3f& tlf, const Vec3f& trf, const Vec3f& tln, const Vec3f& trn, const Vec3f& bln, const Vec3f& brn, const Vec3f& blf, const Vec3f& brf );

public:
    Box3D& operator=( const Box3D& other );
    Box3D& operator=( Box3D&& other_rref );

    bool operator==( const Box3D& other ) const;
    bool operator!=( const Box3D& other ) const;

    Box3D& operator+=( const Vec3f& val );
    Box3D& operator-=( const Vec3f& val );
    Box3D& operator*=( const float val );
    Box3D& operator/=( const float val );

    Box3D operator+( const Vec3f& val ) const;
    Box3D operator-( const Vec3f& val ) const;
    Box3D operator*( const float val ) const;
    Box3D operator/( const float val ) const;

public:
    void Set( const AABox3D& aabb );
    void SetBounds( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z );
    void SetBounds( const Vec3f& min_x, const Vec3f& max_x );
    void Set( const Vec3f (&verts)[8] );
        
    void Translate( const Vec3f& vec );

    Vec3f GetCenter( void ) const;
    float GetOrthogonalHeight( void ) const;
    float GetOrthogonalWidth( void ) const;
    float GetOrthogonalDepth( void ) const;

    Sphere GetCircumsphere( void ) const;
    Sphere GetInsphereOfCube( void ) const; //! returns the origin and radius of a sphere that the box entirely encapsulates
    
    bool IsParalellepiped( void ) const;
    
    float GetLongestEdgeLenSQ( void ) const;

    Vec3f GetVert_FarLeftTop( void ) const { return vert[BOX_FAR_LEFT_TOP_INDEX]; }
    Vec3f GetVert_FarRightTop( void ) const { return vert[BOX_FAR_RIGHT_TOP_INDEX]; }
    Vec3f GetVert_NearLeftTop( void ) const { return vert[BOX_NEAR_LEFT_TOP_INDEX]; }
    Vec3f GetVert_NearRightTop( void ) const { return vert[BOX_NEAR_RIGHT_TOP_INDEX]; }
    Vec3f GetVert_NearLeftBottom( void ) const { return vert[BOX_NEAR_LEFT_BOTTOM_INDEX]; }
    Vec3f GetVert_NearRightBottom( void ) const { return vert[BOX_NEAR_RIGHT_BOTTOM_INDEX]; }
    Vec3f GetVert_FarLeftBottom( void ) const { return vert[BOX_FAR_LEFT_BOTTOM_INDEX]; }
    Vec3f GetVert_FarRightBottom( void ) const { return vert[BOX_FAR_RIGHT_BOTTOM_INDEX]; }

    Vec3f GetLeftNormal( void ) const;
    Vec3f GetRightNormal( void ) const;
    Vec3f GetTopNormal( void ) const;
    Vec3f GetBottomNormal( void ) const;
    Vec3f GetNearNormal( void ) const;
    Vec3f GetFarNormal( void ) const;

    Vec3f GetFaceNormal( const std::size_t face_index ) const;
    
    Vec3f GetFaceVert( const std::size_t face_index ) const;
    
    Vec3f GetFarthestVert( const Vec3f& dir ) const;
    
    const Vec3f& VertAt( const std::size_t vert_index ) const;
    
    std::vector<Vec3f> GetFaceVerts( const uint face_index ) const;
    const Vec3f* GetVertsPtr( void ) const { return vert; }

    static bool Intersects( CollisionReport3D& report, const Box3D& box, const Line3f& line );
    
    static bool Intersects( const Box3D& box1, const Box3D& box2 );
    static bool Intersects( const Box3D& box, const AABox3D& aabox );
    static bool Intersects( const Box3D& box, const Sphere& sphere );
    static bool Intersects( const Box3D& box, const Line3f& line );
    static bool Intersects( const Box3D& box, const Vec3f& point );
    static bool Intersects( const Box3D& box, const Cylinder& cyl );
    static bool Intersects( const Box3D& box, const Capsule& cyl );

    static bool ToModel( const Box3D& box, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );
    static bool SweepToModel( CollisionReport3D& report, const Box3D& box, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );

    struct SAT {
        static bool ToBox3D_Faces( const Box3D& box1, const Box3D& box2 ); //!< Not intended for general consumption!
        static bool ToBox3D_EdgeCrossProducts( const Box3D& box1, const Box3D& box2 ); //!< Not intended for general consumption!
        static bool ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2( const Vec3f& box1_norm, const Box3D& box1, const Box3D& box2 ); //!< not for general consumption
    };

#ifdef CLIB_UNIT_TEST
    static void UnitTest( void );
    static void UnitTest_ToBox3D_Paralelepiped( void );
    static void UnitTest_ToBox3D_NonParalelepiped( void );
    static void UnitTest_IsParalellepiped( void );
    static void UnitTest_FarthestVert( void );
#endif //CLIB_UNIT_TEST

private:
    void ComputeLeftNormal( void ) const;
    void ComputeRightNormal( void ) const;
    void ComputeTopNormal( void ) const;
    void ComputeBottomNormal( void ) const;
    void ComputeNearNormal( void ) const;
    void ComputeFarNormal( void ) const;

    void ClearAllNormals( void );

    void SetNormalsToAABox3D( void );

private:
    Vec3f *vert; //!< the 8 box verts

    mutable std::mutex norms_mutex;
    mutable Vec3f *norms; //!< lazy-evaluated face normals
    mutable BoxNormCalculationsT_BaseType norms_computed;
};

#endif // SRC_CLIB_BOX_H_
