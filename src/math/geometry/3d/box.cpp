// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include <future>
#include "./box.h"
#include "../../collision/gjk.h"

const std::size_t BOX_FAR_LEFT_TOP_INDEX=0;
const std::size_t BOX_FAR_RIGHT_TOP_INDEX=1;
const std::size_t BOX_NEAR_LEFT_TOP_INDEX=2;
const std::size_t BOX_NEAR_RIGHT_TOP_INDEX=3;
const std::size_t BOX_NEAR_LEFT_BOTTOM_INDEX=4;
const std::size_t BOX_NEAR_RIGHT_BOTTOM_INDEX=5;
const std::size_t BOX_FAR_LEFT_BOTTOM_INDEX=6;
const std::size_t BOX_FAR_RIGHT_BOTTOM_INDEX=7;

const std::size_t BOX_RIGHT_FACE_INDEX=0;
const std::size_t BOX_LEFT_FACE_INDEX=1;
const std::size_t BOX_TOP_FACE_INDEX=2;
const std::size_t BOX_BOTTOM_FACE_INDEX=3;
const std::size_t BOX_NEAR_FACE_INDEX=4;
const std::size_t BOX_FAR_FACE_INDEX=5;

bool Box3D::Intersects( const Box3D& box, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, box );
}

bool Box3D::Intersects( const Box3D& box, const Capsule& cap ) {
    return Capsule::Intersects( cap, box );
}

bool Box3D::Intersects( const Box3D& box, const Sphere& sphere ) {
    return Sphere::Intersects( sphere, box );
}

bool Box3D::Intersects( const Box3D& box, const Line3f& line ) {
    return Line3f::Intersects( line, box );
}

bool Box3D::Intersects( const Box3D& box, const Vec3f& point ) {
    return Collision3D::TestPoint::Intersects( point, box );
}

bool Box3D::Intersects( const Box3D& box, const AABox3D& aabox ) {
    const AABox3D aabox2(
        Min8f( box.VertAt(0).x,box.VertAt(1).x,box.VertAt(2).x,box.VertAt(3).x,box.VertAt(4).x,box.VertAt(5).x,box.VertAt(6).x,box.VertAt(7).x ),
        Max8f( box.VertAt(0).x,box.VertAt(1).x,box.VertAt(2).x,box.VertAt(3).x,box.VertAt(4).x,box.VertAt(5).x,box.VertAt(6).x,box.VertAt(7).x ),
        Min8f( box.VertAt(0).y,box.VertAt(1).y,box.VertAt(2).y,box.VertAt(3).y,box.VertAt(4).y,box.VertAt(5).y,box.VertAt(6).y,box.VertAt(7).y ),
        Max8f( box.VertAt(0).y,box.VertAt(1).y,box.VertAt(2).y,box.VertAt(3).y,box.VertAt(4).y,box.VertAt(5).y,box.VertAt(6).y,box.VertAt(7).y ),
        Min8f( box.VertAt(0).z,box.VertAt(1).z,box.VertAt(2).z,box.VertAt(3).z,box.VertAt(4).z,box.VertAt(5).z,box.VertAt(6).z,box.VertAt(7).z ),
        Max8f( box.VertAt(0).z,box.VertAt(1).z,box.VertAt(2).z,box.VertAt(3).z,box.VertAt(4).z,box.VertAt(5).z,box.VertAt(6).z,box.VertAt(7).z )
    );

    if ( !AABox3D::Intersects( aabox2, aabox ) )
        return false;

    return Collision3D::TestGJK::Intersects( box, Box3D( aabox ) );
}

bool Box3D::Intersects( const Box3D& box1, const Box3D& box2 ) {
    // ** early-out using circumspheres. SAT doesn't check for encapsulation, so this will also cover for that
    if ( !Sphere::Intersects( box1.GetCircumsphere(), box2.GetCircumsphere() ) )
        return false;

    return Collision3D::TestGJK::Intersects( box1, box2 );

/** Separating Axis Theorem

    if ( !SAT::ToBox3D_Faces( box1, box1_type, box2 ) )
        return false;

    if ( !SAT::ToBox3D_Faces( box2, box2_type, box1 ) )
        return false;

    return SAT::ToBox3D_EdgeCrossProducts( box1, box1_type, box2, box2_type );
*/
}

Box3D operator*(const Box3D& box, const Quat& quat) {
    return {
        box.VertAt(0) * quat
        , box.VertAt(1) * quat
        , box.VertAt(2) * quat
        , box.VertAt(3) * quat
        , box.VertAt(4) * quat
        , box.VertAt(5) * quat
        , box.VertAt(6) * quat
        , box.VertAt(7) * quat
    };
}

Box3D& operator*=(Box3D& box, const Quat& quat) {
    return box = box * quat;
}

Box3D operator*=( Box3D& box, const Mat3f& mat ) {
    return box = box * mat;
}

Box3D operator*( const Box3D& box, const Mat3f& mat ) {
    return {
        box.VertAt(0) * mat
        , box.VertAt(1) * mat
        , box.VertAt(2) * mat
        , box.VertAt(3) * mat
        , box.VertAt(4) * mat
        , box.VertAt(5) * mat
        , box.VertAt(6) * mat
        , box.VertAt(7) * mat
    };
}

Box3D& operator*=( Box3D& box, const Mat4f& mat ) {
    return box = box * mat;
}

Box3D operator*( const Box3D& box, const Mat4f& mat ){
    return {
        box.VertAt(0) * mat
        , box.VertAt(1) * mat
        , box.VertAt(2) * mat
        , box.VertAt(3) * mat
        , box.VertAt(4) * mat
        , box.VertAt(5) * mat
        , box.VertAt(6) * mat
        , box.VertAt(7) * mat
    };
}

Box3D::Box3D( void )
    : vert( new Vec3f[8] )
    , norms_mutex()
    , norms( nullptr )
    , norms_computed( NO_NORMS_COMPUTED )
{ }

Box3D::Box3D( const Box3D& other )
    : vert(
        new Vec3f[8]{
            other.vert[0] // BOX_FAR_LEFT_TOP_INDEX
            , other.vert[1] // BOX_FAR_RIGHT_TOP_INDEX
            , other.vert[2] // BOX_NEAR_LEFT_TOP_INDEX
            , other.vert[3] // BOX_NEAR_RIGHT_TOP_INDEX
            , other.vert[4] // BOX_NEAR_LEFT_BOTTOM_INDEX
            , other.vert[5] // BOX_NEAR_RIGHT_BOTTOM_INDEX
            , other.vert[6] // BOX_FAR_LEFT_BOTTOM_INDEX
            , other.vert[7]  // BOX_FAR_RIGHT_BOTTOM_INDEX
        }
      )
    , norms_mutex()
    , norms( nullptr )
    , norms_computed( other.norms_computed )
{
    if ( other.norms ) {
        std::lock_guard< std::mutex > other_norms_mutex_lock( other.norms_mutex );

        norms = new Vec3f[6];
        for ( uint i=0; i<6; ++i ) {
            norms[i] = other.norms[i];
        }
    }
}

Box3D::Box3D( Box3D&& other_rref )
    : vert( other_rref.vert )
    , norms_mutex()
    , norms( other_rref.norms )
    , norms_computed( other_rref.norms_computed )
{
    other_rref.vert = nullptr;
    other_rref.norms = nullptr;
    other_rref.norms_computed = NO_NORMS_COMPUTED;
}

Box3D::Box3D( const float side_lengths )
    : Box3D( Vec3f(side_lengths/2, side_lengths/2, side_lengths/2), -Vec3f(side_lengths/2, side_lengths/2, side_lengths/2) )
{ }

Box3D::Box3D( const Vec3f& mins, const Vec3f& maxs )
    : vert(
        new Vec3f[8]{
            Vec3f( mins.x, maxs.y, mins.z )    // BOX_FAR_LEFT_TOP_INDEX
            , Vec3f( maxs.x, maxs.y, mins.z )  // BOX_FAR_RIGHT_TOP_INDEX
            , Vec3f( mins.x, maxs.y, maxs.z )  // BOX_NEAR_LEFT_TOP_INDEX
            , Vec3f( maxs )                    // BOX_NEAR_RIGHT_TOP_INDEX
            , Vec3f( mins.x, mins.y, maxs.z )  // BOX_NEAR_LEFT_BOTTOM_INDEX
            , Vec3f( maxs.x, mins.y, maxs.z )  // BOX_NEAR_RIGHT_BOTTOM_INDEX
            , Vec3f( mins )                    // BOX_FAR_LEFT_BOTTOM_INDEX
            , Vec3f( maxs.x, mins.y, mins.z )  // BOX_FAR_RIGHT_BOTTOM_INDEX
        }
    )
    , norms_mutex()
    , norms(
        new Vec3f[6]{
            AABOX_NORM_RIGHT
            , AABOX_NORM_LEFT
            , AABOX_NORM_TOP
            , AABOX_NORM_BOTTOM
            , AABOX_NORM_NEAR
            , AABOX_NORM_FAR
        }
      )
    , norms_computed( ALL_NORMS_COMPUTED )
{ }

Box3D::Box3D( const AABox3D& aabb )
    : vert(
        new Vec3f[8]{
            aabb.GetVert_FarLeftTop()
            , aabb.GetVert_FarRightTop()
            , aabb.GetVert_NearLeftTop()
            , aabb.GetVert_NearRightTop()
            , aabb.GetVert_NearLeftBottom()
            , aabb.GetVert_NearRightBottom()
            , aabb.GetVert_FarLeftBottom()
            , aabb.GetVert_FarRightBottom()
        }
    )
    , norms_mutex()
    , norms(
        new Vec3f[6]{
            AABOX_NORM_RIGHT
            , AABOX_NORM_LEFT
            , AABOX_NORM_TOP
            , AABOX_NORM_BOTTOM
            , AABOX_NORM_NEAR
            , AABOX_NORM_FAR
        }
      )
    , norms_computed( ALL_NORMS_COMPUTED )
{ }

Box3D::Box3D( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z )
    : vert( new Vec3f[8]{
            Vec3f( min_x, max_y, min_z )    // BOX_FAR_LEFT_TOP_INDEX
            , Vec3f( max_x, max_y, min_z )  // BOX_FAR_RIGHT_TOP_INDEX
            , Vec3f( min_x, max_y, max_z )  // BOX_NEAR_LEFT_TOP_INDEX
            , Vec3f( max_x, max_y, max_z )  // BOX_NEAR_RIGHT_TOP_INDEX
            , Vec3f( min_x, min_y, max_z )  // BOX_NEAR_LEFT_BOTTOM_INDEX
            , Vec3f( max_x, min_y, max_z )  // BOX_NEAR_RIGHT_BOTTOM_INDEX
            , Vec3f( min_x, min_y, min_z )  // BOX_FAR_LEFT_BOTTOM_INDEX
            , Vec3f( max_x, min_y, min_z )  // BOX_FAR_RIGHT_BOTTOM_INDEX
        }
    )
    , norms_mutex()
    , norms(
        new Vec3f[6]{
            AABOX_NORM_RIGHT
            , AABOX_NORM_LEFT
            , AABOX_NORM_TOP
            , AABOX_NORM_BOTTOM
            , AABOX_NORM_NEAR
            , AABOX_NORM_FAR
        }
      )
    , norms_computed( ALL_NORMS_COMPUTED )
{ }

Box3D::Box3D( const Vec3f (&verts)[8] )
    : vert( new Vec3f[8]{verts[0], verts[1], verts[2], verts[3], verts[4], verts[5], verts[6], verts[7]} )
    , norms_mutex()
    , norms( nullptr )
    , norms_computed( NO_NORMS_COMPUTED )
{ }

Box3D::Box3D( const Vec3f& tlf, const Vec3f& trf, const Vec3f& tln, const Vec3f& trn, const Vec3f& bln, const Vec3f& brn, const Vec3f& blf, const Vec3f& brf )
    : vert( new Vec3f[8]{
           Vec3f(tlf), Vec3f(trf), Vec3f(tln), Vec3f(trn), Vec3f(bln), Vec3f(brn), Vec3f(blf), Vec3f(brf),
      })
    , norms_mutex()
    , norms( nullptr )
    , norms_computed( NO_NORMS_COMPUTED )
{ }

Box3D::~Box3D( void ) {
    delete[] vert;

    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    norms_computed = NO_NORMS_COMPUTED;
    delete[] norms;
}


void Box3D::Set( const Vec3f (&verts)[8] ) {
    for ( uint i=0; i<8; ++i )
        vert[i] = verts[i];
    ClearAllNormals();
    
}

void Box3D::SetBounds( const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z ) {
    vert[BOX_FAR_LEFT_TOP_INDEX].Set( min_x, max_y, min_z );
    vert[BOX_FAR_RIGHT_TOP_INDEX].Set( max_x, max_y, min_z );
    vert[BOX_NEAR_LEFT_TOP_INDEX].Set( min_x, max_y, max_z );
    vert[BOX_NEAR_RIGHT_TOP_INDEX].Set( max_x, max_y, max_z );
    vert[BOX_NEAR_LEFT_BOTTOM_INDEX].Set( min_x, min_y, max_z );
    vert[BOX_NEAR_RIGHT_BOTTOM_INDEX].Set( max_x, min_y, max_z );
    vert[BOX_FAR_LEFT_BOTTOM_INDEX].Set( min_x, min_y, min_z );
    vert[BOX_FAR_RIGHT_BOTTOM_INDEX].Set( max_x, min_y, min_z );

    SetNormalsToAABox3D();
}

void Box3D::SetNormalsToAABox3D( void ) {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    if ( ! norms )
        norms = new Vec3f[6]; 

    norms[BOX_RIGHT_FACE_INDEX] = AABOX_NORM_RIGHT;
    norms[BOX_LEFT_FACE_INDEX] = AABOX_NORM_LEFT;
    norms[BOX_TOP_FACE_INDEX] = AABOX_NORM_TOP;
    norms[BOX_BOTTOM_FACE_INDEX] = AABOX_NORM_BOTTOM;
    norms[BOX_NEAR_FACE_INDEX] = AABOX_NORM_NEAR;
    norms[BOX_FAR_FACE_INDEX] = AABOX_NORM_FAR;
    norms_computed = ALL_NORMS_COMPUTED;
}

Vec3f Box3D::GetFaceVert( const std::size_t face_index ) const {
    ASSERT( face_index < 6 );
    switch ( face_index ) {
        case BOX_NEAR_FACE_INDEX: FALLTHROUGH;
        case BOX_BOTTOM_FACE_INDEX: FALLTHROUGH;
        case BOX_RIGHT_FACE_INDEX: return GetVert_NearRightBottom();
        case BOX_LEFT_FACE_INDEX: FALLTHROUGH;
        case BOX_FAR_FACE_INDEX: FALLTHROUGH;
        case BOX_TOP_FACE_INDEX: return GetVert_FarLeftTop();
        default:
            DIE("Box3D::GetFaceVert(): Bad box face_index.\n");
            return GetVert_FarLeftTop(); // if user wishes to not abort the program
    }
}

Vec3f Box3D::GetFaceNormal( const std::size_t face_index ) const {
    ASSERT( face_index < 6 );
    switch ( face_index ) {
        case BOX_RIGHT_FACE_INDEX: return GetRightNormal();
        case BOX_LEFT_FACE_INDEX: return GetLeftNormal();
        case BOX_TOP_FACE_INDEX: return GetTopNormal();
        case BOX_BOTTOM_FACE_INDEX: return GetBottomNormal();
        case BOX_NEAR_FACE_INDEX: return GetNearNormal();
        case BOX_FAR_FACE_INDEX: return GetFarNormal();
        default:
            DIE("Box3D::GetFaceNormal(): Bad box face_index.\n");
            return {}; // if user wishes to not abort the program
    }
}

std::vector<Vec3f> Box3D::GetFaceVerts( const uint face_index ) const {
    switch ( face_index ) {
        case BOX_RIGHT_FACE_INDEX: return {BOX_RIGHT_FACE_VERTS(*this)};
        case BOX_LEFT_FACE_INDEX: return {BOX_LEFT_FACE_VERTS(*this)};
        case BOX_TOP_FACE_INDEX: return {BOX_TOP_FACE_VERTS(*this)};
        case BOX_BOTTOM_FACE_INDEX: return {BOX_BOTTOM_FACE_VERTS(*this)};
        case BOX_NEAR_FACE_INDEX: return {BOX_NEAR_FACE_VERTS(*this)};
        case BOX_FAR_FACE_INDEX: return {BOX_FAR_FACE_VERTS(*this)};
        default:
            DIE("Box3D::GetFaceVerts(): Bad box face_index.\n");
            return {}; // if user wishes to not abort the program
    };
}

Box3D& Box3D::operator=( const Box3D& other ) {
    for ( std::size_t i=0; i<8; ++i )
        vert[i] = other.vert[i];

    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );
    std::lock_guard< std::mutex > other_norms_mutex_lock( other.norms_mutex );

    if ( other.norms ) {

        if ( ! norms ) {
            norms = new Vec3f[6];
        }
        for ( uint i=0; i<6; ++i ) {
            norms[i] = other.norms[i];
        }
    } else {
        DELNULLARRAY( norms );
    }

    norms_computed = other.norms_computed;
    return *this;
}

Box3D& Box3D::operator=( Box3D&& other_rref ) {
    
    delete[] vert;
    vert = other_rref.vert;
    other_rref.vert = nullptr;

    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    delete[] norms;
    norms = other_rref.norms;
    
    norms_computed = other_rref.norms_computed;
    other_rref.norms_computed = NO_NORMS_COMPUTED;
    
    return *this;
}

Box3D& Box3D::operator+=( const Vec3f& vec ) {
    for ( std::size_t i=0; i<8; ++i )
        vert[i] += vec;

    // uniform transformation doesn't affect normals
    return *this;
}

Box3D& Box3D::operator-=( const Vec3f& vec ) {
    for ( std::size_t i=0; i<8; ++i )
        vert[i] -= vec;

    // uniform transformation doesn't affect normals
    return *this;
}

Box3D& Box3D::operator*=( const float val ) {
    for ( std::size_t i=0; i<8; ++i )
        vert[i] *= val;

    norms_computed = NO_NORMS_COMPUTED; // normals are now invalid
    return *this;

}

Box3D Box3D::operator+( const Vec3f& vec ) const {
    Box3D box( *this );
    box += vec;
    return box;
}

Box3D Box3D::operator-( const Vec3f& vec ) const {
    Box3D box( *this );
    box -= vec;
    return box;
}

Box3D Box3D::operator/( const float val ) const {
    Box3D box( *this );
    box /= val;
    return box;
}

Box3D Box3D::operator*( const float val ) const {
    Box3D box( *this );
    box *= val;
    return box;
}

Box3D& Box3D::operator/=( const float val ) {
    for ( std::size_t i=0; i<8; ++i )
        vert[i] /= val;

    norms_computed = NO_NORMS_COMPUTED; // normals are now invalid
    return *this;
}

bool Box3D::operator==( const Box3D& other ) const {
    for ( std::size_t i=0; i<8; ++i )
        if ( !Maths::Approxf( vert[i], other.vert[i] ) )
            return false;

    // normals are irrelevant
    return true;
}

Vec3f Box3D::GetLeftNormal( void ) const {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    ComputeLeftNormal();
    return norms[BOX_LEFT_FACE_INDEX];
}

Vec3f Box3D::GetRightNormal( void ) const {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    ComputeRightNormal();
    return norms[BOX_RIGHT_FACE_INDEX];
}

Vec3f Box3D::GetTopNormal( void ) const {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    ComputeTopNormal();
    return norms[BOX_TOP_FACE_INDEX];
}

Vec3f Box3D::GetBottomNormal( void ) const {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    ComputeBottomNormal();
    return norms[BOX_BOTTOM_FACE_INDEX];
}

Vec3f Box3D::GetNearNormal( void ) const {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    ComputeNearNormal();
    return norms[BOX_NEAR_FACE_INDEX];
}

Vec3f Box3D::GetFarNormal( void ) const {
    std::lock_guard< std::mutex > norms_mutex_lock( norms_mutex );

    ComputeFarNormal();
    return norms[BOX_FAR_FACE_INDEX];
}

void Box3D::ComputeLeftNormal( void ) const {
    // mutex should be locked by the caller

    if ( norms_computed & LEFT_NORM_COMPUTED ) {
        ASSERT( Maths::Approxf( norms[BOX_LEFT_FACE_INDEX], Plane3f::GetNormal( BOX_LEFT_PLANE_VERTS( *this) ) ) );
        return;
    }

    if ( ! norms ) {
        norms = new Vec3f[6];
    }

    norms[BOX_LEFT_FACE_INDEX] = Plane3f::GetNormal( BOX_LEFT_PLANE_VERTS( *this) );
    norms_computed |= LEFT_NORM_COMPUTED;
}

void Box3D::ComputeRightNormal( void ) const {
    // mutex should be locked by the caller


    if ( norms_computed & RIGHT_NORM_COMPUTED ) {
        ASSERT( Maths::Approxf( norms[BOX_RIGHT_FACE_INDEX], Plane3f::GetNormal( BOX_RIGHT_PLANE_VERTS( *this) ) ) );
        return;
    }

    if ( ! norms ) {
        norms = new Vec3f[6];
    }

    norms[BOX_RIGHT_FACE_INDEX] = Plane3f::GetNormal( BOX_RIGHT_PLANE_VERTS( *this) );
    norms_computed |= RIGHT_NORM_COMPUTED;
}

void Box3D::ComputeTopNormal( void ) const {
    // mutex should be locked by the caller


    if ( norms_computed & TOP_NORM_COMPUTED ) {
        ASSERT( Maths::Approxf( norms[BOX_TOP_FACE_INDEX], Plane3f::GetNormal( BOX_TOP_PLANE_VERTS( *this) ) ) );
        return;
    }

    if ( ! norms ) {
        norms = new Vec3f[6];
    }

    norms[BOX_TOP_FACE_INDEX] = Plane3f::GetNormal( BOX_TOP_PLANE_VERTS( *this) );
    norms_computed |= TOP_NORM_COMPUTED;
}

void Box3D::ComputeBottomNormal( void ) const {
    // mutex should be locked by the caller


    if ( norms_computed & BOTTOM_NORM_COMPUTED ) {
        ASSERT( Maths::Approxf( norms[BOX_BOTTOM_FACE_INDEX], Plane3f::GetNormal( BOX_BOTTOM_PLANE_VERTS( *this) ) ) );
        return;
    }

    if ( ! norms ) {
        norms = new Vec3f[6];
    }

    norms[BOX_BOTTOM_FACE_INDEX] = Plane3f::GetNormal( BOX_BOTTOM_PLANE_VERTS( *this) );
    norms_computed |= BOTTOM_NORM_COMPUTED;
}

void Box3D::ComputeNearNormal( void ) const {
    // mutex should be locked by the caller


    if ( norms_computed & NEAR_NORM_COMPUTED ) {
        ASSERT( Maths::Approxf( norms[BOX_NEAR_FACE_INDEX], Plane3f::GetNormal( BOX_NEAR_PLANE_VERTS( *this) ) ) );
        return;
    }

    if ( ! norms ) {
        norms = new Vec3f[6];
    }

    norms[BOX_NEAR_FACE_INDEX] = Plane3f::GetNormal( BOX_NEAR_PLANE_VERTS( *this) );
    norms_computed |= NEAR_NORM_COMPUTED;
}

void Box3D::ComputeFarNormal( void ) const {
    // mutex should be locked by the caller

    
    if ( norms_computed & FAR_NORM_COMPUTED ) {
        ASSERT( Maths::Approxf( norms[BOX_FAR_FACE_INDEX], Plane3f::GetNormal( BOX_FAR_PLANE_VERTS( *this) ) ) );
        return;
    }

    if ( ! norms ) {
        norms = new Vec3f[6];
    }

    norms[BOX_FAR_FACE_INDEX] = Plane3f::GetNormal( BOX_FAR_PLANE_VERTS( *this) );
    norms_computed |= FAR_NORM_COMPUTED;
}

Sphere Box3D::GetCircumsphere( void ) const {
    if ( !IsParalellepiped() )
        return Polygon3D::GetCircumsphere( vert, 8 );
        
    return {
        // center of the cube is midway between opposing corners
        ( vert[BOX_NEAR_LEFT_BOTTOM_INDEX] + vert[BOX_FAR_RIGHT_TOP_INDEX] ) / 2,

        // a line diagonally through the cube corner to corner will be the diameter of a sphere large enough to encompass it.
        // add just at tiny bit more just so we don't cause a problem with the corners
        // and of course divide by two to convert diameter to radius
        ( Vec3f::Len( vert[BOX_NEAR_LEFT_BOTTOM_INDEX] - vert[BOX_FAR_RIGHT_TOP_INDEX] ) + EP*2 ) / 2
    };
    
}

Sphere Box3D::GetInsphereOfCube( void ) const {
    return {
        // center of the cube is midway between opposing corners
        Vec3f( vert[BOX_NEAR_LEFT_BOTTOM_INDEX] + vert[BOX_FAR_RIGHT_TOP_INDEX] ) / 2,

        // length of one of the edges is the diameter of the insphere
        // and of course divide by two to convert diameter to radius
        Vec3f::Len( vert[BOX_NEAR_LEFT_BOTTOM_INDEX] - vert[BOX_NEAR_LEFT_TOP_INDEX] ) / 2
    };
}

void Box3D::Set( const AABox3D& aabb ) {
    SetBounds( aabb.min, aabb.max );
}

float Box3D::GetLongestEdgeLenSQ( void ) const {
    const float width = ( GetVert_FarLeftTop() - GetVert_FarRightTop() ).SquaredLen();
    const float depth = ( GetVert_FarRightTop() - GetVert_NearRightTop() ).SquaredLen();
    const float height = ( GetVert_FarRightTop() - GetVert_FarRightBottom() ).SquaredLen();
    return Max3( width, depth, height );
}

bool Box3D::Intersects( CollisionReport3D& report, const Box3D& box, const Line3f& line ) {
    return Line3f::Intersects( report, line, box );
}

bool Box3D::SAT::ToBox3D_Faces( const Box3D& box1, const Box3D& box2 ) {
    // To use separating axis theorem, we should project all of a box's vertices onto the plane created from
    // the normal of each of the other box's faces, and use Collision2D::TestToPoly. However, in the case of a box,
    // we only need to check three axes

    // SAT doesn't care what order vertices are wound in, just that they are

    // ** near
    if ( ! Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1.GetNearNormal() ) )
        return false;

    // ** bottom
    if ( ! Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1.GetBottomNormal() ) )
        return false;

    // ** left
    if ( ! Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1.GetLeftNormal() ) )
        return false;

    // ** if we're doing a paralleliped box, we're done, otherwise we have to check the other faces
    if ( !box1.IsParalellepiped() ) {

        // ** far
        if ( ! Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1.GetFarNormal() ) )
            return false;

        // ** top
        if ( ! Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1.GetTopNormal() ) )
            return false;

        // ** right
        if ( ! Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1.GetRightNormal() ) )
            return false;
    }

    return true;
}

bool ToBox3D_EdgeCrossProducts_Helper( const Box3D& box1, const Box3D& box2, const std::size_t idx1, const std::size_t idx2, const Vec3f norm );
bool ToBox3D_EdgeCrossProducts_Helper( const Box3D& box1, const Box3D& box2, const std::size_t idx1, const std::size_t idx2, const Vec3f norm ) {
    const Vec3f box1_norm = Vec3f::GetNormalized( box1.VertAt(idx1) - box1.VertAt(idx2) );
    if ( Maths::Approxf( box1_norm, norm ) )
        return false;

    return Box3D::SAT::ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2( box1_norm, box1, box2 );
}

bool Box3D::SAT::ToBox3D_EdgeCrossProducts( const Box3D& box1, const Box3D& box2 ) {
    // *** One of each unique axis (which is good enough for paralleliped boxes)

    Vec3f box1_norm_topToBottom( box1.VertAt(BOX_FAR_RIGHT_TOP_INDEX) - box1.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) ); // destination minus position gives non-normalized directional vector
    if ( !ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2( box1_norm_topToBottom, box1, box2 ) )
        return false;

    Vec3f box1_norm_leftToRight( box1.VertAt(BOX_FAR_LEFT_TOP_INDEX) - box1.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    if ( !ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2( box1_norm_leftToRight, box1, box2 ) )
        return false;

    Vec3f box1_norm_nearToFar( box1.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) - box1.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    if ( !ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2( box1_norm_nearToFar, box1, box2 ) )
        return false;

    if ( box1.IsParalellepiped() )
        return true;

    // *** Top to Bottom
    box1_norm_topToBottom.Normalize();
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_FAR_LEFT_TOP_INDEX, BOX_FAR_LEFT_BOTTOM_INDEX, box1_norm_topToBottom ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_RIGHT_TOP_INDEX, BOX_NEAR_RIGHT_BOTTOM_INDEX, box1_norm_topToBottom ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_LEFT_TOP_INDEX, BOX_NEAR_LEFT_BOTTOM_INDEX, box1_norm_topToBottom ) )
        return false;

    // *** Left to Right
    box1_norm_leftToRight.Normalize();
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_LEFT_TOP_INDEX, BOX_NEAR_RIGHT_TOP_INDEX, box1_norm_leftToRight ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_FAR_LEFT_BOTTOM_INDEX, BOX_FAR_RIGHT_BOTTOM_INDEX, box1_norm_leftToRight ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_LEFT_BOTTOM_INDEX, BOX_NEAR_RIGHT_BOTTOM_INDEX, box1_norm_leftToRight ) )
        return false;

    // *** Near to Far
    box1_norm_nearToFar.Normalize();
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_RIGHT_BOTTOM_INDEX, BOX_FAR_RIGHT_BOTTOM_INDEX, box1_norm_nearToFar ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_LEFT_TOP_INDEX, BOX_FAR_LEFT_TOP_INDEX, box1_norm_nearToFar ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Helper( box1, box2, BOX_NEAR_LEFT_BOTTOM_INDEX, BOX_FAR_LEFT_BOTTOM_INDEX, box1_norm_nearToFar ) )
        return false;

    return true;
}

bool ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( const Vec3f& box1_norm, const Box3D& box1, const Box3D& box2, const std::size_t idx1, const std::size_t idx2, const Vec3f norm );
bool ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( const Vec3f& box1_norm, const Box3D& box1, const Box3D& box2, const std::size_t idx1, const std::size_t idx2, const Vec3f norm ) {
    const Vec3f box2_norm = Vec3f::GetNormalized( box2.VertAt(idx1) - box2.VertAt(idx2) );
    if ( !Maths::Approxf( box2_norm, norm ) )
        if ( !Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1_norm.Cross( box2_norm ) ) )
            return false;

    return true;
}

bool Box3D::SAT::ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2( const Vec3f& box1_norm, const Box3D& box1, const Box3D& box2 ) {

    // *** One of each unique axis (which is good enough for paralleliped boxes)
    Vec3f box2_norm_topToBottom( box2.VertAt(BOX_FAR_RIGHT_TOP_INDEX) - box2.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) ); // destination minus position gives non-normalized directional vector
    if ( !Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1_norm.Cross( box2_norm_topToBottom ) ) )
        return false;

    Vec3f box2_norm_leftToRight( box2.VertAt(BOX_FAR_RIGHT_TOP_INDEX) - box2.VertAt(BOX_FAR_LEFT_TOP_INDEX) );
    if ( !Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1_norm.Cross( box2_norm_leftToRight ) ) )
        return false;

    Vec3f box2_norm_nearToFar( box2.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) - box2.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    if ( !Collision2D::TestPoly::SAT::GetCollision_Helper( box1.GetVertsPtr(), 8, box2.GetVertsPtr(), 8, box1_norm.Cross( box2_norm_nearToFar ) ) )
        return false;

    if ( box2.IsParalellepiped() )
        return true;

    // *** Top to Bottom
    box2_norm_topToBottom.Normalize();
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_FAR_LEFT_TOP_INDEX, BOX_FAR_LEFT_BOTTOM_INDEX, box2_norm_topToBottom ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_NEAR_RIGHT_TOP_INDEX, BOX_NEAR_RIGHT_BOTTOM_INDEX, box2_norm_topToBottom ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_FAR_RIGHT_TOP_INDEX, BOX_FAR_RIGHT_BOTTOM_INDEX, box2_norm_topToBottom ) )
        return false;

    // *** Right to Left
    box2_norm_leftToRight.Normalize();
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_NEAR_RIGHT_TOP_INDEX, BOX_NEAR_LEFT_TOP_INDEX, box2_norm_leftToRight ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_FAR_RIGHT_BOTTOM_INDEX, BOX_FAR_LEFT_BOTTOM_INDEX, box2_norm_leftToRight ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_NEAR_RIGHT_BOTTOM_INDEX, BOX_NEAR_LEFT_BOTTOM_INDEX, box2_norm_leftToRight ) )
        return false;

    // *** Near to Far
    box2_norm_nearToFar.Normalize();
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_NEAR_LEFT_BOTTOM_INDEX, BOX_FAR_LEFT_BOTTOM_INDEX, box2_norm_nearToFar ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_NEAR_RIGHT_TOP_INDEX, BOX_FAR_RIGHT_TOP_INDEX, box2_norm_nearToFar ) )
        return false;
    if ( ! ToBox3D_EdgeCrossProducts_Box1Norm_To_Box2_Helper( box1_norm, box1, box2, BOX_NEAR_LEFT_TOP_INDEX, BOX_FAR_LEFT_TOP_INDEX, box2_norm_nearToFar ) )
        return false;

    return true;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Box3D::ToModel( const Box3D& box, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
        ERR("Not Implemented: Box3D to ToModel.\n"); //toimplement
        return false;
    }

    bool Box3D::SweepToModel( CollisionReport3D& report, const Box3D& box, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {
        ERR("Not Implemented: Box3D sweep to ToModel.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

void Box3D::ClearAllNormals( void ) {
    norms_computed = NO_NORMS_COMPUTED;
}

void Box3D::Translate( const Vec3f& vec ) {
    operator+=( vec );
}

const Vec3f& Box3D::VertAt( const std::size_t vert_index ) const {
    ASSERT( vert_index < 8 );
    return vert[vert_index];
}

bool Box3D::operator!=( const Box3D& other ) const {
    return !operator==( other );
}

void Box3D::SetBounds( const Vec3f& min, const Vec3f& max ) {
    SetBounds( min.x, max.x, min.y, max.y, min.z, max.z );
}

Vec3f Box3D::GetCenter( void ) const {
    return Polygon3D::GetCenter( vert, 8 );
}

float Box3D::GetOrthogonalHeight( void ) const {
    return Vec3f::Len( vert[BOX_FAR_LEFT_TOP_INDEX] - vert[BOX_FAR_LEFT_BOTTOM_INDEX] );
}

float Box3D::GetOrthogonalWidth( void ) const {
    return Vec3f::Len( vert[BOX_FAR_LEFT_TOP_INDEX] - vert[BOX_FAR_RIGHT_TOP_INDEX] );
}

float Box3D::GetOrthogonalDepth( void ) const {
    return Vec3f::Len( vert[BOX_NEAR_LEFT_BOTTOM_INDEX] - vert[BOX_FAR_LEFT_BOTTOM_INDEX] );
}

bool Box3D::IsParalellepiped( void ) const {
    // a paralellepiped box is one in which opposing sides are paralell. ie, they have opposing normals. 
    // if we take the absoulte value of a opposing faces of a paralellepiped box, they will be equal.
    
    ComputeNearNormal();
    ComputeFarNormal();
    if ( !Maths::Approxf( norms[BOX_NEAR_FACE_INDEX], -norms[BOX_FAR_FACE_INDEX] ) )
        return false;
    
    ComputeTopNormal();
    ComputeBottomNormal();
    if ( !Maths::Approxf( norms[BOX_TOP_FACE_INDEX], -norms[BOX_BOTTOM_FACE_INDEX] ) )
        return false;
    
    ComputeRightNormal();
    ComputeLeftNormal();
    if ( !Maths::Approxf( norms[BOX_LEFT_FACE_INDEX], -norms[BOX_RIGHT_FACE_INDEX] ) )
        return false;
        
    return true;
}

Vec3f Box3D::GetFarthestVert( const Vec3f& dir ) const {
    const uint s = Polyhedron::GetFarthestVertIndex( vert, 8, dir );
    return vert[s];
}

#ifdef CLIB_UNIT_TEST
    void Box3D::UnitTest( void ) {
        // *** test constructors

        const Box3D box( -1,1, -1,1, -1,1 );
        const AABox3D aabox( -1,1, -1,1, -1,1);
        const Box3D fromaa(aabox);
        ASSERT( box == Box3D(aabox) );

        const Vec3f top_left_far(-1,1,-1);
        const Vec3f top_right_far(1,1,-1);

        const Vec3f bottom_left_far(-1,-1,-1);
        const Vec3f bottom_right_far(1,-1,-1);

        const Vec3f top_left_near(-1,1,1);
        const Vec3f top_right_near(1,1,1);

        const Vec3f bottom_left_near(-1,-1,1);
        const Vec3f bottom_right_near(1,-1,1);

        ASSERT( box.vert[BOX_FAR_LEFT_TOP_INDEX] == top_left_far );
        ASSERT( box.vert[BOX_FAR_RIGHT_TOP_INDEX] == top_right_far );

        ASSERT( box.vert[BOX_FAR_LEFT_BOTTOM_INDEX] == bottom_left_far );
        ASSERT( box.vert[BOX_FAR_RIGHT_BOTTOM_INDEX] == bottom_right_far );

        ASSERT( box.vert[BOX_NEAR_LEFT_TOP_INDEX] == top_left_near );
        ASSERT( box.vert[BOX_NEAR_RIGHT_TOP_INDEX] == top_right_near );

        ASSERT( box.vert[BOX_NEAR_LEFT_BOTTOM_INDEX] == bottom_left_near );
        ASSERT( box.vert[BOX_NEAR_RIGHT_BOTTOM_INDEX] == bottom_right_near );
        
        UnitTest_ToBox3D_Paralelepiped();
        UnitTest_ToBox3D_NonParalelepiped();
        UnitTest_IsParalellepiped();
        UnitTest_FarthestVert();
    }
    
    void Box3D::UnitTest_FarthestVert( void ) {
        const Box3D cube( 4 );
        ASSERT( cube.GetFarthestVert( Vec3f(-1,1,1) ) == Vec3f(-2,2,2) );
        ASSERT( cube.GetFarthestVert( Vec3f(1,-1,1) ) == Vec3f(2,-2,2) );
        ASSERT( cube.GetFarthestVert( Vec3f(-1,-1,1) ) == Vec3f(-2,-2,2) );
        ASSERT( cube.GetFarthestVert( Vec3f(1,1,-1) ) == Vec3f(2,2,-2) );
        ASSERT( cube.GetFarthestVert( Vec3f(-1,1,-1) ) == Vec3f(-2,2,-2) );
        ASSERT( cube.GetFarthestVert( Vec3f(1,-1,-1) ) == Vec3f(2,-2,-2) );
        ASSERT( cube.GetFarthestVert( Vec3f(-1,-1,-1) ) == Vec3f(-2,-2,-2) );
    }
    
    void Box3D::UnitTest_IsParalellepiped( void ) {
        const Box3D cube( 4 );
        ASSERT( cube.IsParalellepiped() );
        
        const Vec3f tlf(-2,2,4);
        const Vec3f trf(2,2,4);
        const Vec3f tln(-2,-2,4);
        const Vec3f trn(2,-2,4);
        const Vec3f bln(-4,4,-4);
        const Vec3f brn(4,4,-4);
        const Vec3f blf(-4,-4,-4);
        const Vec3f brf(4,-4,-4);
        const Box3D frustum( tlf, trf, tln, trn, bln, brn, blf, brf );
        ASSERT( !frustum.IsParalellepiped() );
        
        const Vec3f tlf_squished_cube(-4,4,2);
        const Vec3f trf_squished_cube(4,4,2);
        const Vec3f tln_squished_cube(-4,-4,2);
        const Vec3f trn_squished_cube(4,-4,2);
        const Vec3f bln_squished_cube(-4,4,-2);
        const Vec3f brn_squished_cube(4,4,-2);
        const Vec3f blf_squished_cube(-4,-4,-2);
        const Vec3f brf_squished_cube(4,-4,-2);
        const Box3D squishedZ( tlf_squished_cube, trf_squished_cube, tln_squished_cube, trn_squished_cube, bln_squished_cube, brn_squished_cube, blf_squished_cube, brf_squished_cube );
        ASSERT( !squishedZ.IsParalellepiped() );
        
        const Vec3f cube_tlf(-4,4,-4);
        const Vec3f cube_trf(4,4,-4);
        const Vec3f cube_tln(-4,4,4);
        const Vec3f cube_trn(4,4,4);
        const Vec3f cube_bln(-4,-4,4);
        const Vec3f cube_brn(4,-4,4);
        const Vec3f cube_blf(-4,-4,-4);
        const Vec3f cube_brf(4,-4,-4);
        const Box3D testCube( cube_tlf, cube_trf, cube_tln, cube_trn, cube_bln, cube_brn, cube_blf, cube_brf );
        ASSERT( testCube.IsParalellepiped() );

        const float topSkewX=-2;
        const Vec3f skewX_tlf(-4+topSkewX,4,-4);
        const Vec3f skewX_trf(4+topSkewX,4,-4);
        const Vec3f skewX_tln(-4+topSkewX,4,4);
        const Vec3f skewX_trn(4+topSkewX,4,4);
        const Vec3f skewX_bln(-4,-4,4);
        const Vec3f skewX_brn(4,-4,4);
        const Vec3f skewX_blf(-4,-4,-4);
        const Vec3f skewX_brf(4,-4,-4);
        const Box3D testSkewXCube( skewX_tlf, skewX_trf, skewX_tln, skewX_trn, skewX_bln, skewX_brn, skewX_blf, skewX_brf );
        ASSERT( testSkewXCube.IsParalellepiped() );
        
        const float topSkewY=-2;
        const Vec3f skewXY_tlf(-4+topSkewX,4+topSkewY,-4);
        const Vec3f skewXY_trf(4+topSkewX,4+topSkewY,-4);
        const Vec3f skewXY_tln(-4+topSkewX,4+topSkewY,4);
        const Vec3f skewXY_trn(4+topSkewX,4+topSkewY,4);
        const Vec3f skewXY_bln(-4,-4,4);
        const Vec3f skewXY_brn(4,-4,4);
        const Vec3f skewXY_blf(-4,-4,-4);
        const Vec3f skewXY_brf(4,-4,-4);
        const Box3D testSkewXYCube( skewXY_tlf, skewXY_trf, skewXY_tln, skewXY_trn, skewXY_bln, skewXY_brn, skewXY_blf, skewXY_brf );
        ASSERT( testSkewXYCube.IsParalellepiped() );

        const float backSkewZ=-2;
        const Vec3f skewXYZ_tlf(-4+topSkewX,4+topSkewY,-4);
        const Vec3f skewXYZ_trf(4+topSkewX,4+topSkewY,-4);
        const Vec3f skewXYZ_tln(-4+topSkewX,4+topSkewY,4+backSkewZ);
        const Vec3f skewXYZ_trn(4+topSkewX,4+topSkewY,4+backSkewZ);
        const Vec3f skewXYZ_bln(-4,-4,4+backSkewZ);
        const Vec3f skewXYZ_brn(4,-4,4+backSkewZ);
        const Vec3f skewXYZ_blf(-4,-4,-4);
        const Vec3f skewXYZ_brf(4,-4,-4);
        const Box3D testSkewXYZCube( skewXYZ_tlf, skewXYZ_trf, skewXYZ_tln, skewXYZ_trn, skewXYZ_bln, skewXYZ_brn, skewXYZ_blf, skewXYZ_brf );
        ASSERT( testSkewXYZCube.IsParalellepiped() );
        
        const Vec3f invalidSkew_tlf(-4+topSkewX,4+topSkewY,-4+backSkewZ);
        const Vec3f invalidSkew_trf(4+topSkewX,4+topSkewY,-4+backSkewZ);
        const Vec3f invalidSkew_tln(-4+topSkewX,4+topSkewY,4);
        const Vec3f invalidSkew_trn(4+topSkewX,4+topSkewY,4);
        const Vec3f invalidSkew_bln(-4,-4,4);
        const Vec3f invalidSkew_brn(4,-4,4);
        const Vec3f invalidSkew_blf(-4,-4,-4);
        const Vec3f invalidSkew_brf(4,-4,-4);
        const Box3D invalidSkew( invalidSkew_tlf, invalidSkew_trf, invalidSkew_tln, invalidSkew_trn, invalidSkew_bln, invalidSkew_brn, invalidSkew_blf, invalidSkew_brf );
        ASSERT( !invalidSkew.IsParalellepiped() );
    }

    void Box3D::UnitTest_ToBox3D_NonParalelepiped( void ) {
        // todo
    }

    void Box3D::UnitTest_ToBox3D_Paralelepiped( void ) {
        const Box3D bigb( -10,10, -10,10, -10,10 );

        // one box inside of the other
        const Box3D smallb( -0.5,0.5, -0.5,0.5, -0.5,0.5 );
        ASSERT( Intersects( bigb, smallb  ) );
        ASSERT( Intersects( smallb, bigb  ) );

        // coexist
        ASSERT( Intersects( bigb, bigb  ) );

        // flush with top and not touching right side
        Box3D nope1( -1,1, -1,1, -2,2 );
        Box3D nope2( -1,1, -1,1, -1,1 );
        nope2.Translate( Vec3f(0,0,1) );
        ASSERT( Intersects( nope1, nope2 ));
    }

#endif //CLIB_UNIT_TEST
