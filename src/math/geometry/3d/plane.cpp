// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./plane.h"
#include "../../collision/collision.h"

Plane3f::Plane3f( void )
    : vert()
    , norm()
{ }

Plane3f::Plane3f( const Plane3f& other )
    : vert( other.vert )
    , norm( other.norm )
{ }

Plane3f::Plane3f( const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC )
    : vert{ planeA }
    , norm( GetNormal( planeA, planeB, planeC ) )
{ }

Plane3f::Plane3f( const Vec3f& planeA, const Vec3f& _normal )
    : vert{ planeA }
    , norm( _normal )
{ }

Vec3f Plane3f::GetProjectedPoint( const Vec3f& point, const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    return GetProjectedPoint( point, planeA, GetNormal( planeA, planeB, planeC )  );
}

Vec3f Plane3f::GetProjectedPoint( const Vec3f& point, const Vec3f& planeA, const Vec3f& planeNormal ) {
    return -planeNormal * GetDistToPoint( point, planeA, planeNormal ) + point;
}

Plane3f& Plane3f::operator=( const Plane3f& other ) {
    vert = other.vert;
    norm = other.norm;
    return *this;
}

Vec3f Plane3f::GetNormal( const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    return ( (planeC-planeA).Cross( planeB-planeA ) ).GetNormalized();
}

Vec3f Plane3f::GetUnnormalizedNormal( const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    return ( (planeC-planeA).Cross( planeB-planeA ) );
}

void Plane3f::Set( const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    vert = planeA;
    norm = GetNormal( planeA, planeB, planeC );
}

void Plane3f::Set( const Vec3f& planeA, const Vec3f& _normal ) {
    vert = planeA;
    norm = _normal;
}

float Plane3f::GetDistToLine( const Line3f& line, const Vec3f& planeA, const Vec3f& plane_normal ) {
    const float dist_start = GetDistToPoint( line.vert[0], planeA, plane_normal );
    const float dist_end = GetDistToPoint( line.vert[1], planeA, plane_normal );
    
    if ( dist_start > 0 && dist_end < 0 )
        return 0; // points exist on different sides
        
    if ( dist_end > 0 && dist_start < 0 )
        return 0; // points exist on different sides
        
    switch ( line.line_type ) {
        case LineTypeT::SEG:
            return std::fminf(dist_start, dist_end);
        case LineTypeT::RAY: // ray collides if the end of line is closer to plane than the start
            if ( dist_end < dist_start ) {
                return 0; // there is collision, zero distance
            } else {
                return dist_start; // this is the shortest distance
            }
        case LineTypeT::LINE: FALLTHROUGH;
        default:
            // if line isn't paralell, it collides with plane
            return !Maths::Approxf( dist_start, dist_end, EP_MIL );
    }
}

float Plane3f::GetDistToAABox3D( const AABox3D& aabox, const Vec3f& planeA, const Vec3f& plane_normal ) {
    return GetDistToBox3D( Box3D( aabox ), planeA, plane_normal );
}

float Plane3f::GetDistToAABox3D( const AABox3D& aabox ) const {
    return GetDistToBox3D( Box3D( aabox ), vert, norm );
}

float Plane3f::GetDistToBox3D( const Box3D& box, const Vec3f& planeA, const Vec3f& plane_normal ) {
    return GetDistToVertGroup( box.GetVertsPtr(), 8, planeA, plane_normal );
}

float Plane3f::GetDistToBox3D( const Box3D& box ) const {
    return GetDistToVertGroup( box.GetVertsPtr(), 8, vert, norm );
}

float Plane3f::GetDistToVertGroup( const Vec3f* poly_verts, const std::size_t num_verts, const Vec3f& planeA, const Vec3f& plane_normal ) {
    // for a lineseg, the closest point is whichever end is closest, unless they are on different sides of the plane, in which case there is intersection
    
    if ( num_verts < 1 ) {
        WARN("Plane3f::GetDistToVertGroup: zero verts given");
        return MAX_FLOAT; // no verts in group is invalid, return a huge distance
    }

    // get the first distance to use as a baseline
    float shortestDist = GetDistToPoint( poly_verts[0], planeA, plane_normal );

    for ( std::size_t v=1; v<num_verts; ++v ) {
        const float distB = GetDistToPoint( poly_verts[v], planeA, plane_normal );

        if ( shortestDist < 0 && distB > 0 )
            return 0; // found two vertices on opposite sides. distance is zero.

        if ( shortestDist > 0 && distB < 0 )
            return 0; // found two vertices on opposite sides. distance is zero.

        if ( fabsf( distB ) < fabsf( shortestDist ) )
            shortestDist = distB;
    }

    return shortestDist;
}

float Plane3f::GetDistToCylinder( const Cylinder& cyl, const Vec3f& planeA, const Vec3f& plane_normal ) {
    const float front = Plane3f::GetDistToPoint( cyl.GetFarthestVert(plane_normal), planeA, plane_normal);
    const float back = Plane3f::GetDistToPoint( cyl.GetFarthestVert(-plane_normal), planeA, plane_normal);
    
    if ( DifferentSigns( front, back ) )
        return 0; // collision
    
    if ( fabsf(front) < fabsf(back) )
        return front;
        
    return back;
}

float Plane3f::GetDistToCylinder( const Cylinder& cyl ) const {
    return GetDistToCylinder( cyl, vert, norm );
}

float Plane3f::GetDistToCone3D( const Cone3D& cone ) const {
    return GetDistToCone3D( cone, vert, norm );
}

float Plane3f::GetDistToCone3D( const Cone3D& cone, const Vec3f& planeA, const Vec3f& plane_normal ) {
    ERR( "Plane3f::GetDistToCone3: This function doesn't work until we get Cone3D::GetFarthestVert working" );    
    return Plane3f::GetDistToPoint( cone.GetFarthestVert(-plane_normal), planeA, plane_normal );
}

Vec3f Plane3f::GetProjectedPoint( const Vec3f& point ) {
    return GetProjectedPoint( point, vert, norm );
}

Vec3f Plane3f::GetNormal( const Vec3f* three_clockwise_verts ) {
    return GetNormal( three_clockwise_verts[0], three_clockwise_verts[1], three_clockwise_verts[2] );
}

Vec3f Plane3f::GetPoint( void ) const {
    return vert;
}

float Plane3f::GetDistToPoint( const Vec3f& point, const Vec3f& planeA, const Vec3f& plane_normal ) {
    return plane_normal.Dot(point - planeA);
}

float Plane3f::GetDistToPoint( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C ) {
    return GetDistToPoint( point, A, GetNormal( A, B, C ) );
}

float Plane3f::GetDistToPoint( const Vec3f& point ) const {
    return GetDistToPoint( point, vert, norm );
}

float Plane3f::GetDistToLine( const Line3f& lineseg ) const {
    return GetDistToLine( lineseg, vert, norm );
}

float Plane3f::GetDistToVertGroup( const Vec3f* poly_verts, const std::size_t num_verts ) const {
    return GetDistToVertGroup( poly_verts, num_verts, vert, norm );
}

Vec3f Plane3f::GetNormal( void ) const {
    return norm;
}

#ifdef CLIB_UNIT_TEST
    void UnitTest_Plane3f( void ) {
        Plane3f::UnitTest_GetDistToLine();
        Plane3f::UnitTest_GetDistToPoint();
        Plane3f::UnitTest_Normal();
        
        //return; // The next ones aren't functional due to logic errors
        Plane3f::UnitTest_GetDistToCylinder();
        Plane3f::UnitTest_GetDistToCone3D();
    }

    void Plane3f::UnitTest_GetDistToLine( void ) {
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal"

        const Plane3f plane( Vec3f( 0, 0, 10 ), Vec3f( 0, 0, 1 ) );
        const Line3f above_perp_10( Vec3f(0,0,20), Vec3f(10,5,20), LineTypeT::SEG );
        ASSERT( plane.GetDistToLine( above_perp_10 ) == 10 );

        const Line3f below_perp_10( Vec3f(0,0,0), Vec3f(10,5,0) );
        ASSERT( plane.GetDistToLine( below_perp_10 ) == -10 );

        const Line3f above_slanted_10( Vec3f(0,0,20), Vec3f(10,5,31) );
        ASSERT( plane.GetDistToLine( above_slanted_10 ) == 10 );

        const Line3f below_slanted_10( Vec3f(0,0,0), Vec3f(10,5,-31) );
        ASSERT( plane.GetDistToLine( below_slanted_10 ) == -10 );

        const Line3f intersect_perp( Vec3f(0,0,10), Vec3f(10,5,10) );
        ASSERT( plane.GetDistToLine( intersect_perp ) == 0 );

        const Line3f intersect_slanted( Vec3f(0,0,10), Vec3f(10,5,100) );
        ASSERT( plane.GetDistToLine( intersect_slanted ) == 0 );
    #pragma GCC diagnostic pop
    }

    void Plane3f::UnitTest_GetDistToPoint( void ) {
        // plane laying on the x/y axes, facing down, normal is (0,0,-1)
        const Vec3f a(-2,2,0);
        const Vec3f b(2,2,0);
        const Vec3f c(2,-2,0);

        ASSERT( Maths::Approxf( GetDistToPoint( Vec3f(0,0,0), a, b, c ), 0 ) );
        ASSERT( Maths::Approxf( GetDistToPoint( Vec3f( 0,0,1), a, b, c ), 1 ) );
        ASSERT( Maths::Approxf( GetDistToPoint( Vec3f(0,0,-1), a, b, c ), -1 ) );
        ASSERT( Maths::Approxf( GetDistToPoint( Vec3f(0,0,12.34f), a, b, c ), 12.34f ) );
        ASSERT( Maths::Approxf( GetDistToPoint( Vec3f(0,0,-12.34f), a, b, c ), -12.34f ) );
    }
    
    void Plane3f::UnitTest_Normal( void ) {
        Plane3f plane( Vec3f(-2,0,0), Vec3f(0,2,0), Vec3f(2,0,0) );
        Vec3f normal( 0,0,1 );
        
        ASSERT( plane.GetNormal() == normal );
    }
    
    void Plane3f::UnitTest_GetDistToCylinder( void ) {
        const float cyl_radius=3;
        const float cyl_length=4;
        const Cylinder cyl( Vec3f(0,0,0), Vec3f(0,0,1), cyl_radius, cyl_length);
        
        const float point_a = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,0), Vec3f(0,0,1) );
        ASSERT( Maths::Approxf( point_a, 0 ) );
        
        const float point_b = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,-1), Vec3f(0,0,1) );
        ASSERT( Maths::Approxf( point_b, 1 ) );
        
        const float point_c = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,-10), Vec3f(0,0,1) );
        ASSERT( Maths::Approxf( point_c, 10 ) );
        
        const float point_d = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,cyl_length+1), Vec3f(0,0,1) );
        ASSERT( Maths::Approxf( point_d, -1 ) );
        
        const float point_e = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,cyl_length+10), Vec3f(0,0,1) );
        ASSERT( Maths::Approxf( point_e, -10 ) );
        
        const float point_f = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,cyl_length/2), Vec3f(0,0,1) );
        ASSERT( Maths::Approxf( point_f, 0 ) );
        
        const float point_g = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,-1), Vec3f(0,0,-1) );
        ASSERT( Maths::Approxf( point_g, -1 ) );
        
        const float point_h = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,-10), Vec3f(0,0,-1) );
        ASSERT( Maths::Approxf( point_h, -10 ) );
        
        const float point_i = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,cyl_length+1), Vec3f(0,0,-1) );
        ASSERT( Maths::Approxf( point_i, 1 ) );
        
        const float point_j = Plane3f::GetDistToCylinder( cyl, Vec3f(0,0,cyl_length+10), Vec3f(0,0,-1) );
        ASSERT( Maths::Approxf( point_j, 10 ) );
        
        const float point_k = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius,0), Vec3f(0,1,0) );
        ASSERT( Maths::Approxf( point_k, 0 ) );
        
        const float point_l = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius+7,0), Vec3f(0,1,0) );
        ASSERT( Maths::Approxf( point_l, -7 ) );
        
        const float point_m = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius+7,0), Vec3f(0,-1,0) );
        ASSERT( Maths::Approxf( point_m, 7 ) );
        
        const float point_n = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius,cyl_length), Vec3f(0,1,1).GetNormalized() );
        ASSERT( Maths::Approxf( point_n, 0 ) );
        
        const float point_o = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius+1,cyl_length+1), Vec3f(0,1,1).GetNormalized() );
        ASSERT( Maths::Approxf( point_o, -Vec3f(1,1,0).Len() ) );
        
        const float point_p = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius+1,cyl_length+1), -Vec3f(0,1,1).GetNormalized() );
        ASSERT( Maths::Approxf( point_p, Vec3f(1,1,0).Len() ) );
        
        const float point_q = Plane3f::GetDistToCylinder( cyl, Vec3f(0,-cyl_radius-1,cyl_length+1), Vec3f(0,-1,1).GetNormalized() );
        ASSERT( Maths::Approxf( point_q, -Vec3f(1,1,0).Len() ) );
        
        const float point_r = Plane3f::GetDistToCylinder( cyl, Vec3f(0,-cyl_radius-1,cyl_length+1), -Vec3f(0,-1,1).GetNormalized() );
        ASSERT( Maths::Approxf( point_r, Vec3f(1,1,0).Len() ) );
        
        const float point_s = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius+1,-1), Vec3f(0,1,-1).GetNormalized() );
        ASSERT( Maths::Approxf( point_s, -Vec3f(1,1,0).Len() ) );
        
        const float point_t = Plane3f::GetDistToCylinder( cyl, Vec3f(0,cyl_radius+1,-1), -Vec3f(0,1,-1).GetNormalized() );
        ASSERT( Maths::Approxf( point_t, Vec3f(1,1,0).Len() ) );
        
        const float point_u = Plane3f::GetDistToCylinder( cyl, Vec3f(0,-cyl_radius-1,-1), Vec3f(0,-1,-1).GetNormalized() );
        ASSERT( Maths::Approxf( point_u, -Vec3f(1,1,0).Len() ) );
        
        const float point_v = Plane3f::GetDistToCylinder( cyl, Vec3f(0,-cyl_radius-1,-1), -Vec3f(0,-1,-1).GetNormalized() );
        ASSERT( Maths::Approxf( point_v, Vec3f(1,1,0).Len() ) );
    }
    
    void Plane3f::UnitTest_GetDistToCone3D( void ) {
        
    }

#endif // CLIB_UNIT_TEST
