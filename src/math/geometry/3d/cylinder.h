// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_CYL_H_
#define SRC_COLLISION_3D_CYL_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class SemiCylinder;
class Polygon3D;

class Cylinder {
public:
    Cylinder( void )
        : origin(), normal(0,0,1), radius(1), length(1) { } //!< the origin is the bottom of the cylinder

    Cylinder( const Vec3f& org, const Vec3f& norm, const float rad, const float len )
        : origin( org ), normal( norm ), radius( rad ), length( len ) { } //!< the origin is the bottom of the cylinder

public:
    Vec3f GetEnd( void ) const;
    Vec3f GetFarthestVert( const Vec3f& normalized_dir ) const;

#ifdef CLIB_UNIT_TEST
    static void UnitTest( void );
#endif //CLIB_UNIT_TEST

    static bool ToPlane( const Cylinder& cyl, const Vec3f& planeA, const Vec3f& plane_normal );
    static bool Intersects( const Cylinder& cyl, const Line3f& line );
    static bool Intersects( const Cylinder& cyl, const AABox3D& aabox );
    static bool Intersects( const Cylinder& cyl, const Box3D& aabox );
    static bool Intersects( const Cylinder& cyl, const Vec3f& point );
    static bool Intersects( const Cylinder& cyl, const Capsule& capsule );
    static bool Intersects( const Cylinder& cyl, const SemiCylinder& semicyl );
    static bool Intersects( const Cylinder& cyl, const Cylinder& other );
    static bool Intersects( const Cylinder& cyl, const Polygon3D& polygon );

    static bool ToModel( const Cylinder& cyl, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );
    static bool SweepToModel( CollisionReport3D& report, const Cylinder& cyl, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );

#ifdef CLIB_UNIT_TEST
    static void UnitTest_ToLineSeg( void );
    static void UnitTest_ToAABox3D( void );
    static void UnitTest_ToBox3D( void );
#endif //CLIB_UNIT_TEST
    
    static bool Intersects( const Cylinder& cyl, const Sphere& sphere );

public:
    Vec3f origin; //!< the origin is the bottom of the cylinder
    Vec3f normal; //!< points from start to end, ie  (end-start).Normalized()
    float radius;
    float length;
};

class SemiCylinder : public Cylinder {
public:
    SemiCylinder( void )
        : Cylinder()
        , dir(0,1,0)
        , degrees(180)
        { }

    SemiCylinder( const Vec3f& org, const Vec3f& norm, const float rad, const float len, const Vec3f& direction, const float angle_degrees )
        : Cylinder( org, norm, rad, len )
        , dir( direction )
        , degrees( angle_degrees )
        { }

public:
    Vec3f dir; //!< the rounded part of the cylindar, which way it faces. This MUST be perpendicular to Cylindar3D::normal
    float degrees; //!< degrees (0 to 360) of how much of the cylinder exists. a 1/2 cylindar would be 180
};

#endif // SRC_COLLISION_3D_CYL_H_
