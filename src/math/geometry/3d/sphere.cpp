// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./sphere.h"
#include "../../collision/collision.h"
#include "../../collision/gjk.h"
#include "../../../rendering/models/model.h"

Sphere::Sphere( const Model& modelToEncompass )
    : origin()
    , radius()
{
    SetRadiusFromModel( modelToEncompass );
}

void Sphere::SetRadiusFromModel( const Model& model ) {
    if ( model.NumMeshes() < 1 ) {
        ERR("Sphere::SetBoundsFromModel: model has no mesh.\n");
        return;
    }
    const AABox3D bounds = model.GetBounds();
    const float minbound = Min3( bounds.min.x, bounds.min.y, bounds.min.z );
    const float maxbound = Max3( bounds.max.x, bounds.max.y, bounds.max.z );
    radius = (maxbound - minbound) / 2;
}

bool Sphere::Intersects( const Sphere& sphere, const Vec3f& point ) {
    return Collision3D::TestPoint::Intersects( point, sphere );
}

bool Sphere::Intersects( const Sphere& sphere1, const Sphere& sphere2 ) {
    return Vec3f::SquaredLen( sphere1.origin - sphere2.origin ) <= (sphere1.radius + sphere2.radius) * (sphere1.radius + sphere2.radius);
}

//! Moves the cylindar's origin to (0,0,0) and rotates it's middle onto the +X axis; moves the sphere with it.
template< typename TYPE >
void ToCyl_TranslateForCheck( Sphere& sphere, TYPE& cyl );

template< typename TYPE >
void ToCyl_TranslateForCheck( Sphere& sphere, TYPE& cyl ) {
    // **** Step1: Check Lateral Section
    // ** translate cyl to (0,0,0) and translate sphere with it
    sphere.origin -= cyl.origin;
    cyl.origin.Zero();

    // ** cylinder end will be rotated onto the +X axis
    const Vec3f cyl_old_normal( cyl.normal );
    cyl.normal.Set( 1,0,0 );

    // ** translate sphere by that rotation, as well
    const float radians( cyl.normal.RadiansBetween( cyl_old_normal ) );
    if ( !Maths::Approxf( radians, 0 ) ) {
        // the axis of our rotation will be the normal of a plane made from our origin (0,0,0), our old end, and our new end
        const Vec3f cyl_new_end( cyl.length,0,0 );
        const Vec3f cyl_old_end( cyl_old_normal * cyl.length );
        const Vec3f axis( Plane3f::GetNormal( cyl.origin, cyl_old_end, cyl_new_end ) );
        sphere.origin *= Quat::FromAxis( -radians, axis );
    }
}

bool Sphere::Intersects( Sphere sphere, Capsule cap ) {
    ToCyl_TranslateForCheck( sphere, cap );
    // ** check end caps

    if ( Sphere::Intersects( sphere, Sphere( cap.origin, cap.radius ) ) )
        return true;

    const Vec3f cap_end( cap.length,0,0 ); // our origin is at (0,0,0)
    if ( Sphere::Intersects( sphere, Sphere( cap_end, cap.radius ) ) )
        return true;

    if ( sphere.origin.x >= 0 ) {
        if ( sphere.origin.x <= cap.length ) {
            // the sphere's origin is between the end caps
            const float sphere_dist_to_x_axis_sq = sphere.origin.y * sphere.origin.y + sphere.origin.z * sphere.origin.z;
            float radii_sum_sq = cap.radius + sphere.radius + EP;
            radii_sum_sq *= radii_sum_sq;
            return sphere_dist_to_x_axis_sq <= radii_sum_sq; // add an epsilon to account for loss of precision during the rotation;

        } else {
            if ( sphere.origin.x - sphere.radius > cap.length + cap.radius + EP ) // add an epsilon to account for loss of precision during the rotation
                return false; // no way they can touch
        }
    } else {
        if ( sphere.origin.x < -(sphere.radius+cap.radius+EP) ) // add an epsilon to account for loss of precision during the rotation
            return false; // no way they can touch
    }

    return false;
}

bool Sphere::Intersects( Sphere sphere, Cylinder cyl ) {
    ToCyl_TranslateForCheck( sphere, cyl );

    if ( sphere.origin.x >= 0 ) {
        if ( sphere.origin.x <= cyl.length ) {
            // the sphere's origin is between the end caps
            const float sphere_dist_to_x_axis_sq = sphere.origin.y * sphere.origin.y + sphere.origin.z * sphere.origin.z;
            float radii_sum_sq = cyl.radius + sphere.radius + EP; // add an epsilon to account for loss of precision during the rotation
            radii_sum_sq *= radii_sum_sq;
            return sphere_dist_to_x_axis_sq <= radii_sum_sq;

        } else {
            if ( sphere.origin.x - sphere.radius > cyl.length + EP ) // add an epsilon to account for loss of precision during the rotation
                return false; // no way they can touch
        }
    } else {
        if ( sphere.origin.x < -(sphere.radius+EP) ) // add an epsilon to account for loss of precision during the rotation
            return false; // no way they can touch
    }

    // check end caps
    return Collision2D::TestCircle::ToCircle(
        Vec2f(sphere.origin.y,sphere.origin.z), sphere.radius,
        Vec2f(0,0), cyl.radius
    );
}

float Sphere::DistToAABox3D( const Sphere& sphere, const AABox3D& aabox, Vec3f* norm_out ) {
    const float dist( Collision3D::TestPoint::DistToAABox3D( sphere.origin, aabox, norm_out ) );
    if ( dist < sphere.radius )
        return 0;

    return dist - sphere.radius;
}

bool Sphere::ToPoly( const Sphere& sphere, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& polyNormal ) {
    ASSERT( vert_count >= 3 );
    ASSERT( clockwise_verts );

    float signed_dist;
    if ( !Sphere::ToPlane( sphere, clockwise_verts[0], polyNormal, &signed_dist ) )
        return false;

    // ** if the collision point is within the poly, success.
    const Vec3f point( Plane3f::GetProjectedPoint( sphere.origin, clockwise_verts[0], polyNormal ) );
    if ( Collision3D::TestPoint::ToPoly( point, clockwise_verts, vert_count, polyNormal ) )
        return true;

    // ** ( vertices are automagically checked this way, too )
    CollisionReport3D report_ignored;
    for ( std::size_t a=vert_count-1,b=0; b < vert_count; a=b++ )
        if ( Line3f::ToSphere( report_ignored, LineTypeT::SEG, clockwise_verts[a], clockwise_verts[b], sphere ) )
            return true;

    return false;
}

bool Sphere::ToPoly( const Sphere& sphere, const Vec3f* clockwise_verts, const std::size_t vert_count ) {
    ASSERT( vert_count >= 3 );
    ASSERT( clockwise_verts );

    const Vec3f norm( Plane3f::GetNormal( clockwise_verts ) );
    return ToPoly( sphere, clockwise_verts, vert_count, norm );
}

bool Sphere::ToPlane( const Sphere& sphere, const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC, float* signed_dist_out ) {
    const Vec3f planeNormal( Plane3f::GetNormal( planeA, planeB, planeC ) );
    return ToPlane( sphere, planeA, planeNormal, signed_dist_out );
}

bool Sphere::ToPlane( const Sphere& sphere, const Vec3f& planeA, const Vec3f& planeNormal, float* signed_dist_out ) {
    const float signed_dist = Plane3f::GetDistToPoint( sphere.origin, planeA, planeNormal );
    if ( signed_dist_out )
        *signed_dist_out = signed_dist;
    return fabsf( signed_dist ) <= sphere.radius;
}

bool Sphere::GetSATCollision_Helper( const Sphere& sphere, const Vec3f* verts, const std::size_t num_verts, const Vec3f& axis ) {
    std::size_t v;

    // ** project all the points from poly1 onto the axis
    const float axis_dot_sphere_origin = axis.Dot( sphere.origin );
    const float sphere_min( axis_dot_sphere_origin - sphere.radius );
    const float sphere_max( axis_dot_sphere_origin + sphere.radius );

    // ** project each point from poly2 onto the axis and test for collision
    typedef int8 SideOfPoly1ProjectionT_BaseType;
    enum class SideOfPoly1ProjectionT : SideOfPoly1ProjectionT_BaseType {
        INSIDE=0 // default
        , LEFT=-1
        , RIGHT=1
    };

    SideOfPoly1ProjectionT prev_side = SideOfPoly1ProjectionT::INSIDE;
    for ( v = 0; v < num_verts; ++v ) {
        float p_dot = axis.Dot( verts[v] );

        if ( p_dot < sphere_min ) { // point is on the LEFT side

            // if any two points are on opposite sides of poly1's projections, there is collision on this axis
            if ( prev_side == SideOfPoly1ProjectionT::RIGHT )
                return true;

            prev_side = SideOfPoly1ProjectionT::LEFT;

        } else if ( p_dot > sphere_max ) { // point is on the RIGHT side

            // if any two points are on opposite sides of poly1's projections, there is collision on this axis
            if ( prev_side == SideOfPoly1ProjectionT::LEFT )
                return true;

            prev_side = SideOfPoly1ProjectionT::RIGHT;

        } else { // the point is INSIDE of the poly1's projection
            return true;
        }
    }

    return false;
}

bool Sphere::Intersects( const Sphere& sphere, const Box3D& box ) {
    return Collision3D::TestGJK::Intersects( sphere, box );
/**
    auto ToBox3D_Helper=[]( const Box3D& box, const Sphere& sphere, const BoxTypeT box_type ) {
        // To use separating axis theorem, we should project all of a box's vertices onto the plane created from
        // the normal of each of the box's faces, and use Collision2D::TestToPoly. However, in the case of a box,
        // we only need to check three axes

        // SAT doesn't care what order vertices are wound in, just that they are

        Vec3f norm;

        if ( ! GetSATCollision_Helper( box.GetVertsPtr(), 8, sphere, box.GetNearNormal() ) )
            return false;

        if ( ! GetSATCollision_Helper( box.GetVertsPtr(), 8, sphere, box.GetBottomNormal() ) )
            return false;

        // ** left
        if ( ! GetSATCollision_Helper( box.GetVertsPtr(), 8, sphere, box.GetLeftNormal() ) )
            return false;

        // if it's a paralelepiped box, we don't need to check the other sides
        if ( box_type != BoxTypeT::PARALLELEPIPED ) {
            if ( ! GetSATCollision_Helper( box.GetVertsPtr(), 8, sphere, box.GetFarNormal() ) )
                return false;

            if ( ! GetSATCollision_Helper( box.GetVertsPtr(), 8, sphere, box.GetTopNormal() ) )
                return false;

            if ( ! GetSATCollision_Helper( box.GetVertsPtr(), 8, sphere, box.GetRightNormal() ) )
                return false;
        }

        return true;
    };

    if ( ! Sphere::Intersects_Helper( box, sphere, box_type ) )
        return Collision3D::TestPoint::ToSphere( box[0], sphere ) || Collision3D::TestPoint::Intersects( sphere.origin, box ); // SAT doesn't check for encapsulation, so we do it here

    return true;
**/
}

bool Sphere::SweepToPlane( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& point_on_plane, const Vec3f& plane_normal ) {

    // if it's not a valid plane, it doesn't collide.
    if ( plane_normal.SquaredLen() < (1-EP) )
        return false;

    report.norm = plane_normal;

    const float sphere_dist_at_origin = Plane3f::GetDistToPoint( sphere.origin, point_on_plane, plane_normal );
    if ( sphere_dist_at_origin <= sphere.radius )
        return false;  // sphere is touching, intersecing, or behind the plane (sweep collision doesn't work if sphere's surface is already on or behind the plane)

    const float sphere_dist_at_dest = Plane3f::GetDistToPoint( sphere_dest, point_on_plane, plane_normal );

    if ( sphere_dist_at_dest > sphere.radius ) // if it were equal, it would mean dest sphere is exactly on the surface
        return false;

    // ** collision has occured

    report.time = sphere_dist_at_origin - sphere.radius; // disregard the length ofour vector that already exists inside of our radius at start
    if ( ! Maths::Approxf( report.time, 0.0f ) ) {
        const float denominator = sphere_dist_at_origin - sphere_dist_at_dest;

        // a distance of zero means coinciding with the plane
        if ( !Maths::Approxf( denominator, 0 ) ) {
            report.time /= denominator; // time
        }
    }

    // ** get intersection point
    if ( report.ComputeIntersection() ) {
        // ** Get where the ORIGIN of the sphere will be at the first collision time
        const Vec3f velocity_vector( sphere_dest-sphere.origin );
        //const Vec3f velocity_normal( velocity_vector.GetNormalized() );
        const Vec3f sphere_origin_at_collision( (velocity_vector * report.time) + sphere.origin );
        report.point = sphere_origin_at_collision - (plane_normal * sphere.radius);
    }

    return true;
}

bool Sphere::SweepToPoly( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f* clockwise_verts, const std::size_t numVerts ) {
    const Vec3f poly_normal( Plane3f::GetNormal( clockwise_verts ) );
    return Sphere::SweepToPoly( report, sphere, sphere_dest, clockwise_verts, numVerts, poly_normal );
}

bool Sphere::SweepToPoly( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f* clockwise_verts, const std::size_t numVerts, const Vec3f& poly_normal ) {
    ASSERT( clockwise_verts && numVerts > 2 );

    if ( poly_normal.SquaredLen() < (1-EP) )
        return false; // if it's not a valid plane, it doesn't collide.

    // NOTE: for an illustration of what is going on here, look at gimp file "sphere-collision-detection-sweep.xcf"

    if ( Sphere::SweepToPlane( report, sphere, sphere_dest, clockwise_verts[0], poly_normal ) ) {
        if ( Collision3D::TestPoint::ToPoly( report.point, clockwise_verts, numVerts, poly_normal ) ) {
            // collision point is within the poly; no need to check corners/edges of the poly
            // report.norm, report.time, and report.point set by SweepToPlane
            return true;
        }
    }

    // ** check all the lines/points ( SweepToLine checks points )
    bool ret = false;
    CollisionReport3D line_report(report.calc); // we don't want to taint our results
    for ( std::size_t a=numVerts-1,b=0; b<numVerts;a=b++ ) {
        if ( SweepToLine( line_report, sphere, sphere_dest, LineTypeT::SEG, clockwise_verts[a], clockwise_verts[b] ) ) {

            // take the closest collision
            if ( ! ret || line_report.time < report.time ) {
                report = line_report;
                ret = true;
            }
        }
    }

    return ret;
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Cylinder& cyl ) {
        ERR("Not Implemented: Sphere::SweepTo Cyl.\n"); //toimplement
        return false;
    }
    
    bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Capsule& cap ) {
        ERR("Not Implemented: Sphere::SweepTo Capsule.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Sphere::SweepTo( const Sphere& sphere, const Vec3f& sphere_dest, const AABox3D& aabox ) {
    CollisionReport3D report(CollisionCalcsT::None);
    return Sphere::SweepTo( report, sphere, sphere_dest, aabox );
}

bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const AABox3D& aabox ) {
    return SweepTo( report, sphere, sphere_dest, Box3D( aabox ) );
}

bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Box3D& box ) {

    // if a simple capsule collision to the sphere encompassing the box doesn't hit, that's an early-out
    if ( ! Sphere::Intersects( box.GetCircumsphere(), Capsule::FromSweepSphere( sphere, sphere_dest ) ) )
        return false;

    CollisionReport3D tmpReport( report.calc | CollisionCalcsT::Intersection );

    std::vector< Vec3f > verts = { BOX_TOP_FACE_VERTS( box ) };
    bool ret = Sphere::SweepToPoly( report, sphere, sphere_dest, verts.data(), 4, box.GetTopNormal() );

    verts = { BOX_LEFT_FACE_VERTS( box ) };
    if ( Sphere::SweepToPoly( tmpReport, sphere, sphere_dest, verts.data(), 4, box.GetLeftNormal() ) ) {
        if ( (!ret) || ( tmpReport.time < report.time ) ) {
            report = tmpReport;
            ret = true;
        }
    }

    verts = { BOX_FAR_FACE_VERTS( box ) };
    if ( Sphere::SweepToPoly( tmpReport, sphere, sphere_dest, verts.data(), 4, box.GetFarNormal() ) ) {
        if ( (!ret) || ( tmpReport.time < report.time ) ) {
            report = tmpReport;
            ret = true;
        }
    }

    verts = { BOX_BOTTOM_FACE_VERTS( box ) };
    if ( Sphere::SweepToPoly( tmpReport, sphere, sphere_dest, verts.data(), 4, box.GetBottomNormal() ) ) {
        if ( (!ret) || ( tmpReport.time < report.time ) ) {
            report = tmpReport;
            ret = true;
        }
    }

    verts = { BOX_RIGHT_FACE_VERTS( box ) };
    if ( Sphere::SweepToPoly( tmpReport, sphere, sphere_dest, verts.data(), 4, box.GetRightNormal() ) ) {
        if ( (!ret) || ( tmpReport.time < report.time ) ) {
            report = tmpReport;
            ret = true;
        }
    }

    verts = { BOX_NEAR_FACE_VERTS( box ) };
    if ( Sphere::SweepToPoly( tmpReport, sphere, sphere_dest, verts.data(), 4, box.GetNearNormal() ) ) {
        if ( (!ret) || ( tmpReport.time < report.time ) ) {
            report = tmpReport;
            ret = true;
        }
    }

    return ret;
}

bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Sphere& obstacle_sphere ) {
    // we need the normal in order to calculate point properly
    CollisionCalcsT calcs( report.calc );
    if ( report.ComputeIntersection() )
        calcs |= CollisionCalcsT::Normal;
    CollisionReport3D tempReport( calcs );
    tempReport = report;

    // temporarily grow our radius by theirs and they become a point
    if ( Sphere::SweepTo( tempReport, Sphere( sphere.origin, sphere.radius + obstacle_sphere.radius ), sphere_dest, obstacle_sphere.origin ) ) {
        if ( tempReport.ComputeIntersection() ) {
            // ** move the collision point toward us by the amount of other sphere's radius. that is the real collision point
            tempReport.point = tempReport.norm; // direction from obstacle to us
            tempReport.point *= obstacle_sphere.radius;
            tempReport.point += obstacle_sphere.origin;
        }

        report = tempReport;
        return true;
    }

    report = tempReport;
    return false;
}

bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& check_point ) {
    // to visualize this, it is almost capsule collision except points inside the sphere at the start point aren't considered collisions.
    const Vec3f velocity( sphere_dest - sphere.origin );

    // **** BEGIN: the following is explained in "Improved Collision Detection and Response" page 15, by Kasper Fauerby / kasper@peroxide.dk / http://www.peroxide.dk / published 25th July 2003{
    // this same formula is also covered here, albeit the calculations seem different and, in my tests, did not work: http://therealdblack.wordpress.com/category/sweep-tests/

    const float a( velocity.SquaredLen() );
    const float b( velocity.Dot(sphere.origin - check_point) * 2 );
    const float c( Vec3f::SquaredLen(check_point - sphere.origin) - sphere.radius * sphere.radius );

    if ( !Maths::GetLowestRoot( a, b, c, report.time ) )
        return false;

    // collision point has to lie somewhere "up the vlocity" that isn't before our start position or beyond the destination
    if ( report.time < 0.0f || report.time > 1.0f )
        return false;

    // **** END

    report.point = check_point;

    if ( report.ComputeNormal() ) {
        const Vec3f sphere_origin_at_collision = sphere.origin + velocity*report.time;
        report.norm = sphere_origin_at_collision - report.point;
        report.norm.Normalize();
    }

    return true;
}

bool Sphere::SweepToLine( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end ) {
    // ** check if closest point is not on the infinite line

    const Vec3f vel( sphere_dest - sphere.origin );
    const Vec3f edge( line_end - line_start );

    const float edge_dot_vel( edge.Dot(vel) );
    const float edge_dot_vel_sq( edge_dot_vel * edge_dot_vel );

    const Vec3f originToLineStart( line_start - sphere.origin );

    const float originToLineStart_len_sq( originToLineStart.SquaredLen() );
    const float edge_dot_originToStart( edge.Dot(originToLineStart) );
    const float edge_dot_originToStart_sq( edge_dot_originToStart * edge_dot_originToStart );
    const float edge_len_sq( edge.SquaredLen() );
    const float vel_len_sq( vel.SquaredLen() );

    // **** BEGIN: the following is explained in "Improved Collision Detection and Response" page 15, by Kasper Fauerby / kasper@peroxide.dk / http://www.peroxide.dk / published 25th July 2003{

    const float a( edge_len_sq * -vel_len_sq + edge_dot_vel_sq );
    const float b( edge_len_sq * vel.Dot(originToLineStart)*2 - edge_dot_vel*edge_dot_originToStart*2 );
    const float c( edge_len_sq * ((sphere.radius*sphere.radius)-originToLineStart_len_sq) + edge_dot_originToStart_sq );

    if ( !Maths::GetLowestRoot( a, b, c, report.time ) )
        return false;

    // **** END

    // ** check if point is even on the closed velocity. if false, we still need to check the vertices later
    bool ret = report.time >= 0.0f && report.time <= 1.0f;

    // ** check if the point we get from that is on the line seg/ray/line
    CollisionReport3D point_report(report.calc);
    const float time_up_line( ( edge_dot_vel * report.time - edge_dot_originToStart ) / edge_len_sq );
    bool vert_1 = false;
    bool vert_2 = false;
    switch ( line_type ) {
        case LineTypeT::LINE:
            if ( !ret )
                return false;
            break; // no need to check vertices
        case LineTypeT::SEG:
            if ( time_up_line > 1.0f )
                ret = false;
            FALLTHROUGH; // all ray checks apply to lineseg
        case LineTypeT::RAY:
            if ( time_up_line < 0.0f )
                ret = false;

            // ** we have to check BOTH vertices regardless of whether we get lineseg or ray collision, in case the line collision point is not on the line segment
            vert_1 = SweepTo( point_report, sphere, sphere_dest, line_start );
            if ( vert_1 ) {
                if ( !ret || point_report.time < report.time ) {
                    report = point_report;
                } else {
                    vert_1 = false; // collision point is not closer than a previous one that was found
                }
            }

            vert_2 = SweepTo( point_report, sphere, sphere_dest, line_end );
            if ( vert_2 ) {
                if ( !ret || point_report.time < report.time ) {
                    report = point_report;
                } else {
                    vert_2 = false; // collision point is not closer than a previous one that was found
                }
            }

            if ( vert_1 || vert_2 ) {
                // reportClosest.normal/point is set by SweepTo
                if ( report.ComputeNormal() ) {
                    const Vec3f sphere_origin_at_collision = sphere.origin + vel*report.time;
                    report.norm = sphere_origin_at_collision - report.point;
                    report.norm.Normalize();
                }
                return true;
            }

            if ( !ret )
                return false;

            break;
        default:
            DIE("Invalid line type\n");
            return false;
    };

    if ( report.ComputeNormal() || report.ComputeIntersection() ) {
        report.point = edge;
        report.point *= time_up_line;
        report.point += line_start;

        if ( report.ComputeNormal() ) { // report.norm points AWAY from sphere
            const Vec3f sphere_origin_at_collision = sphere.origin + vel*report.time;
            report.norm = sphere_origin_at_collision - report.point;
            report.norm.Normalize();
        }
    }

    return true;
}

bool Sphere::SweepTo( const Sphere& sphere, const Vec3f& sphere_dest, const Box3D& box ) {
    CollisionReport3D report(CollisionCalcsT::None);
    return Sphere::SweepTo( report, sphere, sphere_dest, box );
}

bool Sphere::SweepToModel( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {

    Mat3f model_rotation;
    const bool do_model_rotation = model_angles != Vec3f(0,0,0);
    if ( do_model_rotation ) {
        model_rotation.RotateZYX( model_angles );
    }

    // ** early-out if swept-sphere as capsule doesn't collide with model's circumsphere
    const auto model_bounds = model.GetBounds();
    auto model_circumsphere = Sphere( model_bounds.GetCircumsphere() );
    if ( do_model_rotation ) {
        model_circumsphere.origin *= model_rotation;
    }
    model_circumsphere.origin += model_origin;
    if ( !Sphere::Intersects( model_circumsphere, Capsule::FromSweepSphere( sphere, sphere_dest ) ) )
        return false;
        
    CollisionReportTemp3D closest;
    std::vector<Vec3f> verts(3);
    bool collision_occured = false;
    
    for ( uint m=0; m<model.NumMeshes(); ++m ) {
        const Mesh* mesh = model.GetMesh( m );
        if ( !mesh )
            continue;

        for ( auto const& face : mesh->faces ) {
            if ( face.vert.Num() > verts.size() )
                verts.resize( face.vert.Num() * 2 );

            std::size_t v=0;
            for ( auto const& vert : face.vert ) {
                if ( do_model_rotation ) {
                    verts[v] = mesh->verts[ vert.vertIndex ].co * model_rotation;
                } else {
                    verts[v] = mesh->verts[ vert.vertIndex ].co;
                }
                verts[v] += model_origin;
                ++v;
            }

            if ( Sphere::SweepToPoly( report, sphere, sphere_dest, verts.data(), verts.size() ) ) {
                if ( !collision_occured || report.time < closest.time ) {
                    if ( clipType == ModelCollisionT::ANY_POLY ) {
                        return true;
                    }
                    collision_occured = true;
                    closest = report;
                }
            }
        }
    }

    if ( collision_occured )
        report = closest;

    return collision_occured;
}

bool Sphere::ToModel( const Sphere& sphere, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
    if ( model.NumMeshes() < 1 )
        return false;
        
    Mat3f model_rotation;
    const bool do_model_rotation = model_angles != Vec3f(0,0,0);
    if ( do_model_rotation ) {
        model_rotation.RotateZYX( model_angles );
    }

    std::vector<Vec3f> verts(3);
    for ( std::size_t m = 0; m < model.NumMeshes(); ++m ) {
        const Mesh* mesh=model.GetMesh(m);
        
        // must find closest face
        for ( auto const& face : mesh->faces ) {
            if ( face.vert.Num() > verts.size() )
                verts.resize( face.vert.Num() * 2 );

            std::size_t v=0;
            for ( auto const& vert : face.vert ) {
                if ( do_model_rotation ) {
                    verts[v] = mesh->verts[ vert.vertIndex ].co * model_rotation;
                } else {
                    verts[v] = mesh->verts[ vert.vertIndex ].co;
                }
                verts[v] += model_origin;
                ++v;
            }

            if ( Sphere::ToPoly( sphere, verts.data(), verts.size() ) ) {
                return true;
            }
        }
    }

    return false;
}

bool Sphere::Intersects( CollisionReport3D& report, const Sphere& sphere, const Line3f& line ) {
    return Line3f::Intersects( report, line, sphere );
}

bool Sphere::Intersects( const Sphere& sphere, const SemiCylinder& semiCyl ) {
    return Collision3D::TestSemiCylinder::Intersects( semiCyl, sphere );
}
        
bool Sphere::ToPlane( const Sphere& sphere, const Plane3f& plane, float* signed_dist_out ) {
    return Sphere::ToPlane( sphere, plane.GetPoint(), plane.GetNormal(), signed_dist_out );
}

bool Sphere::SweepToPlane( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& plane_vert1, const Vec3f& plane_vert2, const Vec3f& plane_vert3 ) {
    // report.norm is correct here
    report.norm = Plane3f::GetNormal( plane_vert1, plane_vert2, plane_vert3 );
    return Sphere::SweepToPlane( report, sphere, sphere_dest, plane_vert1, report.norm );
}

bool Sphere::SweepToLineSeg( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& line_start, const Vec3f& line_end ) {
    return Sphere::SweepToLine( report, sphere, sphere_dest, LineTypeT::SEG, line_start, line_end );
}

bool Sphere::SweepToRay( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Vec3f& line_start, const Vec3f& line_end ) {
    return Sphere::SweepToLine( report, sphere, sphere_dest, LineTypeT::RAY, line_start, line_end );
}

bool Sphere::SweepTo( CollisionReport3D& report, const Sphere& sphere, const Vec3f& sphere_dest, const Line3f& line ) {
    return Sphere::SweepToLine( report, sphere, sphere_dest, line.line_type, line.vert[0], line.vert[1] );
}

bool Sphere::Intersects( const Sphere& sphere, const AABox3D& aabox, Vec3f* norm_out ) {
    return Sphere::DistToAABox3D(sphere, aabox, norm_out ) < EP;
}

bool Sphere::BeforePlane( const Sphere& sphere, const Vec3f &A, const Vec3f& B, const Vec3f& C ) {
    return Collision3D::TestPoint::BeforePlane( sphere.origin, A, B, C, sphere.radius );
}

bool Sphere::BeforePlane( const Sphere& sphere, const Plane3f& plane ) {
    return Collision3D::TestPoint::BeforePlane( sphere.origin, plane, sphere.radius );
}

bool Sphere::BeforePlane( const Sphere& sphere, const Vec3f& point_on_plane, const Vec3f& plane_normal ) {
    return Collision3D::TestPoint::BeforePlane( sphere.origin, point_on_plane, plane_normal, sphere.radius );
}

bool Sphere::BehindPlane( const Sphere& sphere, const Vec3f &A, const Vec3f& B, const Vec3f& C ) {
    return Collision3D::TestPoint::BehindPlane( sphere.origin, A, B, C, sphere.radius );
}

bool Sphere::BehindPlane( const Sphere& sphere, const Plane3f& plane ) {
    return Collision3D::TestPoint::BehindPlane( sphere.origin, plane, sphere.radius );
}

bool Sphere::BehindPlane( const Sphere& sphere, const Vec3f& point_on_plane, const Vec3f& plane_normal ) {
    return Collision3D::TestPoint::BehindPlane( sphere.origin, point_on_plane, plane_normal, sphere.radius );
}

bool Sphere::Intersects( const Sphere& sphere, const Plane3f& plane ) {
    return ToPlane(sphere, plane.GetPoint(), plane.GetNormal() );
}

bool Sphere::Intersects( const Sphere& sphere, const Polygon3D& polygon ) {
    return ToPoly(sphere, polygon.verts.data(), polygon.verts.size() );
}

bool Sphere::Intersects( const Sphere& sphere, const Line3f& line ) {
    return Line3f::Intersects( line, sphere );
}

Vec3f Sphere::GetFarthestVert( const Vec3f& normalized_dir ) const {
    return GetFarthestVert( normalized_dir, origin, radius );
}

Vec3f Sphere::GetFarthestVert( const Vec3f& normalized_dir, const Vec3f& origin, const float radius ) {
    return origin + normalized_dir * radius;
}
