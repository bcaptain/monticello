// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_POINT_H_
#define SRC_COLLISION_3D_POINT_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Line3f;
class Line2f;

typedef Uint8 LineTypeT_BaseType;
enum class LineTypeT : LineTypeT_BaseType;

template< typename TYPE >
class CollisionReportT;

class Sphere;
class CollisionReport3D;

// all windings are clockwise, as typical

namespace Collision3D {
    namespace TestPoint {
        
        bool Intersects( const Vec3f& point, const Sphere& sphere );
        bool Intersects( const Vec3f& point, const Line3f& line, const float epsilon = EP );
        bool Intersects( const Vec3f& point, const Box3D& box );
        bool Intersects( const Vec3f& point, const AABox3D& aabox );
        bool Intersects( const Vec3f& point, const Cylinder& cyl );
        bool Intersects( const Vec3f& point, const Capsule& cyl );
        bool Intersects( const Vec3f& point1, const Vec3f& point2 );
        
        bool ToLine( const Vec3f& point, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const float epsilon = EP );
        bool ToLineSeg( const Vec3f& point, const Vec3f& line_start, const Vec3f& line_end, const float epsilon = EP );
        
        bool OnPlane( const Vec3f& point, const Vec3f &point_on_plane, const Vec3f& plane_normal, const float epsilon = EP );
        bool OnPlane( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C, const float epsilon = EP );
        bool OnPlane( const Vec3f& point, const Plane3f& plane, const float epsilon = EP );

        bool BeforePlane( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C, const float epsilon = EP );
        bool BeforePlane( const Vec3f& point, const Plane3f& plane, const float epsilon = EP );
        bool BeforePlane( const Vec3f& point, const Vec3f& point_on_plane, const Vec3f& plane_normal, const float epsilon = EP );

        bool BehindPlane( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C, const float epsilon = EP );
        bool BehindPlane( const Vec3f& point, const Plane3f& plane, const float epsilon = EP );
        bool BehindPlane( const Vec3f& point, const Vec3f& point_on_plane, const Vec3f& plane_normal, const float epsilon = EP );

        bool SweepToSphere( CollisionReport3D& report, const Vec3f& point, const Vec3f& point_dest, const Sphere& sphere );

        bool ToPoly( const Vec3f& point, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& unnormalized_plane_normal, const float epsilon = EP );
        bool ToPoly( const Vec3f& point, const Vec3f* clockwise_verts, const std::size_t vert_count, const float epsilon = EP );
        bool ToTri( const Vec3f& point, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC, const Vec3f& unnormalized_plane_normal, const float epsilon = EP );
        bool ToTri( const Vec3f& point, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC, const float epsilon = EP );
        bool ToQuad( const Vec3f& point, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD, const Vec3f& unnormalized_plane_normal, const float epsilon = EP );
        bool ToQuad( const Vec3f& point, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD, const float epsilon = EP );

        
        bool AboveAABox3D( const Vec3f& point, const AABox3D& aabox );
        bool BelowAABox3D( const Vec3f& point, const AABox3D& aabox );
        bool LeftOfAABox3D( const Vec3f& point, const AABox3D& aabox );
        bool RightOfAABox3D( const Vec3f& point, const AABox3D& aabox );
        bool BehindAABox3D( const Vec3f& point, const AABox3D& aabox );
        bool BeforeAABox3D( const Vec3f& point, const AABox3D& aabox );
        
        float DistToAABox3D( const Vec3f& point, const AABox3D& aabox, Vec3f* norm_out = nullptr );
        
        float SquaredDistToAABox3D( const Vec3f& point, const AABox3D& aabox, Vec3f* norm_out = nullptr ); //!< faster than ToAABox3D because it doesn't call sqrt
        
        bool ToAABox3D( const Vec3f& point, const AABox3D& aabox ); //!< implemented in terms of SquaredDistToAABox, and less efficient due to call to sqrtf
        
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const AABox3D& aabox );
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Box3D& aabox );
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Sphere& sphere );
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Cylinder& cyl );
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Capsule& cap );
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Line3f& line );
        bool SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Vec3f& _point );
            
        bool SweepToModel( CollisionReport3D& report, const Vec3f& fromPoint, const Vec3f& toPoint, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );
        bool ToModel( const Vec3f& point, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );
        
        void UnitTest( void );
        void UnitTest_DistToAABox3D( void );
        void UnitTest_ToAABox3D( void );
        void UnitTest_Line( void );
        void UnitTest_LineSeg( void );
        void UnitTest_Sphere( void );
        void UnitTest_CoplanarPoly( void );
        void UnitTest_InsideAABox3D( void );
        void UnitTest_Plane( void );
    }
}

#endif // SRC_COLLISION_3D_POINT_H_
