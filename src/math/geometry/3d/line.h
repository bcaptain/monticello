// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_LINE_H_
#define SRC_COLLISION_3D_LINE_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class CollisionReport3D;
class Sphere;
class Model;
class Polygon3D;

typedef Uint8 ModelCollisionT_BaseType;
enum class ModelCollisionT : ModelCollisionT_BaseType {
    ANY_POLY
    , CLOSEST_POLY
};

typedef Uint8 LineTypeT_BaseType;
enum class LineTypeT : LineTypeT_BaseType {
    LINE,
    SEG,
    RAY
};

template< typename TYPE >
float Line_GetProjectedPoint( const LineTypeT line_type, const TYPE& line_start, const TYPE& line_end, const TYPE& point, TYPE* ipoint_out ) {
    // ( not implemented in terms of ) http://www.gamedev.net/topic/495120-find-closest-point-on-3d-line-from-a-point/

    const TYPE start_to_point = point - line_start;
    const TYPE start_to_end( line_end - line_start );
    float time = start_to_point.Dot(start_to_end) / start_to_end.Dot(start_to_end);

    switch ( line_type ) {
        case LineTypeT::SEG:
            if (time > 1.0f)
                time = 1.0f;
            // seg has same constraints as ray
            FALLTHROUGH;
        case LineTypeT::RAY:
            if (time < 0.0f)
                time = 0.0f;
            FALLTHROUGH;
        case LineTypeT::LINE: FALLTHROUGH;
        default:
            break;

    }

    if ( ipoint_out ) {
        *ipoint_out = start_to_end;
        *ipoint_out *= time;
        *ipoint_out += line_start;
    }

    return time;
}

class Line2f {
public:
    Line2f( void );
    explicit Line2f( const LineTypeT _line_type );
    Line2f( const Line2f& other );
    Line2f( const Vec2f& line_start, const Vec2f& line_end, const LineTypeT _line_type = LineTypeT::SEG );

public:
    Line2f& Set( const Vec2f& line_start, const Vec2f& line_end );
    float Len( void );
    Vec2f GetCenter( void ) const;
    Line3f ToLine3f( void ) const;

    static float GetProjectedPoint( const LineTypeT line_type, const Vec2f &line_start, const Vec2f& line_end, const Vec2f& point, Vec2f* ipoint_out = nullptr ); //!< returns time of the closest point on the line, and sets ipoint_out to the point if not null
    static float TimeToPoint( const LineTypeT line_type, const Vec2f& line_start, const Vec2f& line_end, const Vec2f& point, float *units_out = nullptr ); //!< returns time. calculating units_out uses one sqrtf
    static float GetDistToPoint( const LineTypeT line_type, const Vec2f& line_start, const Vec2f& line_end, const Vec2f& point );
    static float GetDistToPoint( const Line2f& line, const Vec2f& point );
    static Vec2f GetNormal( const Vec2f& line_start, const Vec2f& line_end ); //!< Normal points in the clockwise direction from the line
    Vec2f GetNormal( void ) const; //!< Normal points in the clockwise direction from the line

    static float DistToLine( const Line2f& lineA, const Line2f& lineB );
    static float DistToLine( const Vec2f& lineA_start, const Vec2f& lineA_end, const Vec2f& lineB_start, const Vec2f& lineB_end );

    static float DistToLineSeg( const Vec2f& lineA_start, const Vec2f& lineA_end, const Vec2f& lineB_start, const Vec2f& lineB_end );

    //! does the actual work for all of its overloads
    static float SquaredDistToLine( const Vec2f& lineA_start, const Vec2f& lineA_end, const Line2f& lineA_type, const Vec2f& lineB_start, const Vec2f& lineB_end, const LineTypeT lineB_type = LineTypeT::LINE );
    static float DistToLine( const Vec2f& lineA_start, const Vec2f& lineA_end, const Line2f& lineA_type, const Vec2f& lineB_start, const Vec2f& lineB_end, const LineTypeT lineB_type = LineTypeT::LINE );

public:
    Line2f& operator=( const Line2f& other );
    Line2f operator+( const Vec2f& vec ) const;
    Line2f operator-( const Vec2f& vec ) const;
    Line2f operator/( const float val ) const;
    Line2f operator*( const float val ) const;

    Line2f& operator+=( const Vec2f& vec );
    Line2f& operator-=( const Vec2f& vec );
    Line2f& operator/=( const float val );
    Line2f& operator*=( const float val );

public:
    Vec2f vert[2];
    LineTypeT line_type;
};

class Line3f {
public:
    Line3f( void );
    explicit Line3f( const LineTypeT _line_type );
    Line3f( const Line3f& other, const LineTypeT _line_type );
    Line3f( const Line3f& other );
    Line3f( const Vec3f& line_start, const Vec3f& line_end, const LineTypeT _line_type = LineTypeT::SEG );

public:
    Line3f& Set( const Vec3f& line_start, const Vec3f& line_end );
    float Len( void );
    Vec3f GetCenter( void ) const;
    Line3f ToLine3f( void ) const;

    static float GetProjectedPoint( const LineTypeT line_type, const Vec3f &line_start, const Vec3f& line_end, const Vec3f& point, Vec3f* ipoint_out = nullptr ); //!< returns time of the closest point on the line, and sets ipoint_out to the point if not null
    static float TimeToPoint( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& point, float *units_out = nullptr ); //!< returns time. calculating units_out uses one sqrtf
    static float GetDistToPoint( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& point );
    static float GetDistToPoint( const Line3f& line, const Vec3f& point );
    static Vec3f GetNormal( const Vec3f& line_start, const Vec3f& line_end ); //!< Normal points in the clockwise direction from the line
    Vec3f GetNormal( void ) const; //!< Normal points in the clockwise direction from the line

    static float DistToLine( const Line3f& lineA, const Line3f& lineB );
    static float DistToLine( const Vec3f& lineA_start, const Vec3f& lineA_end, const Vec3f& lineB_start, const Vec3f& lineB_end );

    static float DistToLineSeg( const Vec3f& lineA_start, const Vec3f& lineA_end, const Vec3f& lineB_start, const Vec3f& lineB_end );

    //! does the actual work for all of its overloads
    static float SquaredDistToLine( const Vec3f& lineA_start, const Vec3f& lineA_end, const LineTypeT lineA_type, const Vec3f& lineB_start, const Vec3f& lineB_end, const LineTypeT lineB_type = LineTypeT::LINE );
    static float DistToLine( const Vec3f& lineA_start, const Vec3f& lineA_end, const LineTypeT lineA_type, const Vec3f& lineB_start, const Vec3f& lineB_end, const LineTypeT lineB_type = LineTypeT::LINE );

public:
    Line3f& operator=( const Line3f& other );
    Line3f operator+( const Vec3f& vec ) const;
    Line3f operator-( const Vec3f& vec ) const;
    Line3f operator/( const float val ) const;
    Line3f operator*( const float val ) const;

    Line3f& operator+=( const Vec3f& vec );
    Line3f& operator-=( const Vec3f& vec );
    Line3f& operator/=( const float val );
    Line3f& operator*=( const float val );

    Line3f& operator*=( const Mat3f& mat );
    Line3f operator*( const Mat3f& mat ) const;
    Line3f& operator*=( const Mat4f& mat );
    Line3f operator*( const Mat4f& mat ) const;
    Line3f& operator*=( const Quat& mat );
    Line3f operator*( const Quat& mat ) const;
        
public:
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Vec3f& point );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Box3D& box );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Sphere& sphere );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Line3f& other );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Plane3f& plane );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Polygon3D& polygon );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const AABox3D& aabox );
    static bool Intersects( CollisionReport3D& report, const Line3f& line, const Capsule& cap );
    static bool Intersects( CollisionReport3D& report_unused, const Line3f& line, const Cylinder& cyl );
    
    static bool Intersects( const Line3f& line, const Sphere& sphere );
    static bool Intersects( const Line3f& line, const AABox3D& aabox );
    static bool Intersects( const Line3f& line, const Box3D& box );
    static bool Intersects( const Line3f& line, const Capsule& capsule );
    static bool Intersects( const Line3f& line, const Cylinder& cyl );
    
    static bool ToCapsule( const Line3f& line, const Capsule& capsule );
    static bool ToCapsule( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Capsule& capsule );
    
    static bool ToPoly( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& poly_normal );
    static bool ToPoly( CollisionReport3D& report, const Line3f& line, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& poly_normal );
    static bool ToPoly( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f* clockwise_verts, const std::size_t vert_count ); //!< Less efficient overload
    static bool ToPoly( CollisionReport3D& report, const Line3f& line, const Vec3f* clockwise_verts, const std::size_t vert_count ); //!< Less efficient overload
    static bool ToTri( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC, const Vec3f& tri_normal ); //!< Less efficient overload
    static bool ToTri( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC ); //!< Less efficient overload
    static bool ToQuad( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD, const Vec3f& tri_normal ); //!< Less efficient overload
    static bool ToQuad( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD ); //!< Less efficient overload
    
    static bool ToPlane( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& planeA, const Vec3f& plane_norm );
    static bool ToPlane( CollisionReport3D& report, const Line3f& line, const Vec3f& planeA, const Vec3f& plane_norm );
    static bool ToPlane( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& planeA, const Vec3f& plane_norm );
    static bool ToPlane( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC ); //!< Less efficient overload
    static bool ToPlane( CollisionReport3D& report, const Line3f& line, const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC ); //!< Less efficient overload
    static bool ToPlane( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& clockwise_planeA, const Vec3f& clockwise_planeB, const Vec3f& clockwise_planeC ); //!< Less efficient overload

    static bool ToSphere( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Sphere& sphere );

    static bool ToLine( CollisionReport3D& report, const LineTypeT lineA_type, const Vec3f& lineA_start, const Vec3f& lineA_end, const LineTypeT lineB_type, const Vec3f& lineB_start, const Vec3f& lineB_end );
    
    static bool ToAABox3D( CollisionReport3D& report, const LineTypeT lineType, const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox );
    static bool ToAABox3D( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox );
    static bool ToAABox3D( const LineTypeT lineType, const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox );
    static bool ToAABox3D( const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox );

    static bool ToBox3D( CollisionReport3D& report, const LineTypeT lineType, const Vec3f& line_start, const Vec3f& line_end, const Box3D& box );
    static bool ToBox3D( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const Box3D& box );
    
    static bool SweepToModel( CollisionReport3D& report, const Line3f& line, const Vec3f& vel_ignored, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );
    static bool ToModel( const Line3f& line, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );
    
#ifdef CLIB_UNIT_TEST
    static void UnitTest( void );
    static void UnitTest_ToCapsule( void );
    static void UnitTest_CoplanarPoly( void );
    static void UnitTest_Plane( void );
    static void UnitTest_ToSphere( void );
    static void UnitTest_ToLine( void );
    static void LineSeg3f_UnitTest_LineDist();
    static void LineSeg2f_UnitTest_LineDist();
    static void UnitTest_Seg_Plane();
    static void UnitTest_Seg_CoplanarPoly();
    static void UnitTest_Seg_AABox_and_Box3D();
    static void UnitTest_Seg_ToCapsule();
    static void UnitTest_Seg_ToBox3D();
#endif // CLIB_UNIT_TEST

public:
    Vec3f vert[2];
    LineTypeT line_type;
};

#endif // SRC_COLLISION_3D_LINE_H_
