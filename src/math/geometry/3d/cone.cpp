// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./cone.h"

Cone3D::Cone3D( void )
    : origin()
    , normal()
    , length()
    , radius()
{}

Cone3D::Cone3D( const Vec3f& _origin, const Vec3f& _normal, const float _length, const float _radius )
    : origin( _origin )
    , normal( _normal )
    , length( _length )
    , radius( _radius )
{}

Vec3f Cone3D::GetTip( void ) const {
    return normal * length + origin;
}

Vec3f Cone3D::GetFarthestVert( const Vec3f& normalized_dir ) const {
    const Vec3f cone_tip = GetTip();
    const Vec3f base( Sphere::GetFarthestVert( normalized_dir, origin, radius ) );
    const Vec3f point( Sphere::GetFarthestVert( normalized_dir, cone_tip, radius ) );

    if ( base.Dot( normalized_dir ) > point.Dot( normalized_dir ) ) {
        // ** if the distance of the point is before the base of the cylinder, it exists on the sphere and we need to move it onto the cylinder
        const float dist = Plane3f::GetDistToPoint( base, origin, normal );
        if ( dist < 0 ) {
            const Vec3f projectedToBottom = base + normal * fabsf( dist );
            const Vec3f dir_from_center = (projectedToBottom - origin).GetNormalized();
            const Vec3f moved_to_edge = origin + dir_from_center * radius;
            return moved_to_edge;
        }
        return base;
    } else {
        return point;
    }
}

float Cone3D::GetAngle( void ) const {
    const float hypoteneuse = sqrtf( radius*radius + length*length );
    const float radians = radius / hypoteneuse; //sine=opposite/adjacent
    const float degrees = radians * RAD_TO_DEG_F;
    return degrees*2;
}

#ifdef CLIB_UNIT_TEST
    void Cone3D::UnitTest( void ) {
        UnitTest_GetAngle();
    }

    void Cone3D::UnitTest_GetAngle( void ) {
        // using a triangle 3/4/5 (pthagorean tripple) to test.
        ASSERT( Maths::Approxf( Cone3D(Vec3f(0,0,0), Vec3f(0,1,0), 4, 3 ).GetAngle(), 68.75493f ) );
    }

#endif // CLIB_UNIT_TEST

bool Cone3D::Intersects( const Cone3D& cone, const Plane3f& plane ) {
    return ToPlane( cone, plane.GetPoint(), plane.GetNormal() );
}

bool Cone3D::ToPlane( const Cone3D& cone, const Vec3f& planeA, const Vec3f& plane_normal ) {
    return Maths::Approxf( Plane3f::GetDistToCone3D( cone, planeA, plane_normal ), 0 );
    
    /* was trying something
    const Cylinder cyl( cone.origin, cone.normal, cone.radius, cone.length );
    const float dist = Cylinder::GetDistToPlane( cyl, planeA, normal );

    if ( dist > cone.radius )
        return false;
    
    // if line and plane are parallel, return true
    // if cone.GetEnd() 
    const Vec3f 
    const Vec3f cone_end = cone.GetEnd();
    float dist = Plane3f::GetDistToLineSeg( cone.origin, cone.end, planeA, normal );

    float Plane3f::GetDistToLineSeg( const Line3f& lineseg, const Vec3f& planeA, const Vec3f& plane_normal ) {


    CollisionReport3D report( CollisionCalcsT::Intersection );
    report.
    Polygon3D::Intersects( report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& poly_normal ) {
    Collision3D::TestLineSeg::ToPoly( report, box3d[BOX_FAR_LEFT_TOP_INDEX], box3d[BOX_FAR_RIGHT_TOP_INDEX], clockwise_verts, vert_count )

    ASSERT(false);
    */
}

/*
Cone3D::Intersects( const Cone3D& cone, const AABox3D& aabox ) {
    ASSERT(false);
}

Cone3D::Intersects( const Cone3D& cone, const Box3D& aabox ) {
    
    Capsule::Intersects()
    Vec3f closest_point_to_center_line = Collision3D::TestLineSeg::`
    ASSERT(false);
}
*/

