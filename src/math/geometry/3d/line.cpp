// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include <math.h>
#include "./line.h"
#include "../../collision/collision.h"
#include "../../../rendering/models/model.h"

Line2f::Line2f( void )
    : vert{ Vec2f(0,0), Vec2f(0,0) }
    , line_type( LineTypeT::SEG )
{ }

Line2f::Line2f( const Line2f& other )
    : vert{other.vert[0], other.vert[1]}
    , line_type( other.line_type )
    {
}

Line2f& Line2f::operator=( const Line2f& other ) {
    vert[0] = other.vert[0];
    vert[1] = other.vert[1];
    line_type = other.line_type;
    return *this;
}

Line2f::Line2f( const LineTypeT _line_type )
    : vert()
    , line_type( _line_type )
    {
}

Line2f::Line2f( const Vec2f& line_start, const Vec2f& line_end, const LineTypeT _line_type )
    : vert{line_start, line_end}
    , line_type( _line_type )
    {
}

Line2f& Line2f::Set( const Vec2f& line_start, const Vec2f& line_end ) {
    vert[0] = line_start;
    vert[1] = line_end;
    return *this;
}

Vec2f Line2f::GetNormal( const Vec2f& line_start, const Vec2f& line_end ) {
    Vec2f ret( line_end.y - line_start.y, -( line_end.x - line_start.x ) ); // negate the y to make the normal point in the clockwise direction from the line
    ret.Normalize();
    return ret;
}

Vec2f Line2f::GetNormal( void ) const {
    return GetNormal( vert[0], vert[1] );
}

Line2f Line2f::operator+( const Vec2f& vec ) const {
    return { vert[0]+vec, vert[1]+vec, line_type };
}

Line2f Line2f::operator-( const Vec2f& vec ) const {
    return { vert[0]-vec, vert[1]-vec, line_type };
}

Line2f Line2f::operator/( const float val ) const {
    return { vert[0]/val, vert[1]/val, line_type };
}

Line2f Line2f::operator*( const float val ) const {
    return { vert[0]*val, vert[1]*val, line_type };
}

Line2f& Line2f::operator+=( const Vec2f& vec ) {
    vert[0]+=vec;
    vert[1]+=vec;
    return *this;
}

Line2f& Line2f::operator-=( const Vec2f& vec ) {
    vert[0]-=vec;
    vert[1]-=vec;
    return *this;
}

Line2f& Line2f::operator/=( const float val ) {
    vert[0]/=val;
    vert[1]/=val;
    return *this;
}

Line2f& Line2f::operator*=( const float val ) {
    vert[0]*=val;
    vert[1]*=val;
    return *this;
}

float Line2f::GetProjectedPoint( const LineTypeT line_type, const Vec2f& line_start, const Vec2f& line_end, const Vec2f& point, Vec2f* ipoint_out ) {
    return Line_GetProjectedPoint( line_type, line_start, line_end, point, ipoint_out );
}

Line3f::Line3f( const Line3f& other, const LineTypeT _line_type )
    : vert{other.vert[0], other.vert[1]}
    , line_type( _line_type )
    {
}

Line3f::Line3f( const Line3f& other )
    : vert{other.vert[0], other.vert[1]}
    , line_type( other.line_type )
    {
}

Line3f& Line3f::operator=( const Line3f& other ) {
    vert[0] = other.vert[0];
    vert[1] = other.vert[1];
    line_type = other.line_type;
    return *this;
}

Line3f::Line3f( const LineTypeT _line_type )
    : vert()
    , line_type( _line_type )
    {
}

Line3f::Line3f( const Vec3f& line_start, const Vec3f& line_end, const LineTypeT _line_type )
    : vert{line_start, line_end}
    , line_type( _line_type )
    {
}

Line3f& Line3f::Set( const Vec3f& line_start, const Vec3f& line_end ) {
    vert[0] = line_start;
    vert[1] = line_end;
    return *this;
}

float Line3f::SquaredDistToLine( const Vec3f& lineA_start, const Vec3f& lineA_end, const LineTypeT lineA_type, const Vec3f& lineB_start, const Vec3f& lineB_end, const LineTypeT lineB_type ) {
    if ( lineA_type == LineTypeT::RAY ) {
        ERR("Not Implemented(lineA_type != LineTypeT::RAY).\n"); // not implementedv //toimplement
        return 999999;
    }
    if ( lineA_type != lineB_type ) {
        ERR("SquaredDistToLine(lineA_type == lineB_type): Not Implemented.\n"); // only this is imlpemented for now
        return 999999;
    }

    const Vec3f P( lineA_start - lineA_end );
    const Vec3f Q( lineB_start - lineB_end );
    const float PdotP( P.Dot(P) );
    const float PdotQ( P.Dot(Q) );
    const float QdotQ( Q.Dot(Q) );

    const Vec3f endToEnd( lineA_end - lineB_end );
    const float PdotEndToEnd( P.Dot(endToEnd) );
    const float QdotEndToEnd( Q.Dot(endToEnd) );

    const float D(PdotP*QdotQ - PdotQ*PdotQ);
    float s_denom(D);
    float t_denom(D);
    float t_numerator;
    float s_numerator;

    const bool lines_parallel = D < EP;
    if ( lines_parallel ) {
        t_numerator = QdotEndToEnd;
        t_denom = QdotQ;
        s_numerator = 0;
        s_denom = 1;
    } else {
        // ** check infinite lines
        s_numerator = PdotQ*QdotEndToEnd - QdotQ*PdotEndToEnd;

        if ( lineB_type == LineTypeT::SEG ) {
            const bool is_sEquale0_edge_visible = s_numerator < 0;
            if ( !is_sEquale0_edge_visible ) {
                const bool is_sEquale1_edge_visible = s_numerator > s_denom;
                if ( !is_sEquale1_edge_visible ) {
//                    if ( lineA_type == LineTypeT::SEG ) {
                        t_numerator = (PdotP*QdotEndToEnd - PdotQ*PdotEndToEnd);
//                    }
                } else {
                    if ( lineA_type == LineTypeT::SEG ) {
                        s_numerator = s_denom;
                    }
                    t_numerator = QdotEndToEnd + PdotQ;
                    t_denom = QdotQ;
                }
            } else {
                if ( lineA_type == LineTypeT::SEG ) {
                    s_numerator = 0;
                }
                t_numerator = QdotEndToEnd;
                t_denom = QdotQ;
            }
        } else {
            //t_numerator = QdotEndToEnd;
            t_numerator = QdotEndToEnd + PdotQ;
            t_denom = QdotQ;
            //s_numerator = 0;
        }
    }

    if ( lineA_type == LineTypeT::SEG ) {
        const bool is_tEquale0_edge_visible = t_numerator < 0;
        if ( !is_tEquale0_edge_visible ) {
            const bool is_tEquale1_edge_visible = t_numerator > t_denom;
            if ( is_tEquale1_edge_visible ) {

                if ( lineB_type == LineTypeT::SEG ) {
                    t_numerator = t_denom;
                }
                if ( -PdotEndToEnd + PdotQ < 0) {
                    s_numerator = 0;
                } else if ( -PdotEndToEnd + PdotQ > PdotP ) {
                    s_numerator = s_denom;
                } else {
                    s_numerator = -PdotEndToEnd + PdotQ;
                    s_denom = PdotP;
                }
            }
        } else {

            if ( lineB_type == LineTypeT::SEG ) {
                t_numerator = 0;
            }
            if (-PdotEndToEnd < 0) {
                s_numerator = 0;
            } else if (-PdotEndToEnd > PdotP) {
                s_numerator = s_denom;
            } else {
                s_numerator = -PdotEndToEnd;
                s_denom = PdotP;
            }
        }
    } else {
        s_numerator = -PdotEndToEnd + PdotQ;
        s_denom = PdotP;
        /*
        t_numerator = t_denom;
        s_numerator = -PdotEndToEnd;
        s_denom = PdotP;
        */
    }

    const float tc = std::fabs(t_numerator) < EP ? 0 : t_numerator / t_denom;
    const float sc = std::fabs(s_numerator) < EP ? 0 : s_numerator / s_denom;

    const Vec3f Qts(Q * tc);
    const Vec3f Psc(P * sc);
    const Vec3f dP( endToEnd + Psc );

    return Vec3f::SquaredLen( dP - Qts );
}

float Line3f::DistToLine( const Vec3f& lineA_start, const Vec3f& lineA_end, const LineTypeT lineA_type, const Vec3f& lineB_start, const Vec3f& lineB_end, const LineTypeT lineB_type ) {
    const float dist_sq = SquaredDistToLine( lineA_start, lineA_end, lineA_type, lineB_start, lineB_end, lineB_type );
    return sqrtf( dist_sq );
}

Line3f& Line3f::operator*=( const Mat3f& mat ) {
    return operator=( *this * mat );
}

Line3f Line3f::operator*( const Mat3f& mat ) const {
    return Line3f(
        vert[0] * mat
        , vert[1] * mat
    );
}

Line3f& Line3f::operator*=( const Mat4f& mat ) {
    return operator=( *this * mat );
}

Line3f Line3f::operator*( const Mat4f& mat ) const {
    Line3f ret(
        vert[0] * *static_cast<const Mat3f*>( &mat )
        , vert[1] * *static_cast<const Mat3f*>( &mat )
    );

    ret.vert[0].x += mat.data[12];
    ret.vert[0].y += mat.data[13];
    ret.vert[0].z += mat.data[14];
    ret.vert[1].x += mat.data[12];
    ret.vert[1].y += mat.data[13];
    ret.vert[1].z += mat.data[14];

    return ret;
}

Line3f& Line3f::operator*=( const Quat& quat ) {
    return operator=( *this * quat );
}

Line3f Line3f::operator*( const Quat& quat ) const {
    return Line3f(
        vert[0] * quat
        , vert[1] * quat
    );
}

Line3f Line3f::operator+( const Vec3f& vec ) const {
    return { vert[0]+vec, vert[1]+vec, line_type };
}

Line3f Line3f::operator-( const Vec3f& vec ) const {
    return { vert[0]-vec, vert[1]-vec, line_type };
}

Line3f Line3f::operator/( const float val ) const {
    return { vert[0]/val, vert[1]/val, line_type };
}

Line3f Line3f::operator*( const float val ) const {
    return { vert[0]*val, vert[1]*val, line_type };
}

Line3f& Line3f::operator+=( const Vec3f& vec ) {
    vert[0]+=vec;
    vert[1]+=vec;
    return *this;
}

Line3f& Line3f::operator-=( const Vec3f& vec ) {
    vert[0]-=vec;
    vert[1]-=vec;
    return *this;
}

Line3f& Line3f::operator/=( const float val ) {
    vert[0]/=val;
    vert[1]/=val;
    return *this;
}

Line3f& Line3f::operator*=( const float val ) {
    vert[0]*=val;
    vert[1]*=val;
    return *this;
}

Line3f::Line3f( void )
    : vert{ Vec3f(0,0,0), Vec3f(0,0,0) }
    , line_type( LineTypeT::SEG )
{ }

float Line3f::TimeToPoint( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& point, float *units_out ) {
    Vec3f closest_point_on_line;
    const float time = GetProjectedPoint( line_type, line_start, line_end, point, units_out == nullptr ? nullptr : &closest_point_on_line );

    if ( units_out != nullptr ) {
        closest_point_on_line -= point;
        *units_out = closest_point_on_line.Len();
    }

    return time;
}

float Line3f::GetDistToPoint( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& point ) {
    float ret;
    TimeToPoint( line_type, line_start, line_end, point, &ret );
    return ret;
}

float Line2f::GetDistToPoint( const LineTypeT line_type, const Vec2f& line_start, const Vec2f& line_end, const Vec2f& point ) {
    if ( line_type != LineTypeT::LINE ) {
        ERR("Line2f::GetDistToPoint: That type of line (%u) is not yet implemented for this function", static_cast<uint>(line_type) );
        return 99999;
    }

    // a point and the line creates a triangle.
    // the cross product of the line against a line made from the point and one end of the line will give us the area of the triangle
    // divide the area of the triangle by the length of the line and this yields the hight (which is also the distance to the point)
    const Vec2f A( line_end - line_start );
    const Vec2f B( line_end - point );
    const float area = A.Cross( B );
    const float height = area / A.Len();
    return -height;
}

bool Line3f::ToBox3D( CollisionReport3D& report, const LineTypeT lineType, const Vec3f& line_start, const Vec3f& line_end, const Box3D& box ) {
    const bool closest_face = ( report.calc != CollisionCalcsT::None );
    
    CollisionReport3D closest(report.calc);
    bool ret = false;
    for ( uint face=0; face<6; ++face ) {
        const std::vector<Vec3f> face_verts( box.GetFaceVerts(face) );
        if ( !Line3f::ToPoly( report, lineType, line_start, line_end, face_verts.data(), 4, box.GetFaceNormal(face) ) )
            continue;

        if ( !closest_face )
            return true;

        if ( !ret || report.time < closest.time )
            closest = report;

        ret = true;
    }

    if ( ret ) {
        report = closest;
        return true;
    } else {
        // check for encapsellation (only need to check one vertice since we checked every other case)
        return Collision3D::TestPoint::Intersects( line_start, box );
    }

    return false;
}

bool Line3f::ToCapsule( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Capsule& capsule ) {
    const float line_dist_sq = Line3f::SquaredDistToLine( line_start, line_end, line_type, capsule.origin, capsule.GetEnd(), LineTypeT::SEG );
    return line_dist_sq <= capsule.radius * capsule.radius;
}

bool Line3f::Intersects( [[maybe_unused]] CollisionReport3D& report_unused, const Line3f& line, const Vec3f& point ) {
    return Collision3D::TestPoint::ToLine( point, line.line_type, line.vert[0], line.vert[1] );
}

bool Line3f::Intersects( [[maybe_unused]] CollisionReport3D& report_unused, const Line3f& line, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, line );
}

bool Line3f::Intersects( [[maybe_unused]] CollisionReport3D& report_unused, const Line3f& line, const Capsule& cap ) {
    return Capsule::Intersects( cap, line );
}

bool Line3f::Intersects( const Line3f& line, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, line );
}

bool Line3f::ToPlane( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    return Line3f::ToPlane( report, line_type, line_start, line_end, planeA, Plane3f::GetNormal( planeA, planeB, planeC ) );
}

bool Line3f::ToPlane( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& planeA, const Vec3f& plane_norm ) {
    // if it's not a valid plane, it doesn't collide.
    if ( plane_norm.SquaredLen() < (1-EP) )
        return false;

    report.norm = plane_norm;
    const Vec3f vec(line_end - line_start );
    const float H = report.norm.Dot(vec);

    // if the line is parallel to the plane...
    if ( fabsf(H) < EP) {

        // if the line is ON the plane...
        report.point = line_start;
        if ( Collision3D::TestPoint::OnPlane( report.point, planeA, plane_norm ) ) {
            // the line is coplanar to the plane
            const Vec3f line_to_point(report.point - line_start);
            report.time = Maths::Divide( line_to_point.SquaredLen(), vec.SquaredLen() );
            return true;
        }

        return false;
    }

    report.time = report.norm.Dot( planeA - line_start );
    report.time /= H;

    switch ( line_type ) {
        case LineTypeT::SEG:
            if ( report.time > 1.0f ) {
                //todo: time
                return false;
            } else {
                FALLTHROUGH;
            }
            // seg has the same constraints as ray
        case LineTypeT::RAY:
            if ( report.time < 0.0f ) {
                //todo: time
                return false;
            } else {
                FALLTHROUGH;
            }
        case LineTypeT::LINE: FALLTHROUGH;
        default:
            break;
    }

    if ( report.ComputeIntersection() ) {
        report.point = vec;
        report.point *= report.time;
        report.point += line_start;
    }

    return true;
}

bool Line3f::ToPoly( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f* clockwise_verts, const std::size_t vert_count ) {
    const Vec3f poly_normal( Plane3f::GetNormal( clockwise_verts ) );
    return Line3f::ToPoly( report, line_type, line_start, line_end, clockwise_verts, vert_count, poly_normal );
}
 

bool Line3f::ToPoly( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& poly_normal ) {
    /*<!
    References:
    Line-Line Distance.pdf - http://mathworld.wolfram.com/Line-LineDistance.html \n
    distance between skew lines.pdf - http://answers.yahoo.com/question/index?qid=20080203185109AALfeGN \n
    line-line distance 2.pdf - http://www.science-bbs.com/121-math/2cdfcf84d14268d7.htm \n
    distance between skew lines 2.pdf - http://www.physicsforums.com/showthread.php?t=13858 \n
    http://www.cs.princeton.edu/courses/archive/fall00/cs426/lectures/raycast/sld016.htm \n
     * 
    **/
    
    ASSERT( vert_count > 2 );
    ASSERT( clockwise_verts );

    // if it's not a valid plane, it doesn't collide.
    if ( poly_normal.SquaredLen() < (1-EP) )
        return false;

    CollisionReport3D tempReport( report.calc | CollisionCalcsT::Intersection );
    tempReport = report;

    // ** must collide with plane
    if ( !Line3f::ToPlane( tempReport, line_type, line_start, line_end, clockwise_verts[0], poly_normal ) )
        return false;

    report = tempReport;

    // ** if collision point is inside the poly, success
    Vec3f norm_bak = report.norm;
    float time = report.time;
    bool ret = Collision3D::TestPoint::ToPoly( report.point, clockwise_verts, vert_count );

    if ( ret ) {
        report.time = time;
        report.norm = norm_bak;
        return true;
    }

    // ** if line is coplanar to poly, we have to check our line against all of the edges since Point::ToPoly only checked one point on our line
    const Vec3f pnorm( Plane3f::GetNormal( clockwise_verts[0], line_start, line_end ) );
    const float angle_between_normals = fabsf( pnorm.Dot( report.norm ) );

    if ( ! Maths::Approxf( fabsf( angle_between_normals ), 1.0f ) )
        return false;

    Vec3f closest_point{};
    float closest_dist{};

    for ( std::size_t a=vert_count-1,b=0; b < vert_count; a=b++ ) {
        if ( Line3f::ToLine( report, line_type, line_start, line_end, LineTypeT::SEG, clockwise_verts[a], clockwise_verts[b] ) ) {
            if ( !ret ) { // take the first distance we find, and from then on take the closest distance to zero that is positive and lower than what we already have
                closest_point = report.point;
                closest_dist = report.time;
                ret = true;
            }
        }
    }

    if ( ret ) {
        report.time = closest_dist;
        report.point = closest_point;
    }

    return ret;
}
#include "../../../rendering/renderer.h"
bool Line3f::ToSphere( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Sphere& sphere ) {
    if ( report.calc != CollisionCalcsT::None )
        return Collision3D::TestPoint::SweepTo( report, line_start, line_end, sphere );

    Vec3f closest_point_on_velcoity_to_point;
    Line3f::GetProjectedPoint( line_type, line_start, line_end, sphere.origin, &closest_point_on_velcoity_to_point ); // we must calculate the point in order to determine if there is distance between the line and sphere

    const float radius_sq = sphere.radius * sphere.radius;
    return Vec3f::SquaredLen( sphere.origin - closest_point_on_velcoity_to_point ) <= radius_sq;
}

bool Line3f::ToTri( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC ) {
    const std::size_t vert_count = 3;
    Vec3f verts[vert_count]{triA, triB, triC};
    return ToPoly( report, line_type, line_start, line_end, verts, vert_count );
}

bool Line3f::ToTri( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC, const Vec3f& plane_normal ) {
    const std::size_t vert_count = 3;
    Vec3f verts[vert_count]{triA, triB, triC};
    return ToPoly( report, line_type, line_start, line_end, verts, vert_count, plane_normal );
}

bool Line3f::ToQuad( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD ) {
    const std::size_t vert_count = 4;
    Vec3f verts[vert_count]{quadA, quadB, quadC, quadD};
    return ToPoly( report, line_type, line_start, line_end, verts, vert_count );
}

bool Line3f::ToQuad( CollisionReport3D& report, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD, const Vec3f& plane_normal ) {
    const std::size_t vert_count = 4;
    Vec3f verts[vert_count]{quadA, quadB, quadC, quadD};
    return ToPoly( report, line_type, line_start, line_end, verts, vert_count, plane_normal );
}

//todo: get rid of this and implement in terms of Line::SquaredDistToLine
// will not calculate normal
bool Line3f::ToLine( CollisionReport3D& report, const LineTypeT lineA_type, const Vec3f& lineA_start, const Vec3f& lineA_end,const LineTypeT lineB_type,  const Vec3f& lineB_start, const Vec3f& lineB_end ) {

    // ** move lines into their local spaces with their start points at 0,0,0, respectively
    Vec3f a( lineA_end );
    a -= lineA_start;

    Vec3f b( lineB_end );
    b -= lineB_start;

    // ** do some magic

    Vec3f c( lineB_start );
    c -= lineA_start;

    const Vec3f U = a.Cross( b );
    const Vec3f V = c.Cross( b );

    const float ULen = U.Len();
    const float VLen = V.Len();

    // ** if either of the cross products we calculated have zero length, then lines are parallel and possibly coinciding.

    Uint8 zeroLenCount = 0;
    if ( Maths::Approxf( ULen, 0.0f ) )
        ++zeroLenCount;

    if ( Maths::Approxf( VLen, 0.0f ) )
        ++zeroLenCount;

    switch ( zeroLenCount ) {
        case 2: // Lines coincide
            // ** this case is a bit fugly but is the best way I can think to make everything work.
            if (Collision3D::TestPoint::ToLineSeg( lineA_start, lineB_start, lineB_end )) {
                report.point = lineA_start;
                return true;
            }

            if (Collision3D::TestPoint::ToLineSeg( lineA_end, lineB_start, lineB_end )) {
                report.point = lineA_end;
                return true;
            }

            if (Collision3D::TestPoint::ToLineSeg( lineB_start, lineA_start, lineA_end )) {
                report.point = lineB_start;
                return true;
            }

            if (Collision3D::TestPoint::ToLineSeg( lineB_end, lineA_start, lineA_end )) {
                report.point = lineB_end;
                return true;
            }

            return false;
        case 1: // Lines are parallel but do not touch
            // todo: distance between them
            return false;
        case 0:
            report.time = V.Dot(U) / powf(ULen, 2);
            if ( lineA_type != LineTypeT::LINE && report.time < 0.0f ) { // intersection points before the start of segs or rays aren't accepted
                // todo: distance between them
                return false;
            }
            if ( lineA_type == LineTypeT::SEG && report.time > 1.0f ) { // only rays and lines will accept intersections after the end point
                // todo: distance between them
                return false;
            }

            report.point = a;
            report.point *= report.time;
            report.point += lineA_start;

            // if the line isn't "open", we have work to do
            if ( lineB_type != LineTypeT::LINE ) {
                float lineB_dist;

                // ** use the collision point we just found to crank out lineB_dist
                // we sent LineTypeT::LINE because we want the distance regardless if it is not on the ray or seg
                // todo: there is probably a faster way of doing this
                lineB_dist = Line3f::TimeToPoint( LineTypeT::LINE, lineB_start, lineB_end, report.point );

                if ( lineB_dist < 0.0f ) { // intersection points before the start of segs or rays aren't accepted
                    // todo: distance between them
                    return false;
                }

                if ( lineB_type == LineTypeT::SEG && lineB_dist > 1.0f ) { // only rays and lines will accept intersections after the end point
                    // todo: distance between them
                    return false;
                }
            }

            return true;
        default:
            ASSERT_MSG( false, "Shouldn't be here." );
            return false;
    }
}

bool Line3f::ToModel( const Line3f& line, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
    CollisionReport3D report;
    return Collision3D::TestPoint::SweepToModel( report, line.vert[0], line.vert[1], model, model_angles, model_origin, ModelCollisionT::ANY_POLY );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
bool Line3f::SweepToModel( CollisionReport3D& report, const Line3f& line, const Vec3f& vel_ignored, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {
    ERR( "Line3f::SweepToModel: Not Implemented.\n" ); 
    return false;
}
#pragma GCC diagnostic pop

bool Line3f::ToAABox3D( const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox ) {
    CollisionReport3D report;
    return Line3f::ToAABox3D( report, LineTypeT::LINE, line_start, line_end, aabox );
}

bool Line3f::ToAABox3D( const LineTypeT lineType, const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox ) {
    CollisionReport3D report;
    return Line3f::ToAABox3D( report, lineType, line_start, line_end, aabox );
}

bool Line3f::Intersects( const Line3f& line, const AABox3D& aabox ) {
    CollisionReport3D report;
    return Line3f::ToAABox3D( report, line.line_type, line.vert[0], line.vert[1], aabox );
}

bool Line3f::ToAABox3D( CollisionReport3D& report, const LineTypeT lineType, const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox ) {
    // Cyrus-Beck clipping algorithm
    // Also relevant/similar to Liang-Barsky algorithm
    
    report.norm = line_end - line_start;
    const float line_len = report.norm.Len();
    report.norm /= line_len;
    
    float tmin=0;
    float tmax = MAX_FLOAT;
    
    for ( uint i=0; i<3; ++i ) {

        if ( Maths::Approxf( report.norm[i], 0 ) ) { // paralell
        
            if ( line_start[i] < aabox.min[i] || line_start[i] > aabox.max[i] ) {
                return false;
            }
            
            continue;
        }
        
        // ** get intersection point of both opposing sides
        const float component_inverse = 1.0f / report.norm[i];
        float time_enter = (aabox.min[i] - line_start[i]) * component_inverse;
        float time_exit = (aabox.max[i] - line_start[i]) * component_inverse;

        // put times in proper order
        if ( time_enter > time_exit )
            std::swap( time_enter, time_exit );

        tmin = std::fmaxf(tmin, time_enter);
        tmax = std::fminf(tmax, time_exit);

        if ( tmin > tmax && lineType != LineTypeT::LINE ) {
            return false;
        }
    }

    if ( lineType == LineTypeT::SEG ) {
        if ( tmin >= line_len+EP ) {
            return false;
        }
    }
    
    report.time = tmin; //todoAABOX - this doesn't represent the right time.
    if ( report.ComputeIntersection() ) {
        report.point = line_start + report.norm * tmin;
    }
    
    return true;
}

bool Line3f::Intersects( CollisionReport3D& report, const Line3f& line, const Plane3f& plane ) {
    return ToPlane( report, line, plane.GetPoint(), plane.GetNormal() );
}

bool Line3f::Intersects( CollisionReport3D& report, const Line3f& line, const Polygon3D& polygon ) {
    return ToPoly( report, line, polygon.verts.data(), polygon.verts.size() );
}

bool Line3f::Intersects( const Line3f& line, const Box3D& box ) {
    CollisionReport3D report;
    return Line3f::ToBox3D( report, line.line_type, line.vert[0], line.vert[1], box );
}

bool Line3f::Intersects( const Line3f& line, const Sphere& sphere ) {
    CollisionReport3D report;
    return Line3f::ToSphere( report, line.line_type, line.vert[0], line.vert[1], sphere );
}


bool Line3f::Intersects( const Line3f& line, const Capsule& capsule ) {
    return ToCapsule( line.line_type, line.vert[0], line.vert[1], capsule );
}

float Line3f::GetProjectedPoint( const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& point, Vec3f* ipoint_out ) {
    return Line_GetProjectedPoint( line_type, line_start, line_end, point, ipoint_out );
}

float Line3f::GetDistToPoint( const Line3f& line, const Vec3f& point ) {
    return GetDistToPoint( line.line_type, line.vert[0], line.vert[1], point );
}

float Line3f::DistToLineSeg( const Vec3f& lineA_start, const Vec3f& lineA_end, const Vec3f& lineB_start, const Vec3f& lineB_end ) {
    return DistToLine( lineA_start, lineA_end, LineTypeT::LINE, lineB_start, lineB_end, LineTypeT::SEG );
}

float Line3f::DistToLine( const Line3f& lineA, const Line3f& lineB ) {
    return DistToLine( lineA.vert[0], lineA.vert[1], lineA.line_type, lineB.vert[0], lineB.vert[1], lineB.line_type );
}

float Line3f::DistToLine( const Vec3f& lineA_start, const Vec3f& lineA_end, const Vec3f& lineB_start, const Vec3f& lineB_end ) {
    return DistToLine( lineA_start, lineA_end, LineTypeT::LINE, lineB_start, lineB_end, LineTypeT::LINE );
}

Line3f Line2f::ToLine3f( void ) const {
    return Line3f( vert[0].ToVec3(), vert[1].ToVec3() );
}

float Line2f::GetDistToPoint( const Line2f& line, const Vec2f& point ) {
    return GetDistToPoint( line.line_type, line.vert[0], line.vert[1], point );
}

float Line3f::Len( void ) {
    return Vec3f::Len( vert[1] - vert[0] );
}

Vec3f Line3f::GetCenter( void ) const {
    return ( vert[1] + vert[0] ) / 2.0f;
}

bool Line3f::ToCapsule( const Line3f& line, const Capsule& capsule ) {
    return Line3f::ToCapsule( line.line_type, line.vert[0], line.vert[1], capsule );
}

bool Line3f::ToPoly( CollisionReport3D& report, const Line3f& line, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& poly_normal ) {
    return ToPoly( report, line.line_type, line.vert[0], line.vert[1], clockwise_verts, vert_count, poly_normal );
}

bool Line3f::ToPoly( CollisionReport3D& report, const Line3f& line, const Vec3f* clockwise_verts, const std::size_t vert_count ) {
    return ToPoly( report, line.line_type, line.vert[0], line.vert[1], clockwise_verts, vert_count );
}

bool Line3f::ToPlane( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    return ToPlane( report, LineTypeT::LINE, line_start, line_end, planeA, planeB, planeC );
}

bool Line3f::ToPlane( CollisionReport3D& report, const Line3f& line, const Vec3f& planeA, const Vec3f& planeB, const Vec3f& planeC ) {
    return ToPlane( report, line.line_type, line.vert[0], line.vert[1], planeA, planeB, planeC );
}

bool Line3f::ToPlane( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const Vec3f& planeA, const Vec3f& plane_norm ) {
    return ToPlane( report, LineTypeT::LINE, line_start, line_end, planeA, plane_norm );
}

bool Line3f::ToPlane( CollisionReport3D& report, const Line3f& line, const Vec3f& planeA, const Vec3f& plane_norm ) {
    return ToPlane( report, line.line_type, line.vert[0], line.vert[1], planeA, plane_norm );
}

bool Line3f::Intersects( CollisionReport3D& report, const Line3f& line, const Sphere& sphere ) {
    return ToSphere( report, line.line_type, line.vert[0], line.vert[1], sphere );
}

bool Line3f::Intersects( CollisionReport3D& report, const Line3f& line, const Line3f& other ) {
     return Line3f::ToLine( report, LineTypeT::LINE, line.vert[0], line.vert[1], LineTypeT::LINE, other.vert[0], other.vert[1] );
}

bool Line3f::Intersects( CollisionReport3D& report, const Line3f& line, const AABox3D& aabox ) {
    return Line3f::ToAABox3D( report, line.line_type, line.vert[0], line.vert[1], aabox );
}

bool Line3f::ToAABox3D( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const AABox3D& aabox ) {
    return Line3f::ToAABox3D( report, LineTypeT::LINE, line_start, line_end, aabox );
}

bool Line3f::Intersects( CollisionReport3D& report, const Line3f& line, const Box3D& box ) {
    return Line3f::ToBox3D( report, line.line_type, line.vert[0], line.vert[1], box );
}

bool Line3f::ToBox3D( CollisionReport3D& report, const Vec3f& line_start, const Vec3f& line_end, const Box3D& box ) {
    return Line3f::ToBox3D( report, LineTypeT::LINE, line_start, line_end, box );
}
