// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_POLY_H_
#define SRC_COLLISION_3D_POLY_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Sphere;

class Polygon3D {
public:
    static Vec3f GetCenter( const Vec3f* unsorted_vert_list, const std::size_t num_verts );
    static Sphere GetCircumsphere( const Vec3f* unsorted_vert_list, const std::size_t num_verts );
    static std::size_t GetFarthestVertIndex( const Vec3f* unsorted_vert_list, const std::size_t num_verts, const Vec3f& dir );

    static float GetClosestVert( const Vec3f& to, const Vec3f* unsorted_verts, const std::size_t num_verts, Vec3f& out ); //!< puts the vert in 'out' and returns the distance
    static float GetClosestVertIndex( const Vec3f& to, const Vec3f* unsorted_verts, const std::size_t num_verts, std::size_t& out ); //!< puts the vert index in 'out' and returns the distance

    static bool Intersects( const Polygon3D& polygon, const AABox3D& aabox );
    static bool Intersects( const Polygon3D& polygon, const Box3D& box );
    static bool Intersects( const Polygon3D& polygon, const Sphere& sphere );
    static bool Intersects( const Polygon3D& polygon, const Line3f& point );
    static bool Intersects( const Polygon3D& polygon, const Cylinder& cyl );
    static bool Intersects( const Polygon3D& polygon, const Capsule& cyl );
    static bool Intersects( const Polygon3D& polygon, const Vec3f& point, const float epsilon = EP );
    
public:
    std::vector< Vec3f > verts;
};

namespace Polyhedron {
    std::size_t GetFarthestVertIndex( const Vec3f* unsorted_vert_list, const std::size_t num_verts, const Vec3f& dir );
}

namespace VertCloud {
    std::size_t GetFarthestVertIndex( const Vec3f* unsorted_vert_list, const std::size_t num_verts, const Vec3f& dir );
}

#endif // SRC_COLLISION_3D_POLY_H_
