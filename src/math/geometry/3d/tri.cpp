// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./tri.h"

Circle3f Triangle3f::GetIncircle( const Vec3f& A, const Vec3f& B, const Vec3f& C ) {

        // TODO : UNIT TESTING

    const float len_AB = ( B - A ).Len();
    const float len_BC = ( C - B ).Len();
    const float len_CA = ( A - C ).Len();

    const float perimeter = len_AB + len_BC + len_CA;

    Circle3f incircle;

    incircle.origin.x = ( len_BC * A.x ) + ( len_CA * B.x ) + ( len_AB * C.x );
    incircle.origin.y = ( len_BC * A.y ) + ( len_CA * B.y ) + ( len_AB * C.y );
    incircle.origin.z = ( len_BC * A.z ) + ( len_CA * B.z ) + ( len_AB * C.z );

    incircle.origin /= perimeter;

    // todo: if a right triangle: incircle.radius = ( len1 + len2 - len3 ) / 2; // len3 must be the hypoteneuse:
    // ** using Heron's formula
    const float semi_perimeter = perimeter / 2;
    incircle.radius = sqrtf( semi_perimeter * (semi_perimeter-len_AB) * (semi_perimeter-len_BC) * (semi_perimeter-len_CA) ) / semi_perimeter;

    incircle.axis = Plane3f::GetNormal( A, B, C );

    return incircle;
}

void Triangle3f::UnitTest( void ) {
    GetIncircle_UnitTest();
}

void Triangle3f::GetIncircle_UnitTest( void ) {
    // values calculated with http://www.mathopenref.com/coordincenter.html

    Circle3f cir;

    cir = GetIncircle( {15,15,0}, {47,40,0}, {65,20,0} );
    //ASSERT( cir,radius == 6 );
    ASSERT( Maths::Approxf( cir.origin.x, 45.89f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.origin.y, 27.39f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.radius, 9.25f, 0.01f ) );

    cir = GetIncircle( {15,15,0}, {77,39,0}, {65,20,0} );
    ASSERT( Maths::Approxf( cir.origin.x, 61.26f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.origin.y, 26.05f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.radius, 6.39f, 0.01f ) );

    cir = GetIncircle( {21,26,0}, {56,42,0}, {55,6,0} );
    ASSERT( Maths::Approxf( cir.origin.x, 44.59f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.origin.y, 24.78f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.radius, 10.91f, 0.01f ) );

    cir = GetIncircle( {-38,17,0}, {46,16,0}, {-12,-7,0} );
    ASSERT( Maths::Approxf( cir.origin.x, -9.63f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.origin.y, 5.71f, 0.01f ) );
    ASSERT( Maths::Approxf( cir.radius, 10.94f, 0.01f ) );
}
