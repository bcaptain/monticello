// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./aabox.h"
#include "../../collision/collision.h"
#include "../../../rendering/models/model.h"

// our normals point inward
const Vec3f AABOX_NORM_LEFT(-1,0,0);
const Vec3f AABOX_NORM_RIGHT(1,0,0);
const Vec3f AABOX_NORM_TOP(0,1,0);
const Vec3f AABOX_NORM_BOTTOM(0,-1,0);
const Vec3f AABOX_NORM_NEAR(0,0,1);
const Vec3f AABOX_NORM_FAR(0,0,-1);

AABox3D::AABox3D( const Model& modelToEncompass )
    : min()
    , max()
{
    SetBoundsFromModel( modelToEncompass );
}

void AABox3D::SetBoundsFromModel( const Model& model ) {
    if ( model.NumMeshes() < 1 ) {
        ERR("AABox3D::SetBoundsFromModel: model has no mesh.\n");
        return;
    }
    operator=( model.GetBounds() );

    // extend box outward just a tad, this helps with collision and also ensures that it is a valid box shape for flat surfaces
    min.x -= 0.01f; // one centimeter
    max.x += 0.01f;
    min.y -= 0.01f;
    max.y += 0.01f;
    min.z -= 0.01f;
    max.z += 0.01f;
}

bool AABox3D::Intersects( const AABox3D& aabox, const AABox3D& other ) {
    // we're looking for a separating axis

    if ( !ToAABox3D_HELPER( aabox, other ) )
        return false;

    if ( !ToAABox3D_HELPER( other, aabox ) )
        return false;

    return true;
}

bool AABox3D::Intersects( const AABox3D& aabox, const Polygon3D& polygon ) {
    return ToPoly( aabox, polygon.verts.data(), polygon.verts.size() );
}

bool AABox3D::Intersects( const AABox3D& aabox, const Capsule& capsule ) {
    return Capsule::Intersects( capsule, aabox );
}

bool AABox3D::Intersects( const AABox3D& aabox, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, aabox );
}

bool AABox3D::Intersects( const AABox3D& aabox, const SemiCylinder& semiCyl ) {
    return SemiCylinder::Intersects( semiCyl, aabox );
}

bool AABox3D::Intersects( const AABox3D& aabox, const Line3f& line ) {
    return Line3f::Intersects( line, aabox );
}

bool AABox3D::Intersects( const AABox3D& aabox, const Vec3f& point ) {
    return Collision3D::TestPoint::Intersects( point, aabox );
}

AABox3D::AABox3D( const float abs_min_and_max )
    : min( -abs_min_and_max, -abs_min_and_max, -abs_min_and_max )
    , max( abs_min_and_max, abs_min_and_max, abs_min_and_max )
{ }

AABox3D::AABox3D( void )
    : min()
    , max()
{ }

AABox3D::AABox3D( const float nmin_x, const float nmax_x, const float nmin_y, const float nmax_y, const float nmin_z, const float nmax_z )
    : min( nmin_x, nmin_y, nmin_z )
    , max( nmax_x, nmax_y, nmax_z )
{ }

AABox3D::AABox3D( const Vec3f& nmin, const Vec3f& nmax )
    : min( nmin )
    , max( nmax )
{ }

void AABox3D::SetBounds( const float nmin_x, const float nmax_x, const float nmin_y, const float nmax_y, const float nmin_z, const float nmax_z ) {
    min.Set( nmin_x, nmin_y, nmin_z );
    max.Set( nmax_x, nmax_y, nmax_z );
}

void AABox3D::SetBounds( const Vec3f& nmin, const Vec3f& nmax ) {
    min = nmin;
    max = nmax;
}

void AABox3D::SetBounds( const float abs_min_and_max ) {
    min.Set(-abs_min_and_max,-abs_min_and_max,-abs_min_and_max);
    max.Set(abs_min_and_max,abs_min_and_max,abs_min_and_max);
}

void AABox3D::SetMinsIfHigher( const Vec3f& nmin ) {
    min.x = std::fminf( nmin.x, min.x );
    min.y = std::fminf( nmin.y, min.y );
    min.z = std::fminf( nmin.z, min.z );
}

void AABox3D::SetMaxsIfLower( const Vec3f& nmax ) {
    max.x = std::fmaxf( nmax.x, max.x );
    max.y = std::fmaxf( nmax.y, max.y );
    max.z = std::fmaxf( nmax.z, max.z );
}

float AABox3D::GetHeight( void ) const {
    return fabsf( max.z - min.z );
}

Vec3f AABox3D::GetSize( void ) const {
    return {
        fabsf( max.x - min.x )
        , fabsf( max.y - min.y )
        , fabsf( max.z - min.z )
    };
}

Vec3f AABox3D::GetCenter( void ) const {
    return {
        ( max.x + min.x ) / 2
        , ( max.y + min.y ) / 2
        , ( max.z + min.z ) / 2
    };
}

bool AABox3D::CheckBounds( const Vec3f& origin ) const {
    return (
        CheckX( origin.x )
        && CheckY( origin.y )
        && CheckZ( origin.z )
    );
}

float AABox3D::GetRadius( void ) const {
    float rad = max.x - min.x;
    rad += max.y - min.y;
    rad += max.z - min.z;
    rad /= 3;
    return rad;
}

Sphere AABox3D::GetCircumsphere( void ) const {
    const Vec3f longest_internal_vector(
        fabsf( max.x - min.x )
        , fabsf( max.y - min.y )
        , fabsf( max.z - min.z )
    );
    const float radius( longest_internal_vector.Len() / 2 );
    return {GetCenter(), radius};
}

AABox3D AABox3D::operator+( const Vec3f& vec ) const {
    return {min+vec, max+vec};
}

AABox3D AABox3D::operator-( const Vec3f& vec ) const {
    return {min-vec, max-vec};
}

AABox3D AABox3D::operator/( const float val ) const {
    return {min/val, max/val};
}

AABox3D AABox3D::operator*( const float val ) const {
    return {min*val, max*val};
}

#ifdef CLIB_UNIT_TEST

void AABox3D::UnitTest( void ) {
    UnitTest_GetCircumsphere();
}

void AABox3D::UnitTest_GetCircumsphere( void ) {
    const AABox3D box( Vec3f(-2,-2,-2), Vec3f(2,2,2) );
    const Sphere sphere( box.GetCircumsphere() );
    
    ASSERT( Maths::Approxf( sphere.radius, 3.4641f ) );
    ASSERT( Maths::Approxf( sphere.origin, Vec3f(0,0,0) ) );
}

void AABox3D::Intersects_UnitTest( void ) {
    AABox3D a,b;
    // ****
    // **** THE FAIL TESTS
    // ****

    // ** side by side + reciprocal
    b.SetBounds(0,2,1,3,-1,1);
    a.SetBounds(-3,-1,1,3,-1,1);
    ASSERT( ! AABox3D::Intersects(a,b) );
    ASSERT( ! AABox3D::Intersects(b,a) );

    // ** over and under + reciprocal
    a.SetBounds(1,3,1,3,-1,1);
    b.SetBounds(1,3,0,-2,-1,1);
    ASSERT( ! AABox3D::Intersects(a,b) );
    ASSERT( ! AABox3D::Intersects(b,a) );

    // ** "kiddie-corner" + reciprocal
    ASSERT( ! AABox3D::Intersects(a,b) );
    a.SetBounds(-3,-1,0,2,-1,1);
    b.SetBounds(0,-3,-3,-1,-1,1);
    ASSERT( ! AABox3D::Intersects(b,a) );

    // ****
    // **** THE SUCCEED TESTS
    // ****

    // ** coincide
    a.SetBounds(-2,0,1,3,-1,1);
    b.SetBounds(-2,0,1,3,-1,1);
    ASSERT( AABox3D::Intersects(a,b) );
    ASSERT( AABox3D::Intersects(b,a) );

    // ** intersect corners
    a.SetBounds(-2,0,0,2,-1,1);
    b.SetBounds(-0.5f,1.5f,-1.5f,0.5f,-1,1);
    ASSERT( AABox3D::Intersects(a,b) );
    ASSERT( AABox3D::Intersects(b,a) );

    // ** intersect side + reciprocal
    a.SetBounds(-2,0,1,3,-1,1);
    b.SetBounds(-1,1,1,3,-1,1);
    ASSERT( AABox3D::Intersects(a,b) );
    ASSERT( AABox3D::Intersects(b,a) );

    // ** encompass + reciprocal
    a.SetBounds(1,2,0,2,-1,1);
    b.SetBounds(1.5f,2.5f,0.5f,1.5f,-0.5f,0.5f);
    ASSERT( AABox3D::Intersects(a,b) );
    ASSERT( AABox3D::Intersects(b,a) );
}
#endif // CLIB_UNIT_TEST

bool AABox3D::Intersects( const AABox3D& aabox, const Box3D& box ) {
    return Box3D::Intersects( box, aabox );
}

bool AABox3D::Intersects( const AABox3D& aabox, const Sphere& sphere ) {
    return ToSphere( aabox, sphere, nullptr );
}

bool AABox3D::ToSphere( const AABox3D& aabox, const Sphere& sphere, Vec3f* norm_out ) {
    const bool ret = Sphere::Intersects( sphere, aabox, norm_out );
    if ( norm_out )
        *norm_out = -(*norm_out);
    return ret;
}

bool AABox3D::ToAABox3D_HELPER( const AABox3D& one, const AABox3D& two ) {
    // ** BOX ONE AGAINST BOX TWO
    // X AXIS
    if ( one.min.x > two.max.x )
        if ( one.max.x > two.max.x )
            return false;

    // Y AXIS
    if ( one.min.y > two.max.y )
        if ( one.max.y > two.max.y )
            return false;

    // Z AXIS
    if ( one.min.z > two.max.z )
        if ( one.max.z > two.max.z )
            return false;

    return true;
}

bool AABox3D::ToPoly( const AABox3D& aabox, const Vec3f* clockwise_verts, const std::size_t vert_count ) {
    // First check points
    for ( std::size_t i=0; i<vert_count; ++i ) {
        if ( Collision3D::TestPoint::Intersects( clockwise_verts[i], aabox ) ) {
            return true;
        }
    }

    // then check whether any box lines intersect the poly
    Box3D box3d(aabox);
    CollisionReport3D report;
    if (
        // top
        Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_FAR_LEFT_TOP_INDEX ), box3d.VertAt( BOX_FAR_RIGHT_TOP_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_FAR_LEFT_TOP_INDEX ), box3d.VertAt( BOX_NEAR_LEFT_TOP_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ), box3d.VertAt( BOX_NEAR_LEFT_TOP_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ), box3d.VertAt( BOX_FAR_RIGHT_TOP_INDEX ), clockwise_verts, vert_count )
        // bottom
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ), box3d.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ), box3d.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ), box3d.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ), box3d.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        // sides
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_FAR_LEFT_TOP_INDEX ), box3d.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_FAR_RIGHT_TOP_INDEX ), box3d.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_NEAR_LEFT_TOP_INDEX ), box3d.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        || Line3f::ToPoly( report, LineTypeT::SEG, box3d.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ), box3d.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ), clockwise_verts, vert_count )
        )
        return true;

    return false;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool AABox3D::ToModel( const AABox3D& aabox, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
        ERR("Not Implemented: AABox3D to ToModel.\n"); //toimplement
        return false;
    }

    bool AABox3D::SweepToModel( CollisionReport3D& report, const AABox3D& aabox, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {
        ERR("Not Implemented: AABox3D sweep to ToModel.\n"); //toimplement
        return false;
    }

    bool AABox3D::SweepTo( CollisionReport3D& report, const AABox3D& aabox, const Vec3f& vel, const Sphere& sphere ) {
        ERR("Not Implemented: AABox3D sweep to Sphere.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

Vec3f AABox3D::GetFarthestVert( const Vec3f& dir ) const {
    const Box3D box( *this );
    return box.GetFarthestVert( dir );
}

void AABox3D::SetMins( const Vec3f& nmin ) {
    min = nmin;
}

void AABox3D::SetMaxs( const Vec3f& nmax ) {
    max = nmax;
}

bool AABox3D::CheckXMax( const float x ) const {
    return x <= max.x;
}

bool AABox3D::CheckYMax( const float y ) const {
    return y <= max.y;
}

bool AABox3D::CheckZMax( const float z ) const {
    return z <= max.z;
}

bool AABox3D::CheckXMin( const float x ) const {
    return x >= min.x;
}

bool AABox3D::CheckYMin( const float y ) const {
    return y >= min.y;
}

bool AABox3D::CheckZMin( const float z ) const {
    return z >= min.z;
}

bool AABox3D::CheckX( const float x ) const {
    return x >= min.x && x <= max.x;
}

bool AABox3D::CheckY( const float y ) const {
    return y >= min.y && y <= max.y;
}

bool AABox3D::CheckZ( const float z ) const {
    return z >= min.z && z <= max.z;
}
