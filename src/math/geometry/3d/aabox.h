// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_AABOX_H_
#define SRC_COLLISION_3D_AABOX_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Line3f;
class Line2f;

typedef Uint8 LineTypeT_BaseType;
enum class LineTypeT : LineTypeT_BaseType;

typedef Uint8 ModelCollisionT_BaseType;
enum class ModelCollisionT : ModelCollisionT_BaseType;

class CollisionReport3D;
class Model;
class Sphere;
class Box3D;
class AABox3D;
class Box3D;
class SemiCylinder;
class Cylinder;
class Capsule;
class Polygon3D;

extern const Vec3f AABOX_NORM_LEFT;
extern const Vec3f AABOX_NORM_RIGHT;
extern const Vec3f AABOX_NORM_TOP;
extern const Vec3f AABOX_NORM_BOTTOM;
extern const Vec3f AABOX_NORM_NEAR;
extern const Vec3f AABOX_NORM_FAR;

class AABox3D {
public:
    AABox3D( void );
    explicit AABox3D( const float abs_min_and_max );
    AABox3D( const float nmin_x, const float nmax_x, const float nmin_y, const float nmax_y, const float nmin_z, const float nmax_z );
    AABox3D( const Vec3f& nmin, const Vec3f& nmax );

    explicit AABox3D( const Model& modelToEncompass );

public:
    void SetBoundsFromModel( const Model& model );
    
    void SetBounds( const float nmin_x, const float nmax_x, const float nmin_y, const float nmax_y, const float nmin_z, const float nmax_z );
    void SetBounds( const Vec3f& nmin, const Vec3f& nmax );
    void SetBounds( const float abs_min_and_max );

    bool CheckBounds( const Vec3f& origin ) const;
    float GetRadius( void ) const;

    bool CheckX( const float x ) const;
    bool CheckY( const float y ) const;
    bool CheckZ( const float z ) const;

    bool CheckXMin( const float x ) const;
    bool CheckYMin( const float y ) const;
    bool CheckZMin( const float z ) const;

    bool CheckXMax( const float x ) const;
    bool CheckYMax( const float y ) const;
    bool CheckZMax( const float z ) const;

    void SetMinsIfHigher( const Vec3f& min );
    void SetMaxsIfLower( const Vec3f& max );

    void SetMins( const Vec3f& min );
    void SetMaxs( const Vec3f& max );
    
    Vec3f GetFarthestVert( const Vec3f& dir ) const;

    float GetHeight( void ) const;
    Vec3f GetCenter( void ) const;
    Vec3f GetSize( void ) const;
    Sphere GetCircumsphere( void ) const; //! returns the origin and radius of a sphere just slightly large enough to encompass the entire aabox
    static void UnitTest_GetCircumsphere( void );

    Vec3f GetVert_FarLeftTop( void ) const { return { min.x, max.y, min.z }; }
    Vec3f GetVert_FarRightTop( void ) const { return { max.x, max.y, min.z }; }
    Vec3f GetVert_NearLeftTop( void ) const { return { min.x, max.y, max.z }; }
    Vec3f GetVert_NearRightTop( void ) const { return max; }
    Vec3f GetVert_NearLeftBottom( void ) const { return { min.x, min.y, max.z }; }
    Vec3f GetVert_NearRightBottom( void ) const { return { max.x, min.y, max.z }; }
    Vec3f GetVert_FarLeftBottom( void ) const { return min; }
    Vec3f GetVert_FarRightBottom( void ) const { return { max.x, min.y, min.z }; }

    AABox3D& operator+=( const Vec3f& val );
    AABox3D& operator-=( const Vec3f& val );
    AABox3D& operator*=( const float val );
    AABox3D& operator/=( const float val );

    AABox3D operator+( const Vec3f& val ) const;
    AABox3D operator-( const Vec3f& val ) const;
    AABox3D operator*( const float val ) const;
    AABox3D operator/( const float val ) const;

private:
    static bool ToAABox3D_HELPER( const AABox3D& aabox, const AABox3D& other );
public:
    static bool ToPoly( const AABox3D& aabox, const Vec3f* clockwise_verts, const std::size_t vert_count );
    static bool ToSphere( const AABox3D& aabox, const Sphere& sphere, Vec3f* norm_out = nullptr );
    
    static bool Intersects( const AABox3D& aabox, const Sphere& sphere );
    static bool Intersects( const AABox3D& aabox, const Polygon3D& poly );
    static bool Intersects( const AABox3D& aabox, const AABox3D& other );
    static bool Intersects( const AABox3D& aabox, const Box3D& box );
    static bool Intersects( const AABox3D& aabox, const Line3f& line );
    static bool Intersects( const AABox3D& aabox, const SemiCylinder& semiCyl );
    static bool Intersects( const AABox3D& aabox, const Cylinder& cyl );
    static bool Intersects( const AABox3D& aabox, const Capsule& capsule );
    static bool Intersects( const AABox3D& aabox, const Vec3f& point );
    
    static bool ToModel( const AABox3D& aabox, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );
    static bool SweepToModel( CollisionReport3D& report, const AABox3D& aabox, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );
    
    bool SweepTo( CollisionReport3D& report, const AABox3D& aabox, const Vec3f& vel, const Sphere& sphere );

    #ifdef CLIB_UNIT_TEST
        static void UnitTest( void );
        static void Intersects_UnitTest( void );
    #endif // CLIB_UNIT_TEST
    
public:
    Vec3f min, max;
};

#endif // SRC_COLLISION_3D_AABOX_H_
