// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./line.h"
#include "../../collision/collision.h"
#include "../../../rendering/models/model.h"

#ifdef CLIB_UNIT_TEST
    void Line3f::LineSeg3f_UnitTest_LineDist( void ) {
        float ret;
        {
            const Vec3f a(0,1,0); const Vec3f c(1,0,0);
            const Vec3f b(0,2,0); const Vec3f d(1,0,1);
            ret = Line3f::DistToLine(Line3f(a,b,LineTypeT::SEG), Line3f(c,d,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(a,b,LineTypeT::SEG), Line3f(d,c,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(b,a,LineTypeT::SEG), Line3f(c,d,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(b,a,LineTypeT::SEG), Line3f(d,c,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(c,d,LineTypeT::SEG), Line3f(a,b,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(d,c,LineTypeT::SEG), Line3f(a,b,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(c,d,LineTypeT::SEG), Line3f(b,a,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = Line3f::DistToLine(Line3f(d,c,LineTypeT::SEG), Line3f(b,a,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1.414214f ) );

            ret = Line3f::DistToLine(a,b, c,d); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(a,b, d,c); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(b,a, c,d); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(b,a, d,c); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(c,d, a,b); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(d,c, a,b); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(c,d, b,a); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(d,c, b,a); ASSERT( Maths::Approxf( ret, 1 ) );
        }

        { // criss-corss and touch
            const Vec3f a(0,-1,0); const Vec3f c(-1,0,0);
            const Vec3f b(0,1,0); const Vec3f d(1,0,0);
            ret = Line3f::DistToLine(Line3f(a,b,LineTypeT::SEG), Line3f(c,d,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(a,b,LineTypeT::SEG), Line3f(d,c,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(b,a,LineTypeT::SEG), Line3f(c,d,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(b,a,LineTypeT::SEG), Line3f(d,c,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(c,d,LineTypeT::SEG), Line3f(a,b,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(d,c,LineTypeT::SEG), Line3f(a,b,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(c,d,LineTypeT::SEG), Line3f(b,a,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(Line3f(d,c,LineTypeT::SEG), Line3f(b,a,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(a,b, c,d); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(a,b, d,c); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(b,a, c,d); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(b,a, d,c); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(c,d, a,b); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(d,c, a,b); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(c,d, b,a); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(d,c, b,a); ASSERT( Maths::Approxf( ret, 0 ) );
        }


        { // tee, no touch
            const Vec3f a(0,0,0); const Vec3f c(-1,2,0);
            const Vec3f b(0,1,0); const Vec3f d(1,2,0);
            ret = Line3f::DistToLine(Line3f(a,b,LineTypeT::SEG), Line3f(c,d,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(a,b,LineTypeT::SEG), Line3f(d,c,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(b,a,LineTypeT::SEG), Line3f(c,d,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(b,a,LineTypeT::SEG), Line3f(d,c,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(c,d,LineTypeT::SEG), Line3f(a,b,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(d,c,LineTypeT::SEG), Line3f(a,b,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(c,d,LineTypeT::SEG), Line3f(b,a,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::DistToLine(Line3f(d,c,LineTypeT::SEG), Line3f(b,a,LineTypeT::SEG)); ASSERT( Maths::Approxf( ret, 1 ) );

            ret = Line3f::DistToLine(a,b, c,d); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(a,b, d,c); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(b,a, c,d); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(b,a, d,c); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(c,d, a,b); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(d,c, a,b); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(c,d, b,a); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLine(d,c, b,a); ASSERT( Maths::Approxf( ret, 0 ) );
    /* todo
            ret = Line3f::DistToLineSeg(a,b, c,d); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLineSeg(a,b, d,c); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLineSeg(b,a, c,d); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = Line3f::DistToLineSeg(b,a, d,c); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = LineSeg3f::DistToLine(c,d, a,b); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = LineSeg3f::DistToLine(d,c, a,b); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = LineSeg3f::DistToLine(c,d, b,a); ASSERT( Maths::Approxf( ret, 0 ) );
            ret = LineSeg3f::DistToLine(d,c, b,a); ASSERT( Maths::Approxf( ret, 0 ) );

            ret = LineSeg3f::DistToLine(a,b, c,d); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(a,b, d,c); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(b,a, c,d); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(b,a, d,c); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(c,d, a,b); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(d,c, a,b); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(c,d, b,a); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = LineSeg3f::DistToLine(d,c, b,a); ASSERT( Maths::Approxf( ret, 1 ) );
    */
        }
    /*todo
        { // parallel
            const Vec3f a(-1,0,0); const Vec3f c(1,-2,0);
            const Vec3f b(-1,1,0); const Vec3f d(1,-1,0);
            ret = LineSeg3f::SquaredDistToLineSeg(a,b, c,d); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = LineSeg3f::SquaredDistToLineSeg(a,b, d,c); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = LineSeg3f::SquaredDistToLineSeg(b,a, c,d); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = LineSeg3f::SquaredDistToLineSeg(b,a, d,c); ASSERT( Maths::Approxf( ret, 1.414214f ) );

            ret = LineSeg3f::SquaredDistToLineSeg(c,d, a,b); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = LineSeg3f::SquaredDistToLineSeg(d,c, a,b); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = LineSeg3f::SquaredDistToLineSeg(c,d, b,a); ASSERT( Maths::Approxf( ret, 1.414214f ) );
            ret = LineSeg3f::SquaredDistToLineSeg(d,c, b,a); ASSERT( Maths::Approxf( ret, 1.414214f ) );

            ret = Line3f::SquaredDistToLine(a,b, c,d); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::SquaredDistToLine(a,b, d,c); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::SquaredDistToLine(b,a, c,d); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::SquaredDistToLine(b,a, d,c); ASSERT( Maths::Approxf( ret, 1 ) );

            ret = Line3f::SquaredDistToLine(c,d, a,b); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::SquaredDistToLine(d,c, a,b); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::SquaredDistToLine(c,d, b,a); ASSERT( Maths::Approxf( ret, 1 ) );
            ret = Line3f::SquaredDistToLine(d,c, b,a); ASSERT( Maths::Approxf( ret, 1 ) );
        }
    */
    }
    
    void Line3f::LineSeg2f_UnitTest_LineDist( void ) {
        ASSERT( Maths::Approxf( Line2f::GetDistToPoint( LineTypeT::LINE, Vec2f(0,-1), Vec2f(0,1), Vec2f(1,1) ), -1) );
        ASSERT( Maths::Approxf( Line2f::GetDistToPoint( LineTypeT::LINE, Vec2f(0,-1), Vec2f(0,1), Vec2f(-1,1) ), 1) );
        ASSERT( Maths::Approxf( Line2f::GetDistToPoint( LineTypeT::LINE, Vec2f(0,-1), Vec2f(0,1), Vec2f(2,100) ), -2) );
        ASSERT( Maths::Approxf( Line2f::GetDistToPoint( LineTypeT::LINE, Vec2f(0,-1), Vec2f(0,1), Vec2f(-2,100) ), 2) );
    }

    void Line3f::UnitTest( void ) {
        UnitTest_Plane();
        UnitTest_CoplanarPoly();
        UnitTest_ToLine();
        UnitTest_ToSphere();
        UnitTest_ToCapsule();
        LineSeg3f_UnitTest_LineDist();
        LineSeg2f_UnitTest_LineDist();
        UnitTest_Seg_Plane();
        UnitTest_Seg_CoplanarPoly();
        UnitTest_Seg_AABox_and_Box3D();
        UnitTest_Seg_ToCapsule();
        UnitTest_Seg_ToBox3D();
    }

    void Line3f::UnitTest_ToCapsule( void ) {
        const Vec3f origin(0,0,0), normal(0,0,1);
        const float radius(5), length(5);
        const Capsule cap( origin, normal, radius, length );

        const Line3f inside( Vec3f(1,0,0), Vec3f(-1,0,0), LineTypeT::SEG );
        const Line3f passthru( Vec3f(10,0,0), Vec3f(-10,0,0), LineTypeT::SEG );
        const Line3f HitTopSphere( Vec3f(10,0,9.5f), Vec3f(-10,0,9.5f), LineTypeT::SEG );
        const Line3f HitBottomSphere( Vec3f(10,0,-4.9f), Vec3f(-10,0,-4.9f), LineTypeT::SEG );
        const Line3f GrazeTopSphere( Vec3f(10,0,10), Vec3f(-10,0,10), LineTypeT::SEG );
        const Line3f GrazeBottomSphere( Vec3f(10,0,-5), Vec3f(-10,0,-5), LineTypeT::SEG );

        const Line3f outside( Vec3f(10,5.1f,0), Vec3f(-10,5.1f,0), LineTypeT::SEG );
        const Line3f PassTopSphere( Vec3f(10,0,10.1f), Vec3f(-10,0,10.1f), LineTypeT::SEG );
        const Line3f PassBottomSphere( Vec3f(10,0,-10.1f), Vec3f(-10,0,-10.1f), LineTypeT::SEG );

        ASSERT( ToCapsule( inside, cap ) );
        ASSERT( ToCapsule( passthru, cap ) );
        ASSERT( ToCapsule( HitTopSphere, cap ) );
        ASSERT( ToCapsule( HitBottomSphere, cap ) );
        ASSERT( ToCapsule( GrazeTopSphere, cap ) );
        ASSERT( ToCapsule( GrazeBottomSphere, cap ) );

        ASSERT( !ToCapsule( outside, cap ) );
        ASSERT( !ToCapsule( PassBottomSphere, cap ) );
        ASSERT( !ToCapsule( PassTopSphere, cap ) );
    }

    void Line3f::UnitTest_ToSphere( void ) {
        // **** LineSeg

        CollisionReport3D report;
        Line3f line( LineTypeT::SEG );
        Sphere sphere;

        // ** straight through the sphere

        line.Set( Vec3f(0,0,1.01f), Vec3f(0,0,-1.01f) );
        sphere.origin.Set(0,0,0);
        sphere.radius = 1;

        ASSERT( Intersects( report, line, sphere ) );

        // ** almost grazing the sphere

        line.Set( Vec3f(-1.01f,0,1.01f), Vec3f(-1.01f,0,-1.01f) );
        sphere.origin.Set(0,0,0);
        sphere.radius = 1;

        ASSERT( !Intersects( report, line, sphere ) );

        // ** grazing the sphere

        line.Set( Vec3f(-1,0,1.01f), Vec3f(-1,0,-1.01f) );
        sphere.origin.Set(0,0,0);
        sphere.radius = 1;

        ASSERT( Intersects( report, line, sphere ) );

        // ** pointing at sphere, very close to it, not colinear with sphere's origin, and not colliding

        line.Set( Vec3f(-0.8f,0.8f,-2.8f), Vec3f(-0.8f,0,-0.8f) );
        sphere.origin.Set(0,0,0);
        sphere.radius = 1;

        ASSERT( !Intersects( report, line, sphere ) );

        // ** pointing at sphere, crooked, very close to it, not colinear with sphere's origin, and not colliding

        line.Set( Vec3f(-2.8f,2.8f,-2.8f), Vec3f(-0.8f,0,-0.8f) );
        sphere.origin.Set(0,0,0);
        sphere.radius = 1;

        ASSERT( !Intersects( report, line, sphere ) );

        // ** poking sphere, crooked, not colinear with sphere's origin, and not colliding

        line.Set( Vec3f(-2.8f,2.8f,-2.8f), Vec3f(-0.7f,0,-0.7f) );
        sphere.origin.Set(0,0,0);
        sphere.radius = 1;

        ASSERT( Intersects( report, line, sphere ) );
    }

    void Line3f::UnitTest_Plane( void ) {
        CollisionReport3D report;

        // clockwise
        Vec3f a( -3,3,3 );
        Vec3f b( 3,3,3 );
        Vec3f c( 3,-3,3 );
        Line3f F_parallel( Vec3f( 6,7,4 ), Vec3f( 8,9,4 ), LineTypeT::LINE );
        Line3f F_parallel_very_close( Vec3f( 6,7,3.01f ), Vec3f( 8,9,3.01f ), LineTypeT::LINE );
        Line3f T_coincide( Vec3f( 6,7,3 ), Vec3f( 8,9,3 ), LineTypeT::LINE );
        Line3f T_intersect( Vec3f( 10,20,30 ), Vec3f( 40,50,60 ), LineTypeT::LINE );
        ASSERT( ! ToPlane( report, F_parallel, a,b,c ) );
        ASSERT( ! ToPlane( report, F_parallel_very_close, a,b,c ) );
        ASSERT( ToPlane( report, T_coincide, a,b,c ) );
        ASSERT( ToPlane( report, T_intersect, a,b,c ) );

        Line3f F_tilt_toward( Vec3f(6,6,4), Vec3f( 4,4,6), LineTypeT::SEG );
        Line3f F_tilt_away( Vec3f(4,4,6), Vec3f(6,6,4), LineTypeT::SEG );
        Line3f F_perp_toward( Vec3f(6,6,6), Vec3f( 4,4,4), LineTypeT::SEG );
        Line3f F_perp_away( Vec3f(4,4,4), Vec3f( 6,6,6), LineTypeT::SEG );
        ASSERT( ! ToPlane( report, F_tilt_toward, a,b,c ) );
        ASSERT( ! ToPlane( report, F_tilt_away, a,b,c ) );
        ASSERT( ! ToPlane( report, F_perp_toward, a,b,c ) );
        ASSERT( ! ToPlane( report, F_perp_away, a,b,c ) );
    }

    void Line3f::UnitTest_CoplanarPoly( void ) {

    }

    void Line3f::UnitTest_ToLine( void ) {
        Line3f T_a_Line_Line( Vec3f(-4,-4,0), Vec3f(-4,-2,0) );
        Line3f T_b_Line_Line( Vec3f(6,8,0), Vec3f(8,8,0) );

        Line3f F_a_Line_Line_Paralell( Vec3f(-1,-1,0), Vec3f(1,-1,0) );
        Line3f F_b_Line_Line_Paralell( Vec3f(-1,1,0), Vec3f(1,1,0) );

        Line3f T_a_Line_Line_Coincide( Vec3f(-1,1,0), Vec3f(1,1,0) );
        Line3f T_b_Line_Line_Coincide( Vec3f(-50,1,0), Vec3f(-45,1,0) );

        Line3f T_a_Seg_Seg( Vec3f(-1,-1,0), Vec3f(1,1,0) ); // criss-crosses
        Line3f T_b_Seg_Seg( Vec3f(-1,1,0), Vec3f(1,-1,0) );

        Line3f T_a_Seg_Seg_Coincide( Vec3f(-1,1,0), Vec3f(1,1,0) );
        Line3f T_b_Seg_Seg_Coincide( Vec3f(-0.5f,1,0), Vec3f(1.5f,1,0) );

        Line3f F_a_Seg_Seg_Tee( Vec3f(-1,5,0), Vec3f(1,5,0) );
        Line3f F_b_Seg_Seg_Tee( Vec3f(0,0,0), Vec3f(0,1,0) );

        Line3f F_a_Seg_Seg_ReverseTee( Vec3f(0,0,0), Vec3f(0,1,0) );
        Line3f F_b_Seg_Seg_ReverseTee( Vec3f(-1,5,0), Vec3f(1,5,0) );

        Line3f T_a_Ray_Ray_Cross( Vec3f(-50,0,0), Vec3f(-40,10,0) );
        Line3f T_b_Ray_Ray_Cross( Vec3f(50,0,0), Vec3f(40,10,0) );

        Line3f F_a_Ray_Ray_NoCross( Vec3f(-50,0,0), Vec3f(-55,10,0) );
        Line3f F_b_Ray_Ray_NoCross( Vec3f(-50,0,0), Vec3f(55,10,0) );

        Line3f T_a_Line_Seg( Vec3f(0,-5,0), Vec3f(0,-3,0) );
        Line3f T_b_Line_Seg( Vec3f(-1,0,0), Vec3f(1,0,0) );

        Line3f F_a_Line_Seg( Vec3f(0,-5,0), Vec3f(0,-3,0) );
        Line3f F_b_Line_Seg( Vec3f(-50,0,0), Vec3f(-45,0,0) );

        Line3f T_a_Seg_Line( Vec3f(-1,-1,0), Vec3f(-1,1,0) );
        Line3f T_b_Seg_Line( Vec3f(0,0,0), Vec3f(4,0,0) );

        Line3f F_a_Seg_Line( Vec3f(-1,-1,0), Vec3f(1,-1,0) );
        Line3f F_b_Seg_Line( Vec3f(10,1,0), Vec3f(10,-1,0) );

        Line3f F_a_Seg_Seg_TeeCCW( Vec3f(10,-10,10), Vec3f(10,10,10) );
        Line3f F_b_Seg_Seg_TeeCCW( Vec3f(11,1,10), Vec3f(13,1,10) );


        CollisionReport3D report;

        ASSERT( ! ToLine( report, LineTypeT::SEG, F_a_Seg_Seg_TeeCCW.vert[0], F_a_Seg_Seg_TeeCCW.vert[1], LineTypeT::SEG, F_b_Seg_Seg_TeeCCW.vert[0], F_b_Seg_Seg_TeeCCW.vert[1] ) );
        ASSERT( ToLine( report, LineTypeT::SEG, T_a_Seg_Seg.vert[0], T_a_Seg_Seg.vert[1], LineTypeT::SEG, T_b_Seg_Seg.vert[0], T_b_Seg_Seg.vert[1] ) );
        ASSERT( ToLine( report, LineTypeT::SEG, T_a_Seg_Seg_Coincide.vert[0], T_a_Seg_Seg_Coincide.vert[1], LineTypeT::SEG, T_b_Seg_Seg_Coincide.vert[0], T_b_Seg_Seg_Coincide.vert[1] ) );
        ASSERT( ! ToLine( report, LineTypeT::SEG, F_a_Seg_Seg_Tee.vert[0], F_a_Seg_Seg_Tee.vert[1], LineTypeT::SEG, F_b_Seg_Seg_Tee.vert[0], F_b_Seg_Seg_Tee.vert[1] ) );
        ASSERT( ! ToLine( report, LineTypeT::SEG, F_a_Seg_Seg_ReverseTee.vert[0], F_a_Seg_Seg_ReverseTee.vert[1], LineTypeT::SEG, F_b_Seg_Seg_ReverseTee.vert[0], F_b_Seg_Seg_ReverseTee.vert[1] ) );

        ASSERT( Line3f::ToLine( report, LineTypeT::LINE, T_a_Line_Line.vert[0], T_a_Line_Line.vert[1], LineTypeT::LINE, T_b_Line_Line.vert[0], T_b_Line_Line.vert[1] ) );
        ASSERT( ! Line3f::ToLine( report, LineTypeT::LINE, F_a_Line_Line_Paralell.vert[0], F_a_Line_Line_Paralell.vert[1], LineTypeT::LINE, F_b_Line_Line_Paralell.vert[0], F_b_Line_Line_Paralell.vert[1] ) );
        ASSERT( ! Line3f::ToLine( report, LineTypeT::LINE, T_a_Line_Line_Coincide.vert[0], T_a_Line_Line_Coincide.vert[1], LineTypeT::LINE, T_b_Line_Line_Coincide.vert[0], T_b_Line_Line_Coincide.vert[1] ) );

        ASSERT( Line3f::ToLine( report, LineTypeT::LINE, T_a_Line_Seg.vert[0], T_a_Line_Seg.vert[1], LineTypeT::SEG, T_b_Line_Seg.vert[0], T_b_Line_Seg.vert[1] ) );
        ASSERT( ! Line3f::ToLine( report, LineTypeT::LINE, F_a_Line_Seg.vert[0], F_a_Line_Seg.vert[1], LineTypeT::SEG, F_b_Line_Seg.vert[0], F_b_Line_Seg.vert[1] ) );

        ASSERT( ToLine( report, LineTypeT::RAY, T_a_Ray_Ray_Cross.vert[0], T_a_Ray_Ray_Cross.vert[1], LineTypeT::LINE, T_b_Ray_Ray_Cross.vert[0], T_b_Ray_Ray_Cross.vert[1] ) );
        ASSERT( ! ToLine( report, LineTypeT::RAY, F_a_Ray_Ray_NoCross.vert[0], F_a_Ray_Ray_NoCross.vert[1], LineTypeT::LINE, F_b_Ray_Ray_NoCross.vert[0], F_b_Ray_Ray_NoCross.vert[1] ) );

        ASSERT( ToLine( report, LineTypeT::SEG, T_a_Seg_Line.vert[0], T_a_Seg_Line.vert[1], LineTypeT::LINE, T_b_Seg_Line.vert[0], T_b_Seg_Line.vert[1] ) );
        ASSERT( ! ToLine( report, LineTypeT::SEG, F_a_Seg_Line.vert[0], F_a_Seg_Line.vert[1], LineTypeT::LINE, F_b_Seg_Line.vert[0], F_b_Seg_Line.vert[1] ) );
    }
    
    void Line3f::UnitTest_Seg_ToCapsule( void ) {
        const Capsule capsule( Vec3f(0,0,0), Vec3f(0,1,0), 1, 5 );
        const Line3f to_left( Vec3f(-1.5,0,0), Vec3f(-5.5,0,0), LineTypeT::SEG );
        const Line3f to_right( Vec3f(1.5,0,0), Vec3f(5.5,0,0), LineTypeT::SEG );
        const Line3f very_close( Vec3f(1.01f,1,0), Vec3f(1.01f,5,0), LineTypeT::SEG );
        const Line3f over_top( Vec3f(1,6.01f,0), Vec3f(-1,6.01f,0), LineTypeT::SEG );

        const Line3f graze_top( Vec3f(-1,6,0), Vec3f(1,6,0), LineTypeT::SEG );
        const Line3f graze_bottom( Vec3f(-1,-1,0), Vec3f(1,-1,0), LineTypeT::SEG );
        const Line3f straight_through( Vec3f(2,3,0), Vec3f(-3,3,0), LineTypeT::SEG );
        const Line3f graze_left( Vec3f(-1,0,0), Vec3f(-1,5,0), LineTypeT::SEG );
        const Line3f graze_right( Vec3f(1,0,0), Vec3f(1,5,0), LineTypeT::SEG );

        ASSERT( ! ToCapsule( to_left, capsule ) );
        ASSERT( ! ToCapsule( to_right, capsule ) );
        ASSERT( ! ToCapsule( very_close, capsule ) );
        ASSERT( ! ToCapsule( over_top, capsule ) );
        ASSERT( ToCapsule( graze_bottom, capsule ) );
        ASSERT( ToCapsule( graze_top, capsule ) );
        ASSERT( ToCapsule( graze_left, capsule ) );
        ASSERT( ToCapsule( graze_right, capsule ) );
        ASSERT( ToCapsule( straight_through, capsule ) );
    }

    void Line3f::UnitTest_Seg_AABox_and_Box3D( void ) {

        CollisionReport3D report( CollisionCalcsT::Intersection | CollisionCalcsT::Normal );

        // **** first, a few random tests

        const AABox3D aabox(-1,1,-1,1,-1,1);
        const Box3D box( aabox );

        const Line3f no1( Vec3f(2,0.5f,0), Vec3f(2,0,0), LineTypeT::SEG );
        const Line3f no2( Vec3f(-2,0.5f,0), Vec3f(-2,0,0), LineTypeT::SEG );
        const Line3f no3( Vec3f(0.5f,2,0), Vec3f(0,2,0), LineTypeT::SEG );
        const Line3f no4( Vec3f(0.5f,-2,0), Vec3f(0,-2,0), LineTypeT::SEG );
        const Line3f no5( Vec3f(0.5f,0,2), Vec3f(0,0,2), LineTypeT::SEG );
        const Line3f no6( Vec3f(0.5f,0,-2), Vec3f(0,0,-2), LineTypeT::SEG );

        const Line3f intersect_fully_x( Vec3f(-2,0,0), Vec3f(2,0,0), LineTypeT::SEG );
        const Line3f intersect_fully_y( Vec3f(0,-2,0), Vec3f(0,2,0), LineTypeT::SEG );
        const Line3f intersect_fully_z( Vec3f(0,0,-2), Vec3f(0,0,2), LineTypeT::SEG );

        const Line3f intersect_fully_diagonal_tr_bl( Vec3f(0,2,2), Vec3f(0,-2,-2), LineTypeT::SEG );
        const Line3f intersect_fully_diagonal_br_tl( Vec3f(0,2,-2), Vec3f(0,-2,2), LineTypeT::SEG );
        const Line3f intersect_fully_diagonal_bl_tr( Vec3f(0,-2,-2), Vec3f(0,2,2), LineTypeT::SEG );
        const Line3f intersect_fully_diagonal_tl_br( Vec3f(0,-2,2), Vec3f(0,2,-2), LineTypeT::SEG );

        const Line3f intersect_partial_x_right( Vec3f(2,0,0), Vec3f(0,0,0), LineTypeT::SEG );
        const Line3f intersect_partial_y_right( Vec3f(0,2,0), Vec3f(0,0,0), LineTypeT::SEG );
        const Line3f intersect_partial_z_right( Vec3f(0,0,2), Vec3f(0,0,0), LineTypeT::SEG );

        const Line3f intersect_partial_x_left( Vec3f(-2,0,0), Vec3f(0,0,0), LineTypeT::SEG );
        const Line3f intersect_partial_y_left( Vec3f(0,-2,0), Vec3f(0,0,0), LineTypeT::SEG );
        const Line3f intersect_partial_z_left( Vec3f(0,0,-2), Vec3f(0,0,0), LineTypeT::SEG );

        const Line3f intersect_partial_x_right2( Vec3f(0,0,0), Vec3f(2,0,0), LineTypeT::SEG );
        const Line3f intersect_partial_y_right2( Vec3f(0,0,0), Vec3f(0,2,0), LineTypeT::SEG );
        const Line3f intersect_partial_z_right2( Vec3f(0,0,0), Vec3f(0,0,2), LineTypeT::SEG );

        const Line3f intersect_partial_x_left2( Vec3f(0,0,0), Vec3f(-2,0,0), LineTypeT::SEG );
        const Line3f intersect_partial_y_left2( Vec3f(0,0,0), Vec3f(0,-2,0), LineTypeT::SEG );
        const Line3f intersect_partial_z_left2( Vec3f(0,0,0), Vec3f(0,0,-2), LineTypeT::SEG );

        const Line3f within_bottom( Vec3f(0,0.5f,-1), Vec3f(0,-0.5f,-1), LineTypeT::SEG );
        const Line3f within_edge( Vec3f(0.5f,-1,-1), Vec3f(-0.5f,-1,-1), LineTypeT::SEG );
        const Line3f touches_corner( Vec3f(-2,-2,2), Vec3f(-1,-1,1), LineTypeT::SEG );
        
        // ** AABox, LineSeg paralell to
        
        const Line3f setOne_paralellX_a(         Vec3f(-1.01f,0,-3), Vec3f(-1.01f,0,-2), LineTypeT::SEG );
        const Line3f setOne_paralellX_a_reverse( Vec3f(-1.01f,0,-2), Vec3f(-1.01f,0,-3), LineTypeT::SEG );
        const Line3f setOne_paralellX_b(         Vec3f(-1.01f,0,-2), Vec3f(-1.01f,0,-1), LineTypeT::SEG );
        const Line3f setOne_paralellX_b_reverse( Vec3f(-1.01f,0,-1), Vec3f(-1.01f,0,-2), LineTypeT::SEG );
        const Line3f setOne_paralellX_c(         Vec3f(-1.01f,0,-2), Vec3f(-1.01f,0,0), LineTypeT::SEG );
        const Line3f setOne_paralellX_c_reverse( Vec3f(-1.01f,0,0), Vec3f(-1.01f,0,-2), LineTypeT::SEG );
        const Line3f setOne_paralellX_d(         Vec3f(-1.01f,0,-1), Vec3f(-1.01f,0,0), LineTypeT::SEG );
        const Line3f setOne_paralellX_d_reverse( Vec3f(-1.01f,0,0), Vec3f(-1.01f,0,-1), LineTypeT::SEG );
        const Line3f setOne_paralellX_e(         Vec3f(-1.01f,0,-0.2f), Vec3f(-1.01f,0,0.2f), LineTypeT::SEG );
        const Line3f setOne_paralellX_e_reverse( Vec3f(-1.01f,0,0.2f), Vec3f(-1.01f,0,-0.2f), LineTypeT::SEG );
        const Line3f setOne_paralellX_f(         Vec3f(-1.01f,0,1), Vec3f(-1.01f,0,0), LineTypeT::SEG );
        const Line3f setOne_paralellX_f_reverse( Vec3f(-1.01f,0,0), Vec3f(-1.01f,0,1), LineTypeT::SEG );
        const Line3f setOne_paralellX_g(         Vec3f(-1.01f,0,2), Vec3f(-1.01f,0,0), LineTypeT::SEG );
        const Line3f setOne_paralellX_g_reverse( Vec3f(-1.01f,0,0), Vec3f(-1.01f,0,2), LineTypeT::SEG );
        const Line3f setOne_paralellX_h(         Vec3f(-1.01f,0,2), Vec3f(-1.01f,0,1), LineTypeT::SEG );
        const Line3f setOne_paralellX_h_reverse( Vec3f(-1.01f,0,1), Vec3f(-1.01f,0,2), LineTypeT::SEG );
        const Line3f setOne_paralellX_i(         Vec3f(-1.01f,0,3), Vec3f(-1.01f,0,2), LineTypeT::SEG );
        const Line3f setOne_paralellX_i_reverse( Vec3f(-1.01f,0,2), Vec3f(-1.01f,0,3), LineTypeT::SEG );
        
        ASSERT( ! Intersects( report, setOne_paralellX_a, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_a_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_b, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_b_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_c, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_c_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_d, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_d_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_e, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_e_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_f, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_f_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_g, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_g_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_h, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_h_reverse, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_i, aabox ) );
        ASSERT( ! Intersects( report, setOne_paralellX_i_reverse, aabox ) );
        
        const Line3f setTwo_miss_Left_a(          Vec3f(-0.75f,0,-3), Vec3f(-0.75f,0,-2), LineTypeT::SEG );
        const Line3f setTwo_miss_Left_a_reverse(  Vec3f(-0.75f,0,-2), Vec3f(-0.75f,0,-3), LineTypeT::SEG );
        const Line3f setTwo_graze_Left_b(         Vec3f(-0.75f,0,-2), Vec3f(-0.75f,0,-1), LineTypeT::SEG );
        const Line3f setTwo_graze_Left_b_reverse( Vec3f(-0.75f,0,-1), Vec3f(-0.75f,0,-2), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_c(           Vec3f(-0.75f,0,-2), Vec3f(-0.75f,0,0), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_c_reverse(   Vec3f(-0.75f,0,0), Vec3f(-0.75f,0,-2), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_d(           Vec3f(-0.75f,0,-1), Vec3f(-0.75f,0,0), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_d_reverse(   Vec3f(-0.75f,0,0), Vec3f(-0.75f,0,-1), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_e(           Vec3f(-0.75f,0,-0.2f), Vec3f(-0.75f,0,0.2f), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_e_reverse(   Vec3f(-0.75f,0,0.2f), Vec3f(-0.75f,0,-0.2f), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_f(           Vec3f(-0.75f,0,1), Vec3f(-0.75f,0,0), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_f_reverse(   Vec3f(-0.75f,0,0), Vec3f(-0.75f,0,1), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_g(           Vec3f(-0.75f,0,2), Vec3f(-0.75f,0,0), LineTypeT::SEG );
        const Line3f setTwo_hit_Left_g_reverse(   Vec3f(-0.75f,0,0), Vec3f(-0.75f,0,2), LineTypeT::SEG );
        const Line3f setTwo_graze_Left_h(         Vec3f(-0.75f,0,2), Vec3f(-0.75f,0,1), LineTypeT::SEG );
        const Line3f setTwo_graze_Left_h_reverse( Vec3f(-0.75f,0,1), Vec3f(-0.75f,0,2), LineTypeT::SEG );
        const Line3f setTwo_miss_Left_i(          Vec3f(-0.75f,0,3), Vec3f(-0.75f,0,2), LineTypeT::SEG );
        const Line3f setTwo_miss_Left_i_reverse(  Vec3f(-0.75f,0,2), Vec3f(-0.75f,0,3), LineTypeT::SEG );
        
        ASSERT( ! Intersects( report, setTwo_miss_Left_a, aabox ) );
        ASSERT( ! Intersects( report, setTwo_miss_Left_a_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_graze_Left_b, aabox ) );
        ASSERT( Intersects( report, setTwo_graze_Left_b_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_c, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_c_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_d, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_d_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_e, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_e_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_f, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_f_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_g, aabox ) );
        ASSERT( Intersects( report, setTwo_hit_Left_g_reverse, aabox ) );
        ASSERT( Intersects( report, setTwo_graze_Left_h, aabox ) );
        ASSERT( Intersects( report, setTwo_graze_Left_h_reverse, aabox ) );
        ASSERT( ! Intersects( report, setTwo_miss_Left_i, aabox ) );
        ASSERT( ! Intersects( report, setTwo_miss_Left_i_reverse, aabox ) );
        
        const Line3f setThree_paralellX_miss_a(         Vec3f(-1.0f,0,-3), Vec3f(-1.0f,0,-2), LineTypeT::SEG );
        const Line3f setThree_paralellX_miss_a_reverse( Vec3f(-1.0f,0,-2), Vec3f(-1.0f,0,-3), LineTypeT::SEG );
        const Line3f setThree_paralellX_grazeCorner_b(         Vec3f(-1.0f,0,-2), Vec3f(-1.0f,0,-1), LineTypeT::SEG );
        const Line3f setThree_paralellX_grazeCorner_b_reverse( Vec3f(-1.0f,0,-1), Vec3f(-1.0f,0,-2), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_c(         Vec3f(-1.0f,0,-2), Vec3f(-1.0f,0,0), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_c_reverse( Vec3f(-1.0f,0,0), Vec3f(-1.0f,0,-2), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_d(         Vec3f(-1.0f,0,-1), Vec3f(-1.0f,0,0), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_d_reverse( Vec3f(-1.0f,0,0), Vec3f(-1.0f,0,-1), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_e(         Vec3f(-1.0f,0,-0.2f), Vec3f(-1.0f,0,0.2f), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_e_reverse( Vec3f(-1.0f,0,0.2f), Vec3f(-1.0f,0,-0.2f), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_f(         Vec3f(-1.0f,0,1), Vec3f(-1.0f,0,0), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_f_reverse( Vec3f(-1.0f,0,0), Vec3f(-1.0f,0,1), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_g(         Vec3f(-1.0f,0,2), Vec3f(-1.0f,0,0), LineTypeT::SEG );
        const Line3f setThree_paralellX_graze_g_reverse( Vec3f(-1.0f,0,0), Vec3f(-1.0f,0,2), LineTypeT::SEG );
        const Line3f setThree_paralellX_grazeCorner_h(         Vec3f(-1.0f,0,2), Vec3f(-1.0f,0,1), LineTypeT::SEG );
        const Line3f setThree_paralellX_grazeCorner_h_reverse( Vec3f(-1.0f,0,1), Vec3f(-1.0f,0,2), LineTypeT::SEG );
        const Line3f setThree_paralellX_miss_i(         Vec3f(-1.0f,0,3), Vec3f(-1.0f,0,2), LineTypeT::SEG );
        const Line3f setThree_paralellX_miss_i_reverse( Vec3f(-1.0f,0,2), Vec3f(-1.0f,0,3), LineTypeT::SEG );
        
        ASSERT( ! Intersects( report, setThree_paralellX_miss_a, aabox ) );
        ASSERT( ! Intersects( report, setThree_paralellX_miss_a_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_grazeCorner_b, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_grazeCorner_b_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_c, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_c_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_d, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_d_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_e, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_e_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_f, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_f_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_g, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_graze_g_reverse, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_grazeCorner_h, aabox ) );
        ASSERT( Intersects( report, setThree_paralellX_grazeCorner_h_reverse, aabox ) );
        ASSERT( ! Intersects( report, setThree_paralellX_miss_i, aabox ) );
        ASSERT( ! Intersects( report, setThree_paralellX_miss_i_reverse, aabox ) );
        

        // ** Ray and infinite Line to AABB
        
        const Line3f ray_hit_b( Vec3f(0,0,10), Vec3f(0,0,5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_hit_b, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(0,0,1) ) );
        ASSERT( Line3f::Intersects( report, ray_hit_b, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(0,0,1) ) );
        
        const Line3f ray_hit_a( Vec3f(0,0,-10), Vec3f(0,0,-5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_hit_a, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(0,0,-1) ) );
        ASSERT( Line3f::Intersects( report, ray_hit_a, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(0,0,-1) ) );
        
        const Line3f ray_hit_c( Vec3f(10,0,0), Vec3f(5,0,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_hit_c, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(1,0,0) ) );
        ASSERT( Line3f::Intersects( report, ray_hit_c, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(1,0,0) ) );
        
        const Line3f ray_hit_diagonal( Vec3f(10,10,10), Vec3f(5,5,5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_hit_diagonal, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(1,1,1) ) );
        ASSERT( Line3f::Intersects( report, ray_hit_diagonal, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(1,1,1) ) );


        const Line3f ray_miss_opposite_b( Vec3f(0,0,5), Vec3f(0,0,10), LineTypeT::RAY );
        ASSERT( ! Line3f::Intersects( report, ray_miss_opposite_b, aabox ) );
        
        const Line3f line_hit_opposite_b( Vec3f(0,0,5), Vec3f(0,0,10), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_hit_opposite_b, aabox ) );
        //ASSERT( Maths::Approxf( report.point, Vec3f(0,0,1) ) );
        
        const Line3f ray_miss_opposite_a( Vec3f(0,0,-5), Vec3f(0,0,-10), LineTypeT::RAY );
        ASSERT( ! Line3f::Intersects( report, ray_miss_opposite_a, aabox ) );
        
        const Line3f line_hit_opposite_a( Vec3f(0,0,-5), Vec3f(0,0,-10), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_hit_opposite_a, aabox ) );
        //ASSERT( Maths::Approxf( report.point, Vec3f(0,0,-1) ) );
        
        const Line3f ray_miss_opposite_c( Vec3f(5,0,0), Vec3f(10,0,0), LineTypeT::RAY );
        ASSERT( ! Line3f::Intersects( report, ray_miss_opposite_c, aabox ) );
        
        const Line3f line_hit_opposite_c( Vec3f(5,0,0), Vec3f(10,0,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_hit_opposite_c, aabox ) );
        //ASSERT( Maths::Approxf( report.point, Vec3f(1,0,0) ) );
        
        const Line3f ray_miss_diagonal_opposite_d( Vec3f(5,5,5), Vec3f(10,10,10), LineTypeT::RAY );
        ASSERT( ! Line3f::Intersects( report, ray_miss_diagonal_opposite_d, aabox ) );
        
        const Line3f line_hit_diagonal_opposite_d( Vec3f(5,5,5), Vec3f(10,10,10), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_hit_diagonal_opposite_d, aabox ) );
        //ASSERT( Maths::Approxf( report.point, Vec3f(1,1,1) ) );
        
        
        const Line3f ray_miss_paralell_b( Vec3f(0,5,10), Vec3f(0,-5,10), LineTypeT::RAY );
        ASSERT( ! Line3f::Intersects( report, ray_miss_paralell_b, aabox ) );
        
        const Line3f line_miss_paralell_b( Vec3f(0,5,10), Vec3f(0,-5,10), LineTypeT::LINE );
        ASSERT( ! Line3f::Intersects( report, line_miss_paralell_b, aabox ) );

        const Line3f ray_miss_paralell_a( Vec3f(0,5,10), Vec3f(0,-5,10), LineTypeT::RAY );
        ASSERT( ! Line3f::Intersects( report, ray_miss_paralell_a, aabox ) );
        
        const Line3f line_miss_paralell_a( Vec3f(0,5,10), Vec3f(0,-5,10), LineTypeT::LINE );
        ASSERT( ! Line3f::Intersects( report, line_miss_paralell_a, aabox ) );

        const Line3f ray_hit_graze_paralell_b( Vec3f(-1,10,0), Vec3f(-1,5,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_hit_graze_paralell_b, aabox ) );
        
        const Line3f line_hit_graze_paralell_b( Vec3f(-1,10,0), Vec3f(-1,5,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_hit_graze_paralell_b, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(-1,1,0) ) );

        const Line3f ray_hit_graze_paralell_a( Vec3f(1,10,0), Vec3f(1,5,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_hit_graze_paralell_a, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(1,1,0) ) );
        
        const Line3f line_hit_graze_paralell_a( Vec3f(1,10,0), Vec3f(1,5,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_hit_graze_paralell_a, aabox ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(1,1,0) ) );
        
        
        const Line3f line_encompassed_a( Vec3f(-0.2f,0,0), Vec3f(0.2f,0,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_a, aabox ) );
        
        const Line3f ray_encompassed_a( Vec3f(-0.2f,0,0), Vec3f(0.2f,0,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_a, aabox ) );

        const Line3f line_encompassed_b( Vec3f(0.2f,0,0), Vec3f(-0.2f,0,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_b, aabox ) );
        
        const Line3f ray_encompassed_b( Vec3f(0.2f,0,0), Vec3f(-0.2f,0,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_b, aabox ) );

        const Line3f line_encompassed_c( Vec3f(0,-0.2f,0), Vec3f(0,0.2f,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_c, aabox ) );
        
        const Line3f ray_encompassed_c( Vec3f(0,-0.2f,0), Vec3f(0,0.2f,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_c, aabox ) );

        const Line3f line_encompassed_d( Vec3f(0,0.2f,0), Vec3f(0,-0.2f,0), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_d, aabox ) );
        
        const Line3f ray_encompassed_d( Vec3f(0,0.2f,0), Vec3f(0,-0.2f,0), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_d, aabox ) );
       
        const Line3f line_encompassed_e( Vec3f(0,0,-0.2f), Vec3f(0,0,0.2f), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_e, aabox ) );
        
        const Line3f ray_encompassed_e( Vec3f(0,0,-0.2f), Vec3f(0,0,0.2f), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_e, aabox ) );

        const Line3f line_encompassed_f( Vec3f(0,0,0.2f), Vec3f(0,0,-0.2f), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_f, aabox ) );
        
        const Line3f ray_encompassed_f( Vec3f(0,0,0.2f), Vec3f(0,0,-0.2f), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_f, aabox ) );


        const Line3f line_encompassed_diagonal_a( Vec3f(-0.2f,0.5,-0.5), Vec3f(0.2f,0.5,-0.5), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_diagonal_a, aabox ) );
        
        const Line3f ray_encompassed_diagonal_a( Vec3f(-0.2f,0.5,-0.5), Vec3f(0.2f,0.5,-0.5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_diagonal_a, aabox ) );

        const Line3f line_encompassed_diagonal_b( Vec3f(0.2f,0.5,-0.5), Vec3f(-0.2f,0.5,-0.5), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_diagonal_b, aabox ) );
        
        const Line3f ray_encompassed_diagonal_b( Vec3f(0.2f,0.5,-0.5), Vec3f(-0.2f,0.5,-0.5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_diagonal_b, aabox ) );

        const Line3f line_encompassed_diagonal_c( Vec3f(0.5,-0.2f,0), Vec3f(0.5,0.2f,-0.5), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_diagonal_c, aabox ) );
        
        const Line3f ray_encompassed_diagonal_c( Vec3f(0.5,-0.2f,0), Vec3f(0.5,0.2f,-0.5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_diagonal_c, aabox ) );

        const Line3f line_encompassed_diagonal_d( Vec3f(0.5,0.2f,0), Vec3f(0.5,-0.2f,-0.5), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_diagonal_d, aabox ) );
        
        const Line3f ray_encompassed_diagonal_d( Vec3f(0.5,0.2f,0), Vec3f(0.5,-0.2f,-0.5), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_diagonal_d, aabox ) );
       
        const Line3f line_encompassed_diagonal_e( Vec3f(0.5,-0.5,-0.2f), Vec3f(0.5,-0.5,0.2f), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_diagonal_e, aabox ) );
        
        const Line3f ray_encompassed_diagonal_e( Vec3f(0.5,-0.5,-0.2f), Vec3f(0.5,-0.5,0.2f), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_diagonal_e, aabox ) );

        const Line3f line_encompassed_diagonal_f( Vec3f(0.5,-0.5,0.2f), Vec3f(0.5,-0.5,-0.2f), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, line_encompassed_diagonal_f, aabox ) );
        
        const Line3f ray_encompassed_diagonal_f( Vec3f(0.5,-0.5,0.2f), Vec3f(0.5,-0.5,-0.2f), LineTypeT::RAY );
        ASSERT( Line3f::Intersects( report, ray_encompassed_diagonal_f, aabox ) );

        
        // ** infinite line to aabb
        
        const Line3f hit_b( Vec3f(0,0,-5), Vec3f(0,0,-2), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, hit_b, aabox ) );
        
        const Line3f hit_a( Vec3f(0,0,-2), Vec3f(0,0,-5), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, hit_a, aabox ) );
        
        const Line3f hit_c( Vec3f(0,0,2), Vec3f(0,0,5), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, hit_c, aabox ) );
        
        const Line3f hit_d( Vec3f(0,0,5), Vec3f(0,0,2), LineTypeT::LINE );
        ASSERT( Line3f::Intersects( report, hit_d, aabox ) );
        
        
        // ** AABox
        ASSERT( !Intersects( report, no1, aabox ) );
        ASSERT( !Intersects( report, no2, aabox ) );
        ASSERT( !Intersects( report, no3, aabox ) );
        ASSERT( !Intersects( report, no4, aabox ) );
        ASSERT( !Intersects( report, no5, aabox ) );
        ASSERT( !Intersects( report, no6, aabox ) );
        ASSERT( Intersects( report, intersect_fully_x, aabox ) );
        ASSERT( Intersects( report, intersect_fully_y, aabox ) );
        ASSERT( Intersects( report, intersect_fully_z, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tr_bl, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_br_tl, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_bl_tr, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tl_br, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_right, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_right, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_right, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_left, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_left, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_left, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_right2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_right2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_right2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_left2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_left2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_left2, aabox ) );
        ASSERT( Intersects( report, within_bottom, aabox ) );
        ASSERT( Intersects( report, within_edge, aabox ) );
        ASSERT( Intersects( report, touches_corner, aabox ) );
        ASSERT( !Intersects( report, no1, aabox ) );
        ASSERT( !Intersects( report, no2, aabox ) );
        ASSERT( !Intersects( report, no3, aabox ) );
        ASSERT( !Intersects( report, no4, aabox ) );
        ASSERT( !Intersects( report, no5, aabox ) );
        ASSERT( !Intersects( report, no6, aabox ) );
        ASSERT( Intersects( report, intersect_fully_x, aabox ) );
        ASSERT( Intersects( report, intersect_fully_y, aabox ) );
        ASSERT( Intersects( report, intersect_fully_z, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tr_bl, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_br_tl, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_bl_tr, aabox ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tl_br, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_right, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_right, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_right, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_left, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_left, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_left, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_right2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_right2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_right2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_x_left2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_y_left2, aabox ) );
        ASSERT( Intersects( report, intersect_partial_z_left2, aabox ) );
        ASSERT( Intersects( report, within_bottom, aabox ) );
        ASSERT( Intersects( report, within_edge, aabox ) );
        ASSERT( Intersects( report, touches_corner, aabox ) );

        // ** Box3D
        ASSERT( !Intersects( report, no1, box ) );
        ASSERT( !Intersects( report, no2, box ) );
        ASSERT( !Intersects( report, no3, box ) );
        ASSERT( !Intersects( report, no4, box ) );
        ASSERT( !Intersects( report, no5, box ) );
        ASSERT( !Intersects( report, no6, box ) );
        ASSERT( Intersects( report, intersect_fully_x, box ) );
        ASSERT( Intersects( report, intersect_fully_y, box ) );
        ASSERT( Intersects( report, intersect_fully_z, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tr_bl, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_br_tl, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_bl_tr, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tl_br, box ) );
        ASSERT( Intersects( report, intersect_partial_x_right, box ) );
        ASSERT( Intersects( report, intersect_partial_y_right, box ) );
        ASSERT( Intersects( report, intersect_partial_z_right, box ) );
        ASSERT( Intersects( report, intersect_partial_x_left, box ) );
        ASSERT( Intersects( report, intersect_partial_y_left, box ) );
        ASSERT( Intersects( report, intersect_partial_z_left, box ) );
        ASSERT( Intersects( report, intersect_partial_x_right2, box ) );
        ASSERT( Intersects( report, intersect_partial_y_right2, box ) );
        ASSERT( Intersects( report, intersect_partial_z_right2, box ) );
        ASSERT( Intersects( report, intersect_partial_x_left2, box ) );
        ASSERT( Intersects( report, intersect_partial_y_left2, box ) );
        ASSERT( Intersects( report, intersect_partial_z_left2, box ) );
        ASSERT( Intersects( report, within_bottom, box ) );
        ASSERT( Intersects( report, within_edge, box ) );
        ASSERT( Intersects( report, touches_corner, box ) );
        ASSERT( !Intersects( report, no1, box ) );
        ASSERT( !Intersects( report, no2, box ) );
        ASSERT( !Intersects( report, no3, box ) );
        ASSERT( !Intersects( report, no4, box ) );
        ASSERT( !Intersects( report, no5, box ) );
        ASSERT( !Intersects( report, no6, box ) );
        ASSERT( Intersects( report, intersect_fully_x, box ) );
        ASSERT( Intersects( report, intersect_fully_y, box ) );
        ASSERT( Intersects( report, intersect_fully_z, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tr_bl, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_br_tl, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_bl_tr, box ) );
        ASSERT( Intersects( report, intersect_fully_diagonal_tl_br, box ) );
        ASSERT( Intersects( report, intersect_partial_x_right, box ) );
        ASSERT( Intersects( report, intersect_partial_y_right, box ) );
        ASSERT( Intersects( report, intersect_partial_z_right, box ) );
        ASSERT( Intersects( report, intersect_partial_x_left, box ) );
        ASSERT( Intersects( report, intersect_partial_y_left, box ) );
        ASSERT( Intersects( report, intersect_partial_z_left, box ) );
        ASSERT( Intersects( report, intersect_partial_x_right2, box ) );
        ASSERT( Intersects( report, intersect_partial_y_right2, box ) );
        ASSERT( Intersects( report, intersect_partial_z_right2, box ) );
        ASSERT( Intersects( report, intersect_partial_x_left2, box ) );
        ASSERT( Intersects( report, intersect_partial_y_left2, box ) );
        ASSERT( Intersects( report, intersect_partial_z_left2, box ) );
        ASSERT( Intersects( report, within_bottom, box ) );
        ASSERT( Intersects( report, within_edge, box ) );
        ASSERT( Intersects( report, touches_corner, box ) );

        // **** boxes not centered around 0,0,0

        const float xo = 100;
        const float yo = 200;
        const float zo = 300;
        const float len = 500; // half line length

        const AABox3D uncentered_aabox=[&](void)->AABox3D{
            AABox3D val(-1,1,-1,1,-1,1);
            val.min.x += xo;
            val.max.x += xo;
            val.min.y += yo;
            val.max.y += yo;
            val.min.z += zo;
            val.max.z += zo;
            return val;
        }();
        const Box3D uncentered_box( uncentered_aabox );

        const Line3f straight_through_z( Vec3f(xo,yo,len), Vec3f(xo,yo,-len), LineTypeT::SEG );
        const Line3f straight_through_z_backward( Vec3f(xo,yo,-len), Vec3f(xo,yo,len), LineTypeT::SEG );
        const Line3f straight_through_x( Vec3f(len,yo,zo), Vec3f(-len,yo,zo), LineTypeT::SEG );
        const Line3f straight_through_x_backward( Vec3f(-len,yo,zo), Vec3f(len,yo,zo), LineTypeT::SEG );
        const Line3f straight_through_y( Vec3f(xo,len,zo), Vec3f(xo,-len,zo), LineTypeT::SEG );
        const Line3f straight_through_y_backward( Vec3f(xo,-len,zo), Vec3f(xo,len,zo), LineTypeT::SEG );

        // ** AABox

        ASSERT( Intersects( report, straight_through_x, uncentered_aabox) );
        ASSERT( Intersects( report, straight_through_y, uncentered_aabox) );
        ASSERT( Intersects( report, straight_through_z, uncentered_aabox) );

        // ** Box3D

        ASSERT( Intersects( report, straight_through_x, uncentered_box) );
        ASSERT( Intersects( report, straight_through_y, uncentered_box) );
        ASSERT( Intersects( report, straight_through_z, uncentered_box) );

        // **** oblong, longer on x uncenteres

        const AABox3D oblongx_aabox=[&](void)->AABox3D{
            AABox3D val(-1,1,-1,1,-1,1);
            val.min.x += 100;
            val.max.x += 200;
            val.min.y += 100;
            val.max.y += 100;
            val.min.z += 100;
            val.max.z += 100;
            return val;
        }();
        const AABox3D oblongx_box( oblongx_aabox );

        const Line3f line_through_z_( Vec3f(150,100,200), Vec3f(150,100,-200), LineTypeT::SEG );

        // ** AABox
        ASSERT( Intersects( report, line_through_z_, oblongx_aabox) );

        // ** Box
        ASSERT( Intersects( report, line_through_z_, Box3D( oblongx_box) ) );

        // line top-down through oblong box

        const AABox3D oblongy_aabox=[&](void)->AABox3D{
            AABox3D val(-1,1,-1,1,-1,1);
            val.min.y += 100;
            val.max.y += 200;
            return val;
        }();

        const Line3f topdownz( Vec3f(0,100,4), Vec3f(0,100,-4), LineTypeT::SEG );

        // ** AABox
        ASSERT( Intersects( report, topdownz, oblongy_aabox) );

        // ** Box
        ASSERT( Intersects( report, topdownz, Box3D(oblongy_aabox) ) );

    }

    void Line3f::UnitTest_Seg_Plane( void ) {
        CollisionReport3D report;

        // clockwise
        Vec3f a( -3,3,3 );
        Vec3f b( 3,3,3 );
        Vec3f c( 3,-3,3 );
        Line3f yes_touching_inside(  Vec3f(-1,2,3 ), Vec3f( -1,2,4 ), LineTypeT::SEG );
        Line3f yes_touching_on_corner(  Vec3f(3,-3,3 ), Vec3f( 3,-3,4 ), LineTypeT::SEG );
        Line3f yes_touching_on_edge(  Vec3f(-2,2,3 ), Vec3f( -2,2,4 ), LineTypeT::SEG );
        Line3f yes_touching_outside(  Vec3f(7,7,3 ), Vec3f( 7,7,4 ), LineTypeT::SEG );

        Line3f yes_penetrating_inside(  Vec3f(-1,2,2 ), Vec3f( -1,2,4 ), LineTypeT::SEG );
        Line3f yes_penetrating_on_corner(  Vec3f(3,-3,2 ), Vec3f( 3,-3,4 ), LineTypeT::SEG );
        Line3f yes_penetrating_on_edge(  Vec3f(-2,2,2 ), Vec3f( -2,2,4 ), LineTypeT::SEG );
        Line3f yes_penetrating_outside(  Vec3f(7,7,2 ), Vec3f( 7,7,4 ), LineTypeT::SEG );

        // one unit behind (a,b,c)
        Line3f no_parallel_behind_plane( Vec3f(3,3,4), Vec3f(3,2,4), LineTypeT::SEG );
        Line3f no_parallel_but_very_close( Vec3f(3,3,3-0.01f), Vec3f(3,2,3-0.01f), LineTypeT::SEG );
        Line3f yes_parallel( Vec3f(3,3,3), Vec3f(3,2,3), LineTypeT::SEG );

        ASSERT( ToPlane( report, yes_penetrating_inside, a,b,c ) );
        ASSERT( ToPlane( report, yes_penetrating_on_corner, a,b,c ) );
        ASSERT( ToPlane( report, yes_penetrating_on_edge, a,b,c ) );
        ASSERT( ToPlane( report, yes_penetrating_outside, a,b,c ) );

        ASSERT( ToPlane( report, yes_touching_inside, a,b,c ) );
        ASSERT( ToPlane( report, yes_touching_on_corner, a,b,c ) );
        ASSERT( ToPlane( report, yes_touching_on_edge, a,b,c ) );
        ASSERT( ToPlane( report, yes_touching_outside, a,b,c ) );

        ASSERT( ToPlane( report, yes_parallel, a,b,c ) );

        ASSERT( ! ToPlane( report, no_parallel_behind_plane, a,b,c ) );
        ASSERT( ! ToPlane( report, no_parallel_but_very_close, a,b,c ) );
    }

    void Line3f::UnitTest_Seg_CoplanarPoly( void ) {
        // a (non-paralelepiped) hexagon centered on the origin
        // clockwise

        const std::size_t left_bottom=0;
        const std::size_t left_top=1;
        const std::size_t top_left=2;
        const std::size_t top_right=3;
        const std::size_t right_top=4;
        const std::size_t right_bottom=5;
        const std::size_t bottom_right=6;
        const std::size_t bottom_left=7;

        const std::size_t numVerts=8;

        Vec3f verts[numVerts];
        verts[left_bottom].Set( -2, -1, 0 );
        verts[left_top].Set( -2, 1, 0 );
        verts[top_left].Set( -1, 2, 0 );
        verts[top_right].Set( 1, 2, 0 );
        verts[right_top].Set( 2, 1, 0 );
        verts[right_bottom].Set( 2, -1, 0 );
        verts[bottom_right].Set( 1, -2, 0 );
        verts[bottom_left].Set( -1, -2, 0 );

        // Coplanar
        Line3f succeed_coplanar_edge_cascade( Vec3f(2, 1.5f, 0), Vec3f(2, -0.5f, 0), LineTypeT::SEG );
        Line3f succeed_coplanar_edge_cascade2( Vec3f(2, -0.5f, 0), Vec3f(2, -1.5f, 0), LineTypeT::SEG );

        Line3f succeed_coplanar_end_touches_edge( Vec3f(5,0,0), Vec3f(2,0,0), LineTypeT::SEG );

        Line3f succeed_coplanar_start_touches_corner( verts[right_top], Vec3f(5,0,0), LineTypeT::SEG );
        Line3f succeed_coplanar_end_touches_corner( Vec3f(5,0,0), verts[right_top], LineTypeT::SEG );

        Line3f succeed_coplanar_penetrate_corner( Vec3f(2,2,0), Vec3f(3,0,0), LineTypeT::SEG );

        Line3f succeed_coplanar_inside( Vec3f(-1,-1,0), Vec3f(1,1,0), LineTypeT::SEG );
        Line3f fail_coplanar_outside_vertdist7a( Vec3f(-10,-1,0), Vec3f(-9,1,0), LineTypeT::SEG );
        Line3f fail_coplanar_outside_vertdist7b( Vec3f(-9,1,0), Vec3f(-10,-1,0), LineTypeT::SEG );
        Line3f fail_coplanar_outside_linedist2( Vec3f(-5,-1,0), Vec3f(-3,3,0), LineTypeT::SEG );
        Line3f fail_parallel_not_coplanar_outside( Vec3f(-10,-1,1.01f), Vec3f(-9,1,1), LineTypeT::SEG );
        Line3f fail_parallel_not_coplanar_infront( Vec3f(-1,-1,1.01f), Vec3f(1,1,1), LineTypeT::SEG );
        Line3f fail_outside( Vec3f(-10,-1,1), Vec3f(-9,1,2), LineTypeT::SEG );

        Line3f succeed_coplanar_penetrate_face_no_verts( Vec3f(-3,-3,0), Vec3f(3,3,0), LineTypeT::SEG );
        Line3f succeed_coplanar_halfpenetrate_face_no_verts( Vec3f(-3,-3,0), Vec3f(0,0,0), LineTypeT::SEG );

        Line3f succeed_penetrate_face( Vec3f(0,0,-1), Vec3f(0,0,1), LineTypeT::SEG );
        Line3f succeed_penetrate_corner( Vec3f(2,1,-1), Vec3f(2,1,1), LineTypeT::SEG );
        Line3f succeed_penetrate_edge( Vec3f(2,0,-1), Vec3f(2,0,1), LineTypeT::SEG );

        Line3f succeed_start_touch_face( Vec3f(0,0,0), Vec3f(0,0,5), LineTypeT::SEG );
        Line3f succeed_end_touch_face( Vec3f(0,0,5), Vec3f(0,0,0), LineTypeT::SEG );

        Line3f fail_not_quite_before_face( Vec3f(0,0,0.1f), Vec3f(0,0,5), LineTypeT::SEG );
        Line3f fail_not_quite_behind_face( Vec3f(0,0,5), Vec3f(0,0,0.1f), LineTypeT::SEG );

        Line3f fail_penetrate_plane_outside( Vec3f(5,0,1), Vec3f(5,0,-1), LineTypeT::SEG );

        Line3f succeed_end_touch_edge( Vec3f(2,0,4), Vec3f(2,0,0), LineTypeT::SEG );
        Line3f succeed_start_touch_edge( Vec3f(2,0,0), Vec3f(2,0,4), LineTypeT::SEG );

        Line3f succeed_start_touch_corner( Vec3f(2,1,0), Vec3f(2,1,4), LineTypeT::SEG );
        Line3f succeed_end_touch_corner( Vec3f(2,1,4), Vec3f(2,1,0), LineTypeT::SEG );

        CollisionReport3D report;


        // *---* poly edge
        // *---* line
        Line3f succeed_coplanar_same_as_edge( verts[right_top], verts[right_bottom], LineTypeT::SEG );
        ASSERT( ToPoly( report, succeed_coplanar_same_as_edge, verts, numVerts ) );
        ASSERT( Maths::Approxf( report.time, 0.0f ) );

        // *--------* poly edge
        //   *---* line
        Line3f succeed_coplanar_within_edge( Vec3f(2, 0.5f, 0), Vec3f(2, -0.5f, 0), LineTypeT::SEG );
        ASSERT( ToPoly( report, succeed_coplanar_within_edge, verts, numVerts ) );
        ASSERT( Maths::Approxf( report.time, 0.0f ) );

        // *--------* poly edge
        // *---* line
        Line3f succeed_coplanar_half_edge( Vec3f(2, 1, 0), Vec3f(2, -0.5f, 0), LineTypeT::SEG );
        ASSERT( ToPoly( report, succeed_coplanar_half_edge, verts, numVerts ) );
        ASSERT( Maths::Approxf( report.time, 0.0f ) );

        // *-------* poly edge
        //   *---* line
        Line3f succeed_coplanar_edge_within( Vec3f(2, 2, 0), Vec3f(2, -2, 0), LineTypeT::SEG );
//todo        ASSERT( ToPoly( report, succeed_coplanar_edge_within, verts, numVerts ) );
//        ASSERT( report.time == 0 );

        // *-------* poly edge
        //         *---* line
        Line3f succeed_coplanar_start_touches_edge( Vec3f(2,0,0), Vec3f(5,0,0), LineTypeT::SEG );
        ASSERT( ToPoly( report, succeed_coplanar_start_touches_edge, verts, numVerts ) );
        //ASSERT( report.time == 0 );

        ASSERT( ToPoly( report, succeed_coplanar_end_touches_edge, verts, numVerts ) );
        //ASSERT( report.time == 0 );
        ASSERT( ToPoly( report, succeed_coplanar_start_touches_corner, verts, numVerts ) );
        //ASSERT( report.time == 0 );
        ASSERT( ToPoly( report, succeed_coplanar_end_touches_corner, verts, numVerts ) );
        //ASSERT( report.time == 0 );
//todo        ASSERT( ToPoly( report, succeed_coplanar_penetrate_corner, verts, numVerts ) );
        //ASSERT( report.time == 0 );
        ASSERT( ToPoly( report, succeed_coplanar_inside, verts, numVerts ) );
        //ASSERT( report.time == 0 );
        ASSERT( ToPoly( report, succeed_coplanar_edge_cascade, verts, numVerts ) );
        //ASSERT( report.time == 0 );
        ASSERT( ToPoly( report, succeed_coplanar_edge_cascade2, verts, numVerts ) );
        //ASSERT( report.time == 0 );
        ASSERT( ! ToPoly( report, fail_coplanar_outside_vertdist7a, verts, numVerts ) );
//todo        ASSERT( report.time == 7 );
        ASSERT( ! ToPoly( report, fail_coplanar_outside_vertdist7b, verts, numVerts ) );
//todo        ASSERT( report.time == 7 );
        ASSERT( ! ToPoly( report, fail_coplanar_outside_linedist2, verts, numVerts ) );
//todo        ASSERT( report.time == 2 );
        ASSERT( ! ToPoly( report, fail_parallel_not_coplanar_outside, verts, numVerts ) );
        ASSERT( ! ToPoly( report, fail_parallel_not_coplanar_infront, verts, numVerts ) );
        ASSERT( ! ToPoly( report, fail_outside, verts, numVerts ) );
//todo        ASSERT( ToPoly( report, succeed_coplanar_penetrate_face_no_verts, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_coplanar_halfpenetrate_face_no_verts, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_penetrate_face, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_penetrate_corner, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_penetrate_edge, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_start_touch_face, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_end_touch_face, verts, numVerts ) );
        ASSERT( ! ToPoly( report, fail_not_quite_behind_face, verts, numVerts ) );
        ASSERT( ! ToPoly( report, fail_not_quite_before_face, verts, numVerts ) );
        ASSERT( ! ToPoly( report, fail_penetrate_plane_outside, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_end_touch_edge, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_start_touch_edge, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_start_touch_corner, verts, numVerts ) );
        ASSERT( ToPoly( report, succeed_end_touch_corner, verts, numVerts ) );
    }

    void Line3f::UnitTest_Seg_ToBox3D( void ) {
        Box3D box(0,2,0,2,0,2);
        box += Vec3f(2,3,0);

        CollisionReport3D report;

        // ***** NO INTERSECT
        Line3f a( Vec3f(3,1,0), Vec3f(6,5,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, a.line_type, a.vert[0], a.vert[1], box ) );
        ASSERT( !ToBox3D( report, a.line_type, a.vert[1], a.vert[0], box ) );
        
        Line3f b( Vec3f(  1,5.1f,0), Vec3f( 3,5.1f,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, b.line_type, b.vert[0], b.vert[1], box ) );
        ASSERT( !ToBox3D( report, b.line_type, b.vert[1], b.vert[0], box ) );

        Line3f c( Vec3f(  2,5.1f,0), Vec3f( 7,5.1f,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, c.line_type, c.vert[0], c.vert[1], box ) );
        ASSERT( !ToBox3D( report, c.line_type, c.vert[1], c.vert[0], box ) );

        Line3f d( Vec3f(-10,5.1f,0), Vec3f(10,5.1f,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, d.line_type, d.vert[0], d.vert[1], box ) );
        ASSERT( !ToBox3D( report, d.line_type, d.vert[1], d.vert[0], box ) );

        Line3f e( Vec3f(4.1f,4.5f,0), Vec3f(4.1f,3.5f,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, e.line_type, e.vert[0], e.vert[1], box ) );
        ASSERT( !ToBox3D( report, e.line_type, e.vert[1], e.vert[0], box ) );

        Line3f f( Vec3f(4.1f,5,0), Vec3f(4.1f,7,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, f.line_type, f.vert[0], f.vert[1], box ) );
        ASSERT( !ToBox3D( report, f.line_type, f.vert[1], f.vert[0], box ) );

        Line3f g( Vec3f(4.1f,0,0), Vec3f(4.1f,1,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, g.line_type, g.vert[0], g.vert[1], box ) );
        ASSERT( !ToBox3D( report, g.line_type, g.vert[1], g.vert[0], box ) );

        Line3f h( Vec3f(4.1f,0,0), Vec3f(4.1f,10,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, h.line_type, h.vert[0], h.vert[1], box ) );
        ASSERT( !ToBox3D( report, h.line_type, h.vert[1], h.vert[0], box ) );

        Line3f i( Vec3f(2,6,0), Vec3f(3,6,0), LineTypeT::SEG );
        ASSERT( !ToBox3D( report, i.line_type, i.vert[0], i.vert[1], box ) );
        ASSERT( !ToBox3D( report, i.line_type, i.vert[1], i.vert[0], box ) );

        // ***** INTERSECTIONS
        Line3f j( Vec3f(2.5f,3.5f,1), Vec3f(3.5f,4.5f,1), LineTypeT::SEG ); //within
        ASSERT( ToBox3D( report, j.line_type, j.vert[0], j.vert[1], box ) );
        ASSERT( ToBox3D( report, j.line_type, j.vert[1], j.vert[0], box ) );

        Line3f k( Vec3f(3,4,0), Vec3f(6,4,0), LineTypeT::SEG );
        ASSERT( ToBox3D( report, k.line_type, k.vert[0], k.vert[1], box ) );
        ASSERT( ToBox3D( report, k.line_type, k.vert[1], k.vert[0], box ) );

        Line3f l( Vec3f(1,0,0), Vec3f(4,4,0), LineTypeT::SEG );
        ASSERT( ToBox3D( report, l.line_type, l.vert[0], l.vert[1], box ) );
        ASSERT( ToBox3D( report, l.line_type, l.vert[1], l.vert[0], box ) );

        Line3f m( Vec3f(1,4,0), Vec3f(5,4,0), LineTypeT::SEG );
        ASSERT( ToBox3D( report, m.line_type, m.vert[0], m.vert[1], box ) );
        ASSERT( ToBox3D( report, m.line_type, m.vert[1], m.vert[0], box ) );

        Line3f n( Vec3f(1,5,0), Vec3f(5,5,0), LineTypeT::SEG );
        ASSERT( ToBox3D( report, n.line_type, n.vert[0], n.vert[1], box ) );
        ASSERT( ToBox3D( report, n.line_type, n.vert[1], n.vert[0], box ) );

        Line3f o( Vec3f(2,2,0), Vec3f(2,6,0), LineTypeT::SEG ); //along edge
        ASSERT( ToBox3D( report, o.line_type, o.vert[0], o.vert[1], box ) );
        ASSERT( ToBox3D( report, o.line_type, o.vert[1], o.vert[0], box ) );

        Line3f p( Vec3f(2,2,1), Vec3f(2,6,1), LineTypeT::SEG ); //along face
        ASSERT( ToBox3D( report, p.line_type, p.vert[0], p.vert[1], box ) );
        ASSERT( ToBox3D( report, p.line_type, p.vert[1], p.vert[0], box ) );

        Line3f q( Vec3f(1,4,0), Vec3f(3,6,0), LineTypeT::SEG ); // grazes corner
        ASSERT( ToBox3D( report, q.line_type, q.vert[0], q.vert[1], box ) );
        ASSERT( ToBox3D( report, q.line_type, q.vert[1], q.vert[0], box ) );

        Line3f r( Vec3f(1,4,1), Vec3f(3,6,1), LineTypeT::SEG ); // grazes edge
        ASSERT( ToBox3D( report, r.line_type, r.vert[0], r.vert[1], box ) );
        ASSERT( ToBox3D( report, r.line_type, r.vert[1], r.vert[0], box ) );

        Line3f s( Vec3f(1,5.1f,0), Vec3f( 3,1.5f,0), LineTypeT::SEG );
        ASSERT( ToBox3D( report, s.line_type, s.vert[0], s.vert[1], box ) );
        ASSERT( ToBox3D( report, s.line_type, s.vert[1], s.vert[0], box ) );

        Line3f t( Vec3f(  2,5.1f,0), Vec3f( 7,1.5f,0), LineTypeT::SEG );
        ASSERT( ToBox3D( report, t.line_type, t.vert[0], t.vert[1], box ) );
        ASSERT( ToBox3D( report, t.line_type, t.vert[1], t.vert[0], box ) );

        Line3f v( Vec3f(2,6,0), Vec3f(3,5,0), LineTypeT::SEG ); // line's vertice is on the box's edge
        ASSERT( ToBox3D( report, v.line_type, v.vert[0], v.vert[1], box ) );
        ASSERT( ToBox3D( report, v.line_type, v.vert[1], v.vert[0], box ) );

    }
#endif //CLIB_UNIT_TEST
