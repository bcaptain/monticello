// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./point.h"
#include "../../collision/collision.h"
#include "../../../rendering/models/model.h"

bool Collision3D::TestPoint::SweepToModel( CollisionReport3D& report, const Vec3f& fromPoint, const Vec3f& toPoint, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {
    if ( model.NumMeshes() < 1 )
        return false;
        
    Mat3f model_rotation;
    const bool do_model_rotation = model_angles != Vec3f(0,0,0);
    if ( do_model_rotation ) {
        model_rotation.RotateZYX( model_angles );
    }
    
    CollisionReportTemp3D closest_report;
    bool collision_occured = false;
    std::vector<Vec3f> verts(3);
    
    for ( std::size_t m=0; m<model.NumMeshes(); ++m ) {
        const Mesh* mesh = model.GetMesh(m);

        // must find closest face
        for ( auto const& face : mesh->faces ) {
            if ( face.vert.Num() < 1 )
                continue;

            if ( face.vert.Num() > verts.size() )
                verts.resize( face.vert.Num() * 2 );

            std::size_t v=0;
            for ( auto const& vert : face.vert ) {
                if ( do_model_rotation ) {
                    verts[v] = mesh->verts[ vert.vertIndex ].co * model_rotation;
                } else {
                    verts[v] = mesh->verts[ vert.vertIndex ].co;
                }
                verts[v] += model_origin;
                ++v;
            }

            if ( Line3f::ToPoly( report, LineTypeT::SEG, fromPoint, toPoint, verts.data(), verts.size() ) ) {
                if ( clipType == ModelCollisionT::ANY_POLY ) {
                    return true;
                }
                if ( !collision_occured || report.time < closest_report.time ) {
                    collision_occured = true;
                    closest_report = report;
                }
            }
        }
    }

    if ( collision_occured ) {
        report = closest_report;
    }

    return collision_occured;
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Collision3D::TestPoint::ToModel( const Vec3f& line, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
        ERR("Not Implemented.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Cylinder& cyl ) {
        ERR("Not Implemented.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Capsule& cap ) {
        ERR("Not Implemented.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Line3f& line ) {
        ERR("Not Implemented.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& point_source, const Vec3f& point_dest, const Vec3f& _point ) {
        ERR("Not Implemented.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Collision3D::TestPoint::Intersects( const Vec3f& point1, const Vec3f& point2 ) {
    return (point1-point2).SquaredLen() < EP*EP;
}

bool Collision3D::TestPoint::Intersects( const Vec3f& point, const Sphere& sphere ) {
    return Vec3f::SquaredLen( sphere.origin - point ) <= sphere.radius * sphere.radius;
}

float Collision3D::TestPoint::SquaredDistToAABox3D( const Vec3f& point, const AABox3D& aabox, Vec3f* norm_out ) {
    float dist=0.0f;
    float tmp;

    if ( norm_out )
        
        norm_out->Zero();

    if ( point.x < aabox.min.x ) {
        tmp = point.x;
        tmp -= aabox.min.x;
        dist = tmp * tmp;
        if ( norm_out ) {
            norm_out->x = dist;
        }
    } else if ( point.x > aabox.max.x ) {
        tmp = point.x - aabox.max.x;
        dist = tmp * tmp;
        if ( norm_out ) {
            norm_out->x = dist;
            norm_out->x = -norm_out->x;
        }
    }

    if ( point.y < aabox.min.y ) {
        tmp = point.y;
        tmp -= aabox.min.y;
        if ( norm_out ) {
            norm_out->y = tmp * tmp;
            dist += norm_out->y;
        } else {
            dist += tmp * tmp;
        }
    } else if ( point.y > aabox.max.y ) {
        tmp = point.y;
        tmp -= aabox.max.y;
        if ( norm_out ) {
            norm_out->y = -(tmp * tmp);
            dist += -norm_out->y;
        } else {
            dist += tmp * tmp;
        }
    }

    if ( point.z < aabox.min.z ) {
        tmp = point.z;
        tmp -= aabox.min.z;
        if ( norm_out ) {
            norm_out->z = tmp * tmp;
            dist += norm_out->z;
        } else {
            dist += tmp * tmp;
        }
    } else if ( point.z > aabox.max.z ) {
        tmp = point.z;
        tmp -= aabox.max.z;
        if ( norm_out ) {
            norm_out->z = -(tmp * tmp);
            dist += -norm_out->z;
        } else {
            dist += tmp * tmp;
        }
    }

    if ( norm_out )
        norm_out->Normalize();

    if ( dist < 0 )
        return 0;
    return dist;
}

//clockwise
bool Collision3D::TestPoint::Intersects( const Vec3f& point, const Box3D& box ) {
    return ( // returning true if point is not in front of any face
        !TestPoint::BeforePlane( point, box.GetVert_NearLeftBottom(), box.GetBottomNormal() ) &&
        !TestPoint::BeforePlane( point, box.GetVert_NearLeftTop(), box.GetTopNormal() ) &&
        !TestPoint::BeforePlane( point, box.GetVert_NearLeftBottom(), box.GetLeftNormal() ) &&
        !TestPoint::BeforePlane( point, box.GetVert_NearRightBottom(), box.GetRightNormal() ) &&
        !TestPoint::BeforePlane( point, box.GetVert_FarLeftBottom(), box.GetFarNormal() ) &&
        !TestPoint::BeforePlane( point, box.GetVert_NearLeftBottom(), box.GetNearNormal() )
    );
}

bool Collision3D::TestPoint::ToPoly( const Vec3f& point, const Vec3f* clockwise_verts, const std::size_t vert_count, const float epsilon ) {
    ASSERT( vert_count > 2 );
    return ToPoly( point, clockwise_verts, vert_count, Plane3f::GetNormal( clockwise_verts ), epsilon );
}

bool Collision3D::TestPoint::ToPoly( const Vec3f& point, const Vec3f* clockwise_verts, const std::size_t vert_count, const Vec3f& plane_normal, const float epsilon ) {
    ASSERT( vert_count >= 3 );
    ASSERT( clockwise_verts );

    if ( !OnPlane( point, clockwise_verts[0], plane_normal, epsilon ) )
        return false;

    for ( std::size_t a=vert_count-1,b=0; b < vert_count; a=b++ ) {
        const Vec3f edge_dir( ( clockwise_verts[b] - clockwise_verts[a] ).GetNormalized() );
        const Vec3f edge_normal( plane_normal.Cross( edge_dir ).GetNormalized() );
        if ( BeforePlane( point, clockwise_verts[a], edge_normal ) )
            return false;
    }

    return true;
}

bool Collision3D::TestPoint::ToLine( const Vec3f& point, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end, const float epsilon ) {
    return Maths::Approxf( Line3f::GetDistToPoint( line_type, line_start, line_end, point ), 0.0f, epsilon );
}

bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& point, const Vec3f& point_dest, const Sphere& sphere ) {
    return Sphere::SweepTo( report, Sphere(point,0), point_dest, sphere );
}

bool Collision3D::TestPoint::Intersects( const Vec3f& point, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, point );
}

bool Collision3D::TestPoint::Intersects( const Vec3f& point, const Capsule& cap ) {
    return Capsule::Intersects( cap, point );
}

#ifdef CLIB_UNIT_TEST
    void Collision3D::TestPoint::UnitTest( void ) {
        UnitTest_Line();
        UnitTest_LineSeg();
        UnitTest_Plane();
        UnitTest_Sphere();
        UnitTest_InsideAABox3D();
        UnitTest_CoplanarPoly();
        UnitTest_DistToAABox3D();
    }

    void Collision3D::TestPoint::UnitTest_Plane( void ) {
        // plane laying on the x/y axes, facing down, normal is (0,0,-1)
        const Vec3f a(-2,2,0);
        const Vec3f b(2,2,0);
        const Vec3f c(2,-2,0);

        const Vec3f succeed_middle(1,1,0);
        const Vec3f succeed_on_edge(0,0,0);
        const Vec3f succeed_on_corner(-2,2,0);
        const Vec3f behind(1,1,-1);
        const Vec3f before(1,1,1);
        const Vec3f coplanar_not_on(-2,3,0);

        ASSERT( OnPlane( succeed_middle, a, b, c ) );
        ASSERT( OnPlane( succeed_on_edge, a, b, c ) );
        ASSERT( OnPlane( succeed_on_corner, a, b, c ) );
        ASSERT( BehindPlane( behind, a, b, c ) );
        ASSERT( BeforePlane( before, a, b, c ) );
        ASSERT( OnPlane( coplanar_not_on, a, b, c ) );
    }

    void Collision3D::TestPoint::UnitTest_LineSeg( void ) {
        const Vec3f succeed_start(0,-1,0);
        const Vec3f succeed_end(0,1,0);

        const Line3f line( succeed_start, succeed_end, LineTypeT::SEG );

        const Vec3f succeed_on(0,0,0);
        const Vec3f fail_before_start(0,-1.1f,0);
        const Vec3f fail_past_end(0,1.1f,0);
        const Vec3f fail_left(1,0,0);
        const Vec3f fail_right(-1,0,0);
        const Vec3f fail_behind(0,0,-1);
        const Vec3f fail_infront(0,0,1);

        ASSERT( Intersects( succeed_on, line ) );
        ASSERT( Intersects( succeed_start, line ) );
        ASSERT( Intersects(succeed_end, line ) );
        ASSERT( ! Intersects( fail_before_start, line) );
        ASSERT( ! Intersects( fail_past_end, line) );
        ASSERT( ! Intersects( fail_left, line) );
        ASSERT( ! Intersects( fail_right, line) );
        ASSERT( ! Intersects( fail_behind, line) );
        ASSERT( ! Intersects( fail_infront, line) );
    }

    void Collision3D::TestPoint::UnitTest_Line( void ) {
        const Vec3f succeed_start(0,-1,0);
        const Vec3f succeed_end(0,1,0);

        const Line3f line( succeed_start, succeed_end, LineTypeT::LINE );

        const Vec3f succeed_on(0,0,0);
        const Vec3f succeed_before_start(0,-1.1f,0);
        const Vec3f succeed_past_end(0,1.1f,0);
        const Vec3f fail_left(1,0,0);
        const Vec3f fail_right(-1,0,0);
        const Vec3f fail_behind(0,0,-1);
        const Vec3f fail_infront(0,0,1);

        ASSERT( Intersects( succeed_on, line) );
        ASSERT( Intersects( succeed_start, line ) );
        ASSERT( Intersects( succeed_end, line ) );
        ASSERT( Intersects( succeed_before_start, line) );
        ASSERT( Intersects( succeed_past_end, line) );
        ASSERT( ! Intersects( fail_left, line) );
        ASSERT( ! Intersects( fail_right, line) );
        ASSERT( ! Intersects( fail_behind, line) );
        ASSERT( ! Intersects( fail_infront, line) );
    }

    void Collision3D::TestPoint::UnitTest_Sphere( void ) {
        Sphere sphere( Vec3f(0,0,0), 10 );

        const Vec3f fail_outside(9,9,0);
        const Vec3f fail_way_outside(10,10,0);
        const Vec3f succeed_other_edge(10,0,0);
        const Vec3f succeed_edge(-10,0,0);
        const Vec3f succeed_inside(0,0,0);

        ASSERT( ! Intersects( fail_way_outside, sphere ) );
        ASSERT( ! Intersects( fail_outside, sphere ) );
        ASSERT( Intersects( succeed_inside, sphere ) );
        ASSERT( Intersects( succeed_edge, sphere ) );
        ASSERT( Intersects( succeed_other_edge, sphere ) );
    }

    void Collision3D::TestPoint::UnitTest_InsideAABox3D( void ) {
        AABox3D aabox( -2,2,-2,2,-2,2 );
        const Vec3f succeed_inside(1,1,1);
        const Vec3f succeed_on_face_top(0,0,2);
        const Vec3f succeed_on_face_bottom(0,0,-2);
        const Vec3f succeed_on_face_left(-2,0,0);
        const Vec3f succeed_on_face_right(2,0,0);
        const Vec3f succeed_on_face_near(0,-2,0);
        const Vec3f succeed_on_face_far(0,2,0);
        const Vec3f succeed_on_edge_top_1(2,0,2);
        const Vec3f succeed_on_edge_top_2(-2,0,2);
        const Vec3f succeed_on_edge_top_3(0,-2,2);
        const Vec3f succeed_on_edge_top_4(0,2,2);
        const Vec3f succeed_on_edge_bottom_1(2,0,-2);
        const Vec3f succeed_on_edge_bottom_2(-2,0,-2);
        const Vec3f succeed_on_edge_bottom_3(0,-2,-2);
        const Vec3f succeed_on_edge_bottom_4(0,2,-2);
        const Vec3f succeed_on_edge_sides_1(2,2,0);
        const Vec3f succeed_on_edge_sides_2(2,-2,0);
        const Vec3f succeed_on_edge_sides_3(-2,-2,0);
        const Vec3f succeed_on_edge_sides_4(-2,2,0);
        const Vec3f succeed_on_corner_1(2,2,2);
        const Vec3f succeed_on_corner_2(2,2,-2);
        const Vec3f succeed_on_corner_3(2,-2,2);
        const Vec3f succeed_on_corner_4(2,-2,-2);
        const Vec3f succeed_on_corner_5(-2,2,2);
        const Vec3f succeed_on_corner_6(-2,2,-2);
        const Vec3f succeed_on_corner_7(-2,-2,2);
        const Vec3f succeed_on_corner_8(-2,-2,-2);

        // random outside tests
        const Vec3f succeed_outside_1(2.01f,2,2);
        const Vec3f succeed_outside_2(2,2.01f,2);
        const Vec3f succeed_outside_3(2,2,2.01f);
        const Vec3f succeed_outside_4(2.01f,2,2);
        const Vec3f succeed_outside_5(-2.01f,2,2);
        const Vec3f succeed_outside_6(-2,2.01f,2);
        const Vec3f succeed_outside_7(-2,2,2.01f);
        const Vec3f succeed_outside_8(-2.01f,2,2);

        ASSERT( Collision3D::TestPoint::Intersects( succeed_inside, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_face_top, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_face_bottom, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_face_left, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_face_right, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_face_near, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_face_far, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_top_1, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_top_2, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_top_3, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_top_4, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_bottom_1, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_bottom_2, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_bottom_3, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_bottom_4, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_sides_1, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_sides_2, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_sides_3, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_edge_sides_4, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_1, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_2, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_3, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_4, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_5, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_6, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_7, aabox) );
        ASSERT( Collision3D::TestPoint::Intersects( succeed_on_corner_8, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_1, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_2, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_3, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_4, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_5, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_6, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_7, aabox) );
        ASSERT( ! Collision3D::TestPoint::Intersects( succeed_outside_8, aabox) );
    }

    void Collision3D::TestPoint::UnitTest_DistToAABox3D( void ) {
        AABox3D box( -2,2, -2,2, -2,2 );
        Vec3f inside(1,1,0);
        Vec3f on_face(2,0,0);
        Vec3f on_edge(2,2,0);
        Vec3f on_corner(2,2,2);
        Vec3f away_side(3,0,0);
        Vec3f away_edge(3,3,0);
        Vec3f away_corner(3,3,3);

        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( inside, box ) ) );
        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( on_face, box ) ) );
        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( on_edge, box ) ) );
        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( on_corner, box ) ) );
        ASSERT( Maths::Approxf( 1.0f, DistToAABox3D( away_side, box ) ) );
        ASSERT( Maths::Approxf( sqrtf( 2.0f ), DistToAABox3D( away_edge, box ) ) );
        ASSERT( Maths::Approxf( sqrtf( 3.0f ), DistToAABox3D( away_corner, box ) ) );
    }

    void Collision3D::TestPoint::UnitTest_CoplanarPoly( void ) {
        const std::size_t vert_count = 4;
        const Vec3f square[vert_count]={
            Vec3f(-1,-1,0), // left bottom
            Vec3f(-1,1,0), // left top
            Vec3f(1,1,0), // right top
            Vec3f(1,-1,0) // right bottom
        };

        const Vec3f succeed_in_but_not_on_edge(0.2f,0.2f,0.0f);
        const Vec3f succeed_on(0,0,0);
        const Vec3f succeed_on_edge(1,0,0);
        const Vec3f succeed_on_corner(1,1,0);
        const Vec3f fail_toright(EP+1,0,0);
        const Vec3f fail_behind_face(0,0,-EP);
        const Vec3f fail_before_face(0,0,EP);
        const Vec3f fail_behind_rightedge(1,0,-EP);
        const Vec3f fail_before_right(1,0,EP);
        const Vec3f fail_barely_to_right_of_rightedge(EP+1,0,0);
        const Vec3f fail_barely_above_corner(1,EP+1,0);

        ASSERT( Collision3D::TestPoint::ToPoly( succeed_in_but_not_on_edge, square, vert_count ) );
        ASSERT( Collision3D::TestPoint::ToPoly( succeed_on, square, vert_count ) );
        ASSERT( Collision3D::TestPoint::ToPoly( succeed_on_edge, square, vert_count ) );
        ASSERT( Collision3D::TestPoint::ToPoly( succeed_on_corner, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_toright, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_behind_face, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_before_face, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_behind_rightedge, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_before_right, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_barely_to_right_of_rightedge, square, vert_count ) );
        ASSERT( !Collision3D::TestPoint::ToPoly( fail_barely_above_corner, square, vert_count ) );
    }

#endif //CLIB_UNIT_TEST

bool Collision3D::TestPoint::ToTri( const Vec3f& point, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC, const float epsilon ) {
    Vec3f tri[3]{triA, triB, triC};
    return Collision3D::TestPoint::ToPoly( point, tri, 3, Plane3f::GetNormal( tri ), epsilon );
}

bool Collision3D::TestPoint::ToTri( const Vec3f& point, const Vec3f& triA, const Vec3f& triB, const Vec3f& triC, const Vec3f& plane_normal, const float epsilon ) {
    Vec3f tri[3]{triA, triB, triC};
    return Collision3D::TestPoint::ToPoly( point, tri, 3, plane_normal, epsilon );
}

bool Collision3D::TestPoint::ToQuad( const Vec3f& point, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD, const float epsilon ) {
    const Vec3f quad[4]{quadA, quadB, quadC, quadD};
    return Collision3D::TestPoint::ToPoly( point, quad, 4, Plane3f::GetNormal( quadA, quadB, quadC ), epsilon );
}

bool Collision3D::TestPoint::ToQuad( const Vec3f& point, const Vec3f& quadA, const Vec3f& quadB, const Vec3f& quadC, const Vec3f& quadD, const Vec3f& plane_normal, const float epsilon ) {
    Vec3f quad[4]{quadA, quadB, quadC, quadD};
    return Collision3D::TestPoint::ToPoly( point, quad, 4, plane_normal, epsilon );
}

bool Collision3D::TestPoint::OnPlane( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C, const float epsilon ) {
    return Collision3D::TestPoint::OnPlane( point, A, Plane3f::GetNormal( A, B, C ), epsilon );
}

bool Collision3D::TestPoint::OnPlane( const Vec3f& point, const Vec3f &point_on_plane, const Vec3f& plane_normal, const float epsilon ) {
    return Maths::Approxf( Plane3f::GetDistToPoint( point, point_on_plane, plane_normal ), 0.0f, epsilon );
}

bool Collision3D::TestPoint::OnPlane( const Vec3f& point, const Plane3f& plane, const float epsilon ) {
    return Collision3D::TestPoint::OnPlane( point, plane.GetPoint(), plane.GetNormal(), epsilon );
}

bool Collision3D::TestPoint::BehindPlane( const Vec3f& point, const Plane3f& plane, const float epsilon ) {
    return Collision3D::TestPoint::BehindPlane( point, plane.GetPoint(), plane.GetNormal(), epsilon );
}

bool Collision3D::TestPoint::BehindPlane( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C, const float epsilon ) {
    return Collision3D::TestPoint::BehindPlane( point, A, Plane3f::GetNormal(A,B,C), epsilon );
}

bool Collision3D::TestPoint::BeforePlane( const Vec3f& point, const Vec3f &A, const Vec3f& B, const Vec3f& C, const float epsilon ) {
    return Collision3D::TestPoint::BeforePlane( point, A, Plane3f::GetNormal( A,B,C ), epsilon );
}

bool Collision3D::TestPoint::BeforePlane( const Vec3f& point, const Plane3f& plane, const float epsilon ) {
    return Collision3D::TestPoint::BeforePlane( point, plane.GetPoint(), plane.GetNormal(), epsilon );
}

bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& start_point, const Vec3f& end_point, const AABox3D& aabox ) {
    return Line3f::Intersects( report, Line3f(start_point, end_point, LineTypeT::SEG), aabox );
}

bool Collision3D::TestPoint::SweepTo( CollisionReport3D& report, const Vec3f& start_point, const Vec3f& end_point, const Box3D& box ) {
    return Line3f::Intersects( report, Line3f(start_point, end_point, LineTypeT::SEG), box );
}

float Collision3D::TestPoint::DistToAABox3D( const Vec3f& point, const AABox3D& aabox, Vec3f* norm_out ) {
    return sqrtf( SquaredDistToAABox3D( point, aabox, norm_out ) );
}

bool Collision3D::TestPoint::Intersects( const Vec3f& point, const AABox3D& aabox ) {
    return aabox.CheckBounds( point );
}

bool Collision3D::TestPoint::BehindPlane( const Vec3f& point, const Vec3f& point_on_plane, const Vec3f& plane_normal, const float epsilon ) {
    return Plane3f::GetDistToPoint( point, point_on_plane, plane_normal ) <= -epsilon;
}

bool Collision3D::TestPoint::BeforePlane( const Vec3f& point, const Vec3f &point_on_plane, const Vec3f& plane_normal, const float epsilon ) {
    return Plane3f::GetDistToPoint( point, point_on_plane, plane_normal ) >= epsilon;
}

bool Collision3D::TestPoint::Intersects( const Vec3f& point, const Line3f& line, const float epsilon ) {
    return ToLine( point, line.line_type, line.vert[0], line.vert[1], epsilon );
}

bool Collision3D::TestPoint::ToLineSeg( const Vec3f& point, const Vec3f& line_start, const Vec3f& line_end, const float epsilon ) {
    return ToLine( point, LineTypeT::SEG, line_start, line_end, epsilon );
}

bool Collision3D::TestPoint::AboveAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return point.z > aabox.max.z;
}

bool Collision3D::TestPoint::BelowAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return point.z < aabox.min.z;
}

bool Collision3D::TestPoint::LeftOfAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return point.x < aabox.min.x;
}

bool Collision3D::TestPoint::RightOfAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return point.x > aabox.max.x;
}

bool Collision3D::TestPoint::BehindAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return point.y > aabox.max.y;
}

bool Collision3D::TestPoint::BeforeAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return point.y < aabox.min.y;
}

bool Collision3D::TestPoint::ToAABox3D( const Vec3f& point, const AABox3D& aabox ) {
    return !( DistToAABox3D( point, aabox, nullptr ) > 0.0f );
}
