// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_SEMICYL_H_
#define SRC_COLLISION_3D_SEMICYL_H_

#include "../../../base/main.h"
#include "../../geometry.h"

namespace Collision3D {
    namespace TestSemiCylinder {
        void UnitTest( void );

        bool Intersects( const SemiCylinder& semiCyl, const Vec3f& box );
        bool Intersects( const SemiCylinder& semiCyl, const Box3D& box );
        bool Intersects( const SemiCylinder& semiCyl, const Sphere& sphere );
        bool Intersects( const SemiCylinder& semiCyl, const AABox3D& aabox );
        bool Intersects( const SemiCylinder& semiCyl, const SemiCylinder& aabox );
        bool Intersects( const SemiCylinder& semiCyl, const Cylinder& aabox );
        bool Intersects( const SemiCylinder& semiCyl, const Capsule& aabox );
        bool Intersects( const SemiCylinder& semiCyl, const Line3f& aabox );
    
        void UnitTest_ToAABox3D( void );
        void UnitTest_ToSphere( void );
    }
}

#endif // SRC_COLLISION_3D_SEMICYL_H_
