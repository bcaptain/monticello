// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include <cfloat>
#include "./semiCylinder.h"

/*!
the two faces of the "wedge" of our semi-cylindar are treated as two planes.
the normals of the planes point outward from the semicyl face.
in a <=180 semicyl, any shape existing in front of either plane does not collide with it
in a >180 semicyl, if any shape exists in front of both planes, it does not collide (but may if it only collides with one)
**/

Plane3f GetSemiCylinderSliceSideA( const SemiCylinder& semiCyl );
Plane3f GetSemiCylinderSliceSideA( const SemiCylinder& semiCyl ) {
    const Quat rotA( Quat::FromAxis( (-semiCyl.degrees/2) * DEG_TO_RAD_F, semiCyl.normal ) );
    const Plane3f sideA( semiCyl.origin, semiCyl.normal.Cross(semiCyl.dir) * rotA );
    return sideA;
}

Plane3f GetSemiCylinderSliceSideB( const SemiCylinder& semiCyl );
Plane3f GetSemiCylinderSliceSideB( const SemiCylinder& semiCyl ) {
    const Quat rotB( Quat::FromAxis( (semiCyl.degrees/2) * DEG_TO_RAD_F, semiCyl.normal ) );
    const Plane3f sideB( semiCyl.origin, -semiCyl.normal.Cross(semiCyl.dir) * rotB );
    return sideB;
}
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Box3D& box ) {
        ERR("Not Implemented: SemiCylinder to Box3D.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Line3f& line ) {
        ERR("Not Implemented: SemiCylinder to Line3f.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const SemiCylinder& other ) {
        ERR("Not Implemented: SemiCylinder to SemiCylinder.\n"); //toimplement
        return false;
    }

    bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Cylinder& cyl ) {
        ERR("Not Implemented: SemiCylinder to Cylinder.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Capsule& capsule ) {
    ERR("Not Implemented: Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Capsule& capsule )\n"); //toimplement
    return false;
}

bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Sphere& sphere ) {
    if ( ! Sphere::Intersects( sphere, *static_cast< const Cylinder* >( &semiCyl ) ) )
        return false;

    const Plane3f sideA( GetSemiCylinderSliceSideA( semiCyl ) );
    const float distA = sideA.GetDistToPoint( sphere.origin );

    const Plane3f sideB( GetSemiCylinderSliceSideB( semiCyl ) );
    const float distB = sideB.GetDistToPoint( sphere.origin );

    if ( semiCyl.degrees > 180 ) {

        if ( distA < -sphere.radius )
            if ( distB < -sphere.radius )
                return false;

        return true;
    }

    // ** semiCyl.degrees <= 180

    if ( distA < -sphere.radius )
        return false;

    if ( distB < -sphere.radius )
        return false;

    return true;
}

bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const AABox3D& aabox ) {
    if ( !Cylinder::Intersects( semiCyl, aabox ) )
        return false;

    const Box3D box( aabox );

    const Plane3f sideA( GetSemiCylinderSliceSideA( semiCyl ) );
    const float distA = sideA.GetDistToBox3D( box );

    if ( semiCyl.degrees <= 180 ) {
        if ( distA < 0 )
            return false;
    }

    const Plane3f sideB( GetSemiCylinderSliceSideB( semiCyl ) );
    const float distB = sideB.GetDistToBox3D( box );

    if ( semiCyl.degrees <= 180 ) {
        if ( distB < 0 )
            return false;
        return true;
    }

    // semiCyl.degrees > 180

    if ( distA >= 0 )
        return true;

    if ( distB >= 0 )
        return true;

    return false;
}

bool Collision3D::TestSemiCylinder::Intersects( const SemiCylinder& semiCyl, const Vec3f& point ) { //todo:unittest
    if ( !Cylinder::Intersects( semiCyl, point ) )
        return false;

    const Plane3f sideA( GetSemiCylinderSliceSideA( semiCyl ) );
    const float distA = sideA.GetDistToPoint( point );

    if ( semiCyl.degrees <= 180 ) {
        if ( distA < 0 )
            return false;
    }

    const Plane3f sideB( GetSemiCylinderSliceSideB( semiCyl ) );
    const float distB = sideB.GetDistToPoint( point );

    if ( semiCyl.degrees <= 180 ) {
        if ( distB < 0 )
            return false;
        return true;
    }

    // semiCyl.degrees > 180

    if ( distA >= 0 )
        return true;

    if ( distB >= 0 )
        return true;

    return false;
}

#ifdef CLIB_UNIT_TEST
    void Collision3D::TestSemiCylinder::UnitTest( void ) {
        Collision3D::TestSemiCylinder::UnitTest_ToSphere();
        Collision3D::TestSemiCylinder::UnitTest_ToAABox3D();
    }

    void Collision3D::TestSemiCylinder::UnitTest_ToAABox3D( void ) {
        const Vec3f one(1,1,1);

        const AABox3D front( Vec3f(0,4,0)-one, Vec3f(0,4,0)+one );
        const AABox3D left( Vec3f(-4,0,0)-one, Vec3f(-4,0,0)+one );
        const AABox3D right( Vec3f(4,0,0)-one, Vec3f(4,0,0)+one );
        const AABox3D onFaceA( Vec3f(-1,-2,0)-one, Vec3f(-1,-2,0)+one );
        const AABox3D onFaceB( Vec3f(2,-2,0)-one, Vec3f(2,-2,0)+one );
        const AABox3D in90Spot( Vec3f(0,-3,0)-one, Vec3f(0,-3,0)+one );
        const AABox3D above( Vec3f(0,0,6.01f)-one, Vec3f(0,0,6.01f)+one );
        const AABox3D below( Vec3f(0,0,-6.01f)-one, Vec3f(0,0,-6.01f)+one );

        // **** > 180 degrees
        SemiCylinder cyl;
        cyl.degrees = 270;
        cyl.dir.Set(0,1,0);
        cyl.length = 10;
        cyl.normal.Set(0,0,1);
        cyl.origin.Set(0,0,-5);
        cyl.radius = 4;

        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, front ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, left ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, right ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, onFaceA ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, onFaceB ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, in90Spot ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, above ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, below ) );

        // **** < 180 degrees
        cyl.degrees = 90;
        cyl.dir.Set(0,-1,0);
        cyl.length = 10;
        cyl.normal.Set(0,0,1);
        cyl.origin.Set(0,0,-5);
        cyl.radius = 4;

        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, front ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, left ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, right ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, onFaceA ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, onFaceB ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, in90Spot ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, above ) );
        ASSERT( ! Collision3D::TestSemiCylinder::Intersects( cyl, below ) );
    }

    void Collision3D::TestSemiCylinder::UnitTest_ToSphere( void ) {
        // *********** a 270 degree cylinder

        SemiCylinder cyl;
        cyl.normal = Vec3f(0,0,1);
        cyl.radius = 4;
        cyl.length = 1;
        cyl.degrees = 270;
        cyl.origin = Vec3f(0,0,0);
        cyl.dir = Vec3f(0,-1,0);

        const Sphere sphere_north( Vec3f(0,4,0), 1 );
        const Sphere sphere_south( Vec3f(0,-4,0), 1 );
        const Sphere sphere_south_west( Vec3f(-4,-2,0), 1 );
        const Sphere sphere_south_east( Vec3f(4,-2,0), 1 );
        const Sphere sphere_north_east( Vec3f(4,2,0), 1 );
        const Sphere sphere_north_west( Vec3f(-4,2,0), 1 );

        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere_south_east ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere_south_west ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere_north_east ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere_north_west ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere_south ) );
        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere_north ) );

        // *********** a 90 degree cylinder

        cyl.normal = Vec3f(0,0,1);
        cyl.radius = 4;
        cyl.length = 1;
        cyl.degrees = 90;
        cyl.origin = Vec3f(0,0,0);
        cyl.dir = Vec3f(0,1,0);

        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere_south_east ) );
        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere_south_west ) );
        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere_north_east ) );
        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere_north_west ) );
        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere_south ) );
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere_north ) );

        // ***************** a 180 degree cylinder

        const Sphere sphere( Vec3f(0,0,0), 1 );

        cyl.normal = Vec3f(0,0,1);
        cyl.radius = 1;
        cyl.length = 1;
        cyl.degrees = 180;
        cyl.origin = Vec3f(0,-1,0);
        cyl.dir = Vec3f(0,-1,0);

        // touches behind
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

        // intersects from in front
        cyl.origin.Set(0,-1,0);
        cyl.dir.Set(0,1,0);
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

        // intersects from behind
        cyl.origin.Set(0,1,0);
        cyl.dir.Set(0,-1,0);
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

        // touches in front
        cyl.origin.Set(0,1,0);
        cyl.dir.Set(0,1,0);
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

        // totally misses
        cyl.origin.Set(0,-2.1f,0);
        cyl.dir.Set(0,1,0);
        ASSERT( !Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

        // touches inside a bit, fron in front
        cyl.origin.Set(0,0.90f,0);
        cyl.dir.Set(0,1,0);
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

        // touches inside a bit, fron behind
        cyl.origin.Set(0,-0.90f,0);
        cyl.dir.Set(0,-1,0);
        ASSERT( Collision3D::TestSemiCylinder::Intersects( cyl, sphere ) );

    }
#endif //CLIB_UNIT_TEST
