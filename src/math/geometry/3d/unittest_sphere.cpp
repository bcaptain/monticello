// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./sphere.h"
#include "./box.h"
#include "./capsule.h"
#include "./plane.h"
#include "../../collision/collision.h"

#ifdef CLIB_UNIT_TEST
    void Sphere::UnitTest( void ) {
        UnitTest_ToPlane();
        UnitTest_ToPoly();
        UnitTest_ToAABox3D();
        UnitTest_ToBox();
        UnitTest_ToSphere();
        UnitTest_ToCylinder();
        UnitTest_ToCapsule();

        UnitTest_SweepToSphere();
        UnitTest_SweepToPoint();
        UnitTest_SweepToLineSeg();
        UnitTest_SweepToRay();
        UnitTest_SweepToLine();
        UnitTest_SweepToPlane();
        UnitTest_SweepToPoly();
        UnitTest_SweepToBox3D();
    }

    void Sphere::UnitTest_SweepToBox3D( void ) {
        auto dotest = [] ( const bool collides, const Sphere& sphere, const Vec3f& sphere_dest, const Box3D& box ) {
            CollisionReport3D report;
            
            ASSERT( collides == Sphere::SweepTo( report, sphere, sphere_dest, box ) );
            
            Quat rot( Quat::FromAngles_ZYX(33.33f,0,0) );
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(0,0,33.33f);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(0,33.33f,0);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(33.33f,0,55.55f);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(0,44.44f,55.55f);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(323.33f,44.44f,55.55f);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(33.33f,443.44f,55.55f);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );

            rot = Quat::FromAngles_ZYX(33.33f,44.44f,555.55f);
            ASSERT( collides == Sphere::SweepTo( report, Sphere(sphere.origin * rot, sphere.radius), sphere_dest * rot, box * rot ) );
        };
        
        const Box3D box( Vec3f(-40,-20,-20), Vec3f(40,20,20) );
        const float radius = 1.5f;
        
        dotest( true, Sphere( Vec3f(0,0,-30), radius ), Vec3f(0,0,-19.95f-radius), box );
        dotest( true, Sphere( Vec3f(0,0,-30), radius ), Vec3f(0,0,-19.98f-radius), box );
        dotest( true, Sphere( Vec3f(0,0,-30), radius ), Vec3f(0,0,-19.9999f-radius), box );
        dotest( false, Sphere( Vec3f(0,0,-30), radius ), Vec3f(0,0,-20.1f-radius), box );
        dotest( false, Sphere( Vec3f(0,0,-30), radius ), Vec3f(0,0,-20.01f-radius), box );
        dotest( false, Sphere( Vec3f(0,0,-30), radius ), Vec3f(0,0,-20.001f-radius), box );
        
        dotest( true, Sphere( Vec3f(0,0,30), radius ), Vec3f(0,0,19.95f+radius), box );
        dotest( true, Sphere( Vec3f(0,0,30), radius ), Vec3f(0,0,19.98f+radius), box );
        dotest( true, Sphere( Vec3f(0,0,30), radius ), Vec3f(0,0,19.9999f+radius), box );
        dotest( false, Sphere( Vec3f(0,0,30), radius ), Vec3f(0,0,20.1f+radius), box );
        dotest( false, Sphere( Vec3f(0,0,30), radius ), Vec3f(0,0,20.01f+radius), box );
        dotest( false, Sphere( Vec3f(0,0,30), radius ), Vec3f(0,0,20.001f+radius), box );
    }
    
    void Sphere::UnitTest_ToSphere( void ) {
        // coiincide
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 1 ), Sphere( Vec3f(0,0,0), 1 ) ) );
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 1 ), Sphere( Vec3f(0,0,0), 1 ) ) );

        // encapselation
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 1 ), Sphere( Vec3f(0,0,0), 0.5f ) ) );
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 0.5f ), Sphere( Vec3f(0,0,0), 1 ) ) );

        // intersection
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 1 ), Sphere( Vec3f(0.5f,0,0), 1 ) ) );
        ASSERT( Intersects( Sphere( Vec3f(0.5f,0,0), 1 ), Sphere( Vec3f(0,0,0), 1 ) ) );

        // intersection
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 1 ), Sphere( Vec3f(1,0,0), 1 ) ) );
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 1 ), Sphere( Vec3f(1,0,0), 1 ) ) );

        // touch
        ASSERT( Intersects( Sphere( Vec3f(-1,0,0), 1 ), Sphere( Vec3f(1,0,0), 1 ) ) );
        ASSERT( Intersects( Sphere( Vec3f(-1,0,0), 1 ), Sphere( Vec3f(1,0,0), 1 ) ) );

        // no touch
        ASSERT( ! Intersects( Sphere( Vec3f(-1.01f,0,0), 1 ), Sphere( Vec3f(1,0,0), 1 ) ) );
        ASSERT( ! Intersects( Sphere( Vec3f(1,0,0), 1 ), Sphere( Vec3f(-1.01f,0,0), 1 ) ) );
    }

    void Sphere::UnitTest_SweepToSphere( void ) {
        Sphere a( Vec3f(-4,0,0), 2);
        Vec3f a_dest( 0,0,0 );

        Sphere b( Vec3f(0,0,0), 1);


        CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
        ASSERT( SweepTo( report, a, a_dest, b ) );
        ASSERT( Maths::Approxf( report.norm, Vec3f( -1,0,0 ) ) );
        ASSERT( Maths::Approxf( report.point, Vec3f( -1,0,0 ) ) );
        ASSERT( Maths::Approxf( report.time, 0.25f ) );

        a.origin.Set(-4,0,0);
        a_dest.Set( 4,0,0 );
        a.radius = 1;
        b.origin.Set( 0, 2, 0 );
        b.radius = 1;
        ASSERT( SweepTo( report, a, a_dest, b ) );
        ASSERT( Maths::Approxf( report.norm, Vec3f( 0,-1,0 ) ) );
        ASSERT( Maths::Approxf( report.point, Vec3f( 0,1,0 ) ) );
        ASSERT( Maths::Approxf( report.time, 0.5f ) );


        a.origin.Set(-4,0,0);
        a_dest.Set( 4,0,0 );
        a.radius = 1;
        b.origin.Set( 0, 2.01f, 0 );
        b.radius = 1;
        ASSERT( !SweepTo( report, a, a_dest, b ) );
    }

    void Sphere::UnitTest_ToCylinder( void ) {
        auto dotest = [] ( const Quat& rotate, const Vec3f& translate ) {
            const Vec3f cyl_origin(0,0,0);
            const Vec3f cyl_normal(1,0,0);
            const float cyl_radius=1;
            const float cyl_length=4;
            const Cylinder cyl_around_x_axis(
                cyl_origin * rotate + translate,
                cyl_normal * rotate,
                cyl_radius,
                cyl_length
            );

            const Sphere insphere( Vec3f(2,0,0) * rotate + translate, 0.9f );
            const Sphere circumsphere( Vec3f(2,0,0) * rotate + translate, 10 );
            const Sphere lateralSurfaceIntersect( Vec3f(2,1,0) * rotate + translate, 1 );
            const Sphere lateralSurfaceTouch( Vec3f(2,2,0) * rotate + translate, 1 );
            const Sphere lateralSurface_NO_Touch( Vec3f(2,2.01f,0) * rotate + translate, 1 );
            const Sphere endcapIntersect( Vec3f(4.9f,0,0) * rotate + translate, 1 );
            const Sphere endcapTouch( Vec3f(5,0,0) * rotate + translate, 1 );
            const Sphere endcap_NO_Touch( Vec3f(5.01f,0,0) * rotate + translate, 1 );
            const Sphere startcapIntersect( Vec3f(-0.9f,0,0) * rotate + translate, 1 );
            const Sphere startcapTouch( Vec3f(-1,0,0) * rotate + translate, 1 );
            const Sphere startcap_NO_Touch( Vec3f(-1.01f,0,0) * rotate + translate, 1 );

            ASSERT(   Intersects( insphere,                cyl_around_x_axis ) );
            ASSERT(   Intersects( circumsphere,            cyl_around_x_axis ) );
            ASSERT(   Intersects( lateralSurfaceIntersect, cyl_around_x_axis ) );
            ASSERT(   Intersects( lateralSurfaceTouch,     cyl_around_x_axis ) );
            ASSERT( ! Intersects( lateralSurface_NO_Touch, cyl_around_x_axis ) );
            ASSERT(   Intersects( endcapIntersect,         cyl_around_x_axis ) );
            ASSERT(   Intersects( endcapTouch,             cyl_around_x_axis ) );
            ASSERT( ! Intersects( endcap_NO_Touch,         cyl_around_x_axis ) );
            ASSERT(   Intersects( startcapIntersect,       cyl_around_x_axis ) );
            ASSERT(   Intersects( startcapTouch,           cyl_around_x_axis ) );
            ASSERT( ! Intersects( startcap_NO_Touch,       cyl_around_x_axis ) );
        };

        // a cylinder that extends 2 units along the x axis in both sides of the y/z
        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(0,0,0) );
        dotest( Quat::FromAngles_ZYX(34,0,0), Vec3f(0,0,0) );
        dotest( Quat::FromAngles_ZYX(0,34,0), Vec3f(0,0,0) );
        dotest( Quat::FromAngles_ZYX(0,0,34), Vec3f(0,0,0) );
        dotest( Quat::FromAngles_ZYX(0,16,34), Vec3f(0,0,0) );
        dotest( Quat::FromAngles_ZYX(80,16,34), Vec3f(0,0,0) );

        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(65, -1, 7) );
        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(83, -76, 71) );
        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(60, -146, 71) );
        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(267, -106, 37) );
        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(94, -162, 737) );
        dotest( Quat::FromAngles_ZYX(0,0,0), Vec3f(1900, -123, 7471) );

        dotest( Quat::FromAngles_ZYX(0,0,0),    Vec3f(65, -1, 7) );
        dotest( Quat::FromAngles_ZYX(34,0,0),   Vec3f(83, -76, 71) );
        dotest( Quat::FromAngles_ZYX(0,34,0),   Vec3f(60, -146, 71) );
        dotest( Quat::FromAngles_ZYX(0,0,34),   Vec3f(267, -106, 37) );
        dotest( Quat::FromAngles_ZYX(0,16,34),  Vec3f(94, -162, 737) );
        dotest( Quat::FromAngles_ZYX(80,16,34), Vec3f(1900, -123, 7471) );

    }

    void Sphere::UnitTest_ToCapsule( void ) {
        // a capinder that extends 2 units along the x axis in both sides of the y/z
        const Vec3f cap_origin(0,0,0);
        const Vec3f cap_normal(1,0,0);
        const float cap_radius=1;
        const float cap_length=4;
        const Capsule cap_around_x_axis(
            cap_origin,
            cap_normal,
            cap_radius,
            cap_length
        );

        const Sphere insphere( Vec3f(2,0,0), 0.9f );
        const Sphere circumsphere( Vec3f(2,0,0), 10 );
        const Sphere lateralSurfaceIntersect( Vec3f(2,1,0), 1 );
        const Sphere lateralSurfaceTouch( Vec3f(2,2,0), 1 );
        const Sphere lateralSurface_NO_Touch( Vec3f(2,2.01f,0), 1 );
        const Sphere endcapIntersect( Vec3f(5.5f,0,0), 1 );
        const Sphere endcapTouch( Vec3f(6,0,0), 1 );
        const Sphere endcap_NO_Touch( Vec3f(6.01f,0,0), 1 );
        const Sphere startcapIntersect( Vec3f(-1.9f,0,0), 1 );
        const Sphere startcapTouch( Vec3f(-2,0,0), 1 );
        const Sphere startcap_NO_Touch( Vec3f(-2.01f,0,0), 1 );

        ASSERT(   Intersects( insphere,                cap_around_x_axis ) );
        ASSERT(   Intersects( circumsphere,            cap_around_x_axis ) );
        ASSERT(   Intersects( lateralSurfaceIntersect, cap_around_x_axis ) );
        ASSERT(   Intersects( lateralSurfaceTouch,     cap_around_x_axis ) );
        ASSERT( ! Intersects( lateralSurface_NO_Touch, cap_around_x_axis ) );
        ASSERT(   Intersects( endcapIntersect,         cap_around_x_axis ) );
        ASSERT(   Intersects( endcapTouch,             cap_around_x_axis ) );
        ASSERT( ! Intersects( endcap_NO_Touch,         cap_around_x_axis ) );
        ASSERT(   Intersects( startcapIntersect,         cap_around_x_axis ) );
        ASSERT(   Intersects( startcapTouch,             cap_around_x_axis ) );
        ASSERT( ! Intersects( startcap_NO_Touch,         cap_around_x_axis ) );

    }

    void Sphere::UnitTest_SweepToLineSeg( void ) {
        { // to line dead-on to the side
            CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
            Sphere sphere( Vec3f(-2,0,0), 1 );
            Line3f line( Vec3f(0,10,0), Vec3f(0,-10,0) );
            Vec3f dest(2,0,0);
            ASSERT( SweepTo( report, sphere, dest, line ) );
            ASSERT( Maths::Approxf( report.norm, Vec3f(-1,0,0) ) );
            ASSERT( Maths::Approxf( report.time, 0.25f ) );
            ASSERT( Maths::Approxf( report.point, Vec3f(0,0,0) ) );
        }

        { // to line, hits line off to side a bit
            CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
            Sphere sphere( Vec3f(-2,0,0.5f), 1 );
            Line3f line( Vec3f(0,10,0), Vec3f(0,-10,0) );
            Vec3f dest(2,0,0.5f);
            ASSERT( SweepTo( report, sphere, dest, line ) );
            ASSERT( Maths::Approxf( report.norm, Vec3f(-0.866025448f,0,0.5f) /*looks good to me*/ ) );
            ASSERT( Maths::Approxf( report.time, 0.283493638f /*just a touch beyond 0.25*/ ) );
            ASSERT( Maths::Approxf( report.point, Vec3f(0,0,0) ) );
        }

        CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
        Vec3f checkline_a;
        Vec3f checkline_b;
        Vec3f sphere_dest;

        // sphere nips tip of line 50% to sphere's destination
        checkline_a.Set(0,2,0);
        checkline_b.Set(0,1,0);
        Sphere sphere( Vec3f(-4,0,0), 1 );
        sphere_dest.Set(4,0,0);
        ASSERT( SweepToLineSeg( report, sphere, sphere_dest, checkline_a, checkline_b ) );
        ASSERT( Maths::Approxf( report.time, 0.5f ) );
        ASSERT( report.point == Vec3f(0,1,0) );
        ASSERT( report.norm == Vec3f(0,-1,0) );

        // same as above, but reversed line
        checkline_b.Set(0,2,0);
        checkline_a.Set(0,1,0);
        sphere_dest.Set(4,0,0);
        ASSERT( SweepToLineSeg( report, sphere, sphere_dest, checkline_a, checkline_b ) );
        ASSERT( Maths::Approxf( report.time, 0.5f ) );
        ASSERT( report.point == Vec3f(0,1,0) );
        ASSERT( report.norm == Vec3f(0,-1,0) );

        checkline_a.Set(0,2,0.5f);
        checkline_b.Set(0,-2,0.5f);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLineSeg( report, sphere, sphere_dest, checkline_a, checkline_b ) );
        ASSERT( Maths::Approxf( report.point, Vec3f(0,0,0.5f) ) );
            float partial_unit = (86.0f/680.0f); // measured by pixels in gimp with a blender screenshot
            float j = 3.0f + partial_unit;
            j /= 8.0f;
        ASSERT( Maths::Approxf( report.time, j, 0.01f ) );

        // ** no collision

        // line above sphere pointing at it at 50% time, not touching
        checkline_a.Set(0,2,0);
        checkline_b.Set(0,1.01f,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( !SweepToLineSeg( report, sphere, sphere_dest, checkline_a, checkline_b ) );

    }

    void Sphere::UnitTest_SweepToRay( void ) {
    }

    void Sphere::UnitTest_SweepToLine( void ) {
        CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
        Vec3f checkline_a;
        Vec3f checkline_b;
        Vec3f sphere_orig;
        Vec3f sphere_dest;

        // pass straight through the middle of hte sphere's path
        checkline_a.Set(0,1,0);
        checkline_b.Set(0,-1,0);
        Sphere sphere( Vec3f(-4,0,0), 1 );
        sphere_dest.Set(4,0,0);
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( report.point == Vec3f(0,0,0) );
        ASSERT( Maths::Approxf( report.time, 3.0f/8.0f ) );

        // pass straight through the middle of hte sphere's path
        checkline_a.Set(0,5,0);
        checkline_b.Set(0,10,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0 ) );
        ASSERT( report.point == Vec3f(0,0,0) );
        ASSERT( Maths::Approxf( report.time, 3.0f/8.0f ) );

        // pass straight through the middle of hte sphere's path
        checkline_a.Set(0,10,0);
        checkline_b.Set(0,5,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0 ) );
        ASSERT( report.point == Vec3f(0,0,0) );
        ASSERT( Maths::Approxf( report.time, 3.0f/8.0f ) );

        // pass straight through the middle of hte sphere's path
        checkline_a.Set(0,0,5);
        checkline_b.Set(0,0,10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == (Vec3f(-1,0,0 ) ) );
        ASSERT( report.point == Vec3f(0,0,0) );
        ASSERT( Maths::Approxf( report.time, 3.0f/8.0f ) );

        // pass straight through the middle of hte sphere's path
        checkline_a.Set(0,0,10);
        checkline_b.Set(0,0,5);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0 ) );
        ASSERT( report.point == Vec3f(0,0,0) );
        ASSERT( Maths::Approxf( report.time, 3.0f/8.0f ) );

        // graze through the sphere's center on the right side at it's start point, which is 0% time on the velocity ( time is 0 to 1 from surface to surface, not origin to origin)
        checkline_a.Set(-3.0,0,-4);
        checkline_b.Set(-3.0,0,4);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( report.point == Vec3f(-3,0,0) );
        ASSERT( Maths::Approxf( report.time, 0.0f ) );

        // barely graze the sphere at the end pos
        checkline_a.Set(5,0,-4);
        checkline_b.Set(5,0,4);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0 ) );
        ASSERT( report.point == Vec3f(5,0,0) );
        ASSERT( Maths::Approxf( report.time, 1.0f ) );

        // barely graze the sphere at the end pos
        checkline_a.Set(5,-4,0);
        checkline_b.Set(5,4,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0 ) );
        ASSERT( report.point == Vec3f(5,0,0) );
        ASSERT( Maths::Approxf( report.time, 1.0f ) );

        // diagonal
        checkline_a.Set(2,-10,10);
        checkline_b.Set(2,10,-10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
        ASSERT( report.norm == Vec3f(-1,0,0 ) );
        ASSERT( report.point == Vec3f(2,0,0) );
        ASSERT( Maths::Approxf( report.time, 5.0f/8.0f ) );

        // ** no collide

        // barely graze the sphere at the start pos, but is "behind the trajectory" so-to-speak, which is 2 units behind 0% time on the velocity (since time is from surface to surface, not origin to origin)
        checkline_a.Set(-5,0,-4);
        checkline_b.Set(-5,0,4);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( !SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );

        checkline_a.Set(5.1f,0,-4);
        checkline_b.Set(5.1f,0,4);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( !SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );

        checkline_a.Set(5.1f,-4,0);
        checkline_b.Set(5.1f,4,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( !SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );

        checkline_a.Set(5.01f,-4,0);
        checkline_b.Set(5.01f,4,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT(! SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );

        // barely not graze the sphere at the end pos
        checkline_a.Set(5.01f,-4,0);
        checkline_b.Set(5.01f,4,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT(! SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );

        // diagonal
        // line horizontal, above sphere, nowhere near colliding
        checkline_a.Set(-8,8,-10);
        checkline_b.Set(8,8,-10);
        sphere.origin.Set(0,6,8);
        sphere_dest.Set(0,10,8);
        ASSERT( !SweepToLine( report, sphere, sphere_dest, LineTypeT::LINE, checkline_a, checkline_b ) );
    }

    void Sphere::UnitTest_SweepToPoint( void ) {
        CollisionReport3D report( CollisionCalcsT::Normal | CollisionCalcsT::Intersection );
        Vec3f check_point;
        Vec3f sphere_orig;
        Vec3f sphere_dest;
        Sphere sphere( Vec3f(-4,0,0), 1 );
        Vec3f tmp;

        check_point.Set(0,0,0);
        sphere_dest.Set(4,0,0);
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( Maths::Approxf( report.time, 3.0f/8.0f ) ); // collision is 3/8 of the way to the destination point
        ASSERT( report.point == check_point );

        check_point.Set(1,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( Maths::Approxf( report.time, 0.5f ) );
        ASSERT( report.point == check_point );

        check_point.Set(-2,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( Maths::Approxf( report.time, 1.0f/8.0f ) ); // one eighth of the way there
        ASSERT( report.point == check_point );

        check_point.Set(-1,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( Maths::Approxf( report.time, 2.0f/8.0f ) );
        ASSERT( report.point == check_point );

        check_point.Set(2,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );
        ASSERT( Maths::Approxf( report.time, 5.0f/8.0f ) );
        ASSERT( report.point == check_point );

        // a diagonal
        check_point.Set(0,0,0);
        sphere.origin.Set(-4,-4,0);
        sphere_dest.Set(4,4,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );

        tmp.Set(-1,-1,0);
        tmp.Normalize();
        ASSERT( report.norm == tmp );

        float x = ( sphere_dest.Len() - 1 ) / ( sphere_dest.Len() * 2 );
        ASSERT( Maths::Approxf( report.time, x ) );

        ASSERT( report.point == check_point );

        check_point.Set(5.00f,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( Maths::Approxf( report.time, 1.0f ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );

        // touching the front of the sphere at start point
        check_point.Set(-3.0f,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );
        ASSERT( Maths::Approxf( report.time, 0.0f ) );
        ASSERT( report.norm == Vec3f(-1,0,0) );

        // would be just behind the front of the sphere at start point, but up a little so it is in front
        check_point.Set(-3.01f,0.25f,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( SweepTo( report, sphere, sphere_dest, check_point ) );

        // ** No Collision

        // just behind the front of the sphere at start point
        check_point.Set(-3.01f,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( !SweepTo( report, sphere, sphere_dest, check_point ) );

        // just behind the front of the sphere at start point, and also up a little (still behind front of the sphere, though)
        check_point.Set(-3.15f,0.25f,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(5.01f,0,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(4.79f,0.79f,0);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(10,10,10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(-10,10,10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(10,-10,10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(10,10,-10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(-10,10,-10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        check_point.Set(-10,-10,-10);
        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius=1.0f;
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );

        sphere.origin.Set(-4,0,0);
        sphere_dest.Set(4,0,0);
        sphere.radius = 1.0f;
        check_point.Set(0,1.1f,0);
        ASSERT( ! SweepTo( report, sphere, sphere_dest, check_point ) );
    }

    void Sphere::UnitTest_SweepToPoly( void ) {
        const std::vector<Vec3f> poly{
            Vec3f(0,0,0)
            , Vec3f(0,10,0)
            , Vec3f(10,10,0)
            , Vec3f(10,0,0)
        };

        CollisionReport3D report( CollisionCalcsT::Intersection | CollisionCalcsT::Normal );

        { // head-on with poly's center
            const Sphere sphere( Vec3f(5,5,2), 1 );
            const Vec3f dest( 5,5,-2 );
            ASSERT( SweepToPoly( report, sphere, dest, poly.data(), poly.size() ) );
            ASSERT( report.norm == Vec3f(0,0,1) );
            ASSERT( Maths::Approxf( report.point, Vec3f(5,5,0), 0.001f ) );
            ASSERT( Maths::Approxf( report.time, 0.25f, 0.001f ) );
        }

        { // head-on with poly's edge
            const Sphere sphere( Vec3f(0,0,2), 1 );
            const Vec3f dest( 0,0,-2 );
            ASSERT( SweepToPoly( report, sphere, dest, poly.data(), poly.size() ) );
            ASSERT( report.norm == Vec3f(0,0,1) );
            ASSERT( Maths::Approxf( report.point, Vec3f(0,0,0), 0.001f ) );
            ASSERT( Maths::Approxf( report.time, 0.25f, 0.001f ) );
        }
    }

    void Sphere::UnitTest_SweepToPlane( void ) {
        const Plane3f plane( Vec3f(0,1,0), Vec3f(0,0,1) );

        ASSERT( plane.GetNormal() == Vec3f(0,0,1) );

        // "on" means the sphere's surface touched the plane.

        const Vec3f behind1(-16,-16,-1.1f);
        const Vec3f behind2(16,16,-1.1f);
        const Vec3f before1(-16,-16,1.1f);
        const Vec3f before2(16,16,1.1f);
        const Vec3f touching_back1(10,10,1.0f);
        const Vec3f touching_back2(-10,-10,1.0f);
        const Vec3f touching_front1(10,10,0);
        const Vec3f touching_front2(-10,-10,0);
        const Vec3f inside1(25,25,0.0f);
        const Vec3f inside2(-25,-25,0.0f);

        CollisionReport3D report( CollisionCalcsT::Intersection | CollisionCalcsT::Normal );

        ASSERT( SweepToPlane( report, Sphere( before1, 1 ), touching_front1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( report.time, 0.1f / 1.1f, 0.001f ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before2, 1 ), touching_front1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( report.time, 0.1f / 1.1f, 0.001f ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before1, 1 ), touching_front2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( report.time, 0.1f / 1.1f, 0.001f ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before2, 1 ), touching_front2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( report.time, 0.1f / 1.1f, 0.001f ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before1, 1 ), behind1, plane.GetPoint(), plane.GetNormal() ) );
            Vec3f t=before1;
            t -= behind1;
            float g = t.Len();
            float x = 0.1f / g;
        ASSERT( Maths::Approxf( report.time, x, 0.01f ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before1, 1 ), behind2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before2, 1 ), behind1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        ASSERT( SweepToPlane( report, Sphere( before2, 1 ), behind2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( report.norm == Vec3f(0,0,1) );

        // ** already inside
        ASSERT( ! SweepToPlane( report, Sphere( inside1, 1 ), inside1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( inside1, 1 ), inside2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( inside2, 1 ), inside1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( inside2, 1 ), inside2, plane.GetPoint(), plane.GetNormal() ) );

        // ** already touching
        ASSERT( ! SweepToPlane( report, Sphere( touching_front1, 1 ), touching_front1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( touching_front1, 1 ), touching_front2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( touching_front2, 1 ), touching_front1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( touching_front2, 1 ), touching_front2, plane.GetPoint(), plane.GetNormal() ) );

        // ** never collides
        ASSERT( ! SweepToPlane( report, Sphere( before1, 1 ), before1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( before1, 1 ), before2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( before2, 1 ), before1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( before2, 1 ), before2, plane.GetPoint(), plane.GetNormal() ) );

        // ** already behind
        ASSERT( ! SweepToPlane( report, Sphere( behind1, 1 ), touching_back1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind2, 1 ), touching_back1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind1, 1 ), touching_back2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind2, 1 ), touching_back2, plane.GetPoint(), plane.GetNormal() ) );

        ASSERT( ! SweepToPlane( report, Sphere( behind1, 1 ), behind1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind2, 1 ), behind1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind1, 1 ), behind2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind2, 1 ), behind2, plane.GetPoint(), plane.GetNormal() ) );

        ASSERT( ! SweepToPlane( report, Sphere( behind1, 1 ), before1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind1, 1 ), before2, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind2, 1 ), before1, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( ! SweepToPlane( report, Sphere( behind2, 1 ), before2, plane.GetPoint(), plane.GetNormal() ) );

        // ** now let's see if the collision point is correct

        CollisionReport3D tempReport( CollisionCalcsT::Intersection | CollisionCalcsT::Normal );

        Vec3f a(0,0,2);
        Vec3f b(0,0,-2);
        ASSERT( SweepToPlane( tempReport, Sphere( a, 1 ), b, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( tempReport.point, Vec3f(0,0,0), 0.001f ) );

        a.Set(1,1,2);
        b.Set(1,1,-2);
        ASSERT( SweepToPlane( tempReport, Sphere( a, 1 ), b, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( tempReport.point, Vec3f(1,1,0), 0.001f ) );

        a.Set(2,1,2);
        b.Set(-2,1,-2);
        ASSERT( SweepToPlane( tempReport, Sphere( a, 1 ), b, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( tempReport.point, Vec3f(1,1,0), 0.001f ) );

        a.Set(2,2,2);
        b.Set(2,-2,-2);
        ASSERT( SweepToPlane( tempReport, Sphere( a, 1 ), b, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( tempReport.point, Vec3f(2,1,0), 0.001f ) );

        a.Set(20,20,20);
        b.Set(0,0,1);
        ASSERT( SweepToPlane( tempReport, Sphere( a, 1 ), b, plane.GetPoint(), plane.GetNormal() ) );
        ASSERT( Maths::Approxf( tempReport.point, Vec3f(0,0,0), 0.001f ) );

        // ** should not collide

        Sphere sphere( Vec3f(1,0,0), 1 );
        Vec3f sphere_dest(4,0,0);

        const Plane3f back_grazes_back_of_start_sphere( Vec3f(0,0,0), Vec3f(0,1,0), Vec3f(0,1,1) );
        Vec3f planeNormal = back_grazes_back_of_start_sphere.GetNormal( Vec3f(0,0,0), Vec3f(0,1,0), Vec3f(0,1,1) );
        ASSERT( planeNormal == Vec3f(-1,0,0 ) );

        ASSERT( !Sphere::SweepToPlane( tempReport, sphere, sphere_dest, back_grazes_back_of_start_sphere.GetPoint(), planeNormal ) );

        const Plane3f front_grazes_back_of_start_sphere( Vec3f(0,0,0), Vec3f(0,1,1), Vec3f(0,1,0) );
        planeNormal = front_grazes_back_of_start_sphere.GetNormal( Vec3f(0,0,0), Vec3f(0,1,1), Vec3f(0,1,0) );
        ASSERT( planeNormal == Vec3f(1,0,0 ) );

        ASSERT( !Sphere::SweepToPlane( tempReport, sphere, sphere_dest, front_grazes_back_of_start_sphere.GetPoint(), planeNormal ) );
    }

    void Sphere::UnitTest_ToAABox3D( void ) {
        const AABox3D box( -2,2, -2,2, -2,2 );
        Vec3f norm;

        // ** next to sides
        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( Sphere( Vec3f(0,0,0), 1), box, &norm ) ) );

        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( Sphere( Vec3f(-3,0,0), 1), box, &norm ) ) );
        ASSERT( norm == Vec3f(1,0,0) );

        ASSERT( Maths::Approxf( 0.0f, DistToAABox3D( Sphere( Vec3f(3,0,0), 1), box, &norm ) ) );
        ASSERT( norm == Vec3f(-1,0,0) );

        ASSERT( Maths::Approxf( 1.0f, DistToAABox3D( Sphere( Vec3f(-4,0,0), 1), box, &norm ) ) );
        ASSERT( norm == Vec3f(1,0,0) );

        ASSERT( Maths::Approxf( 1.0f, DistToAABox3D( Sphere( Vec3f(4,0,0), 1), box, &norm ) ) );
        ASSERT( norm == Vec3f(-1,0,0) );

        ASSERT( Maths::Approxf( 2.0f, DistToAABox3D( Sphere( Vec3f(5,0,0), 1), box, &norm ) ) );
        ASSERT( norm == Vec3f(-1,0,0) );

        // ** next to edge
        float radius = 1;
        float d = sqrtf(2) - radius;
        ASSERT( Maths::Approxf( d, DistToAABox3D( Sphere( Vec3f(3,3,0), radius), box, &norm ) ) );

        radius = 1.0f;
        d = sqrtf(2*2 + 1*1) - radius;
        ASSERT( Maths::Approxf( d, DistToAABox3D( Sphere( Vec3f(4,3,0), radius), box, &norm ) ) );

        // ** next to corner
        radius = 1.0f;
        d = sqrtf(1*1 + 1*1 + 1*1) - radius;
        ASSERT( Maths::Approxf( d, DistToAABox3D( Sphere( Vec3f(3,3,3), radius), box, &norm ) ) );

        radius = 1.0f;
        d = sqrtf(1*1 + 2*2 + 1*1) - radius;
        ASSERT( Maths::Approxf( d, DistToAABox3D( Sphere( Vec3f(3,4,3), radius), box, &norm ) ) );

        // ** sphere bigger than box, encompassing box, origin inside of box
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 3 ), box, &norm ) );
        ASSERT( Intersects( Sphere( Vec3f(0,0,0), 4 ), box, &norm ) );

        // ** intersecting box almost completely but origin not inside box
        ASSERT( Intersects( Sphere( Vec3f(0,3.5,0), 7 ), box, &norm ) );
    }

    void Sphere::UnitTest_ToPoly( void ) {
        const Vec3f verts[4]{
            Vec3f(-10,0,0),
            Vec3f(-10,10,0),
            Vec3f(10,10,0),
            Vec3f(10,0,0)
        };

        const float radius=1;

        const Sphere touches_face( Vec3f(0,5,-1), radius );
        const Sphere intersects_face( Vec3f(0,5,0), radius );
        const Sphere touches_left_edge( Vec3f(-11,5,0), radius );
        const Sphere touches_right_edge( Vec3f(11,5,0), radius );
        const Sphere touches_bottom_edge( Vec3f(0,-1,0), radius );
        const Sphere touches_top_edge( Vec3f(0,11,0), radius );
        const Sphere touches_corner( Vec3f(-11,0,0), radius );
        const Sphere outside_left( Vec3f(-11.1f,0,0), radius );
        const Sphere outside_right( Vec3f(11.1f,0,0), radius );
        const Sphere outside_top( Vec3f(0,11.1f,0), radius );
        const Sphere outside_bottom( Vec3f(0,-1.1f,0), radius );
        const Sphere before( Vec3f(0,5,-1.1f), radius );
        const Sphere behind( Vec3f(0,5,1.1f), radius );

        ASSERT( ToPoly( touches_face, verts, 4 ) );
        ASSERT( ToPoly( intersects_face, verts, 4 ) );
        ASSERT( ToPoly( touches_left_edge, verts, 4 ) );
        ASSERT( ToPoly( touches_right_edge, verts, 4 ) );
        ASSERT( ToPoly( touches_bottom_edge, verts, 4 ) );
        ASSERT( ToPoly( touches_top_edge, verts, 4 ) );
        ASSERT( ToPoly( touches_corner, verts, 4 ) );
        ASSERT( !ToPoly( outside_left, verts, 4 ) );
        ASSERT( !ToPoly( outside_right, verts, 4 ) );
        ASSERT( !ToPoly( outside_top, verts, 4 ) );
        ASSERT( !ToPoly( outside_bottom, verts, 4 ) );
        ASSERT( !ToPoly( before, verts, 4 ) );
        ASSERT( !ToPoly( behind, verts, 4 ) );
    }

    void Sphere::UnitTest_ToPlane( void ) {
        // first test without CollisionCalcsT::Normal | CollisionCalcsT::Intersection
        {
            CollisionReport3D report;
            const Plane3f touches( Vec3f(10,1,2), Vec3f(11,0,2), Vec3f(10,0,2) );
            //const float touches_dist = 1.0f; // time
            //const Vec3f touches_point(0,0,2);

            const Plane3f barely_intersects( Vec3f(10,1,1.99f), Vec3f(11,0,1.99f), Vec3f(10,0,1.99f) );

            const Plane3f intersects( Vec3f(10,1,1), Vec3f(11,0,1), Vec3f(10,0,1) );
            //const float intersects_dist = 0.5f; // time

            const Plane3f straight_through( Vec3f(10,1,0), Vec3f(11,0,0), Vec3f(10,0,0) );
            //const float straight_through_dist = 0.0f; //time

            const Plane3f doesnt_touch( Vec3f(10,1,2.01f), Vec3f(11,0,2.01f), Vec3f(10,0,2.01f) );
            //const float doesnt_touch_dist = 2.01f; // units

            Sphere sphere( Vec3f(0,0,0), 2 );

            ASSERT( Sphere::ToPlane( sphere, touches ) );
            ASSERT( Sphere::ToPlane( sphere, barely_intersects ) );
            ASSERT( Sphere::ToPlane( sphere, intersects ) );
            ASSERT( Sphere::ToPlane( sphere, straight_through ) );
            ASSERT( !Sphere::ToPlane( sphere, doesnt_touch ) );
        }

        // then with it
        {
            const Plane3f touches( Vec3f(10,1,2), Vec3f(11,0,2), Vec3f(10,0,2) );
            const Vec3f touches_point(0,0,2);
            const Plane3f barely_intersects( Vec3f(10,1,1.99f), Vec3f(11,0,1.99f), Vec3f(10,0,1.99f) );
            const Plane3f intersects( Vec3f(10,1,1), Vec3f(11,0,1), Vec3f(10,0,1) );
            const Plane3f straight_through( Vec3f(10,1,0), Vec3f(11,0,0), Vec3f(10,0,0) );
            const Plane3f doesnt_touch( Vec3f(10,1,2.01f), Vec3f(11,0,2.01f), Vec3f(10,0,2.01f) );

            Sphere sphere( Vec3f(0,0,0), 2 );

            ASSERT( Sphere::ToPlane( sphere, touches ) );
            ASSERT( Sphere::ToPlane( sphere, barely_intersects ) );
            ASSERT( Sphere::ToPlane( sphere, intersects ) );
            ASSERT( Sphere::ToPlane( sphere, straight_through ) );
            ASSERT( !Sphere::ToPlane( sphere, doesnt_touch ) );
        }
    }

    void Sphere::UnitTest_ToBox( void ) {
        const float radius = 2;
        const Box3D box(-2,2,-2,2,-2,2);
        const Sphere succeed_touches_right_side( Vec3f(4,0,0), radius );
        const Sphere succeed_touches_left_side( Vec3f(-4,0,0), radius );
        const Sphere succeed_touches_edge( Vec3f(4,2,0), radius );
        const Sphere succeed_touches_corner( Vec3f(3.99f,2,0), radius ); // a solid 4 fails
        const Sphere succeed_inside( Vec3f(0,0,0), 1 );
        const Sphere succeed_encompass( Vec3f(0,0,0), 10 );
        const Sphere fail_notouches_right_edge( Vec3f(4.01f,0,0), radius );
        const Sphere fail_notouches_corner( Vec3f(4.01f,2,0), radius );

        ASSERT( Sphere::Intersects( succeed_inside, box ) );
        ASSERT( Sphere::Intersects( succeed_encompass, box ) );

        ASSERT( Sphere::Intersects( succeed_touches_corner, box ) );

        ASSERT( Sphere::Intersects( succeed_touches_right_side, box ) );
        ASSERT( Sphere::Intersects( succeed_touches_left_side, box ) );

        ASSERT( ! Sphere::Intersects( fail_notouches_right_edge, box ) );
        ASSERT( ! Sphere::Intersects( fail_notouches_corner, box ) );
    }
#endif //CLIB_UNIT_TEST
