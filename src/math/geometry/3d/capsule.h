// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_COLLISION_3D_CAPSULE_H_
#define SRC_COLLISION_3D_CAPSULE_H_

#include "../../../base/main.h"
#include "../../geometry.h"

class Line3f;
class Line2f;

typedef Uint8 LineTypeT_BaseType;
enum class LineTypeT : LineTypeT_BaseType;

class Sphere;
class SemiCylinder;
class Polygon3D;

class Capsule {
public:
    Capsule( void )
        : origin(), normal(), radius(0), length(0) { } //!< the origin is the bottom of the cylinder

    Capsule( const Vec3f& org, const Vec3f& norm, const float rad, const float len )
        : origin( org ), normal( norm ), radius( rad ), length( len ) { } //!< the origin is the bottom of the cylinder

public:
    static Capsule FromSweepSphere( const Sphere& sphere, const Vec3f& sphere_destination );
    static Capsule FromPoints( const Vec3f& start, const Vec3f& end, const float _radius );

    static bool ToPlane( const Capsule& capsule, const Vec3f& plane_a, const Vec3f& plane_b, const Vec3f& plane_c );
    static bool ToPlane( const Capsule& capsule, const Vec3f& point_on_plane, const Vec3f& plane_normal );
    static bool ToLine( const Capsule& capsule, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end );
    
    static bool Intersects( const Capsule& capsule, const Vec3f& point );
    static bool Intersects( const Capsule& capsule, const Box3D& aabox );
    static bool Intersects( const Capsule& capsule, const AABox3D& aabox );
    static bool Intersects( const Capsule& capsule, const Plane3f& plane );
    static bool Intersects( const Capsule& capsule, const Sphere& sphere );
    static bool Intersects( const Capsule& capsule, const Line3f& line );
    static bool Intersects( const Capsule& capsule, const Cylinder& cyl );
    static bool Intersects( const Capsule& capsule, const SemiCylinder& cyl );
    static bool Intersects( const Capsule& capsule, const Capsule& cyl );
    static bool Intersects( const Capsule& capsule, const Polygon3D& cyl );
    
    static bool ToModel( const Capsule& cap, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin );
    static bool SweepToModel( CollisionReport3D& report, const Capsule& cap, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType );

    #ifdef CLIB_UNIT_TEST
        static void UnitTest( void );

        static void UnitTest_ToPlane( void );
        static void UnitTest_ToAABox3D( void );
    #endif //CLIB_UNIT_TEST
public:
    Vec3f GetFarthestVert( const Vec3f& normalized_dir ) const;
    Vec3f GetEnd( void ) const;

public:
    Vec3f origin; //!< the origin is the bottom of the cylinder
    Vec3f normal;
    float radius;
    float length;
};


#endif // SRC_COLLISION_3D_CAPSULE_H_
