// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include <cfloat>
#include "./capsule.h"
#include "../../collision/collision.h"

Vec3f GetCornerFromMask( const AABox3D& aabox, uint mask );
Vec3f GetCornerFromMask( const AABox3D& aabox, uint mask ) {
    Vec3f c;
    c.x = ( (mask&1) ? aabox.max.x : aabox.min.x);
    c.y = ( (mask&2) ? aabox.max.y : aabox.min.y);
    c.z = ( (mask&4) ? aabox.max.z : aabox.min.z);
    return c;
}

bool Capsule::Intersects( const Capsule& capsule, const Sphere& sphere ) {
    return Sphere::Intersects( sphere, capsule );
}

bool Capsule::Intersects( const Capsule& capsule, const SemiCylinder& semiCyl ) {
    return SemiCylinder::Intersects( semiCyl, capsule );
}

bool Capsule::Intersects( const Capsule& capsule, const Vec3f& point ) { //todo:unittest
    const Vec3f line_start( capsule.origin );
    const Vec3f line_end( capsule.origin + capsule.normal * capsule.length );
    return Line3f::GetDistToPoint( LineTypeT::SEG, line_start, line_end, point ) <= capsule.radius;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
    bool Capsule::Intersects( const Capsule& capsule, const Line3f& line ) {
        ERR("Not Implemented: Capsule to Line3f.\n"); //toimplement
        return false;
    }
    
    bool Capsule::Intersects( const Capsule& capsule, const Box3D& box ) {
        ERR("Not Implemented: Capsule to Box3d.\n"); //toimplement
        return false;
    }

    bool Capsule::Intersects( const Capsule& cap1, const Capsule& cap2 ) {
        ERR("Not Implemented: Capsule::Intersects( const Capsule& cap1, const Capsule& cap2 )\n"); //toimplement
        return false;
    }

    bool Capsule::Intersects( const Capsule& cap, const Polygon3D& poly ) {
        ERR("Not Implemented: Capsule::Intersects( const Capsule& cap, const Polygon3D& poly )\n"); //toimplement
        return false;
    }

    bool Capsule::ToModel( const Capsule& cap, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin ) {
        ERR("Not Implemented: Capsule to ToModel.\n"); //toimplement
        return false;
    }

    bool Capsule::SweepToModel( CollisionReport3D& report, const Capsule& cap, const Vec3f& vel, const Model& model, const Vec3f& model_angles, const Vec3f& model_origin, const ModelCollisionT clipType ) {
        ERR("Not Implemented: Capsule sweep to ToModel.\n"); //toimplement
        return false;
    }
#pragma GCC diagnostic pop

bool Capsule::Intersects( const Capsule& capsule, const Plane3f& plane ) {
    return Capsule::ToPlane( capsule, plane.GetPoint(), plane.GetNormal() );
}

bool Capsule::Intersects( const Capsule& capsule, const AABox3D& aabox ) {
    // if capsule's line does not intersect with an aabox that is expanded in all
    // directions by the radius of the capsule, there is no collision
    const Line3f capsule_line(capsule.origin, capsule.GetEnd(), LineTypeT::SEG);
    const AABox3D expanded_aabox( aabox.min - capsule.radius, aabox.max + capsule.radius );
    CollisionReport3D report( CollisionCalcsT::Intersection );
    if ( ! Line3f::Intersects( report, capsule_line, expanded_aabox ) )
        return false;
/**/ // todo: IntersectMovingSphereAABB https://books.google.com/books?id=WGpL6Sk9qNAC&pg=PA228&lpg=PA228&dq=capsule+aabb&source=bl&ots=Pn3MkH0fhJ&sig=F32MDlbfG7sosIvzYc33YWVxwAQ&hl=en&sa=X&ved=0ahUKEwiMtf368YfMAhVmhYMKHQL-AIIQ6AEISjAD#v=onepage&q=capsule%20aabb&f=false

    uint u=0, v=0;

    for ( uint i = 0; i < 3; ++i ) {
        if ( report.point[i] < aabox.min[i] )
            u |= 1u<<i;
        if ( report.point[i] > aabox.max[i] )
            v |= 1u<<i;
    }

    const uint mask = u+v;

    // encompass
    if ( mask < 1 )
        return true;

    // intersects all faces?
    if ( mask == 7 ) { // point is near a vertex
//todo        float tmin = FLT_MAX;

        Capsule edge_cap = Capsule::FromPoints( GetCornerFromMask(aabox, v), GetCornerFromMask(aabox, v^1), capsule.radius );
        if ( Line3f::Intersects( capsule_line, edge_cap ) )
            return true;
//todo        float t = Line3f::Intersects( capsule_line, edge_cap );
//todo        tmin = std::fminf(tmin, t);

        edge_cap = Capsule::FromPoints( GetCornerFromMask(aabox, v), GetCornerFromMask(aabox, v^2), capsule.radius );
        if ( Line3f::Intersects( capsule_line, edge_cap ) )
            return true;
//todo        t = Line3f::Intersects( capsule_line, edge_cap );
//todo        tmin = std::fminf(tmin, t);

        edge_cap = Capsule::FromPoints( GetCornerFromMask(aabox, v), GetCornerFromMask(aabox, v^4), capsule.radius );
        if ( Line3f::Intersects( capsule_line, edge_cap ) )
            return true;
//todo        t = Line3f::Intersects( capsule_line, edge_cap );
//todo        tmin = std::fminf(tmin, t);

        return false;
//todo        return ( tmin < FLT_MAX );
    }

    // point is only outside one face?
    if ( ( mask & (mask-1) ) == 0 ) {
        return true; // point is on a face
    }

    // ** point is on an edge. make a capsule from the edge and test it against the point
    const Capsule edge_cap = Capsule::FromPoints( GetCornerFromMask(aabox, u^7), GetCornerFromMask(aabox, v), capsule.radius );
    return Line3f::Intersects( capsule_line, edge_cap );

    //return Sphere::SweepToBox3D( Sphere( capsule.origin, capsule.radius ), capsule.origin + capsule.normal * capsule.length, Box3D( aabox ) );

    //return Collision3D::TestGJK::Intersects( capsule, Box3D( aabox ) );
}

Vec3f Capsule::GetEnd( void ) const {
    return {
          normal.x * length + origin.x
        , normal.y * length + origin.y
        , normal.z * length + origin.z
    };
}

Vec3f Capsule::GetFarthestVert( const Vec3f& normalized_dir ) const {
    const Vec3f a( Sphere::GetFarthestVert( normalized_dir, origin, radius ) );
    const Vec3f b( Sphere::GetFarthestVert( normalized_dir, GetEnd(), radius ) );

    if ( a.Dot( normalized_dir ) > b.Dot( normalized_dir ) )
        return a;
    return b;
}

Capsule Capsule::FromPoints( const Vec3f& start, const Vec3f& end, const float _radius ) {
    Vec3f norm = end - start;
    const float len = norm.Len();
    norm /= len;

    return Capsule( start, norm, _radius, len );
}

Capsule Capsule::FromSweepSphere( const Sphere& sphere, const Vec3f& sphere_destination ) {
    Vec3f norm( sphere_destination - sphere.origin );
    const float len = norm.Len();
    if ( !Maths::Approxf( len, 0.0f ) )
        norm /= len;
    return Capsule( sphere.origin, norm, sphere.radius, len );
}

bool Capsule::ToLine( const Capsule& capsule, const LineTypeT line_type, const Vec3f& line_start, const Vec3f& line_end ) {
    return Line3f::ToCapsule( line_type, line_start, line_end, capsule );
}

bool Capsule::ToPlane( const Capsule& capsule, const Vec3f& point_on_plane, const Vec3f& plane_normal ) {

    // if it's not a valid plane, it doesn't collide.
    if ( plane_normal.SquaredLen() < (1-EP) )
        return false;

    // ** if either end-cap (sphere) collides with the plane, there is collision
    const float start_dist = Plane3f::GetDistToPoint( capsule.origin, point_on_plane, plane_normal );
    if ( fabsf( start_dist ) <= capsule.radius )
        return true;

    const float end_dist = Plane3f::GetDistToPoint( capsule.GetEnd(), point_on_plane, plane_normal );
    if ( fabsf( end_dist ) <= capsule.radius )
        return true;

    // ** if the start and end points are on opposite sides of the plane, there is collision
    if ( start_dist < 0 ) {
        if ( end_dist > 0 ) {
            return true; // plane slices through the cylinder of our capsule
        }
    } else {
        if ( end_dist < 0 ) {
            return true; // plane slices through the cylinder of our capsule
        }
    }

    return false;
}

bool Capsule::ToPlane( const Capsule& capsule, const Vec3f& plane_a, const Vec3f& plane_b, const Vec3f& plane_c ) {
    return Capsule::ToPlane( capsule, plane_a, Plane3f::GetNormal( plane_a, plane_b, plane_c ) );
}

bool Capsule::Intersects( const Capsule& capsule, const Cylinder& cyl ) {
    return Cylinder::Intersects( cyl, capsule );
}
