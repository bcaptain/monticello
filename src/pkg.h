// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_PKG_H_
#define SRC_PKG_H_

#include "./clib/src/warnings.h"
#include "./base/main.h"

class Packages;

typedef void (*ManifestFuncPtr)( const char* directory );

/*!

Packages \n\n

The package system will populate the manifests for each asset manager. \n
Zip packages must be in ./run/data, other files can be in subdirectories of ./run/data \n

**/

class Packages {
public:
    static void ManifestAll( void ); //!< manifest all the packages we have

private:
    static void ScanDir( const char* dir, ManifestFuncPtr func ); //! recursively search for and call func on all files

    static void ManifestZip( const char* pkg_root ); //!< manifest all the files in a zip
    static void ManifestDir( const char* pkg_root ); //!< manifest a directory

    static void Manifest( const char* file_path, const char* file_container = "" /* blank means filesystem */ ); //!< actaully do the manifesting
};

#endif  // SRC_PKG_H_
