// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./laser.h"
#include "./game.h"
#include "./rendering/renderer.h"
#include "./rendering/vbo.h"
#include "./math/geometry/3d/plane.h"

VBO<float>* Laser::vbo_coord( nullptr );
uint Laser::num_lasers_in_world( 0 );

const Vec3f DEFAULT_LASER_DIRECTION( 0,-1,0 ); // negative on the Y because that's the direction entities face by default

Laser::Laser( void )
    : origin(0,0,0)
    , dir(DEFAULT_LASER_DIRECTION)
    , color(1,0,0)
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::FREQUENTLY ) )
    , length( 15 )
    , cap_length( length / 10 )
    , width( 0.3f )
    , intensity( 0.7f )
    , fade( true )
{
    Construct();
}

Laser::Laser( const float _length, const float _width, const Vec3f& _color )
    : origin(0,0,0)
    , dir(DEFAULT_LASER_DIRECTION)
    , color(_color)
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::FREQUENTLY ) )
    , length( _length )
    , cap_length( length / 10 )
    , width( _width )
    , intensity( 0.7f )
    , fade( true )
{
    Construct();
}

Laser::Laser( const Laser& other )
    : origin( other.origin )
    , dir( other.dir )
    , color( other.color )
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::FREQUENTLY ) )
    , length( other.length )
    , cap_length( other.cap_length )
    , width( other.width )
    , intensity( other.intensity )
    , fade( other.fade )
{
    Construct();
}

void Laser::Construct( void ) {
    if ( num_lasers_in_world == 0 ) {
        ASSERT( ! vbo_coord );
        vbo_coord = new VBO<float>( 2, VBOChangeFrequencyT::RARELY );
        PackCoordVBO();
    }

    ++num_lasers_in_world;
}

Laser& Laser::operator=( const Laser& other ) {
    origin = other.origin;
    dir = other.dir;
    color = other.color;
    vbo_vert->Clear();
    length = other.length;
    cap_length = other.cap_length;
    width = other.width;
    intensity = other.intensity;
    fade = other.fade;

    return *this;
}

Laser::~Laser( void ) {
    delete vbo_vert;

    --num_lasers_in_world;
    if ( num_lasers_in_world == 0 ) {
        ASSERT( vbo_coord );
        DELNULL( vbo_coord );
    }
}

void Laser::DrawVBO( void ) const {
//    glDepthMask( GL_FALSE );

    // VBO needs to be rebuilt every time the camera moves
    ASSERT( vbo_coord );
    ASSERT( vbo_coord->Finalized() );
    ASSERT( vbo_vert );
    ASSERT( vbo_vert->Finalized() );

    const float pulsation = intensity + sinf( game->GetRenderTime() / FPS_CAP ) / 26;

    shaders->UseProg(  GLPROG_LASER  );
    shaders->SendData_Matrices();
    shaders->SetUniform3f("vColor" , color);
    shaders->SetUniform1f("fIntensity", pulsation );
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->SetAttrib( "vCoord", *vbo_coord );
    shaders->DrawArrays( GL_QUADS, 0, vbo_vert->Num() );

//    glDepthMask( GL_TRUE );
}

void Laser::PackCoordVBO( void ) {
    ASSERT( vbo_coord );
    ASSERT( ! vbo_coord->Finalized() );

    const uint data_size=16;
    const float data[data_size]{
        1,-1, 1,1, // start of laser
        1,1, 1,-1, // end of laser (before cap)
        1,-1, 1,1, // start of laser cap
        0,0, 0,0, // end of laser cap
    };

    vbo_coord->PackArray( data_size, data );
    vbo_coord->MoveToVideoCard();
}

void Laser::PackVBO( const float laser_length ) {
    if ( vbo_vert->Finalized() )
        return;

    Vec3f laser_start[2];
    Vec3f laser_end[2];
    Vec3f cap_start[2];
    Vec3f cap_end[2];

    // get the normal of a plane from the laser's end points and the camera's position.
    const Vec3f normal( Plane3f::GetNormal( origin, origin + dir, renderer->camera.GetOrigin() ) * width );

    laser_start[0] = laser_start[1] = origin;
    laser_start[0] += normal;
    laser_start[1] -= normal;

    const Vec3f vec( dir * (laser_length - cap_length) );
    laser_end[0] = laser_end[1] = cap_start[0] = cap_start[1] = cap_end[0] = cap_end[1] = origin + vec;
    laser_end[1] += normal;
    cap_start[0] += normal;
    laser_end[0] -= normal;
    cap_start[1] -= normal;

    const Vec3f cap_vec( dir * cap_length );
    cap_end[0] += cap_vec;
    cap_end[1] += cap_vec;

    vbo_vert->PackArray( 2, laser_start );
    vbo_vert->PackArray( 2, laser_end );
    vbo_vert->PackArray( 2, cap_start );
    vbo_vert->PackArray( 2, cap_end );
    vbo_vert->MoveToVideoCard();
}

void Laser::SetLength( const float to ) {
    length = to;
    cap_length = 0;

    // reset fade
    if ( fade )
        SetFade( true );

    UpdateVBO();
}

void Laser::SetFade( const bool whether ) {
    fade = whether;

    if ( fade ) {

        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wfloat-equal" // this is okay
        if ( cap_length == 0 ) {
        #pragma GCC diagnostic pop
            cap_length = length / 10;
        }
        return;
    }

    cap_length = 0;
    UpdateVBO();
}

void Laser::UpdateVBO( void )  {
    vbo_vert->Clear();
}

void Laser::SetIntensity( const float to ) {
    intensity = to;
}

void Laser::SetOrigin( const float x, const float y, const float z ) {
    origin.Set( x,y,z );
}

void Laser::SetOrigin( const Vec3f& to ) {
    origin = to;
}

float Laser::GetLength( void ) const {
    return length;
}
