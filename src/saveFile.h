// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_SAVEFILE_H_
#define SRC_SAVEFILE_H_

#include "./clib/src/warnings.h"
#include "./base/main.h"

class Entity;
class Navmesh_Poly;

class FileMap : public File {
    friend class Game; //todoattorney

public:
    FileMap( void );
    FileMap( FileMap& other ) = delete;
    ~FileMap( void );

public:
    FileMap& operator=( const FileMap& other ) = delete;

public:
    void WriteEntityRef( const Entity* ent );
    std::shared_ptr< Entity > ReadEntityRef( void );

    bool Save( const char * fname ); //!< will open, save, and then close
    bool Load( const char * fname ); //!< will open and call Load( void )
    bool Load( void ); //!< will load, and then close

    void ReadDict( Dict& dict );
    void WriteDict_IgnoreFromDict( const Dict& dict, const Dict* values_to_ignore );
    void WriteDict_IgnoreFromVec( const Dict& dict, const std::vector< DictEntry >* values_to_ignore );
    void WriteDict( const Dict& dict );
    
    void ReadAABox3D( AABox3D& aabox );
    void WriteAABox3D( const AABox3D& aabox );
    
    void ReadBox3D( Box3D& box );
    void WriteBox3D( const Box3D& box );
    
    void SetPlayer( void ) const;
    void SetFollowEnt( void ) const;

    Uint8 GetMapWidth( void ) const;
    Uint8 GetMapLength( void ) const;
    Uint8 GetMapHeight( void ) const;
    
    const std::vector< std::shared_ptr< Entity > >& GetEntList_Ref( void ) const { return entities; }

private:
    void SaveEntities( LinkList< std::string >& string_pool );
    void LoadEntities( const std::vector< std::string >& string_pool );

private:
    std::vector< std::shared_ptr< Entity > > entities;
    Uint8 width;
    Uint8 length;
    Uint8 height;
    bool hasPlayer;
    uint playerIndex;
    bool hasFollowEnt;
    uint followEntIndex;

public:
    ParserBinary parser;
};

#endif  // SRC_SAVEFILE_H_
