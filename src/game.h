// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_GAME_H_
#define SRC_GAME_H_

#include "./clib/src/warnings.h"
#include "./base/main.h"
#include "./base/time.h"
#include "./mapscripts/mapscript.h"
#include "./rendering/order.h"
#include "./ui/dialog/common.h"

const int SPRITE_LAYERS=10;

class Game;
class SDL;
class GameUI_Page;
class GameUI_Timer;
class GameUI_InfestLevel;
class Window;
class Actor;
class EntityTree;
class Trigger_SafeZone;
class HUD_Stats;
struct MapScript;
struct MoveOp;
class AssetManifest;
class SpriteList;
class Trigger_Door;
class Flat;
class Sprite;

extern Game* game;
extern SDL sdl;
extern const std::string MONTICELLO_MARK;
extern const std::string MONTICELLO_VERSION;
extern const std::string DataDir;

typedef Uint8 HistoryDisplayT_BaseType;
enum class HistoryDisplayT : HistoryDisplayT_BaseType;

class Entity;
typedef void (Entity::*EntityVoidMethodPtr)( void );

typedef Uint8 GameStateT_BaseType;
enum class GameStateT : GameStateT_BaseType {
    INIT
    , MAP_LOAD
    , MAP_CLEAR
    , SHUTDOWN
    , ABORT // used for places throughout the engine where where we want to kill the game and get a stack trace, for example
    , READY // if you're "less than ready" you're not ready (:
};

typedef Uint32 PauseStateT_BaseType;
enum class PauseStateT : PauseStateT_BaseType {
    Unpaused = 0
    , PausedByConsole = 1
    , PausedByEditor = 1 << 1
    , PausedByMenu = 1 << 2
    , PausedByPlayer = 1 << 3 // !< player pause has a "weak hold" on the game - any time we unpause for any reason, this bit get scleared
    , PausedByDialog = 1 << 4
    , PausedByGameUI = 1 << 5
    , PausedByLoad = 1 << 6
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( PauseStateT, PauseStateT_BaseType )

class EntityEvent {
public:
    EntityEvent( void ) = delete;
    ~EntityEvent( void ) = default;

    EntityEvent( std::weak_ptr< Entity >& entity, EntityVoidMethodPtr entity_method ) : ent(entity), method( entity_method ) {}

    EntityEvent( const EntityEvent& other ) : ent( other.ent ), method( other.method ) {}
    EntityEvent& operator=( const EntityEvent& other ) { ent = other.ent; method = other.method; return *this; }

public:
    std::weak_ptr< Entity > ent;
    EntityVoidMethodPtr method;
};

struct GameEvent {
    GameEvent( const std::string& eventname, const std::vector< std::weak_ptr<Entity> >& entlist )
        : name( eventname )
        , entities( entlist )
        {}
        
    std::string name;
    std::vector< std::weak_ptr<Entity> > entities;
};

void CloseTopWindow_Click( void );
void OpenUIPage_Click( const std::vector< std::string >& pageName );

class Game {
    friend void debug_print_entity_indices( void );

private:
    class Map {
    public:
        Map( void ) = delete;
        ~Map( void ) = delete;
        Map( const Map& other ) = delete;

    public:
        #ifdef MONTICELLO_EDITOR
        static bool Save( const char * filename );
        #endif // MONTICELLO_EDITOR
        static bool Load( const char * filename );
    };

public:
    Game( void );
    ~Game( void );
    Game( const Game& other ) = delete;

public:
    Game& operator=( const Game& other ) = delete;

public:
    void Init( void );
    void Deinit( void );
private:
    void InitEntTree( void );
public:
    void MainLoop( void );
public:
    void UpdateTime( void );
    void ProcessEntities( void );
#ifdef MONTICELLO_EDITOR
    void ProcessEditor( void );
#endif // MONTICELLO_EDITOR
    void CheckGamestate( void );
private:
    void DeleteMarkedEntities( void );
    void RemoveEntity( const uint index );
public:
    void NextFrame( void ); //!< for debugging only

    void UpdateCursor( void ) const;

#ifdef MONTICELLO_EDITOR
    void ToggleEditor( void );
#endif // MONTICELLO_EDITOR

    void PressEscape( void );
    
    void QueueEvent_EntityMethod( Entity& ent, EntityVoidMethodPtr method );
    void QueueEvent_DisplayLevelSummary( void );
    
private:
    void DoGameEvents( void );
    void DoGameEvent_DisplayLevelSummary( const GameEvent& event_unused );
public:

    // ** Game UI Pages
    std::shared_ptr< GameUI_Page > GetUIPage( const std::string& pageName );
    bool OpenUIPage( const std::shared_ptr< GameUI_Page >& page );
    bool OpenUIPage( const std::string& pageName );
    void AddWorldUI( std::shared_ptr< Sprite >& sprite );
    void DrawWorldUI( void );
    void DrawHUDUI( void );
    void InitUIPages( void );
    bool CloseTopWindow( void );

    // ** UI
    Window* GetTopWindow( void ) const; //!< searches for a window, only in the sprite layers responsible for windows
    Window* GetTopDialog( void ) const; //!< searches for a dialog, only in the sprite layers responsible for dialogs
    Window* GetTopWindowOrDialog( void ) const; //!< searches for a dialog, only in the sprite layers responsible for dialogs
    Window* GetTopWindow( const std::vector< UI_LayerT_BaseType >& ordered_layers ) const; //!< searches for a window in the layers specified

    void ResetLevelTimer( void );
    void AddLevelTime( const uint seconds );
    void SubtractLevelTime( const uint seconds );
    
    void DecreaseInfestLevel( const int amt );
    void IncreaseInfestLevel( const int amt );
    int GetInfestLevel( void ) const;

    // ** Map Loading
public:
    void LoadMap( const char *name ); //!< sets next_map and changes gamestate to MAP_LOAD
    void LoadMap( const std::string& mapName ); //!< sets next_map and changes gamestate to MAP_LOAD
    void ReloadMap( void ); //!< sets next_map to loaded_map and changes gamestate to MAP_LOAD
    bool MapLoadFinished( void );
private:
    bool LoadNextMap(); //!< this acutally performs the load, and also clears next_map when cone
    bool LoadMap( FileMap& openedMapFile );

    // **

    // ** Map Clearing
public:
    void ClearMap( void ); //!< changes gamestate to MAP_CLEAR
    void ClearMapNow( void ); //!< this actually clears the map

private:
    void DoMapScript( void );

public:
#ifdef MONTICELLO_EDITOR
    bool CheckIfOkayToSaveMap( void ) const;
    bool SaveLoadedMap( void );
    bool SaveMapAs( const std::string& name );
    bool SaveMapAs( const char *name );
#endif // MONTICELLO_EDITOR

    bool IsInitialized( void ) const;

    void SetState( const GameStateT newState );
    GameStateT GetState( void ) const;
    std::shared_ptr< Actor > GetPlayer( void ) const;
    std::shared_ptr< Entity > GetPlayerEntity( void ) const;
    void SetPlayer( const std::weak_ptr< Actor >& p );

    void SetSafeZone( const std::weak_ptr< Entity >& to );

    Uint32 GetGameTime( void ) const; //!< time in the game (stops incrementing when paused)
    Uint32 GetRenderTime( void ) const;
    Uint32 GetRealTime( void ) const;
    Uint32 GetGameFrame( void ) const; //!< frame the game is on (stops incrementing when paused)
    Uint32 GetRenderFrame( void ) const; //!< number of frames the renderer has done, total

    bool IsPaused( void ) const;
    bool IsPausedBy( const PauseStateT _state ) const;
    void Pause( const PauseStateT _state );
    void Unpause( const PauseStateT _state );
    void TogglePause( const PauseStateT _state );

    bool RespawnPlayer( Entity& playerEnt );
    void UnsetPlayer( void );
    void UnsetPlayer( const Actor* actor );
    bool IsPlayer( const Entity* entity ) const;
    bool IsPlayer( const Entity& entity ) const;
    bool IsPlayer( const std::shared_ptr< Entity >& entity ) const;

    void MarkEntityForDeletion( const std::weak_ptr< Entity >& ent );
    void UnmarkEntityForDeletion( const std::shared_ptr< Entity >& ent );

    bool SetEntTreeDimensions( const uint x, const uint y, const uint z );
    bool SetEntTreeSubdivisions( const uint subs );

    bool LoadMoverScript( const char* name, LinkList< MoveOp >& moveOps ) const;

    void UnloadUnusedAssets( void );

    void CallOnAllEntities( EntityVoidMethodPtr method );
    void CallOnAwakeEntities( EntityVoidMethodPtr method );

    const std::vector< std::shared_ptr< Entity > >& GetEntityListRef( void ) const;

    uint NumEnts( void ) const;
    
    bool IsEditorOpen( void ) const;
    bool IsMenuOpen( void ) const;
    bool IsConsoleOpen( void ) const;

    static int Compare_EntNameToEntName( const std::weak_ptr< Entity >& A, const std::shared_ptr< Entity >& B );
    static bool ComparesLess_EntNameToEntName( const std::weak_ptr< Entity >& A, const std::shared_ptr< Entity >& B );

    static int Compare_EntNameToStr( const std::weak_ptr< Entity >& A, const std::string& B );
    static bool ComparesLess_EntNameToStr( const std::weak_ptr< Entity >& A, const std::string& B );

    void AwakenEntity( const std::shared_ptr< Entity >& ent ); //!< add entity to awake list

    std::shared_ptr< Entity > NewEnt( const Entity& entCopy );
    std::shared_ptr< Entity > NewEnt( const TypeID type_id );

    std::shared_ptr< Entity > NewUnmanagedEnt( const TypeID type_id ); //!< entity will not be inserted into the all_entities list
    std::shared_ptr< Entity > NewUnmanagedEnt( const Entity& other ); //!< entity will not be inserted into the all_entities list
    
    void InsertEntity( const std::shared_ptr< Entity >& ent );

private:
    std::shared_ptr< Entity > NewEntityFromID( const TypeID type_id );
    
    template< typename TYPE >
    std::shared_ptr< Entity > NewEnt_Helper( const Entity& other );

    template< typename TYPE >
    std::shared_ptr< Entity > NewEnt_Helper( void );
    

    void LoadSafeConfigDefaults( void );
public:
    void LoadConfig( void );
    void SaveConfig( void );
    
    void ResaveAllMaps( void );

public:
    void RegisterMap( const std::string& path, const char* container = "" /* blank means filesystem */ );
    void RegisterGen( const std::string& path, const char* container = "" /* blank means filesystem */ );
    void RegisterMoverScript( const std::string& path, const char* container = "" /* blank means filesystem */ );
    uint GetAllMapNames( LinkList< std::string >& names ) const; //!< returns number of names put into list

    float GetFPS( void ) const;
    Uint32 GetFrame( void ) const;
private:
    LinkList< std::weak_ptr<Sprite> > world_ui;
public:
    HUD_Stats* hud_stats;
    GameUI_Timer* ui_timer;
    GameUI_InfestLevel* ui_infestLevel;
    std::unique_ptr< EntityTree > entTree;

    /*! all sprites to be rendered and/or checked for collision. \n
        Destruction of those sprites, as well as those contained in Windows, are handled within.
    */
    SpriteList* sprites;

    std::vector< std::shared_ptr< GameUI_Page > > game_ui_pages;
    
public:
    HashList< AssetManifest > gen_manifest;
    HashList< AssetManifest > map_manifest;
    
private:
    HashList< AssetManifest > moverScript_manifest;
    Scheduler scheduler;
    std::string loaded_map;
    std::string next_map;

    MapScript mapscript;

private:
    GameStateT state;
    std::weak_ptr< Actor > player;
    std::weak_ptr< Entity > safezone;
    Uint32 prevFPSUpdate;

public:
    Random randnum;
    std::vector< std::pair< std::string, HistoryDisplayT > > preConsoleMessages;

private:
    std::vector< EntityEvent > entityEventStack;
    std::vector< std::shared_ptr< Entity > > all_entities; //!< all entities, whether or not they are in the ent-tree, spawned, or anything, sorted by their names
    std::vector< std::weak_ptr< Entity > > awake_entities; //!< entities that want Think() called on them
    std::vector< std::weak_ptr< Entity > > ents_to_delete;
    std::vector< GameEvent > game_events;

    PauseStateT pause_state;
public: //todo: private
#ifdef MONTICELLO_EDITOR
public:
    bool editor_open;
#endif // MONTICELLO_EDITOR
private:
    bool runOneFrame;
    bool isInitialized;
    static constexpr const bool respawnPlayerWhenDead=false;
};

template< typename TYPE >
std::shared_ptr< Entity > Game::NewEnt_Helper( const Entity& other ) {
    std::shared_ptr< TYPE > obj( std::make_shared< TYPE >( *static_cast< const TYPE* >( &other ) ) );
    std::shared_ptr< Entity > ent( std::static_pointer_cast< Entity >( obj ) );
    return ent;
}

template< typename TYPE >
std::shared_ptr< Entity > Game::NewEnt_Helper( void ) {
    std::shared_ptr< Entity > ent( std::static_pointer_cast< Entity >( std::make_shared< TYPE >() ) );
    return ent;
}

#endif  // SRC_GAME_H_
