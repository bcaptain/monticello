// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./joystick.h"
#include "../sdl/sdl.h"
#include "../game.h"

Joystick::Joystick( void )
    : axis{0}
    , joystick( nullptr )
    , controller( nullptr )
    , instance(-1)
    , padType(PadType::Generic)
    , hat_state(0) //!< same type as SDL_JoyHatEvent.value
{ }

Joystick::~Joystick( void ) {
    Close();
}

Joystick::Joystick( Joystick && other_rref )
    : axis{0}
    , joystick( other_rref.joystick )
    , controller( other_rref.controller )
    , instance( other_rref.instance )
    , padType(PadType::Generic)
    , hat_state(other_rref.hat_state) //!< same type as SDL_JoyHatEvent.value
{
    other_rref.joystick = nullptr;
    other_rref.controller = nullptr;
    other_rref.instance = -1;
    for ( uint i=0; i<MAX_JOYSTICK_AXIS; ++i ) {
        axis[i]=other_rref.axis[i];
        other_rref.axis[i]=0;
    }
    other_rref.hat_state = 0;
}

Joystick& Joystick::operator=( Joystick && other_rref ) {
    joystick = other_rref.joystick;
    other_rref.joystick = nullptr;

    controller = other_rref.controller;
    other_rref.controller = nullptr;

    instance = other_rref.instance;
    other_rref.instance = -1;

    padType = other_rref.padType;

    for ( uint i=0; i<MAX_JOYSTICK_AXIS; ++i ) {
        axis[i]=other_rref.axis[i];
        other_rref.axis[i]=0;
    }
    
    other_rref.hat_state = 0;

    return *this;
}

void Joystick::Close( void ) {
    if ( controller ) {
        SDL_GameControllerClose( controller );
        CMSG_LOG("Closed supported joystick (instance %i)\n", instance );
    } else if ( joystick ) {
        SDL_JoystickClose( joystick );
        CMSG_LOG("Closed unsupported joystick (instance %i)\n", instance );
    }

    controller = nullptr;
    joystick = nullptr;
    instance = -1;
}

bool Joystick::TryOpen( void ) {
    const int num_sticks = SDL_NumJoysticks();
    for ( int i = 0; i < num_sticks; ++i ) {
        // for supported sticks
        if ( SDL_IsGameController(i) ) {

            auto new_controller = SDL_GameControllerOpen(i);
            if ( !new_controller || new_controller == controller)
                continue;

            auto new_joystick = SDL_JoystickOpen( i );
            if ( !new_joystick ) {
                ERR("Error opening supported controller at index %i: %s\n", i, SDL_GetError(), SDL_GetError() );
                continue;
            }

            Close(); // close any previous joystick

            controller = new_controller;
            joystick = new_joystick;
            SetPadType( SDL_GameControllerName( controller ) );
            
            //int num_hats = SDL_JoystickNumHats( new_joystick );
            //const char * joyStickName = SDL_JoystickName( new_joystick );
/*
            char *mapping_ptr = SDL_GameControllerMapping( controller );
            std::string mapping( emapping_ptr );
            SDL_free( mapping_ptr );
*/
            instance = SDL_JoystickInstanceID( new_joystick );
            CMSG_LOG("Found supported joystick (instance %i) at index %i\n", instance, i );
            
            PumpStartupEvents();
            
            return true;
        }

        // for unsupported sticks
        SDL_Joystick *new_joystick = SDL_JoystickOpen( i );
        if ( !new_joystick ) {
            ERR("Error opening unsupported controller at index %i: %s\n", i, SDL_GetError() );
            continue;
        }

        Close(); // close any previous joystick
        joystick = new_joystick;
        controller = nullptr;
        instance = SDL_JoystickInstanceID( joystick );
        if ( instance < 0 ) {
            ERR("Error getting instance of unsupported controller at index %i: %s\n", i, SDL_GetError() );
            continue;
        }
        
        CMSG_LOG("Found unsupported joystick (instance %i) at index %i\n", instance, i );
        SetPadType( SDL_GameControllerNameForIndex(i) );
        return true;
    }

    return false;
}

void Joystick::PumpStartupEvents( void ) {
    if ( GetPadType() != PadType::Playstation )
        return;

    //PS4 controller : On startup L/R Triggers report 0 instead of -32,768 until you start pressing them.. this screws with input interpretation
    //this function simulates a few trigger events to get the input to read correctly on startup.
    
    SDL_Event flush_joy_events[2];
                 
    //Left Trigger
    flush_joy_events[0].jaxis.type = SDL_JOYAXISMOTION;
    flush_joy_events[0].jaxis.timestamp = 0;
    flush_joy_events[0].jaxis.which = instance;
    flush_joy_events[0].jaxis.axis = JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_TRIGGER;
    flush_joy_events[0].jaxis.value = -32767;    
    
    //Right Trigger
    flush_joy_events[1].jaxis.type = SDL_JOYAXISMOTION;
    flush_joy_events[1].jaxis.timestamp = 0;
    flush_joy_events[1].jaxis.which = instance;
    flush_joy_events[1].jaxis.axis = JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_TRIGGER;
    flush_joy_events[1].jaxis.value = -32767;
    
    for (auto & event : flush_joy_events ) {
        if ( SDL_PushEvent( &event ) < 0 )
            ERR("Error when pumping input events : %s\n", SDL_GetError() );
    }
}

void Joystick::CheckDevices( void ) {
    const uint UNSUPPORTED_JOY_CHECK_FREQUENCY = 4000;
    static uint prevUnsupportedJoyPlugCheck = game->GetGameTime() - UNSUPPORTED_JOY_CHECK_FREQUENCY;

    if ( joystick )
        return;

    if ( game->GetRenderTime() < prevUnsupportedJoyPlugCheck )
        return;

    prevUnsupportedJoyPlugCheck = game->GetRenderTime();
    TryOpen();
}

bool Joystick::Plugged( const int joystick_index ) {
    if ( !SDL_IsGameController( joystick_index ) )
        return false;

    SDL_GameController *new_controller = SDL_GameControllerOpen( joystick_index );
    if ( ! new_controller ) {
        ERR( "SDL_GameControllerOpen(): %s\n", SDL_GetError() );
        return false;
    }

    SDL_Joystick *new_joystick = SDL_GameControllerGetJoystick( new_controller );
    if ( ! new_joystick ) {
        ERR( "SDL_GameControllerGetJoystick(): %s\n", SDL_GetError() );
        return false;
    }

    int new_instance = SDL_JoystickInstanceID( new_joystick );

    if ( new_instance == instance )
        return false;

    Close(); // close any previous joystick

    CMSG_LOG("Found hot-plugged joystick (instance %i) at index %i\n", new_instance, joystick_index );

    joystick = new_joystick;
    controller = new_controller;
    SetPadType( SDL_GameControllerNameForIndex( joystick_index ) );
    instance = new_instance;

    return true;
}

bool Joystick::Unplugged( const SDL_JoystickID joystick_instance ) {
    if ( instance < 0 )
        return false;

    if ( instance != joystick_instance )
        return false;

    Close();

    CMSG_LOG("Joystick (instance %i) unplugged, searching for another\n", joystick_instance );
    TryOpen();

    return true;
}

void Joystick::SetPadType( const char* sdlPadTypeName ) {
    if ( ! sdlPadTypeName ) {
        padType = PadType::Generic;
        CMSG_LOG("Joystick type is unknown/generic\n");
        return;
    }

    const std::string name( sdlPadTypeName );
    const std::string pad( String::ToUpper( String::Left( name, 3 ) ) );

    if ( pad == "X36" || pad == "XBO" || pad == "XIN" ) {
        padType = PadType::XBox;
    
    } else if ( pad == "PS3" || pad == "PS4" ) {
        padType = PadType::Playstation;

    } else {
        padType = PadType::Generic;
    }

    CMSG_LOG("Joystick type is %s (%s)\n", name.c_str(), pad.c_str() );
}

uint Joystick::GetDPadIndex( const uint hat_index ) const {
    if ( hat_index > 4 )
        return SDL_CONTROLLER_BUTTON_MAX;
    return SDL_CONTROLLER_BUTTON_MAX + hat_index;
}

bool Joystick::GetButtonState( const uint button_index ) const {  
    return std::find( std::cbegin( joy_held ), std::cend( joy_held ), button_index ) != std::cend( joy_held );
/* doesn't do button remapping
    if ( ! joystick )
        return false;

    if ( button_index >= SDL_CONTROLLER_BUTTON_MAX )
        return GetDPadButtonState( button_index - SDL_CONTROLLER_BUTTON_MAX );

    return SDL_JoystickGetButton( joystick, button_index ) != 0;
*/
}

std::string Joystick::GetGenericButtonName( const std::string& display_name ) const {
    if ( String::Left( display_name, 3 ) != "pad" )
        return display_name;
    
    if ( display_name == "padA" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT );
    if ( display_name == "padB" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT );
    if ( display_name == "padX" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT );
    if ( display_name == "padY" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT );
    if ( display_name == "padCross" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT );
    if ( display_name == "padCircle" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT );
    if ( display_name == "padSquare" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT );
    if ( display_name == "padTriangle" ) return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT );
    if ( display_name == "padUp" ) return std::string("joy") + std::to_string( SDL_CONTROLLER_BUTTON_MAX+0 );
    if ( display_name == "padRight" ) return std::string("joy") + std::to_string( SDL_CONTROLLER_BUTTON_MAX+1 );
    if ( display_name == "padDown" ) return std::string("joy") + std::to_string( SDL_CONTROLLER_BUTTON_MAX+2 );
    if ( display_name == "padLeft" ) return std::string("joy") + std::to_string( SDL_CONTROLLER_BUTTON_MAX+3 );
    if ( display_name == "padStart" ) {
        if ( GetPadType() == PadType::XBox )
            return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA );
            
        return std::string("joy") + std::to_string( JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA );
    }
    
    ERR("Unknown joy button display name: %s\n", display_name.c_str() );
    return display_name;
}

std::string Joystick::GetButtonUIDisplayName( const uint generic_joy_button_index ) const {
    const uint device_index = GetDeviceButtonIndex( generic_joy_button_index );

    switch ( GetPadType() ) {
        case PadType::XBox:
            switch ( device_index ) {
                case SDL_CONTROLLER_BUTTON_A: return "padA";
                case SDL_CONTROLLER_BUTTON_B: return "padB";
                case SDL_CONTROLLER_BUTTON_X: return "padX";
                case SDL_CONTROLLER_BUTTON_Y: return "padY";
                case SDL_CONTROLLER_BUTTON_START: return "padStart";
                case SDL_CONTROLLER_BUTTON_MAX: return "padUp";
                case SDL_CONTROLLER_BUTTON_MAX+1: return "padRight";
                case SDL_CONTROLLER_BUTTON_MAX+2: return "padDown";
                case SDL_CONTROLLER_BUTTON_MAX+3: return "padLeft";
                // todo: case SDL_CONTROLLER_BUTTON_GUIDE, etc
                default: break;
            }
            break;
        case PadType::Playstation:
            switch ( device_index ) {
                case SDL_CONTROLLER_BUTTON_A: return "padCross";
                case SDL_CONTROLLER_BUTTON_B: return "padCircle";
                case SDL_CONTROLLER_BUTTON_X: return "padSquare";
                case SDL_CONTROLLER_BUTTON_Y: return "padTriangle";
                case SDL_CONTROLLER_BUTTON_START: return "padStart";
                case SDL_CONTROLLER_BUTTON_MAX: return "padUp";
                case SDL_CONTROLLER_BUTTON_MAX+1: return "padRight";
                case SDL_CONTROLLER_BUTTON_MAX+2: return "padDown";
                case SDL_CONTROLLER_BUTTON_MAX+3: return "padLeft";
                // todo: case SDL_CONTROLLER_BUTTON_GUIDE, etc
                default: break;
            }
            break;
        case PadType::Generic: FALLTHROUGH;
        default: break;
    }

    std::string ret( "joy" );
    ret += std::to_string( device_index );
    return ret;
}

uint Joystick::GetPlaystationIndexForGenericAxis( const uint generic_axis ) const {
    static bool error_displayed = false;
    switch ( generic_axis ) {
        case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_HORIZONTAL: return JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_HORIZONTAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_VERTICAL: return JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_VERTICAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_HORIZONTAL: return JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_HORIZONTAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_TRIGGER: return JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_TRIGGER;
        case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_TRIGGER: return JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_TRIGGER;
        case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_VERTICAL: return JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_VERTICAL;
        default:
            if ( !error_displayed ) {
                WARN("GetPlaystationIndexForGenericAxis: Unknown joy axis (%u). This warning will not appear again.\n", generic_axis);
                error_displayed = true;
            }
            break;
    }

    DIE("Invalid generic joystick axis: %u\n", generic_axis );
    return JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_HORIZONTAL; // if user wishes to not abort the program
}

uint Joystick::GetXBoxIndexForGenericAxis( const uint generic_axis ) const {
    static bool error_displayed = false;
    switch ( generic_axis ) {
        case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_HORIZONTAL: return JOYSTICK_XBOX_AXIS_INDEX_LEFT_HORIZONTAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_VERTICAL: return JOYSTICK_XBOX_AXIS_INDEX_LEFT_VERTICAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_HORIZONTAL: return JOYSTICK_XBOX_AXIS_INDEX_LEFT_TRIGGER;
        case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_TRIGGER: return JOYSTICK_XBOX_AXIS_INDEX_RIGHT_HORIZONTAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_TRIGGER: return JOYSTICK_XBOX_AXIS_INDEX_RIGHT_VERTICAL;
        case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_VERTICAL: return JOYSTICK_XBOX_AXIS_INDEX_RIGHT_TRIGGER;
        default:
            if ( !error_displayed ) {
                WARN("GetXBoxIndexForGenericAxis: Unknown joy axis (%u). This warning will not appear again.\n", generic_axis);
                error_displayed = true;
            }
            break;
    }

    DIE("Invalid generic joystick axis: %u\n", generic_axis );
    return JOYSTICK_XBOX_AXIS_INDEX_LEFT_HORIZONTAL; // if user wishes to not abort the program
}

std::string Joystick::GetButtonUIDisplayName( const std::string& generic_name ) const {
    if ( String::Left( generic_name, 3 ) != "joy" )
        return generic_name;
    
    const uint index = String::ToUInt( String::Sub( generic_name, 3, generic_name.size() ) );
    return GetButtonUIDisplayName( index );
}

uint Joystick::GetGenericButtonIndex( const uint device_button_index ) const {
    switch ( GetPadType() ) {
        case PadType::Playstation:
            switch ( device_button_index ) {
                case JOYSTICK_PS4_BUTTON_CROSS: return JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT;
                case JOYSTICK_PS4_BUTTON_CIRCLE: return JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT;
                case JOYSTICK_PS4_BUTTON_SQUARE: return JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT;
                case JOYSTICK_PS4_BUTTON_TRIANGLE: return JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT;
                case JOYSTICK_PS4_BUTTON_OPTION: return JOYSTICK_GENERIC_BUTTON_INDEX_SELECT;
                case JOYSTICK_PS4_BUTTON_PS: return JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA;
                case JOYSTICK_PS4_BUTTON_LEFT_STICK_PRESS: return JOYSTICK_GENERIC_BUTTON_INDEX_LEFT_STICK_PRESS;
                case JOYSTICK_PS4_BUTTON_RIGHT_STICK_PRESS: return JOYSTICK_GENERIC_BUTTON_INDEX_RIGHT_STICK_PRESS;
                case JOYSTICK_PS4_BUTTON_SHOULDER_LEFT: return JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_LEFT;
                case JOYSTICK_PS4_BUTTON_SHOULDER_RIGHT: return JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_RIGHT;
                case JOYSTICK_PS4_BUTTON_DPAD_UP: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_UP;
                case JOYSTICK_PS4_BUTTON_DPAD_RIGHT: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_RIGHT;
                case JOYSTICK_PS4_BUTTON_DPAD_DOWN: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_DOWN;
                case JOYSTICK_PS4_BUTTON_DPAD_LEFT: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_LEFT;
                case JOYSTICK_PS4_BUTTON_SHARE: return JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_EXTRA;
                default: break;
            }
        case PadType::XBox:
            switch ( device_button_index ) {
                case JOYSTICK_XBOX_BUTTON_A: return JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT;
                case JOYSTICK_XBOX_BUTTON_B: return JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT;
                case JOYSTICK_XBOX_BUTTON_X: return JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT;
                case JOYSTICK_XBOX_BUTTON_Y: return JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT;
                case JOYSTICK_XBOX_BUTTON_START: return JOYSTICK_GENERIC_BUTTON_INDEX_SELECT;
                case JOYSTICK_XBOX_BUTTON_MENU: return JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA;
                case JOYSTICK_XBOX_BUTTON_LEFT_STICK_PRESS: return JOYSTICK_GENERIC_BUTTON_INDEX_LEFT_STICK_PRESS;
                case JOYSTICK_XBOX_BUTTON_RIGHT_STICK_PRESS: return JOYSTICK_GENERIC_BUTTON_INDEX_RIGHT_STICK_PRESS;
                case JOYSTICK_XBOX_BUTTON_SHOULDER_LEFT: return JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_LEFT;
                case JOYSTICK_XBOX_BUTTON_SHOULDER_RIGHT: return JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_RIGHT;
                case JOYSTICK_XBOX_BUTTON_DPAD_UP: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_UP;
                case JOYSTICK_XBOX_BUTTON_DPAD_RIGHT: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_RIGHT;
                case JOYSTICK_XBOX_BUTTON_DPAD_DOWN: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_DOWN;
                case JOYSTICK_XBOX_BUTTON_DPAD_LEFT: return JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_LEFT;
                default: break;
            }
        
        case PadType::Generic:
            return device_button_index;
        
        default:
            break;
    }

    static bool error_displayed = false;
    if ( !error_displayed ) {
        WARN("GetGenericButtonIndex: Unknown joy button (%u). This warning will not appear again.\n", device_button_index);
        error_displayed = true;
    }

    return 0;
}

uint Joystick::GetDeviceButtonIndex( const uint generic_index ) const {
    switch ( GetPadType() ) {
        case PadType::Playstation:
            switch ( generic_index ) {
                case JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT: return JOYSTICK_PS4_BUTTON_CROSS;
                case JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT: return JOYSTICK_PS4_BUTTON_CIRCLE;
                case JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT: return JOYSTICK_PS4_BUTTON_SQUARE;
                case JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT: return JOYSTICK_PS4_BUTTON_TRIANGLE;
                case JOYSTICK_GENERIC_BUTTON_INDEX_SELECT: return JOYSTICK_PS4_BUTTON_OPTION;
                case JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA: return JOYSTICK_PS4_BUTTON_PS;
                case JOYSTICK_GENERIC_BUTTON_INDEX_LEFT_STICK_PRESS: return JOYSTICK_PS4_BUTTON_LEFT_STICK_PRESS;
                case JOYSTICK_GENERIC_BUTTON_INDEX_RIGHT_STICK_PRESS: return JOYSTICK_PS4_BUTTON_RIGHT_STICK_PRESS;
                case JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_LEFT: return JOYSTICK_PS4_BUTTON_SHOULDER_LEFT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_RIGHT: return JOYSTICK_PS4_BUTTON_SHOULDER_RIGHT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_UP: return JOYSTICK_PS4_BUTTON_DPAD_UP;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_RIGHT: return JOYSTICK_PS4_BUTTON_DPAD_RIGHT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_DOWN: return JOYSTICK_PS4_BUTTON_DPAD_DOWN;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_LEFT: return JOYSTICK_PS4_BUTTON_DPAD_LEFT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_EXTRA: return JOYSTICK_PS4_BUTTON_SHARE;
                default: break;
            }
        case PadType::XBox:
            switch ( generic_index ) {
                case JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT: return JOYSTICK_XBOX_BUTTON_A;
                case JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT: return JOYSTICK_XBOX_BUTTON_B;
                case JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT: return JOYSTICK_XBOX_BUTTON_X;
                case JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT: return JOYSTICK_XBOX_BUTTON_Y;
                case JOYSTICK_GENERIC_BUTTON_INDEX_SELECT: return JOYSTICK_XBOX_BUTTON_START;
                case JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA: return JOYSTICK_XBOX_BUTTON_MENU;
                case JOYSTICK_GENERIC_BUTTON_INDEX_LEFT_STICK_PRESS: return JOYSTICK_XBOX_BUTTON_LEFT_STICK_PRESS;
                case JOYSTICK_GENERIC_BUTTON_INDEX_RIGHT_STICK_PRESS: return JOYSTICK_XBOX_BUTTON_RIGHT_STICK_PRESS;
                case JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_LEFT: return JOYSTICK_XBOX_BUTTON_SHOULDER_LEFT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_RIGHT: return JOYSTICK_XBOX_BUTTON_SHOULDER_RIGHT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_UP: return JOYSTICK_XBOX_BUTTON_DPAD_UP;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_RIGHT: return JOYSTICK_XBOX_BUTTON_DPAD_RIGHT;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_DOWN: return JOYSTICK_XBOX_BUTTON_DPAD_DOWN;
                case JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_LEFT: return JOYSTICK_XBOX_BUTTON_DPAD_LEFT;
                default: break;
            }
        
        case PadType::Generic:
            return generic_index;
        
        default:
            break;
    }
    
    static bool error_displayed = false;
    if ( !error_displayed ) {
        WARN("GetDeviceButtonIndex: Unknown joy button (%u). This warning will not appear again.\n", generic_index);
        error_displayed = true;
    }
    
    return 0;
}

uint Joystick::GetGenericIndexForAxis( const uint device_axis ) const {
    switch ( GetPadType() ) {
        case PadType::Playstation:
            switch ( device_axis ) {
                case JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_HORIZONTAL: return JOYSTICK_GENERIC_AXIS_INDEX_LEFT_HORIZONTAL;
                case JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_VERTICAL: return JOYSTICK_GENERIC_AXIS_INDEX_LEFT_VERTICAL;
                case JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_HORIZONTAL: return JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_HORIZONTAL;
                case JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_TRIGGER: return JOYSTICK_GENERIC_AXIS_INDEX_LEFT_TRIGGER;
                case JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_TRIGGER: return JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_TRIGGER;
                case JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_VERTICAL: return JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_VERTICAL;
                default: break;
            }
            
            break;
        case PadType::XBox:
            switch ( device_axis ) {
                case JOYSTICK_XBOX_AXIS_INDEX_LEFT_HORIZONTAL: return JOYSTICK_GENERIC_AXIS_INDEX_LEFT_HORIZONTAL;
                case JOYSTICK_XBOX_AXIS_INDEX_LEFT_VERTICAL: return JOYSTICK_GENERIC_AXIS_INDEX_LEFT_VERTICAL;
                case JOYSTICK_XBOX_AXIS_INDEX_LEFT_TRIGGER: return JOYSTICK_GENERIC_AXIS_INDEX_LEFT_TRIGGER;
                case JOYSTICK_XBOX_AXIS_INDEX_RIGHT_HORIZONTAL: return JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_HORIZONTAL;
                case JOYSTICK_XBOX_AXIS_INDEX_RIGHT_VERTICAL: return JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_VERTICAL;
                case JOYSTICK_XBOX_AXIS_INDEX_RIGHT_TRIGGER: return JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_TRIGGER;
                default: break;
            }
            break;

        case PadType::Generic:
            return device_axis;
        
        default:
            break;
    }

    static bool error_displayed = false;
    if ( !error_displayed ) {
        WARN("GetGenericIndexForAxis: Unknown joy axis (%u). This warning will not appear again.\n", device_axis);
        error_displayed = true;
    }
    
    return 0;
}


float Joystick::GetAxisState( const uint generic_axis ) {
    static bool error_displayed = false;
    ASSERT( generic_axis < MAX_JOYSTICK_AXIS );
    
    float ret = static_cast< float >( axis[generic_axis] ) / JOYSTICK_MAX_AXIS_VAL;
    
    switch( generic_axis ) {
        case 0: if ( globalVals.GetBool( gval_axis0_invert ) ) ret = -ret; break;
        case 1: if ( globalVals.GetBool( gval_axis1_invert ) ) ret = -ret; break;
        case 2: if ( globalVals.GetBool( gval_axis2_invert ) ) ret = -ret; break;
        case 3: if ( globalVals.GetBool( gval_axis3_invert ) ) ret = -ret; break;
        case 4: if ( globalVals.GetBool( gval_axis4_invert ) ) ret = -ret; break;
        case 5: if ( globalVals.GetBool( gval_axis5_invert ) ) ret = -ret; break;
        default:
            if ( !error_displayed ) {
                WARN("GetAxisState: Unknown joy axis (%u). This warning will not appear again.\n", generic_axis);
                error_displayed = true;
            }
            break;
    }

    // ** triggers go from -1 to 1, but we need them to go from 0 to 1
    switch ( GetPadType() ) {
        case PadType::Playstation:
            FALLTHROUGH;
        case PadType::XBox:
            switch ( generic_axis ) {
                case JOYSTICK_GENERIC_AXIS_INDEX_LEFT_TRIGGER:
                    FALLTHROUGH;
                case JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_TRIGGER:
                    ret += 1;
                    ret /= 2;
                    break;
                default:
                    break;
            }
            break;

        case PadType::Generic:
            break;
            
        default:
            break;
    }

    return ret;
}

PadType Joystick::GetPadType( void ) const {
    return padType;
}

SDL_JoystickID Joystick::GetInstanceID( void ) const {
    return instance;
}

bool Joystick::IsSupported( void ) const {
    return controller != nullptr;
}

uint Joystick::GetNumButtons( void ) const {
    return SDL_CONTROLLER_BUTTON_MAX + 4u;
}

bool Joystick::GetDPadButtonState( const uint hat_index ) const {
    return hat_state & (1<<hat_index);
}
