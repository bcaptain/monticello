// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_INPUT_INPUT
#define SRC_INPUT_INPUT

#include "../base/main.h"
#include "./input_binds.h"
#include "./joystick.h"

extern const Uint32 KEY_REPEAT_FREQUENCY;
extern const Uint32 KEY_REPEAT_DELAY;

extern const Uint32 JOYAXIS_UI_SCROLL_DELAY_BIG;

extern const Uint8 MAX_MOUSE_BUTTONS;

extern const std::string BUTTON_NAMES_MOUSE_LEFT;
extern const std::string BUTTON_NAMES_MOUSE_MIDDLE;
extern const std::string BUTTON_NAMES_MOUSE_RIGHT;

extern std::vector< uint > keys_held; //!< the index (ie, SDL_SCANCODE_LSHIFT) of every key that is currently being held down
extern std::vector< uint > joy_held; //!< same as keys_held, but for joy buttons
extern std::vector< uint > axis_held; //!< same as keys_held, but for joy axis (interpreted as buttons)
extern std::vector< uint > mouse_held; //!< same as keys_held, but for mouse buttons

extern std::vector< std::string > buttons_held; //!< names of all the buttons held (ie, "lshift") in order of when they were pressed

class Window;
class Widget;

typedef uint InputDeviceTypeT_BaseType;
enum class ButtonTypeT : InputDeviceTypeT_BaseType;

typedef uint InputDeviceTypeT_BaseType;
enum class InputDeviceTypeT : InputDeviceTypeT_BaseType {
    Key
    , Joy
    , Axis
    , Mouse
};

#include "../sdl/sdl.h"

typedef bool MenuNavDirectionT_BaseType;
enum class MenuNavDirectionT : MenuNavDirectionT_BaseType {
    Up,
    Down
};

typedef uint InputTypeT_BaseType;
enum class InputTypeT : InputTypeT_BaseType {
    Press
    , Hold
    , Release
    , Axis
};

typedef Uint8 MouseButtonT_BaseType;
enum MouseButtonT : MouseButtonT_BaseType {
    MOUSE_BUTTON_LEFT = SDL_BUTTON_LEFT
    , MOUSE_BUTTON_RIGHT = SDL_BUTTON_RIGHT
    , MOUSE_BUTTON_MIDDLE = SDL_BUTTON_RIGHT
};

extern const std::string BUTTON_NAMES_MOUSE_RIGHT;
extern const std::string BUTTON_NAMES_MOUSE_LEFT;

struct KeyState {
    KeyState( void ) : numkeys(0), keys(nullptr) {
        keys = SDL_GetKeyboardState( &numkeys );
    }
    KeyState( const KeyState& other ) = default;
    KeyState& operator=( const KeyState& other ) = default;
    ~KeyState( void ) = default;
    int numkeys;
    const Uint8* keys;
};

struct MouseState {
    MouseState( void ) : pos(), buttons(0) {}
    Vec2f pos; //!< not a Vec3i simply to avoid the codebload of that tmplate class - nothing else uses it
    Uint8 buttons;
};

// ****
//
// Input
// used to check and process mouse /keyb / etc
//
// ****

class Input {
    typedef bool(*INPUT_ACTION_FUNCTION_PTR)(const uint bind_index, const std::string& action, const InputTypeT inputType);

public:
    DISABLE_INSTANTIATION( Input )

public:
    static void InitJoysticks( void );
    static void DeinitJoysticks( void );

    static void Clear( void ); //!< clears all keys and button "held" states 

    static Vec3f GetWindowMousePos( void );
    static Vec3f GetUIMousePos( void );

    static Vec3f GetWorldMousePos( const Vec3f& plane_A, const Vec3f& plane_B, const Vec3f& plane_C );
    static Vec3f GetWorldMousePos( const Vec3f& plane_A=Vec3f(0,0,0), const Vec3f& plane_normal=Vec3f(0,0,1) );

    static bool GetMouseButtonState( const MouseButtonT_BaseType btn );

    static uint NumButtonsDown( void );
    static bool IsKeyDown( const SDL_Keycode key );
    static bool IsKeyDown( const std::string& key ); //!< takes a key convertible to an id with GetButtonIDByName
    static bool IsKeyDown( const char* key );
private:
    static bool IsKeyDown_helper( const SDL_Scancode scancode );
    static void UnsetBind( const std::string& bindName, std::vector< BindInfo >& binds );
    static void GetBind( const std::string& bindName, std::vector< BindInfo >& binds, std::vector<std::string>& out, const int qty = -1 ); //!< if qty is <0, will retrieve all binds
public:
    static std::vector< std::string > GetUIBind( const std::string& bindName, const int qty = -1 ); //!< if qty is <0, will retrieve all binds. return vector elements are in the form "someKey" or "someKey + someOtherKey""
    static std::vector< std::string > GetGameBind( const std::string& bindName, const int qty = -1 ); //!< if qty is <0, will retrieve all binds. return vector elements are in the form "someKey" or "someKey + someOtherKey""
    static std::vector< std::string > GetEditorBind( const std::string& bindName, const int qty = -1 ); //!< if qty is <0, will retrieve all binds. return vector elements are in the form "someKey" or "someKey + someOtherKey""
    static void UnsetUIBind( const std::string& bindName );
    static void UnsetGameBind( const std::string& bindName );
    static void UnsetEditorBind( const std::string& bindName );
    static bool NavigateMenu( const MenuNavDirectionT direction );

    static Widget* GetFocusWidget( void ); //!< Get a pointer to whatever Widget has (or last had, but don't count on this) keyboard focus (even if it's invisible)
    static Widget* GetVisibleFocusWidget( void ); //!< Get a pointer to the Widget which is both visible on screen and has keyboard focus
    static void SetFocusWidget( Widget* widget );
    static void UnsetFocusWidget( Widget* widget ); //!< set focusWidget to nullptr if it is the same as "widget"
    static void UpdateFocusWidget( void );

    static Uint32 GetLastMouseMoveTime( void );

    static Widget* GetWidgetUnderMouse( void );

    static std::string GetButtonIDByName( const std::string& name ); //!< converts something like "escape" to "27"

    static void GetBindKey( void ); //!< next key pressed wil be sent to MenuUI::KeyPressed
private:
    static bool BindKeyPressed( const std::string& buttonName );
public:
    static void StopWaitingForBindKey( void ); //!< next key pressed wil be sent to MenuUI::KeyPressed
    static bool IsWaitingForBindKey( void );

    static void InitButtonIDsAndNames( void );
private:
    static void InitButton( const char* name, const uint id );

public:
    static void ProcessInput( void ); //!< process all input, happens once per frame
private:
    static void ProcessAllStates( void );
    static void ProcessAllEvents( void );
    
    static bool ProcessEvent( const SDL_Event& pevent );
    
    static void ExecuteJoyAxisStateBinds( void );
    static void ExecuteLeftJoyStateBinds( void );
    static void ExecuteRightJoyStateBinds( void );
    static void UpdateMouseState( void );
    
    static void StartRepeatKey( const SDL_Keysym& key ); //!< SDL_TEXTINPUT handles repeating text keys, but non-text keys (such as holding "enter" or "backspace") are handled by this
    static void StopRepeatKey( void );
    static bool ProcessRepeatKey( void );
    
    // ******* BINDS ***********

    static void ProcessComboRelease( void );
    
    static bool ProcessBindForButton( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, const BindActionTypeT bindActionType );
    
    static bool ExecuteUIBind( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType );
    static bool ExecuteGameBind( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType );
    static bool ExecuteEditorBind( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType );
    
    static bool ExecuteBind( const uint index, const std::vector< BindInfo >& binds, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType );
    
    // ******* BIND ACTIONS ***********

    static bool ExecuteGameAction( const uint bind_index, const std::string& action, const InputTypeT inputType );
    static bool ExecuteEditorAction( const uint bind_index, const std::string& action, const InputTypeT inputType );
    static bool ExecuteUIAction( const uint bind_index, const std::string& action, const InputTypeT inputType );
    
    static bool ExecuteGameActionEvent_Press( const std::string& action );
    static bool ExecuteGameActionEvent_Release( const std::string& action );
    static bool ExecuteGameActionState( const std::string& action );
    static bool ExecuteGameActionJoyAxis_State( const std::string& action, const float axis_value );

    static bool ExecuteUIActionEvent_Press( const std::string& action );
    static bool ExecuteUIActionState( const std::string& action, const float axis_value );
    
    // ******* EVENTS ***********
    
    static bool Event_MouseButtonPress( const SDL_Event& pevent );
    static bool UIEvent_MouseButtonPress( const SDL_MouseButtonEvent& button );
        
    static bool Event_MouseButtonRelease( const SDL_Event& pevent );
    static bool UIEvent_MouseButtonRelease( const SDL_MouseButtonEvent& button );
    
    static bool Event_MouseWheel( const SDL_MouseWheelEvent& wevent );
    static bool UIEvent_MouseWheel( const SDL_MouseWheelEvent& wevent );
        
    static bool Event_KeyPress( const SDL_Event& pevent );
    static bool UIKeyDown( const SDL_Event& pevent );
    
    static bool Event_KeyRelease( const SDL_Event& pevent );
    
    static bool Event_MouseMotion( void ); //!< todo: SDL_ControllerAxisEvent?
    
    static bool Event_TextEntered( const SDL_Event& pevent );
    
    static bool Event_GamepadAdded( const SDL_ControllerDeviceEvent& cdevice );
    static bool Event_GamepadRemoved( const SDL_ControllerDeviceEvent& cdevice );
    static bool Event_GamepadButtonPress( const SDL_ControllerButtonEvent& cbutton );
    static bool Event_GamepadButtonRelease( const SDL_ControllerButtonEvent& cbutton );
    
    static bool Event_JoyButtonPress( const SDL_JoyButtonEvent& jbutton );
    static bool Event_JoyButtonPress( const Uint8 jbutton_index );
    static bool Event_JoyButtonRelease( const SDL_JoyButtonEvent& jbutton );
    static bool Event_JoyButtonRelease( const Uint8 jbutton_index );
    static bool Event_JoyDPad( const SDL_JoyHatEvent& jhat );
    
    static bool Event_JoyAxis( const SDL_JoyAxisEvent& jaxis );
    static bool Event_JoyAxis_ButtonCheck( void ); //!< executes any time the axis is moved
    static bool Event_JoyAxis_ButtonCheck_Helper( const std::string& button_name, const uint axis, const bool isOutsideThreshold );

private:
    static bool SendKeyToWidget( const SDL_Keycode key, Widget* widget );
    static void SetDragWidget( Widget* widget );
    static void UnsetDragWidget( void );
public:
    static void UnsetDragWidget( Widget* widget );

private:
    static HashList<std::string> buttonIDs;
public: // todo: private
    static std::string keyNames[];
public:
    static Joystick joystick;
private:
    static Widget* focusWidget;
    static Widget* dragWidget;
    static Vec3f mouseDragPrev;
    static Vec3f mouseDragFirst;
    static SDL_Keysym repeatKey;
    static Uint32 prevKeyRepeat;
    static Uint32 keyRepeatDelay;
    static Uint32 menuJoyaxisScrollCheckDelay;
    static Uint32 menuJoyaxisScrollCheckPrev;
    static Uint32 last_mouse_move;
    static KeyState keyState;
    static MouseState mouseState;
    static bool getBindKey;
    static bool keyRepeating;
};

#endif  // SRC_INPUT_INPUT
