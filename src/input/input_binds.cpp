// Copyright 2010-2019 Brandon Captain. You may not copy this work.

/*
 * At the time of writing this comment, the input system is intended to work as follows:
 * During a state/event/etc check, All UI binds are evaluated and executed. If any have executed,
 * no other binds are checked. If no UI binds have executed, we execute all editor binds. if no editor
 * binds are executed, then all game binds are executed.
 */

#include "./clib/src/warnings.h"
#include "./input.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../editor/editor.h"
#include "../ui/widget.h"
#include "../console.h"
#include "../entities/actor.h"
#include "../ui/dialog/common.h"
#include "../menuui/binds.h"

std::vector< BindInfo > ui_key_binds;
std::vector< BindInfo > ui_joy_binds;
std::vector< BindInfo > ui_axis_binds;
std::vector< BindInfo > ui_mouse_binds;

std::vector< BindInfo > game_key_binds;
std::vector< BindInfo > game_joy_binds;
std::vector< BindInfo > game_axis_binds;
std::vector< BindInfo > game_mouse_binds;

std::vector< BindInfo > editor_key_binds;
std::vector< BindInfo > editor_joy_binds;
std::vector< BindInfo > editor_axis_binds;
std::vector< BindInfo > editor_mouse_binds;

std::vector< BindInfo > editor_addmode_key_binds;
std::vector< BindInfo > editor_addmode_joy_binds;
std::vector< BindInfo > editor_addmode_axis_binds;
std::vector< BindInfo > editor_addmode_mouse_binds;

std::vector< BindInfo > editor_selectmode_key_binds;
std::vector< BindInfo > editor_selectmode_joy_binds;
std::vector< BindInfo > editor_selectmode_axis_binds;
std::vector< BindInfo > editor_selectmode_mouse_binds;

std::vector< BindInfo > editor_navmeshmode_key_binds;
std::vector< BindInfo > editor_navmeshmode_joy_binds;
std::vector< BindInfo > editor_navmeshmode_axis_binds;
std::vector< BindInfo > editor_navmeshmode_mouse_binds;

bool Input::getBindKey( false );

#ifdef MONTICELLO_EDITOR

bool Input::ExecuteEditorAction( [[maybe_unused]] const uint bindIndex, const std::string& action, const InputTypeT inputType ) {

    if ( ! editor )
        return false;
        
    switch ( inputType ) {
        case InputTypeT::Press: return editor->EvalEventBinds( action );
        case InputTypeT::Release: return editor->EvalButtonReleased( action );
        case InputTypeT::Hold: return editor->EvalStateBinds( action );
        case InputTypeT::Axis: break;
        default: break;
    }
    
    return false;
}
#endif // MONTICELLO_EDITOR

bool Input::ExecuteGameAction( const uint bindIndex, const std::string& action, const InputTypeT inputType ) {
    switch ( inputType ) {
        case InputTypeT::Press: return ExecuteGameActionEvent_Press( action );
        case InputTypeT::Release: return ExecuteGameActionEvent_Release( action );
        case InputTypeT::Hold: return ExecuteGameActionState( action );
        case InputTypeT::Axis: return ExecuteGameActionJoyAxis_State( action, joystick.GetAxisState(bindIndex) );
        default: break;
    }
    
    return false;
}

bool Input::ExecuteUIAction( const uint bindIndex, const std::string& action, const InputTypeT inputType ) {
    switch ( inputType ) {
        case InputTypeT::Press: return ExecuteUIActionEvent_Press( action );
        case InputTypeT::Release: break;//return ExecuteUIActionEvent_Release( action );
        case InputTypeT::Hold: break;//return ExecuteUIActionState( action );
        case InputTypeT::Axis: return ExecuteUIActionState( action, joystick.GetAxisState(bindIndex) );
        default: break;
    }
    
    return false;
}

bool Input::ExecuteUIActionState( const std::string& action, const float axis_value ) {
    const float absVal = fabsf( axis_value );
    const float buttonThreshold = globalVals.GetFloat( gval_axis_buttonThreshold );
    
    if ( absVal < buttonThreshold )
        return false;
    
    Window* window = game->GetTopWindowOrDialog();
    if ( ! window )
        return false;
        
    // todomaybe: invert axis option with hash_axis0_invert, etc?
        
    if ( action == "vertical" ) {
        if ( game->GetRenderTime() - menuJoyaxisScrollCheckPrev <= menuJoyaxisScrollCheckDelay )
            return false;
        
        const bool up = axis_value > 0;
        
        if ( up ) {
            window->PressUp();
        } else {
            window->PressDown();
        }
        
        if ( Maths::Approxf( absVal, 0 ) ) {
            menuJoyaxisScrollCheckDelay = JOYAXIS_UI_SCROLL_DELAY_BIG;
        } else {
            menuJoyaxisScrollCheckDelay = static_cast< uint >( (1.0f/fabsf( axis_value )) * JOYAXIS_UI_SCROLL_DELAY_BIG );
        }
        menuJoyaxisScrollCheckPrev = game->GetRenderTime();
        
        return true;
    }
    
    return false;
}

void Input::ExecuteJoyAxisStateBinds( void ) {
    float axis_deadzone = EP;
    float sensitivity_multiplier = 1.0f;
    
    ExecuteLeftJoyStateBinds();
    ExecuteRightJoyStateBinds();
    
    //Execute the rest
    for ( uint i=4; i<MAX_JOYSTICK_AXIS; ++i ) {
        switch ( i ) {
            case 4:
                sensitivity_multiplier = globalVals.GetFloat(gval_axis4_sens, sensitivity_multiplier);
                axis_deadzone = globalVals.GetFloat( gval_axis4_deadzone, axis_deadzone );
                break;
            case 5:
                sensitivity_multiplier = globalVals.GetFloat(gval_axis5_sens, sensitivity_multiplier);
                axis_deadzone = globalVals.GetFloat( gval_axis5_deadzone, axis_deadzone );
                break;
            default:
                WARN("ExecuteJoyAxisStateBinds: Invalid axis index [index = %i].\n", i);
                break;
        }
    
        const float axis_val = joystick.GetAxisState( i ) * sensitivity_multiplier;
        const bool in_deadzone = fabsf( axis_val ) < axis_deadzone;
                
        if ( in_deadzone )
            continue;
    
        ProcessBindForButton( i, InputDeviceTypeT::Axis, InputTypeT::Axis, BindActionTypeT::ALL );
    }
}

void Input::ExecuteLeftJoyStateBinds( void ) {
    float axis_deadzone = EP;
    float sensitivity_multiplier = 1.0f;
    
    int deadAxis_cnt = 0;
    for ( uint i=0; i<=1; ++i ) {
        switch ( i ) {
            case 0:
                sensitivity_multiplier = globalVals.GetFloat(gval_axis0_sens, sensitivity_multiplier);
                axis_deadzone = globalVals.GetFloat( gval_axis0_deadzone, axis_deadzone );
                break;
            case 1:
                sensitivity_multiplier = globalVals.GetFloat(gval_axis1_sens, sensitivity_multiplier);
                axis_deadzone = globalVals.GetFloat( gval_axis1_deadzone, axis_deadzone );
                break;
            default:
                WARN("ExecuteLeftJoyAxisStateBinds: Invalid axis index, [index = %i].\n", i);
        }
        
        const float axis_val = joystick.GetAxisState( i ) * sensitivity_multiplier;
        const bool in_deadzone = fabsf( axis_val ) < axis_deadzone;
        
        if ( in_deadzone )
            deadAxis_cnt++;
    }
    
    if ( deadAxis_cnt > 1 )
        return;
    
    for ( uint i=0; i<=1; ++i ) 
        ProcessBindForButton( i, InputDeviceTypeT::Axis, InputTypeT::Axis, BindActionTypeT::ALL );
}

void Input::ExecuteRightJoyStateBinds( void ) {
    float axis_deadzone = EP;
    float sensitivity_multiplier = 1.0f;
    
    int deadAxis_cnt = 0;
    for ( uint i=2; i<=3; ++i ) {
        switch ( i ) {
            case 2:
                sensitivity_multiplier = globalVals.GetFloat(gval_axis2_sens, sensitivity_multiplier);
                axis_deadzone = globalVals.GetFloat( gval_axis2_deadzone, axis_deadzone );
                break;
            case 3:
                sensitivity_multiplier = globalVals.GetFloat(gval_axis3_sens, sensitivity_multiplier);
                axis_deadzone = globalVals.GetFloat( gval_axis3_deadzone, axis_deadzone );
                break;
            default:
                WARN("ExecuteJoyAxisStateBinds: Invalid axis index [index = %i].\n", i);
        }
        
        const float axis_val = joystick.GetAxisState( i ) * sensitivity_multiplier;
        const bool in_deadzone = fabsf( axis_val ) < axis_deadzone;
        
        if ( in_deadzone )
            deadAxis_cnt++;
    }
    
    if ( deadAxis_cnt > 1 )
        return;
    
    for ( uint i=2; i<=3; ++i ) 
        ProcessBindForButton( i, InputDeviceTypeT::Axis, InputTypeT::Axis, BindActionTypeT::ALL );    
}

bool Input::ExecuteGameActionState( const std::string& action ) {
    
    auto player = game->GetPlayer();
    if ( ! player )
        return false;
        
    return player->ExecuteInputAction( action, InputTypeT::Hold );
}

bool Input::ExecuteGameActionEvent_Release( const std::string& action ) {
    
    auto player = game->GetPlayer();
    if ( ! player )
        return false;
    
    return player->ExecuteInputAction( action, InputTypeT::Release );
}

bool Input::ExecuteGameActionJoyAxis_State( const std::string& action, const float axis_value ) {
    
    auto player = game->GetPlayer();
    if ( ! player )
        return false;
        
    return player->ExecuteActionJoyAxis_State( action, axis_value );
}

bool Input::ExecuteGameActionEvent_Press( const std::string& action ) {
    
    auto player = game->GetPlayer();
    if ( ! player )
        return false;

    return player->ExecuteInputAction( action, InputTypeT::Press );
}

bool Input::ExecuteUIActionEvent_Press( const std::string& action ) {
    if ( action == "open" ) {
        game->PressEscape();
        return true;
    }
    
    if ( action == "pause" ) {
        game->TogglePause( PauseStateT::PausedByPlayer );
        return true;
    }
    
    if ( action == "debug" ) {
        debugOutput();
        return true;
    }

    if ( action == "fullscreen" ) {
        renderer->ToggleFullScreen();
        return true;
    }
    
    if ( action == "console" && globalVals.GetBool( gval_a_console ) ) {
        console->SetVisible( !console->Object::IsVisible() );
        if ( console->IsVisible() ) {
            SetFocusWidget( console );
        } else {
            UpdateFocusWidget();
        }
        return true;
    }
    
    #ifdef MONTICELLO_EDITOR

        if ( action == "editor" ) {
            Dialog_ToggleEditor();
            return true;
        }

    #endif // MONTICELLO_EDITOR
    
    Window* window = game->GetTopWindowOrDialog();
    if ( ! window )
        return false;

    if ( action == "accept" ) {
        if ( window->ClickButton( ButtonTypeT::Accept ) )
            return true;

        if ( focusWidget && focusWidget->IsVisible() && focusWidget->AreAllParentsVisible() ) {
            focusWidget->PressEnter();
            return true;
        }
    }

    if ( action == "cancel" ) {
        if ( window->ClickButton( ButtonTypeT::Decline ) )
            return true;

        if ( GameUI_Page* page = window->GetPage() ) {
            page->Hide();
            return true;
        }
    }

    if ( action == "up" ) {
        window->PressUp();
        return true;
    }

    if ( action == "down" ) {
        window->PressDown();
        return true;
    }

    if ( action == "left" ) {
        return true;
    }

    if ( action == "right" ) {
        return true;
    }

    if ( action == "opt1" ) {
        return window->ClickButton( ButtonTypeT::Option1 );
    }

    if ( action == "opt2" ) {
        return window->ClickButton( ButtonTypeT::Option2 );
    }

    return false;
}

bool Input::BindKeyPressed( const std::string& buttonName ) {
    if ( !IsWaitingForBindKey() )
        return false;
    
    StopWaitingForBindKey();

    if ( !game )
        return false;
    
    auto page = game->GetUIPage( "binds" );
    ASSERT( page );
    std::shared_ptr< UI_Binds > binds_page = std::static_pointer_cast< UI_Binds >( page );
    binds_page->BindKeyPressed( buttonName );
    
    return true;
}

void Input::GetBind( const std::string& bindName, std::vector< BindInfo >& binds, std::vector<std::string>& out, const int qty ) {
    if ( qty > 0 && out.size() > static_cast< uint >( qty ) )
        return;
    
    for ( auto const & bindInfo : binds ) {
        for ( auto const & bind : bindInfo.all ) {
            if ( bind.action == bindName ) {
                if ( bind.combo.size() < 1 )
                    continue;
                
                out.emplace_back( bind.combo[0] );
                
                for ( uint b=1; b<bind.combo.size(); ++b ) {
                    out[out.size()-1] += "+";
                    out[out.size()-1] += bind.combo[b];
                }
                
                if ( qty > 0 && static_cast< uint >( qty ) == out.size() )
                    return;
            }
        }
    }
    
    return;
}

std::vector< std::string > Input::GetUIBind( const std::string& bindName, const int qty ) {
    std::vector< std::string > ret;
    
    GetBind( bindName, ui_key_binds, ret, qty );
    GetBind( bindName, ui_joy_binds, ret, qty );
    GetBind( bindName, ui_axis_binds, ret, qty );
    GetBind( bindName, ui_mouse_binds, ret, qty );

    return ret;
}

std::vector< std::string > Input::GetGameBind( const std::string& bindName, const int qty ) {
    std::vector< std::string > ret;
    
    GetBind( bindName, game_key_binds, ret, qty );
    GetBind( bindName, game_joy_binds, ret, qty );
    GetBind( bindName, game_axis_binds, ret, qty );
    GetBind( bindName, game_mouse_binds, ret, qty );

    return ret;
}

std::vector< std::string > Input::GetEditorBind( const std::string& bindName, const int qty ) {
    std::vector< std::string > ret;
    
    GetBind( bindName, editor_key_binds, ret, qty );
    GetBind( bindName, editor_joy_binds, ret, qty );
    GetBind( bindName, editor_axis_binds, ret, qty );
    GetBind( bindName, editor_mouse_binds, ret, qty );

    return ret;
}

void Input::UnsetBind( const std::string& bindName, std::vector< BindInfo >& binds ) {
    for ( uint a=0; a<binds.size(); ++a ) {
        for ( uint i=0; i<binds[a].all.size(); ++i ) {
            auto& bind = binds[a].all[i];
            if ( bind.action == bindName ) {
                bind.action = "";
                bind.combo.clear();
                bind.type = BindType::Invalid;
            }
        }
    }
}

void Input::UnsetGameBind( const std::string& bindName ) {
    UnsetBind( bindName, game_key_binds );
    UnsetBind( bindName, game_joy_binds );
    UnsetBind( bindName, game_axis_binds );
    UnsetBind( bindName, game_mouse_binds );
}

void Input::UnsetUIBind( const std::string& bindName ) {
    UnsetBind( bindName, ui_key_binds );
    UnsetBind( bindName, ui_joy_binds );
    UnsetBind( bindName, ui_axis_binds );
    UnsetBind( bindName, ui_mouse_binds );
}

void Input::UnsetEditorBind( const std::string& bindName ) {
    UnsetBind( bindName, editor_key_binds );
    UnsetBind( bindName, editor_joy_binds );
    UnsetBind( bindName, editor_axis_binds );
    UnsetBind( bindName, editor_mouse_binds );
}

bool Input::ExecuteBind( const uint bindIndex, const std::vector< BindInfo >& binds, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType ) {
    ASSERT( actionFunction );
    
    struct BindSignature {
        public:
            BindSignature( void ) = delete;
            BindSignature( const BindSignature& other )
                : combo( other.combo)
                , device_binds( other.device_binds )
            { };
            BindSignature( std::vector< std::string > _combo, const std::vector< BindInfo > * const _device_binds )
                : combo( _combo )
                , device_binds( _device_binds )
            { }
            bool operator==( const BindSignature& other ) const { return combo == other.combo && device_binds == other.device_binds; }
            bool operator!=( const BindSignature& other ) const { return !operator==(other); }
            BindSignature& operator=( [[maybe_unused]] const BindSignature& other ) { ASSERT( false ); return *this; };
            
        private:
            const std::vector< std::string > combo;
            const std::vector< BindInfo > * const device_binds;
    };
    
    static std::vector< BindSignature > combosExecuting;

    if ( bindIndex >= binds.size() )
        return false;
        
    uint actions = 0;
    bool ret = false;
    
    // cycle through each bind for this button (eg for button J: J, Shift+J, Ctrl+J, Ctrl+Shift+J, ...)
    for ( uint b=0; b<binds[bindIndex].all.size(); ++b ) {
        const Bind& bind = binds[bindIndex].all[b];
        
        switch ( bind.combo.size() ) {
            case 0:
                continue;
            case 1:
                if ( ! BitwiseAnd( bindActionType, BindActionTypeT::SINGLE ) )
                    continue; // we are requested not to execute single keys
                break;
            default: // a combo
                if ( ! BitwiseAnd( bindActionType, BindActionTypeT::COMBO ) )
                    continue; // we are requested to not execute key combos
                
                if ( inputType == InputTypeT::Release ) {
                    if ( bind.combo.size() > 1 ) {
                        // on release events, buttons_held may not be be the same size because other modifier keys may have been pressed or
                        // released to change the behavior of the combo after it was first executed, so we have to check it with combosExecuting
                        if ( Contains( combosExecuting, BindSignature( bind.combo, &binds ) ) )
                            break;
                        
                        continue;
                    }
                }
                
                if ( bind.combo.size() != buttons_held.size() )
                    continue;
                
                // ** the order of the combo is irrelevant, ie SHIFT+CTRL+J or CTRL+SHIFT+J
                if ( !std::is_permutation( std::cbegin(bind.combo), std::cend(bind.combo), std::cbegin(buttons_held), std::cend(buttons_held) ) )
                    continue;
                    
                break;
        }
        
        if ( bind.combo.size() > 1 ) {
            const BindSignature bindSig( bind.combo, &binds );
            switch ( inputType ) {
                case InputTypeT::Press:
                    ASSERT( !Contains( combosExecuting, bindSig ) );
                    combosExecuting.push_back( bindSig );
                    break;
                case InputTypeT::Release:
                    ASSERT( Contains( combosExecuting, bindSig ) );
                    RemoveValue( combosExecuting, bindSig );
                    break;
                case InputTypeT::Hold: break;
                case InputTypeT::Axis: break;
                default: break;
            }
        }
        
        ret = (*actionFunction)( bindIndex, bind.action, inputType );
        ++actions;
        
        // if we are looking for combos as well as singles, we will have to continue searching
        if ( bindActionType != BindActionTypeT::ALL )
            break;
            
        // there can be only 1 combo or single, or 2 total if both
        if ( actions == 2 )
            break;
    }
    
    return ret;
}

bool Input::ExecuteUIBind( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType ) {
    switch ( deviceType ) {
        case InputDeviceTypeT::Key: return ExecuteBind( buttonIndex, ui_key_binds, inputType, actionFunction, bindActionType );
        case InputDeviceTypeT::Joy: return ExecuteBind( buttonIndex, ui_joy_binds, inputType, actionFunction, bindActionType );
        case InputDeviceTypeT::Axis: return ExecuteBind( buttonIndex, ui_axis_binds, inputType, actionFunction, bindActionType );
        case InputDeviceTypeT::Mouse: return ExecuteBind( buttonIndex, ui_mouse_binds, inputType, actionFunction, bindActionType );
        default: ERR("ExecuteUIBind(): Invalid deviceType.\n"); break;
    }
    
    return false;
}

bool Input::ExecuteGameBind( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType ) {
    switch ( deviceType ) {
        case InputDeviceTypeT::Key: return ExecuteBind( buttonIndex, game_key_binds, inputType, actionFunction, bindActionType );
        case InputDeviceTypeT::Joy: return ExecuteBind( buttonIndex, game_joy_binds, inputType, actionFunction, bindActionType );
        case InputDeviceTypeT::Axis: return ExecuteBind( buttonIndex, game_axis_binds, inputType, actionFunction, bindActionType );
        case InputDeviceTypeT::Mouse: return ExecuteBind( buttonIndex, game_mouse_binds, inputType, actionFunction, bindActionType );
        default: ERR("ExecuteUIBind(): Invalid deviceType.\n"); break;
    }
    
    return false;
}

bool Input::ExecuteEditorBind( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, INPUT_ACTION_FUNCTION_PTR actionFunction, const BindActionTypeT bindActionType ) {
    
    const std::vector< BindInfo > * edBinds_CurrentMode=nullptr;
    const std::vector< BindInfo > * edBinds_AnyMode=nullptr;
    
    switch ( deviceType ) {
        case InputDeviceTypeT::Key:
            edBinds_CurrentMode = &editor_key_binds;
            switch ( editor->GetMode() ) {
                case EditModeT::SELECT: edBinds_AnyMode = &editor_selectmode_key_binds; break;
                case EditModeT::NAVMESH: edBinds_AnyMode = &editor_navmeshmode_key_binds; break;
                case EditModeT::ADD: edBinds_AnyMode = &editor_addmode_key_binds; break;
                case EditModeT::EFFECTS: break;
                case EditModeT::MAPS: break;
                case EditModeT::WORLDSIZE: break;
                case EditModeT::NONE: break;
                default: break;
            };

            break;
        case InputDeviceTypeT::Joy:
            edBinds_CurrentMode = &editor_joy_binds;
            switch ( editor->GetMode() ) {
                case EditModeT::SELECT: edBinds_AnyMode = &editor_selectmode_joy_binds; break;
                case EditModeT::NAVMESH: edBinds_AnyMode = &editor_navmeshmode_joy_binds; break;
                case EditModeT::ADD: edBinds_AnyMode = &editor_addmode_joy_binds; break;
                case EditModeT::EFFECTS: break;
                case EditModeT::MAPS: break;
                case EditModeT::WORLDSIZE: break;
                case EditModeT::NONE: break;
                default: break;
            };

            break;
        case InputDeviceTypeT::Axis:
            edBinds_CurrentMode = &editor_axis_binds;
            switch ( editor->GetMode() ) {
                case EditModeT::SELECT: edBinds_AnyMode = &editor_selectmode_axis_binds; break;
                case EditModeT::NAVMESH: edBinds_AnyMode = &editor_navmeshmode_axis_binds; break;
                case EditModeT::ADD: edBinds_AnyMode = &editor_addmode_axis_binds; break;
                case EditModeT::EFFECTS: break;
                case EditModeT::MAPS: break;
                case EditModeT::WORLDSIZE: break;
                case EditModeT::NONE: break;
                default: break;
            };

            break;
        case InputDeviceTypeT::Mouse:
            edBinds_CurrentMode = &editor_mouse_binds;
            switch ( editor->GetMode() ) {
                case EditModeT::SELECT: edBinds_AnyMode = &editor_selectmode_mouse_binds; break;
                case EditModeT::NAVMESH: edBinds_AnyMode = &editor_navmeshmode_mouse_binds; break;
                case EditModeT::ADD: edBinds_AnyMode = &editor_addmode_mouse_binds; break;
                case EditModeT::EFFECTS: break;
                case EditModeT::MAPS: break;
                case EditModeT::WORLDSIZE: break;
                case EditModeT::NONE: break;
                default: break;
            };

            break;
        default:
            ERR("ExecuteUIBind(): Invalid deviceType.\n");
            break;
    }
    
    if ( edBinds_AnyMode )
        if ( ExecuteBind( buttonIndex, *edBinds_AnyMode, inputType, actionFunction, bindActionType ) )
            return true;
    
    if ( edBinds_CurrentMode )
        return ExecuteBind( buttonIndex, *edBinds_CurrentMode, inputType, actionFunction, bindActionType );
    
    return false;
}

void Input::ProcessComboRelease( void ) {
    // any time the input changes, we consider that as a "release" event of any combos that are being held prior to adding the event

    /* Relic:
        This is code from a previous iteration of this functinality. Previously, we only cycled the editor's binds, and we only called the Release bind action if 
        the main button (is, Mouse3 in Ctrl+Mouse3) was released. This does feel a little overboard to cycle ALL binds for combos. Perhaps we could store them?
        not concerned for now, since I doubt this should really be a performance issue. Revisit later if needed.

            if ( inputType == InputTypeT::Release ) {
                // only the primary key without any other part of the combo has to be released in order to stop the action
                for ( uint i=0; i<binds[index].all.size(); ++i ) {
                    if ( binds[index].all[i].action.size() > 0 ) {
                        editor->EvalButtonReleased( binds[index].all[i].action );
                    }
                }

                return false;
            }
    */

    for ( uint i=0; i<keys_held.size(); ++i )
        ProcessBindForButton( keys_held[i], InputDeviceTypeT::Key, InputTypeT::Release, BindActionTypeT::COMBO );

    for ( uint i=0; i<joy_held.size(); ++i )
        ProcessBindForButton( joy_held[i], InputDeviceTypeT::Joy, InputTypeT::Release, BindActionTypeT::COMBO );
        
    for ( uint i=0; i<axis_held.size(); ++i )
        ProcessBindForButton( axis_held[i], InputDeviceTypeT::Axis, InputTypeT::Release, BindActionTypeT::COMBO );

    for ( uint i=0; i<mouse_held.size(); ++i )
        ProcessBindForButton( mouse_held[i], InputDeviceTypeT::Mouse, InputTypeT::Release, BindActionTypeT::COMBO );
}

bool Input::ProcessBindForButton( const uint buttonIndex, const InputDeviceTypeT deviceType, const InputTypeT inputType, const BindActionTypeT bindActionType ) {
    // we must execute combos separately from singles
    std::vector< BindActionTypeT > actionTypes;
    actionTypes.reserve(3);
    if ( bindActionType == BindActionTypeT::ALL ) {
        // we must execute combos before singles
        actionTypes.emplace_back( BindActionTypeT::COMBO );
        actionTypes.emplace_back( BindActionTypeT::SINGLE );
    } else {
        actionTypes.emplace_back( bindActionType );
    }
    
    for ( auto const actionType : actionTypes )
        if ( ExecuteUIBind( buttonIndex,deviceType,  inputType, &ExecuteUIAction, actionType ) )
            return true;
    
    #ifdef MONTICELLO_EDITOR
        // if editor is open, game input is ignored
        const bool inDevMode = editor && game->editor_open;
        if ( inDevMode ) {
            for ( auto const actionType : actionTypes )
                if ( ExecuteEditorBind( buttonIndex, deviceType, inputType, &ExecuteEditorAction, actionType ) )
                    return true;
        }
    #endif // MONTICELLO_EDITOR
    
    if ( game->IsPaused() )
        return false;
    
    if ( console && console->IsVisible() )
        return false; // don't process game input if console is open
    
    if ( GetVisibleFocusWidget() )
        return false; // don't do character input / editor binds if we're typing into a textbox or something

    for ( auto const actionType : actionTypes )
        if ( ExecuteGameBind( buttonIndex, deviceType, inputType, &ExecuteGameAction, actionType  ) )
            return true;
            
    return false;
}
