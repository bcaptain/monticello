// Copyright 2010-2019 Brandon Captain. You may not copy this work.

/*
 * At the time of writing this comment, the input system is intended to work as follows:
 * During a state/event/etc check, All UI binds are evaluated and executed. If any have executed,
 * no other binds are checked. If no UI binds have executed, we execute all editor binds. if no editor
 * binds are executed, then all game binds are executed.
 */

#include "./clib/src/warnings.h"
#include "./input.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../math/geometry/3d/plane.h"
#include "../editor/editor.h"
#include "../ui/widget.h"
#include "../console.h"
#include "../entities/actor.h"
#include "../ui/dialog/common.h"
#include "../menuui/binds.h"

const Uint32 KEY_REPEAT_FREQUENCY = 70_ms;
const Uint32 KEY_REPEAT_DELAY = 500_ms;

const Uint32 JOYAXIS_UI_SCROLL_DELAY_BIG = 280_ms;

const Uint8 MAX_MOUSE_BUTTONS = 30;

extern const std::string BUTTON_NAMES_MOUSE_LEFT;
extern const std::string BUTTON_NAMES_MOUSE_MIDDLE;
extern const std::string BUTTON_NAMES_MOUSE_RIGHT;

std::vector< uint > keys_held;
std::vector< uint > joy_held;
std::vector< uint > axis_held;
std::vector< uint > mouse_held;

std::vector< std::string > buttons_held;

HashList<std::string> Input::buttonIDs; //scaleme
std::string Input::keyNames[SDL_NUM_SCANCODES]; //scaleme
Joystick Input::joystick;
Widget* Input::focusWidget( nullptr );
Widget* Input::dragWidget( nullptr );
Vec3f Input::mouseDragPrev;
Vec3f Input::mouseDragFirst;
SDL_Keysym Input::repeatKey;
bool Input::keyRepeating(false);
Uint32 Input::keyRepeatDelay(KEY_REPEAT_DELAY); // this will alternate between KEY_REPEAT_DELAY and INPUT_DELAY_FREQUENCY depending on whether the key is being held fown
Uint32 Input::menuJoyaxisScrollCheckDelay(JOYAXIS_UI_SCROLL_DELAY_BIG);
Uint32 Input::menuJoyaxisScrollCheckPrev(9999); // anything ahead of current game time is in the past
Uint32 Input::prevKeyRepeat(9999); // anything ahead of current game time is in the past
Uint32 Input::last_mouse_move(0);
KeyState Input::keyState;
MouseState Input::mouseState;

void Input::DeinitJoysticks( void ) {
    joystick.Close();
}

void Input::InitJoysticks( void ) {
    joystick.TryOpen();
}

void Input::Clear( void ) {
    keys_held.clear();
    joy_held.clear();
    axis_held.clear();
    mouse_held.clear();
    buttons_held.clear();
}

bool Input::Event_JoyAxis( const SDL_JoyAxisEvent& jaxis ) {
    if ( jaxis.which != joystick.GetInstanceID() ) 
        return false;

    const uint translated_index = joystick.GetGenericIndexForAxis(jaxis.axis);
    ASSERT( translated_index < MAX_JOYSTICK_AXIS );
    joystick.axis[translated_index] = jaxis.value;
    
    return Event_JoyAxis_ButtonCheck();
}

inline bool Input::Event_JoyAxis_ButtonCheck( void ) {
    bool ret = false;
    const float thresh = globalVals.GetFloat( gval_axis_buttonThreshold );
    
    // if an axis has changed, we check all of them at once
    
    for ( uint axis=0; axis<MAX_JOYSTICK_AXIS; ++axis ) {
        std::string button_name("axis");
        button_name += std::to_string( axis );
        
        const bool outsideThresh = joystick.GetAxisState(axis) > thresh || joystick.GetAxisState(axis) < -thresh;
        ret = Event_JoyAxis_ButtonCheck_Helper( button_name, axis, outsideThresh ) || ret;
    }

    return ret;
}

inline bool Input::Event_JoyAxis_ButtonCheck_Helper( const std::string& button_name, const uint axis, const bool isOutsideThreshold ) {
    InputTypeT inputType;

    if ( Contains( buttons_held, button_name ) ) {
        if ( !isOutsideThreshold ) {
            ProcessComboRelease();
            buttons_held.erase( std::remove( std::begin( buttons_held ), std::end( buttons_held ), button_name ), std::end( buttons_held ) );
            axis_held.erase( std::remove( std::begin( axis_held ), std::end( axis_held ), axis ), std::end( axis_held ) );
            inputType = InputTypeT::Release;
        } else {
            return false; // no event happened
        }
    } else {
        if ( isOutsideThreshold ) {
            ProcessComboRelease();
            
            if ( BindKeyPressed( button_name ) )
                return true;
            
            buttons_held.push_back( button_name );
            axis_held.push_back( axis );
            inputType = InputTypeT::Press;
        } else {
            return false; // no event happened
        }
    }
    
    if ( game->IsPaused() )
        return false;
    
    return ProcessBindForButton( axis, InputDeviceTypeT::Axis, inputType, BindActionTypeT::ALL );
}

bool Input::ProcessEvent( const SDL_Event& pevent ) {
    switch ( pevent.type ) {
        case SDL_MOUSEBUTTONDOWN: return Event_MouseButtonPress( pevent );
        case SDL_MOUSEBUTTONUP: return Event_MouseButtonRelease( pevent );
        case SDL_MOUSEWHEEL: return Event_MouseWheel( pevent.wheel );
        case SDL_KEYDOWN: return Event_KeyPress( pevent );
        case SDL_KEYUP: return Event_KeyRelease( pevent );
        case SDL_MOUSEMOTION: return Event_MouseMotion();
        case SDL_TEXTINPUT: return Event_TextEntered( pevent );

        // SDL2-supported controllers
        case SDL_CONTROLLERDEVICEADDED: return Event_GamepadAdded( pevent.cdevice );
        case SDL_CONTROLLERDEVICEREMOVED: return Event_GamepadRemoved( pevent.cdevice );
        case SDL_CONTROLLERBUTTONDOWN: return Event_GamepadButtonPress( pevent.cbutton );
        case SDL_CONTROLLERBUTTONUP: return Event_GamepadButtonRelease( pevent.cbutton );
        // unused case SDL_CONTROLLERAXISMOTION: return ProcessGamepadMotion( pevent.caxis );

        // non-SDL2 supported controllers
        case SDL_JOYBUTTONDOWN: return Event_JoyButtonPress( pevent.jbutton );
        case SDL_JOYBUTTONUP: return Event_JoyButtonRelease( pevent.jbutton );
        case SDL_JOYAXISMOTION: return Event_JoyAxis( pevent.jaxis );
        case SDL_JOYHATMOTION: return Event_JoyDPad( pevent.jhat ); // used also for SDL2-supported controllers

        // unused stuff
        case SDL_FIRSTEVENT: break;
        case SDL_QUIT: break;
        case SDL_APP_TERMINATING: break;
        case SDL_APP_LOWMEMORY: break;
        case SDL_APP_WILLENTERBACKGROUND: break;
        case SDL_APP_DIDENTERBACKGROUND: break;
        case SDL_APP_WILLENTERFOREGROUND: break;
        case SDL_APP_DIDENTERFOREGROUND: break;
        case SDL_WINDOWEVENT: break;
        case SDL_SYSWMEVENT: break;
        case SDL_TEXTEDITING: break;
        case SDL_KEYMAPCHANGED: break;
        case SDL_JOYBALLMOTION: break;
        case SDL_JOYDEVICEADDED: break;
        case SDL_JOYDEVICEREMOVED: break;
        case SDL_CONTROLLERAXISMOTION: break;
        case SDL_CONTROLLERDEVICEREMAPPED: break;
        case SDL_FINGERDOWN: break;
        case SDL_FINGERUP: break;
        case SDL_FINGERMOTION: break;
        case SDL_DOLLARGESTURE: break;
        case SDL_DOLLARRECORD: break;
        case SDL_MULTIGESTURE: break;
        case SDL_CLIPBOARDUPDATE: break;
        case SDL_DROPFILE: break;
        case SDL_AUDIODEVICEADDED: break;
        case SDL_AUDIODEVICEREMOVED: break;
        case SDL_RENDER_TARGETS_RESET: break;
        case SDL_RENDER_DEVICE_RESET: break;
        case SDL_USEREVENT: break;
        case SDL_LASTEVENT: break;
        default: break;
    }
    
    return false;
}

bool Input::Event_GamepadAdded( const SDL_ControllerDeviceEvent& cdevice ) {
    // on add, cdevice.which is an INDEX, not an INSTANCE
    return joystick.Plugged( cdevice.which );
}

bool Input::Event_GamepadRemoved( const SDL_ControllerDeviceEvent& cdevice ) {
    // on removal, cdevice.which is an INSTANCE, not an INDEX
    return joystick.Unplugged( cdevice.which );
}

Widget* Input::GetVisibleFocusWidget( void ) {
    if ( ! focusWidget )
        return nullptr;

    if ( ! focusWidget->IsVisible() )
        return nullptr;

    // it is possible to have a widget with focus that is itself visible, but not actually visible because it's parent is hidden (a text box inside a non-visible window, for example)
    if ( !focusWidget->AreAllParentsVisible() )
        return nullptr;

    return focusWidget;
}

bool Input::Event_TextEntered( const SDL_Event& pevent ) {
    Widget* widget = GetVisibleFocusWidget();
    if ( !widget )
        return false;
    return widget->TakeInput( pevent.text.text );
}

bool Input::Event_MouseMotion( void ) {

    if ( dragWidget ) {
        const Vec3f curMousePos( GetUIMousePos() );
        dragWidget->NudgeOrigin( - (mouseDragPrev - curMousePos) );
        mouseDragPrev = curMousePos;
        return true;
    }

#ifdef MONTICELLO_EDITOR
    last_mouse_move = game->GetRenderTime();
    const bool inDevMode = editor && game->editor_open;
    if ( inDevMode )
        editor->EvalMouseMotion();
#endif // MONTICELLO_EDITOR

    return false;
}

bool Input::Event_MouseButtonRelease( const SDL_Event& pevent ) {

    if ( UIEvent_MouseButtonRelease( pevent.button ) )
        return true;

    std::string buttonName( "mouse" );
    buttonName += std::to_string( static_cast<uint>( pevent.button.button ) );
    
    ProcessComboRelease();
    const bool ret = ProcessBindForButton( pevent.button.button, InputDeviceTypeT::Mouse, InputTypeT::Release, BindActionTypeT::ALL );
    
    buttons_held.erase( std::remove( std::begin( buttons_held ), std::end( buttons_held ), buttonName ), std::end( buttons_held ) );
    mouse_held.erase( std::remove( std::begin( mouse_held ), std::end( mouse_held ), pevent.button.button ), std::end( mouse_held ) );
    
    return ret;
}

bool Input::UIEvent_MouseButtonRelease( const SDL_MouseButtonEvent& button ) {
    switch ( button.button ) {
        case MouseButtonT::MOUSE_BUTTON_LEFT:
            if ( dragWidget == nullptr )
                return false;

            if ( mouseDragFirst == GetUIMousePos() ) {
                dragWidget->Click();
            }

            UnsetDragWidget();
            return true;
        default:
            break;
    }

    return false;
}

bool Input::Event_MouseWheel( const SDL_MouseWheelEvent& wevent ) {
    const uint mouseWheelUpIdentifier = 28;
    const uint mouseWheelDownIdentifier = 29;
    uint identifier;

    std::string buttonName( "mouse" );
    if ( wevent.y > 0 ) {
        identifier = mouseWheelUpIdentifier;
    } else if ( wevent.y < 0 ) {
        identifier = mouseWheelDownIdentifier;
    } else {
        return false;
    }

    buttonName += std::to_string( identifier );
    
    if ( BindKeyPressed( buttonName ) )
        return true;

    if ( UIEvent_MouseWheel( wevent ) )
        return true;

    // mouse wheel can't actually be held, but we need to add it temporarily so the functions know that's what we pressed
    buttons_held.push_back( buttonName );
    mouse_held.push_back( identifier );

        bool ret = ProcessBindForButton( identifier, InputDeviceTypeT::Mouse, InputTypeT::Press, BindActionTypeT::ALL );

    buttons_held.erase( std::remove( std::begin( buttons_held ), std::end( buttons_held ), buttonName ), std::end( buttons_held ) );
    mouse_held.erase( std::remove( std::begin( mouse_held ), std::end( mouse_held ), identifier ), std::end( mouse_held ) );

    return ret;
}

bool Input::UIEvent_MouseWheel( const SDL_MouseWheelEvent& wevent ) {
    Widget* widget = GetWidgetUnderMouse();

    if ( !widget )
        return false;

    if ( !widget->children ) {
        widget = dynamic_cast< Widget* >( widget->GetParent() );
    }

    if ( !widget )
        return false;

    if ( wevent.y > 0 ) {
        widget->MouseWheelUp();
        return true;
    }

    if ( wevent.y < 0 ) {
        widget->MouseWheelDown();
        return true;
    }

    return false;
}

bool Input::Event_MouseButtonPress( const SDL_Event& pevent ) {
    std::string buttonName( "mouse" );
    buttonName += std::to_string( static_cast<uint>( pevent.button.button ) );

    if ( BindKeyPressed( buttonName ) )
        return true;

    if ( UIEvent_MouseButtonPress( pevent.button ) )
        return true;
        
    ProcessComboRelease();
    buttons_held.push_back( buttonName );
    mouse_held.push_back( pevent.button.button );

    return ProcessBindForButton( pevent.button.button, InputDeviceTypeT::Mouse, InputTypeT::Press, BindActionTypeT::ALL );
}

bool Input::Event_JoyButtonPress( const SDL_JoyButtonEvent& jbutton ) {
    if ( jbutton.which != joystick.GetInstanceID() )
        return false;

    // if this is a supported controller, the controller events will handle this
    return !joystick.IsSupported() && Event_JoyButtonPress( jbutton.button );
}

bool Input::Event_GamepadButtonPress( const SDL_ControllerButtonEvent& cbutton ) {
    if ( cbutton.which != joystick.GetInstanceID() )
        return false;

    // Hat events are evaluated by ProcessJoyDPadEvent
    if ( cbutton.button >= SDL_CONTROLLER_BUTTON_DPAD_UP )
        if ( cbutton.button <= SDL_CONTROLLER_BUTTON_DPAD_RIGHT )
            return false;

    return Event_JoyButtonPress( cbutton.button );
}

bool Input::Event_JoyButtonPress( const Uint8 jbutton_index ) {
    const uint index = joystick.GetGenericButtonIndex( jbutton_index );
    
    std::string buttonName( "joy" );
    buttonName += std::to_string( static_cast<uint>( index ) );
 
    if ( BindKeyPressed( buttonName ) )
        return true;
    
    ProcessComboRelease();
    buttons_held.push_back( buttonName );
    joy_held.push_back( index );
 
    return ProcessBindForButton( index, InputDeviceTypeT::Joy, InputTypeT::Press, BindActionTypeT::ALL );
}

bool Input::Event_JoyButtonRelease( const SDL_JoyButtonEvent& jbutton ) {
    // if this is a supported controller, the controller events will handle this
    return !joystick.IsSupported() && Event_JoyButtonRelease( jbutton.button );
}

bool Input::Event_GamepadButtonRelease( const SDL_ControllerButtonEvent& cbutton ) {
    // Hat events are evaluated by ProcessJoyDPadEvent
    if ( cbutton.button >= SDL_CONTROLLER_BUTTON_DPAD_UP )
        if ( cbutton.button <= SDL_CONTROLLER_BUTTON_DPAD_RIGHT )
            return false;

    return Event_JoyButtonRelease( cbutton.button );
}

bool Input::Event_JoyButtonRelease( const Uint8 jbutton_index ) {
    const uint index = joystick.GetGenericButtonIndex( jbutton_index );
    
    std::string buttonName("joy");
    buttonName += std::to_string( static_cast<uint>( index ) );
    
    ProcessComboRelease();
    const bool ret = ProcessBindForButton( index, InputDeviceTypeT::Joy, InputTypeT::Release, BindActionTypeT::ALL );
    
    buttons_held.erase( std::remove( std::begin( buttons_held ), std::end( buttons_held ), buttonName ), std::end( buttons_held ) );
    joy_held.erase( std::remove( std::begin( joy_held ), std::end( joy_held ), index ), std::end( joy_held ) );
    
    return ret;
}

bool IsKeyModifier( const SDL_Keycode keycode );
bool IsKeyModifier( const SDL_Keycode keycode ) {
    switch ( keycode ) {
        case SDLK_LSHIFT: return true;
        case SDLK_RSHIFT: return true;
        case SDLK_LCTRL: return true;
        case SDLK_RCTRL: return true;
        case SDLK_LALT: return true;
        case SDLK_RALT: return true;
        case SDLK_LGUI: return true;
        case SDLK_RGUI: return true;
        case SDLK_MENU: return true;
        case SDLK_APPLICATION: return true;
        default: return false;
    }
}

bool Input::Event_KeyRelease( const SDL_Event& pevent ) {
    if ( !IsKeyModifier( pevent.key.keysym.sym ) )
        ProcessComboRelease();
    
    const auto scancode = SDL_GetScancodeFromKey( pevent.key.keysym.sym );
    const bool ret = ProcessBindForButton( scancode, InputDeviceTypeT::Key, InputTypeT::Release, BindActionTypeT::ALL );
    
    keys_held.erase( std::remove( std::begin( keys_held ), std::end( keys_held ), scancode ), std::end( keys_held ) );
    buttons_held.erase( std::remove( std::begin( buttons_held ), std::end( buttons_held ), keyNames[scancode] ), std::end( buttons_held ) );
    
    StopRepeatKey();
    return ret;
}

bool Input::Event_KeyPress( const SDL_Event& pevent ) {
    if ( pevent.key.repeat )
        return false;

    const auto scancode = SDL_GetScancodeFromKey( pevent.key.keysym.sym );

    if ( scancode >= SDL_NUM_SCANCODES )
        return false; 

    if ( scancode < 0 )
        return false;
    
    if ( scancode < 0 || scancode >= SDL_NUM_SCANCODES ) {
        if ( BindKeyPressed( "escape" ) ) { // "escape" tell the bind system to nevermind
            return true;
        }
    } else {
        if ( BindKeyPressed( keyNames[scancode] ) ) {
            return true;
        }
    }
    
    if ( !IsKeyModifier( pevent.key.keysym.sym ) )
        ProcessComboRelease();

    buttons_held.push_back( keyNames[scancode] );
    keys_held.push_back( scancode );

    if ( UIKeyDown( pevent ) )
        return true;

    return ProcessBindForButton( scancode, InputDeviceTypeT::Key, InputTypeT::Press, BindActionTypeT::ALL );
}

bool Input::Event_JoyDPad( const SDL_JoyHatEvent& jhat ) {
    if ( jhat.which != joystick.GetInstanceID() )
        return false;

    constexpr uint numHatButtons = 4; // the last 4 are used for hat buttons

    const Uint8 prev_hat_state = joystick.hat_state;
    joystick.hat_state = jhat.value;
    const Uint8 hat_changes = prev_hat_state ^ joystick.hat_state;

    bool ret = false;

    // for each hat button that was changed...
    for ( uint i=0; i<numHatButtons; ++i ) {
        uint index = 1u << i;
        if ( ! (hat_changes & index ) )
            continue;

        // ...was it pressed or released?
        if ( joystick.hat_state & index ) {
            if ( Event_JoyButtonPress( joystick.GetDPadIndex( i ) ) ) {
                ret = true;
            }
        } else {
            if ( Event_JoyButtonRelease( joystick.GetDPadIndex( i ) ) ) {
                ret = true;
            }
        }
    }

    return ret;
}

bool Input::UIKeyDown( const SDL_Event& pevent ) {
    const auto scancode = SDL_GetScancodeFromKey( pevent.key.keysym.sym );

    if ( scancode == SDL_SCANCODE_UNKNOWN )
        return false;
        
    #ifdef MONTICELLO_EDITOR
        if ( editor && game->editor_open ) {
            #pragma GCC diagnostic push
            #pragma GCC diagnostic ignored "-Wswitch-enum"
                switch ( scancode ) {
                    case SDL_SCANCODE_ESCAPE:
                        if ( editor->IsContextMenuOpen() ) {
                            editor->CloseContextMenus();
                            return true;
                        }
                        break;
                    default:
                        break;
                }
            #pragma GCC diagnostic pop
        }
    #endif // MONTICELLO_EDITOR

    if ( !focusWidget || !focusWidget->IsVisible() )
        return false;

    if ( !focusWidget->AreAllParentsVisible() )
        return false;

    StartRepeatKey( pevent.key.keysym );

    // ** send key to focused widget
    if ( SendKeyToWidget( pevent.key.keysym.sym, focusWidget ) ) {
        if ( focusWidget != nullptr )
            focusWidget->UpdateVBO();
        return true;
    }

    return false;
}

bool Input::SendKeyToWidget( const SDL_Keycode key, Widget* widget ) {
    ASSERT( widget );

    switch ( key ) {
        case SDLK_RETURN: widget->PressEnter(); return true;
        case SDLK_KP_ENTER: widget->PressEnter(); return true;
        case SDLK_UP: widget->PressUp(); return true;
        case SDLK_DOWN: widget->PressDown(); return true;
        case SDLK_LEFT: widget->PressLeft(); return true;
        case SDLK_RIGHT: widget->PressRight(); return true;
        case SDLK_DELETE: widget->PressDelete(); return true;
        case SDLK_BACKSPACE: widget->PressBackspace();return true;
        case SDLK_PAGEDOWN: widget->PressPageDown(); return true;
        case SDLK_PAGEUP: widget->PressPageUp(); return true;
        case SDLK_TAB: widget->PressTab(); return true;
        case SDLK_KP_9: widget->PressPageUp(); return true; // todo: checl numlock/modifier
        case SDLK_KP_3: widget->PressPageDown(); return true; // todo: checl numlock/modifier
        default: return false;
    }
}

void Input::UpdateMouseState( void ) {
    int x, y;
    mouseState.buttons = SDL_GetMouseState(&x,&y);
    mouseState.pos.x = static_cast< float >( x );
    mouseState.pos.y = static_cast< float >( y );
}

void Input::StopRepeatKey( void ) {
    keyRepeating = false;
}

void Input::StartRepeatKey( const SDL_Keysym& key ) {
    repeatKey = key;
    prevKeyRepeat = game->GetRenderTime();
    keyRepeatDelay = KEY_REPEAT_DELAY;
    keyRepeating = true;
}

bool Input::ProcessRepeatKey( void ) {
    if ( !keyRepeating )
        return false;

    if ( !focusWidget )
        return false;

    // if the widget or any parent of the widget is not visible, it is also not visible
    if ( !focusWidget->IsVisible() || !focusWidget->AreAllParentsVisible() )
        return false;

    if ( game->GetRenderTime() - prevKeyRepeat > keyRepeatDelay ) {
        prevKeyRepeat = game->GetRenderTime();
        keyRepeatDelay = KEY_REPEAT_FREQUENCY;
        Widget* oldFocusWidget = focusWidget;
        if ( SendKeyToWidget( repeatKey.sym, focusWidget ) ) {
            oldFocusWidget->UpdateVBO();
            if ( focusWidget ) {
                focusWidget->UpdateVBO();
            }
        }
    }

    return true;
}

Sprite* ClickCollision( CollisionReport2D& report, std::vector< Sprite* >& sprite_list );
Sprite* ClickCollision( CollisionReport2D& report, std::vector< Sprite* >& sprite_list ) {
    return Collision2D::TestPoint::ToSpriteList( report.point, sprite_list );
}

Widget* Input::GetWidgetUnderMouse( void ) {
    Sprite* sp;
    Vec3f uipos( Input::GetUIMousePos() );
    CollisionReport2D report( uipos.ToVec2() );

    //Note: windows themselves will absorb clicks so that they don't fall through to underlying objects
    
    for ( int i = SPRITE_LAYERS-1; i >= 0; --i ) {
        if ( game->sprites[i].list.size() > 0 ) {
            sp = ClickCollision( report, game->sprites[i].list );
            if ( sp ) {
                return static_cast<Widget*>(sp);
            }
        }
    }

    return nullptr;
}

bool Input::UIEvent_MouseButtonPress( const SDL_MouseButtonEvent& button ) {
    Widget* widget = GetWidgetUnderMouse();

    if ( ! widget )
        return false;

    Input::SetFocusWidget( widget );

    switch ( button.button ) {
        case MouseButtonT::MOUSE_BUTTON_LEFT:
            widget->Raise();
            if ( widget->IsDragable() ) {
                SetDragWidget( widget );
            } else {
                widget->Click();
            }
            break;

        default:
            return false;
    }

#ifdef MONTICELLO_EDITOR
    if ( editor )
        editor->CloseContextMenus();
#endif // MONTICELLO_EDITOR

    return true;
}

void Input::UnsetDragWidget( void ) {
    dragWidget = nullptr;
}

void Input::UnsetDragWidget( Widget* widget ) {
    if ( dragWidget == widget )
        UnsetDragWidget();
}

void Input::SetDragWidget( Widget* widget ) {
    if ( widget ) {
        mouseDragFirst = GetUIMousePos();
        mouseDragPrev = GetUIMousePos();
    }

    dragWidget = widget;
}

void Input::UpdateFocusWidget( void ) {
    Widget* widget = nullptr;
    if ( Window* window = game->GetTopWindow() )
        widget = window->GetFirstTabstop();
    SetFocusWidget( widget );
}

bool Input::NavigateMenu( const MenuNavDirectionT direction ) {
    if ( !focusWidget )
        return false;

    // manually setting focus widget so that the repeatKey doesn't get reset
    focusWidget->SetColor( Color4f(0,0,0,0) ); //un-highlight
    focusWidget = focusWidget->GetTabstop( direction );
    ASSERT( focusWidget );
    focusWidget->HasGainedFocus();

    return true;
}

void Input::ProcessInput( void ) {
    joystick.CheckDevices();
    ProcessAllEvents();
    ProcessAllStates();
    ExecuteJoyAxisStateBinds();
}

void Input::ProcessAllEvents( void ) {
    static SDL_Event pevent; // this is static to avoid cost of frequent construction

    // **** Process Events
    while ( SDL_PollEvent( &pevent ) ) {
        UpdateMouseState();

        switch( pevent.type ) {
            case SDL_QUIT:
                ASSERT( game );
                game->SetState( GameStateT::SHUTDOWN );
                return;

            default:
                ProcessEvent(pevent);
        }
    }
}

void Input::ProcessAllStates( void ) {
    if ( ProcessRepeatKey() )
        return;

    for ( uint i=0; i<keys_held.size(); ++i )
        ProcessBindForButton( keys_held[i], InputDeviceTypeT::Key, InputTypeT::Hold, BindActionTypeT::ALL );

    for ( uint i=0; i<joy_held.size(); ++i )
        ProcessBindForButton( joy_held[i], InputDeviceTypeT::Joy, InputTypeT::Hold, BindActionTypeT::ALL );
        
    for ( uint i=0; i<axis_held.size(); ++i )
        ProcessBindForButton( axis_held[i], InputDeviceTypeT::Axis, InputTypeT::Hold, BindActionTypeT::ALL );

    for ( uint i=0; i<mouse_held.size(); ++i )
        ProcessBindForButton( mouse_held[i], InputDeviceTypeT::Mouse, InputTypeT::Hold, BindActionTypeT::ALL );
}

Vec3f Input::GetWorldMousePos( const Vec3f& plane_A, const Vec3f& plane_B, const Vec3f& plane_C ) {
    return GetWorldMousePos( plane_A, Plane3f::GetNormal( plane_A, plane_B, plane_C ) );
}

Vec3f Input::GetWorldMousePos( const Vec3f& plane_A, const Vec3f& planeNormal ) {
    static CollisionReport3D report( CollisionCalcsT::Intersection ); // this is static to avoid cost of frequent construction

    const Line3f ray( GetPickLine(), LineTypeT::RAY );

    if ( Line3f::ToPlane( report, ray, plane_A, planeNormal ) )
        return report.point;

    return {};
}

void Input::SetFocusWidget( Widget* widget ) {
    // setting focus interrupts repeatKey
    keyRepeating = false;

    if ( focusWidget )
        focusWidget->HasLostFocus();

    // only the console can have focus while open
    if ( console->IsVisible() ) {
        focusWidget = console;
        return;
    }

    focusWidget = widget;

    if ( focusWidget )
        focusWidget->HasGainedFocus();
}

void Input::UnsetFocusWidget( Widget* widget ) {
    if ( focusWidget != widget )
        return;

    focusWidget->HasLostFocus();
    focusWidget = nullptr;
}

Vec3f Input::GetWindowMousePos( void ) {
    return { mouseState.pos.x, mouseState.pos.y, 0.0f };
}

Vec3f Input::GetUIMousePos( void ) {
    const int ui_fixed_width = 800;
    const int ui_fixed_height = 600;

    return {
        mouseState.pos.x / globalVals.GetInt( gval_v_width ) * ui_fixed_width,
        mouseState.pos.y / globalVals.GetInt( gval_v_height ) * ui_fixed_height,
        0.0f
    };
}

std::string Input::GetButtonIDByName( const std::string& name ) {
    if ( name.size() == 0 ) {
        ERR("GetButtonIDByName(): No button specified\n");
        return "";
    }

    if ( String::Left( name, 3 ) == "joy" )
        return name;

    if ( String::Left( name, 4 ) == "axis" )
        return name;

    if ( String::Left( name, 5 ) == "mouse" )
        return name;

    const std::string* ret = buttonIDs.Get( name.c_str() );
    if ( ret )
        return *ret;

    ERR("GetButtonIDByName(): Unknown keyname: %s\n", name.c_str() );
    return "";
}

bool Input::IsKeyDown( const char* key ) {
    if ( !key || key[0] == '\0' )
        return false;
        
    const std::string button_id( GetButtonIDByName( key ) );
    const int button_id_int( String::ToInt( button_id ) );
    const SDL_Scancode scancode = static_cast< SDL_Scancode >( button_id_int );
    return IsKeyDown_helper( scancode );
}

bool Input::IsKeyDown( const SDL_Keycode key ) {
    return IsKeyDown_helper( SDL_GetScancodeFromKey( key ) );
}

bool Input::IsKeyDown_helper( const SDL_Scancode scancode ) {
    if ( scancode < 0 || scancode >= keyState.numkeys) {
        ERR("IsKeyDown(): Invalid key scancode: %i\n", scancode);
        return false;
    }

    ASSERT( keyState.keys );
    return keyState.keys[scancode];
}

void Input::InitButtonIDsAndNames( void ) {
    InitButton("tilde", static_cast<uint>( SDL_SCANCODE_GRAVE ) );
    InitButton("backspace", static_cast<uint>( SDL_SCANCODE_BACKSPACE ) );
    InitButton("tab", static_cast<uint>( SDL_SCANCODE_TAB ) );
    InitButton("clear", static_cast<uint>( SDL_SCANCODE_CLEAR ) );
    InitButton("enter", static_cast<uint>( SDL_SCANCODE_RETURN ) );
    InitButton("pause", static_cast<uint>( SDL_SCANCODE_PAUSE ) );
    InitButton("escape", static_cast<uint>( SDL_SCANCODE_ESCAPE ) );
    InitButton("space", static_cast<uint>( SDL_SCANCODE_SPACE ) );
    InitButton("comma", static_cast<uint>( SDL_SCANCODE_COMMA ) );
    InitButton("minus", static_cast<uint>( SDL_SCANCODE_MINUS ) );
    InitButton("period", static_cast<uint>( SDL_SCANCODE_PERIOD ) );
    InitButton("slash", static_cast<uint>( SDL_SCANCODE_SLASH ) );
    InitButton("0", static_cast<uint>( SDL_SCANCODE_0 ) );
    InitButton("1", static_cast<uint>( SDL_SCANCODE_1 ) );
    InitButton("2", static_cast<uint>( SDL_SCANCODE_2 ) );
    InitButton("3", static_cast<uint>( SDL_SCANCODE_3 ) );
    InitButton("4", static_cast<uint>( SDL_SCANCODE_4 ) );
    InitButton("5", static_cast<uint>( SDL_SCANCODE_5 ) );
    InitButton("6", static_cast<uint>( SDL_SCANCODE_6 ) );
    InitButton("7", static_cast<uint>( SDL_SCANCODE_7 ) );
    InitButton("8", static_cast<uint>( SDL_SCANCODE_8 ) );
    InitButton("9", static_cast<uint>( SDL_SCANCODE_9 ) );
    InitButton("semicolon", static_cast<uint>( SDL_SCANCODE_SEMICOLON ) );
    InitButton("equals", static_cast<uint>( SDL_SCANCODE_EQUALS ) );
    InitButton("leftbracket", static_cast<uint>( SDL_SCANCODE_LEFTBRACKET ) );
    InitButton("backslash", static_cast<uint>( SDL_SCANCODE_BACKSLASH ) );
    InitButton("rightbracket", static_cast<uint>( SDL_SCANCODE_RIGHTBRACKET ) );
    InitButton("backspace", static_cast<uint>( SDL_SCANCODE_BACKSPACE ) );
    InitButton("a", static_cast<uint>( SDL_SCANCODE_A ) );
    InitButton("b", static_cast<uint>( SDL_SCANCODE_B ) );
    InitButton("c", static_cast<uint>( SDL_SCANCODE_C ) );
    InitButton("d", static_cast<uint>( SDL_SCANCODE_D ) );
    InitButton("e", static_cast<uint>( SDL_SCANCODE_E ) );
    InitButton("f", static_cast<uint>( SDL_SCANCODE_F ) );
    InitButton("g", static_cast<uint>( SDL_SCANCODE_G ) );
    InitButton("h", static_cast<uint>( SDL_SCANCODE_H ) );
    InitButton("i", static_cast<uint>( SDL_SCANCODE_I ) );
    InitButton("j", static_cast<uint>( SDL_SCANCODE_J ) );
    InitButton("k", static_cast<uint>( SDL_SCANCODE_K ) );
    InitButton("l", static_cast<uint>( SDL_SCANCODE_L ) );
    InitButton("m", static_cast<uint>( SDL_SCANCODE_M ) );
    InitButton("n", static_cast<uint>( SDL_SCANCODE_N ) );
    InitButton("o", static_cast<uint>( SDL_SCANCODE_O ) );
    InitButton("p", static_cast<uint>( SDL_SCANCODE_P ) );
    InitButton("q", static_cast<uint>( SDL_SCANCODE_Q ) );
    InitButton("r", static_cast<uint>( SDL_SCANCODE_R ) );
    InitButton("s", static_cast<uint>( SDL_SCANCODE_S ) );
    InitButton("t", static_cast<uint>( SDL_SCANCODE_T ) );
    InitButton("u", static_cast<uint>( SDL_SCANCODE_U ) );
    InitButton("v", static_cast<uint>( SDL_SCANCODE_V ) );
    InitButton("w", static_cast<uint>( SDL_SCANCODE_W ) );
    InitButton("x", static_cast<uint>( SDL_SCANCODE_X ) );
    InitButton("y", static_cast<uint>( SDL_SCANCODE_Y ) );
    InitButton("z", static_cast<uint>( SDL_SCANCODE_Z ) );
    InitButton("delete", static_cast<uint>(SDL_SCANCODE_DELETE) );
    InitButton("kp0", static_cast<uint>(SDL_SCANCODE_KP_0) );
    InitButton("kp1", static_cast<uint>(SDL_SCANCODE_KP_1) );
    InitButton("kp2", static_cast<uint>(SDL_SCANCODE_KP_2) );
    InitButton("kp3", static_cast<uint>(SDL_SCANCODE_KP_3) );
    InitButton("kp4", static_cast<uint>(SDL_SCANCODE_KP_4) );
    InitButton("kp5", static_cast<uint>(SDL_SCANCODE_KP_5) );
    InitButton("kp6", static_cast<uint>(SDL_SCANCODE_KP_6) );
    InitButton("kp7", static_cast<uint>(SDL_SCANCODE_KP_7) );
    InitButton("kp8", static_cast<uint>(SDL_SCANCODE_KP_8) );
    InitButton("kp9", static_cast<uint>(SDL_SCANCODE_KP_9) );
    InitButton("kp_period", static_cast<uint>(SDL_SCANCODE_KP_PERIOD) );
    InitButton("kp_divide", static_cast<uint>( SDL_SCANCODE_KP_DIVIDE ) );
    InitButton("kp_multiply", static_cast<uint>(SDL_SCANCODE_KP_MULTIPLY) );
    InitButton("kp_minus", static_cast<uint>(SDL_SCANCODE_KP_MINUS) );
    InitButton("kp_plus", static_cast<uint>(SDL_SCANCODE_KP_PLUS) );
    InitButton("kp_enter", static_cast<uint>(SDL_SCANCODE_KP_ENTER) );
    InitButton("kp_equals", static_cast<uint>(SDL_SCANCODE_KP_EQUALS) );
    InitButton("up", static_cast<uint>( SDL_SCANCODE_UP ) );
    InitButton("down", static_cast<uint>( SDL_SCANCODE_DOWN ) );
    InitButton("right", static_cast<uint>( SDL_SCANCODE_RIGHT ) );
    InitButton("left", static_cast<uint>( SDL_SCANCODE_LEFT ) );
    InitButton("insert", static_cast<uint>( SDL_SCANCODE_INSERT ) );
    InitButton("home", static_cast<uint>( SDL_SCANCODE_HOME ) );
    InitButton("end", static_cast<uint>( SDL_SCANCODE_END ) );
    InitButton("pageup", static_cast<uint>( SDL_SCANCODE_PAGEUP ) );
    InitButton("pagedown", static_cast<uint>( SDL_SCANCODE_PAGEDOWN ) );
    InitButton("f1", static_cast<uint>(SDL_SCANCODE_F1) );
    InitButton("f2", static_cast<uint>(SDL_SCANCODE_F2) );
    InitButton("f3", static_cast<uint>(SDL_SCANCODE_F3) );
    InitButton("f4", static_cast<uint>(SDL_SCANCODE_F4) );
    InitButton("f5", static_cast<uint>(SDL_SCANCODE_F5) );
    InitButton("f6", static_cast<uint>(SDL_SCANCODE_F6) );
    InitButton("f7", static_cast<uint>(SDL_SCANCODE_F7) );
    InitButton("f8", static_cast<uint>(SDL_SCANCODE_F8) );
    InitButton("f9", static_cast<uint>(SDL_SCANCODE_F9) );
    InitButton("f10", static_cast<uint>(SDL_SCANCODE_F10) );
    InitButton("f11", static_cast<uint>(SDL_SCANCODE_F11) );
    InitButton("f12", static_cast<uint>(SDL_SCANCODE_F12) );
    InitButton("f13", static_cast<uint>(SDL_SCANCODE_F13) );
    InitButton("f14", static_cast<uint>(SDL_SCANCODE_F14) );
    InitButton("f15", static_cast<uint>(SDL_SCANCODE_F15) );
    InitButton("numlock", static_cast<uint>( SDL_SCANCODE_NUMLOCKCLEAR ) );
    InitButton("capslock", static_cast<uint>( SDL_SCANCODE_CAPSLOCK ) );
    InitButton("scrollock", static_cast<uint>( SDL_SCANCODE_SCROLLLOCK ) );
    InitButton("rshift", static_cast<uint>( SDL_SCANCODE_RSHIFT ) );
    InitButton("lshift", static_cast<uint>( SDL_SCANCODE_LSHIFT ) );
    InitButton("rctrl", static_cast<uint>( SDL_SCANCODE_RCTRL ) );
    InitButton("lctrl", static_cast<uint>( SDL_SCANCODE_LCTRL ) );
    InitButton("ralt", static_cast<uint>( SDL_SCANCODE_RALT ) );
    InitButton("lalt", static_cast<uint>( SDL_SCANCODE_LALT ) );
    InitButton("rsuper", static_cast<uint>( SDL_SCANCODE_RGUI ) );
    InitButton("lsuper", static_cast<uint>( SDL_SCANCODE_LGUI ) );
    InitButton("mode", static_cast<uint>( SDL_SCANCODE_MODE ) );
    InitButton("help", static_cast<uint>( SDL_SCANCODE_HELP ) );
    InitButton("print", static_cast<uint>( SDL_SCANCODE_PRINTSCREEN ) );
    InitButton("sysrq", static_cast<uint>( SDL_SCANCODE_SYSREQ ) );
    InitButton("pause", static_cast<uint>( SDL_SCANCODE_PAUSE ) );
    InitButton("menu", static_cast<uint>( SDL_SCANCODE_MENU ) );
    InitButton("power", static_cast<uint>( SDL_SCANCODE_POWER ) );
    InitButton("undo", static_cast<uint>( SDL_SCANCODE_UNDO ) );
}

void Input::InitButton( const char* name, const uint id ) {
    ASSERT( id < SDL_NUM_SCANCODES );
    buttonIDs.Set(name, std::to_string( id ).c_str() );
    keyNames[id] = name;
}

uint Input::NumButtonsDown( void ) {
    return buttons_held.size();
}

Uint32 Input::GetLastMouseMoveTime( void ) {
    return last_mouse_move;
}

bool Input::IsKeyDown( const std::string& key ) {
    return IsKeyDown( key.c_str() );
}

Widget* Input::GetFocusWidget( void ) {
    return focusWidget;
}

bool Input::GetMouseButtonState( const MouseButtonT_BaseType btn ) {
    return mouseState.buttons & SDL_BUTTON(btn);
}

bool Input::IsWaitingForBindKey( void ) {
    return getBindKey;
}

void Input::GetBindKey( void ) {
    getBindKey = true;
}

void Input::StopWaitingForBindKey( void ) {
    getBindKey = false;
}
