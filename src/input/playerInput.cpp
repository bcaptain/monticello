// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./playerInput.h"
#include "../rendering/renderer.h"
#include "../entities/actor.h"

void PlayerInput::ExecuteMoveVector( Actor& player, Vec3f worldVec ) {
    if ( globalVals.GetBool( gval_cam_moveRelative, true ) ) {
        worldVec = GetCamRelativeVector( worldVec );
    }
    
    player.SetMoveVec( worldVec );
    
    if ( globalVals.GetBool( gval_g_faceMoveDir, true ) && player.CanTurn() )
        player.TurnToFaceDir( worldVec );
}

Vec3f PlayerInput::GetCamRelativeVector( const Vec3f& worldVec ) {
    if ( Maths::Approxf( worldVec, Vec3f(0,0,0) ) )
        return worldVec;
    
    return worldVec * Quat::FromAngles_ZYX( 0, 0, -renderer->camera.GetRot() );
}
