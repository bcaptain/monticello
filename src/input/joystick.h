// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_INPUT_JOYSTICK_H_
#define SRC_INPUT_JOYSTICK_H_

#include "./clib/src/warnings.h"
#include "../sdl/sdl.h"

constexpr uint JOYSTICK_GENERIC_AXIS_INDEX_LEFT_HORIZONTAL = 0;
constexpr uint JOYSTICK_GENERIC_AXIS_INDEX_LEFT_VERTICAL = 1;
constexpr uint JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_HORIZONTAL = 2;
constexpr uint JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_VERTICAL = 3;
constexpr uint JOYSTICK_GENERIC_AXIS_INDEX_LEFT_TRIGGER = 4;
constexpr uint JOYSTICK_GENERIC_AXIS_INDEX_RIGHT_TRIGGER = 5;

constexpr uint JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_HORIZONTAL = 0;
constexpr uint JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_VERTICAL = 1;
constexpr uint JOYSTICK_PLAYSTATION_AXIS_INDEX_LEFT_TRIGGER = 2;
constexpr uint JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_HORIZONTAL = 3;
constexpr uint JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_VERTICAL = 4;
constexpr uint JOYSTICK_PLAYSTATION_AXIS_INDEX_RIGHT_TRIGGER = 5;

constexpr uint JOYSTICK_XBOX_AXIS_INDEX_LEFT_HORIZONTAL = 0; //todobrandon: make sure these are right
constexpr uint JOYSTICK_XBOX_AXIS_INDEX_LEFT_VERTICAL = 1;
constexpr uint JOYSTICK_XBOX_AXIS_INDEX_LEFT_TRIGGER = 2;
constexpr uint JOYSTICK_XBOX_AXIS_INDEX_RIGHT_HORIZONTAL = 3;
constexpr uint JOYSTICK_XBOX_AXIS_INDEX_RIGHT_VERTICAL = 4;
constexpr uint JOYSTICK_XBOX_AXIS_INDEX_RIGHT_TRIGGER = 5;

constexpr uint JOYSTICK_XBOX_BUTTON_A = 0;
constexpr uint JOYSTICK_XBOX_BUTTON_B = 1;
constexpr uint JOYSTICK_XBOX_BUTTON_X = 2;
constexpr uint JOYSTICK_XBOX_BUTTON_Y = 3;
constexpr uint JOYSTICK_XBOX_BUTTON_START = 4;
constexpr uint JOYSTICK_XBOX_BUTTON_CENTER = 5;
constexpr uint JOYSTICK_XBOX_BUTTON_MENU = 6;
constexpr uint JOYSTICK_XBOX_BUTTON_LEFT_STICK_PRESS = 7;
constexpr uint JOYSTICK_XBOX_BUTTON_RIGHT_STICK_PRESS = 8;
constexpr uint JOYSTICK_XBOX_BUTTON_SHOULDER_LEFT = 9;
constexpr uint JOYSTICK_XBOX_BUTTON_SHOULDER_RIGHT = 10;
constexpr uint JOYSTICK_XBOX_BUTTON_DPAD_UP = 15;
constexpr uint JOYSTICK_XBOX_BUTTON_DPAD_RIGHT = 16;
constexpr uint JOYSTICK_XBOX_BUTTON_DPAD_DOWN = 17;
constexpr uint JOYSTICK_XBOX_BUTTON_DPAD_LEFT = 18;

constexpr uint JOYSTICK_PS4_BUTTON_CROSS = 0;
constexpr uint JOYSTICK_PS4_BUTTON_CIRCLE = 1;
constexpr uint JOYSTICK_PS4_BUTTON_SQUARE = 2;
constexpr uint JOYSTICK_PS4_BUTTON_TRIANGLE = 3;
constexpr uint JOYSTICK_PS4_BUTTON_PS = 4;
constexpr uint JOYSTICK_PS4_BUTTON_SHARE = 5;
constexpr uint JOYSTICK_PS4_BUTTON_OPTION = 6;
constexpr uint JOYSTICK_PS4_BUTTON_LEFT_STICK_PRESS = 7;
constexpr uint JOYSTICK_PS4_BUTTON_RIGHT_STICK_PRESS = 8;
constexpr uint JOYSTICK_PS4_BUTTON_SHOULDER_LEFT = 9;
constexpr uint JOYSTICK_PS4_BUTTON_SHOULDER_RIGHT = 10;
constexpr uint JOYSTICK_PS4_BUTTON_DPAD_UP = 15;
constexpr uint JOYSTICK_PS4_BUTTON_DPAD_RIGHT = 16;
constexpr uint JOYSTICK_PS4_BUTTON_DPAD_DOWN = 17;
constexpr uint JOYSTICK_PS4_BUTTON_DPAD_LEFT = 18;

constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_TOP_LEFT = 0;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_TOP_RIGHT = 1;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_LEFT = 2;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_RIGHT = 3;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_TOP_EXTRA = 4;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_BOTTOM_EXTRA = 5;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_LEFT = 6;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_SHOULDER_RIGHT = 7;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_LEFT_STICK_PRESS = 8;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_RIGHT_STICK_PRESS = 9;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_SELECT = 10;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_ANALOG_TOGGLE = 11;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_UP = 15;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_RIGHT = 16;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_DOWN = 17;
constexpr uint JOYSTICK_GENERIC_BUTTON_INDEX_DPAD_LEFT = 18;

constexpr uint MAX_JOYSTICK_AXIS = 6;

constexpr int JOYSTICK_MAX_AXIS_VAL = 32768;

typedef Uint8 PadTypeT_BaseType;
enum class PadType : PadTypeT_BaseType{
    Generic,
    Playstation,
    XBox
};

extern std::vector< uint > joy_held;
extern std::vector< uint > axis_held;

typedef uint PhysicalJoyStickT_BaseT;
enum PhysicalJoyStickT : PhysicalJoyStickT_BaseT {
    LEFT=0,
    RIGHT
};

class Joystick {
    friend class Input; //todoattorney?

public:
    Joystick( void );
    ~Joystick( void );
    Joystick( const Joystick& other ) = delete; // no copying allowed
    Joystick( Joystick && other_rref );

public:
    Joystick& operator=( const Joystick& other ) = delete; // no copying allowed
    Joystick& operator=( Joystick && other_rref );

private: // to be accessed by Input class
    void Close( void );
    bool TryOpen( void );
    void PumpStartupEvents( void );

    SDL_JoystickID GetInstanceID( void ) const;
    bool Plugged( const int joystick_index );
    bool Unplugged( const SDL_JoystickID joystick_instance );
    bool IsSupported( void ) const; //!< whether the joystick is supported by SDL2's button mapping API
    void SetPadType( const char* sdlPadTypeName );

public:
    void CheckDevices( void );
    uint GetNumButtons( void ) const;
    uint GetDPadIndex( const uint hat_index ) const;
    bool GetDPadButtonState( const uint hat_index ) const;
    bool GetButtonState( const uint button_index ) const;
    float GetAxisState( const Uint generic_axis ); //!< returns a percentage from 0.0f to 1.0f, how far the stick is being pushed

    PadType GetPadType( void ) const;
    uint GetDeviceButtonIndex( const uint generic_button_index ) const;
    uint GetGenericButtonIndex( const uint device_button_index ) const;
    uint GetGenericIndexForAxis( const uint device_axis ) const; //!< returns the generic_index for Joystick::axis[generic_index] based on what type of controller we're using
    uint GetPlaystationIndexForGenericAxis( const uint generic_axis ) const; //!< returns the true playstation gamepad index index based on the generic_index
    uint GetXBoxIndexForGenericAxis( const uint generic_axis ) const; //!< returns the true xbox gamepad index index based on the generic_index    
     
    std::string GetButtonUIDisplayName( const uint generic_button_index ) const;

    std::string GetButtonUIDisplayName( const std::string& generic_name ) const;
    std::string GetGenericButtonName( const std::string& display_name ) const;

private:
    int axis[MAX_JOYSTICK_AXIS]; //!< uses generic indexes, not device indexes
    SDL_Joystick *joystick;
    SDL_GameController *controller;
    SDL_JoystickID instance;
    PadType padType;
    Uint8 hat_state;
};

#endif  // SRC_INPUT_JOYSTICK_H_
