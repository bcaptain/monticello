// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_INPUT_BINDS
#define SRC_INPUT_BINDS

#include "../base/main.h"

typedef Uint8 BindType_Type;
enum class BindType : BindType_Type {
    Invalid,
    Game,
    Editor,
    Editor_Navmesh,
    Editor_Select,
    Editor_Add,
    UI
};

struct Bind {
    Bind( void ) : action(), combo(), type() {}
    //todo: ctor
    std::string action;
    std::vector<std::string> combo;
    BindType type;
};

struct BindInfo {
    BindInfo( void ) : all() { }
    std::vector< Bind > all;
};

extern std::vector< BindInfo > ui_key_binds;
extern std::vector< BindInfo > ui_joy_binds; 
extern std::vector< BindInfo > ui_axis_binds; 
extern std::vector< BindInfo > ui_mouse_binds; 

extern std::vector< BindInfo > game_key_binds;
extern std::vector< BindInfo > game_joy_binds; 
extern std::vector< BindInfo > game_axis_binds; 
extern std::vector< BindInfo > game_mouse_binds;

extern std::vector< BindInfo > editor_key_binds;
extern std::vector< BindInfo > editor_joy_binds;
extern std::vector< BindInfo > editor_axis_binds;
extern std::vector< BindInfo > editor_mouse_binds;

extern std::vector< BindInfo > editor_addmode_key_binds;
extern std::vector< BindInfo > editor_addmode_joy_binds;
extern std::vector< BindInfo > editor_addmode_axis_binds;
extern std::vector< BindInfo > editor_addmode_mouse_binds;

extern std::vector< BindInfo > editor_selectmode_key_binds;
extern std::vector< BindInfo > editor_selectmode_joy_binds;
extern std::vector< BindInfo > editor_selectmode_axis_binds;
extern std::vector< BindInfo > editor_selectmode_mouse_binds;

extern std::vector< BindInfo > editor_navmeshmode_key_binds;
extern std::vector< BindInfo > editor_navmeshmode_joy_binds;
extern std::vector< BindInfo > editor_navmeshmode_axis_binds;
extern std::vector< BindInfo > editor_navmeshmode_mouse_binds;

typedef uint BindActionTypeT_BaseType;
enum class BindActionTypeT : BindActionTypeT_BaseType {
    NONE = 0
    , SINGLE = 1
    , COMBO = 1<<1
    , ALL = SINGLE | COMBO
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( BindActionTypeT, BindActionTypeT_BaseType )

#endif  // SRC_INPUT_BINDS
