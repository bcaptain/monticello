// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_INPUT_PLAYERINPUT_H_
#define SRC_INPUT_PLAYERINPUT_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"

/*
 * PlayerInput
 * 
 * Called by Input class when it has received player movement information.
 * It will decide how to move the player or camera
 * 
 */

class Actor;

class PlayerInput {
public:
    static void ExecuteMoveVector( Actor& player, Vec3f worldVec );
    
private:
    NODISCARD static Vec3f GetCamRelativeVector( const Vec3f& worldVec ); //!< rotate the vector to camera-relative instead of world-relative
};

#endif  // SRC_INPUT_PLAYERINPUT_H_
