// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_NAV_FLOWFIELD_H_
#define SRC_NAV_FLOWFIELD_H_

#include "../base/main.h"
#include "./navMesh.h"

#include <queue>

class NavPoly;
class NavPath;
class EntBounds;

typedef uint GridVertPosIndexT_BaseType;
enum GridVertPosIndexT : GridVertPosIndexT_BaseType {
    GRID_VERT_BACK_LEFT = 0
    , GRID_VERT_FRONT_LEFT = 1
    , GRID_VERT_FRONT_RIGHT = 2
    , GRID_VERT_BACK_RIGHT = 3
};

class FlowField {
public:
    FlowField( void );
    FlowField( const FlowField& other ) = default;

public:
    FlowField& operator=( const FlowField& other ) = default;

public:
    bool Build( const Vec3f& dest_origin );

// **** cost field stuff
public:
    void ComputeCostField( const std::shared_ptr< const NavPoly >& curPoly, const Circle2f& destCircle );
private:
    Vec2f ComputeDimensions( void ); //!< make sure the box can encompass an even number of cells based on cellSize. Returns dimensions of grid
    void InsertNavPolies( const std::vector< std::shared_ptr< const NavPoly > >& polies );
    void InsertObstacles( const std::vector< std::shared_ptr< const NavPoly > >& polies );
    void DrawCostFieldDebug( void ) const;
    std::pair<int,int> TranslateToGridPos( const Vec3f& origin ) const; //!< this will return -1,-1 if the position is not within the grid
public:
    bool IsReady( void ) const;
    void SetUnready( void );
    void DrawDebug( void ) const;
    bool IsPosWithinBounds( const std::pair<int,int>& pos ) const;
    bool IsPosWithinBounds( const int x, const int y ) const;

public:
    void GetIntegrationField( const Circle2f& destCircle, const bool oneDestCellOnly = true );
    Vec3f GetMoveDirection( const Entity& movingEnt, const Vec3f& curOrigin, const std::vector<uint>& obstacle_ent_ids_to_ignore ) const; //!< return the direciton we should be moving based on the grid
private:
    void DoWaveFront( std::queue< std::pair< int, int > > destCells );
        void DoWaveCell( const int val, const int x, const int y, std::queue< std::pair< int, int > >& destCells );
    int GetCellCost( const int x, const int y ) const;
    Vec3f DoCellObstaclePush( const Vec3f& dir, const Entity& movingEnt, const std::vector<uint>& obstacle_ent_ids_to_ignore ) const;
public:
    void DrawDebug_HeatMap( void ) const;

private:
    Vec2f gridVert[4]; //!< indices reflect GridVertPosIndexT
    std::vector< std::vector<int> > grid; //!< a 2d grid of vectors of entityIDs_ints. unique entityIDs that parallels the objects inseretd into grid.
    std::vector< std::weak_ptr< Entity > > obstacles;
    
    int rows;
    int cols;
    float cellSize;
};

#endif // SRC_NAV_FLOWFIELD_H_
