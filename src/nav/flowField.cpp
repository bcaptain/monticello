// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./flowField.h"
#include "../entities/actor.h"
#include "../rendering/renderer.h"
#include "../rendering/font.h"
#include "../rendering/fontManager.h"
#include "../math/collision/collision.h"

const int FLOW_CELL_OUTSIDE_NAVMESH = MAX_INT;
const int FLOW_CELL_TO_GRADIENT = MAX_INT-1; // cells we'll want to gradient later
const int FLOW_CELL_DESTINATION = 1;

FlowField::FlowField( void )
    : gridVert()
    , grid()
    , obstacles()
    , rows(0)
    , cols(0)
    , cellSize(0)
{ }

bool FlowField::Build( const Vec3f& dest_origin ) {
    // grid ranges from 1 to MAX_INT.
    // 1 == a destination cell ( can be more than one of them )
    // max_int == impassible

    SetUnready();
    
    // ** calculate grid information

    cellSize = 0.75f;

    const Vec2f gridMin( dest_origin.x - AI_DEFAULT_CHASE_RADIUS_LIMIT, dest_origin.y - AI_DEFAULT_CHASE_RADIUS_LIMIT );
    const Vec2f gridMax( dest_origin.x + AI_DEFAULT_CHASE_RADIUS_LIMIT, dest_origin.y + AI_DEFAULT_CHASE_RADIUS_LIMIT );

    gridVert[GRID_VERT_BACK_LEFT].Set( gridMin.x, gridMin.y );
    gridVert[GRID_VERT_FRONT_LEFT].Set( gridMin.x, gridMax.y );
    gridVert[GRID_VERT_FRONT_RIGHT].Set( gridMax.x, gridMax.y );
    gridVert[GRID_VERT_BACK_RIGHT].Set( gridMax.x, gridMin.y );

    const Vec2f gridDimensions = ComputeDimensions();

    // ** create the vector grid
    
    // the grid will be initialized in InsertNavPolies()
    
    rows = static_cast<int>( Round( gridDimensions.x / cellSize ) );
    cols = static_cast<int>( Round( gridDimensions.y / cellSize ) );
    
    grid.resize( static_cast<uint>(rows) );
    obstacles.clear();
    
    for ( int row=0; row<rows; ++row ) {
        grid[static_cast<uint>(row)].resize( static_cast<uint>(cols) );
    }

    if ( grid.size() < 1 ) {
        SetUnready();
        return false;
    }
    
    return true;
}

void FlowField::ComputeCostField( const std::shared_ptr< const NavPoly >& curPoly, const Circle2f& destCircle ) {
    if ( ! curPoly )
        return;

    std::vector< std::shared_ptr< const NavPoly > > polies;
    polies.reserve(6);
    const AASquare2v fieldBounds( gridVert[0], gridVert[2] );
    curPoly->GetNeighboringPoliesWithinAASquare( curPoly, polies, fieldBounds );

    InsertNavPolies( polies );
    GetIntegrationField( destCircle );
    InsertObstacles( polies );
}

int GetFlowCellWeight_Helper( const NavPoly& poly, const std::size_t edge_index_a, const std::size_t edge_index_b, const AASquare4v& square );
int GetFlowCellWeight_Helper( const NavPoly& poly, const std::size_t edge_index_a, const std::size_t edge_index_b, const AASquare4v& square ) {
    Line2f edge( poly.GetVert(edge_index_a), poly.GetVert(edge_index_b) );

    bool shared_edge = poly.HasPortalOnEdge(edge_index_a, edge_index_b);

    if ( shared_edge ) {
        // this is a shared edge, so we need to keep any squares that the edge line collides with.
        if ( Collision2D::TestAASquare::BeforeLine( square.bl, square.tl, square.tr, square.br, edge ) )
            return true;
    } else {
        // the other side of this edge is void, so we mark the squares it touches as no-no
        if ( !Collision2D::TestAASquare::BehindLine( square.bl, square.tl, square.tr, square.br, edge ) )
            return true;
    }
    
    return false;
}

int GetFlowCellWeight( const std::vector< std::shared_ptr< const NavPoly > >& polies, const AASquare4v& square );
int GetFlowCellWeight( const std::vector< std::shared_ptr< const NavPoly > >& polies, const AASquare4v& square ) {
    for ( const auto & poly : polies ) {
        if ( ! poly )
            continue;

        if ( GetFlowCellWeight_Helper(*poly, 2, 0, square ) )
            continue;

        if ( GetFlowCellWeight_Helper(*poly, 0, 1, square ) )
            continue;

        if ( GetFlowCellWeight_Helper(*poly, 1, 2, square ) )
            continue;

        return FLOW_CELL_TO_GRADIENT;
    }

    return FLOW_CELL_OUTSIDE_NAVMESH;
}

void FlowField::GetIntegrationField( const Circle2f& destCircle, const bool oneDestCellOnly ) {
    /* integration field starts initialized as all FLOW_CELL_TO_GRADIENT.
       then it gets FLOW_CELL_DESTINATION inserted into it
    */
    
    std::queue< std::pair< int, int > > destCells;
    
    // ** insert destination
    
    // just the origin as destination?
    if ( oneDestCellOnly ) {
        const std::pair<int,int> pos( TranslateToGridPos( Vec3f( destCircle.origin.x, destCircle.origin.y, 0 ) ) );
        
        if ( !IsPosWithinBounds( pos ) )
            return;

        // if it's unwalkable, it cant be a destination
        if ( grid[static_cast<uint>(pos.first)][static_cast<uint>(pos.second)] == FLOW_CELL_OUTSIDE_NAVMESH )
            return;

        grid[static_cast<uint>(pos.first)][static_cast<uint>(pos.second)] = FLOW_CELL_DESTINATION;
        destCells.emplace( pos );
    } else {
        // check against their radius + our radius, in essence reducing us to a single point and increasing the obstacles by our radius. same effect, simpler calculation
        const Vec3f origin3f( destCircle.origin.x, destCircle.origin.y, 0 );
        const std::pair<int,int> pos_min( TranslateToGridPos( origin3f - destCircle.radius ) );
        const std::pair<int,int> pos_max( TranslateToGridPos( origin3f + destCircle.radius ) );

        if ( !IsPosWithinBounds( pos_min ) )
            return;
        if ( !IsPosWithinBounds( pos_max ) )
            return;

        for ( uint x=static_cast<uint>(pos_min.first); x<=static_cast<uint>(pos_max.first); ++x ) {
            for ( uint y=static_cast<uint>(pos_min.second); y<=static_cast<uint>(pos_max.second); ++y ) {
                
                // if it's unwalkable, it cant be a destination
                if ( grid[x][y] == FLOW_CELL_OUTSIDE_NAVMESH ) {
                    continue;
                }
                    
                const Vec2f aabb_min( gridVert[0].x + (cellSize*x), gridVert[0].y + (cellSize*y) );
                const Vec2f aabb_max( aabb_min.x + cellSize, aabb_min.y + cellSize );

                if ( !Collision2D::TestCircle::ToAASquare( Vec2f(destCircle.origin.x, destCircle.origin.y), destCircle.radius, aabb_min, aabb_max ) )
                    continue;

                grid[x][y] = FLOW_CELL_DESTINATION;
                destCells.emplace( x,y );
            }
        }
    }

    // ** destionation has been inseretd, now calculate integration field
    
    if ( destCells.size() > 0 )
        DoWaveFront( destCells );

    return;
}

void FlowField::InsertNavPolies( const std::vector< std::shared_ptr< const NavPoly > >& polies ) {

    // ** mark the entire grid unwalkable except for areas inside triangles
    for ( uint x=0; x<grid.size(); ++x ) {
        for ( uint y=0; y<grid[x].size(); ++y ) {
            ASSERT( grid[x].size() == static_cast<uint>( cols ) );

            const Vec2f aabb_min( gridVert[0].x + (cellSize*x), gridVert[0].y + (cellSize*y) );
            const Vec2f aabb_max( aabb_min.x + cellSize, aabb_min.y + cellSize );
            const AASquare2v square( aabb_min, aabb_max );

            grid[x][y] = GetFlowCellWeight( polies, square );
        }
    }
}

void FlowField::InsertObstacles( const std::vector< std::shared_ptr< const NavPoly > >& polies ) {
    std::vector< uint > checked_ent_ids;
    
    for ( const auto & check_poly : polies ) {
        if ( ! check_poly )
            continue;

        for ( auto & obstacle : check_poly->GetOccupyingEnts() ) {
            auto ent = obstacle.lock();

            if ( ! ent )
                continue;

            if ( !ent->IsVisible() )
                continue;

            if ( ent->GetNoPhys() )
                continue;
                
            if ( !ent->bounds.IsSphere() )
                continue;
                
            const uint ent_id = ent->GetIndex();
            
            if ( Contains( checked_ent_ids, ent_id ) )
                continue;
                
            const Vec3f origin( ent->GetOrigin() );
            const float ent_radius = ent->bounds.GetRadius();

            const std::pair<int,int> pos_min( TranslateToGridPos( origin - ent_radius ) );
            const std::pair<int,int> pos_max( TranslateToGridPos( origin + ent_radius ) );
            
            for ( int x=pos_min.first; x<=pos_max.first; ++x ) {
                for ( int y=pos_min.second; y<=pos_max.second; ++y ) {
                    if ( !IsPosWithinBounds( x,y ) )
                        continue;
                    
                    const uint ux = static_cast<uint>(x);
                    const uint uy = static_cast<uint>(y);

                    if ( grid[ux][uy] == FLOW_CELL_OUTSIDE_NAVMESH )
                        continue; // already unreachable
                        
                    const Vec2f aabb_min( gridVert[0].x + (cellSize*x), gridVert[0].y + (cellSize*y) );
                    const Vec2f aabb_max( aabb_min.x + cellSize, aabb_min.y + cellSize );

                    if ( !Collision2D::TestCircle::ToAASquare( Vec2f(origin.x, origin.y), ent_radius, aabb_min, aabb_max ) )
                        continue;
                }
            }
            
            obstacles.push_back( ent );
            checked_ent_ids.push_back( ent->GetIndex() ); // mark this entity as checked
        }
    }
}

void FlowField::DoWaveCell( const int val, const int x, const int y, std::queue< std::pair< int, int > >& destCells ) {
    if ( !IsPosWithinBounds(x,y) )
        return;

    const uint ux = static_cast<uint>( x );
    const uint uy = static_cast<uint>( y );
    
    if ( grid[ux][uy] != FLOW_CELL_TO_GRADIENT )
        return;
    
    grid[ux][uy] = val+1;
    destCells.emplace(x,y);
}

void FlowField::DoWaveFront( std::queue< std::pair< int, int > > destCells ) {
    
    while ( destCells.size() > 0 ) {
        const int x = destCells.front().first;
        const int y = destCells.front().second;
        destCells.pop();
        
        const uint ux = static_cast<uint>( x );
        const uint uy = static_cast<uint>( y );
        
        const int val = grid[ux][uy];

        if ( val == FLOW_CELL_OUTSIDE_NAVMESH )
            continue; // this was a destination, but it's outside the navmesh

        // **  we just do a plus-shape, the diagonals will be filled out properly with this algorithm
        DoWaveCell( val, x+1, y, destCells );
        DoWaveCell( val, x-1, y, destCells );
        DoWaveCell( val, x, y+1, destCells );
        DoWaveCell( val, x, y-1, destCells );
    }
}

void FlowField::DrawDebug( void ) const {
    if ( ! IsReady() )
        return;

    if ( globalVals.GetBool( gval_d_flowfield  ) || globalVals.GetBool( gval_d_navAll )) {
        DrawDebug_HeatMap();
        DrawCostFieldDebug();
    }
}

//! make sure the box can encompass an even number of cells based on cellSize. Returns dimensions of grid
Vec2f FlowField::ComputeDimensions( void ) {
    
    Vec2f gridDimensions( gridVert[2].x - gridVert[1].x, gridVert[1].y - gridVert[0].y );

    ASSERT( gridDimensions.x > 0 );
    ASSERT( gridDimensions.y > 0 );

    Vec2f adjustment;
    if ( cellSize > gridDimensions.y ) {
        adjustment.y = cellSize - gridDimensions.y; // box is not big enough, so just make it big enough for one cell
    } else {
        adjustment.y = cellSize - fmodf( gridDimensions.y, cellSize );
    }

    gridVert[1].y += adjustment.y;

    gridVert[2].y += adjustment.y;
    gridDimensions.y += adjustment.y;

    if ( cellSize > gridDimensions.x ) {
        adjustment.x = cellSize - gridDimensions.x; // box is not big enough, so just make it big enough for one cell
    } else {
        adjustment.x = cellSize - fmodf( gridDimensions.x, cellSize );
    }

    gridVert[2].x += adjustment.x;
    gridVert[3].x += adjustment.x;
    gridDimensions.x += adjustment.x;

    ASSERT( gridDimensions.x >= 1 );
    ASSERT( gridDimensions.y >= 1 );

    return gridDimensions;
}

void FlowField::DrawDebug_HeatMap( void ) const {

    Color4f red1(1,0,1,1);
    Color4f red2(0.9f,0.1f,0,1);
    Color4f red3(0.6f,0.4f,0,1);

    if ( ! IsReady() )
        return;

    for ( int row=0; row<rows; ++row ) {
        for ( int col=0; col<cols; ++col ) {
            Vec3f bottomLeft(gridVert[0].x, gridVert[0].y, 0);
            bottomLeft.x += row * cellSize;
            bottomLeft.y += col * cellSize;
            Vec3f bottomRight( bottomLeft.x, bottomLeft.y + cellSize, 0 );
            Vec3f topLeft( bottomLeft.x + cellSize, bottomLeft.y, 0 );

            //const Vec3f cell_center( ( topLeft + bottomRight ) / 2 );

            //int a1 = grid[static_cast<uint>(row)][static_cast<uint>(col)];
            int a1 = GetCellCost(row, col );
            if ( a1 == 0 )
                a1 = 1;
            red1.b = a1*0.08f;
            red1.r = 1/red1.b;
            red1.a = 0.5f;

            Vec3f topRight( bottomLeft.x + cellSize, bottomLeft.y + cellSize, 0 );
            renderer->DrawTri( bottomRight, topRight, topLeft, red1, red1, red1  );
            renderer->DrawTri( topLeft, bottomLeft, bottomRight, red1, red1, red1 );
        }
    }

    renderer->DrawLine( gridVert[0].ToVec3(), gridVert[1].ToVec3(), Color4f(0,0,3,1), Color4f(0,0,3,1) );
    renderer->DrawLine( gridVert[1].ToVec3(), gridVert[2].ToVec3(), Color4f(0,0,3,1), Color4f(0,0,3,1) );
    renderer->DrawLine( gridVert[2].ToVec3(), gridVert[3].ToVec3(), Color4f(0,0,3,1), Color4f(0,0,3,1) );
    renderer->DrawLine( gridVert[3].ToVec3(), gridVert[0].ToVec3(), Color4f(0,0,3,1), Color4f(0,0,3,1) );
}

void FlowField::DrawCostFieldDebug( void ) const {
    if ( ! IsReady() )
        return;

    VBO<float> vbo_cost( 2,VBOChangeFrequencyT::FREQUENTLY ); //TODOWEAKPTR
    VBO<float> vbo_cost_uv( 2,VBOChangeFrequencyT::FREQUENTLY ); //TODOWEAKPTR

    FontInfo fi;
    fi.size_ui_px = 0.4f;

    glDisable( GL_DEPTH_TEST );

        const char* fontname = "courier";
        std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
        Font::AssertValid( fnt, fontname );

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SetUniform1f("fWeight", 1.0f );

        for ( int row=0; row<rows; ++row ) {
            for ( int col=0; col<cols; ++col ) {

                vbo_cost.Clear();
                vbo_cost_uv.Clear();

                vbo_cost.PackText( vbo_cost_uv, std::to_string(grid[static_cast<uint>(row)][static_cast<uint>(col)]).c_str(), fi );

                vbo_cost.MoveToVideoCard();
                vbo_cost_uv.MoveToVideoCard();

                ASSERT( vbo_cost.Finalized() );
                ASSERT( vbo_cost_uv.Finalized() );

                renderer->PushMatrixMV();

                    Vec3f center_square(gridVert[0].ToVec3());
                    center_square.x += (cellSize*row) + (cellSize/2);
                    center_square.y += (cellSize*col) + (cellSize/2);

                    renderer->ModelView().Translate( center_square.x, center_square.y, 0 );

                    // we must keep translation but discard rotation in order for text to always face the screen
                    Vec3f translation( renderer->ModelView().GetTranslation() );

                    renderer->ModelView().LoadIdentity();
                    renderer->ModelView().SetTranslation( translation );

                    // flip Y axis, since UI is usually for drawing with 0,0 as top-left of screen
                    renderer->ModelView().Scale( 1.0f,-1.0f,1.0f );

                    shaders->SendData_Matrices();
                    shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
                    shaders->SetUniform4f("vColor", Color4f(0,1,0,1) );
                    shaders->SetAttrib( "vUV", vbo_cost_uv );
                    shaders->SetAttrib( "vPos", vbo_cost );
                    shaders->DrawArrays( GL_QUADS, 0, vbo_cost.Num() );

                renderer->PopMatrixMV();

            }
        }

    glEnable( GL_DEPTH_TEST );

}

int FlowField::GetCellCost( const int x, const int y ) const {
    if ( ! IsPosWithinBounds(x,y) )
        return FLOW_CELL_OUTSIDE_NAVMESH;

    const uint ux = static_cast<uint>(x);
    const uint uy = static_cast<uint>(y);
    
    return grid[ux][uy];
}

Vec3f FlowField::GetMoveDirection( const Entity& movingEnt, const Vec3f& curOrigin, const std::vector<uint>& obstacle_ent_ids_to_ignore ) const {
    if ( ! IsReady() )
        return {};

    const std::pair<int,int> pos = TranslateToGridPos( curOrigin );

    //todo: enemy is still chasing player outside of grid when sight distance is greater than the bounds of the grid. I've tagged the place you can set the sight distance at todotagstuff
    if ( !IsPosWithinBounds( pos ) ) {
        const Vec3f centerOfGrid(
            ( gridVert[0].x + gridVert[2].x ) / 2
            , ( gridVert[0].y + gridVert[2].y ) / 2
            , 0
        );

        const Vec3f org = movingEnt.GetOrigin();
        Vec3f moveDir( centerOfGrid - org );
        moveDir.z = 0;
        moveDir.Normalize();
        return moveDir;
    }

    // Get all the spaces with values less than our current space
    
    const int& x = pos.first;
    const int& y = pos.second;
    
    const float up = GetCellCost( x, y+1 );
    const float down = GetCellCost( x, y-1 );
    const float left = GetCellCost( x-1, y );
    const float right = GetCellCost( x+1, y );
    Vec3f dir( left - right, down - up, 0 );
    dir.Normalize();
    
    if ( globalVals.GetBool( gval_ai_ffPush ) )
        return DoCellObstaclePush( dir, movingEnt, obstacle_ent_ids_to_ignore );

    return dir;
}

Vec3f FlowField::DoCellObstaclePush( const Vec3f& dir, const Entity& movingEnt, const std::vector<uint>& obstacle_ent_ids_to_ignore ) const {
    // find the nearest obstacle around us and apply a force on our movement vector perpendicular to it
    
    ASSERT(movingEnt.bounds.IsSphere());
    
    const Vec3f movingEntOrigin3d = movingEnt.GetOrigin();
    const Vec2f movingEntOrigin( movingEntOrigin3d.x, movingEntOrigin3d.y );
    float dist = 9999; // big enough
    std::shared_ptr< Entity > obstacle;
    Vec2f obstacle_origin;
    const float movingEntRadius = movingEnt.bounds.GetRadius();
    Vec2f push;

    for ( auto & entp : obstacles ) {
        auto ent = entp.lock();
        if ( ! ent )
            continue;
            
        if ( ! ent->bounds.IsSphere() )
            continue;
            
        if ( Contains( obstacle_ent_ids_to_ignore, ent->GetIndex() ) )
            continue;
            
        const Vec3f origin_check3d = ent->GetOrigin();
        const Vec2f origin_check( origin_check3d.x, origin_check3d.y );
        const float origin_radius = ent->bounds.GetRadius();
        const Vec2f push_check = movingEntOrigin - origin_check;
        const float dist_check = push_check.Len() - origin_radius - movingEntRadius;
        if ( dist_check >= dist )
            continue;
    /*
        // if obstacle is behind us in the direction of our intended path of travel, ignore it
        const float distBehind = Line2f::GetProjectedPoint( LineTypeT::LINE, movingEntOrigin, movingEntOrigin+Vec2f(dir.x,dir.y), obstacle_origin ); 
        if ( distBehind <= 0 )
            continue;
    */
        push = push_check;
        dist = dist_check;
        obstacle = ent;
        obstacle_origin.Set( origin_check.x, origin_check.y );
    }
    
    const float bubbleArea = 2;

    if ( globalVals.GetBool( gval_d_ffPush ) ) {
        const Line3f dirLine( movingEntOrigin3d, movingEntOrigin3d+dir*5 );
        renderer->DrawLine_Debug( dirLine.vert[0], dirLine.vert[1], Color4f(1,0,0,1), Color4f(1,0,0,1) );
    }
    
    if ( dist < bubbleArea && obstacle ) {
        
        const Vec2f pushDir = push.GetNormalized();
        
        const float fraction = (bubbleArea-dist) / bubbleArea;
        const Vec3f newDir = dir + Vec3f(pushDir.x, pushDir.y, 0 ) * fraction;
        
        if ( globalVals.GetBool( gval_d_ffPush ) ) {
            const Line3f pushLine( movingEntOrigin3d, movingEntOrigin3d + Vec3f(pushDir.x, pushDir.y, 0 ) );
            renderer->DrawLine_Debug( pushLine.vert[0], pushLine.vert[1], Color4f(0,1,0,1), Color4f(0,1,0,1) );
            
            const Line3f newDirLine( movingEntOrigin3d, movingEntOrigin3d + newDir );
            renderer->DrawLine_Debug( newDirLine.vert[0], newDirLine.vert[1], Color4f(0,0,1,1), Color4f(0,0,1,1) );
        }
        
        return newDir;
    }
    
    return dir;
}

bool FlowField::IsPosWithinBounds( const std::pair<int,int>& pos ) const {
    return IsPosWithinBounds(pos.first, pos.second);
}

bool FlowField::IsPosWithinBounds( const int x, const int y ) const {
    if ( ! IsReady() )
        return false;

    if ( x < 0 )
        return false;

    if ( y < 0 )
        return false;

    if ( x >= rows )
        return false;

    if ( y >= cols )
        return false;

    return true;
}

std::pair<int,int> FlowField::TranslateToGridPos( const Vec3f& origin ) const {
    if ( ! IsReady() )
        return {-1,-1};

    float x = ( origin.x - gridVert[0].x ) / ( gridVert[2].x - gridVert[0].x );
    float y = ( origin.y - gridVert[0].y ) / ( gridVert[2].y - gridVert[0].y );
    x *= rows;
    y *= cols;

    if ( ( x < 0 ) || ( x >= rows ) )
        x = -1;

    if ( ( y < 0 ) || ( y >= cols ) )
        y = -1;

    return std::pair<int,int>( static_cast<int>( x ), static_cast<int>( y ) );
}

bool FlowField::IsReady( void ) const {
    return rows != 0;
}

void FlowField::SetUnready( void ) {
    rows = 0;
}
