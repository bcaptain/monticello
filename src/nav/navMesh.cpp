// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./navMesh.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../math/collision/collision.h"
#include "../rendering/fontManager.h"
#include "../rendering/font.h"

const bool NAVMESH_DEBUG = true;

const float SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT = 0.8f;
const float SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT_SQ = 0.8f;
const Color4f DEFAULT_NUDGE_NAVPOLY_VERT_PREVIEW_COLOR(0,0,1,1);
const float DEFAULT_NUDGE_NAVPOLY_VERT_PREVIEW_SIZE(7);

const Color4f NAV_PATH_POLY_COLOR(1,1,0,0.2f);
const Color4f NAV_PATH_PORTAL_COLOR(0,0,1,0.1f);
const Color4f NAV_PATH_LINE_COLOR(0,1,0,1);

LinkList< std::shared_ptr< NavMesh > > NavMesh::meshes;
std::vector< Vec2f > NavMesh::vertex_pool;
uint NavMesh::next_id(0);
const Color4f DEFAULT_NAV_EXTRUSION_PREVIEW_COLOR( 0,1,0,0.3f );
const Color4f DEFAULT_NAV_VERT_ADD_PREVIEW_COLOR( 1,0,0,1 );

NavPoly::NavPoly( void )
    : children()
    , neighbors()
    , verts()
    , vbo_qty()
    , vbo_qty_uv()
    , master(nullptr)
    , parent(nullptr)
    , occupying_entities()
    , id( NavMesh::GenerateNewID() )
    , edges_with_children()
{ }

NavPoly::NavPoly( NavMesh* _master, NavPoly* _parent )
    : children()
    , neighbors()
    , verts()
    , vbo_qty()
    , vbo_qty_uv()
    , master( _master )
    , parent( _parent )
    , occupying_entities()
    , id( NavMesh::GenerateNewID() )
    , edges_with_children()
{
    if ( parent ) {
        edges_with_children.push_back(0); // the child's bottom (heh) is where the seam is
    }
}

NavPoly::~NavPoly( void ) {
}

void NavPoly::pack_and_send_qty_vbo( void ) const {
    if ( ! vbo_qty || ! vbo_qty_uv ) {
        return;
    }

    FontInfo fi;
    fi.size_ui_px = 0.4f;
    vbo_qty->PackText( *vbo_qty_uv, std::to_string(occupying_entities.size()).c_str(), fi );
    vbo_qty->MoveToVideoCard();
    vbo_qty_uv->MoveToVideoCard();
}

void NavPoly::DrawQtyVBO( void ) {
    if ( ! vbo_qty ) {
        vbo_qty.reset( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) ); //TODOWEAKPTR
        vbo_qty_uv.reset( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) ); //TODOWEAKPTR
    }

    if ( ! vbo_qty->Finalized() || ! vbo_qty_uv->Finalized() ) {
        return;
    }

    glDisable( GL_DEPTH_TEST );

        const char* fontname = "courier";
        std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
        Font::AssertValid( fnt, fontname );

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SetUniform1f("fWeight", 1.0f );

        renderer->PushMatrixMV();

            Vec2f center_poly(GetVert(0));
            center_poly += GetVert(1);
            center_poly += GetVert(2);
            center_poly /= 3;
            
            renderer->ModelView().Translate( center_poly.x, center_poly.y, 0 );

            // we must keep translation but discard rotation in order for text to always face the screen
            Vec3f translation( renderer->ModelView().GetTranslation() );

            renderer->ModelView().LoadIdentity();
            renderer->ModelView().SetTranslation( translation );

            // flip Y axis, since UI is usually for drawing with 0,0 as top-left of screen
            renderer->ModelView().Scale( 1.0f,-1.0f,1.0f );

            shaders->SendData_Matrices();
            shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );

        renderer->PopMatrixMV();

        shaders->SetUniform4f("vColor", Color4f(0,1,0,1) );
        shaders->SetAttrib( "vUV", *vbo_qty_uv );
        shaders->SetAttrib( "vPos", *vbo_qty );
        shaders->DrawArrays( GL_QUADS, 0, vbo_qty->Num() );

    glEnable( GL_DEPTH_TEST );
}

#ifdef MONTICELLO_EDITOR
void NavPoly::VertexMerge_Helper( NavPoly& poly, const std::size_t cur_index, const std::size_t merge_index ) {
    for ( uint v=0; v<poly.verts.size(); ++v ) {
        if ( poly.verts[v] == cur_index ) {
            poly.verts[v] = merge_index;
            poly.FindNewNeighborsAtVertIndex( merge_index );
            auto _master = poly.GetMaster();
            ASSERT( _master );
            _master->UpdateAllVBOs();
        }

        // we are going to remove the index when we are done, since no verts will be pointing to it.
        // because of this, any vertex that previously pointed to one higher than it in vertex_pool must be decreased by 1.
        // we do this even if merge_index was just assigned in the block above 
        if ( poly.verts[v] > cur_index )
            poly.verts[v] -= 1;

    };

    for ( auto & child : poly.children ) {
        if ( child ) {
            VertexMerge_Helper( *child, cur_index, merge_index );
        }
    }
}

void NavPoly::MergeVert( const std::size_t cur_index, const std::size_t merge_index ) {
    for ( auto & mesh : NavMesh::meshes ) {
        if ( mesh ) {
            VertexMerge_Helper( *mesh, cur_index, merge_index );
        }
    }

    ShuffleErase( NavMesh::vertex_pool, cur_index );

    // this vertex is no longer in use.
    // all mesh verts pointing to an index in vertex_pool higher than this index have been updated by VertexMerge_Helper
}

void NavPoly::VertInserted( const uint at_index ) {
    // update sides with edges
    for ( uint v=0; v<edges_with_children.size(); ++v )
        if ( edges_with_children[v] >= at_index )
            ++edges_with_children[v];
}

void NavPoly::FindNewNeighborsAtVertIndex( const std::size_t index ) {
    for ( auto & mesh : NavMesh::meshes )
        if ( mesh )
            TryToBecomeNeighborsWithAllShareresOfIndex( *mesh, index );
}

void NavPoly::TryToBecomeNeighborsWithAllShareresOfIndex( NavPoly& with, const std::size_t index ) {
    for ( uint v=0; v<with.verts.size(); ++v )
        if ( index == with.verts[v] )
            with.TryToBecomeNeighbors( *this );

    for ( auto & child : with.children )
        if ( child )
            TryToBecomeNeighborsWithAllShareresOfIndex( *child, index );
}

bool NavPoly::TryToBecomeNeighbors( NavPoly& with ) {
    if ( this == &with )
        return false;

    std::size_t num = 0;
    for ( uint v=0; v<verts.size(); ++v )
        num += static_cast< uint >( std::count( std::cbegin( with.verts ), std::cend( with.verts ), verts[v] ) );

    if ( num != 2 )
        return false;

    auto with_sptr = with.GetSharedPtr();
    if ( !with_sptr )
        return false;

    // can't be neighbors with a parent
    if ( parent && parent == &with )
        return false;

    // can't be neighbors with a child (heh)
    for ( auto const & child : children )
        if ( child.get() == &with )
            return false;

    if ( HasNeighbor( *with_sptr ) )
        return true; // we're already neighbors

    auto this_sptr = GetSharedPtr();
    if ( !this_sptr )
        return false;

    neighbors.Append( with_sptr );
    with.neighbors.Append( this_sptr );
    return true;
}
#endif // MONTICELLO_EDITOR

bool NavPoly::HasNeighbor( const NavPoly& poly ) const {
    for ( auto const & wptr : neighbors )
        if ( auto const & sptr = wptr.lock() )
            if ( sptr.get() == &poly )
                return true;
    return false;
}

void NavPoly::PackVBO( VBO<float>& vbo_vert, VBO<float>& vbo_color, const Color4f& tint ) {
    if ( verts.size() < 3 )
        return;

    Color4f tintGradient(tint);
    tintGradient.r += 0.5f;
    tintGradient.g += 0.5f;
    tintGradient.b += 0.5f;

    for ( uint i=2; i<verts.size(); ++i ) {
        vbo_vert.Pack( GetVert(0).ToVec3() );
        vbo_color.Pack( Color4f(tint) );

        vbo_vert.Pack( GetVert(i-1).ToVec3() );
        vbo_color.Pack( Color4f(tintGradient) );

        vbo_vert.Pack( GetVert(i).ToVec3() );
        vbo_color.Pack( Color4f(tintGradient) );
    }

    pack_and_send_qty_vbo();

    for ( auto & child : children )
        if ( child )
            child->PackVBO( vbo_vert, vbo_color, tint );
}

bool NavPoly::CircleIntersects( const Vec2f& center, const float radius ) const {
    std::vector< Vec2f > verts2d;
    verts2d.reserve(verts.size());

    // pack in reverse to send them clockwise
    for ( uint v=0; v<verts.size(); ++v ) {
        verts2d.emplace_back( GetVert(v) );
    }

    return Collision2D::TestCircle::ToPoly( verts2d, center, radius );
}

std::shared_ptr<NavPoly> NavPoly::GetChildAt( const Vec2f& pos ) const {
    for ( auto const & child : children ) {
        if ( child ) {
            if ( child->PointIntersects( pos ) )
                return child;
            if ( auto ret = child->GetChildAt( pos ) )
                return ret;
        }
    }
    return nullptr;
}

int NavPoly::GetEdgeClosestTo( const Vec2f& to ) const {
    if ( verts.size() < 3 )
        return -1;

    int ret=-1;
    float shortest;
    uint prev = verts.size()-1;
    for ( uint v=0; v<verts.size(); ++v ) {
        const Vec2f B = GetVert(v);
        const Vec2f A = GetVert(prev);

        const float dist = (B-A).Cross(to-A);
        if ( dist > 0 ) {
            if ( ret < 0  || dist < shortest ) {
                shortest = dist;
                ret = static_cast<int>( prev );
            }
        }
        prev = v;
    }

    return ret;
}

bool NavPoly::HasPortalOnEdge( const std::size_t edge_index_a, const std::size_t edge_index_b ) const {
    return HasChildOnEdge( edge_index_a ) || HasNeighborOnEdge( edge_index_a, edge_index_b );
}

bool NavPoly::HasChildOnEdge( const std::size_t edge_index_a ) const {
    return Contains(edges_with_children, edge_index_a );
}

#ifdef MONTICELLO_EDITOR
int NavPoly::GetUnusedEdgeNear( const Vec2f& to ) const {
    if ( verts.size() < 3 )
        return -1;

    const int edge = GetEdgeClosestTo( to );
        if ( edge < 0 )
            return -1;

    const uint e_start = static_cast< uint >( edge );

    if ( HasChildOnEdge( e_start ) )
        return -1; // can't use an edge twice

    const uint e_end = e_start+1 < verts.size() ? e_start+1 : 0;
    
    // can't use an edge that is neighbored
    if ( HasNeighborOnEdge( e_start, e_end ) )
        return -1;

    return static_cast<int>(e_start);
}

bool NavPoly::Extrude_Preview( const Vec2f& to ) const {
    if ( verts.size() < 3 )
        return false;

    const int edge_s = GetUnusedEdgeNear( to );
    if ( edge_s < 0 )
        return false;

    const uint edge = static_cast<uint>( edge_s );

    const uint a_index = verts[ edge ];
    const uint b_index = static_cast<uint>(edge)+1 < verts.size() ? verts[edge+1] : verts[0];

    const Vec3f a( NavMesh::vertex_pool[a_index].ToVec3() );
    const Vec3f b( NavMesh::vertex_pool[b_index].ToVec3() );
    renderer->DrawTri( b, a, to.ToVec3(), DEFAULT_NAV_EXTRUSION_PREVIEW_COLOR ); // reverse order of A and B to make it clockwise
    return true;
}

bool NavPoly::HasNeighborOnEdge( const std::size_t edge_index_a, const std::size_t edge_index_b ) const {
    for ( const auto n : neighbors )
        if ( auto neighnor = n.lock() )
            if ( Contains( neighnor->verts, verts[edge_index_a] ) )
                if ( Contains( neighnor->verts, verts[edge_index_b] ) )
                    return true;

    return false;
}

bool NavPoly::Get_InsertVertOnEdge_Pos( const Vec2f& click_pos, Vec2f& pos_out ) const {
    const int edge = GetUnusedEdgeNear( click_pos );
    if ( edge < 0 )
        return false;

    const uint a = static_cast<uint>(edge);
    const uint b = a+1 < verts.size() ? a+1 : 0;

    // can't add verts to sides that have children
    if ( HasChildOnEdge( a ) )
        return false;

    if ( HasNeighborOnEdge( a, b ) )
        return false;

    pos_out = NavMesh::vertex_pool[ verts[a] ];
    pos_out +=  NavMesh::vertex_pool[ verts[b] ];
    pos_out /= 2;
    return true;
}

void NavPoly::AppendVert( const Vec2f& vert ) {
    const std::size_t c_index = NavMesh::vertex_pool.size();
    NavMesh::vertex_pool.emplace_back( vert );
    verts.push_back( c_index );
}

bool NavPoly::AppendVertFromPool( const uint index ) {
    if ( index >= NavMesh::vertex_pool.size() )
        return false;

    verts.push_back( index );
    return true;
}

bool NavPoly::InsertVertAfterIndex( const int index, const Vec2f& vert ) {
    if ( index < 0 )
        return false;

    const uint a = static_cast<uint>( index );
    const uint b = a+1 < verts.size() ? a+1 : 0;

    // can't add verts to sides that have children
    if ( HasChildOnEdge( a ) )
        return false;

    if ( HasNeighborOnEdge( a, b ) )
        return false;

    const std::size_t c_index = NavMesh::vertex_pool.size();
    NavMesh::vertex_pool.emplace_back( vert );
    verts.insert( std::cbegin(verts)+static_cast< std::ptrdiff_t >(a)+1, c_index );

    VertInserted( static_cast<uint>(index)+1 );
    return true;
}

bool NavPoly::InsertVertAfterIndex( const int index ) {
    if ( index < 0 )
        return false;

    const uint a = static_cast<uint>( index );
    const uint b = a+1 < verts.size() ? a+1 : 0;

    Vec2f pos = NavMesh::vertex_pool[ verts[a] ];
    pos +=  NavMesh::vertex_pool[ verts[b] ];
    pos /= 2;

    return InsertVertAfterIndex( index, pos );
}

bool NavPoly::InsertVertOnEdge( const Vec2f& click_pos ) {
    const int edge = GetUnusedEdgeNear( click_pos );
    if ( edge < 0 )
        return false;

    return InsertVertAfterIndex(edge);
}

bool NavPoly::InsertVertOnEdge_Preview( const Vec2f& click_pos ) const {
    Vec2f vert;
    if ( ! Get_InsertVertOnEdge_Pos( click_pos, vert ) )
        return false;

    const int point_size = 10;
    renderer->DrawPoint( vert.ToVec3(), DEFAULT_NAV_VERT_ADD_PREVIEW_COLOR, point_size );
    return true;
}
#endif // MONTICELLO_EDITOR

NavMesh* NavPoly::GetMaster( void ) {
    return master ? master : dynamic_cast<NavMesh*>(this);
}

std::shared_ptr<NavPoly> NavPoly::Extrude( const Vec2f& to ) {
    int edge = GetUnusedEdgeNear( to );
    if ( edge < 0 )
        return nullptr;

    return ExtrudeFromEdge( to, static_cast<uint>( edge ) );
}

std::shared_ptr<NavPoly> NavPoly::ExtrudeFromEdge( const Vec2f& to, const uint edge ) {
    if ( verts.size() < 3 )
        return nullptr;

    const uint edge_u = static_cast<uint>(edge);

    if ( HasChildOnEdge( edge_u ) )
        return nullptr;

    edges_with_children.push_back( edge_u );
    auto _master = GetMaster();
    ASSERT( _master );

    const std::size_t c_index = NavMesh::vertex_pool.size();
    NavMesh::vertex_pool.emplace_back( to );

    auto extrusion_sptr( std::make_shared<NavPoly>( _master, this ) );
    ASSERT( extrusion_sptr );

    const uint a_index = static_cast<uint>( verts[ edge_u ] );
    const uint b_index = edge_u+1 < verts.size() ? verts[edge_u+1] : verts[0];

    // reverse order of A and B to make it clockwise
    extrusion_sptr->AppendVertFromPool( b_index );
    extrusion_sptr->AppendVertFromPool( a_index );
    extrusion_sptr->AppendVertFromPool( c_index );

    children.Append( extrusion_sptr );
    _master->UpdateAllVBOs();

    return extrusion_sptr;
}

void NavPoly::BruteInsertEntity( const std::shared_ptr< Entity >& ent, const Vec2f& pos, const float radius, std::vector< std::weak_ptr< NavPoly > >& new_occupied ) {
    if ( ! ent )
        return;

    GetOccupiedPolies( pos, radius, new_occupied );

    for ( auto & poly_wptr : new_occupied ) {
        auto poly = poly_wptr.lock();
        if ( ! poly )
            continue;

        // if we were already in it, we don't get inserted twice
        if ( !ContainsEnt( poly->occupying_entities, *ent ) ) {
            poly->occupying_entities.push_back( ent );
        }
    }
}

bool NavPoly::ReinsertEntity_Helper( const std::shared_ptr< Entity >& ent, const Vec2f& pos, const float radius, std::vector< std::weak_ptr< NavPoly > >& new_occupied, const std::shared_ptr< NavPoly >& poly ) {

    if ( !poly )
        return false;

    if ( poly->CircleIntersects( pos, radius ) ) {
        if ( !ContainsWeakPtr( poly->occupying_entities, ent ) ) {

            // link entity to poly
            poly->occupying_entities.push_back( ent );
            new_occupied.push_back( poly );
            return true;
        }
    } else {

        // unlink entity from poly
        RemoveWeakPtr( poly->occupying_entities, ent  );
        RemoveWeakPtr( new_occupied, poly );
        return true;
    }

    return false;
}

std::vector< std::weak_ptr< NavPoly > > NavPoly::ReinsertEntity( const std::shared_ptr< Entity >& ent, const Vec2f& pos, const float radius, const std::vector< std::weak_ptr< NavPoly > >& cur_occupied ) {
    if ( ! ent )
        return {};

    std::vector< std::weak_ptr< NavPoly > > new_occupied( cur_occupied );

    // for each NavPoly we were in previously, check it as well as its neighbors if we are inside them currently

    for ( auto & poly_wptr : cur_occupied ) {
        auto poly = poly_wptr.lock();
        if ( !poly )
            continue;

        bool updateQtyVBO = false;

        updateQtyVBO = ReinsertEntity_Helper( ent, pos, radius, new_occupied, poly ) || updateQtyVBO;

        for ( auto & child : poly->children )
            updateQtyVBO = ReinsertEntity_Helper( ent, pos, radius, new_occupied, child ) || updateQtyVBO;

        if ( poly->parent )
            updateQtyVBO = ReinsertEntity_Helper( ent, pos, radius, new_occupied, poly->parent->GetSharedPtr() ) || updateQtyVBO;

        if ( updateQtyVBO ) {
            auto _master = poly->GetMaster();
            ASSERT( _master );
            _master->UpdateAllVBOs();
        }

        for ( auto & neighbor : poly->neighbors ) {
            std::shared_ptr< NavPoly > nei = neighbor.lock();
            if ( ! nei )
                continue;

            ReinsertEntity_Helper( ent, pos, radius, new_occupied, nei );
            auto _master = nei->GetMaster();
            ASSERT( _master );
            _master->UpdateAllVBOs();
        }
    }

    return new_occupied;
}

void NavPoly::GetOccupiedPolies( const Vec2f& pos, const float radius, std::vector< std::weak_ptr< NavPoly > >& new_occupied ) {
    if ( CircleIntersects( pos, radius ) ) {
        new_occupied.push_back( GetSharedPtr() ); // we don't worry about checking if we're already in the list because the caller is supposed to give us an empty list
    }

    for ( auto & child : children )
        if ( child )
            child->GetOccupiedPolies( pos, radius, new_occupied );
}


void NavPoly::BruteInsertEntity( const std::shared_ptr< Entity >& ent, const Vec2f& pos, std::vector< std::weak_ptr< NavPoly > >& new_occupied ) {
    if ( ! ent )
        return;

    GetOccupiedPolies( pos, new_occupied );

    for ( auto & poly_wptr : new_occupied ) {
        auto poly = poly_wptr.lock();
        if ( ! poly )
            continue;

        // if we were already in it, we don't get inserted twice
        if ( !ContainsEnt( poly->occupying_entities, *ent ) ) {
            poly->occupying_entities.push_back( ent );
        }
    }

}

void NavPoly::GetOccupiedPolies( const Vec2f& pos, std::vector< std::weak_ptr< NavPoly > >& new_occupied ) {
    if ( PointIntersects( pos ) ) {
        new_occupied.push_back( GetSharedPtr() ); // we don't worry about checking if we're already in the list because the caller is supposed to give us an empty list
    }

    for ( auto & child : children )
        if ( child )
            child->GetOccupiedPolies( pos, new_occupied );
}


std::shared_ptr< const NavPoly > NavPoly::GetSharedPtr( void ) const {
    ASSERT( parent );
    for ( auto & child : parent->children )
        if ( child.get() == this )
            return child;
    DIE("Could not find shared_ptr for NavPoly.\n");
    return {}; // if user wishes to not abort the program
}

std::shared_ptr< NavPoly > NavPoly::GetSharedPtr( void ) {
    return std::const_pointer_cast< NavPoly >( const_cast< const NavPoly* >( this )->GetSharedPtr() );
}

std::shared_ptr< NavPoly > NavPoly::GetChild( const NavPoly& poly ) const {
    for ( auto const & child : children )
        if ( child.get() == &poly )
            return child;
    return nullptr;
}

bool AddPortalHelper( const Vec2f& A, const Vec2f& B, LinkList< Line2f>& portals_out, const float actor_radius );
bool AddPortalHelper( const Vec2f& A, const Vec2f& B, LinkList< Line2f>& portals_out, const float actor_radius ) {
    if ( Vec2f::SquaredLen(A-B) < actor_radius * actor_radius )
        return false;

    portals_out.Append( Line2f( A, B ) );
    return true;
}

bool NavPoly::AddPortal( const NavPoly& with, LinkList< Line2f>& portals_out, const float actor_radius ) const {
    if ( verts.size() < 3 )
        return false;

    const int edge_out = GetSharedEdge( with );
    if ( edge_out < 0 )
        return false;

    const uint b = static_cast<uint>( edge_out );
    const uint a = b+1 < verts.size() ? b+1 : 0;

    return AddPortalHelper( NavMesh::vertex_pool[verts[a]], NavMesh::vertex_pool[verts[b]], portals_out, actor_radius );
}

void NavPoly::GetPathTo_Helper( const std::shared_ptr<const NavPoly>& poly, NavData& data ) const {
    if ( ! poly )
        return;

    // ** push

    // if a portal wasn't added, it's because the path is too narrow
    if ( ! AddPortal( *poly, data.portals[data.portals.size()-1], data.actor_radius ) )
        return;

    data.polies[data.polies.size()-1].Append( poly );

    // ** execute
    poly->GetPathTo( data );

    // ** pop
    data.polies[data.polies.size()-1].GetLast()->Del();
    data.portals[data.portals.size()-1].GetLast()->Del();
}

void NavPoly::GetPathTo( NavData& data ) const {
    ASSERT( data.polies.size() >= 1 );
    ASSERT( data.portals.size() >= 1 );

    if ( data.polies[data.polies.size()-1].Num() >= data.max_poly_moves )
        return;

    // if we've checked this already, don't check. (prevents infinite looping also)
    if ( std::find( std::cbegin( data.checked_ids ), std::cend( data.checked_ids ), GetID() ) != std::cend( data.checked_ids ) )
        return;

    data.checked_ids.emplace_back( GetID() ); // push

    // check children to see if this is one of our destinations, and if so add them to a list and continue checking for another
    for ( auto const & target : data.any_of ) {
        if ( auto const & targ = target.lock() ) {
            if ( targ.get() == this ) {
                // ** duplicate the current path
                data.polies.emplace_back( data.polies[data.polies.size()-1] );
                data.portals.emplace_back( data.portals[data.portals.size()-1] );
                return; // no need to check our children/parents/etc because we've already arrived
            }
        }
    }

    // recurse and do the same for the children
    for ( auto itr = std::cbegin(children); itr != std::cend(children); ++itr )
        if ( auto child = *itr )
            GetPathTo_Helper( child, data );

    // recurse and do the same for the neighbors
    for ( auto itr = std::cbegin(neighbors); itr != std::cend(neighbors); ++itr )
        if ( auto neighbor = itr->lock() )
            GetPathTo_Helper( neighbor, data );

    // recurse and do the same for the parent
    if ( parent )
        GetPathTo_Helper( parent->GetSharedPtr(), data );

    data.checked_ids.pop_back(); // pop
}

//! returns whether edge a is the same as edge b (same indices, whether forward or backward)
bool IsEdgeSame( const std::size_t a1, const std::size_t b1, const std::size_t a2, const std::size_t b2 );
bool IsEdgeSame( const std::size_t a1, const std::size_t b1, const std::size_t a2, const std::size_t b2 ) {
    if ( a1 == a2 && b1 == b2 )
        return true;

    // try the line in reverse
    if ( a1 == b2 && b1 == a2 )
        return true;

    return false;
}

bool NavPoly::HasEdge( const std::size_t a1, const std::size_t b1 ) const {
    if ( verts.size() < 3 )
        return false;

    uint prev = verts.size()-1;
    for ( uint v=0; v<verts.size(); ++v ) {
        if ( IsEdgeSame( a1, b1, GetVertIndex(prev), GetVertIndex(v) ) )
            return true;
        prev = v;
    }

    return false;
}

int NavPoly::GetSharedEdge( const NavPoly& with ) const {
    if ( verts.size() < 3 )
        return -1;

    // ** find two shared vertices;
    uint prev = verts.size()-1;
    for ( uint v=0; v<verts.size(); ++v ) {
        if ( with.HasEdge( verts[prev], verts[v] ) ) {
            return static_cast<int>( prev );
        }
        prev = v;
    }

    return -1;
}

void NavPoly::NudgeVert_Preview( const Vec2f& click_pos ) {
    const auto indices = GetIndicesForDeform( click_pos );
    auto itr = std::cbegin( indices );
    auto previous_itr = std::cend( indices );
    while ( itr != std::cend( indices ) ) {
        auto index = *itr;
        ASSERT( index < NavMesh::vertex_pool.size() );

        // Draw a point at the vertex
        renderer->DrawPoint(
            NavMesh::vertex_pool[index].ToVec3(),
             DEFAULT_NUDGE_NAVPOLY_VERT_PREVIEW_COLOR,
             DEFAULT_NUDGE_NAVPOLY_VERT_PREVIEW_SIZE
        );

        // Draw a Line if it's an edge
        if ( previous_itr !=  std::cend( indices ) ) {
            auto previous_index = *previous_itr;
            renderer->DrawLine(
                NavMesh::vertex_pool[previous_index].ToVec3(),
                NavMesh::vertex_pool[index].ToVec3(),
                DEFAULT_NUDGE_NAVPOLY_VERT_PREVIEW_COLOR,
                DEFAULT_NUDGE_NAVPOLY_VERT_PREVIEW_COLOR
            );
        }

        previous_itr = itr;
        ++itr;
    }
}

int NavMesh::GetVertexFromPoolIndexAt( const Vec2f& at, const std::vector<std::size_t>& but_none_of ) {
    // ** if we click within some radius of the vertex, return it
    int ret = -1;
    float closest=100; // something high is all that matters, if nothing is lower than SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT_SQ we return -1 anyway
    for ( std::size_t i=0; i< vertex_pool.size(); ++i ) {
        if ( Contains( but_none_of, i ) )
            continue;
        float len_sq = Vec2f::SquaredLen( at - vertex_pool[i] );
        if ( len_sq <= SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT_SQ ) {
            if ( len_sq < closest ) {
                ret = static_cast< int >( i );
                closest = len_sq;
            }
        }
    }
    if ( closest <= SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT_SQ )
        return ret;
    return -1;
}

void NavPoly::NudgeVert( const std::size_t index, const Vec2f& nudge ) {
    ASSERT( index < NavMesh::vertex_pool.size() );
    NavMesh::vertex_pool[index] += nudge;
    auto _master = GetMaster();
    ASSERT( _master );
    _master->UpdateAllVBOs();
}

std::vector<std::size_t> NavPoly::GetAllVertsBehindPoint( const Vec2f& at ) const {
    std::vector<std::size_t> ret = GetAllVertIndicesBehindPoint( at );
    for ( uint i=0; i<ret.size(); ++i )
        ret[i] = verts[ ret[i] ];
    return ret;
}

std::vector<std::size_t> NavPoly::GetAllVertIndicesBehindPoint( const Vec2f& at ) const {
    std::vector<std::size_t> ret;
    ret.reserve(3); // reasonable

    // get all edges the point is in front of
    uint prev = verts.size()-1;
    for ( uint v=0; v<verts.size(); ++v ) {
        const Vec2f B(GetVert(v));
        const Vec2f A(GetVert(prev));

        if ( (B-A).Cross(at-A) >= 0 ) {
            ret.emplace_back( prev );
            ret.emplace_back( v );
        }

        prev = v;
    }

    SortAndRemoveDuplicates( ret );
    return ret;
}

int NavPoly::GetEdgeBehindPoint( const Vec2f& at ) const {
    int ret = -1;
    float ret_dist=999999; // it's impossible for this to be uninitialized, but the compiler doens't comprehend that so here's a save default

    uint prev = verts.size()-1;
    for ( uint v=0; v<verts.size(); ++v ) {
        const Vec2f B(GetVert(v));
        const Vec2f A(GetVert(prev));

        float dist = (B-A).Cross(at-A);
        if ( dist >= 0 ) {
            if ( ret < 0 || dist < ret_dist ) {
                ret = static_cast<int>(prev);
                ret_dist = dist;
            }
        }

        prev = v;
    }

    return ret;
}

std::vector<std::size_t> NavPoly::GetIndicesForDeform( const Vec2f& at ) const {
    if ( verts.size() < 3 )
        return {};

    // ** if we click within some radius of the vertex, return it
    for ( auto i : verts )
        if ( Vec2f::SquaredLen( at - NavMesh::vertex_pool[i] ) <= SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT_SQ )
            return {i};

    // ** if we selected in the middle of the poly, return all vertices
    if ( PointIntersects( at ) )
        return verts;

    // get all edges the point is in front of
    return GetAllVertsBehindPoint( at );
}

//todonow: neighbor also
bool NavPoly::ChildOrParentContainsVertIndex( const std::size_t index ) const {
    if ( parent )
        for ( uint v=0; v<parent->verts.size(); ++v )
            if ( parent->GetVertIndex(v) == index )
                return true;

    for ( auto const & sptr : children )
        if ( sptr )
            for ( uint v=0; v<sptr->verts.size(); ++v )
                if ( sptr->GetVertIndex(v) == index )
                    return true;
    return false;
}

Vec2f NavPoly::GetVertFromPool( const std::size_t index ) const {
    ASSERT( index < NavMesh::vertex_pool.size() );
    return NavMesh::vertex_pool[index];
}

std::vector< Vec3f > NavPoly::GetVerts3D( void ) const {
    std::vector< Vec3f > ret;
    ret.reserve( verts.size() );
    for (uint i=0;i<verts.size();++i)
        ret.emplace_back( NavMesh::vertex_pool[verts[i]].ToVec3() );
    return ret;

}

std::vector< Vec2f > NavPoly::GetVerts( void ) const {
    std::vector< Vec2f > ret;
    ret.reserve( verts.size() );
    for (uint i=0;i<verts.size();++i)
        ret.emplace_back( NavMesh::vertex_pool[verts[i]] );
    return ret;
}

Vec2f NavPoly::GetVert( const std::size_t index ) const {
    return NavMesh::vertex_pool[verts[index]];
}

void NavPoly::MapSaveTree( FileMap& saveFile ) const {

    saveFile.parser.WriteUInt( verts.size() );
    for ( uint v=0; v<verts.size(); ++v )
        saveFile.parser.WriteUInt( verts[v] );

    // master and parent get automatically generated on load
    // occupying_Entities get regenerated when entities re-enter the mesh
    saveFile.parser.WriteUInt( id );

    saveFile.parser.WriteUInt( edges_with_children.size() );
    for ( uint e=0; e<edges_with_children.size(); ++e )
        saveFile.parser.WriteUInt( edges_with_children[e] );

    saveFile.parser.WriteUInt( children.Num() );
    for ( auto const & child : children )
        if ( child )
            child->MapSaveTree( saveFile );
}

void NavPoly::GetNavChildrenData( std::vector< PolyData >& flattened_tree ) {
    auto ptr = GetSharedPtr();
    if ( ! ptr )
        return;

    flattened_tree.emplace_back( id, ptr );

    for ( auto const & child : children )
        if ( child )
            child->GetNavChildrenData( flattened_tree );
}

void NavPoly::MapSaveNeighbors( FileMap& saveFile ) const {
    saveFile.parser.WriteUInt( neighbors.Num() );
    for ( auto const & wptr : neighbors )
        if ( auto neighbor = wptr.lock() )
            saveFile.parser.WriteUInt( neighbor->GetID() );

    for ( auto const & child : children )
        if ( child )
            child->MapSaveNeighbors( saveFile );
}

void NavPoly::MapLoadTree( FileMap& saveFile, NavMesh* _master ) {
    uint vertNum = saveFile.parser.ReadUInt();
    verts.clear();
    edges_with_children.clear();
    verts.reserve(vertNum);
    for ( uint v=0; v<vertNum; ++v )
        verts.push_back( saveFile.parser.ReadUInt() );

    // master and parent get automatically generated on load
    // occupying_Entities get regenerated when entities re-enter the mesh
    saveFile.parser.ReadUInt( id );

    const uint numEdgesUsed = saveFile.parser.ReadUInt();
    edges_with_children.reserve(numEdgesUsed);
    for ( uint e=0; e<numEdgesUsed; ++e )
        edges_with_children.push_back( saveFile.parser.ReadUInt() );

    const uint num_kids = saveFile.parser.ReadUInt();
    children.DelAll();
    for ( uint i=0; i<num_kids; ++i ) {
        std::shared_ptr< NavPoly > child( new NavPoly ); // not using std::make_shared because our zero-arg constructor is private. we could make the allocator a friend class, but this just feels better.
        children.Append( child );
        child->master = _master;
        child->parent = this;
        child->MapLoadTree( saveFile, _master );
    }
}

void NavPoly::MapLoadNeighbors( FileMap& saveFile, const std::vector<PolyData> & linear_polies ) {
    const uint num_neighbors = saveFile.parser.ReadUInt();
    neighbors.DelAll();
    for ( uint i=0; i<num_neighbors; ++i ) {
        uint nid = saveFile.parser.ReadUInt();
        auto x = std::lower_bound( std::cbegin( linear_polies ), std::cend( linear_polies ), nid );
        ASSERT( x != std::cend( linear_polies ) );
        ASSERT( *x == nid );
        neighbors.Append( x->poly );
    }

    for ( auto const & child : children )
        if ( child )
            child->MapLoadNeighbors( saveFile, linear_polies );
}

bool NavPoly::GetNeighboringPoliesWithinAASquare( const std::shared_ptr< const NavPoly >& poly, std::vector< std::shared_ptr< const NavPoly > >& polies_out, const AASquare2v& aaSquare ) {
    if ( ! poly )
        return false;

    for ( uint i=0; i<polies_out.size(); ++i )
        if ( polies_out[i] )
            if ( poly == polies_out[i] )
                return false;

    const AASquare4v aaSquare4( aaSquare );
    const std::vector< Vec2f > tri{ poly->GetVert(0), poly->GetVert(1), poly->GetVert(2) };
    bool ret = false;

    // ** some early-outs
    ret = ret || Collision2D::TestPoint::ToAASquare4v( tri[0], aaSquare4 );
    ret = ret || Collision2D::TestPoint::ToAASquare4v( tri[1], aaSquare4 );
    ret = ret || Collision2D::TestPoint::ToAASquare4v( tri[2], aaSquare4 );

    // ** this probably doesn't save us any time
    // ret = ret || Collision2D::TestPoint::ToPoly( aabb_min, tri );
    // ret = ret || Collision2D::TestPoint::ToPoly( aabb_max, tri );
    // ret = ret || Collision2D::TestPoint::ToPoly( aabb_topLeft, tri );
    // ret = ret || Collision2D::TestPoint::ToPoly( aabb_bottomRight, tri );

    // ** more intense checking
    ret = ret || Collision2D::TestLineSeg::ToAASquare( tri[0], tri[1], aaSquare );
    ret = ret || Collision2D::TestLineSeg::ToAASquare( tri[1], tri[2], aaSquare );
    ret = ret || Collision2D::TestLineSeg::ToAASquare( tri[2], tri[0], aaSquare );

    if ( ! ret )
        return false;

    polies_out.push_back( poly );

    for ( const auto & neighbor_w : poly->neighbors )
        if ( auto neighbor = neighbor_w.lock() )
            neighbor->GetNeighboringPoliesWithinAASquare( neighbor, polies_out, aaSquare );

    for ( const auto & child : poly->children )
        if ( child )
            child->GetNeighboringPoliesWithinAASquare( child, polies_out, aaSquare );

    if ( poly->parent )
        GetNeighboringPoliesWithinAASquare( poly->parent->GetSharedPtr(), polies_out, aaSquare );


    return true;
}

void NavPoly::UpdateQtyVBO( void ) {
    if ( vbo_qty ) {
        vbo_qty->Clear();
        vbo_qty_uv->Clear();
    }

    for ( auto & child : children ) {
        child->UpdateQtyVBO();
    }
}

void NavPoly::DrawQtyVBOs( void ) {
    DrawQtyVBO();
    for ( auto & child : children )
        if ( child )
            child->DrawQtyVBOs();
}

uint NavPoly::NumEntities( void ) const {
    return occupying_entities.size();
}

bool NavPoly::PointIntersects( const Vec2f& to ) const {
    return CircleIntersects(to, 0);
}

uint NavPoly::NumVerts( void ) const {
    return verts.size();
}

std::vector< std::weak_ptr< Entity > > NavPoly::GetOccupyingEnts( void ) const {
    return occupying_entities;
}

NavMesh::~NavMesh( void ) {
}

NavMesh::NavMesh( void )
    : NavPoly( nullptr, nullptr )
    , vbo_vert()
    , vbo_color()
    , tint( Random::Float(0, 0.8f), Random::Float(0, 0.8f), Random::Float(0, 0.8f), 0.5f )
{
    vertex_pool.reserve(3);
}

void NavMesh::MapSaveAll( FileMap& saveFile ) {
    saveFile.parser.WriteUInt( next_id );
    saveFile.parser.WriteUInt( vertex_pool.size() );
    for ( auto const & vert : vertex_pool )
        saveFile.parser.WriteVec2f( vert );

    saveFile.parser.WriteUInt( meshes.Num() );
    for ( auto const & mesh : meshes )
        if ( mesh )
            mesh->NavPoly::MapSaveTree( saveFile );

    for ( auto const & mesh : meshes )
        if ( mesh )
            mesh->NavPoly::MapSaveNeighbors( saveFile );
}

void NavMesh::MapLoadAll( FileMap& saveFile ) {
    meshes.DelAll();

    saveFile.parser.ReadUInt( next_id );
    const uint num_vert = saveFile.parser.ReadUInt();
    vertex_pool.clear();
    vertex_pool.reserve( num_vert );
    for ( uint i=0; i<num_vert; ++i )
        vertex_pool.emplace_back( saveFile.parser.ReadVec2f() );

    const uint num_meshes = saveFile.parser.ReadUInt(); //unused

    std::vector< PolyData > flattened_tree; // a sorted std::vector of ids all the polies in all the trees
    flattened_tree.reserve(1024);

    // ** Load the tree heirarchies
    for ( uint i=0; i<num_meshes; ++i ) {
        auto mesh = std::make_shared<NavMesh>();
        meshes.Append( mesh );
        ASSERT( ! mesh->master ); // mesh->master = nullptr;
        ASSERT( ! mesh->parent ); // mesh->parent = nullptr;

        mesh->NavPoly::MapLoadTree( saveFile, mesh.get() );
        mesh->GetNavChildrenData( flattened_tree ); // we will need this for connecting neighbors
    }

    // ** Load the neighbors
    std::sort( std::begin( flattened_tree ), std::end( flattened_tree ) );
    for ( auto const & mesh : meshes ) {
        ASSERT( mesh );
        mesh->NavPoly::MapLoadNeighbors( saveFile, flattened_tree );
    }
}

void NavMesh::DrawVBO( void ) {
    if ( ! vbo_vert ) {
        vbo_vert.reset( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) ); //TODOWEAKPTR
        vbo_color.reset( new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY) ); //TODOWEAKPTR
    }

    if ( ! vbo_vert->Finalized() ) {
        vbo_vert->Clear();
        vbo_color->Clear();
        NavPoly::PackVBO( *vbo_vert, *vbo_color, tint );
        vbo_vert->MoveToVideoCard();
        vbo_color->MoveToVideoCard();
    }

    ASSERT( vbo_vert->Finalized() );
    ASSERT( vbo_color->Finalized() );

    glDisable( GL_DEPTH_TEST );
    shaders->UseProg(  GLPROG_GRADIENT  );
    shaders->SendData_Matrices();
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->SetAttrib( "vColor", *vbo_color );
    shaders->DrawArrays( GL_TRIANGLES, 0, vbo_vert->Num() );
    glEnable( GL_DEPTH_TEST );

    DrawQtyVBOs();
}

void NavMesh::UpdateAllVBOs( void ) {
    for ( auto & mesh : meshes )
        if ( mesh )
            mesh->UpdateVBO();
}

void NavMesh::UpdateVBO( void ) {
    if ( vbo_vert ) {
        vbo_vert->Clear();
        vbo_color->Clear();
    }

    UpdateQtyVBO();

}

std::shared_ptr< const NavPoly > NavMesh::GetSharedPtr( void ) const {
    for ( auto & mesh : meshes )
        if ( mesh.get() == this )
            return mesh;
    DIE("NavMesh should have been entered in NavMesh::meshes.\n");
    return {}; // if user wishes to not abort the program
}

std::shared_ptr< const NavPoly > NavMesh::GetPolyAt( const Vec2f& at ) const {
    if ( PointIntersects( at ) )
        return GetSharedPtr();

    return NavPoly::GetChildAt( at );
}

void NavMesh::BruteInsertEntity( const std::shared_ptr< Entity >& ent, std::vector< std::weak_ptr< NavPoly > >& new_occupied ) {
    const Vec3f ent_origin( ent->GetOrigin() );
    const Vec2f pos( ent_origin.x, ent_origin.y );
    if ( ent->bounds.IsSphere() ) {
        NavPoly::BruteInsertEntity( ent, pos, ent->bounds.GetRadius(), new_occupied );
    } else {
        NavPoly::BruteInsertEntity( ent, pos, new_occupied );
    }
}

std::shared_ptr<NavPoly> NavMesh::FindSharedPoly( const std::vector< std::weak_ptr< NavPoly > >& from_any, const std::vector< std::weak_ptr< NavPoly > >& to_any ) {
    for ( auto const & from_wptr : from_any )
        if ( auto from = from_wptr.lock() )
            for ( auto to_wptr : to_any )
                if ( from == to_wptr.lock() ) // todo: subdivision
                    return from;
    return nullptr;
}

uint NavMesh::FindPaths( const std::vector< std::weak_ptr< NavPoly > >& from_any, NavPoly::NavData& data ) {
    for ( auto const & from_wptr : from_any ) {
        if ( auto from_sptr = from_wptr.lock() ) {
            // start the work-in-progress path
            data.polies.push_back( { from_sptr } );
            data.portals.emplace_back(); // an empty list

            from_sptr->NavPoly::GetPathTo( data );
            data.checked_ids.clear(); // we're on a new starting poly, so the ids get reset.

            // remove the work-in-progress path
            data.portals.pop_back(); // discard the work-in-progress path path
            data.polies.pop_back(); // discard the work-in-progress path path
        }
    }

    if ( data.polies.size() < 1 )
        return 0;

    ASSERT( data.portals.size() > 0 );
    return data.polies.size();
}

uint NavMesh::FindShortestPathIndex( const std::vector<NavPath>& paths ) {
    auto GetSquaredPathDist=[]( const NavPath& path ) {
        float ret = 0;
        for ( uint u=0, v=1; v < path.dests.size(); ++u, ++v )
            ret += Vec3f::SquaredLen( path.dests[u] - path.dests[v] );

        return ret;
    };

    uint shortest = 0;
    float dist = GetSquaredPathDist( paths[0] );
    for ( uint i=1; i<paths.size(); ++i ) {
        float tmp_dist = GetSquaredPathDist( paths[i] );
        if ( tmp_dist < dist ) {
            dist = tmp_dist;
            shortest = i;
        }
    }
    return shortest;
}

uint NavMesh::GenerateDestinations( NavPath& path, const LinkList< Line2f>& portals ) {
    // ** generate the destinations from the middle of the portals
    path.dests.reserve( portals.Num() );
    for ( auto const & port : portals ) {
        // the middle of the portal
        path.dests.emplace_back(
            ( port.vert[0].x + port.vert[1].x ) / 2
            , ( port.vert[0].y + port.vert[1].y ) / 2
            , 0
        );
    }

    return path.dests.size();
}

void NavMesh::CopyPortalInfo( NavPath& path, const LinkList< std::shared_ptr< const NavPoly > >& polies, const LinkList< Line2f>& portals ) {
    path.polies.reserve( polies.Num() );
    for ( auto const & poly : polies )
        path.polies.emplace_back( poly );

    path.portals.reserve( portals.Num() );
    for ( auto const & poly : portals )
        path.portals.emplace_back(
            Vec3f( poly.vert[0].x, poly.vert[0].y, 0 )
            , Vec3f( poly.vert[1].x, poly.vert[1].y, 0 )
        );
}

void NavMesh::RemoveExtraStartPolys( NavPath& path, const std::vector< std::weak_ptr< NavPoly > >& from_any ) {
    // ** of all the polies we're intersecting, we start with the one that is closest to the destination
    while ( path.dests.size() > 1 ) {
        auto possible_start = path.polies[1].lock();
        if ( ! possible_start )
            break;

        if ( !ContainsObj( from_any, *possible_start ) )
            break;

        path.dests.erase( std::cbegin(path.dests) );

        // path.polies or path.portals could be empty

        if ( path.polies.size() > 0 )
            path.polies.erase( std::cbegin(path.polies) );

        if ( path.portals.size() > 0 )
            path.portals.erase( std::cbegin(path.portals) );
    }
}

NavPath NavMesh::GetPath( const Entity& from_ent, const Vec3f& to_pos, const uint max_poly_moves ) {
    std::vector< std::weak_ptr< NavPoly > > new_polies;
    const Vec2f pos_2d( to_pos.x, to_pos.y );

    for ( auto & mesh : NavMesh::meshes ) {
        if ( ! mesh )
            continue;
        mesh->GetOccupiedPolies( pos_2d, new_polies );
    }

    return GetPath( from_ent, to_pos, new_polies, max_poly_moves );
}

NavPath NavMesh::GetPath( const Entity& from_ent, const Entity& to_ent, const uint max_poly_moves ) {
    return GetPath( from_ent, to_ent.GetOrigin(), to_ent.GetOccupiedNavPolies(), max_poly_moves );
}

NavPath NavMesh::GetPath( const Entity& from_ent, const Vec3f& to_pos, const std::vector< std::weak_ptr< NavPoly > >& occupied_polies, const uint max_poly_moves ) {
    ASSERT( from_ent.bounds.IsSphere() );

    const std::vector< std::weak_ptr< NavPoly > >& to_any = occupied_polies;

    auto const from_any( from_ent.GetOccupiedNavPolies() );

    NavPoly::NavData data(to_any);
    // reserved sized are fairly arbitrary, but are safe low defaults
    data.polies.reserve( 6 );
    data.portals.reserve( 6 );
    data.checked_ids.reserve(32);
    data.actor_radius = from_ent.bounds.GetRadius();

    // add 1 for the poly we're already in, and add another because the list will always be 1 larger for the poly we're currently checking.
    // this way, max_poly_moves is the max number of poly moves to make in order to reach the dest.
    data.max_poly_moves = max_poly_moves + 2;

    // check if any poly we're already in is our destination
    if ( auto dest = FindSharedPoly( from_any, to_any ) ) {
        NavPath path;
        path.polies.emplace_back( dest );
        path.dests.emplace_back( to_pos );
        return path;
    }

    const uint num = FindPaths( from_any, data );
    if ( num < 1 )
        return {};

    std::vector<NavPath> paths;
    paths.resize(num);

    // Generate destinations
    for ( uint i=0; i<num; ++i ) {
        ASSERT( data.portals[i].Num() == data.polies[i].Num() - 1 ); // there should exist a single portal between every set of polies
        paths[i].dests.reserve( data.portals[i].Num()+2 ); // reserve an extra 2 for the start & end points ( needed by Tighten() )

        // add the start point (will be removed later)
        paths[i].dests.emplace_back( from_ent.GetOrigin() );
        paths[i].dests[ paths[i].dests.size()-1 ].z=0;

        GenerateDestinations( paths[i], data.portals[i] );
    }

    const uint shortest = FindShortestPathIndex( paths );

    CopyPortalInfo( paths[shortest], data.polies[shortest], data.portals[shortest] );

    // add the final destination
    paths[shortest].dests.emplace_back( to_pos );
    paths[shortest].dests[ paths[shortest].dests.size()-1 ].z=0;

    // tighten the pathlines
    const float EXTRA_PADDING = 0.5f;
    paths[shortest].ShortenPortals( data.actor_radius + EXTRA_PADDING );
    paths[shortest].Tighten();
    paths[shortest].dests.erase( std::cbegin(paths[shortest].dests) ); // remove the first point because it is where we are starting (Tighten() expected it)

    RemoveExtraStartPolys( paths[shortest], from_any );

    return paths[shortest];
}

std::shared_ptr< NavPoly > NavMesh::GetPolyAt( const Vec2f& at ) {
    return std::const_pointer_cast< NavPoly >( const_cast< const NavMesh* >( this )->GetPolyAt( at ) );
}

std::shared_ptr< NavPoly > NavMesh::GetSharedPtr( void ) {
    return std::const_pointer_cast< NavPoly >( const_cast< const NavMesh* >( this )->GetSharedPtr() );
}

void NavMesh::DrawVBO_AllMeshes( void ) {
    for ( auto & nav : meshes )
        if ( nav )
            nav->DrawVBO();
}

NavPath::NavPath( void )
    : dests()
    , portals()
    , polies()
    , cur_dest( 0 )
{ }

void NavPath::ShortenPortal( Vec3f& vert1, Vec3f& vert2, const float actor_radius ) const {
    // ** move the point toward the center by half of our width
    vert1.z = vert2.z = 0;

    // get unit vectors
    Vec3f toward_vert2( vert2 - vert1 );
    float len = toward_vert2.Len();
    if ( len < actor_radius || Maths::Approxf(len,0) )
        return; // portal too small to adjust verts

    toward_vert2 /= len; // normalize
    Vec3f toward_vert1( -toward_vert2 );

    // scale vectors by width
    toward_vert1 *= actor_radius;
    toward_vert2 *= actor_radius;

    // add them to arguments
    vert1 += toward_vert2;
    vert2 += toward_vert1;
}

void NavPath::ShortenPortals( const float amount ) {
    for ( auto & port : portals )
        ShortenPortal( port.vert[0], port.vert[1], amount );
}

void NavPath::Tighten( void ) {
    // If a dest can see the next-next dest through the portal between them, we can remove the dest in between

    if ( portals.size() < 1 )
        return;

    // it must pass through all portals to elliminate the midway destination
    std::size_t portals_to_check = 1;
    while ( dests.size() > 2 ) {

        for ( std::size_t p=0; p < portals_to_check && p < portals.size(); ++p ) {
            if ( !Collision2D::TestLineSeg::ToLineSeg( dests[0].ToVec2(), dests[2].ToVec2(), portals[p].vert[0].ToVec2(), portals[p].vert[1].ToVec2() ) ) {
                return;
            }
        }

        // it crossed all of them, so we can safely remove the destination between
        dests.erase( std::cbegin(dests)+1 );
        ++portals_to_check;
    }
}

void NavPath::DrawPathDebug( const Entity* from_ent ) const {
    const bool draw_all = globalVals.GetBool(gval_d_navAll);

    if ( draw_all || globalVals.GetBool(gval_d_navLines) )
        DrawLines( from_ent );

    if ( draw_all || globalVals.GetBool(gval_d_navPolies) )
        DrawPolies();

    if ( draw_all || globalVals.GetBool(gval_d_navPortals) )
        DrawPortals();
}

void NavPath::Clear( void ) {
    dests.clear();
    portals.clear();
    polies.clear();
    cur_dest = 0;
}

void NavPath::DrawLines( const Entity* from_ent ) const {
    Vec3f prev;
    if ( dests.size() > 0 ) {
        if ( from_ent ) {
            prev = from_ent->GetOrigin();
        } else {
            prev = *std::cbegin( dests );
        }

        for ( auto const & cur : dests ) {
            renderer->DrawLine( prev, cur, NAV_PATH_LINE_COLOR, NAV_PATH_LINE_COLOR );
            prev = cur;
        }
    }
}

void NavPath::DrawPolies( void ) const {
    for ( auto const & poly_wptr : polies ) {
        if ( auto poly = poly_wptr.lock() ) {
            renderer->DrawPoly( poly->GetVerts3D(), NAV_PATH_POLY_COLOR );
        }
    }
}

void NavPath::DrawPortals( void ) const {
    for ( auto const & cur : portals ) {
        renderer->DrawLine( cur, NAV_PATH_LINE_COLOR, NAV_PATH_LINE_COLOR );
    }
}

Vec3f NavPath::GetCurDest( void ) const {
    ASSERT( cur_dest < dests.size() );
    if ( cur_dest >= dests.size() )
        return {};
    return dests[cur_dest];
}

int NavPoly::GetIndexOfVert( const Vec2f& vert ) const {
    for ( uint v=0; v<verts.size(); ++v )
        if ( Maths::Approxf( NavMesh::vertex_pool[ verts[v] ], vert ) )
            return static_cast<int>(v);
    return -1;
}

std::weak_ptr< const NavPoly > NavPath::GetCurPoly( void ) const {
    if ( polies.size() < 1 )
        return {};
    return polies[0];
}

Line3f NavPath::GetFirstPortal( void ) const {
    return portals[0];
}

bool NavPath::HasAPortal( void ) const {
    return portals.size() > 0;
}

uint NavPath::GetNumDests( void ) const {
    return dests.size();
}

void NavPath::PrepareNextDest( void ) {
    ++cur_dest;
}

bool NavPath::HasCurDest( void ) const {
    return cur_dest < dests.size();
}
