// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_NEW_NAV_H
#define SRC_NEW_NAV_H

extern const bool NAVMESH_DEBUG;

/*

The NavMesh system is a 2-dimensional model composed only of triangles.
The nav polies are in a parent/child heirarchy. loops and such can be created
by merging vertices. Two tris with two shared vertices that are not parent and
child will be come "neighbors." Neighbors, Children, and Parents are not the same
relationship.

In the future, we hope to subdivide individual nav polies by the objects inside of them
in order to allow for dynamic obstacle avoidance. this will be accomplished by making
another NavMesh inside of the poly itself. This is done only at run-time and the editor
is oblivious to it.

*/

#include "../base/main.h"

extern const Color4f DEFAULT_NAV_EXTRUSION_PREVIEW_COLOR;
extern const float SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT;
extern const float SELECTION_DIST_FROM_CLICKPOS_TO_NAVPOLY_VERT_SQ;

class NavMesh;
class NavPath;

template< typename TYPE >
class VBO;

/*

NavPoly

*/

//!< an extrusion from two points to form a triangle
class NavPoly {
    friend class NavMesh;//todoattorney?

    struct NavData {
        NavData( void ) = delete;
        NavData( const std::vector< std::weak_ptr<NavPoly> >& to_any_of )
            : any_of( to_any_of )
            , polies()
            , portals()
            , checked_ids()
            , actor_radius(0)
            , max_poly_moves()
        {}

        std::vector< std::weak_ptr<NavPoly> > any_of;
        std::vector< LinkList< std::shared_ptr< const NavPoly > > > polies;
        std::vector< LinkList< Line2f> > portals;
        std::vector<uint> checked_ids;
        float actor_radius; //!< for testing if actor is too narrow for a poly
        uint max_poly_moves;
    };

    struct PolyData {
        PolyData( const uint _unique_id, const std::shared_ptr< NavPoly >& _poly ) : unique_id( _unique_id ), poly( _poly ) { }
        bool operator<( const PolyData& other ) const { return unique_id < other.unique_id; }
        bool operator<=( const PolyData& other ) const { return unique_id <= other.unique_id; }
        bool operator>( const PolyData& other ) const { return unique_id > other.unique_id; }
        bool operator>=( const PolyData& other ) const { return unique_id >= other.unique_id; }
        bool operator==( const PolyData& other ) const { return unique_id == other.unique_id; }
        bool operator!=( const PolyData& other ) const { return unique_id != other.unique_id; }

        bool operator<( const unsigned int other_id ) const { return unique_id < other_id; }
        bool operator<=( const unsigned int other_id ) const { return unique_id <= other_id; }
        bool operator>( const unsigned int other_id ) const { return unique_id > other_id; }
        bool operator>=( const unsigned int other_id ) const { return unique_id >= other_id; }
        bool operator==( const unsigned int other_id ) const { return unique_id == other_id; }
        bool operator!=( const unsigned int other_id ) const { return unique_id != other_id; }

        uint unique_id;
        std::shared_ptr< NavPoly > poly;
    };

private:
    NavPoly( void ); //!< to be called only by this class
public:
    NavPoly( const NavPoly& other ) = delete;
    NavPoly( NavMesh* _master, NavPoly* _parent );
    virtual ~NavPoly( void );

public:
    NavPoly& operator=( const NavPoly& other ) = delete;

public:
#ifdef MONTICELLO_EDITOR
    /*! creates a new neighbor. the edge chosen for the extrusion will be the one in which the point is in
        front of and closest to. an edge that already connects to a poly cannoy be extruded a second time. */
    std::shared_ptr<NavPoly> Extrude( const Vec2f& to );
    std::shared_ptr<NavPoly> ExtrudeFromEdge( const Vec2f& to, const uint edge );

    //! draws a preview of what will happen if we call Extrude with the same value
    bool Extrude_Preview( const Vec2f& to ) const;
    Vec2f GetVertFromPool( const std::size_t index ) const;
#endif // MONTICELLO_EDITOR

    uint GetID( void ) const { return id; }

    bool operator==( const NavPoly& other ) const { return other.id == id; }
    bool operator!=( const NavPoly& other ) const { return !operator==( other ); }

    void PackVBO( VBO<float>& vbo_vert, VBO<float>& vbo_color, const Color4f& tint );
    void UpdateQtyVBO( void );
    bool PointIntersects( const Vec2f& to ) const;
    bool CircleIntersects( const Vec2f& to, const float radius ) const;
    std::shared_ptr<NavPoly> GetChildAt( const Vec2f& pos ) const;
    Vec2f GetVert( const std::size_t index ) const;
    uint NumVerts( void ) const;
    int GetIndexOfVert( const Vec2f& vert ) const; //!< returns -1 if not found
    std::vector< Vec2f > GetVerts( void ) const;
    std::vector< Vec3f > GetVerts3D( void ) const;

    std::size_t GetVertIndex( const std::size_t index ) const { return verts[index]; }

    void NudgeVert( const std::size_t index, const Vec2f& nudge );
    void NudgeVert_Preview( const Vec2f& click_pos );

    std::vector<std::size_t> GetIndicesForDeform( const Vec2f& at ) const;
    std::vector<std::size_t> GetAllVertIndicesBehindPoint( const Vec2f& at ) const; //!< return the indices of the edges (ie, 0 for edge[0])
    int GetEdgeBehindPoint( const Vec2f& at ) const; //!< get the edge behing the point that is closest to it
    std::vector<std::size_t> GetAllVertsBehindPoint( const Vec2f& at ) const; //!< return the vertex indices, ie the value at edge[x]

    void AppendVert( const Vec2f& vert ); //!< this just tacks on a vertex without doing anything special ( for initial poly construction ). it is up to programmer to ensure the vertexes are appended in clockwise order
    bool AppendVertFromPool( const uint vertpool_index ); //!< add a vert and set it's value (index reference from vert_pool)
    bool InsertVertAfterIndex( const int index ); //!< insert a new vert into the list, whose value is right in the middle of the edge
    bool InsertVertAfterIndex( const int index, const Vec2f& vert ); //!< insert a new vert into the list
    bool InsertVertOnEdge( const Vec2f& click_pos );
    bool InsertVertOnEdge_Preview( const Vec2f& click_pos ) const;
private:
    bool Get_InsertVertOnEdge_Pos( const Vec2f& click_pos, Vec2f& pos_out ) const;
    bool HasEdge( const std::size_t a1, const std::size_t b1 ) const; //!< reutrns whether this poly has an edge with the given vertex indices

    const NavPoly* GetParent( void ) const { return parent; }
    const LinkList< std::shared_ptr<NavPoly> > GetChildren_Ref( void ) const { return children; }

    int GetSharedEdge( const NavPoly& with ) const; //! returns the first vertex index of the edge of this poly (not the one passed). that index+1 (or 0 if it's > verts.size()) is the next vertex index. Returns -1 if none

public:
#ifdef MONTICELLO_EDITOR
    bool ChildOrParentContainsVertIndex( const std::size_t index ) const;
    static void MergeVert( const std::size_t cur_index, const std::size_t merge_index ); //!< cur_index becomes merge_index, cur_index goes away completely
#endif // MONTICELLO_EDITOR

public:
    bool HasPortalOnEdge( const std::size_t edge_index_a, const std::size_t edge_index_b ) const;

private:
    bool HasNeighbor( const NavPoly& poly ) const;
    
    bool HasNeighborOnEdge( const std::size_t edge_index_a, const std::size_t edge_index_b ) const;
    bool HasChildOnEdge( const std::size_t edge_index_a ) const;

public:
    static bool GetNeighboringPoliesWithinAASquare( const std::shared_ptr< const NavPoly >& poly, std::vector< std::shared_ptr< const NavPoly > >& polies_out, const AASquare2v& aaSquare );
    uint NumEntities( void ) const;

private:
    void VertInserted( const uint at_index );

private:
#ifdef MONTICELLO_EDITOR
    static void VertexMerge_Helper( NavPoly& poly, const std::size_t cur_index, const std::size_t merge_index );
    void FindNewNeighborsAtVertIndex( const std::size_t index ); //!< returns false if there are less than two verts to connect them
    void TryToBecomeNeighborsWithAllShareresOfIndex( NavPoly& with, const std::size_t index ); //!< returns false if there are less than two verts to connect them
    bool TryToBecomeNeighbors( NavPoly& with ); //!< returns false if there are less than two verts to connect them
#endif // MONTICELLO_EDITOR

    void MapSaveTree( FileMap& saveFile ) const;
    void MapLoadTree( FileMap& saveFile, NavMesh* _master );
    void MapSaveNeighbors( FileMap& saveFile ) const;
    void MapLoadNeighbors( FileMap& saveFile, const std::vector<PolyData> & linear_polies );
    void GetNavChildrenData( std::vector< PolyData >& flattened_tree );

public:
    NavMesh* GetMaster( void ); //!< the master is the highest parent up the heirarchy

protected:
#ifdef MONTICELLO_EDITOR
    int GetUnusedEdgeNear( const Vec2f& click_pos ) const; //!< returns the beginning vert index for the edge, or -1 for failure
#endif // MONTICELLO_EDITOR

    virtual std::shared_ptr< NavPoly > GetSharedPtr( void );
    virtual std::shared_ptr< const NavPoly > GetSharedPtr( void ) const;

    void GetPathTo( NavData& data ) const;
    void GetPathTo_Helper( const std::shared_ptr<const NavPoly>& poly, NavData& data ) const;

    std::shared_ptr< NavPoly > GetChild( const NavPoly& poly ) const; //!< returns the shared_ptr to the child if it exists

    int GetEdgeClosestTo( const Vec2f& to ) const; //! returns the first vertex index of the edge of this object that is closest to the point. Returns -1 if none
    bool AddPortal( const NavPoly& with, LinkList< Line2f >& portals_out, const float actor_radius = 0 ) const; //! returns false if there is no shared edge or the path is too small

    void GetOccupiedPolies( const Vec2f& pos, std::vector< std::weak_ptr< NavPoly > >& new_occupied ); //!< same as BruteInsertEntity except doesn't insert
    void BruteInsertEntity( const std::shared_ptr< Entity >& ent, const Vec2f& pos, std::vector< std::weak_ptr< NavPoly > >& new_occupied ); //!< insert entity and sets new_occupied to the currently occupied polies

    void GetOccupiedPolies( const Vec2f& pos, const float radius, std::vector< std::weak_ptr< NavPoly > >& new_occupied ); //!< same as BruteInsertEntity except doesn't insert
    void BruteInsertEntity( const std::shared_ptr< Entity >& ent, const Vec2f& pos, const float radius, std::vector< std::weak_ptr< NavPoly > >& new_occupied ); //!< insert entity and sets new_occupied to the currently occupied polies
    
public:

    std::vector< std::weak_ptr< Entity > > GetOccupyingEnts( void ) const;
    static std::vector< std::weak_ptr< NavPoly > > ReinsertEntity( const std::shared_ptr< Entity >& ent, const Vec2f& pos, const float radius, const std::vector< std::weak_ptr< NavPoly > >& cur_occupied ); //!< returns the new list of occupied NavPolys
    static bool ReinsertEntity_Helper( const std::shared_ptr< Entity >& ent, const Vec2f& pos, const float radius, std::vector< std::weak_ptr< NavPoly > >& new_occupied, const std::shared_ptr< NavPoly >& poly );

protected:
    LinkList< std::shared_ptr<NavPoly> > children; //!< if master is null (meaning we are the master), the first item in this list is our parent
    LinkList< std::weak_ptr<NavPoly> > neighbors; //!< NavPolies we are connected to but are not parent/child of/to
    // vertex indices 0 and 1 are shared with the edge of the parent from which we were extruded
    std::vector< std::size_t > verts; //!< stored counter-clockwise
protected:
    void DrawQtyVBOs( void );
    void DrawQtyVBO( void ); //!< Draw the name of the entity
    void pack_and_send_qty_vbo( void ) const;
private:
    std::unique_ptr< VBO<float> > vbo_qty;
    std::unique_ptr< VBO<float> > vbo_qty_uv;

    NavMesh* master; //!< will be valid throughout the lifetime of this child (if we are not master)
    NavPoly* parent; //!< will be valid throughout the lifetime of this child (if we are not master)
    std::vector< std::weak_ptr< Entity > > occupying_entities;
    uint id; //! each poly has a unique identifier
    std::vector<std::size_t> edges_with_children;
};

class NavMesh : public NavPoly {
    friend class NavPoly;//todoattorney?
public:
    NavMesh( void );
    NavMesh( const NavMesh& other ) = delete;
    ~NavMesh( void ) override;

public:
    NavMesh& operator=( const NavMesh& other ) = delete;

private:
    void DrawVBO( void );
    void UpdateVBO( void );
public:
    static void DrawAllVBOs( void );
    static void UpdateAllVBOs( void );
    std::shared_ptr< NavPoly > GetPolyAt( const Vec2f& at );
    std::shared_ptr< const NavPoly > GetPolyAt( const Vec2f& at ) const;

    void BruteInsertEntity( const std::shared_ptr< Entity >& ent, std::vector< std::weak_ptr< NavPoly > >& new_occupied );

private:
    static NavPath GetPath( const Entity& from_ent, const Vec3f& to_pos, const std::vector< std::weak_ptr< NavPoly > >& occupied_polies, const uint max_poly_moves );
public:
    static NavPath GetPath( const Entity& from_ent, const Entity& to_ent, const uint max_poly_moves ); //!< this is slower than calling GetPath with an entity instead od a vec3f. This is because the game has to find out what poly the point is in.
    static NavPath GetPath( const Entity& from_ent, const Vec3f& to_pos, const uint max_poly_moves ); //!< this is slower than calling GetPath with an entity instead od a vec3f. This is because the game has to find out what poly the point is in.
    static void DrawVBO_AllMeshes( void );

    bool operator==( const NavMesh& other ) const { return &other == this; }
    bool operator!=( const NavMesh& other ) const { return !operator==(other); }

private:
    //! each poly has a unique identifier
    static uint GenerateNewID( void ) { return next_id++; }
    std::shared_ptr< NavPoly > GetSharedPtr( void ) override;
    std::shared_ptr< const NavPoly > GetSharedPtr( void ) const override;

    void PackVBO( void );

private:
    static std::shared_ptr<NavPoly> FindSharedPoly( const std::vector< std::weak_ptr< NavPoly > >& from_any, const std::vector< std::weak_ptr< NavPoly > >& to_any );
    static uint FindPaths( const std::vector< std::weak_ptr< NavPoly > >& from_any, NavPoly::NavData& data ); //!< returns how many paths were found
    static uint FindShortestPathIndex( const std::vector<NavPath>& paths );
    static uint GenerateDestinations( NavPath& path, const LinkList< Line2f>& portals );
    static void CopyPortalInfo( NavPath& path, const LinkList< std::shared_ptr< const NavPoly > >& polies, const LinkList< Line2f>& portals );
    static void RemoveExtraStartPolys( NavPath& path, const std::vector< std::weak_ptr< NavPoly > >& from_any );

private:
    void MapSaveTree( FileMap& saveFile ) const;
    void MapLoadTree( FileMap& saveFile );
public:
    static void MapSaveAll( FileMap& saveFile );
    static void MapLoadAll( FileMap& saveFile );
public:
    static LinkList< std::shared_ptr< NavMesh > > meshes;
    static int GetVertexFromPoolIndexAt( const Vec2f& at, const std::vector<std::size_t>& but_none_of = {} ); //!< returns negative if not found

private:
    std::unique_ptr< VBO<float> > vbo_vert;
    std::unique_ptr< VBO<float> > vbo_color;
    static uint next_id;
    static std::vector< Vec2f > vertex_pool;
    Color4f tint;
};

/*!

NavPath \n\n

*/

class NavPath {
    friend class NavMesh;//todoattorney?

public:
    NavPath( void );

public:
    void DrawPathDebug( const Entity* from_ent = nullptr ) const;
    void Clear( void );
    void Tighten( void );
    void ShortenPortals( const float amount );

private:
    void ShortenPortal( Vec3f& vert1, Vec3f& vert2, const float actor_radius ) const;
    void DrawLines( const Entity* from_ent = nullptr ) const;
    void DrawPolies( void ) const;
    void DrawPortals( void ) const;

public:
    uint GetNumDests( void ) const;
    Vec3f GetCurDest( void ) const;
    bool HasCurDest( void ) const;
    void PrepareNextDest( void );
    std::weak_ptr< const NavPoly > GetCurPoly( void ) const;

    bool HasAPortal( void ) const;
    Line3f GetFirstPortal( void ) const;

private:
    std::vector< Vec3f > dests; //!< the positions we will stop at from start to end.
    std::vector< Line3f > portals; //!< the windows which we will pass through from start to end.
    std::vector< std::weak_ptr< const NavPoly > > polies; //!< the polies we will visit from start to end.
    std::size_t cur_dest; // current index of dests
};

#endif // SRC_NEW_NAV_H
