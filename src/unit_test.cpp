// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./base/main.h"
#include "./unit_test.h"
#include "./math/collision/collision.h"

#ifdef CLIB_UNIT_TEST

void UnitTest( void ) {
    CLibUnitTest();
    Collision2D::UnitTest();
    Collision3D::UnitTest();
    
    Cone3D::UnitTest();
    
    // COLLISION FAILURE CASE
    const Vec3f line_start( 0.4481f, 0.0972f, 1.0f );
    const Vec3f line_end( 0.4481f, 3.097f, 1.0f );
    const Sphere sphere( Vec3f( 0.0f, -0.8719f, 1.0f ), 1.0f);
    const Vec3f sphere_dest( 0.0f, -0.7439f, 1.0f );
    CollisionReport3D report;
    if ( Sphere::SweepToLine( report, sphere, sphere_dest, LineTypeT::SEG, line_start, line_end ) ) {
        ASSERT_MSG( false, "HEY! YOU FIXED IT!" ); // ASSERT( placed here so we'll know
    }

}

#endif // CLIB_UNIT_TEST
