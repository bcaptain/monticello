// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_BASE_TIME_H_
#define SRC_BASE_TIME_H_
// Since SDL_GetTicks() returns miliseconds in the form of unsigned int, that is the
// data type we will be using as the base for time.

//! Seconds
constexpr unsigned long long int operator "" _s ( long double seconds ) {
    return static_cast< unsigned long long int >( seconds * 1000 );
}

//! Seconds (floating point)
constexpr float operator "" _sf ( long double seconds ) {
    return static_cast< float >( seconds * 1000 );
}

//! Seconds
constexpr unsigned long long int operator "" _s ( unsigned long long int seconds ) {
    return seconds * 1000;
}

//! Seconds (floating point)
constexpr float operator "" _sf ( unsigned long long int seconds ) {
    return static_cast< float >( seconds * 1000 );
}

//! Miliseconds
constexpr unsigned long long int operator "" _ms ( unsigned long long int miliseconds ) {
    return miliseconds;
}

//! Miliseconds (floating point)
constexpr float operator "" _msf ( unsigned long long int miliseconds ) {
    return static_cast< float >( miliseconds );
}

#include "./main.h"
#include "../sdl/sdl.h"

extern const float FPS_CAP;
extern const float FPSCAP_OVER_THOUSAND;
extern const float MS_PER_FRAME;
extern const uint ONE_QUARTER_SECOND_U;
extern const float ONE_QUARTER_SECOND_F;

extern const float EP_ONE_CM_PER_FRAME;
extern const float EP_ONE_CM_PER_FRAME_SQ;
extern const float EP_HALF_CM_PER_FRAME;
extern const float EP_HALF_CM_PER_FRAME_SQ;

// ****
//
// Scheduler
// handles game time, timers, FPS, etc
//
// ****

class Scheduler {

public:
    Scheduler( void );
    ~Scheduler( void ) = default;

public:
    void CalcFPS( void );

    void UpdateFrame( void );
    bool TimeForNextFrame( void ) const;

    float GetFPS( void ) const;

    Uint32 GetRenderFrame( void ) const; //!< render_frame does not stop incrementing when paused, unlike game_frame
    Uint32 GetGameFrame( void ) const; //!< game_frame stops incrementing when paused, unlike rander_frame
    void ResetGameTime( void );

    Uint32 GetGameTime( void ) const;
    Uint32 GetRenderTime( void ) const;
    Uint32 GetRealTime( void ) const;
    void IncrementGameFrame( void );

private:
    float fps;
    Uint32 cur_frame_count;
    Uint32 prevFrameTime;
    Uint32 game_frame; //!< stops incrementing when paused, unlike rander_frame
    Uint32 render_frame; //!< does not stop incrementing when paused, unlike game_frame
};

void GetDateString( std::string& out );

#endif  // SRC_BASE_TIME_H_
