// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_BASE_MAIN_H_
#define SRC_BASE_MAIN_H_
#include <cassert>
#include <vector>
#include <bitset>
#include <memory>
#include <mutex>
#include <atomic>
#include <string>

#include "./clib/src/warnings.h"
#include "../types/types.h"
#include "./clib/src/clib.h"
#include "../local.h"
#include "../globalConsts.h"

extern Dict globalVals;

void FreeGlobals( void );
void ProgramStart( void );
int GetWindowsArgs( char**& argv );
void ProcessStartupArgs( const char *const * strings, const int num ); //!< processed immediately after console is initialized
void ProcessPostStartupArgs( const char *const * strings, const int num ); //!< processed immediately after game is ready (ie, to load a map)

void LogOperatingSystem();
void LogTotalRAM();
void LogCPUInfo();

#include "../saveFile.h" // used in many places

#endif  // SRC_BASE_MAIN_H_

