// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <ctime>
#include "./clib/src/warnings.h"
#include "./time.h"
#include "../game.h"

const float FPS_CAP = 60.0f;
const float FPSCAP_OVER_THOUSAND = FPS_CAP/1_s;
const float MS_PER_FRAME = 1_sf/FPS_CAP;
const uint ONE_QUARTER_SECOND_U = 1_sf/4;
const float ONE_QUARTER_SECOND_F = 1_sf/4;

const float EP_ONE_CM_PER_FRAME = EP_ONE_CENTIMETER / FPS_CAP;
const float EP_ONE_CM_PER_FRAME_SQ = EP_ONE_CM_PER_FRAME * EP_ONE_CM_PER_FRAME;
const float EP_HALF_CM_PER_FRAME = EP_HALF_CENTIMETER / FPS_CAP;
const float EP_HALF_CM_PER_FRAME_SQ = EP_HALF_CM_PER_FRAME * EP_HALF_CM_PER_FRAME;

Scheduler::Scheduler( void )
    : fps(0)
    , cur_frame_count(0)
    , prevFrameTime(0)
    , game_frame(1) // we start frame out at frame 1 because a timestamp of 0 is considered by the asset managers as "not loaded"
    , render_frame(1)
{ }

void Scheduler::UpdateFrame( void ) {
    // govern
    while ( ! TimeForNextFrame() ) {
        // continue
    }

    prevFrameTime = SDL_GetTicks();
    ++cur_frame_count;
    ++render_frame;
}

void Scheduler::CalcFPS( void ) {
    static Uint32 lastFPSCalc = 0;
    const Uint32 curTime_ms = SDL_GetTicks();
    const Uint32 time_since_last_frame_ms = SDL_GetTicks() - lastFPSCalc;

    if ( time_since_last_frame_ms == 0 )
        return;

    fps = 1_sf / static_cast< float >( time_since_last_frame_ms );
    fps *= static_cast< float >( cur_frame_count );
    cur_frame_count = 0;
    lastFPSCalc = curTime_ms;
}

void GetDateString( std::string& out ) {
    time_t rawtime;
    time( &rawtime );
    const std::size_t ct_size = 32;
    char ct[ct_size];
    strftime( ct, ct_size-1, "%c", localtime( &rawtime ) );
    // todo: For threadsafe applications it is recommended to use the reentrant replacement function 'localtime_r
    // todo: localtime_r - is it portable?
    // todo: localtime_s - look into this as well
    // todo: https://stackoverflow.com/questions/7313919/c11-alternative-to-localtime-r
    out = ct;
}

float Scheduler::GetFPS( void ) const {
    return fps;
}

bool Scheduler::TimeForNextFrame( void ) const {
    return SDL_GetTicks() - prevFrameTime >= MS_PER_FRAME;
}

Uint32 Scheduler::GetRenderFrame( void ) const {
    return render_frame;
}

void Scheduler::IncrementGameFrame( void ) {
    ++game_frame;
}

void Scheduler::ResetGameTime( void ) {
    game_frame = 1; // we start frame out at frame 1 because a timestamp of 0 is considered by the asset managers as "not loaded"
}

Uint32 Scheduler::GetGameFrame( void ) const {
    return game_frame;
}

Uint32 Scheduler::GetGameTime( void ) const {
    return static_cast<Uint32>( MS_PER_FRAME * GetGameFrame() );
}

Uint32 Scheduler::GetRenderTime( void ) const {
    return static_cast< Uint32 >( MS_PER_FRAME * GetRenderFrame() );
}

Uint32 Scheduler::GetRealTime( void ) const {
    return static_cast< Uint32 >( SDL_GetTicks() );
}
