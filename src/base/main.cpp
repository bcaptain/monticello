// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifdef __linux
    #include <sys/utsname.h> // for uname()
    #include <sys/types.h> // for sysinfo
    #include <sys/sysinfo.h> // for sysinfo
#endif // __linux

#include "./main.h"

#include "../game.h"
#include "../unit_test.h"

#include "../rendering/renderer.h"
#include "../editor/editor.h"
#include "../console.h"
#include "../commandsys.h"
#include "../messages.h"

/* profiling code
                #include <iostream>
                #include <string>
                #include <unordered_map>
*/

Game *game=nullptr; // global accessor
Dict globalVals; // scaleme: set to a decent size for how many we'll be using

#ifdef _WIN32
    // caller is responsible for freeing argv before AND after using this function.
    int GetWindowsArgs( char**& argv ) {
        // ** use Windows API to get the Unicode strings, and convert them to ASCII

        int argc_w;
        wchar_t** argv_w = CommandLineToArgvW( GetCommandLineW(), &argc_w );

        argv = new char*[argc_w];
        ASSERT( argv );
        int argc;

        for ( argc=0; argc < argc_w; ++argc ) {
            std::size_t tmp = wcslen( argv_w[argc] );
            ++tmp; // for the null terminator
            argv[argc] = new char[tmp];
            wcstombs( argv[argc], argv_w[argc], tmp );
        }
        LocalFree( argv_w );

        return argc;
    }
#endif // _WIN32

    void monticello_new_handler();
    void monticello_new_handler() {
        std::set_new_handler(nullptr);
        DIE( "Unable to allocate memory.\n" );
    }

#ifdef _WIN32
    int WINAPI WinMain (HINSTANCE, HINSTANCE, LPSTR, int) {
#else

    int main( int argc, char *argv[] ) {
#endif //_WIN32

    // ** Messages created before console is initialized will be cached and displayed afterward
    error_hook = &Messages::Error;
    warning_hook = &Messages::Warning;
    message_hook = &Messages::Message;
    
    std::set_new_handler( monticello_new_handler );

    // set up global argument values so we can check them later
    #ifdef _WIN32
        Arg::arg_num = GetWindowsArgs( Arg::args );
    #else
        Arg::args = argv;
        Arg::arg_num = argc;
    #endif //_WIN32

    #ifdef CLIB_UNIT_TEST
        UnitTest();
    #endif // CLIB_UNIT_TEST

    ProgramStart();

    InitEntityTypes();

    CommandSys::Init();

    game = new Game;
    game->Init();
    game->MainLoop();
    game->Deinit();
    DELNULL( game );

    FreeGlobals();

    #ifdef _WIN32
        DELNULLARRAY( Arg::args );
    #endif //_WIN32
    
    #ifdef CLIB_DEBUG
        //Printf("press key to continue...\n");
        //std::getchar(); // pause before exit when debugging
    #endif // CLIB_DEBUG
    
    return 0;
}

void FreeGlobals( void ) {
    DELNULL( editor );
    DELNULL( console );
    DELNULL( renderer );
}

void ProgramStart( void ) {
    ClearLog();
    LOG("Starting Monticello, Mark %s, Version %s\n", MONTICELLO_MARK.c_str(), MONTICELLO_VERSION.c_str() );

    std::string today;
    GetDateString( today );
    LOG("Todays Date: %s\n", today.c_str() );

    LogOperatingSystem();
    LOG("Compiled by: %s\n", TARGET_ARCH );
    LogTotalRAM();
    LogCPUInfo();
}

void LogOperatingSystem( void ) {
    #ifdef __linux
        LOG("OS: Linux 32bit\n");

        // ** http://linux.die.net/man/2/uname
        utsname info;
        if ( uname( &info ) != 0 ) {
            WARN("Execution of uname() failed\n");
            LOG("Linux Release: Undetermined\n");
            LOG("Linux Version: Undetermined\n");
            LOG("Linux Machine: Undetermined\n");
        } else {
            LOG("Linux Release: %s\n", info.release);
            LOG("Linux Version: %s\n", info.version);
            LOG("Linux Machine: %s\n", info.machine);
        }

        File issue("/etc/issue", "rb");
        ParserText parser( issue );
        if ( !issue.IsOpen() ) {
            LOG("Couldn't determine distribution (could not open /etc/issue).\n");
        } else {
            std::string distro;
            parser.ReadUnquotedString( distro );
            LOG( "Linux Distro: %s\n", distro.c_str() );
            issue.Close();
        }
    #elif defined( _WIN32 )
        // ** http://msdn.microsoft.com/en-us/library/ms724833%28VS.85%29.aspx
        OSVERSIONINFO version_info;
        memset(&version_info, 0, sizeof( version_info ) );
        version_info.dwOSVersionInfoSize = sizeof( version_info );
        GetVersionEx( &version_info );
        if ( !version_info.dwPlatformId == VER_PLATFORM_WIN32_NT ) {
            LOG("OS: Unknown Windows Non-NT\n");
        } else {
            switch ( version_info.dwMajorVersion ) {
                case 6:
                    switch ( version_info.dwMinorVersion ) {
                        case 3:
                            LOG("OS: Windows 8.1\n");
                            break;
                        case 2:
                            LOG("OS: Windows 8\n");
                            break;
                        case 1:
                            LOG("OS: Windows 7\n");
                            break;
                        case 0:
                            LOG("OS: Windows Vista\n");
                            break;
                        default:
                            LOG("OS: Unknown Windows Vista or higher\n");
                            break;
                    }
                    break;
                case 5:
                    switch ( version_info.dwMinorVersion ) {
                        case 2:
                            LOG("OS: Windows XP Professional x64 Edition\n");
                            break;
                        case 1:
                            LOG("OS: Windows XP\n");
                            break;
                        case 0:
                            LOG("OS: Windows 2000\n");
                            break;
                        default:
                            LOG("OS: Unknown Windows between 2000 and Vista\n");
                            break;
                    }
                    break;
                default:
                    LOG("OS: Unknown Windows NT or higher\n");
                    break;
            }
            LOG(" (version: %i.%i)\n", version_info.dwMajorVersion, version_info.dwMinorVersion );
        }
    #else // platform
        #error "Unknown platform"
    #endif // platform
}

void LogCPUInfo( void ) {
    // ** first, CPUID

    CPUID_Helper cpuid;
    cpuid.Go();

    LOG( "CPUID Vendor: %s\n", cpuid.vendor );
    LOG( "CPUID Signature: %x\n", cpuid.signature );
    LOG( "CPUID Stepping: %u\n", cpuid.stepping );
    LOG( "CPUID Model: %u\n", cpuid.model );
    LOG( "CPUID Family: %u\n", cpuid.family);
    LOG( "CPUID Processor Type: %u\n", cpuid.type );
    LOG( "CPUID Features EBX: %u\n", cpuid.features_ebx );
    LOG( "CPUID Features ECX: %u\n", cpuid.features_ecx );
    LOG( "CPUID Features EDX: %u\n", cpuid.features_edx );

    // then other means

    #ifdef __linux
        // ** for linux, we're just gonna spit out the contents of /proc/cpu

        FILE *cpuinfo = popen("/bin/cat /proc/cpuinfo", "r");
        if ( !cpuinfo ) {
            WARN("Could not get cpu info\n");
        } else {
            LOG("BEGIN /proc/cpuinfo\n");

            const uint buf_size = 512;
            char buf[buf_size];

            uint bytes_read = fread( buf, 1, buf_size, cpuinfo );
            std::string content;
            if ( bytes_read > 0 ) {
                do {
                    for ( uint i=0; i<bytes_read; ++i )
                        content += buf[i];
                    bytes_read = fread( buf, 1, buf_size, cpuinfo );
                } while ( bytes_read > 0 );
            }
            LOG(content);

            LOG("\nEND /proc/cpuinfo\n");

            pclose( cpuinfo );
        }

    #elif defined( _WIN32 )

        // ** for windows, it's all over the place

        SYSTEM_INFO sysinfo;
        memset(&sysinfo, 0, sizeof( sysinfo ) );
        GetSystemInfo( &sysinfo );
        std::string cpu;
        switch ( sysinfo.dwProcessorType ) {
            case PROCESSOR_ARCHITECTURE_AMD64:
                cpu = "x64 (AMD or Intel)";
                break;
            case PROCESSOR_ARCHITECTURE_INTEL:
                cpu = "x86";
                break;
            case PROCESSOR_ARCHITECTURE_PPC:
                cpu = "PPC";
                break;
            case PROCESSOR_ARCHITECTURE_UNKNOWN:
                cpu = "Unknown";
                break;
            default:
                cpu = static_cast< int >( sysinfo.dwProcessorType ); // the value itself will give us some info. (and it may actually be 586)
                break;
        };

        LOG("CPU Arch: %s\n", cpu.c_str());
        LOG("CPU Cores: %i\n", sysinfo.dwNumberOfProcessors ); // Not necessarily accurate, this may include hyperthreads, etc.

        HKEY hKey;
        if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0", 0, KEY_READ, &hKey) != 0 ) {
            WARN("Could not determine CPU frequency.\n");
            LOG("CPU Frequency: Undetermined\n");
        } else {
            unsigned long int dwMHz;
            unsigned long int BufSize = sizeof( dwMHz );
            RegQueryValueEx(hKey, "~MHz", NULL, NULL, reinterpret_cast< uchar* >( &dwMHz ) /* is this REALLY how Windows does things? */, &BufSize);
            LOG("CPU Frequency: %u\n", dwMHz);

            const unsigned long int str_size = 256;
            uchar str[str_size];
            unsigned long int str_size_send = str_size;

            RegQueryValueEx(hKey, "VendorIdentifier", NULL, NULL, str, &str_size_send);
            LOG("CPU Vendor ID: %s\n", reinterpret_cast<const char *>(str));

            str_size_send = str_size;
            RegQueryValueEx(hKey, "ProcessorNameString", NULL, NULL, str, &str_size_send);
            LOG("CPU Name: %s\n", reinterpret_cast<const char *>(str));

            str_size_send = str_size;
            RegQueryValueEx(hKey, "Identifier", NULL, NULL, str, &str_size_send);
            LOG("CPU Identifier: %s\n", reinterpret_cast<const char *>(str));
        }

    #else // platform
        #error "Unknown platform"
    #endif // platform
}

void LogTotalRAM( void ) {
    #ifdef __linux

        struct sysinfo memInfo;
        sysinfo( &memInfo );

        LOG("RAM Total Physical: %u\n", static_cast< Uint64 >( memInfo.totalram ) * memInfo.mem_unit );
        LOG("RAM Total Swap: %u\n", static_cast< Uint64 >( memInfo.totalswap ) * memInfo.mem_unit );

        LOG("RAM Avail Physical: %u\n", static_cast< Uint64 >( memInfo.freeram ) * memInfo.mem_unit );
        LOG("RAM Avail Swap: %u\n", static_cast< Uint64 >( memInfo.freeswap ) * memInfo.mem_unit );

    #elif defined( _WIN32 )

        MEMORYSTATUS status;
        status.dwLength = sizeof(status);
        GlobalMemoryStatus( &status );

        LOG("RAM Total Physical: %u\n", status.dwTotalPhys );
        LOG("RAM Total Virtual: %u\n", status.dwTotalVirtual );
        LOG("RAM Total Pagefile: %u\n", status.dwTotalPageFile );

        LOG("RAM Avail Physical: %u\n", status.dwAvailPhys );
        LOG("RAM Avail Virtual: %u\n", status.dwAvailVirtual );
        LOG("RAM Avail Pagefile: %u\n", status.dwAvailPageFile );
    #else // platform
        #error "Unknown platform"
    #endif // platform
}

void ProcessStartupArgs( [[maybe_unused]] const char *const * strings, [[maybe_unused]] const int num ) {
}

void ProcessPostStartupArgs( const char *const * strings, const int num ) {
    if ( num < 0 )
        return;

    uint argnum = static_cast< uint >( num );
    std::vector< std::string > cmd;

    const char* arg_map = "-map";
    const int arg_map_params = 1;

    const char* arg_force_map = "-force_map";
    const int arg_force_map_params = 1;

    const char* arg_force_editor = "-force_editor";

    const char* arg_set = "-set"; // set should not be used before game is loaded
    const int arg_set_params = 2;

    const char* arg_editor = "-editor";
    const int arg_editor_params = 0;

    const char* arg_nomenu = "-nomenu";
    const int arg_nomenu_params = 0;

    for ( uint i=1; i<argnum; ++i ) {
        if ( String::Cmp( strings[i], arg_map ) == 0 ) {
            if ( i+arg_force_map_params >= argnum ) {
                ERR("no argument supplied for %s\n", arg_map);
            } else {
                cmd.resize(0);
                cmd.emplace_back("map");
                cmd.push_back(strings[i+1]);
                CommandSys::Execute( cmd );
                i+=arg_force_map_params;
                continue;
            }
        }

        if ( String::Cmp( strings[i], arg_force_editor ) == 0 ) {
            cmd.resize(0);
            cmd.emplace_back("force_editor");
            CommandSys::Execute( cmd );
            i+=arg_editor_params;
            continue;
        }

        if ( String::Cmp( strings[i], arg_force_map ) == 0 ) {
            if ( i+arg_map_params >= argnum ) {
                ERR("no argument supplied for %s\n", arg_force_map);
            } else {
                cmd.resize(0);
                cmd.emplace_back("force_map");
                cmd.push_back(strings[i+1]);
                CommandSys::Execute( cmd );
                i+=arg_map_params;
                continue;
            }
        }

        if ( String::Cmp( strings[i], arg_set ) == 0 ) {
            if ( i+arg_set_params >= argnum ) {
                ERR("no argument supplied for %s\n", arg_set);
            } else {
                cmd.resize(0);
                cmd.emplace_back("set");
                cmd.push_back(strings[i+1]);
                cmd.push_back(strings[i+2]);
                CommandSys::Execute( cmd );
                i+=arg_set_params;
            }
        }

        if ( String::Cmp( strings[i], arg_editor ) == 0 ) {
            cmd.resize(0);
            cmd.emplace_back("editor");
            CommandSys::Execute( cmd );
            i+=arg_editor_params;
        }

        if ( String::Cmp( strings[i], arg_nomenu ) == 0 ) {
            globalVals.SetBool( gval_a_menu, false );
            i+=arg_nomenu_params;
        }
    }
}
