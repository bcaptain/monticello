// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_PRELOAD_H
#define SRC_PRELOAD_H

#include "./clib/src/warnings.h"
#include "./base/main.h"
#include "./particles/effectsManager.h"
#include "./rendering/models/modelManager.h"

class DamageInfo;
class Effect;
class EntityInfo;
struct Mix_Chunk;
class Model;
class SoundData;

//! To ensure assets are already loaded from disk before they are needed
class Preload {
public:
    template< typename ASSET_MANAGER, typename CONTAINER >
    static auto PreloadAsset( CONTAINER& container, ASSET_MANAGER& manager, const std::string& asset_name );

    static void PreloadDamage( const std::string& name );
    static void PreloadEffect( const std::string& name );
    static void PreloadSound( const std::string& name );
    static void PreloadModel( const std::string& name );
    static void PreloadEntity( const std::string& name );
    
private:
    static void PreloadEntity( const std::shared_ptr< const EntityInfo >& ent_info );
    static void PreloadEntityInfo( const std::shared_ptr< const EntityInfo >& ent_info );

public:
    static void Clear( void ); //!< clears all the preloaded stuff. This is meant to be called after unused assets are already unloaded
    static void ClearForShutdown( void );

private:
    static HashList< std::shared_ptr< const DamageInfo > > preloaded_damage;
    static HashList< std::shared_ptr< const Effect > > preloaded_effects;
    static HashList< std::shared_ptr< const EntityInfo > > preloaded_entityInfo;
    static HashList< std::shared_ptr< const Entity > > preloaded_entities;
    static HashList< std::shared_ptr< const SoundData > > preloaded_sounds;
    static HashList< std::shared_ptr< const Model > > preloaded_models;
};

template< typename ASSET_MANAGER, typename CONTAINER >
auto Preload::PreloadAsset( CONTAINER& container, ASSET_MANAGER& manager, const std::string& asset_name ) {
    auto const asset = manager.Get( asset_name.c_str() );
    if ( asset )
        container.Update( asset_name.c_str(), asset );
        
    return asset;
}

#endif // SRC_PRELOAD_H
