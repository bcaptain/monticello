// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_GAMEUI_GAMEUI_H_
#define SRC_GAMEUI_GAMEUI_H_

#include "../base/main.h"
#include "../ui/ui.h"
#include "../rendering/vbo.h"

class GameUI_InfestLevel {
public:
    GameUI_InfestLevel( void );
    virtual ~GameUI_InfestLevel( void );
    GameUI_InfestLevel( const GameUI_InfestLevel& other ) = delete;
    
public:
    GameUI_InfestLevel& operator=( const GameUI_InfestLevel& other ) = delete;

public:
    //void Show( void );
    //void Hide( void );
    
    void PackVBO( void );
    void Draw( void );
    
    void Update( void );
    void Reset( void );
    
    void DecreaseInfestLevel( const int amt );
    void IncreaseInfestLevel( const int amt );
    
    int GetInfestLevel( void ) const;
    
private:

    VBO<float>* vbo_ui_infestLevel;
    VBO<float>* vbo_ui_infestLevel_uv;
    int infestLevel;
    
};

class GameUI_Timer {
public:
    GameUI_Timer( void );
    virtual ~GameUI_Timer( void );
    GameUI_Timer( const GameUI_Timer& other ) = delete;
    
public:
    GameUI_Timer& operator=( const GameUI_Timer& other ) = delete;
    
public:
    //void Show( void );
    //void Hide( void );
    
    void PackVBO( void );
    void Draw( void );
    
    void Update( void );
    void Reset( void );
    
    void AddTime( const Uint seconds );
    void SubtractTime( const Uint seconds);
    
private:

    std::string SecondsToString_MMcSS( const Uint seconds ) const;
    
private:
    VBO<float>* vbo_ui_timer;
    VBO<float>* vbo_ui_timer_uv;
    Uint level_duration;
    Uint time_played;
    
};

class GameUI_Page {
public:
    GameUI_Page( void );
    virtual ~GameUI_Page( void );
    GameUI_Page( const GameUI_Page& other ) = delete;

public:
    GameUI_Page& operator=( const GameUI_Page& other ) = delete;

public:
    virtual void Show( void );
    virtual void Hide( void );

    bool IsOpen( void ) const;

    std::string GetName( void ) const;
    void UnpauseGameWhenClosed( const bool whether ); //!< returns to false when page is shown. Call this with TRUE only after first showing the page.

protected:
    std::string name;
    bool isOpen;
    bool unpauseGameWhenClosed;
};

#endif  // SRC_GAMEUI_GAMEUI_H_
