// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_GAMEUI_LEVELSUMMARY_H_
#define SRC_GAMEUI_LEVELSUMMARY_H_

#include "../gameui/gameui.h"

void LevelSummary_RestartLevel_Click( void );
void LevelSummary_QuitProgram_Click( void );

class UI_LevelSummary : public GameUI_Page {
    
public:
    UI_LevelSummary( void );
    ~UI_LevelSummary( void ) override { };
    UI_LevelSummary( const UI_LevelSummary& other ) = delete;

public:
    UI_LevelSummary& operator=( const UI_LevelSummary& other ) = delete;

    void Show( void ) override;
    void Hide( void ) override;

private:
    void Create( void );

private:
    Window* levelSummary;
    bool created;
};
#endif
