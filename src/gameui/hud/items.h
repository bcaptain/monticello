// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_GAMEUI_HUD_ITEMS_H_
#define SRC_GAMEUI_HUD_ITEMS_H_

#include "../../base/main.h"
#include "./stats.h"

class HUD_ItemsWindow;
class Entity;

/*!

HUD_ItemSlot

**/

class HUD_ItemSlot : public Button {
public:
    HUD_ItemSlot( void );
    ~HUD_ItemSlot( void );
    explicit HUD_ItemSlot( HUD_ItemsWindow *itemWindow );
    HUD_ItemSlot( const HUD_ItemSlot& other ) = delete;

public:
    HUD_ItemSlot& operator=( const HUD_ItemSlot& other ) = delete;

public:
    void Click( void );
    void SetContainer( HUD_ItemsWindow *itemWindow );

private:
    HUD_ItemsWindow *container;
};

/*!

HUD_ItemsWindow

**/

class HUD_ItemsWindow : public Window {
public:
    HUD_ItemsWindow( void );
    ~HUD_ItemsWindow( void );
    HUD_ItemsWindow( const HUD_ItemsWindow& other ) = delete;

public:
    HUD_ItemsWindow& operator=( const HUD_ItemsWindow& other ) = delete;

public:
    void SetOwner( Entity *ent );

private:
    void CreateUI( void );

private:
    Entity *owner; //!< who's items are going to appear in our window
};

#endif  // SRC_GAMEUI_HUD_ITEMS_H_
