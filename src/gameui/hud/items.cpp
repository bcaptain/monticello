// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./items.h"
#include "../../game.h"

/*!

HUD_ItemsWindow

**/

HUD_ItemsWindow::HUD_ItemsWindow( void )
    : owner( nullptr )
{
    CreateUI();
}

void HUD_ItemsWindow::SetOwner( Entity *ent ) {
    owner = ent;
}

void HUD_ItemsWindow::CreateUI( void ) {
    // edit mode buttons
//    slotWeapon = new HUD_ItemSlot( this );
//    game->sprites[UI_LAYER_WINDOWELEMENT].Add( slotWeapon );
}

HUD_ItemsWindow::~HUD_ItemsWindow( void ) {
    // todo: delete item in the slot also?

    // sprite list will handle these deletion

    owner = nullptr; // sprite list will handle deletion
}

/*!

HUD_ItemSlot

**/

HUD_ItemSlot::HUD_ItemSlot( void )
    : container( nullptr )
{
    SetLabel("HUD_ItemSlot");
}

HUD_ItemSlot::HUD_ItemSlot( HUD_ItemsWindow *itemWindow )
    : container(itemWindow )
{
    SetLabel("HUD_ItemSlot");
}

HUD_ItemSlot::~HUD_ItemSlot( void ) {

}

void HUD_ItemSlot::SetContainer( HUD_ItemsWindow *itemWindow ) {
    container = itemWindow;
}

void HUD_ItemSlot::Click( void ) {
    Button::Click();
}
