// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./stats.h"
#include "../../game.h"
#include "../../rendering/renderer.h"
#include "../../entities/actor.h"

HUD_Stats::HUD_Stats( void )
    : healthBox()
    , fiHud()
    , created( false )
{ }

HUD_Stats::~HUD_Stats( void ) {
    // data is deleted via the spriteList
}

void HUD_Stats::Create( void ) {
    ASSERT( ! created );
    created = true;
//    HUD_Stats &ui = game->ui;

    healthBox.Resize( 4 );
    healthBox.vert[FACE2D_VERT_CW_TOP_LEFT].co.Set(-1,1);
    healthBox.vert[FACE2D_VERT_CW_TOP_LEFT].uv.Set(-1,1);
    healthBox.vert[FACE2D_VERT_CW_TOP_LEFT].co.Set(1,1);
    healthBox.vert[FACE2D_VERT_CW_TOP_LEFT].uv.Set(-1,1);
    healthBox.vert[FACE2D_VERT_CW_BOTTOM_RIGHT].co.Set(1,-1);
    healthBox.vert[FACE2D_VERT_CW_BOTTOM_RIGHT].uv.Set(1,-1);
    healthBox.vert[FACE2D_VERT_CW_BOTTOM_LEFT].co.Set(-1,-1);
    healthBox.vert[FACE2D_VERT_CW_BOTTOM_LEFT].uv.Set(-1,-1);

    healthBox.BuildRect(100,60);
    healthBox.SetColor(1, 0.3f, 0.3f);
    healthBox.SetOpacity(0.3f);

    fiHud.size_ui_px = 32;
    fiHud.pos.Set(0,552);
    fiHud.color.Set(1,0,0);
    fiHud.opacity = 0.7f;
}

void HUD_Stats::Draw( void ) {
    if ( ! created )
        Create();

    if ( auto player = game->GetPlayer() )
        player->DrawHUD();
}
