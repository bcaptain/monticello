// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_GAMEUI_HUD_STATS_H_
#define SRC_GAMEUI_HUD_STATS_H_

#include "../../base/main.h"
#include "../../ui/ui.h"
#include "./items.h"

class HUD_Stats {
public:
    HUD_Stats( void );
    ~HUD_Stats( void );

private:
    void Create( void );

public:
    void Draw( void );

private:
    Face2D healthBox;
    FontInfo fiHud;
    bool created;
};

#endif  // SRC_GAMEUI_HUD_STATS_H_
