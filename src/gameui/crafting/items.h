// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_GAMEUI_CRAFTING_ITEMS_H_
#define SRC_GAMEUI_CRAFTING_ITEMS_H_

#include "../../base/main.h"
#include "../../ui/ui.h"
#include "../gameui.h"
#include "../../rendering/models/modelInfo.h"

extern const uint MAX_SHCEMATIC_REQUIREMENTS;

class UI_Items : public GameUI_Page {
public:
    UI_Items( void );
    ~UI_Items( void ) override { };
    UI_Items( const UI_Items& other ) = delete;

public:
    UI_Items& operator=( const UI_Items& other ) = delete;

    void Show( void ) override;
    void Hide( void ) override;

private:
    void Create( void );

private:
    Window* itemsMenu;
    std::vector< ModelBox* > mdlReqs;
    bool created;
};

void Event_AttachmentItemSelected( const std::vector< std::string >& args );

#endif  // SRC_GAMEUI_CRAFTING_ITEMS_H_
