// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./items.h"
#include "../../game.h"
#include "../../rendering/renderer.h"
#include "../../input/input.h"
#include "../../entities/actor.h"
#include "../../entities/entityInfoManager.h"

void CloseWorkbench_Click( void );
void CloseWorkbench_Click( void ) {
    const std::string pageName = "items";
    auto page = game->GetUIPage( pageName );

    if ( page )
        page->Hide();
}

UI_Items::UI_Items( void )
    : itemsMenu(nullptr)
    , mdlReqs()
    , created( false )
{
    name = "items";
}

void UI_Items::Create( void ) {
    ASSERT( ! created );
    created = true;
}

void UI_Items::Show( void ) {
    std::shared_ptr<Actor> player = game->GetPlayer();
    if ( !player )
        return;

    if ( !itemsMenu ) {
        Create();
    }

    isOpen = true;
    itemsMenu->SetVisible( true );
//
//    for ( auto link = player->inventory.GetFirst(); link != nullptr; link = link->GetNext() ) {
//        Item& item = link->Data();
//            continue;
//
//    }

    GameUI_Page::Show();
    Input::SetFocusWidget( itemsMenu->GetFirstTabstop() );
}

void UI_Items::Hide( void ) {
    if ( itemsMenu )
        itemsMenu->SetVisible( false );
    isOpen = false;

    GameUI_Page::Hide();
}

