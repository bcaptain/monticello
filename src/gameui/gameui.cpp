// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./gameui.h"
#include "../game.h"
#include "../rendering/renderer.h"
#include "../rendering/fontManager.h"
#include "../rendering/font.h"

#include "../input/input.h"

const float UI_TIMER_FONT_SIZE(48);
const Color4f UI_TIMER_FONT_COLOR(0,1,0,1);
const float UI_INFESTLEVEL_FONT_SIZE(48);
const Color4f UI_INFESTLEVEL_FONT_COLOR(1,1,0,1);
const Uint DEFAULT_LEVEL_DURATION_SEC(300);

GameUI_InfestLevel::GameUI_InfestLevel( void )
    : vbo_ui_infestLevel()
    , vbo_ui_infestLevel_uv()
    , infestLevel()
{
    
}

GameUI_InfestLevel::~GameUI_InfestLevel( void ) {
}

void GameUI_InfestLevel::PackVBO( void ){
    if ( vbo_ui_infestLevel ) { 
        ASSERT( vbo_ui_infestLevel_uv );
        vbo_ui_infestLevel->Clear();
        vbo_ui_infestLevel_uv->Clear();
    } else {
        ASSERT( ! vbo_ui_infestLevel_uv);
        vbo_ui_infestLevel = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
        vbo_ui_infestLevel_uv = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
    }

    FontInfo fi;
    fi.size_ui_px = UI_TIMER_FONT_SIZE;
    fi.pos.Set(800 - ( UI_INFESTLEVEL_FONT_SIZE * std::to_string( infestLevel ).size() ),0);
    vbo_ui_infestLevel->PackText( *vbo_ui_infestLevel_uv,std::to_string( infestLevel ).c_str(), fi, &fi.pos );
    vbo_ui_infestLevel->MoveToVideoCard();
    vbo_ui_infestLevel_uv->MoveToVideoCard();
}

void GameUI_InfestLevel::Draw( void ) {
    PackVBO();

    ASSERT( vbo_ui_infestLevel );
    ASSERT( vbo_ui_infestLevel_uv );
    ASSERT( vbo_ui_infestLevel->Finalized() );
    ASSERT( vbo_ui_infestLevel_uv->Finalized() );

    glDisable( GL_DEPTH_TEST );
        const char* fontname = "courier";
        std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
        Font::AssertValid( fnt, fontname );

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SendData_Matrices();
        shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
        shaders->SetUniform1f("fWeight", 1.0f );
        shaders->SetUniform4f("vColor", UI_INFESTLEVEL_FONT_COLOR );
        shaders->SetAttrib( "vUV", *vbo_ui_infestLevel_uv );
        shaders->SetAttrib( "vPos", *vbo_ui_infestLevel );
        shaders->DrawArrays( GL_QUADS, 0, vbo_ui_infestLevel->Num() );
    glEnable( GL_DEPTH_TEST );
}

void GameUI_InfestLevel::Update( void ) {
    infestLevel = 0;
    for ( auto& ent : game->GetEntityListRef() ) {
        infestLevel += ent->GetInfestValue();
    }
}

GameUI_Timer::GameUI_Timer( void )
    : vbo_ui_timer()
    , vbo_ui_timer_uv()
    , level_duration( DEFAULT_LEVEL_DURATION_SEC )
    , time_played()
{
    
}

GameUI_Timer::~GameUI_Timer( void ) {
}

void GameUI_Timer::PackVBO( void ){
    if ( vbo_ui_timer ) { 
        ASSERT( vbo_ui_timer_uv );
        vbo_ui_timer->Clear();
        vbo_ui_timer_uv->Clear();
    } else {
        ASSERT( ! vbo_ui_timer_uv);
        vbo_ui_timer = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
        vbo_ui_timer_uv = new VBO<float>( 2,VBOChangeFrequencyT::FREQUENTLY );
    }

    FontInfo fi;
    
    const Uint time_left = level_duration - time_played;
    
    fi.size_ui_px = UI_TIMER_FONT_SIZE;
    fi.pos.Set(0,0);
    vbo_ui_timer->PackText( *vbo_ui_timer_uv,SecondsToString_MMcSS( time_left ).c_str(), fi, &fi.pos );
    vbo_ui_timer->MoveToVideoCard();
    vbo_ui_timer_uv->MoveToVideoCard();
}

void GameUI_Timer::Draw( void ) {
    PackVBO();

    ASSERT( vbo_ui_timer );
    ASSERT( vbo_ui_timer_uv );
    ASSERT( vbo_ui_timer->Finalized() );
    ASSERT( vbo_ui_timer_uv->Finalized() );

    glDisable( GL_DEPTH_TEST );
        const char* fontname = "courier";
        std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
        Font::AssertValid( fnt, fontname );

        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
        shaders->SendData_Matrices();
        shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
        shaders->SetUniform1f("fWeight", 1.0f );
        shaders->SetUniform4f("vColor", UI_TIMER_FONT_COLOR );
        shaders->SetAttrib( "vUV", *vbo_ui_timer_uv );
        shaders->SetAttrib( "vPos", *vbo_ui_timer );
        shaders->DrawArrays( GL_QUADS, 0, vbo_ui_timer->Num() );
    glEnable( GL_DEPTH_TEST );
}

void GameUI_Timer::Update( void ) {
    
    if ( globalVals.GetBool( gval_g_noLevelSummary ) )
        return;
    
    if ( globalVals.GetBool( gval_s_levelTimerExpired ) )
        return;
        
    time_played = game->GetGameTime() / 1000;
    
    if ( time_played >= level_duration ) {
        time_played = level_duration;
        game->QueueEvent_DisplayLevelSummary();
        globalVals.SetBool( gval_s_levelTimerExpired, true );
        return;
    }
}

std::string GameUI_Timer::SecondsToString_MMcSS( const Uint seconds ) const {
    std::string str;
    if ( seconds < 60 ) {
        str.append( std::to_string( seconds ) );
        return str;
    }
    
    str = std::to_string( seconds / 60 );
    
    std::string sec = std::to_string( seconds % 60 );
    if ( sec.size() < 2 )
        sec.insert(0,"0");
        
    str.append(":" + sec );

    return str;
}

void GameUI_Timer::AddTime( const Uint seconds ) {
    level_duration += seconds;
}

void GameUI_Timer::SubtractTime( const Uint seconds ) {
    if ( seconds > level_duration ) {
        level_duration = 0;
        return;
    }
        
    level_duration -= seconds;
}


GameUI_Page::GameUI_Page( void )
    : name()
    , isOpen( false )
    , unpauseGameWhenClosed( false )
{

}

GameUI_Page::~GameUI_Page( void ) {
}

void GameUI_Page::Show( void ) {
    game->Pause( PauseStateT::PausedByGameUI );
    unpauseGameWhenClosed = true;
}

void GameUI_Page::Hide( void ) {
    if ( unpauseGameWhenClosed ) {
        game->Unpause( PauseStateT::PausedByGameUI );
    }

    Input::UpdateFocusWidget();
}

std::string GameUI_Page::GetName( void ) const {
    return name;
}

bool GameUI_Page::IsOpen( void ) const {
    return isOpen;
}

void GameUI_Page::UnpauseGameWhenClosed( const bool whether ) {
    unpauseGameWhenClosed = whether;
}

void GameUI_Timer::Reset( void ) {
    time_played = 0;
}

void GameUI_InfestLevel::DecreaseInfestLevel( const int amt ) {
    infestLevel = std::max( infestLevel - amt, 0 );
}

void GameUI_InfestLevel::IncreaseInfestLevel( const int amt ) {
    infestLevel += amt;
}

void GameUI_InfestLevel::Reset( void ) {
    infestLevel = 0;
}

int GameUI_InfestLevel::GetInfestLevel( void ) const {
    return infestLevel;
}
