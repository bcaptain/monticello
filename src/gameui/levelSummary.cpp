// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./levelSummary.h"
#include "../input/input.h"

void LevelSummary_RestartLevel_Click( void ) {
    ASSERT( game );

    game->ReloadMap();
    game->ResetLevelTimer();

    auto page = game->GetUIPage( "levelSummary" );
    
    if ( page )
        page->Hide();
}

void LevelSummary_QuitProgram_Click( void ) {
    ASSERT( game );
    game->SetState( GameStateT::SHUTDOWN );
}

UI_LevelSummary::UI_LevelSummary( void )
    : levelSummary(nullptr)
    , created( false )
{
    name = "levelSummary";
}

void UI_LevelSummary::Create( void ) {
    ASSERT( ! levelSummary );
    created = true;

    levelSummary = new Window( WindowOptionsT::NO_COLLAPSE_BUTTON, 500 ,400 );
    levelSummary->SetOrigin(150,100,0);
    levelSummary->GetFace()->SetOpacity( 1.0f );
    levelSummary->SetMenu( true );
    levelSummary->SetPage( this );
    game->sprites[UI_LAYER_PROGRAM_WINDOW].Add( levelSummary );

    levelSummary->SetOverlayImage( "levelSummaryBG" );
    
    if ( game->GetInfestLevel() < 0 ) {        
        Label* label_cleared = new Label("Clear!");
        label_cleared->SetOrigin(110,130,0);
        label_cleared->SetFontSize( 56 );
        label_cleared->SetFontColor( Vec3f(0.949f,0.894f,0.0f) );
        label_cleared->SetFontOpacity( 1.0f );
        label_cleared->AdjustSizeForText();
        levelSummary->children->Add( label_cleared );
    }

    Button_Func* btnTryAgain=new Button_Func( "", &LevelSummary_RestartLevel_Click, ButtonTypeT::None, 202, 58);
    btnTryAgain->SetOrigin( 153,245,0);
    btnTryAgain->GetFace()->SetOpacity(1.0f);
    btnTryAgain->SetTabstopIndex( 1 );
    btnTryAgain->SetOverlayImage( "tryAgainButton" );
    levelSummary->children->Add( btnTryAgain );
    
    Button_Func* btnMenuQuit=new Button_Func( "", &LevelSummary_QuitProgram_Click, ButtonTypeT::None, 202, 58);
    btnMenuQuit->SetOrigin( 153,308,0);
    btnMenuQuit->GetFace()->SetOpacity(1.0f);
    btnMenuQuit->SetTabstopIndex( 2 );
    btnMenuQuit->SetOverlayImage( "quitButton" );
    levelSummary->children->Add( btnMenuQuit );

}

void UI_LevelSummary::Show( void ) {

    if ( !levelSummary ) {
        Create();
    }

    levelSummary->SetVisible( true );
    isOpen = true;
    
    GameUI_Page::Show();

    Input::SetFocusWidget( levelSummary->GetFirstTabstop() );

    SDL_ShowCursor(true);
}

void UI_LevelSummary::Hide( void ) {
    if ( levelSummary )
        levelSummary->SetVisible( false );
        
    isOpen = false;

    GameUI_Page::Hide();
    
    game->UpdateCursor();
}
