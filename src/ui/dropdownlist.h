// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_DROPDOWNLOIST_H_
#define SRC_UI_DROPDOWNLOIST_H_

#include "../base/main.h"
#include "../ui/listbox.h"

class TextBox;

/*!

DropDownList \n\n

**/

class DropDownList : public ListBox {
public:
    DropDownList( void ) = delete;
    DropDownList( const int visibleElements, const int firstItemTabstopIndex, FuncThatTakesStringVector _func, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    DropDownList( const ListBox& other ) = delete;
    DropDownList( const DropDownList& other ) = delete;
    ~DropDownList( void ) override;
    DropDownList& operator=( const DropDownList& other ) = delete;

protected:
    void CreateItems( const uint y_pos_begin ) override;
    void CallFunction( const std::vector< std::string >& arguments ) override;

public:
    void Refresh( void ) override;
public:
    void SetLabel( const std::string& to );
    void SetLabel( const char * to );

    void Trigger( const UITriggerTypeT uiTriggerType ) override;
    void ToggleOpen( void );
    void SetOpen( const bool whether );
    void Open( void );
    void Close( void );
    void Deselect( void ) override;

    std::string GetLabel( void ) const;
    int GetSelectedIndex( void ) const;

    void RemoveSelected( void );

    void Search( void );

private:
    Button_TriggerParent* btnOpenList;
    TextBox* textbox;
    bool childrenVisible;
};

#endif  // SRC_UI_DROPDOWNLOIST_H_
