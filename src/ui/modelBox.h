// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_MODELBOX
#define SRC_UI_MODELBOX

#include "./widget.h"
#include "../rendering/models/modelInfo.h"
#include "../rendering/fbo.h"

class ModelBox : public Widget {
public:
    ModelBox( void ) = delete;
    ModelBox( const uint width, const uint height );
    ModelBox( const ModelBox& other ) = delete;
    ModelBox& operator=( const ModelBox& other ) = delete;

public:
    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;

public:
    void SetModel( const std::string& to );
    void SetMaterial( const std::string& to );
    void SetSpinRate( const Vec3f& to ); //!< multiplier. 0 = no spin, 1 = slow spin, >1 for faster.
    Vec3f GetSpinRate( void ) const;
    void SetOpacity( const float to );

private:
    std::string materialName;
    std::string modelName;
    ModelInfo modelInfo;
    ModelDrawInfo drawInfo;
    Vec3f spinRate;
};

#endif  // SRC_UI_MODELBOX
