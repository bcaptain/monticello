// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./slider.h"
#include "../input/input.h"
#include "../editor/editor.h"

const float OPACITY_SLIDER_KNOB = 0.1f;
const uint SLIDER_KNOB_WIDTH = 12;

template Slider<float>::Slider( const char* text, const uint thewidth, const uint theheight );
template void SliderKnob<float>::UpdateOrigin( void );
template void SliderKnob<float>::Click( void );
template void SliderKnob<float>::DragUpdate( void );
template void Slider<float>::DragUpdate( void );

template Slider<int>::Slider( const char* text, const uint thewidth, const uint theheight );
template void SliderKnob<int>::UpdateOrigin( void );
template void SliderKnob<int>::Click( void );
template void SliderKnob<int>::DragUpdate( void );
template void Slider<int>::DragUpdate( void );

template Slider<uint>::Slider( const char* text, const uint thewidth, const uint theheight );
template void SliderKnob<uint>::UpdateOrigin( void );
template void SliderKnob<uint>::Click( void );
template void SliderKnob<uint>::DragUpdate( void );
template void Slider<uint>::DragUpdate( void );

SliderBase::SliderBase( const char* text, const uint thewidth, const uint theheight )
    : TextBox( text, thewidth, theheight )
    , sliderWidth( thewidth )
{
    ASSERT( ! children );
    children = new SpriteList( this );
}

inline float SliderBase::GetSliderWidth( void ) const {
    return sliderWidth;
}

template<>
void Slider<float>::UpdateSliderLabel( void ) {
    label = std::to_string( val );
    String::TrimTrailing( label, "0" );
    UpdateVBO();
}

template<>
void Slider<int>::UpdateSliderLabel( void ) {
    label = std::to_string( val );
    UpdateVBO();
}

template<>
void Slider<uint>::UpdateSliderLabel( void ) {
    label = std::to_string( val );
    UpdateVBO();
}

template<>
void Slider<float>::ValFromLabel( void ) {
    SetVal( String::ToFloat( label.c_str() ) );
}

template<>
void Slider<int>::ValFromLabel( void ) {
    SetVal( String::ToInt( label.c_str() ) );
}

template<>
void Slider<uint>::ValFromLabel( void ) {
    SetVal( String::ToUInt( label.c_str() ) );
}

template<typename TYPE>
void SliderKnob<TYPE>::Click( void ) {
    if (! owner)
        return;

#ifdef MONTICELLO_EDITOR
    editor->SetSelectedSlider( SliderKnob<TYPE>::owner );
#endif // MONTICELLO_EDITOR

    Vec3f uipos( Input::GetUIMousePos() );
    owner->clickPos = static_cast< int >( uipos.x );
    click_x = origin.x;
}

template<typename TYPE>
void SliderKnob<TYPE>::DragUpdate( void ) {
    float new_x = click_x;

    Vec3f uipos( Input::GetUIMousePos() );
    new_x += uipos.x;
    new_x -= owner->clickPos;
    new_x = std::fmaxf(static_cast< TYPE >( new_x ), static_cast< TYPE >( 0 ) );
    new_x = std::fminf(static_cast< TYPE >( new_x ), static_cast< TYPE >( owner->GetSliderWidth() ) );

    new_x /= owner->GetSliderWidth();

    owner->SetValFromFraction( new_x );
    owner->UpdateSliderLabel();
    UpdateOrigin();
}
