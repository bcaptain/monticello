// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./checkBox.h"

extern const uint DIALOG_BUTTON_HEIGHT;

CheckBox::CheckBox( void(*click_function)( const bool checked ) )
    : Button( "", ButtonTypeT::None, WIDGET_STANDARD_HEIGHT, WIDGET_STANDARD_HEIGHT )
    , clickFunc( click_function )
{
    face.SetColor( COLOR_CHECKBOX_UNCHECKED );
    face.SetOpacity( UI_OPACITY );
    SetBorder( true );
}

CheckBox::~CheckBox( void ) {

}

void CheckBox::Check( const bool checked ) {
    if ( checked ) {
        label = "x";
        face.SetColor( COLOR_CHECKBOX_CHECKED );
        UpdateVBO();
    } else {
        label = "";
        face.SetColor( COLOR_CHECKBOX_UNCHECKED );
        UpdateVBO();
    }
    
    if ( clickFunc )
        (*clickFunc)( checked );
}

void CheckBox::Click( void ) {
    if ( label.size() == 0 ) {
        Check( true );
    } else {
        Check( false );
    }
}

void CheckBox::PressEnter( void ) {
    Click();
}
