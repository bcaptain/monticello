// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./button.h"
#include "../rendering/renderer.h"
#include "../rendering/materialManager.h"
#include "../input/input.h"

extern const uint DIALOG_BUTTON_HEIGHT;

Face2D Button::joyButtonImage;
std::unique_ptr< VBO<float> > Button::vbo_padButtonVert(nullptr);
std::unique_ptr< VBO<float> > Button::vbo_padButtonUV(nullptr);

Button::Button( void )
    : type( ButtonTypeT::None )
{
    Construct();
}

Button::~Button( void ) {

}

Button::Button( const char* text, const ButtonTypeT btnType, const uint width, const uint height )
    : Label( text, width, height )
    , type( btnType )
{
    Construct();
}

void Button::Construct( void ) {
    if ( !vbo_padButtonVert ) {
        ASSERT( ! vbo_padButtonUV );
        vbo_padButtonVert = std::make_unique< VBO<float> >(2,VBOChangeFrequencyT::RARELY );
        vbo_padButtonUV = std::make_unique< VBO<float> >(2,VBOChangeFrequencyT::RARELY );
    }

    SetFaceTopColor( COLOR_BUTTON_GRADIENT_A );
    SetFaceBottomColor( COLOR_BUTTON_GRADIENT_B );
    face.SetOpacity( UI_OPACITY );

    SetBorder( true );
}

void Button::Click( void ) {
    Label::Click();
}

void Button::PressEnter( void ) {
    Click();
}

void Button::UpdateVBO( void )  {
    ASSERT( vbo_padButtonUV );
    ASSERT( vbo_padButtonVert );
    Label::UpdateVBO();
    // we don't clear the button VBOs
}

std::shared_ptr< const Material > Button::GetJoyButtonMaterial( void ) const {
    std::vector< std::string > joyButtonNames;
    switch ( GetButtonTypeT() ) {
        case ButtonTypeT::Accept: joyButtonNames = Input::GetUIBind( "accept"); break;
        case ButtonTypeT::Decline: joyButtonNames = Input::GetUIBind( "cancel" ); break;
        case ButtonTypeT::Option1: joyButtonNames = Input::GetUIBind( "opt1"); break;
        case ButtonTypeT::Option2: joyButtonNames = Input::GetUIBind( "opt2" ); break;
        case ButtonTypeT::None: return nullptr;
        default: return nullptr;
    }
    
    static bool error_displayed = false;
    if ( joyButtonNames.size() < 1 ) {
        if ( !error_displayed ) {
            WARN("Button::GetJoyButtonMaterial(): No button material for %u. This warning will not appear again.\n" );
            error_displayed = true;
        }
        return nullptr;
    }
    
    std::string button_name = Input::joystick.GetButtonUIDisplayName( joyButtonNames[0] );
    
    static bool error2_displayed = false;
    const std::shared_ptr< const Material > mtl_p = materialManager.Get_NoErr( button_name.c_str() );
    if ( ! mtl_p || ! mtl_p->GetDiffuse() ) {
        if ( !error2_displayed ) {
            ERR("Button::GetJoyButtonMaterial(): couldn't find button material: %s. This warning will not appear again.\n", button_name.c_str() );
            error2_displayed = true;
        }
        return nullptr;
    }

    return mtl_p;
}

bool Button::DrawJoyButtonImageVBO( void ) const {
    ASSERT( vbo_padButtonUV );
    ASSERT( vbo_padButtonVert );

    if ( !vbo_padButtonVert->Finalized() || !vbo_padButtonUV->Finalized() ) {
        pack_vbo();
    }

    std::shared_ptr< const Material > mtl_p = GetJoyButtonMaterial();
    if ( !mtl_p )
        return false;

    shaders->UseProg(  GLPROG_TEXTURE_2D  );
    shaders->SendData_Matrices();
    shaders->SetTexture( mtl_p->GetDiffuse()->GetID() );
    shaders->SetAttrib( "vUV", *vbo_padButtonUV );
    shaders->SetAttrib( "vPos", *vbo_padButtonVert );
    shaders->DrawArrays( GL_QUADS, 0, vbo_padButtonVert->Num() );

    return true;
}

void Button::DrawVBO( void ) const {
    Widget::DrawVBO();

        if ( DrawJoyButtonImageVBO() ) {
            renderer->PushMatrixMV();
                const float joyButtonImageSize = GetHeight();
                renderer->ModelView().Translate( joyButtonImageSize, 0, 0 );
                Label::DrawLabelVBO();
            renderer->PopMatrixMV();
        } else {
            Label::DrawLabelVBO();
        }
}

void Button::pack_vbo( void ) const {
    ASSERT( vbo_padButtonUV );
    ASSERT( vbo_padButtonVert );

    if ( vbo_padButtonVert->Finalized() ) {
        ASSERT( vbo_padButtonUV->Finalized() );
        return;
    }

    const uint joyButtonImageSize = DIALOG_BUTTON_HEIGHT;
    joyButtonImage.BuildRect( joyButtonImageSize, joyButtonImageSize );

    vbo_padButtonUV->PackFace2DTexCoords( joyButtonImage );
    vbo_padButtonVert->PackFace2DVerts( joyButtonImage );

    vbo_padButtonUV->MoveToVideoCard();
    vbo_padButtonVert->MoveToVideoCard();
}

Button_TriggerParent::Button_TriggerParent( void )
{ }

Button_TriggerParent::Button_TriggerParent( const char* text, const ButtonTypeT btnType, const uint width, const uint height )
    : Button( text, btnType, width, height )
{ }

void Button_TriggerParent::Click( void ) {
    if ( parent ) {
        Widget* window = dynamic_cast< Widget* >( parent );
        if ( window )
            window->Trigger( UITriggerTypeT::Activate );
    }
}

Button_Func::Button_Func( void )
    : click_func(nullptr)
{ }

Button_Func::Button_Func( const char* text, void(*click_function)( void), const ButtonTypeT btnType, const uint width, const uint height )
    : Button( text, btnType, width, height )
    , click_func( click_function )
{ }

void Button_Func::Click( void ) {
    Label::Click();

    if ( click_func )
        (*click_func)();
}

void Button_Func::PressEnter( void ) {
    Click();
}

Button_FuncWithArg::Button_FuncWithArg( const char* text, FuncThatTakesStringVector click_function, const std::vector< std::string >& arg, const ButtonTypeT btnType, const uint width, const uint height )
    : Button( text, btnType, width, height )
    , arguments( arg )
    , click_func( click_function )
    {

}

void Button_FuncWithArg::Click( void ) {
    Label::Click();

    if ( click_func )
        (*click_func)( arguments );
}

void Button_FuncWithArg::PressEnter( void ) {
    Click();
}
