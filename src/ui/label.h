// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_LABEL_H_
#define SRC_UI_LABEL_H_

#include "./widget.h"
#include "../rendering/font.h"

class Label : public Widget {
public:
    Label( void );
    explicit Label( const char* text );
    Label( const char* text, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    ~Label( void ) override;
private:
    void Construct( void );

public:
    Label( const Label& other ) = delete;

public:
    Label& operator=( const Label& other ) = delete;

public:
    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;
protected:
    void DrawLabelVBO( void ) const;
    void pack_vbo( void ) const;

public:
    virtual void SetLabel( const char *n );
    virtual void SetLabel( const std::string& n);
    std::string GetLabel( void ) const;
    void SetFontSize( const float val );
    float GetFontSize( void ) const;
    void SetFontPos( const Vec2f& org );
    void SetFontColor( const Vec3f& to );
    void SetFontOpacity( const float val );
    void AdjustSizeForText( void );

protected:
    Vec2f nextCharPos;
    std::string label;
    VBO<float>* vbo_textvert;
    VBO<float>* vbo_textcoord;
    FontInfo fi;
};


#endif  // SRC_UI_LABEL_H_
