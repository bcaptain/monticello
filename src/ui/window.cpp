// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./window.h"

#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../input/input.h"
#include "../sprites/spriteList.h"

WindowTitle::WindowTitle( const char* title, const uint width, const uint height )
    : Label( title, width, height )
{
    SetLabel( title );
    SetFaceOpacity( UI_OPACITY );
    SetBorder( true );
}

WindowTitle::~WindowTitle( void ) {
}

ButtonWinCollapse::ButtonWinCollapse( Window* xowner )
    : Button("", ButtonTypeT::None, WIDGET_STANDARD_HEIGHT,WIDGET_STANDARD_HEIGHT)
    , owner(xowner)
    , vbo_arrow(new VBO<float>(3, VBOChangeFrequencyT::FREQUENTLY))
{
    SetFaceOpacity( 0.0f );
    SetBorder( false );
}

ButtonWinCollapse::~ButtonWinCollapse( void ) {
    delete vbo_arrow;
}

void ButtonWinCollapse::Click( void ) {
    Button::Click();
    ASSERT( owner );
    owner->ToggleCollapsed();
}

void ButtonWinCollapse::DrawVBO( void ) const {
    ASSERT( owner );

    Button::DrawVBO();

    ASSERT( vbo_arrow );

    if ( !vbo_arrow->Finalized() ) {
        pack_vbo();
    }

    shaders->UseProg(  GLPROG_MINIMAL  );
    shaders->SendData_Matrices();
    shaders->SetUniform4f("vColor", 0.0f,0.0f,0.0f, 1.0f );
    shaders->SetAttrib( "vPos", *vbo_arrow );
    shaders->DrawArrays( owner->IsCollapsed() ? GL_LINE_LOOP : GL_TRIANGLES, 0, vbo_arrow->Num() );
}

void ButtonWinCollapse::pack_vbo( void ) const {
    ASSERT( vbo_arrow );

    // ** Pack in a triagle for an arrow
    // counter-clockwise
    std::vector<float> verts;

    if ( !owner->IsCollapsed() ) {
        // pointind down
        verts = {
            11.0, 3.0, 0.0,
            6.0, 8.0, 0.0,
            1.0, 3.0, 0.0
        };
    } else {
        // pointing sideways
        verts = {
            3.0, 2.5, 0.0,
            10.0, 6.0, 0.0,
            3.0, 9.5, 0.0
        };
    }

    vbo_arrow->PackArray(verts.size(), verts.data());
    vbo_arrow->MoveToVideoCard();
}

void ButtonWinCollapse::UpdateVBO( void )  {
    Button::UpdateVBO();

    if ( vbo_arrow )
        vbo_arrow->Clear();
}

Window::Window( void )
    : element_visibility()
    , wndOpts( WindowOptionsT::DEFAULT )
    , next_element_vpos(0)
    , page(nullptr)
    , oldheight(WIDGET_STANDARD_HEIGHT)
    , collapsed(false)
    , isMenu( false )
{
    Construct();
}

Window::Window( const WindowOptionsT type )
    : element_visibility()
    , wndOpts( type )
    , next_element_vpos(0)
    , page(nullptr)
    , oldheight(WIDGET_STANDARD_HEIGHT)
    , collapsed(false)
    , isMenu( false )
{
    Construct();
}

Window::Window( const WindowOptionsT type, const uint width, const uint height )
    : Widget( width, height )
    , element_visibility()
    , wndOpts( type )
    , next_element_vpos(0)
    , page(nullptr)
    , oldheight(WIDGET_STANDARD_HEIGHT)
    , collapsed(false)
    , isMenu( false )
{
    Construct();
}

Window::Window( const uint width, const uint height )
    : Widget( width, height )
    , element_visibility()
    , wndOpts( WindowOptionsT::DEFAULT )
    , next_element_vpos(0)
    , page(nullptr)
    , oldheight(WIDGET_STANDARD_HEIGHT)
    , collapsed(false)
    , isMenu( false )
{
    Construct();
}

void Window::Construct( void ) {

    ASSERT( ! children );
    children = new SpriteList( this );

    if ( HasCollapseButton() )
        children->Add( new ButtonWinCollapse( this ) );

    SetBorder( true );

    face.SetColor( BitwiseAnd( wndOpts, WindowOptionsT::INNER ) ? COLOR_WINDOW_BACKGROUND_B : COLOR_WINDOW_BACKGROUND_A );
    SetBorderColor( COLOR_WINDOW_BORDER );
    face.SetOpacity( UI_OPACITY );
}

Window::~Window( void ) {
}

void Window::ToggleCollapsed( void ) {
    float newheight;

    if ( collapsed ) {
        newheight = oldheight;
    } else {
        oldheight = GetHeight();
        newheight = WIDGET_STANDARD_HEIGHT;
    }

    SetHeight(newheight);

    // if we're collapsing, make sure the element_visibility status list is clear
    if ( !collapsed )
        element_visibility.DelAll();

    if ( children->list.size() < 1 )
        return;

    for ( auto& sprite_ptr : children->list ) {
        ASSERT( sprite_ptr );
        // everything rendered with it's top above the bottom of the 'title bar' will not be hidden when window is collapsed
        if ( sprite_ptr->GetOrigin().y >= WIDGET_STANDARD_HEIGHT ) {
            if ( !collapsed ) {
                // save element visibliity status for later
                element_visibility.Append( sprite_ptr->IsVisible() );
                sprite_ptr->SetVisible( false );
            } else {
                // restore element visibility
                sprite_ptr->SetVisible( element_visibility.GetFirst()->Data() );
                element_visibility.GetFirst()->Del();
            }
        }
    }

    collapsed = !collapsed;
}

bool Window::ShowNamedElement( const std::string& str, const bool whether ) {
    for ( auto& sprite_ptr : children->list ) {
        ASSERT( sprite_ptr );
        if ( sprite_ptr->GetName() == str ) {
            sprite_ptr->SetVisible( whether );
            return true;
        }
    }

    return false;
}

bool Window::ClickButton( const ButtonTypeT buttonType ) {
    // find the first button of the type and click it
    for ( auto& sprite_ptr : children->list ) {
        ASSERT( sprite_ptr );

        if ( !sprite_ptr->IsVisible() )
            continue;

        Button* button = dynamic_cast< Button* > ( sprite_ptr );
        if ( ! button )
            continue;

        if ( button->GetButtonTypeT() == buttonType ) {
            button->Click();
            return true;
        }
    }

    return false;
}


    bool ClickButton( const ButtonTypeT type );

Window* Window::NewWindow( const char* wnd_name, const char* wnd_title, const uint wnd_width, const uint wnd_height, Window* parent ) {
    WindowOptionsT wndOpts;
    if ( parent ) {
        wndOpts = WindowOptionsT::INNER;
    } else {
        wndOpts = WindowOptionsT::DEFAULT;
    }

    Window* wnd = new Window( wndOpts, wnd_width, wnd_height );
    InitNewWindow( *wnd, wnd_name, wnd_title, parent );
    return wnd;
}

void Window::CreateTitle( const char* wnd_title ) {
    const uint wnd_title_width = static_cast<uint>( GetWidth() ) - ( HasCollapseButton() ? WINDOW_COLLAPSE_BUTTON_WIDTH : 0 );
    WindowTitle* wndTitle = new WindowTitle(wnd_title, wnd_title_width);
    children->Add( wndTitle );

    if ( HasCollapseButton() )
        wndTitle->SetOrigin( WINDOW_COLLAPSE_BUTTON_WIDTH,0,0);

    const uint vertical_buffer = 2;
    next_element_vpos = WIDGET_STANDARD_HEIGHT+vertical_buffer;
}

void Window::InitNewWindow( Window& wnd, const char* wnd_name, const char* wnd_title, Window* parent ) {
    if ( parent )
        parent->children->Add( &wnd );

    wnd.SetName( wnd_name );
    wnd.CreateTitle( wnd_title );
}

void Window::DeleteElementsBelowYPos( const uint y_border ) {
    // ** clear out UI from any previous selected entity
    uint i=0;
    Link< bool > *vis_lnk=element_visibility.GetFirst();

    while ( i < children->list.size() ) {
        float y_pos = children->list[i]->GetOrigin().y;

        if ( y_pos < WIDGET_STANDARD_HEIGHT ) { // do not delete items on the title bar. also, these have no vis info
            ++i;

        } else if ( y_pos < y_border ) { // don't delete anything beyond our specified y position

            ++i;
            if ( vis_lnk )
                vis_lnk = vis_lnk->GetNext();

        } else { // delete it and it's vis info
            Widget* w = dynamic_cast< Widget* >( children->list[i] );
            if ( w )
                Input::SetFocusWidget( w );

            children->DelAt(i);
            if ( vis_lnk )
                vis_lnk = vis_lnk->Del();
        }
    }

    // set up to start drawing right below the title bar
    extern uint widget_v_spacing;
    next_element_vpos = y_border + widget_v_spacing;
}

Widget* Window::GetFirstTabstop( void ) const {
    Widget* first = nullptr;

    for ( auto& sprite : children->list ) {
        if ( !sprite )
            continue;

        Widget* widget = static_cast< Widget* >( sprite );
        const Uint8 idx = widget->GetTabstopIndex();
        if ( idx < 1 )
            continue;

        if ( ! first || idx < first->GetTabstopIndex() )
            first = widget;
    }

    return first;
}

void Window::HasGainedFocus( void ) {
    FocusFirstTabstop();
}

void Window::SetPage( GameUI_Page* newPage ) {
    page = newPage;
}

GameUI_Page* Window::GetPage( void ) const {
    return page;
}

bool Window::HasCollapseButton( void ) const {
    return !BitwiseAnd(wndOpts, WindowOptionsT::NO_COLLAPSE_BUTTON);
}

bool Window::IsCollapsed( void ) const {
    return collapsed;
}

void Window::DeleteAllElements( void ) {
    DeleteElementsBelowYPos( WIDGET_STANDARD_HEIGHT ); //! everything below the toolbar (if something exists in negative y space it won't be deleted)
}

void Window::SetMenu( const bool whether ) {
    isMenu = whether;
}

bool Window::IsMenu( void ) const {
    return isMenu;
}
