// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./widget.h"

const Color4f FOCUS_WIDGET_HIGHTLIGHT_COLOR( 1,0,0,0.5f );

const Vec3f COLOR_UI_BACKGROUND( 80.0f/256, 116.0f/256, 122.0f/256 );
const Vec3f COLOR_UI_BORDER( 55.0f/256, 82.0f/256, 86.0f/256 );
const Vec3f COLOR_UI_FONT( 184.0f/256, 255.0f/256, 237.0f/256 );
const Vec3f COLOR_UI_FONT_2( 255.0f/256, 237.0f/256, 184.0f/256 );
const Vec3f COLOR_WINDOW_BACKGROUND_A( 152.0f/256, 157.0f/256, 164.0f/256 );
const Vec3f COLOR_WINDOW_BACKGROUND_B( 127.0f/256, 132.0f/256, 138.0f/256 );
const Vec3f COLOR_WINDOW_BORDER( 64.0f/256, 72.0f/256, 73.0f/256 );
const Vec3f COLOR_BUTTON_GRADIENT_A( 55.0f/256, 82.0f/256, 86.0f/256 );
const Vec3f COLOR_BUTTON_GRADIENT_B( 98.0f/256, 142.0f/256, 149.0f/256 );
const Vec3f COLOR_RADIOBUTTON_GRADIENT_B( 198.0f/256, 142.0f/256, 149.0f/256 );
const Vec3f COLOR_WINDOW_TITLEBAR( 78.0f/256, 78.0f/256, 83.0f/256 );
const Vec3f COLOR_DISPLAYLABEL( 109.0f/256, 113.0f/256, 118.0f/256 );
const Vec3f COLOR_TEXTBOX( 187.0f/256, 192.0f/256, 200.0f/256 );
const Vec3f COLOR_CHECKBOX_UNCHECKED( 186.0f/256, 186.0f/256, 186.0f/256 );
const Vec3f COLOR_CHECKBOX_CHECKED( 186.0f/256, 222.0f/256, 186.0f/256 );
const float UI_OPACITY = 0.7f;

#include "../rendering/font.h"

const uint WIDGET_STANDARD_HEIGHT(12);
const uint WIDGET_HALF_HEIGHT( WIDGET_STANDARD_HEIGHT/2 );
const uint WINDOW_COLLAPSE_BUTTON_WIDTH(WIDGET_STANDARD_HEIGHT);
const uint FONT_DEFAULT_POSITION_X(2);
const uint FONT_DEFAULT_POSITION_Y( (WIDGET_STANDARD_HEIGHT - static_cast<uint>(FONT_DEFAULT_SIZE)) / 2 );

#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../rendering/materialManager.h"
#include "../sprites/spriteList.h"
#include "./window.h"
#include "../input/input.h"

Widget::~Widget( void ) {
    Input::UnsetFocusWidget( this );
    Input::UnsetDragWidget( this );
    delete vbo_borderColor;
}

Widget::Widget( const uint width, const uint height )
    : Sprite( width, height )
    , vbo_borderColor( nullptr )
    , borderColor()
    , tooltip()
    , tabstopindex(0)
    , enabled( true )
    , dragable( false )
    {

    Construct();
}

Widget::Widget( void )
    : Sprite( 50, WIDGET_STANDARD_HEIGHT )
    , vbo_borderColor( nullptr )
    , borderColor( COLOR_UI_BORDER )
    , tooltip()
    , tabstopindex(0)
    , enabled( true )
    , dragable( false )
    {

    Construct();
}

void Widget::Construct( void ) {
    face.SetColor( COLOR_UI_BACKGROUND );
    SetBorderColor( COLOR_UI_BORDER );
}

void Widget::SetBorder( bool whether ) {
    if ( whether ) {
        if ( ! vbo_borderColor )
            vbo_borderColor = new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY);
    } else {
        DELNULL(vbo_borderColor);
    }
}

void Widget::UpdateVBO( void )  {
    Sprite::UpdateVBO();

    if ( vbo_borderColor ) // widget might not have a border
        vbo_borderColor->Clear();
}

bool Widget::TakeInput( [[maybe_unused]] const char* str ) {
    return false;
}

void Widget::PressEnter( void ) {
}

void Widget::PressUp( void ) {
    if ( Window* window = dynamic_cast< Window* >( this ) ) {
        if ( window->IsMenu() ) {
            Input::NavigateMenu( MenuNavDirectionT::Up );
            return;
        }
    }

    if ( !parent )
        return;

    if ( Window* window = dynamic_cast< Window* >( parent ) ) {
        if ( window->IsMenu() ) {
            Input::NavigateMenu( MenuNavDirectionT::Up );
        }
    }
}

void Widget::PressDown( void ) {
    if ( Window* window = dynamic_cast< Window* >( this ) ) {
        if ( window->IsMenu() ) {
            Input::NavigateMenu( MenuNavDirectionT::Down );
            return;
        }
    }

    if ( !parent )
        return;

    if ( Window* window = dynamic_cast< Window* >( parent ) ) {
        if ( window->IsMenu() ) {
            Input::NavigateMenu( MenuNavDirectionT::Down );
        }
    }
}

void Widget::PressLeft( void ) {
}

void Widget::PressRight( void ) {
}

bool Widget::PressDelete( void ) {
    return false;
}

bool Widget::PressBackspace( void ) {
    return false;
}

void Widget::PressPageUp( void ) {
}

void Widget::PressTab( void ) {
    MenuNavDirectionT direction;

    if ( Input::IsKeyDown(SDLK_LSHIFT) || Input::IsKeyDown(SDLK_RSHIFT) ) {
        direction = MenuNavDirectionT::Up;
    } else {
        direction = MenuNavDirectionT::Down;
    }

    Input::NavigateMenu( direction );
}

void Widget::PressPageDown( void ) {
}

void Widget::MouseWheelUp( void ) {
}

void Widget::MouseWheelDown( void ) {
}

void Widget::HasLostFocus( void ) {
    SetColor( Color4f(0,0,0,0) ); //un-highlight
}

void Widget::HasGainedFocus( void ) {
    SetColor( FOCUS_WIDGET_HIGHTLIGHT_COLOR );
}

void Widget::Trigger( [[maybe_unused]] const UITriggerTypeT uiTriggerType_unused ) {

}

void Widget::pack_vbo( void ) const {
    if ( !vbo_borderColor || vbo_borderColor->Finalized() )
        return;

    vbo_borderColor->Clear();

    std::size_t i=face.NumVert();
    while ( i>0 ) {
        --i;

        vbo_borderColor->Pack( borderColor );
        vbo_borderColor->Pack( UI_OPACITY );
    }

    if ( material && material->GetDiffuse() ) {
        vbo_uv->MoveToVideoCard(); // bg image
    }

    vbo_secondary->MoveToVideoCard(); // bg color

    if ( vbo_borderColor )
        vbo_borderColor->MoveToVideoCard();
}

void Widget::DrawVBO( void ) const {
    Sprite::DrawVBO();

    if ( ! vbo_borderColor )
        return;

    ASSERT( vbo_vert );

    if ( !vbo_borderColor->Finalized() || !vbo_vert->Finalized() ) {
        pack_vbo();
    }

    const bool hasFocus = Input::GetFocusWidget() == this;

    if ( vbo_borderColor ) {
        if ( hasFocus ) {
            const Color4f selected_border_color(1,0,0,1);
            shaders->UseProg(  GLPROG_MINIMAL  );
            shaders->SetUniform4f("vColor", selected_border_color );
        } else {
            shaders->UseProg(  GLPROG_GRADIENT  );
            shaders->SetAttrib( "vColor", *vbo_borderColor );
        }

        shaders->SendData_Matrices();
        shaders->SetAttrib( "vPos", *vbo_vert ); // border is the same vertices as the sprite
        shaders->DrawArrays( GL_LINE_LOOP, 0, vbo_vert->Num() );
    }
}

void Widget::Raise( void ) {
    if ( !parent )
        return;

    auto& list = parent->children->list;

    // find our link in our parent's element list so we can push it to the end
    for ( uint i=0; i < list.size(); ++i ) {
        if ( i == list.size() - 1 )
            break;

        Widget *w = static_cast< Widget* >( list[i] );
        if ( w == this ) {
            std::swap( list[i], list[list.size()-1] );
            break;
        }
    }

    // now push the parent to the end of it's own parent's elements list
    Widget* w = dynamic_cast< Widget* >( parent );
    if ( w )
        w->Raise();
}

void Widget::Click( void ) {
}

void Widget::SetVisible( const bool whether ) {
    Sprite::SetVisible( whether );
}

Widget* Widget::GetTabstop( const MenuNavDirectionT direction ) {
    if ( ! parent )
        return this;

    Widget* next = nullptr;
    Uint8 cur_idx=0;

    for ( auto& sprite : parent->children->list ) {
        if ( ! sprite )
            continue;

        if ( ! sprite->IsVisible() )
            continue;

        Widget* widget = static_cast< Widget* >( sprite );
        Uint8 idx = widget->tabstopindex;

        // zero means it does not have a tabstop
        if ( idx == 0 )
            continue;

        if ( direction == MenuNavDirectionT::Up ) {
            if ( idx < tabstopindex ) {
                if ( cur_idx == 0 || cur_idx < idx ) {
                    next = widget;
                    cur_idx = idx;
                }
            }
        } else {
            if ( idx > tabstopindex ) {
                if ( cur_idx == 0 || cur_idx > idx ) {
                    next = widget;
                    cur_idx = idx;
                }
            }
        }
    }

    if ( next )
        return next;

    return this;
}

void Widget::FocusFirstTabstop( void ) {
    Widget* focus_widget = nullptr;
    Uint8 focus_idx = 255;

    for ( auto& sprite : children->list ) {
        if ( !sprite )
            continue;

        Widget* widget = static_cast< Widget* >( sprite );

        if ( !widget->IsVisible() )
            continue;

        const Uint8 idx = widget->GetTabstopIndex();
        if ( idx < 1 )
            continue;

        if ( !focus_widget || idx < focus_idx ) {
            focus_widget = widget;
            focus_idx = idx;
            continue;
        }
    }

    Input::SetFocusWidget( focus_widget );
}

bool Widget::SetOverlayImage( const char* materialName ) {
    if ( materialName && materialName[0] != '\0' ) {
        material = materialManager.Get( materialName );
    }

    if ( material && material->GetDiffuse() ) {
        SetupUV();
        
        // widgets with overlay images won't have borders
        // if we want a border, we must set it after setting the overlay image
        SetBorder( false );
        return true;
    }

    RemoveUV();
    return false;
}

bool Widget::IsDragable( void ) const {
    return dragable;
}

void Widget::SetDragable( const bool whether ) {
    dragable = whether;
}

void Widget::SetTabstopIndex( const Uint8 to ) {
    tabstopindex = to;
}

Uint8 Widget::GetTabstopIndex( void ) const {
    return tabstopindex;
}

void Widget::SetToolTip( const char* to ) {
    tooltip = to;
}

std::string Widget::GetClassToolTip( void ) const {
    return tooltip;
}

bool Widget::HasBorder( void ) const {
    return vbo_borderColor != nullptr;
}

bool Widget::Enabled( void ) const {
    return enabled;
}

void Widget::Enabled( const bool xenabled ) {
    enabled = xenabled;
}

void Widget::SetBorderColor( const Vec3f& newcolor ) {
    borderColor = newcolor;
}
