// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_UI_LISTBOX
#define SRC_UI_LISTBOX

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./widget.h"
#include "../misc.h"
#include "../ui/button.h"

class ListBox;

/*!

ListBoxItem \n\n

In order to scroll/select with up/down keys and joystick buttons, each listbox item must be given sequentail tab stops.

**/

class ListBoxItem : public Button {

public:
    ListBoxItem( void ) = delete;
    ListBoxItem( const char* text, const uint width, const uint height = WIDGET_STANDARD_HEIGHT  );
    ListBoxItem( const ListBoxItem& other ) = delete;
    ListBoxItem& operator=( const ListBoxItem& other ) = delete;

public:
    void Click( void ) override;
    void SetID( const std::string& to );
    std::string GetID( void ) const;
    void PressTab( void ) override;
    void PressUp( void ) override;
    void PressDown( void ) override;
    void HasGainedFocus( void ) override;

private:
    std::string id;
};

class ListBox : public Widget {
public:
    ListBox( void ) = delete;
    ListBox( const int visibleElements, const int firstItemTabstopIndex, FuncThatTakesStringVector _func, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    ListBox( const ListBox& other ) = delete;

public:
    void MouseWheelUp( void ) override;
    void MouseWheelDown( void ) override;

    void Add( const std::vector< std::string >& newItem ); //!< will not call Refresh(), you must do this when your're done adding items
    void Remove( const uint index ); //!< Removed argument_data[i], does not call Refresh()
    void Clear( void ); //!< this function will call Refresh()
    virtual void Refresh( void ); //!< implement in derived classes the way in which list items should be updated

    void Sort( void );

    uint Num( void ) const;
    
    bool SelectItemByIndex( const int index );
    void SelectItemByID( const std::string& item_id ); //!< calls our function on the first set of matching arguments in the list
    void SelectItemByName( const std::string& item_name ); //!< calls our function on the first set of matching arguments in the list
private:
    void SelectItem_Helper( const std::string& arg_string, const uint arg_index ); //!< calls our function on the first set of matching arguments in the list
public:
    void SelectLastItem( void );
    virtual void Deselect( void );

    std::string GetSelection( void ) const;

    void HasGainedFocus( void ) override;
    void Scroll( const int childTabStopIndex, const MenuNavDirectionT direction );

protected:
    virtual void CreateItems( const uint y_pos_begin ); //!< implement in derived classes the way in which the displayed widgets will be created
    void UpdateSelection( void ); //!< implement in derived classes the way in which the displayed widgets will be created
    virtual void CallFunction( const std::vector< std::string >& arguments );

public:
    std::vector< std::vector< std::string > > argument_data; //!< a list of argument lists.

protected:
    FuncThatTakesStringVector func;
    int topOfList; //!< the index of the first item that is shown at the top of the list
    int numRows; //!< total number of rows of items (independent of number of children since we may have multiple children on a row, such as a label next to a textbox)
    int selection;
    int topTabstopIndex;
    int bottomTabstopIndex;
    bool created; //!< whether the list item widgets have been created yet
};

inline uint ListBox::Num( void ) const {
    return argument_data.size();
}

#endif  // SRC_UI_LISTBOX

