// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_UI_WIDGET_H_
#define SRC_UI_WIDGET_H_

class Widget;

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../sprites/sprite.h"

extern const Vec3f COLOR_UI_BACKGROUND;
extern const Vec3f COLOR_UI_BORDER;
extern const Vec3f COLOR_UI_FONT;
extern const Vec3f COLOR_UI_FONT_2;
extern const Vec3f COLOR_WINDOW_BACKGROUND_A;
extern const Vec3f COLOR_WINDOW_BACKGROUND_B;
extern const Vec3f COLOR_WINDOW_BORDER;
extern const Vec3f COLOR_BUTTON_GRADIENT_A;
extern const Vec3f COLOR_BUTTON_GRADIENT_B;
extern const Vec3f COLOR_RADIOBUTTON_GRADIENT_B;
extern const Vec3f COLOR_WINDOW_TITLEBAR;
extern const Vec3f COLOR_DISPLAYLABEL;
extern const Vec3f COLOR_TEXTBOX;
extern const Vec3f COLOR_CHECKBOX_CHECKED;
extern const Vec3f COLOR_CHECKBOX_UNCHECKED;
extern const float UI_OPACITY;

extern const uint WIDGET_STANDARD_HEIGHT;
extern const uint WIDGET_HALF_HEIGHT;
extern const uint WINDOW_COLLAPSE_BUTTON_WIDTH;
extern const uint FONT_DEFAULT_POSITION_X;
extern const uint FONT_DEFAULT_POSITION_Y;

typedef bool MenuNavDirectionT_BaseType;
enum class MenuNavDirectionT : MenuNavDirectionT_BaseType;

class SpriteList;

typedef int UITriggerTypeT_BaseType;
enum class UITriggerTypeT : UITriggerTypeT_BaseType {
    Activate
    , Changed
};

class Widget : public Sprite {

public:
    Widget( void );
    Widget( const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    ~Widget( void ) override;
    Widget( const Widget& other ) = delete;
private:
    void Construct( void );

public:
    Widget& operator=( const Widget& other ) = delete;

public:
    void Enabled( const bool xenabled );
    bool Enabled( void ) const;

    void SetBorder( bool whether );
    bool HasBorder( void ) const;
    void SetBorderColor( const Vec3f& newcolor );

    virtual void Click( void );
    void Raise( void ); //!< raises the widget and all of its parents so that they are on top of other widgets

    virtual bool TakeInput( const char* str );

    virtual void PressEnter( void );
    virtual void PressUp( void );
    virtual void PressDown( void );
    virtual void PressLeft( void );
    virtual void PressRight( void );
    virtual bool PressDelete( void );
    virtual bool PressBackspace( void );
    virtual void PressPageUp( void );
    virtual void PressPageDown( void );
    virtual void PressTab( void );
    virtual void MouseWheelUp( void );
    virtual void MouseWheelDown( void );

    virtual void Trigger( const UITriggerTypeT uiTriggerType_unused ); //!< used by children of the parent to trigger it in some way
    void HasLostFocus( void ); //!< called by Input class when this widget has become unfocused
    virtual void HasGainedFocus( void ); //!< called by Input class when this widget has become focused

    Widget* GetTabstop( const MenuNavDirectionT direction );

    void SetToolTip( const char* to );
    std::string GetClassToolTip( void ) const;

    void SetVisible( const bool whether ) override;

    void SetTabstopIndex( const Uint8 to );
    Uint8 GetTabstopIndex( void ) const;

    bool SetOverlayImage( const char* materialName );

    void FocusFirstTabstop( void );

    bool IsDragable( void ) const;
    void SetDragable( const bool whether );

public:
    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;
protected:
    void pack_vbo( void ) const;

protected:
    VBO<float>* vbo_borderColor;
    Vec3f borderColor;
    std::string tooltip;
    Uint8 tabstopindex;
    bool enabled;
    bool dragable;
};

#endif  // SRC_UI_WIDGET_H_

