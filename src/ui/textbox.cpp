// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./textbox.h"
#include "../input/input.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../editor/editor.h"
#include "../sprites/spriteList.h"

TextBox::TextBox( void )
    : maxlen(100)
{
    Construct();
}

TextBox::TextBox( const char* text, const uint width, const uint height )
    : Label( text, width, height )
    , maxlen(100)
{
    Construct();
}

void TextBox::Construct( void ) {
    face.SetColor( COLOR_TEXTBOX );
    face.SetOpacity( UI_OPACITY );
    SetBorder( true );
}

TextBox::~TextBox( void ) {
    Input::UnsetFocusWidget( static_cast< Widget* >( this ) );
}

bool TextBox::TakeInput( const char* str ) {
    const uint prev_len = label.size();

    label += str;

    const uint new_len = label.size();

    // truncate if we've exceeded the max length
    if ( new_len >= maxlen ) 
        label.resize( maxlen );

    // we will return true even if we only added part of the text
    if ( new_len != prev_len ) {
        fi.color = COLOR_UI_FONT_2;
        UpdateVBO();

        Changed();

        return true;
    }

    return false;
}

void TextBox::Changed( void ) const {
    if ( parent ) {
        if ( Widget* window = dynamic_cast< Widget* >( parent ) ) {
            window->Trigger( UITriggerTypeT::Changed );
        }
    }
}

bool TextBox::PressBackspace( void ) {
    //todostring
    bool ret = label.size() > 0;
    if ( ret ) {
        fi.color = COLOR_UI_FONT_2;
        label.resize(label.size()-1);
        Changed();
        UpdateVBO();
    }

    return ret;
}

void TextBox::DrawVBO( void ) const {
    Label::DrawVBO();
}

void TextBox::Click( void ) {
    Label::Click();
    Input::SetFocusWidget( this );
}

/*!

TextBox_FuncWithArg \n\n

A textbox that calls a function with a string vector as an argument when clicked

*/


TextBox_FuncWithArg::TextBox_FuncWithArg( const char* _text, FuncThatTakesStringVector _func, const std::vector<std::string>& _args, const uint width, const uint height )
    : TextBox( _text, width, height )
    , func( _func )
    , arguments( _args )
{ }

void TextBox_FuncWithArg::PressEnter( void ) {
    std::vector< std::string > pass( arguments );
    pass.push_back( label );

    if ( func )
        return (*func)( pass );
}
