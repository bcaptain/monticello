// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./listbox.h"
#include "../sprites/spriteList.h"
#include "./label.h"
#include "../input/input.h"

void ListBox::MouseWheelDown( void ) {
    ASSERT( children );

    if ( ( static_cast<int>(Num()) - topOfList ) > numRows ) {
        ++topOfList;
        Refresh();
    }
}

void ListBox::MouseWheelUp( void ) {
    if ( topOfList > 0 ) {
         --topOfList; Refresh();
         Refresh();
    }
}

ListBox::ListBox( const int visibleElements, const int firstItemTabstopIndex, FuncThatTakesStringVector _func, const uint width, const uint height )
    : Widget( width, height )
    , argument_data()
    , func( _func )
    , topOfList(0)
    , numRows(visibleElements)
    , selection( -1 )
    , topTabstopIndex( firstItemTabstopIndex )
    , bottomTabstopIndex( -1 )
    , created( false )
{
    ASSERT( ! children );
    children = new SpriteList( this );
}

void ListBox::Remove( const uint index ) {
    if ( index >= argument_data.size() )
        return;

    argument_data.erase( std::cbegin(argument_data) + static_cast<int>( index ) );
    if ( selection >= static_cast<int>(Num()) )
        selection = static_cast<int>(Num())-1;
}

void ListBox::Add( const std::vector< std::string >& argData ) {
    argument_data.push_back( {argData} );
}

void ListBox::Clear( void ) {
    argument_data.clear();
    topOfList = 0;
    selection = -1;
    Refresh();
}

void ListBox::Refresh( void ) {
    if ( ! created ) {
        const bool y_start = 0;
        CreateItems(y_start);
    }

    if ( topOfList < 0 )
        topOfList = 0;

    if ( numRows < 1 )
        numRows = 1;

    for ( uint idx = static_cast< uint >( topOfList ); idx < static_cast< uint >( numRows+topOfList ); ++idx ) {

        // ** set up the item names we're going to be searching for
        // ** we must search because children can be in any of order

        // we start at the zeroth element and on
        const std::string intString( std::to_string( idx-static_cast<uint>(topOfList) ) );

        std::string labelName( "ListBox_Label" );

        labelName += intString;

        // ** find the widgets by name

        ListBoxItem* item = dynamic_cast< ListBoxItem* >( children->FindNamed( labelName ) );

        if ( !item ) {
            ERR( "ListBox: could not find or cast child: %s\n.", labelName.c_str() );
            continue;
        }

        Input::UnsetFocusWidget( item );

        // ** update their label/etc based on our arguments

        std::string label,id;

        if ( idx < argument_data.size() ) {
            switch ( argument_data[idx].size() ) {
                case 3: tooltip = argument_data[idx][2]; FALLTHROUGH;
                case 2: id = argument_data[idx][1]; FALLTHROUGH;
                case 1: label = argument_data[idx][0]; FALLTHROUGH;
                default: break;
            }
        } else {
            item->SetVisible( false );
            continue;
        }

        item->SetVisible( true );
        item->SetLabel( label );
        item->SetID( id );
        item->SetToolTip( tooltip.c_str() );
    }

    UpdateSelection();
}

void ListBox::UpdateSelection( void ) {
    if ( selection > -1 && selection >= static_cast<int>(Num()) ) {
        selection = -1;
    }
}

void ListBox::CreateItems( const uint y_pos_begin ) {
    if ( created ) {
        ERR("ListBox: Items already created.\n");
        return;
    }    
    created = true;
    bottomTabstopIndex = numRows + topTabstopIndex - 1;

    // set a default height for the listbox if we haven't already specified one
    if ( static_cast< uint >( GetHeight() ) == WIDGET_STANDARD_HEIGHT ) {
        SetHeight( static_cast< float >( WIDGET_STANDARD_HEIGHT * static_cast<uint>(numRows) ) );
    }

    const uint listBoxWidth = static_cast<uint>( GetWidth() );

    Vec3f nextLabelPos(0,y_pos_begin,0);

    for ( int i=0; i<numRows; ++i ) {
        // ** create item names
        const std::string intString( std::to_string( i ) );

        std::string labelName( "ListBox_Label" );

        labelName += intString;

        // ** create the label
        ListBoxItem* item = new ListBoxItem( "", listBoxWidth );
        item->SetOrigin( nextLabelPos );
        item->SetName( labelName );
        item->SetTabstopIndex(topTabstopIndex+i);

        children->Add( item );

        // ** set up the position of the next widgets
        nextLabelPos.y += WIDGET_STANDARD_HEIGHT;
    }
}

std::string ListBox::GetSelection( void ) const {
    if ( selection < 0 || static_cast< uint >( selection ) >= argument_data.size() )
        return "";

    if ( argument_data[0].size() < 1 )
        return "";

    return argument_data[static_cast<uint>(selection)][0];
}

void ListBox::Sort( void ) {
    std::sort( std::begin( argument_data ), std::end( argument_data ) );
}

void ListBox::SelectLastItem( void ) {
    if ( Num() > 0 )
        SelectItemByIndex( static_cast<int>(Num()) - 1 );
}

bool ListBox::SelectItemByIndex( const int index ) {
    if ( index >= static_cast<int>(Num()) )
        return false;

    if ( index < 0 ) {
        Deselect();
        return false;
    }

    selection = index;
    CallFunction( argument_data[static_cast<uint>(index)] );
    return true;
}

void ListBox::SelectItemByID( const std::string& item_id ) {
    const uint id_index = 1;
    SelectItem_Helper( item_id, id_index );
}

void ListBox::SelectItemByName( const std::string& item_name ) {
    const uint name_index = 0;
    SelectItem_Helper( item_name, name_index );
}

void ListBox::SelectItem_Helper( const std::string& arg_string, const uint arg_index ) {
    uint i;

    for ( i=0; i<static_cast<uint>(Num()); ++i ) {
        if ( argument_data[i].size() < 2 )
            continue;

        if ( argument_data[i][arg_index] == arg_string )
            break; // found it
    }

    if ( i >= static_cast<uint>( Num() ) ) {
        return; // not found
    }

    selection = static_cast<int>(i);
    CallFunction( argument_data[i] );
}

void ListBox::CallFunction( const std::vector< std::string >& arguments ) {
    if ( func )
        (*func)(arguments);
}

void ListBox::Deselect( void ) {
    selection = -1;
}


void ListBox::HasGainedFocus( void ) {
    FocusFirstTabstop();
}

void ListBox::Scroll( const int childTabStopIndex, const MenuNavDirectionT direction ) { 
    if ( childTabStopIndex == 1 && direction == MenuNavDirectionT::Up ) {
        MouseWheelUp();
        return;
    } else if ( childTabStopIndex == bottomTabstopIndex && direction == MenuNavDirectionT::Down ) {
        MouseWheelDown();
        return;
    } else {
        Input::NavigateMenu( direction );
    }
}

ListBoxItem::ListBoxItem( const char* text, const uint width, const uint height  )
    : Button( text, ButtonTypeT::None, width, height )
    , id()
{ }

void ListBoxItem::Click( void ) {
    if ( ! parent )
        return;

    ListBox* list = dynamic_cast< ListBox* >(parent);
    if ( ! list )
        return;

    list->SelectItemByID( id );
}

void ListBoxItem::PressTab( void ) {
    if ( Input::IsKeyDown(SDLK_LSHIFT) || Input::IsKeyDown(SDLK_RSHIFT) ) {
        PressUp();
    } else {
        PressDown();
    }
}

void ListBoxItem::PressDown( void ) {
    if ( !parent ) {
        ERR("ListBoxItem without a parent.\n");
        return;
    }

    ListBox* owner = static_cast<ListBox*>( parent );
    owner->Scroll( tabstopindex, MenuNavDirectionT::Down );
}

void ListBoxItem::PressUp( void ) {
    if ( !parent ) {
        ERR("ListBoxItem without a parent.\n");
        return;
    }

    ListBox* owner = static_cast<ListBox*>( parent );
    owner->Scroll( tabstopindex, MenuNavDirectionT::Up );
}

void ListBoxItem::HasGainedFocus( void ) {
    // todo: Click(); // when a listbox entry is highlighted/focused/etc, it is selected and thus clicked.
    // this is not true for editor add-mode UI
}

void ListBoxItem::SetID( const std::string& to ) {
    id = to;
}

std::string ListBoxItem::GetID( void ) const {
    return id;
}
