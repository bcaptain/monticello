// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./modelBox.h"
#include "../rendering/renderer.h"
#include "../rendering/models/modelManager.h"
#include "../rendering/materialManager.h"
#include "../game.h"

ModelBox::ModelBox( const uint width, const uint height )
    : Widget( width, height )
    , materialName()
    , modelName()
    , modelInfo()
    , drawInfo()
    , spinRate()
{
    material = std::make_shared<FBO>( globalVals.GetUInt( gval_v_width  ), globalVals.GetUInt( gval_v_height ) );

    ASSERT( ! vbo_uv );
    vbo_uv = new VBO<float>(2,VBOChangeFrequencyT::RARELY);
    vbo_uv->PackQuad2D_CCW_BR();
    vbo_uv->MoveToVideoCard();
    ASSERT( vbo_uv->Finalized() );
    
    //SetBorder( false );
}

void ModelBox::DrawVBO( void ) const {    
    std::shared_ptr< const Model > model = modelInfo.GetModel();
    if ( ! model )
        return;

    const std::shared_ptr< const FBO > fbo = std::static_pointer_cast<const FBO>( material );

    AABox3D modelBounds = model->GetBounds();

    const Vec3f len(
        modelBounds.max.x - modelBounds.min.x,
        modelBounds.max.y - modelBounds.min.y,
        modelBounds.max.z - modelBounds.min.z
    );

    const Vec3f mid(
        len.x / 2 + modelBounds.min.x,
        len.y / 2 + modelBounds.min.y,
        len.z / 2 + modelBounds.min.z
    );

    const float len_max = std::fmaxf( len.x, std::fmaxf( len.y, len.z ) );

    fbo->Bind();
        glClearColor(0, 0, 0, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // ** backup culling mode
        const bool culling = glIsEnabled( GL_CULL_FACE );
        int culling_mode;
        glGetIntegerv( GL_CULL_FACE_MODE, &culling_mode );
        glDisable( GL_CULL_FACE );

            uchar depth_state=0;
            glGetBooleanv( GL_DEPTH_TEST, &depth_state );
            glEnable( GL_DEPTH_TEST ); // this is to be always enabled at the exit of every function.

                glClearDepth(1);
                glClear(GL_DEPTH_BUFFER_BIT);

                    renderer->PushMatrixMV();
                        renderer->PushMatrixPJ();
                            renderer->Projection().Perspective(
                                90,
                                globalVals.GetFloat( gval_v_width ),
                                globalVals.GetFloat( gval_v_height ),
                                globalVals.GetFloat( gval_v_nearPlane ),
                                globalVals.GetFloat( gval_v_farPlane )
                            );
                            renderer->ModelView().LoadIdentity();

                            // zoom out so the model can be seen
                            const float extra = 0.23f; // we zoom just a little extra so that it's not a perfect fit on the screen (if rotating, might otherwise roate outside the bounds of the screen)
                            renderer->ModelView().Translate(0,0,-len_max - extra);

                            const float rot = game->GetRenderFrame();

                            if ( !Maths::Approxf( spinRate.z, 0 ) )
                                renderer->ModelView().Rotate( rot*spinRate.z, 0,0,1 );

                            if ( !Maths::Approxf( spinRate.y, 0 ) )
                                renderer->ModelView().Rotate( rot*spinRate.y, 0,1,0 );

                            if ( !Maths::Approxf( spinRate.x, 0 ) )
                                renderer->ModelView().Rotate( rot*spinRate.x, 1,0,0 );

                            renderer->ModelView().Rotate( 90, 1,0,0 );
                            renderer->ModelView().Rotate( 180, 0,0,1 );

                            // center the model at origin
                            renderer->ModelView().Translate(-mid.x, -mid.y, -mid.z);

                            modelInfo.Draw( drawInfo );
                        renderer->PopMatrixPJ();
                    renderer->PopMatrixMV();

                if ( !depth_state )
                    glDisable( GL_DEPTH_TEST ); // this is to be always enabled at the exit of every function.

        glCullFace( static_cast< GLenum >( culling_mode ) );
        if ( !culling ) {
            glDisable( GL_CULL_FACE );
        } else {
            glEnable( GL_CULL_FACE );
        }

    fbo->Unbind();

    Widget::DrawVBO();
}

void ModelBox::UpdateVBO( void )  {
    Widget::UpdateVBO();
}

void ModelBox::SetModel( const std::string& to ) {
    modelInfo.SetModel( modelManager.Get( to.c_str() ) );
}

void ModelBox::SetOpacity( const float to ) {
    drawInfo.opacity = to;
}

void ModelBox::SetMaterial( const std::string& to ) {
    modelInfo.SetMaterial( to.c_str() );
}

void ModelBox::SetSpinRate( const Vec3f& to ) {
    spinRate = to;
}

Vec3f ModelBox::GetSpinRate( void ) const {
    return spinRate;
}
