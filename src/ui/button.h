// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_BUTTON_H_
#define SRC_UI_BUTTON_H_

#include "../base/main.h"
#include "./label.h"
#include "../misc.h"

typedef uint ButtonTypeT_BaseType;
enum class ButtonTypeT : ButtonTypeT_BaseType {
    None,
    Accept,
    Decline,
    Option1,
    Option2
};

class Button : public Label {
public:
    Button( void );
    Button( const char* text, const ButtonTypeT btnType, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    Button( const Button& other ) = delete;
    ~Button( void ) override;
private:
    void Construct( void );

public:
    Button& operator=( const Button& other ) = delete;

public:
    void PressEnter( void ) override;
    void Click( void ) override;
    ButtonTypeT GetButtonTypeT( void ) const { return type; }

public:
    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;
protected:
    std::shared_ptr< const Material > GetJoyButtonMaterial( void ) const;
    bool DrawJoyButtonImageVBO( void ) const; //!< returns true if it was drawn
    void pack_vbo( void ) const;

private:
    ButtonTypeT type;
    static Face2D joyButtonImage;
    static std::unique_ptr< VBO<float> > vbo_padButtonVert;
    static std::unique_ptr< VBO<float> > vbo_padButtonUV;
};

/*!

Button_TriggerParent \n\n

A button that calls parent->Trigger()

**/

class Button_TriggerParent : public Button {
public:
    Button_TriggerParent( void );
    Button_TriggerParent( const char* text, const ButtonTypeT btnType, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );

public:
    void Click( void ) override;
};

// ****
//
// ButtonFunc
// Button that takes a function pointer to execute when clicked.
//
// ****

class Button_Func : public Button {
public:
    Button_Func( void );
    Button_Func( const char* text, void(*click_function)( void), const ButtonTypeT btnType, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );

public:
    void Click( void ) override;
    void PressEnter( void ) override;

private:
    void(*click_func)( void );
};

// ****
//
// ButtonFunc_Arg
// Button that takes a function pointer to execute when clicked (takes a string as an argument)
//
// ****

class Button_FuncWithArg : public Button {
public:
    Button_FuncWithArg( void ) = delete;
    Button_FuncWithArg( const char* text, FuncThatTakesStringVector click_function, const std::vector< std::string >& arg, const ButtonTypeT btnType, const uint width, const uint height = WIDGET_STANDARD_HEIGHT  );

public:
    void Click( void ) override;
    void PressEnter( void ) override;

public:
    std::vector< std::string > arguments;

private:
    void(*click_func)( const std::vector< std::string >& arg );
};

#endif  // SRC_UI_BUTTON_H_

