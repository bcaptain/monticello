// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./label.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../rendering/fontManager.h"

void Label::UpdateVBO( void )  {
    ASSERT( vbo_textcoord );
    ASSERT( vbo_textvert );
    Widget::UpdateVBO();
    vbo_textvert->Clear();
    vbo_textcoord->Clear();
}

void Label::DrawVBO( void ) const {
    Widget::DrawVBO();
    DrawLabelVBO();
}

void Label::DrawLabelVBO( void ) const {
    // we don't draw the label text if we have an overlay image
    if ( vbo_uv )
        return;

    if ( label.size() == 0 )
        return;

    ASSERT( vbo_textcoord );
    ASSERT( vbo_textvert );

    if ( !vbo_textvert->Finalized() || !vbo_textcoord->Finalized() ) {
        pack_vbo();
        // if it still isn't finalized, all we have is whitespace
        if ( !vbo_textvert->Finalized() || !vbo_textcoord->Finalized() )
            return;
    }

    std::shared_ptr< const Font > fnt = fontManager.Get( fi.fontname.c_str() );
    Font::AssertValid( fnt, fi.fontname.c_str() );

    shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
    shaders->SendData_Matrices();
    shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
    shaders->SetUniform1f("fWeight", 1.0f );
    shaders->SetUniform4f("vColor", fi.color.x,fi.color.y,fi.color.z,fi.opacity );
    shaders->SetAttrib( "vUV", *vbo_textcoord );
    shaders->SetAttrib( "vPos", *vbo_textvert );
    shaders->DrawArrays( GL_QUADS, 0, vbo_textvert->Num() );
}

void Label::pack_vbo( void ) const {
    ASSERT( vbo_textvert );
    ASSERT( vbo_textcoord );

    vbo_textvert->Clear();
    vbo_textcoord->Clear();

    if ( label.size() < 1 )
        return;

    const uint num_faces_packed = vbo_textvert->PackText( *vbo_textcoord, label.c_str(), fi, nullptr, this );

    // if no faces were packed, it's all whitespace
    if ( num_faces_packed > 0 ) {
        vbo_textvert->MoveToVideoCard();
        vbo_textcoord->MoveToVideoCard();
    }
}

void Label::SetFontPos( const Vec2f& org ) {
    fi.pos = org;
    UpdateVBO();
}

void Label::SetFontSize( const float val ) {
    fi.size_ui_px = val;
    UpdateVBO();
}

void Label::SetFontColor( const Vec3f& to ) {
    fi.color = to;
    UpdateVBO();
}

Label::Label( const char* text, const uint width, const uint height )
    : Widget( width, height )
    , nextCharPos()
    , label( text )
    , vbo_textvert( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_textcoord( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , fi()
{
    Construct();
}

Label::Label( const char* text )
    : nextCharPos()
    , label( text )
    , vbo_textvert( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_textcoord( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , fi()
    {
    Construct();
}

Label::Label( void )
    : nextCharPos()
    , label()
    , vbo_textvert( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_textcoord( new VBO<float>(2,VBOChangeFrequencyT::FREQUENTLY) )
    , fi()
    {
    Construct();
}

void Label::Construct( void ) {
    SetFaceOpacity( 0 );
    fi.opacity = UI_OPACITY;
    fi.color = COLOR_UI_FONT;
}

Label::~Label( void ) {
    delete vbo_textvert;
    delete vbo_textcoord;
}

void Label::SetLabel( const char *n ) {
    label = n;
    UpdateVBO();
}

void Label::SetLabel( const std::string& n ) {
    label = n;
    UpdateVBO();
}

void Label::AdjustSizeForText( void ) {
    if ( label.size() < 1 )
        return;

    uint longestLine = 0;
    uint numLines = 1;
    uint curLineLen = 0;

    for ( uint i=0; i < label.size(); ++i ) {
        if ( label[i] == '\n' ) {
            curLineLen = 0;
            if ( curLineLen > longestLine ) {
                longestLine = curLineLen;
            }
            ++numLines;
        } else {
            ++curLineLen;
        }
    }

    if ( numLines < 1 )
        numLines = 1;

    if ( longestLine < 1 )
        longestLine = curLineLen;

    SetWidth( longestLine * fi.size_ui_px );
    SetHeight( numLines * fi.size_ui_px );
}

std::string Label::GetLabel( void ) const {
    return label;
}

float Label::GetFontSize( void ) const {
    return fi.size_ui_px;
}

void Label::SetFontOpacity( const float val ) {
    fi.opacity = val;
}
