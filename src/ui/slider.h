// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_SLIDER_H_
#define SRC_UI_SLIDER_H_

#include "../base/main.h"
#include "./button.h"
#include "./textbox.h"
#include "../sprites/spriteList.h"

extern const float OPACITY_SLIDER_KNOB;
extern const uint SLIDER_KNOB_WIDTH;

template<typename TYPE>
class Slider;

template<typename TYPE>
class SliderKnob : public Button {
    friend class Slider<TYPE>;

public:
    SliderKnob( void ) = delete;
    SliderKnob( const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    SliderKnob( const SliderKnob& other ) = delete;

public:
    SliderKnob& operator=( const SliderKnob& other ) = delete;

public:
    void Click( void ) override;
    void SetAmount( const int new_amount );
    void UpdateOrigin( void );

private:
    void DragUpdate( void );

public:
    Slider<TYPE> *owner;
    float click_x;
};

class SliderBase : public TextBox {
public:
    SliderBase( void ) = delete;
    SliderBase( const char* text, const uint thewidth, const uint theheight = WIDGET_STANDARD_HEIGHT );
    SliderBase( const SliderBase& other ) = delete;
private:
    void Construct( void );

public:
    SliderBase& operator=( const SliderBase& other ) = delete;

public:
    virtual void ValFromLabel( void ) = 0;

    virtual void AdjustButtonPositions( void ) = 0;
    virtual void SetValFromFraction( const float frac ) = 0;
    virtual void CheckVal( void ) = 0;
    virtual void DragUpdate( void ) = 0;
    virtual float GetSliderWidth( void ) const;

protected:
    float sliderWidth;
};

template<typename TYPE>
class Slider : public SliderBase {
    friend class SliderKnob<TYPE>;

public:
    Slider( void ) = delete;
    Slider( const char* text, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    Slider( const Slider& other ) = delete;

public:
    Slider& operator=( const Slider& other ) = delete;

public:
    void SetOrigin( const Vec3f& org ) override;
    void SetOrigin( const float x, const float y, const float z ) override;
    void SetVisible( const bool whether ) override;
    bool TakeInput( const char* chr ) override;
    void PressEnter( void ) override;
    void UpdateSliderLabel( void );
    void ValFromLabel( void ) override;
    void SetMethod( void (*method)( const TYPE ) ) { setter_function = method; }

    void AdjustButtonPositions( void ) override;
    TYPE GetVal( void ) const;
    void SetMax( const TYPE set_max );
    void SetMin( const TYPE set_min );
    void SetValFromFraction( const float frac ) override;
    void CheckVal( void ) override;
    void DragUpdate( void ) override;
    virtual void SetVal( const TYPE to );

    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;

public:
    SliderKnob<TYPE> *sliderKnob;
    void (*setter_function)( const TYPE to );

protected:
    int clickPos;
    TYPE min_val;
    TYPE max_val;

private:
    TYPE val; //!< SetVal() is the only method that modifies this, important for derived classes.
};

template<typename TYPE>
SliderKnob<TYPE>::SliderKnob( const uint thewidth, const uint height )
    : Button( "", ButtonTypeT::None, thewidth, height )
    , owner(nullptr)
    , click_x()
    {
    SetBorder( true );
}

template<typename TYPE>
void SliderKnob<TYPE>::UpdateOrigin( void ) {
    if ( ! Maths::Approxf( owner->max_val - owner->min_val, 0 ) ) {
        // get percentage position based on value
        float pos = static_cast< float >( owner->val );
        pos -= static_cast< float >( owner->min_val );

        float denom = static_cast< float >( owner->max_val );
        denom -= static_cast< float >( owner->min_val );

        pos /= denom;

        // translate to pixel coordinates
        pos *= owner->GetSliderWidth() - SLIDER_KNOB_WIDTH;

        origin.x = pos;

        if ( origin.x < 0 )
            origin.x = 0.0f;

        if ( origin.x > owner->GetSliderWidth() )
            origin.x = owner->GetSliderWidth();

    }

    SetOrigin(origin);
}

template<typename TYPE>
Slider<TYPE>::Slider( const char* text, const uint width, const uint height )
    : SliderBase( text, width, height )
    , sliderKnob(nullptr)
    , setter_function(nullptr)
    , clickPos(0)
    , min_val(0)
    , max_val(100)
    , val(0)
    {

    ASSERT( children );

    SetBorder( true );

    Slider<TYPE>::face.BuildRect( 120, WIDGET_STANDARD_HEIGHT );

    Slider<TYPE>::sliderKnob = new SliderKnob<TYPE>(SLIDER_KNOB_WIDTH,WIDGET_STANDARD_HEIGHT);
    Slider<TYPE>::sliderKnob->owner = this;
    Slider<TYPE>::sliderKnob->SetFaceOpacity( OPACITY_SLIDER_KNOB );

    Slider<TYPE>::AdjustButtonPositions();

    children->list.push_back( sliderKnob );
}

template<typename TYPE>
bool Slider<TYPE>::TakeInput( const char* str ) {
    return TextBox::TakeInput( str );
}

template<typename TYPE>
void Slider<TYPE>::PressEnter( void ) {
    ValFromLabel();
    UpdateSliderLabel();
    sliderKnob->UpdateOrigin();
}

template<typename TYPE>
void Slider<TYPE>::SetVal( const TYPE to ) {
    Slider<TYPE>::val = to;

    UpdateSliderLabel();
    AdjustButtonPositions();

    if ( setter_function )
        (*setter_function)( Slider<TYPE>::val );
}

template<typename TYPE>
inline TYPE Slider<TYPE>::GetVal( void ) const {
    return val;
}

template<typename TYPE>
inline void Slider<TYPE>::SetMax( const TYPE newmax ) {
    max_val = newmax;
}

template<typename TYPE>
inline void Slider<TYPE>::SetMin( const TYPE newmin ) {
    min_val = newmin;
}

template<typename TYPE>
void Slider<TYPE>::SetOrigin( const Vec3f& org ) {
    Widget::SetOrigin( org );
    AdjustButtonPositions();
}

template<typename TYPE>
void Slider<TYPE>::SetOrigin( const float x, const float y, const float z ) {
    Widget::SetOrigin( x,y,z );
    AdjustButtonPositions();
}

template<typename TYPE>
void Slider<TYPE>::CheckVal( void ) {
    if ( val > max_val )
        SetVal( max_val );
    else if ( val < min_val )
        SetVal( min_val );
}

template<typename TYPE>
void Slider<TYPE>::SetValFromFraction( const float frac ){
    SetVal( static_cast<TYPE>( ( frac * static_cast< float >(max_val - min_val) ) + min_val ) );
    CheckVal();
}

template<typename TYPE>
void Slider<TYPE>::SetVisible( const bool whether ) {
    TextBox::SetVisible( whether );
    sliderKnob->SetVisible(whether);
}

template<typename TYPE>
void Slider<TYPE>::AdjustButtonPositions( void ) {
    sliderWidth = GetWidth();
    sliderKnob->UpdateOrigin();
    UpdateVBO();
}

template<typename TYPE>
inline void Slider<TYPE>::DragUpdate( void ) {
    sliderKnob->DragUpdate();
}

template< typename TYPE >
void Slider<TYPE>::DrawVBO( void ) const {
    TextBox::DrawVBO();
}

template< typename TYPE >
void Slider<TYPE>::UpdateVBO( void )  {
    TextBox::UpdateVBO();
}

#endif  // SRC_UI_SLIDER_H_
