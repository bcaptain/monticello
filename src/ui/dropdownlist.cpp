// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./dropdownlist.h"
#include "../ui/textbox.h"
#include "../ui/button.h"
#include "../input/input.h"
#include "../sprites/spriteList.h"

DropDownList::DropDownList( const int visibleElements, const int firstItemTabstopIndex, FuncThatTakesStringVector _func, const uint width, const uint height )
    : ListBox( visibleElements, firstItemTabstopIndex, _func, width, height )
    , btnOpenList( nullptr )
    , textbox( nullptr )
    , childrenVisible( false )
{
    const uint textWidth = static_cast<uint>(GetWidth()) - WIDGET_STANDARD_HEIGHT;
    textbox = new TextBox( "", textWidth );

    btnOpenList = new Button_TriggerParent( "v", ButtonTypeT::None, WIDGET_STANDARD_HEIGHT, WIDGET_STANDARD_HEIGHT );
    btnOpenList->SetOrigin( Vec3f(textWidth,0,0) );

    children->Add( textbox );
    children->Add( btnOpenList );

    CreateItems( WIDGET_STANDARD_HEIGHT );
}

DropDownList::~DropDownList( void ) {
}

void DropDownList::Refresh( void ) {
    Sort();

    if ( topOfList < 0 )
        topOfList = 0;

    if ( numRows < 1 )
        numRows = 1;

    for ( uint idx = static_cast< uint >( topOfList ); idx < static_cast< uint >( numRows+topOfList ); ++idx ) {

        // ** set up the item names we're going to be searching for
        // ** we must search because children can be in any of order

        // we start at the zeroth element and on
        const std::string intString( std::to_string( idx-static_cast<uint>(topOfList) ) );

        std::string labelName( "ListBox_Label" );

        labelName += intString;

        // ** find the widgets by name

        ListBoxItem* btnEntry = dynamic_cast< ListBoxItem* >( children->FindNamed( labelName ) );

        if ( !btnEntry ) {
            ERR( "ListBox: could not find or cast child: %s\n.", labelName.c_str() );
            continue;
        }

        // ** update their label/etc based on our arguments

        std::string label, id;

        if ( idx < argument_data.size() ) {
            switch ( argument_data[idx].size() ) {
                default:
                case 3: tooltip = argument_data[idx][2]; FALLTHROUGH;
                case 2: id = argument_data[idx][1]; FALLTHROUGH;
                case 1: label = argument_data[idx][0]; FALLTHROUGH;
                case 0: break;
            }
        } else {
            btnEntry->SetVisible( false );
            label.clear();
            tooltip.clear();
        }

        btnEntry->SetLabel( label );
        btnEntry->SetToolTip( tooltip.c_str() );
        btnEntry->SetID( id );
    }

    UpdateSelection();
}

void DropDownList::Deselect( void ) {
    selection = -1;
    textbox->SetLabel( "" );
}

void DropDownList::CreateItems( const uint y_pos_begin ) {
    if ( created ) {
        ERR("ListBox: Items already created.\n");
        return;
    }
    created = true;
    bottomTabstopIndex = numRows + topTabstopIndex - 1;

    const uint listBoxWidth = static_cast<uint>( GetWidth() );

    Vec3f nextLabelPos(0,y_pos_begin,0);

    for ( int i=0; i<numRows; ++i ) {
        // ** create item names
        const std::string intString( std::to_string( i ) );

        std::string labelName( "ListBox_Label" );

        labelName += intString;

        // ** create the label
        ListBoxItem* item = new ListBoxItem( "", listBoxWidth );
        item->SetOrigin( nextLabelPos );
        item->SetName( labelName );
        item->SetTabstopIndex(topTabstopIndex+i);
        children->Add( item );

        // ** set up the position of the next widgets
        nextLabelPos.y += WIDGET_STANDARD_HEIGHT;
    }

    Close();
}

void DropDownList::SetLabel( const char *n ) {
    if ( textbox )
        textbox->SetLabel( n );
}

void DropDownList::SetLabel( const std::string& n ) {
    if ( textbox )
        textbox->SetLabel( n );
}

void DropDownList::SetOpen( const bool whether ) {
    childrenVisible = whether;

    for ( auto& child : children->list )
        if ( child && child != textbox && child != btnOpenList )
            child->SetVisible( whether );
}

void DropDownList::Trigger( const UITriggerTypeT uiTriggerType ) {
    switch ( uiTriggerType ) {
        case UITriggerTypeT::Activate: ToggleOpen(); break;
        case UITriggerTypeT::Changed: Search(); break;
        default: break;
    }
}

void DropDownList::Search( void ) {/* listsearchtodo */
    // we're only searching if the list is open. otherwise, we might be trying to type something differen
    if ( !childrenVisible )
        return;

    const std::string text = textbox->GetLabel();
    Sort();

    // search the list for a match to the text in our textbox
    int i;
    for ( i=0; i < static_cast< int >( argument_data.size() ); ++i ) {
        if ( argument_data[static_cast<uint>(i)].size() < 1 )
            continue;

        std::string checkString( argument_data[static_cast<uint>(i)][0] );

        if ( checkString.size() > text.size() )
            checkString.resize( text.size() );

        if ( checkString < text )
            continue;

        if ( checkString == text )
            break; // we found a match, lets take it

        if ( checkString > text ) {
            return; // nothing found for this, don't scroll the lsit
        }
    }

    if ( static_cast< int >( Num() ) - i < numRows )
        i = static_cast< int >( Num() ) - numRows;

    topOfList = i;
    Refresh();
}

std::string DropDownList::GetLabel( void ) const {
    return textbox->GetLabel();
}

void DropDownList::CallFunction( const std::vector< std::string >& arguments ) {
    if ( arguments.size() > 0 ) {
        textbox->SetLabel( arguments[0] );
    } else {
        textbox->SetLabel( "" );
    }

    Close();

    ListBox::CallFunction( arguments );
}

void DropDownList::RemoveSelected( void ) {
    if ( selection >= 0 )
        Remove( static_cast< uint >( selection ) );
}

int DropDownList::GetSelectedIndex( void ) const {
    return selection;
}

void DropDownList::ToggleOpen( void ) {
    SetOpen( !childrenVisible );
}

void DropDownList::Open( void ) {
    SetOpen( true );
}

void DropDownList::Close( void ) {
    SetOpen( false );
}
