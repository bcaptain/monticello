// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_CHECKBOX_H_
#define SRC_UI_CHECKBOX_H_

#include "../base/main.h"
#include "./button.h"
#include "../misc.h"

class CheckBox : public Button {
public:
    CheckBox( void ) = delete;
    CheckBox( void(*click_function)( const bool checked ) );
    CheckBox( const CheckBox& other ) = delete;
    ~CheckBox( void ) override;

public:
    CheckBox& operator=( const CheckBox& other ) = delete;

public:
    void Click( void ) override;
    void PressEnter( void ) override;
    
    void Check( const bool checked );
    
private:
    void(*clickFunc)( const bool checked );
};

#endif  // SRC_UI_CHECKBOX_H_

