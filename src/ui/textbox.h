// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_TEXTBOX_H_
#define SRC_UI_TEXTBOX_H_

#include "../base/main.h"
#include "./label.h"
#include "../misc.h"

extern Widget* focusWidget;

class TextBox : public Label {
public:
    TextBox( void );
    TextBox( const char* text, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    TextBox( const TextBox& other ) = delete;
    ~TextBox( void ) override;
private:
    void Construct( void );

public:
    TextBox& operator=( const TextBox& other ) = delete;

    void Click( void ) override;
    bool TakeInput( const char* str ) override;
    bool PressBackspace( void ) override;
    void DrawVBO( void ) const override;

private:
    void Changed( void ) const;
    
private:
    uint maxlen;
};

class TextBox_FuncWithArg : public TextBox {
public:
    TextBox_FuncWithArg( void ) = delete;
    TextBox_FuncWithArg( const char* _text, FuncThatTakesStringVector _func, const std::vector<std::string>& _args, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    TextBox_FuncWithArg( const TextBox_FuncWithArg& other ) = delete;

public:
    TextBox_FuncWithArg& operator=( const TextBox_FuncWithArg& other ) = delete;

public:
    void PressEnter( void ) override;

public:
    FuncThatTakesStringVector func;
    std::vector<std::string> arguments;
};


#endif  // SRC_UI_TEXTBOX_H_
