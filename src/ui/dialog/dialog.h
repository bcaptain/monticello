// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_DIALOG_DIALOG_H
#define SRC_UI_DIALOG_DIALOG_H

#include "../../base/main.h"
#include "../window.h"
#include "../../rendering/order.h"

extern const uint DEFAULT_DIALOG_WIDTH;
extern const uint DEFAULT_DIALOG_HEIGHT;
extern const uint DIALOG_BUTTON_WIDTH;
extern const uint DIALOG_BUTTON_HEIGHT;

class Dialog;

template< typename F >
struct DialogButton : public Button {
    DialogButton( void ) = delete;
    DISABLE_COPY( DialogButton )
    DialogButton( const char* text, Dialog& dlg, const ButtonTypeT type );
    DialogButton( const char* text, Dialog& dlg, F _action, const ButtonTypeT type );
    void Click( void ) override;
private:
    Dialog* dialog;
    F action;
    bool has_action;
};

// ****
//
// Dialog
// a Dialog on the screen that can have a background image/color/transparency
//
// ****

class Dialog : public Window {
private: // Dialogs can only be created by Dialog::Create()
    Dialog( void );
    Dialog( const char* message, const uint width = DEFAULT_DIALOG_WIDTH, const uint height = DEFAULT_DIALOG_HEIGHT );
    Dialog( const Dialog& other ) = delete;
private:
    void Construct( const char* message, const uint width, const uint height );

public:
    ~Dialog( void ) override;

public:
    Dialog& operator=( const Dialog &other ) = delete;

public:
    static Dialog* Create( const char* message, const uint width = DEFAULT_DIALOG_WIDTH, const uint height = DEFAULT_DIALOG_HEIGHT, const UI_LayerT layer=UI_LAYER_PROGRAM_WINDOW_DIALOG );
    void Close( void );
    void AddButton( Button* btn );
    void Accept( void );
    void Decline( void );

private:
    void ButtonClicked( const ButtonTypeT type );

private:
    int next_button_x;
    int spriteLayer;
};

template< typename F >
DialogButton<F>::DialogButton( const char* text, Dialog& dlg, const ButtonTypeT btnType )
    : Button( text, btnType, DIALOG_BUTTON_WIDTH, DIALOG_BUTTON_HEIGHT )
    , dialog( &dlg )
    , action()
    , has_action( false )
{
    ASSERT( dialog );
}

template< typename F >
DialogButton<F>::DialogButton( const char* text, Dialog& dlg, F _action, const ButtonTypeT btnType )
    : Button( text, btnType, DIALOG_BUTTON_WIDTH, DIALOG_BUTTON_HEIGHT )
    , dialog( &dlg )
    , action( _action )
    , has_action( false )
{
    ASSERT( dialog );
}

template< typename F >
void DialogButton<F>::Click( void ) {
    action();
    ASSERT( dialog );
    dialog->Close();
}

#endif  // SRC_UI_DIALOG_DIALOG_H

