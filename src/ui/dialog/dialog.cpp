// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./dialog.h"
#include "../../game.h"

#include "../../input/input.h"
#include "../../sprites/spriteList.h"

const uint DEFAULT_DIALOG_WIDTH = 300;
const uint DEFAULT_DIALOG_HEIGHT = 150;
const uint DIALOG_BUTTON_WIDTH = 80;
const uint DIALOG_BUTTON_HEIGHT = 20;

Dialog::Dialog( void )
    : Window( WindowOptionsT::NO_COLLAPSE_BUTTON )
    , next_button_x( 0 )
    , spriteLayer( UI_LAYER_GAMEUI_DIALOG )
{
    Construct( "Default dialog text", DEFAULT_DIALOG_WIDTH, DEFAULT_DIALOG_HEIGHT );
}

Dialog::Dialog( const char* message, const uint width, const uint height )
    : Window( WindowOptionsT::NO_COLLAPSE_BUTTON, width, height )
    , next_button_x( 0 )
    , spriteLayer( UI_LAYER_GAMEUI_DIALOG )
{
    Construct( message, width, height );
}

void Dialog::Construct( const char* message, const uint width, const uint height ) {
    const uint x = ( 800 - width ) / 2;
    const uint y = ( 600 - height ) / 2;

    const Vec3f org( x, y, 0 );
    SetOrigin( org );

    CreateTitle("Dialog");

    auto const title_bar_height = WIDGET_STANDARD_HEIGHT;
    Label* lbl = new Label(message, width, height - DIALOG_BUTTON_HEIGHT - title_bar_height );
    lbl->SetOrigin(0,next_element_vpos,0);
    children->Add( lbl );
}

Dialog::~Dialog( void ) {
    game->Unpause( PauseStateT::PausedByDialog );
}

Dialog* Dialog::Create( const char* message, const uint width, const uint height, const UI_LayerT layer ) {
    if ( layer > UI_LAYER_TOP ) {
        DIE("Invalid layer for Dialog: %u,\n", static_cast<UI_LayerT_BaseType>(layer));
        return {};
    }
    
    game->Pause( PauseStateT::PausedByDialog );

    auto dialog = new Dialog( message, width, height );
    dialog->spriteLayer = layer;
    game->sprites[layer].list.push_back( dialog );
    return dialog;
}

void Dialog::Close( void ) {
    SetVisible( false ); // let the system know this window is done
    game->sprites[spriteLayer].Del( this );
    Input::UpdateFocusWidget();
}

void Dialog::AddButton( Button* btn ) {
    ASSERT( btn );
    const Vec3f pos( next_button_x, GetHeight() - DIALOG_BUTTON_HEIGHT, 0 );
    btn->SetOrigin( pos );
    children->Add( btn );
    next_button_x += static_cast< int >( btn->GetWidth() );
}

void Dialog::Accept( void ) {
    ButtonClicked( ButtonTypeT::Accept );
}

void Dialog::Decline( void ) {
    ButtonClicked( ButtonTypeT::Decline );
}

void Dialog::ButtonClicked( const ButtonTypeT type ) {
    for ( auto& widget : children->list ) {
        if ( Button* btn = dynamic_cast< Button* >( widget ) ) {
            if ( btn->GetButtonTypeT() == type ) {
                btn->Click();
            }
        }
    }
}
