// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_DIALOG_COMMON_H
#define SRC_UI_DIALOG_COMMON_H

#include "../../base/main.h"
#include "./dialog.h"

void Dialog_LoadMap( const std::string& mapname );
void Dialog_ReloadMap( void );

#ifdef MONTICELLO_EDITOR
void Dialog_SaveLoadedMap( void );
void Dialog_SaveMap( const std::string& mapname );
#endif //MONTICELLO_EDITOR

void Dialog_ToggleEditor( void );

void DialogFunc_Cancel( void );

NORETURN void DialogFunc_Abort( void );

void Dialog_Message( const std::string& msg );

template <class ...Args>
void DIALOG( const std::string& fmt, const Args&... args );

template <class ...Args>
void ERR_DIALOG( const std::string& fmt, const Args&... args );

void Dialog_FatalError( const char* msg );

template <class ...Args>
void WARN_DIALOG( const std::string& fmt, const Args&... args );

template <class ...Args>
void DIALOG( const std::string& fmt, const Args&... args ) {
    auto const msg = Getf( fmt, args... );
    CMSG_LOG( msg );
    DIALOG( msg );
}

template <class ...Args>
void ERR_DIALOG( const std::string& fmt, const Args&... args ) {
    auto const msg = Getf( fmt, args... );
    ERR( msg );
    #ifdef MONTICELLO_DEBUG
        Dialog_Message( std::string( "Error: ") + msg );
    #endif // MONTICELLO_DEBUG
}

template <class ...Args>
void WARN_DIALOG( const std::string& fmt, const Args&... args ) {
    auto const msg = Getf( fmt, args... );
    WARN( msg );
    #ifdef MONTICELLO_DEBUG
        Dialog_Message( std::string( "Warning: ") + msg );
    #endif // MONTICELLO_DEBUG
}

    
#endif  // SRC_UI_DIALOG_COMMON_H

