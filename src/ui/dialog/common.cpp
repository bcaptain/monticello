// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./common.h"
#include "../../game.h"
#include "../../input/input.h"
#include "../../rendering/renderer.h"

void DialogFunc_Cancel( void ) {
    game->Unpause( PauseStateT::PausedByDialog );
}

void DialogFunc_Abort( void ) {
    game->SetState( GameStateT::ABORT );
}

void Dialog_LoadMap( const std::string& mapName ) {
    auto loadMapFunc = std::bind(
        static_cast< void(Game::*)(const std::string&) >( &Game::LoadMap )
        , game
        , mapName
    );

    std::string msg("Load map \"");
    msg += mapName;
    msg += "\"?";
    auto dlg = Dialog::Create( msg.c_str() );
    dlg->AddButton( new DialogButton<decltype(loadMapFunc) >( "Yes", *dlg, loadMapFunc, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton< void(*)() >( "No", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
}

#ifdef MONTICELLO_EDITOR
void Dialog_SaveMap( const std::string& mapname ) {
    if ( !game->CheckIfOkayToSaveMap() )
        return;

    auto saveMapFunc = std::bind(
        static_cast< bool(Game::*)(const std::string&) >( &Game::SaveMapAs )
        , game
        , mapname
    );

    std::string msg("Save map as \"");
    msg += mapname;
    msg += "\"?";
    auto dlg = Dialog::Create( msg.c_str() );
    dlg->AddButton( new DialogButton<decltype(saveMapFunc) >( "Yes", *dlg, saveMapFunc, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton< void(*)() >( "No", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
}
#endif // MONTICELLO_EDITOR

void Dialog_ReloadMap( void ) {
    auto reloadMap = [](){ game->ReloadMap(); };

    auto dlg = Dialog::Create( "Reload map?" );
    dlg->AddButton( new DialogButton<decltype(reloadMap) >( "Yes", *dlg, reloadMap, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton< void(*)() >( "No", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
}

void Dialog_Message( const std::string& msg ) {
    auto dlg = Dialog::Create( msg.c_str() );
    dlg->AddButton( new DialogButton< void(*)() >( "Ok", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
}

void Dialog_FatalError( const char* msg ) {
    auto dlg = Dialog::Create( msg );
    dlg->AddButton( new DialogButton< void(*)() >( "Abort", *dlg, &DialogFunc_Abort, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton< void(*)() >( "Continue", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
    
    // just update input and render until user clicks abort.
    while ( game->IsPausedBy( PauseStateT::PausedByDialog ) && ( game->GetState() != GameStateT::ABORT ) ) {
        game->UpdateTime();
        Input::ProcessInput();
        renderer->DrawGLScene();
    }
    
    if ( game->GetState() == GameStateT::ABORT ) {
        assert(false);
        abort();
    }
}

#ifdef MONTICELLO_EDITOR
void Dialog_SaveLoadedMap( void ) {
    auto saveIt = [](){ game->SaveLoadedMap(); };

    auto dlg = Dialog::Create( "Save the loaded map?" );
    dlg->AddButton( new DialogButton<decltype(saveIt) >( "Yes", *dlg, saveIt, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton< void(*)() >( "No", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
}

void Dialog_ToggleEditor( void ) {
    auto toggleEditor = [](){
        ASSERT( game );
        game->ToggleEditor();
    };

    const std::string turnoff_message( "If the game starts, saving the map will not be allowed.\n\nTurn editor off?" );
    const std::string turnon_message( "If the game has started, saving the map will not be allowed until the map is reloaded.\n\nTurn editor on?" );

    auto dlg = Dialog::Create( game->editor_open ? turnoff_message.c_str() : turnon_message.c_str() );
    dlg->AddButton( new DialogButton<decltype(toggleEditor) >( "Yes", *dlg, toggleEditor, ButtonTypeT::Accept ) );
    dlg->AddButton( new DialogButton< void(*)() >( "No", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
}
#endif //MONTICELLO_EDITOR
