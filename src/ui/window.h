// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_UI_WINDOW_H_
#define SRC_UI_WINDOW_H_

#include "../base/main.h"
#include "./button.h"

typedef Uint8 WindowOptionsT_BaseType;
enum class WindowOptionsT : WindowOptionsT_BaseType {
    DEFAULT = 0,
    INNER = 1,
    NO_COLLAPSE_BUTTON = 1<<1
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( WindowOptionsT, WindowOptionsT_BaseType )

class GameUI_Page;
class Window;

class ButtonWinCollapse : public Button {
public:
    ButtonWinCollapse( void ) = delete;
    explicit ButtonWinCollapse( Window* xowner );
    ButtonWinCollapse( const ButtonWinCollapse& other ) = delete;
    ~ButtonWinCollapse( void );

public:
    ButtonWinCollapse& operator=( const ButtonWinCollapse& other ) = delete;

public:
    void DrawVBO( void ) const override;
    void UpdateVBO( void )  override;
protected:
    void pack_vbo( void ) const;

public:
    void Click( void ) override;

private:
    Window* owner;
    VBO<float> *vbo_arrow;
};

// Window
// a window on the screen that can have a background image/color/transparency

class Window : public Widget {
public:
    Window( void );
    explicit Window( const WindowOptionsT type );
    Window( const WindowOptionsT type, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    Window( const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    Window( const Window& other ) = delete;
    ~Window( void ) override;
private:
    void Construct( void );

public:
    Window& operator=( const Window &other ) = delete;

public:
    void DeleteElementsBelowYPos( const uint y_border ); //!< NOTE: Assumes all widgets are in decending order (top of screen to bottom) and packed that way in elemen_visibility as well
    void DeleteAllElements( void );
    Widget* GetFirstTabstop( void ) const;
    bool ShowNamedElement( const std::string& str, const bool whether );
    void ToggleCollapsed( void ); //!< constructs element_visibility in proper order (parellel to Window)
    bool IsCollapsed( void ) const;
    bool HasCollapseButton( void ) const;
    bool ClickButton( const ButtonTypeT type );
    void SetPage( GameUI_Page* newPage );
    GameUI_Page* GetPage( void ) const; //!< get what page (if any) this window belongs to
    void SetMenu( const bool whether ); //!< set whether this should be treated as a menu (arrow keys navigate the tabstops)
    bool IsMenu( void ) const;
    void HasGainedFocus( void ) override;

public:
    //! create a new window. calling function is responsible for making sure resource gets deleted
    static Window* NewWindow( const char* wnd_name, const char* wnd_title, const uint wnd_width, const uint wnd_height, Window* parent = nullptr );

private:
    //! initializes a window created with NewWindow()
    static void InitNewWindow( Window& wnd, const char* wnd_name, const char* wnd_title, Window* parent = nullptr );

protected:
    void CreateTitle( const char* wnd_title );

public: //todo: private
    LinkList< bool > element_visibility; //!< this list, when populated, is parallel to elements in Windows, and also assumes all widgets are in decenging order (top of screen to bottom)

private:
    WindowOptionsT wndOpts;
public: //todo: private
    uint next_element_vpos;
private:
    GameUI_Page* page;
    float oldheight;
    bool collapsed;
    bool isMenu;
};

// elements are considered part of title bar if they're above WIDGET_STANDARD_HEIGHT.
// all elements will be properly repositioned after this block of code via RepositionElements()

class WindowTitle : public Label {
public:
    WindowTitle( void ) = delete;
    WindowTitle( const WindowTitle& other ) = delete;
    WindowTitle( const char* title, const uint width, const uint height = WIDGET_STANDARD_HEIGHT );
    ~WindowTitle( void ) override;

public:
    WindowTitle& operator=( const WindowTitle& other ) = delete;
};

#endif  // SRC_UI_WINDOW_H_

