// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_FBO
#define SRC_RENDERING_FBO

#include "../base/main.h"
#include "./material.h"

// todo: it would be bad if Material::SetDiffuse() got called

class FBO : public Material {
public:
    FBO( void ) = delete;
    FBO( const int xwidth, const int yheight );
    FBO( const FBO& other );
    ~FBO( void ) override;

public:
    FBO& operator=( const FBO& other );

private:
    uint CreateTexBuffer( void ) const;
    uint CreateDepthBuffer( void ) const;

public:
    int GetWidth( void ) const;
    int GetHeight( void ) const;

    void Bind( void ) const; //!< bind the FBO to be drawn to
    void Unbind( void ) const; //!< rebind the default framebuffer to be drawn to

private:
    std::shared_ptr<uint> fbo_id;
    std::shared_ptr<uint> tex_id; //!< we keep a copy of this here in case Material::SetDiffuse() gets called or something
    uint depth_id;
    int width; //!< we keep a copy of this in case Material::SetDiffuse() gets called or something
    int height; //!< we keep a copy of this in case Material::SetDiffuse() gets called or something
};

#endif  // SRC_RENDERING_FBO
