// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "../sdl/sdl.h"
#include "./textureManager.h"
#include "./texinfo.h"
#include "../filesystem.h" // for OverrideWarning

bool TextureManager::instantiated(false);
TextureManager textureManager;

TextureManager::TextureManager( void )
    : AssetManager( "Texture" )
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

void TextureManager::Reload( void ) {
    CMSG_LOG("Reloading all textures.\n");
    
    File tex_file;
    AssetData< Texture >* tex;

    for ( const auto data : manifest ) {
        // ** don't load textures that weren't loaded
        if ( !data.second.IsLoaded() )
            continue;

        if ( ! tex_file.Open( data.second.GetPath().c_str() ) ) {
            ERR("Couldn't reload texture, could not open file: %s\n", data.second.GetPath().c_str() );
            continue;
        }

        // if the manifest says it's loaded, but it's not, there's a problem.
        tex = manifest.Get( data.second.GetName().c_str() );
        if ( !tex ) {
            ERR("Couldn't reload texture, could not determine texture ID: %s\n", data.second.GetPath().c_str() );
            continue;
        }

        // because we have texture information, it'll load the texture into the same ID as before
        
        ImageData image_data;
        if ( !LoadImageData( tex_file, image_data ) )
            return;
            
        image_data.SetName( data.second.GetName() );
        
        if ( !BindImageData( *tex->data, tex_file, tex->data->GetID(), image_data ) ) {
            return;
        }
    }
}

bool TextureManager::GenerateImageFileDef( File& opened_texture_file, ImageFileDef& imageDef_out ) {
    // ** make sure the file is open

    if ( !opened_texture_file.IsOpen() ) {
        ERR("Couldn't load texture %s.\n", opened_texture_file.GetFilePath().c_str() );
        return false;
    }

    // ** genereate imageDef including the name of the texture from the filename
    imageDef_out.path = opened_texture_file.GetFilePath();

    if ( imageDef_out.path.empty() ) {
        ERR("Texture does not specify an image: %s\n", opened_texture_file.GetFilePath().c_str() );
        return false;
    }

    imageDef_out.name = String::TrimExtentionFromFileName( String::GetFileFromPath(opened_texture_file.GetFilePath().c_str() ) );

    if ( imageDef_out.name.size() == 0 ) {
        ERR("Texture does not have name: %s.\n", opened_texture_file.GetFilePath().c_str() );
        return false;
    }

    imageDef_out.type = "";
    return true;
}

bool TextureManager::Load( File& opened_file ) {
    ImageFileDef imageDef;

    if ( !GenerateImageFileDef( opened_file, imageDef ) )
        return false;

    if ( imageDef.name.size() == 0 ) {
        ERR("Texture not given a name: %s.\n", imageDef.path.c_str());
        return false;
    }

    AssetData< Texture >* manifest_s = manifest.Get( imageDef.name.c_str() );
    if ( !manifest_s ) {
        ERR_DIALOG("Couldn't load texture file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }

    auto tex = std::make_shared< Texture >();
    GLuint id;

    ImageData image_data;
    if ( !LoadImageData( opened_file, image_data ) )
        return false;
    
    const bool asset_existed = manifest_s->data != nullptr;
    
    if ( asset_existed ) {
        id = manifest_s->data->GetID(); // texture id doenst change if it was already loaded
        ASSERT( id != 0 );
    } else {
        glGenTextures( 1, &id );
    }
    
    image_data.SetName( imageDef.name );
    
    if ( !BindImageData( *tex, opened_file, id, image_data ) ) {
        if ( !asset_existed ) {
            ( *glDeleteTextures )( 1, &id );
        }
        
        return false;
    }
    
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), imageDef.name, opened_file );
    
    manifest_s->data = tex;
        
    return true;
}

bool TextureManager::LoadImageData( File& opened_file, ImageData &image_data_out ) {
    // **** for now, all we have is TGA
    ParserTGA parser( opened_file );
    if ( ! parser.Load( image_data_out ) ) {
        ERR("Could not load texture: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }
    
    CMSG_LOG("Loaded texture: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
    return true;
}

bool TextureManager::BindImageData( Texture& ti, File& tex_file, const uint id, const ImageData& image_data ) {
    if ( !ti.Bind( image_data, id ) ) {
        ERR("Could not bind texture: %s from %s\n", tex_file.GetFilePath().c_str(), tex_file.GetFileContainerName_Full().c_str() );
        return false;
    }
    
    return true;
}
