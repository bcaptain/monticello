// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_MODELS_MODELINFO_H_
#define SRC_RENDERING_MODELS_MODELINFO_H_

#include "../../base/main.h"
#include "./model.h"
#include "../material.h"
#include "../animation/armatureInfo.h"
#include "../models/animatedModel.h"

typedef Uint32 AnimStateT_BaseType;
enum class AnimStateT : AnimStateT_BaseType;

/*!

ModelInfo \n\n

Information needed to draw a model such as ArmatureInfo. \n
A ModelInfo may not have a collision model when it has no draw model. \n
If there is no collision model, the draw model will be used in its place. \n

There may be multiple Draw Models. If one is animated, then all must be animated using the same skeleton. \n

This class is self contained such that every index of the vector will have a valid model (none will not be nullptr)

*/

class ModelInfo {
public:
    ModelInfo( void );
    explicit ModelInfo( const std::shared_ptr< const Model >& _model );
    explicit ModelInfo( Entity* _owner );
    ModelInfo( Entity* _owner, const std::shared_ptr< const Model >& _model );
    
    ModelInfo( const ModelInfo& other );
    ModelInfo( ModelInfo&& other_rref );
    
    ModelInfo( Entity* _owner, const ModelInfo& other );
    ModelInfo( Entity* _owner, ModelInfo&& other );
    ~ModelInfo( void );

public:
    ModelInfo& operator=( const ModelInfo& other );
    ModelInfo& operator=( ModelInfo&& other_rref );

public:
    bool SetModel( const std::string& model_name );
    bool SetModel( const std::shared_ptr< const Model >& new_model );
    void UnsetModel( void );
    
    void SetVisible( bool visibility );
    bool IsVisible( void ) const;
    
    bool IsAnimatedModel( void ) const;
    std::size_t GetNumArmatures( void ) const;

    Vec3f GetOffset( void ) const;
    void SetOffset( const Vec3f& offset );

    std::string GetOwnerName( void ) const;
    Entity* GetOwner( void ) const;
    void SetOwner( Entity* _owner );
    
    void Draw( ModelDrawInfo drawInfo_byVal ) const;
    
#ifdef MONTICELLO_DEBUG
    void TestModel_Animate( const char* anim_name ) const;
    void TestModel_Animate( void ) const;
#endif // MONTICELLO_DEBUG

    const ModelInfo* GetBaseModelInfo( void ) const; //!< get the base ModelInfo if we are a attachment, otherwise nullptr
    AnimChannelT_BaseType GetBaseChannel( void ) const;
    std::shared_ptr< const Model > GetModel( void ) const; //!< get draw model
    bool HasDrawModel( void ) const;
    bool HasClipModel( void ) const;
    std::shared_ptr< const Model > GetClipModel( void ) const;
    std::shared_ptr< const Model > GetDrawModel( void ) const;

    ArmatureInfo* GetArmatureInfo( const AnimChannelT channel );
    const ArmatureInfo* GetArmatureInfo( const AnimChannelT channel ) const;

	uint GetNumAnimFrames( const char* anim_name, const AnimChannelT channel ) const;
    
    /*! if the model opened is door.bm1 and you want to set the clipmodel to the file
        "door_opened_cm.bm1", you can pass either "door_opened_cm", or "opened" and
        the function will piece the name together for you.
        If the file does not exist, nothing changes. (if you're trying to unset the clipmodel,
        you should instead just set the entity to ghost or noclip).
        This function will not throw any errors or warnings if the clipmodel is not found. */
    void SetClipModel( const std::shared_ptr< const Model >& to );
    void SetClipModel( const char* cm_name );
    void SetClipModel( const std::string& cm_name );
    void SetClipModelToDefault( void );

private:
    void AddAttachment( const std::shared_ptr< const Model >& model, const AnimChannelT_BaseType channel );
public:
    void AddAttachment( const std::string& attachment_pos, const std::string& attachment_name, const AnimChannelT_BaseType channel );
    void RemoveAttachment(  const std::string& attachment_pos, const std::string& attachment_name  );
    void RemoveAttachmentAtIndex( const uint index );
    bool HasAttachment(  const std::string& attachment_pos, const std::string& attachment_name ) const;
    uint GetAttachmentIndexAtPos( const std::string& attachment_position ) const;

    uint NumAttachments( void ) const;
    const ModelInfo* GetAttachment( const uint index ) const; //!< does not do bounds checking
    ModelInfo* GetAttachment( const uint index ); //!< does not do bounds checking
    
    void SetAttachmentVisibility ( const bool visibility );

    bool SetMaterial( const std::shared_ptr< const Material >& mtl );
    bool SetMaterial( const char* mtl );
    bool SetMaterialToDefault( void ); //!< if there is a model, sets default material if it doesn't have one, and on all it's attachments
    void UnsetMaterial( void );
    std::shared_ptr< const Material > GetMaterial( void ) const;

    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool );
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const;
    
    BonePos GetBonePos( const Uint8 _id, const AnimChannelT channel ) const;
    BonePos GetBonePos( const std::string& _name, const AnimChannelT channel ) const;

    bool HasChannel( const AnimChannelT channel ) const;
    
    bool IsAnimated( void ) const;

    AnimInfo* GetAnimInfo( void );
    const AnimInfo* GetAnimInfo( void ) const;

    void AddAnimState( const AnimStateT animState );
    void RemAnimState( const AnimStateT animState );
    bool HasAnimState( const AnimStateT animState ) const;   
    void ClearAnimState( void );
    
private:
    uint GetAttachmentIndex( const std::string& model_name ) const; //!< returns the index in 'attachments' of the modelInfo matching the name. If none, will return attachments.size()
    std::string GetAttachmentModelName( const std::string& attachment_pos, const std::string& attachment_name ) const;

private:
    std::vector< ModelInfo > attachments;
    friend class AnimInfo;
    std::vector< AnimStateT > animationsToTry;
    std::vector< AnimStateT > animationsToInsert;
    std::vector< AnimStateT > animationsToRemove;
    Vec3f modelOffset;
    std::shared_ptr< const Model > drawModel;
    std::shared_ptr< const Model > clipModel;
    std::shared_ptr< const Material > material;
    std::unique_ptr< AnimInfo > animInfo;
    ModelInfo* base; //!< bases have attachments, attachments cannot be bases. rawptr is fine, attachments cannot exist without base.
    AnimChannelT_BaseType base_channel; //!< the channel of the base model that controls the attachment's animations
    Entity* owner; //rawptr is fine, modelInfo cannot exist without entity
    bool isVisible;
    bool animationsToTry_lock;
};

#endif // SRC_RENDERING_MODELS_MODELINFO_H_
