// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../../game.h"
#include "./modelManager.h"
#include "../../filesystem.h" // for OverrideWarning()
#include "../../files/bm1.h"
#include "../../files/bm1anim.h"
#include "../animation/animCue.h"
#include "../animation/animation.h"

const uint ANIMCUE_PROJECTILE_NAME_INDEX = 0;
const uint ANIMCUE_ENTITY_NAME_INDEX = 0;
const uint ANIMCUE_EFFECT_NAME_INDEX = 0;
const uint ANIMCUE_SOUND_NAME_INDEX = 0;
const uint ANIMCUE_FIRE_BULLET_DAMAGE_NAME_INDEX = 0;

//! lists all the valid positions in which a model attachment can be
const std::vector<const char *> attachmentPositions {
    "rHandItem",
    "lHandItem"
};

ModelData::ModelData( void )
    : AssetData()
    , available_attachments()
{ }

ModelData::ModelData( const std::string& _name, const std::string& _path, const char* _container )
    : AssetData( _name, _path, _container )
    , available_attachments()
{ }

ModelData::ModelData( const ModelData& other )
    : AssetData( other )
    , available_attachments( other.available_attachments )
{ }

ModelData& ModelData::operator=( const ModelData& other ) {
    AssetData::operator=( other );
    available_attachments = other.available_attachments;
    return *this;
}

bool ModelManager::instantiated( false );
ModelManager modelManager;

ModelManager::ModelManager( void )
    : AssetManager("Model")
    , accessory_manifest() //scaleme
    , events_manifest()
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

std::string GetModelFromAnimName( const std::string& file_path );
std::string GetModelFromAnimName( const std::string& file_path ) {
    const std::string model_name = String::TrimExtentionFromFileName( String::GetFileFromPath( file_path ) );

    // get the index of the first underscode character
    std::size_t underscore_index=0;
    while ( true ) {
        if ( underscore_index >= model_name.size() ) {
            ERR("Couldn't get animation name for file: %s\n", file_path.c_str() );
            return std::string("");
        }

        if ( model_name[underscore_index] == '_' )
            break;

        ++underscore_index;
    }

    return String::Left( model_name, underscore_index );
}

std::string GetModelFromAnimName( const char* file_path );
std::string GetModelFromAnimName( const char* file_path ) {
    return GetModelFromAnimName( std::string( file_path ) );
}

std::string StripModelFromAnimName( const std::string& file_path );
std::string StripModelFromAnimName( const std::string& file_path ) {
    const std::string anim_name = String::TrimExtentionFromFileName( String::GetFileFromPath( file_path ) );

    // get the index of the first underscode character
    std::size_t underscore_index=0;
    while ( true ) {
        if ( underscore_index >= anim_name.size() ) {
            ERR("Couldn't strip model from animation name for file: %s\n", file_path.c_str() );
            return std::string("");
        }

        if ( anim_name[underscore_index] == '_' )
            break;

        ++underscore_index;
    }

    return anim_name.substr( underscore_index+1, anim_name.size() ); // from underscore_index+1 to the end
}

std::string StripModelFromAnimName( const char* file_path );
std::string StripModelFromAnimName( const char* file_path ) {
    return StripModelFromAnimName( std::string( file_path ) );
}

void ModelManager::PackAllClipModelVBOs( void ) const {
    for ( uint i=0; i<manifest.GetTableSize(); ++i ) {
        for ( auto& model : manifest ) {
            if ( ! model.second.data )
                continue;
            
            if ( String::RightIs( model.second.data->GetName(), "_cm" ) ) {
                model.second.data->PackVBO();
            }
        }
    }
}

bool ModelManager::Load( File& opened_file ) {
    // ** make sure it's open
    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open model file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    ModelData* manifest_s = manifest.Get( name.c_str() );
    if ( !manifest_s )
        ERR_DIALOG("Couldn't load model file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );

    std::shared_ptr< Model > model=nullptr;
    ParserBM1 parser( opened_file );
    model = parser.Load();
    if ( ! model ) {
        ERR_DIALOG("Couldn't parse model file during load: %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    if ( manifest_s->data )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );

    manifest_s->data = model;
    
    model->SetName( name.c_str() );
    
    // if the model is a collision model, we don't pack the VBO until a developer turns on d_clipModels in the console.
    if ( globalVals.GetBool( gval_d_clipModels ) || !String::RightIs( name, "_cm" ) )
        model->PackVBO();
    
    CMSG_LOG("Loaded model: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
    
    // if we have the model loaded, let's load all animations for it so it doesn't happen during game-play
    if ( model->IsAnimated() ) {
        // Ww must load all anims before the cues (in case the cues reference an anim that isn't yet loaded)
        auto & animModel = *std::static_pointer_cast< AnimatedModel >( model );
        LoadAllAnimsForModel( animModel );
        LoadAllAnimCuesForModel( animModel );
    }
    
    PreloadAttachments( *manifest_s );

    return true;
}

void ModelManager::ManifestAttachments( void ) {
    for ( auto& us : manifest ) {
        std::vector< std::string > prefix;
        for ( auto const & pos : attachmentPositions ) {
            prefix.emplace_back( std::string(us.first) + '-' + pos ); 
        }
        
        for ( auto const & them : manifest ) {
            if ( &us == &them )
                continue;
                
            for ( uint p=0; p<prefix.size(); p++ )
                if ( String::LeftIs( them.first, prefix[p] ) )
                    us.second.available_attachments.emplace_back( them.first );
        }
    }
}

void ModelManager::PreloadAttachments( const ModelData& data ) const {
    for ( auto& modelName : data.available_attachments )
        Preload::PreloadModel( modelName );
}

void ModelManager::RegisterAnim( const std::string& path, const char* container ) {
    const std::string assetName = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );
    const AssetManifest asset_manifest( assetName, path, container );
    const bool asset_existed = accessory_manifest.Set( assetName.c_str(), asset_manifest );
    
    if ( asset_existed )
        OverrideWarning("Anim Manifest", assetName, path, container );
}

void ModelManager::RegisterAnimCue( const std::string& path, const char* container ) {
    const std::string fullName = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );
    const std::string assetName( "cue_" + fullName );
    const AssetManifest asset_manifest( assetName, path, container );
    const bool asset_existed = accessory_manifest.Set( assetName.c_str(), asset_manifest );
    
    if ( asset_existed )
        OverrideWarning("AnimCue Manifest", assetName, path, container );
}

void ModelManager::RegisterAnimEvents( const std::string& path, const char* container ) {
    const std::string assetName = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );
    const AssetData< std::vector< std::pair< uint, std::string > > > asset_manifest( assetName, path, container );
    const bool asset_exists = events_manifest.Get( assetName.c_str() );
    if ( asset_exists ) {
        WontOverrideWarning("AnimEvents Manifest", assetName.c_str(), path, container );
        return;
    }
    events_manifest.Set( assetName.c_str(), asset_manifest );
}

void ModelManager::LoadAllAnimsForModel( AnimatedModel& model ) {
    const std::string prefix( model.GetName() + '_' );
    for ( const auto& data : accessory_manifest ) {
        if ( !String::LeftIs( data.second.GetName(), prefix ) )
            continue;
            
        if ( !String::RightIs( data.second.GetPath(), ".bm1anim" ) )
            continue;

        LoadAnim( model, data.second );
    }
}

void ModelManager::LoadAllAnimCuesForModel( AnimatedModel& model ) {
    const std::string prefix( "cue_" + model.GetName() + '_' );
    for ( const auto& data : accessory_manifest ) {
        if ( !String::LeftIs( data.second.GetName(), prefix ) )
            continue;

        if ( !String::RightIs( data.second.GetPath(), ".cue" ) )
            continue;

        std::string cue_name;
        cue_name = data.second.GetName().substr( prefix.size() );
        LoadAnimCue( model, cue_name, data.second );
    }
}

const std::shared_ptr< const std::vector< std::pair< uint, std::string > > > ModelManager::GetEventList( const std::string& modelName ) {
    AssetData< std::vector< std::pair< uint, std::string > > > * asset = events_manifest.Get( modelName.c_str() );
    if ( ! asset )
        return nullptr; // asset was not manifested, so there is no events file for this model

    if ( asset->data ) {
        return asset->data;
    }
        
    File file;
    OpenAssetFile( "EventList", *asset, file );
    if ( ! file.IsOpen() ) {
        ERR("Couldn't open Events file %s\n", file.GetFilePath().c_str() );
        return nullptr;
    }

    asset->data = std::make_shared< std::vector< std::pair< uint, std::string > > >();
    
    // ** parse the file
    ParserSSV parser( file );
    parser.SetCommentIndicator('#');

    std::string word;
    while ( parser.ReadWord( word ) ) {
        uint eventIndex = String::ToUInt( word );
        
        if ( !parser.ReadWordOnLine( word ) ) {
            ERR("incomplete AnimEvent in file %s, expected an event name.\n", file.GetFilePath().c_str() );
            break;
        }
        
        asset->data->emplace_back( eventIndex, word );
        parser.SkipToNextLine();
    }

    CMSG_LOG("Loaded Cue Events: %s from %s\n", file.GetFilePath().c_str(), file.GetFileContainerName_Full().c_str() );
    return asset->data;
}

bool ModelManager::LoadAnimCue( AnimatedModel& model, const std::string& animName, const AssetManifest& data ) {
    File file;
    return OpenAssetFile( "AnimCue", data, file ) && LoadAnimCue( model, animName, file );
}

bool ModelManager::LoadAnimCue( AnimatedModel& model, const std::string& animName, File& opened_file ) {
    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open AnimCue file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    // ** parse the file
    ParserSSV parser( opened_file );
    parser.SetCommentIndicator('#');
    AnimChannelT channel = AnimChannelT::Channel_0;
    AnimCue cue;

    const char* str_onFrame = "onFrame";
    const char* str_ofChannelID = "ofChannelID";
    const char* str_ofChannelName = "ofChannelName";

    uint last_cue_frame = 0;
    
    // load the event list, if it has one
    const std::shared_ptr< const std::vector< std::pair< uint, std::string > > > eventList = GetEventList( model.GetName() );

    while ( !opened_file.EndOfFile() ) {
        cue.Unset();

        // read "onFrame"
        std::string onFrame;
        if ( !parser.ReadWord( onFrame ) || onFrame != str_onFrame ) {
            // empty file?
            if ( opened_file.EndOfFile() )
                break;
            ERR_DIALOG("Parse error loading AnimCue %s: got %s but expected %s in file\n", opened_file.GetFilePath().c_str(), onFrame.c_str(), str_onFrame );
            break;
        }

        // Read frame number
        std::string frameNum;
        if ( !parser.ReadWordOnLine( frameNum ) ) {
            ERR_DIALOG("incomplete AnimCue in file %s, expected a frame number.\n", opened_file.GetFilePath().c_str() );
            break;
        }
        cue.frame = String::ToUInt( frameNum );

        if ( cue.frame < last_cue_frame ) {
            ERR_DIALOG("Cue frames are not in order in cue file: %s\n", opened_file.GetFilePath().c_str() );
        }

        last_cue_frame = cue.frame;

        // read "ofChannelID" or "ofChannelName"
        std::string ofChannelID;
        if ( !parser.ReadWordOnLine( ofChannelID ) ) {
            ERR_DIALOG("incomplete AnimCue in file %s, expected %s or %s\n", opened_file.GetFilePath().c_str(), str_ofChannelID, str_ofChannelName );
            break;
        }

        // parse ofChannelID/Name
        if ( ofChannelID == str_ofChannelID )  {
            // read the channel number
            if ( !parser.ReadWordOnLine( ofChannelID ) ) {
                ERR_DIALOG("incomplete AnimCue in file %s, expected a channel number\n", opened_file.GetFilePath().c_str() );
                break;
            }
            channel = static_cast< AnimChannelT >( String::ToUInt( ofChannelID ) );
        } else if ( ofChannelID == str_ofChannelName )  {
            // read the channe name
            if ( !parser.ReadWordOnLine( ofChannelID ) ) {
                ERR_DIALOG("incomplete AnimCue in file %s, expected a channel name\n", opened_file.GetFilePath().c_str() );
                break;
            }

            if ( ofChannelID == "legs" ) {
                channel = AnimChannelT::Legs;
            } else if ( ofChannelID == "torso" ) {
                channel = AnimChannelT::Torso;
            } else {
                ERR_DIALOG("invalid channel name (%s) in AnimCue file %s\n", ofChannelID.c_str(), opened_file.GetFilePath().c_str() );
                break;
            }
        } else {
            ERR_DIALOG("Parse error loading AnimCue %s: got %s but expected %s or %s\n", opened_file.GetFilePath().c_str(), ofChannelID.c_str(), str_ofChannelID, str_ofChannelName );
            break;
        }

        Armature* arm = model.GetAnim(animName, channel);
        if ( ! arm ) {
            ERR_DIALOG("Couldn't get armature %s on channel %u for AnimCue %s\n", animName.c_str(), static_cast<AnimChannelT_BaseType>( channel ), opened_file.GetFilePath().c_str() );
            break;
        }

        // read the type of cue
        std::string cueType;
        if ( !parser.ReadWordOnLine( cueType ) ) {
            ERR_DIALOG("incomplete AnimCue in file %s, expected an event\n", opened_file.GetFilePath().c_str() );
            break;
        }

         if ( ! cue.SetType( cueType, eventList.get() ) ) {
            ERR_DIALOG("unknown AnimCue event %s in file %s\n", cueType.c_str(), opened_file.GetFilePath().c_str() );
            continue;
        }

        // read in any arguments
        std::string args;
        while ( parser.ReadWordOnLine( args ) ) {
            cue.arguments.emplace_back( args );
        }

        model.PreloadAnimCueAssets( cue );
        arm->AddCue( cue );
    }

    CMSG_LOG("Loaded cue: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
    return true;
}

bool ModelManager::LoadAnim( AnimatedModel& toAnimModel, const AssetManifest& am ) {
    File file;
    return OpenAssetFile( "Bone", am, file ) && LoadAnim( toAnimModel, file );
}

bool ModelManager::LoadAnim( AnimatedModel& toAnimModel, File& opened_anim_file ) {
    if ( ! opened_anim_file.IsOpen() ) {
        ERR("Couldn't open animation file %s\n", opened_anim_file.GetFilePath().c_str() );
        return false;
    }

    const std::string anim_name = StripModelFromAnimName( opened_anim_file.GetFilePath() );

    bool asset_existed = false;

    // ** delete animation if there already exists one with the same name
    for ( auto& animlist : toAnimModel.anims ) { // for each channel
        if ( !animlist )
            continue;

        Link< Armature > *link = animlist->GetFirst();
        while ( link != nullptr ) { // for each armature
            if ( link->Data().GetAnimName() == anim_name ) {
                asset_existed = true;
                link->Del();
                break; // no two animations will be named the same, so we are done searching
            }

            link = link->GetNext();
        }
    }

    // ** load it

    ParserBM1Anim bm1anim( opened_anim_file );
    if ( !bm1anim.Load( toAnimModel, anim_name.c_str() ) ) {
        DIE("Error loading anim %s\n", opened_anim_file.GetFilePath().c_str() );
        return false;
    }
    
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), anim_name, opened_anim_file );

    CMSG_LOG("Loaded anim: %s from %s\n", opened_anim_file.GetFilePath().c_str(), opened_anim_file.GetFileContainerName_Full().c_str() );
    return true;
}
