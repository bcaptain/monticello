// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ANIMATEDMODEL_H_
#define SRC_CLIB_ANIMATEDMODEL_H_

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../models/model.h"

class Bone;
class Armature;
class ArmatureInfo;

constexpr uint MAX_ANIM_CHANNELS=3;

typedef uint AnimChannelT_BaseType;
enum class AnimChannelT : AnimChannelT_BaseType {
    Channel_0 = 0
    , Legs = Channel_0

    , Channel_1 = 1
    , Torso = Channel_1

    , Channel_2 = 2
    , Head = Channel_2
};

/*!

AnimatedModel \n\n

A model shared between lots of entities. Each entity has it's own ArmatureInfo inside of it's ModelInfo for it's own non-shared information about it's current animation, timing, etc

**/

class AnimatedModel : public Model {
    friend class ModelManager; //todoattorney?
    friend class ArmatureInfo; //todoattorney?
    friend class ParserBM1Anim; //todoattorney?
    friend class ParserBM1; //todoattorney?

public:
    AnimatedModel( void );
    AnimatedModel( const AnimatedModel& other ) = delete;
    virtual ~AnimatedModel( void );

public:
    AnimatedModel& operator=( const AnimatedModel& other ) = delete;

public:
    std::size_t GetArmatureBoneCount( void ) const;
    uint NumChannels( void ) const;
    uint GetNumBones( void ) const;
    AnimChannelT GetChannelID( const std::size_t index ) const;
    uint GetChannelIndex( const AnimChannelT channel ) const;
    
    void BuildChannelList( void );

private:
    Armature* GetAnim( const std::string& anim_name, const AnimChannelT channel );
    Armature* GetAnim( const char* anim_name, const AnimChannelT channel ); //!< less efficient overload to GetAnim( const char* ), it must construct a std::string

public:    
    const Armature* GetRandomAnim( const std::string& anim_name, const AnimChannelT channel ) const; //!< Get animation name exactly, or with an integer suffix at the end of the name. as soon as an animation is not found (incrementally), the seach stops.
    const Armature* GetAnim( const std::string& anim_name, const AnimChannelT channel ) const;
    
    const Armature* GetAnim_NoErr( const std::string& anim_name, const AnimChannelT channel ) const;
    
    bool HasChannel( const AnimChannelT channel ) const;

    bool PackVBO( void ) override;
    void DrawVBOs( const ModelDrawInfo& drawInfo ) const override;

private:
    void DrawModelVBO( const ModelDrawInfo& drawInfo ) const;
    bool DrawAnimated( const ModelDrawInfo& drawInfo ) const;

private:
    std::vector< std::unique_ptr< LinkList< Armature > > > anims; //!< vector represents channel. LinkList is the armature for that channel. This is a unique_ptr because LinkList needs to be able to use it's link type's ctor, and Armature copy ctor is deleted.
    AnimChannelT* channels; // list of channel identifiers
    uint* channel_indices; // list of channel indices (the index of this variable is the channel itself)

    //As of 10/30/13 mesh vertices support 2 bones for weighting (two vertex groups in blender)
    //Jesse - I kept the VBOs for boneID and Weight separate to allow for more weights in the future (i.e. up to 4)
    VBO<float>* vbo_mesh_boneID;  //todo : GL_UPGRADE, if we move to GL 3.0+ this should be of type uchar.
    VBO<float>* vbo_mesh_boneWeights;

    std::size_t boneCount; //!< Used to allocate the space for boneArray.
    uint numChannels;
    uint numBones;
};

#endif  // SRC_CLIB_ANIMATEDMODEL_H_
