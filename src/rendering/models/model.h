// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MESH_H_
#define SRC_CLIB_MESH_H_

#include "./clib/src/warnings.h"
#include "../../base/main.h"
#include "../drawInfo.h"
#include "../../math/geometry/3d/aabox.h"

extern const std::string CUSTOM_MODEL_;
extern const std::string CUSTOM_MODEL_COPY;
extern const std::string CUSTOM_MODEL_COMBINED;

typedef uint AnimChannelT_BaseType;

class ModelInfo;
class AnimInfo;
class Material;
struct AnimCue;
struct Mix_Chunk;

class VertexInfo {
public:
    VertexInfo( void );
    VertexInfo( const VertexInfo& other );
    VertexInfo( VertexInfo&& other_rref );
    explicit VertexInfo( const Uint _vertIndex, const Uint *_texIndex=nullptr, const Uint *_normIndex=nullptr );
    virtual ~VertexInfo( void );

public:
    VertexInfo& operator=( const VertexInfo& other );
    bool operator==( const VertexInfo& other ) const;
    bool operator!=( const VertexInfo& other ) const;

public:
    Uint vertIndex; //!< the index in MeshVert Mesh::verts
    Uint *texIndex; //!< the index in MeshVert Mesh::texs, or nullptr if it doesn't have a texindex.
    Uint *normIndex; //!< e index in MeshVert Mesh::verts, or nullptr if it doesn't have a texindex. If it doesm, this is expected to be the same index as vertIndex
};

class ModelFace {
public:
    ModelFace( void );
    ModelFace( const ModelFace& other );
    ModelFace( ModelFace&& other_rref );
    ModelFace( std::initializer_list< uint > co_indices, std::initializer_list< uint > uv_indices, std::initializer_list< uint > norm_indices );
    virtual ~ModelFace( void );

public:
    ModelFace& operator=( const ModelFace& other );

public:
    LinkList< VertexInfo > vert;
};

class MeshVert {
public:
    MeshVert( void );
    ~MeshVert( void );
    MeshVert( const Vec3f& coordinate );
    MeshVert( const Vec3f& coordinate, const Vec3f& vertNorm );
    MeshVert( const MeshVert& other );

public:
    MeshVert& operator=( const MeshVert& other );

public:
    Vec3f co;
    Vec3f normal;
    uchar numGroups; //!< set by PackVBO_Model, this is also the size of bone_weights and bone_ids arrays
    float* bone_weights;
    uchar* bone_ids;
};

template<typename TYPE>
class VBO;

class Mesh {
public:
    Mesh( void );
    ~Mesh( void );
    explicit Mesh( const uint number_of_verts );
    Mesh( const Mesh& other );
    Mesh( Mesh&& other_rref );

public:
    Mesh& operator=( Mesh&& other_rref );
    Mesh& operator=( const Mesh& other );

public:
    uint GetVersion( void ) const;
    void SetVersion( const uint to );

public: //todo: private stuff
    std::vector< MeshVert > verts;
    std::vector< Vec2f > texs;
    std::vector< ModelFace > faces; // references verts/texs and contains vertexNormals
    
private:
    uint version;
};

struct ModelDrawInfo : public DrawInfo {
    ModelDrawInfo( void );
    ModelDrawInfo( const ModelDrawInfo& other ) = default;
    ModelDrawInfo& operator=( const ModelDrawInfo& other );
    ~ModelDrawInfo( void ) override {}

    AnimInfo* animInfo;
    bool noAnimPause;
    bool animateThisFrame;
    bool forceStaticDraw;
    std::weak_ptr< const Entity > entity;
};

class Model {
    friend class ModelManager;  //todoattorney?
    friend class ParserBM1; // the parser loads data into the model from a bm1 file  //todoattorney?
    friend class Flat; // flat needs to directly modify vertices and such  //todoattorney?

public:
    Model( void );
    Model( const Model& other ); //doesn't allow copying animated models
    Model( Model&& other_rref ); //doesn't allow copying animated models
    virtual ~Model( void );

public:
    Model& operator=( const Model& other ) = delete; // Models should not be copied this way
    Model& operator=( Model&& other_rref ) = delete; // Models should not be copied this way

public:
    bool IsAnimated( void ) const;
    std::string GetName( void ) const;

    void PreloadAnimCueAssets( const AnimCue& cue );
    
private:
    void CalculateBounds( void ) const;
    
public:
    AABox3D GetBounds( void ) const;
    float GetHeight( void ) const;
    void UpdateBounds( void ) const;
    
    void Clear( void );
    
    std::vector< Vec3f > GetAllVerts( void ) const;
    
    bool Combine( const std::shared_ptr< const Model >& other, const Vec3f& offset=Vec3f(0,0,0) );
    bool Combine( const Model& other, const Vec3f& offset=Vec3f(0,0,0) );
    void TransformMeshVerts( const uint mesh_number, const std::function< void(Vec3f&) > transform_function );
    std::shared_ptr< Model > GetCopy( void ) const; //!< an explicitly named copy method ensures we are only copying intentionally
    
    void MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool );
    void MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const;

    void SetName( const char* str );
    Uint8 NumMeshes( void ) const;

    const Mesh* GetMesh( const std::size_t idx ) const;
    Mesh* GetMesh( const std::size_t idx );
    Uint8 GetMeshVersion( const std::size_t idx ) const;

    std::shared_ptr< const Model > GetDefaultClipModel( void ) const;

    void UpdateVBOs( void ) const;
    virtual void DrawVBOs( const ModelDrawInfo& drawInfo ) const;
private:
    void PrepareShaders_LightedModel( const ModelDrawInfo& drawInfo, const Material& material ) const;
    void PrepareShaders_UnlightedModel( const ModelDrawInfo& drawInfo, const Material& material ) const;
    
public:
    virtual bool PackVBO( void );

protected:
    std::string name;
    mutable std::mutex bounds_mutex;
    mutable AABox3D bounds;
    VBO<float>* vbo_vert;
    VBO<float>* vbo_uv;
    VBO<float>* vbo_norm;
    std::vector< Mesh > meshes;
    bool animated;
    mutable bool boundsComputed;
};

#endif  // SRC_CLIB_MESH
