// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./modelInfo.h"
#include "../animation/animInfo.h"
#include "../animation/animation.h"
#include "../materialManager.h"
#include "../models/modelManager.h"
#include "../renderer.h"
#include "../vbo.h"
#include "../../entities/entity.h"

const uint DFAULT_ANIMATIONS_TO_TRY_CAPACITY = 5;

ModelInfo::ModelInfo( Entity* _owner )
    : attachments()
    , animationsToTry()
    , animationsToInsert()
    , animationsToRemove()
    , modelOffset()
    , drawModel()
    , clipModel( nullptr )
    , material()
    , animInfo()
    , base( nullptr )
    , base_channel( 0 )
    , owner( _owner )
    , isVisible( true )
    , animationsToTry_lock(false)
{
    SetMaterialToDefault();
    animationsToTry.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToInsert.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToRemove.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
}

ModelInfo::ModelInfo( const std::shared_ptr< const Model >& _model )
    : attachments()
    , animationsToTry()
    , animationsToInsert()
    , animationsToRemove()
    , modelOffset()
    , drawModel()
    , clipModel( nullptr )
    , material()
    , animInfo()
    , base( nullptr )
    , base_channel( 0 )
    , owner( nullptr )
    , isVisible( true )
    , animationsToTry_lock(false)
{
    SetModel( _model );
    SetMaterialToDefault();
    animationsToTry.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToInsert.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToRemove.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
}

ModelInfo::ModelInfo( Entity* _owner, const std::shared_ptr< const Model >& _model )
    : attachments()
    , animationsToTry()
    , animationsToInsert()
    , animationsToRemove()
    , modelOffset()
    , drawModel()
    , clipModel( nullptr )
    , material()
    , animInfo()
    , base( nullptr )
    , base_channel( 0 )
    , owner( _owner )
    , isVisible( true )
    , animationsToTry_lock(false)
{
    SetModel( _model );
    SetMaterialToDefault();
    animationsToTry.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToInsert.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToRemove.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
}

ModelInfo::ModelInfo( void )
    : attachments()
    , animationsToTry()
    , animationsToInsert()
    , animationsToRemove()
    , modelOffset()
    , drawModel()
    , clipModel( nullptr )
    , material()
    , animInfo()
    , base( nullptr )
    , base_channel( 0 )
    , owner( nullptr )
    , isVisible( true )
    , animationsToTry_lock(false)
{
    animationsToTry.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToInsert.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
    animationsToRemove.reserve(DFAULT_ANIMATIONS_TO_TRY_CAPACITY);
}

ModelInfo::ModelInfo( const ModelInfo& other )
    : attachments( other.attachments )
    , animationsToTry( other.animationsToTry )
    , animationsToInsert( other.animationsToInsert )
    , animationsToRemove( other.animationsToRemove )
    , modelOffset( other.modelOffset )
    , drawModel( other.drawModel )
    , clipModel( other.clipModel )
    , material( other.material )
    , animInfo()
    , base( other.base )
    , base_channel( other.base_channel )
    , owner( nullptr ) // owner doesn't copy
    , isVisible( other.isVisible )
    , animationsToTry_lock( other.animationsToTry_lock )
{
    if ( other.animInfo ) {
        animInfo = std::make_unique< AnimInfo >( *other.animInfo );
        animInfo->SetOwner( *this );
    }
}

ModelInfo::ModelInfo( ModelInfo&& other_rref )
    : attachments( std::move( other_rref.attachments ) )
    , animationsToTry( std::move( other_rref.animationsToTry ) )
    , animationsToInsert( std::move( other_rref.animationsToInsert ) )
    , animationsToRemove( std::move( other_rref.animationsToRemove ) )
    , modelOffset( other_rref.modelOffset )
    , drawModel( other_rref.drawModel )
    , clipModel( other_rref.clipModel )
    , material( other_rref.material )
    , animInfo( std::move(other_rref.animInfo ) )
    , base( other_rref.base )
    , base_channel( other_rref.base_channel )
    , owner( nullptr ) // owner doesn't copy
    , isVisible( other_rref.isVisible )
    , animationsToTry_lock( other_rref.animationsToTry_lock )
{
    if ( animInfo ) {
        animInfo->SetOwner( *this );
    }
}

ModelInfo::ModelInfo( Entity* _owner, ModelInfo&& other_rref )
    : attachments( std::move( other_rref.attachments ) )
    , animationsToTry( std::move( other_rref.animationsToTry ) )
    , animationsToInsert( std::move( other_rref.animationsToInsert ) )
    , animationsToRemove( std::move( other_rref.animationsToRemove ) )
    , modelOffset( other_rref.modelOffset )
    , drawModel( other_rref.drawModel )
    , clipModel( other_rref.clipModel )
    , material( other_rref.material )
    , animInfo( std::move(other_rref.animInfo ) )
    , base( other_rref.base )
    , base_channel( other_rref.base_channel )
    , owner( _owner )
    , isVisible( other_rref.isVisible )
    , animationsToTry_lock( other_rref.animationsToTry_lock )
{
    if ( animInfo ) {
        animInfo->SetOwner( *this );
    }
}

ModelInfo::ModelInfo( Entity* _owner, const ModelInfo& other )
    : attachments( other.attachments )
    , animationsToTry( other.animationsToTry )
    , animationsToInsert( other.animationsToInsert )
    , animationsToRemove( other.animationsToRemove )
    , modelOffset( other.modelOffset )
    , drawModel( other.drawModel )
    , clipModel( other.clipModel )
    , material( other.material )
    , animInfo()
    , base( other.base )
    , base_channel( other.base_channel )
    , owner( _owner )
    , isVisible( other.isVisible )
    , animationsToTry_lock( other.animationsToTry_lock )
{
    if ( other.animInfo ) {
        animInfo = std::make_unique< AnimInfo >( *other.animInfo );
        animInfo->SetOwner( *this );
    }
}

ModelInfo& ModelInfo::operator=( const ModelInfo& other ) {
    attachments = other.attachments;
    animationsToTry = other.animationsToTry;
    animationsToTry_lock = other.animationsToTry_lock;
    animationsToInsert = other.animationsToInsert;
    animationsToRemove = other.animationsToRemove;
    modelOffset = other.modelOffset;
    drawModel = other.drawModel;
    clipModel = other.clipModel;
    material = other.material;
    
    if ( other.animInfo ) {
        animInfo = std::make_unique< AnimInfo >( *other.animInfo );
        animInfo->SetOwner( *this );
    } else {
        animInfo.reset();
    }
    
    base = other.base;
    base_channel = other.base_channel;
    // owner doesn't change
    isVisible = other.isVisible;
    return *this;
}

ModelInfo& ModelInfo::operator=( ModelInfo&& other_rref ) {
    attachments = std::move( other_rref.attachments );
    animationsToTry = std::move( other_rref.animationsToTry );
    animationsToTry_lock = other_rref.animationsToTry_lock;
    animationsToInsert = std::move( other_rref.animationsToInsert );
    animationsToRemove = std::move( other_rref.animationsToRemove );
    modelOffset = other_rref.modelOffset;
    drawModel = other_rref.drawModel;
    clipModel = other_rref.clipModel;
    material = other_rref.material;
    std::swap( animInfo, other_rref.animInfo );
    if ( animInfo ) {
        animInfo->SetOwner( *this );
    }
    base = other_rref.base;
    base_channel = other_rref.base_channel;
    // owner doesn't change
    isVisible = other_rref.isVisible;
    return *this;
}

ModelInfo::~ModelInfo( void ) {
}

void ModelInfo::SetClipModelToDefault( void ) {
    if ( ! drawModel ) {
        SetClipModel( nullptr );
    } else {
        auto _clipModel = drawModel->GetDefaultClipModel();
        SetClipModel( _clipModel );
    }
}

void ModelInfo::SetClipModel( const std::shared_ptr< const Model >& to ) {
    clipModel = to;
}

void ModelInfo::UnsetModel( void ) {
    drawModel.reset();
    clipModel.reset();
    animInfo.reset();
}

bool ModelInfo::SetModel( const std::string& model_name ) {
    if ( model_name.size() == 0 ) {
        UnsetModel();
        return true;
    }
    return SetModel( modelManager.GetRandom( model_name.c_str() ) );
}

bool ModelInfo::SetModel( const std::shared_ptr< const Model >& new_model ) {
    if ( !new_model ) {
        UnsetModel();
        return true;
    }
    
    if ( drawModel == new_model )
        return true;

    attachments.clear();
    drawModel = new_model;
    SetClipModelToDefault();
    ASSERT( drawModel );

    if ( drawModel->IsAnimated() ) {
        if ( !animInfo ) {
            animInfo = std::make_unique< AnimInfo >( *this );
        }
        animInfo->ModelChanged();
    } else {
        animInfo.reset();
    }
    
    if ( !String::LeftIs( drawModel->GetName(), CUSTOM_MODEL_ ) )
        if ( ! material )
            SetMaterialToDefault();

    return true;
}

std::string ModelInfo::GetOwnerName( void ) const {
    if ( !owner )
        return {};
        
    return owner->GetEntName();
}

std::shared_ptr< const Model > ModelInfo::GetModel( void ) const {
    return drawModel;
}

bool ModelInfo::IsAnimatedModel( void ) const {
    if ( !drawModel )
        return false;
    return drawModel->IsAnimated();
}

void ModelInfo::Draw( ModelDrawInfo drawInfo_byVal ) const {
    
    if ( !IsVisible() )
        return;
    
    drawInfo_byVal.material = material;
    drawInfo_byVal.animInfo = animInfo.get();

    if ( clipModel && globalVals.GetBool( gval_d_clipModels ) ) {
        const Color4f red(1,0,0,1);
        const auto mtl = materialManager.Get("notex");
        ASSERT( mtl );

        drawInfo_byVal.colorize = red;
        drawInfo_byVal.material = mtl;
        drawInfo_byVal.opacity = 0.5f;
        
        clipModel->DrawVBOs( drawInfo_byVal );
    }

    if ( !drawModel )
        return;

    drawModel->DrawVBOs( drawInfo_byVal );
    
    for ( uint i=0; i<attachments.size(); ++i )
        attachments[i].Draw( drawInfo_byVal );
}

#ifdef MONTICELLO_DEBUG
void ModelInfo::TestModel_Animate( const char* anim_name ) const {
    if ( animInfo )
        animInfo->TestModel_Animate( anim_name );
}

void ModelInfo::TestModel_Animate( void ) const {
    if ( animInfo )
        animInfo->TestModel_Animate();
}
#endif // MONTICELLO_DEBUG

const ArmatureInfo* ModelInfo::GetArmatureInfo( const AnimChannelT channel ) const {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( c >= MAX_ANIM_CHANNELS )
        DIE ("modelInfo.cpp::GetArmatureInfo: Invalid channel");
    
    if ( animInfo )
        return animInfo->GetArmatureInfo(channel);
    return nullptr;
}

uint ModelInfo::GetNumAnimFrames( const char* anim_name, const AnimChannelT channel ) const {
    const ArmatureInfo* armInfo = GetArmatureInfo( channel );
    ASSERT( armInfo );
    return armInfo->GetNumAnimFrames( anim_name, channel );
}

std::size_t ModelInfo::GetNumArmatures( void ) const {
    if (!animInfo)
        return 0;

    return animInfo->GetNumArmatures();
}

std::shared_ptr< const Model > ModelInfo::GetClipModel( void ) const {
    if ( clipModel )
        return clipModel;
    return drawModel;
}

void ModelInfo::SetClipModel( const std::string& cm_name ) {
    if ( clipModel && clipModel->GetName() == cm_name )
        return; // The clipModel is already set to what we requested

    /* this function is often called automatically, so it's normal for it to
        not find a collision model (such as if one doesn't exist for an
        animation). Thus, we do not throw errors if it simply can't find one.*/

    if ( String::Right( cm_name, 3 ) == "_cm" ) {
        // the fully-qualified modelname was passed

        if ( auto m = modelManager.Get_NoErr( cm_name.c_str() ) )
            clipModel = m;

        return;
    }

    // ** the name passed is part of a set, we must affix the drawModel name and "_cm"
    
    if ( !drawModel ) {
        ERR("Cannot set clipModel without either a drawModel or the fully qualified clipModel name. This modelInfo has no drawModel!\n");
        return;
    }

    std::string fullClipModelName( drawModel->GetName() );
    fullClipModelName += "_";
    fullClipModelName += cm_name;
    fullClipModelName += "_cm";

    if ( auto _clipModel = modelManager.Get_NoErr( fullClipModelName.c_str() ) ) {
        clipModel = _clipModel;
    }
}

void ModelInfo::SetClipModel( const char* cm_name ) {
    std::string n;
    if ( cm_name != nullptr  )
        n = cm_name;
    SetClipModel( n );
}

void ModelInfo::RemoveAttachment( const std::string& attachment_pos, const std::string& attachment_name ) {
    const std::string model_name = GetAttachmentModelName( attachment_pos, attachment_name );

    const uint i = GetAttachmentIndex( model_name );

    if ( i >= attachments.size() )
        return;

    PopSwap( attachments, i );
}

void ModelInfo::RemoveAttachmentAtIndex( const uint index ) {
    if ( index >= attachments.size() )
        return;
        
    PopSwap( attachments, index );
}

void ModelInfo::AddAttachment( const std::string& attachment_pos, const std::string& attachment_name, const AnimChannelT_BaseType channel ) {
    if ( base != nullptr ) {
        ERR("ModelInfo::AddAttachment: can't add attachment %s-%s to another attachment.\n", attachment_pos.c_str(), attachment_name.c_str() );
        return;
    }
    
    if ( ! drawModel ) {
        ERR("ModelInfo::AddAttachment: No draw model.\n");
        return;
    }
    
    const std::string model_name = GetAttachmentModelName( attachment_pos, attachment_name );

    if ( !HasDrawModel() ) {
        ERR("AddAttachment: cannot add attachment %s, there is no drawModel %s.\n", attachment_name.c_str(), model_name.c_str() );
        return;
    }

    // ** find any attachment currently in the same position
    for ( uint i=0; i<attachments.size(); ++i ) {
        auto mdl = attachments[i].GetDrawModel();
        if ( ! mdl )
            continue;

        const std::vector<std::string> oldAttachment(String::ParseStringList( mdl->GetName(), "-" ) );
        ASSERT(oldAttachment.size() == 3);

        // are they not in the same position?
        if ( oldAttachment[1] != attachment_pos )
            continue;

        if ( mdl->GetName() == model_name )
            return; // already has the attachment
            
        PopSwap( attachments, i );
        break; // it's not possible to have multiple attachments in the same position, so we're done
    }
    
    auto model = modelManager.Get( model_name.c_str() );
    
    AddAttachment( model, channel );
}

void ModelInfo::AddAttachment( const std::shared_ptr< const Model >& model, const AnimChannelT_BaseType channel ) {
    attachments.emplace_back( model );
    attachments[attachments.size()-1].base = this;
    attachments[attachments.size()-1].base_channel = channel;
    attachments[attachments.size()-1].owner = owner;
//    attachments[attachments.size()-1].SetMaterial( material );
}

bool ModelInfo::HasAttachment( const std::string& attachment_pos, const std::string& attachment_name ) const {
    const std::string model_name = GetAttachmentModelName( attachment_pos, attachment_name );
    return GetAttachmentIndex( model_name ) < attachments.size();
}

void ModelInfo::SetAttachmentVisibility( const bool visibility ) {
    if ( base )
        SetVisible( visibility );
    
    for ( auto & attachment : attachments )
        attachment.SetAttachmentVisibility( visibility );
}

std::string ModelInfo::GetAttachmentModelName( const std::string& attachment_pos, const std::string& attachment_name  ) const {
    std::string model_name = drawModel->GetName();
    model_name += "-";
    model_name += attachment_pos;
    model_name += "-";
    model_name += attachment_name;
    return model_name;
}
    
uint ModelInfo::GetAttachmentIndexAtPos( const std::string& attachment_pos ) const {
    for ( uint i=0; i<attachments.size(); ++i ) {
        if ( auto mdl = attachments[i].GetDrawModel() ) {
            const std::vector<std::string> attachment(String::ParseStringList( mdl->GetName(), "-" ) );
            if ( attachment.size() != 3 )
                continue; // malformed

            if ( attachment[1] == attachment_pos ) {
                return i;
            }
        }
    }

    return attachments.size();
}

uint ModelInfo::GetAttachmentIndex( const std::string& model_name ) const {
    uint i;
    for ( i=0; i<attachments.size(); ++i )
        if ( auto mdl = attachments[i].GetDrawModel() )
            if ( mdl->GetName() == model_name )
                break;

    return i;
}

void ModelInfo::MapSave( FileMap& saveFile, LinkList< std::string >& string_pool ) const {
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    
    saveFile.parser.WriteVec3f( modelOffset );

    // custom models are saved to the map.
    // custom models will also not have multiple models
    const bool has_noncustom_model = drawModel && !String::LeftIs( drawModel->GetName(), CUSTOM_MODEL_ );
    saveFile.parser.WriteBool( has_noncustom_model );

    if ( has_noncustom_model ) {
        saveFile.parser.WriteUInt( string_pool.AppendUnique( drawModel->GetName() ) );

        saveFile.parser.WriteBool( clipModel != nullptr );
        if ( clipModel ) {
            const auto clipModel_name = clipModel->GetName();
            const bool has_nondefault_clipmodel = clipModel_name.size() != 0 && clipModel_name != drawModel->GetName();
            saveFile.parser.WriteBool( has_nondefault_clipmodel );
            if ( has_nondefault_clipmodel )
                saveFile.parser.WriteUInt( string_pool.AppendUnique( clipModel->GetName() ) );
        }
    }

    MapSaveMaterial( saveFile, string_pool, material );

    saveFile.parser.WriteUInt( base_channel );
    saveFile.parser.WriteUInt(0);//unused
    saveFile.parser.WriteBool( isVisible );
    saveFile.WriteEntityRef( owner );
    
    saveFile.parser.WriteUInt(attachments.size());
    for ( const auto & attachment : attachments )
        attachment.MapSave( saveFile, string_pool );
}

void ModelInfo::MapLoad( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();

    saveFile.parser.ReadVec3f( modelOffset );
    
    const bool has_noncustom_model = saveFile.parser.ReadBool();
    if ( has_noncustom_model ) {
        std::string drawModel_name( string_pool[ saveFile.parser.ReadUInt() ] );
        SetModel( modelManager.Get( drawModel_name.c_str() ) );

        const bool has_clip_model = saveFile.parser.ReadBool();
        if ( has_clip_model ) {
            const bool has_nondefault_clipmodel = saveFile.parser.ReadBool();
            if ( has_nondefault_clipmodel ) {
                std::string clipModel_name( string_pool[ saveFile.parser.ReadUInt() ] );
                SetClipModel( modelManager.Get( clipModel_name.c_str() ) );
            }
        }

    }

    material = MapLoadMaterial( saveFile, string_pool );

    saveFile.parser.ReadUInt( base_channel );
    saveFile.parser.ReadUInt(); // unused
    saveFile.parser.ReadBool( isVisible );
    owner = saveFile.ReadEntityRef().get();

    const uint numAttachments = saveFile.parser.ReadUInt();
    attachments.clear();
    attachments.resize( numAttachments );
    for ( uint i=0; i<numAttachments; ++i ) {
        attachments[i].MapLoad( saveFile, string_pool );
        attachments[i].base = this;
    }
}

bool ModelInfo::SetMaterial( const char* mtl ) {
    return SetMaterial( materialManager.Get( mtl ) );
}

bool ModelInfo::SetMaterial( const std::shared_ptr< const Material >& mtl ) {
    if ( !mtl )
        return false;

    material = mtl;
    return true;
}

bool ModelInfo::SetMaterialToDefault( void ) {
    if ( !drawModel )
        return false;

    const std::shared_ptr< const Material > mtl_p = materialManager.Get( drawModel->GetName().c_str() );

    if ( !mtl_p ) {
        ERR("ModelInfo: Default material does not exist: %s.\n", drawModel->GetName().c_str() );
        return false;
    }
    
    return SetMaterial( mtl_p ); // load default material
}

bool ModelInfo::HasChannel( const AnimChannelT channel ) const {
    auto model = GetDrawModel();
    if ( ! model || ! model->IsAnimated() )
        return false;

    const auto animModel = static_cast< const AnimatedModel* >( model.get() );
    ASSERT( animModel );

    return animModel->HasChannel( channel );
}

void ModelInfo::AddAnimState( const AnimStateT animState ) {
    if ( animationsToTry_lock ) {
        animationsToInsert.emplace_back( owner->GetAnimStateForAttachment( animState ) );
    } else {
        animationsToTry.emplace_back( owner->GetAnimStateForAttachment( animState ) );
    }
}

void ModelInfo::ClearAnimState( void ) {
    if ( animationsToTry_lock ) {
        animationsToRemove = animationsToTry;
    } else {
        animationsToTry.clear();
    }
    
    animationsToInsert.clear();
}

void ModelInfo::RemAnimState( const AnimStateT animState ) {
    if ( animationsToTry_lock ) {
        animationsToRemove.emplace_back( owner->GetAnimStateForAttachment( animState ) );
    } else {
        PopSwap_Value(animationsToTry, owner->GetAnimStateForAttachment( animState ) );
    }
}

bool ModelInfo::HasAnimState( const AnimStateT animState ) const {
    if ( Contains( animationsToTry, owner->GetAnimStateForAttachment( animState ) ) )
        return true;
    return Contains( animationsToInsert, owner->GetAnimStateForAttachment( animState ) );
}

Vec3f ModelInfo::GetOffset( void ) const {
    return modelOffset;
}

void ModelInfo::SetOffset( const Vec3f& offset ) {
    modelOffset = offset;
}

void ModelInfo::SetOwner( Entity* _owner ) {
    owner = _owner;
}

Entity* ModelInfo::GetOwner( void ) const { 
    return owner;
}

AnimChannelT_BaseType ModelInfo::GetBaseChannel( void ) const {
    return base_channel;
}

const ModelInfo* ModelInfo::GetBaseModelInfo( void ) const {
    return base;
}

uint ModelInfo::NumAttachments( void ) const {
    return attachments.size();
}

const ModelInfo* ModelInfo::GetAttachment( const uint index ) const {
    return &attachments[index];
}

ModelInfo* ModelInfo::GetAttachment( const uint index ) {
    return &attachments[index];
}

bool ModelInfo::HasDrawModel( void ) const {
    return !!drawModel;
}

bool ModelInfo::HasClipModel( void ) const {
    return !!clipModel;
}

std::shared_ptr< const Model > ModelInfo::GetDrawModel( void ) const {
    return drawModel;
}

std::shared_ptr< const Material > ModelInfo::GetMaterial( void ) const {
    return material;
}

void ModelInfo::UnsetMaterial( void ) {
    material = nullptr;
}

ArmatureInfo* ModelInfo::GetArmatureInfo( const AnimChannelT channel ) {
    return const_cast< ArmatureInfo* >( const_cast< const ModelInfo* >( this )->GetArmatureInfo( channel ) );
}

void ModelInfo::SetVisible( const bool visibility ) {
    isVisible = visibility;
}

bool ModelInfo::IsVisible( void ) const {
    return isVisible;
}

AnimInfo* ModelInfo::GetAnimInfo( void ) {
    return animInfo.get();
}

const AnimInfo* ModelInfo::GetAnimInfo( void ) const {
    return animInfo.get();
}

bool ModelInfo::IsAnimated( void ) const {
    return animInfo != nullptr;
}
