// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./model.h"
#include "../models/modelInfo.h"
#include "../renderer.h"
#include "../vbo.h"
#include "../materialManager.h"
#include "../models/modelManager.h"
#include "../../sound.h"
#include "../../preload.h"

extern const uint ANIMCUE_PROJECTILE_NAME_INDEX;
extern const uint ANIMCUE_ENTITY_NAME_INDEX;
extern const uint ANIMCUE_EFFECT_NAME_INDEX;
extern const uint ANIMCUE_SOUND_NAME_INDEX;
extern const uint ANIMCUE_FIRE_BULLET_DAMAGE_NAME_INDEX;

const uint DEFAULT_MODEL_VERSION = 2;

const std::string CUSTOM_MODEL_ = "CUSTOM_MODEL_"; // string prefix. custom models aren't in managed by the model manager.
const std::string CUSTOM_MODEL_COPY = "CUSTOM_MODEL_COPY";
const std::string CUSTOM_MODEL_COMBINED = "CUSTOM_MODEL_COMBINED";

ModelDrawInfo::ModelDrawInfo( void )
    : DrawInfo()
    , animInfo(nullptr)
    , noAnimPause( false )
    , animateThisFrame( true )
    , forceStaticDraw( false )
    , entity()
{ }

ModelDrawInfo& ModelDrawInfo::operator=( const ModelDrawInfo& other ) {
    DrawInfo::operator=( other );
    animInfo = other.animInfo;
    noAnimPause = other.noAnimPause;
    animateThisFrame = other.animateThisFrame;
    forceStaticDraw = other.forceStaticDraw;
    entity = other.entity;
    return *this;
}

ModelFace::ModelFace( void )
    : vert()
{ }

void CheckListSizes( const std::initializer_list< uint >& co_indices, const std::initializer_list< Uint >& uv_indices, const std::initializer_list< uint >& norm_indices );
void CheckListSizes( const std::initializer_list< uint >& co_indices, const std::initializer_list< Uint >& uv_indices, const std::initializer_list< uint >& norm_indices ) {
    if ( norm_indices.size() == 0 )
        if ( uv_indices.size() == 0 )
            return;

    if ( norm_indices.size() == 0 )
        if ( uv_indices.size() == co_indices.size() )
            return;

    if ( norm_indices.size() == uv_indices.size()  )
        if ( co_indices.size() == uv_indices.size()  )
            return;

    WARN("constructing a ModelFace with mismatched list size of vertex, uv, and normals.\n");
}

ModelFace::ModelFace( std::initializer_list< uint > co_indices, std::initializer_list< Uint > uv_indices, std::initializer_list< uint > norm_indices )
    : vert()
{
    CheckListSizes( co_indices, uv_indices, norm_indices );

    auto uv_itr = begin( uv_indices );
    auto norm_itr = begin( norm_indices );

    for ( auto & co : co_indices ) {
        const Uint *uv = nullptr;
        const Uint *norm = nullptr;

        if ( uv_itr != std::cend( uv_indices ) ) {
            uv = &*uv_itr;
            if ( norm_itr != std::cend( norm_indices ) ) {
                norm = &*norm_itr;
                ++norm_itr;
            }
            ++uv_itr;
        }

        vert.Append( VertexInfo(co, uv, norm ) );
    }
}

ModelFace::ModelFace( const ModelFace& other )
    : vert( other.vert )
{ }

ModelFace::ModelFace( ModelFace&& other_rref )
    : vert( std::move( other_rref.vert ) )
{ }

ModelFace::~ModelFace( void )
{ }

ModelFace& ModelFace::operator=( const ModelFace& other ) {
    if (this == &other)
        return *this;

    vert = other.vert;

    return *this;
}

MeshVert::MeshVert( void )
    : co()
    , normal()
    , numGroups(0)
    , bone_weights(nullptr)
    , bone_ids(nullptr)
    {
}

MeshVert::MeshVert( const Vec3f& coordinate )
    : co( coordinate )
    , normal()
    , numGroups(0)
    , bone_weights(nullptr)
    , bone_ids(nullptr)
    {
}

MeshVert::MeshVert( const Vec3f& coordinate, const Vec3f& vertNorm )
    : co( coordinate )
    , normal( vertNorm )
    , numGroups(0)
    , bone_weights(nullptr)
    , bone_ids(nullptr)
    {
}

MeshVert::MeshVert( const MeshVert& other )
    : co( other.co )
    , normal( other.normal )
    , numGroups( other.numGroups )
    , bone_weights( other.bone_weights ? new float[other.numGroups] : nullptr )
    , bone_ids( other.bone_ids ? new uchar[other.numGroups] : nullptr )
    {
}

MeshVert& MeshVert::operator=( const MeshVert& other ) {
    if ( &other == this )
        return *this;

    delete[] bone_weights;
    delete[] bone_ids;

    co = other.co;
    normal = other.normal;
    numGroups = other.numGroups;
    if ( numGroups > 0 ) {
        bone_weights = new float[numGroups];
        bone_ids = new uchar[numGroups];
    } else {
        bone_weights = nullptr;
        bone_ids = nullptr;
    }

    return *this;
}

MeshVert::~MeshVert( void ) {
    delete[] bone_weights;
    delete[] bone_ids;
}

Mesh::Mesh( const uint number_of_verts )
    : verts( number_of_verts )
    , texs( number_of_verts )
    , faces()
    , version(DEFAULT_MODEL_VERSION)
{ }

Mesh::Mesh( void )
    : verts()
    , texs()
    , faces()
    , version(DEFAULT_MODEL_VERSION)

{ }

Mesh::Mesh( const Mesh& other )
    : verts( other.verts )
    , texs( other.texs )
    , faces( other.faces )
    , version( other.version )

{ }

Mesh::Mesh( Mesh&& other_rref )
    : verts( std::move( other_rref.verts ) )
    , texs( std::move( other_rref.texs ) )
    , faces( std::move( other_rref.faces ) )
    , version( other_rref.version )
{ }

Mesh& Mesh::operator=( Mesh&& other_rref ) {
    if ( &other_rref == this )
        return *this;

    verts = std::move( other_rref.verts );
    texs = std::move( other_rref.texs );
    faces = std::move( other_rref.faces );
    version = other_rref.version;

    return *this;
}

Mesh& Mesh::operator=( const Mesh& other ) {
    if ( &other == this )
        return *this;

    verts = other.verts;
    texs = other.texs;
    faces = other.faces;
    version = other.version;

    return *this;
}

Mesh::~Mesh( void ) { }

void AddTri( const Vec3f& vert_coord, const float u, const float v );

Model::Model( void )
    : name()
    , bounds_mutex()
    , bounds()
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::RARELY ) )
    , vbo_uv( new VBO<float>( 2, VBOChangeFrequencyT::RARELY ) )
    , vbo_norm( new VBO<float>( 3, VBOChangeFrequencyT::RARELY ) )
    , meshes()
    , animated( false )
    , boundsComputed( false )
{ }

Model::Model( const Model& other )
    : name( other.name )
    , bounds_mutex()
    , bounds()
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::RARELY ) )
    , vbo_uv( new VBO<float>( 2, VBOChangeFrequencyT::RARELY ) )
    , vbo_norm( new VBO<float>( 3, VBOChangeFrequencyT::RARELY ) )
    , meshes( other.meshes )
    , animated( other.animated )
    , boundsComputed( false )
{
    std::lock_guard< std::mutex > other_bounds_mutex_lock( other.bounds_mutex );
    std::lock_guard< std::mutex > bounds_mutex_lock( bounds_mutex );

    ASSERT( !animated ); // we just haven't gone to the trouble of allowing this

    boundsComputed = other.boundsComputed;
    if ( boundsComputed )
        bounds = other.bounds;
}

Model::Model( Model&& other_rref )
    : name( other_rref.name )
    , bounds_mutex()
    , bounds( other_rref.bounds )
    , vbo_vert( other_rref.vbo_vert )
    , vbo_uv( other_rref.vbo_uv )
    , vbo_norm( other_rref.vbo_norm )
    , meshes( std::move( other_rref.meshes ) )
    , animated( other_rref.animated )
    , boundsComputed( other_rref.boundsComputed )
{
    ASSERT( !animated );
    other_rref.vbo_uv = nullptr;
    other_rref.vbo_vert = nullptr;
}

Model::~Model( void ) {
    delete vbo_vert;
    delete vbo_uv;
    delete vbo_norm;
}

void Model::PrepareShaders_UnlightedModel( const ModelDrawInfo& drawInfo, const Material& material ) const {
    if ( drawInfo.opacity < 1.0f || drawInfo.colorize.a > 0.0f ) {
        shaders->UseProg(  GLPROG_COLORIZE_TEXTURE  );
        shaders->SetUniform4f("vColor", drawInfo.colorize.r, drawInfo.colorize.g, drawInfo.colorize.b,drawInfo.opacity );
        shaders->SetUniform1f("fWeight", drawInfo.colorize.a );
        shaders->SetTexture( material.GetDiffuse()->GetID() );
        shaders->SetAttrib( "vUV", *vbo_uv );
        return;
    }
    
    bool doProjector = globalVals.GetBool( gval_d_projections ) && renderer->projectors.size() > 0;
        
    if ( !doProjector ) {
        shaders->UseProg(  GLPROG_TEXTURE  );
    } else {
        // Right now only 1 projector is set up in the shaders, need to discuss
        auto pj = renderer->projectors[0].projector.lock();
        static bool projector_error_displayed = false;
        
        if ( !pj ) {
            if ( !projector_error_displayed ) {
                ERR("PrepareShaders_UnlightedModel: Null Projector reference. This warning will not appear again.\n");
                projector_error_displayed = true;
            }
            return;
        }
        
        auto entity_sptr = drawInfo.entity.lock();
        
        if ( !entity_sptr || pj->CanTarget( *entity_sptr ) ) {
            shaders->UseProg(   GLPROG_PROJTEX  );
            pj->SendShaderData();
        } else {
            shaders->UseProg(  GLPROG_TEXTURE  );
        }
    }
    
    shaders->SetTexture( material.GetDiffuse()->GetID() );
    shaders->SetAttrib( "vUV", *vbo_uv );
}

void Model::PrepareShaders_LightedModel( const ModelDrawInfo& drawInfo, const Material& material ) const {
    shaders->UseProg(  GLPROG_ANIM_TEX_MULTI_LIGHT );
    renderer->SendShaderData_Lights();
    shaders->SetAttrib( "vNorm", *vbo_norm );
    shaders->SetTexture( material.GetDiffuse()->GetID() );
    shaders->SetAttrib( "vUV", *vbo_uv );
    shaders->SetUniform4f("vColor", drawInfo.colorize.r, drawInfo.colorize.g, drawInfo.colorize.b, drawInfo.opacity );
    shaders->SetUniform1f("fWeight", drawInfo.colorize.a );
}

void Model::DrawVBOs( const ModelDrawInfo& drawInfo ) const {
    if ( !globalVals.GetBool( gval_d_models ) )
        return;

    if ( !vbo_vert->Finalized() )
        return;
        
    const auto material = drawInfo.material ? drawInfo.material : materialManager.Get( "notex" );
    const bool textured = material && material->GetDiffuse() && vbo_uv->Finalized();
    const bool lighted = (
        textured
        && GetMeshVersion(0) > 1
        && globalVals.GetBool( gval_v_light )
        && vbo_norm->Finalized()
    );

    if ( lighted ) {
        ASSERT( material );
        PrepareShaders_LightedModel( drawInfo, *material );
        
    } else if ( textured ) {
        ASSERT( material );
        PrepareShaders_UnlightedModel( drawInfo, *material );
        
    } else {
        shaders->UseProg(  GLPROG_MINIMAL  );
    }

    shaders->SendData_Matrices();
    if ( lighted && globalVals.GetBool( gval_v_light ) ) {
        shaders->SetUniformMatrix3fv( "N" ,1, GL_FALSE, renderer->ModelView().GetRotationMatrix().data ); //Assumes matrix has uniform scaling
    }
    
    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->DrawArrays( GL_TRIANGLES, 0, vbo_vert->Num() );
}

void Model::UpdateVBOs( void ) const {
    vbo_vert->Clear();
    vbo_uv->Clear();
    vbo_norm->Clear();
}

const Mesh* Model::GetMesh( const std::size_t idx ) const {
    if ( idx >= meshes.size() )
        return nullptr;
    return &meshes[idx];
}

Mesh* Model::GetMesh( const std::size_t idx ) {
    if ( idx >= meshes.size() )
        return nullptr;
    return &meshes[idx];
}

bool Model::PackVBO( void ) {
    if ( vbo_vert->Finalized() )
        return true;
    
    if ( meshes.size() < 1 )
        return false;

    return ::PackVBO_Model( *vbo_vert, *vbo_uv, *vbo_norm, nullptr, nullptr, *this );
}

std::shared_ptr< const Model > Model::GetDefaultClipModel( void ) const {
    if ( String::RightIs(name,"_cm") )
        return nullptr; // this IS a collision model
    std::string cm_name( name );
    cm_name += "_cm";
    return modelManager.Get_NoErr( cm_name.c_str() );
}

void Model::PreloadAnimCueAssets( const AnimCue& cue ) {
    
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wswitch-enum" // this is okay
    
    switch ( cue.event ) {
        case AnimEventT::LAUNCH_PROJECTILE:
            if ( cue.arguments.size() > ANIMCUE_PROJECTILE_NAME_INDEX )
                Preload::PreloadEntity( cue.arguments[ANIMCUE_PROJECTILE_NAME_INDEX] );
            break;
        case AnimEventT::PLAY_SOUND:
            if ( cue.arguments.size() > ANIMCUE_SOUND_NAME_INDEX ) {
                Preload::PreloadSound( cue.arguments[ANIMCUE_SOUND_NAME_INDEX].c_str() );
            }
            break;
        case AnimEventT::SPAWN_EFFECT:
            if ( cue.arguments.size() > ANIMCUE_EFFECT_NAME_INDEX )
                Preload::PreloadEffect( cue.arguments[ANIMCUE_EFFECT_NAME_INDEX] );
            break;
        case AnimEventT::FIRE_BULLET:
            if ( cue.arguments.size() > ANIMCUE_FIRE_BULLET_DAMAGE_NAME_INDEX )
                Preload::PreloadDamage( cue.arguments[ANIMCUE_FIRE_BULLET_DAMAGE_NAME_INDEX] );
            break;
        default: break;
    }

    #pragma GCC diagnostic pop
}

void Model::UpdateBounds( void ) const {
    std::lock_guard< std::mutex > bounds_mutex_lock( bounds_mutex );

    boundsComputed = false;
}

void Model::CalculateBounds( void ) const {
    if ( boundsComputed )
        return;

    bool first_vert_found_and_set = false;

    // ** find the smallest/largest x/y/z coordinates and use these to create the bbox
    for ( std::size_t i=0; i<meshes.size(); ++i ) {
        for ( auto const& face : meshes[i].faces ) {
            for ( auto const& vertInfo : face.vert ) {
                const Vec3f *v = &meshes[i].verts[ vertInfo.vertIndex ].co;

                if ( !first_vert_found_and_set ) {
                    bounds.SetMins( *v );
                    bounds.SetMaxs( *v );
                    first_vert_found_and_set = true;
                } else {
                    bounds.SetMinsIfHigher( *v );
                    bounds.SetMaxsIfLower( *v );
                }
            }
        }
    }

    boundsComputed = true;
}

AABox3D Model::GetBounds( void ) const {
    std::lock_guard< std::mutex > bounds_mutex_lock( bounds_mutex );
    CalculateBounds();
    return bounds;
}

float Model::GetHeight( void ) const {
    std::lock_guard< std::mutex > bounds_mutex_lock( bounds_mutex );
    CalculateBounds();
    return bounds.GetHeight();
}

std::vector< Vec3f > Model::GetAllVerts( void ) const {
    std::vector< Vec3f > allVerts;
    
    for ( uint m=0; m<meshes.size(); ++m )
        for ( const auto & vert : meshes[m].verts )
            allVerts.emplace_back( vert.co );
    
    return allVerts;
}

std::shared_ptr< Model > Model::GetCopy( void ) const {
    std::shared_ptr< Model > model( std::make_shared< Model >() );
    model->Combine( *this );
    model->SetName( CUSTOM_MODEL_COPY.c_str() );
    model->PackVBO();
    return model;
}

bool Model::Combine( const std::shared_ptr< const Model >& other_model, const Vec3f& offset ) {
    if ( !other_model )
        return false;
    return Combine( *other_model, offset );
}

bool Model::Combine( const Model& other_model, const Vec3f& offset ) {
    if ( &other_model == this )
        return false;
    
    if ( animated || other_model.animated ) {
        ERR("Animated models cannot be combined, only static models.\n");
        return false;
    }
    
    std::lock_guard< std::mutex > bounds_mutex_lock( bounds_mutex );
    std::lock_guard< std::mutex > other_bounds_mutex_lock( other_model.bounds_mutex );
    boundsComputed = false;
    
    if ( other_model.meshes.size() < 1 )
        return true;
    
    meshes.reserve( meshes.size() + other_model.meshes.size() );
    
    for ( uint m = 0; m < other_model.meshes.size(); ++m ) {
        const auto & other = other_model.meshes[m];
        
        meshes.emplace_back( other );
        auto & mesh = meshes[meshes.size()-1];
        
        if ( Maths::Approxf( offset, Vec3f(0,0,0) ) )
            continue;
            
        for ( uint v=0; v<mesh.verts.size(); ++v )
            mesh.verts[v].co += offset;
    }
    
    return true;
}

void Model::TransformMeshVerts( const uint mesh_number, const std::function< void(Vec3f&) > transform_function ) {
    if ( mesh_number >= meshes.size() ) {
        return;
    }
    
    std::lock_guard< std::mutex > bounds_mutex_lock( bounds_mutex );
    boundsComputed = false;
    
    auto & mesh = meshes[mesh_number];
    
    for ( uint v=0; v<mesh.verts.size(); ++v )
        transform_function( mesh.verts[v].co );
}

extern const Vec3f FLAT_VERTEX_NORMAL(0,0,1);
extern std::string CUSTOM_MODEL_FLAT;
void Model::MapLoad( FileMap& saveFile, [[maybe_unused]] const std::vector< std::string >& string_pool ) {
    [[maybe_unused]] const uint map_version = saveFile.parser.ReadUInt();
    Uint8 numMeshes = 0;
    
    name = string_pool[ saveFile.parser.ReadUInt() ];
    saveFile.parser.ReadUInt8( numMeshes );
    
    meshes.resize(numMeshes);
    for ( uint m=0; m<meshes.size(); ++m ) {
        // ** load verts, texs, uvs
        
        Mesh& mesh = meshes[m];

        const std::size_t numVerts=saveFile.parser.ReadUInt();
        mesh.verts.reserve( numVerts );
        mesh.texs.reserve( numVerts );

        for ( std::size_t v=0; v<numVerts; ++v ) {
            mesh.verts.emplace_back( saveFile.parser.ReadVec3f() );
        
            saveFile.parser.ReadVec3f( mesh.verts[v].normal );

            const float x = saveFile.parser.ReadFloat();
            const float y = saveFile.parser.ReadFloat();
            mesh.texs.emplace_back( x, y );
        }

        // ** load faces
        
        const uint numFaces = saveFile.parser.ReadUInt();
        mesh.faces.resize(numFaces);
            
        for ( std::size_t f=0; f<numFaces; ++f ) {
            auto & face = mesh.faces[f];
            const uint numFaceVerts = saveFile.parser.ReadUInt();
            face.vert.DelAll();
            
            for ( uint i=0; i<numFaceVerts; ++i ) {
                uint vert = saveFile.parser.ReadUInt();
                
                uint tex = 0;
                const bool hasTexIndex = saveFile.parser.ReadBool();
                if ( hasTexIndex ) {
                    tex = saveFile.parser.ReadUInt();
                }
                
                uint norm = 0;
                const bool hasNormIndex = saveFile.parser.ReadBool();
                if ( hasNormIndex ) {
                    norm = saveFile.parser.ReadUInt();
                }
                
                face.vert.Append( VertexInfo( vert, hasTexIndex ? &tex : nullptr, hasNormIndex ? &norm : nullptr ) );
            }
        }
    }
}

void Model::MapSave( FileMap& saveFile, [[maybe_unused]] LinkList< std::string >& string_pool ) const {
    const uint map_version = 0;
    saveFile.parser.WriteUInt( map_version );
    saveFile.parser.WriteUInt( string_pool.AppendUnique( name ) );
    saveFile.parser.WriteUInt8( static_cast<Uint8>( meshes.size() ) ); //todo: use int in the next model version
    
    for ( uint m=0; m<meshes.size(); ++m ) {
        const Mesh& mesh = meshes[m];
        
        saveFile.parser.WriteUInt( mesh.verts.size() );
        for ( std::size_t v=0; v<mesh.verts.size(); ++v ) {
            saveFile.parser.WriteVec3f( mesh.verts[v].co );
            saveFile.parser.WriteVec3f( mesh.verts[v].normal );
            saveFile.parser.WriteFloat( mesh.texs[v].x );
            saveFile.parser.WriteFloat( mesh.texs[v].y );
        }
        
        saveFile.parser.WriteUInt( mesh.faces.size() );
        for ( std::size_t f=0; f<mesh.faces.size(); ++f ) {
            auto & face = mesh.faces[f];
            
            const uint numFaceVerts = face.vert.size();
            saveFile.parser.WriteUInt( numFaceVerts );
            
            for ( const Link< VertexInfo >* faceVert = face.vert.GetFirst(); faceVert != nullptr; faceVert = faceVert->GetNext() ) {
                saveFile.parser.WriteUInt( faceVert->Data().vertIndex );
                
                const bool hasTexIndex = !!faceVert->Data().texIndex;
                saveFile.parser.WriteBool( hasTexIndex );
                if ( hasTexIndex ) {
                    saveFile.parser.WriteUInt( *(faceVert->Data().texIndex) );
                }
                
                const bool hasNormIndex = !!faceVert->Data().texIndex;
                saveFile.parser.WriteBool( hasNormIndex );
                if ( hasNormIndex ) {
                    saveFile.parser.WriteUInt( *(faceVert->Data().normIndex) );
                }
            }
        }
    }
}

VertexInfo::VertexInfo( const Uint _vertIndex, const Uint *_texIndex, const Uint *_normIndex )
    : vertIndex(_vertIndex)
    , texIndex( _texIndex ? new Uint(*_texIndex) : nullptr )
    , normIndex( _normIndex ? new Uint(*_normIndex) : nullptr )
    {
}

VertexInfo::VertexInfo( void )
    : vertIndex()
    , texIndex(nullptr)
    , normIndex(nullptr)
    {

}

VertexInfo::~VertexInfo( void ) {
    DELNULL( texIndex );
    DELNULL( normIndex );
}

VertexInfo::VertexInfo( const VertexInfo& other )
    : vertIndex(other.vertIndex)
    ,texIndex(nullptr)
    , normIndex(nullptr)
    {

    if ( other.texIndex )
        texIndex = new Uint(*other.texIndex);

    if ( other.normIndex )
        normIndex = new Uint(*other.normIndex);
}

VertexInfo::VertexInfo( VertexInfo&& other_rref )
    : vertIndex(other_rref.vertIndex)
    , texIndex(other_rref.texIndex)
    , normIndex(other_rref.normIndex)
    {
    other_rref.texIndex = nullptr;
    other_rref.normIndex = nullptr;
}

VertexInfo& VertexInfo::operator=( const VertexInfo& other ) {
    if ( this == &other )
        return *this;

    vertIndex=other.vertIndex;

    DELNULL(texIndex);
    if ( other.texIndex )
        texIndex=new Uint(*other.texIndex);

    DELNULL(normIndex);
    if ( other.normIndex )
        normIndex=new Uint(*other.normIndex);

    return *this;
}

bool VertexInfo::operator==( const VertexInfo& other ) const {
    if ( normIndex ) {
        if ( ! other.normIndex ) {
            return false;
        } else if ( !Maths::Approxf( *normIndex, *other.normIndex ) )
            return false;
    } else if ( other.normIndex ) {
        return false;
    }

    if ( texIndex ) {
        if ( ! other.texIndex ) {
            return false;
        } else if ( !Maths::Approxf( *texIndex, *other.texIndex ) )
            return false;
    } else if ( other.texIndex ) {
        return false;
    }

    if ( !Maths::Approxf( vertIndex, other.vertIndex ) )
        return false;

    return true;
}

bool VertexInfo::operator!=( const VertexInfo& other ) const {
    return !operator==(other);
}

bool Model::IsAnimated( void ) const {
    return animated;
}

std::string Model::GetName( void ) const {
    return name;
}

void Model::SetName(const char* str) {
    name = str;
}

Uint8 Model::NumMeshes( void ) const {
    return meshes.size();
}

Uint8 Model::GetMeshVersion( const std::size_t idx ) const {
    ASSERT( &meshes[idx] );
    return meshes[idx].GetVersion();
}

uint Mesh::GetVersion( void ) const {
    return version;
}

void Mesh::SetVersion( const uint to ) {
    version = to;
}
