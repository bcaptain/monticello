// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./animatedModel.h"
#include "../animation/animation.h"
#include "../materialManager.h"
#include "./modelInfo.h"
#include "../animation/animInfo.h"
#include "../renderer.h"
#include "../../game.h"
#include "../../entities/light/allLights.h"

AnimatedModel::AnimatedModel( void )
    : anims()
    , channels( nullptr )
    , channel_indices( nullptr )
    , vbo_mesh_boneID( new VBO<float>( 2,VBOChangeFrequencyT::RARELY ) )
    , vbo_mesh_boneWeights( new VBO<float>( 2,VBOChangeFrequencyT::RARELY ) )
    , boneCount(0)
    , numChannels(0)
    , numBones(0)
{
    animated = true;
}

AnimatedModel::~AnimatedModel( void ) {
    delete[] channels;
    delete[] channel_indices;
    delete vbo_mesh_boneID;
    delete vbo_mesh_boneWeights;
}

void AnimatedModel::BuildChannelList( void) {
    delete [] channel_indices;
    channel_indices = new uint[MAX_ANIM_CHANNELS]{0};
    for (uint c=0; c < MAX_ANIM_CHANNELS; c++) {
        bool found = false;
        for (uint i=0; i < numChannels; i++) {
            if ( channels[i] == static_cast< AnimChannelT >(c) ) {
                channel_indices[c] = i;
                found = true;
                break;
            }
        }
        if ( !found ) {
            WARN("AnimatedModel::BuildChannelList: %s Has no channel %i\n", GetName().c_str(), c);
        }
    }
}

const Armature* AnimatedModel::GetRandomAnim( const std::string& anim_name, const AnimChannelT channel ) const {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if (c >= anims.size() )
        return nullptr;

    if ( anims[c] == nullptr )
        return nullptr;

    std::vector< const Armature* > found;
    const Armature* arm = GetAnim_NoErr(anim_name, channel );
    uint idx=1;

    while ( arm ) {
        found.push_back( arm );
        const std::string checkName( anim_name + std::to_string(idx) );
        arm = GetAnim_NoErr( checkName, channel );
        ++idx;
    }

    switch ( found.size() ) {
        case 0:
            ERR("Anim %s not found.\n", anim_name.c_str() );
            return nullptr;
        case 1:
            return found[0];
        default:
            return Random::Element(found);
    }
}

bool AnimatedModel::HasChannel( const AnimChannelT channel ) const {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( c >= anims.size() )
        return false;
    
    const std::unique_ptr< LinkList< Armature > > & animlist = anims[c];
    return animlist != nullptr;
}

const Armature* AnimatedModel::GetAnim_NoErr( const std::string& anim_name, const AnimChannelT channel ) const {
    const auto c = static_cast< AnimChannelT_BaseType >( channel );
    
    if ( c >= anims.size() )
        return nullptr;
    
    const std::unique_ptr< LinkList< Armature > > & animlist = anims[c];
    if ( !animlist )
        return nullptr;
    
    for ( auto const & anim : *animlist )
        if ( anim.GetAnimName() == anim_name )
            return &anim;

    return nullptr;
}

const Armature* AnimatedModel::GetAnim( const std::string& anim_name, const AnimChannelT channel ) const {
    const Armature* ret = GetAnim_NoErr( anim_name, channel );
    if ( ! ret )
        ERR("Anim %s not found.\n", anim_name.c_str() ); //todoanimb: this happens when we switch attachments because we have animations pending
    return ret;
}

Armature* AnimatedModel::GetAnim( const std::string& anim_name, const AnimChannelT channel ) {
    return const_cast< Armature* >( const_cast< const AnimatedModel* >( this )->GetAnim( anim_name, channel ) );
}

bool AnimatedModel::PackVBO( void ) {
    return ::PackVBO_Model( *vbo_vert, *vbo_uv, *vbo_norm, vbo_mesh_boneID, vbo_mesh_boneWeights, *this );
}

void AnimatedModel::DrawVBOs( const ModelDrawInfo& drawInfo ) const {
    if ( globalVals.GetBool( gval_d_models ) )
        DrawModelVBO( drawInfo );
        
    if ( globalVals.GetBool( gval_d_skel ) )
        drawInfo.animInfo->DrawSkel();
}

void AnimatedModel::DrawModelVBO( const ModelDrawInfo& drawInfo ) const {
    if ( !vbo_vert->Finalized() )
        return;

    if ( !vbo_uv->Finalized() )
        return;

    ASSERT( drawInfo.animInfo );
    const std::size_t numArms( drawInfo.animInfo->GetNumArmatures() );
    
    if ( drawInfo.forceStaticDraw || numArms < 1 ) {
        Model::DrawVBOs( drawInfo );
        return;
    }

    if ( !vbo_mesh_boneID->Finalized() )
        return;
        
    if ( !vbo_mesh_boneWeights->Finalized() )
        return;
    
    if ( DrawAnimated( drawInfo ) ) {
        return;
    }
    
    Model::DrawVBOs( drawInfo );
}

bool AnimatedModel::DrawAnimated( const ModelDrawInfo& drawInfo ) const {

    const auto material = drawInfo.material ? drawInfo.material : materialManager.Get( "notex" );
    const bool textured = material && material->GetDiffuse() && vbo_uv->Finalized();
    const bool lighted = (
        textured
        && GetMeshVersion(0) > 1
        && globalVals.GetBool( gval_v_light )
        && vbo_norm->Finalized()
    );
    
    if ( !drawInfo.material || !drawInfo.material->GetDiffuse() ) {
        ERR("Cannot draw animated model, no material.\n");
        return false;
    }

    if ( !vbo_vert->Finalized() || !vbo_uv->Finalized() || !vbo_norm->Finalized()  ) {
        ERR("Cannot draw animated model, vbos not finalized.\n");
        return false;
    }

    if ( !drawInfo.animInfo )
        return false;
    
    if ( drawInfo.animateThisFrame || !drawInfo.animInfo->BonePosComputed() ) {
        if ( !drawInfo.animInfo->ComputeBonePos() ) {
            return false;
        }
    }

    if ( !lighted || !globalVals.GetBool( gval_v_light ) ) {
        shaders->UseProg(   GLPROG_ANIM_MESH );
    } else {
        shaders->UseProg(  GLPROG_ANIM_TEX_MULTI_LIGHT  );
        
        int numDirLights = 0;
        for( uint i=0; i < renderer->dirLights.size(); i++ ) {
            std::shared_ptr< DirLight > dl = renderer->dirLights[i].light.lock();
            if ( !dl )
                continue;
                
            numDirLights += 1;
            std::string lightIndex( "dirLights[" );
            lightIndex += std::to_string(i) + std::string( "]" );
            
            shaders->SetUniform3f( ( lightIndex + ".direction").c_str() , dl->GetDirection() * renderer->ViewMatrix().GetRotationMatrix() );
            shaders->SetUniform3f( ( lightIndex + ".diffuse").c_str() , dl->GetColor() );
            shaders->SetUniform3f( ( lightIndex + ".ambient").c_str() , dl->GetAmbientStrength() );
        }
        shaders->SetUniform1i( "numDirLights", numDirLights );
        
        int numPointLights = 0;
        for ( const auto & lightInfo : renderer->pointLights ) {
            std::shared_ptr< PointLight > light = lightInfo.light.lock();
            if ( !light )
                continue;

            std::string lightIndex( "pointLights[" );
            lightIndex += std::to_string(numPointLights) + std::string( "]" );

            shaders->SetUniform3f( ( lightIndex + ".position").c_str() , light->GetOrigin() * renderer->ViewMatrix() );
            shaders->SetUniform3f( ( lightIndex + ".diffuse").c_str() , light->GetColor() );
            shaders->SetUniform3f( ( lightIndex + ".ambient").c_str() , light->GetAmbientStrength() );
            shaders->SetUniform1f( ( lightIndex + ".constant").c_str() , light->Attenuation().constant );
            shaders->SetUniform1f( ( lightIndex + ".quadratic").c_str() , light->Attenuation().quadratic );
            shaders->SetUniform1f( ( lightIndex + ".linear").c_str() , light->Attenuation().linear );

            ++numPointLights;
        }
        shaders->SetUniform1i( "numPointLights", numPointLights );
        
        shaders->SetAttrib( "vNorm", *vbo_norm );
    }    
    
    renderer->PushMatrixMV();
        shaders->SendData_Matrices();
        if ( lighted && globalVals.GetBool( gval_v_light ) ) {
            auto const mat_data = renderer->ModelView().GetRotationMatrix().data;
            shaders->SetUniformMatrix3fv( "N" ,1, GL_FALSE, mat_data ); //Assumes matrix has uniform scaling
        }
    renderer->PopMatrixMV();

    shaders->SetAttrib( "vPos", *vbo_vert );
    shaders->SetAttrib( "vUV", *vbo_uv );
    shaders->SetAttrib( "Bone_ID", *vbo_mesh_boneID );
    shaders->SetAttrib( "Bone_WGT", *vbo_mesh_boneWeights );
    shaders->SetTexture( drawInfo.material->GetDiffuse()->GetID() );
    shaders->SetUniform4f("vColor", drawInfo.colorize.r, drawInfo.colorize.g, drawInfo.colorize.b, drawInfo.opacity );
    shaders->SetUniform1f("fWeight", drawInfo.colorize.a );

    if ( ! drawInfo.animInfo->SendShaderInfo() ) {
        return false;
    }
    
    shaders->DrawArrays( GL_TRIANGLES, 0, vbo_uv->Num() );    
    return true;
}

AnimChannelT AnimatedModel::GetChannelID( const std::size_t index ) const {
    ASSERT( index < numChannels );
    return channels[index];
}

uint AnimatedModel::GetChannelIndex( const AnimChannelT channel ) const {
    const AnimChannelT_BaseType c = static_cast< AnimChannelT_BaseType >( channel );
    ASSERT( c < MAX_ANIM_CHANNELS );
    return channel_indices[c];
}

std::size_t AnimatedModel::GetArmatureBoneCount( void ) const {
    return boneCount;
}

uint AnimatedModel::NumChannels( void ) const {
    return numChannels;
}

uint AnimatedModel::GetNumBones( void ) const {
    return numBones;
}
