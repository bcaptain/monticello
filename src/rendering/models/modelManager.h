// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_MODELS_MODELMANAGER_H_
#define SRC_RENDERING_MODELS_MODELMANAGER_H_

#include "../../base/main.h"
#include "../../files/manifest.h"
#include "../../files/assetManager.h"

extern const uint ANIMCUE_PROJECTILE_NAME_INDEX;
extern const uint ANIMCUE_ENTITY_NAME_INDEX;
extern const uint ANIMCUE_EFFECT_NAME_INDEX;
extern const uint ANIMCUE_SOUND_NAME_INDEX;
extern const uint ANIMCUE_FIRE_BULLET_DAMAGE_NAME_INDEX;

class Bone;
class Model;
class AnimatedModel;
struct AnimCue;

extern const std::vector<const char *> attachmentPositions;

class ModelData : public AssetData< Model > {
public:
    ModelData( void );
    ModelData( const std::string& _name, const std::string& _path, const char* _container = "" /* blank means filesystem */ );
    ModelData( const ModelData& other );
    ~ModelData( void ) override { };

public:
    ModelData& operator=( const ModelData& other );

public:
    std::vector< std::string > available_attachments;
};

/*!

ModelManager \n\n

holds all the models an anims for the game. Models are just the model name, anim is modelname_animname.

**/

class ModelManager : public AssetManager< ModelData, Model > {
public:
    ModelManager( void );

public:
    void RegisterAnim( const std::string& path, const char* container = "" /* blank means filesystem */ ); //!< will load the animation when the model is loaded
    void RegisterAnimCue( const std::string& path, const char* container = "" /* blank means filesystem */ ); //!< will load the animcue when the model is loaded
    void RegisterAnimEvents( const std::string& path, const char* container = "" /* blank means filesystem */ ); //!< will load the animevent when the model is loaded

    void PackAllClipModelVBOs( void ) const;
    
    void PreloadAttachments( const ModelData& data ) const;
    void ManifestAttachments( void );
    
private:
    bool Load( File& opened_model_file ) override;
    using AssetManager< ModelData,Model >::Load;

    void LoadAllAnimsForModel( AnimatedModel& model );
    void LoadAllAnimCuesForModel( AnimatedModel& model );
    bool LoadAnim( AnimatedModel& toAnimModel, File& opened_anim_file ); //!< warning: don't override animations when a map is loaded
    bool LoadAnim( AnimatedModel& toAnimModel, const AssetManifest& am ); //!< warning: don't override animations when a map is loaded

    // cue files tell the entity what events to do when an animtion reaches specified frames
    bool LoadAnimCue( AnimatedModel& model, const std::string& animName, const AssetManifest& data );
    bool LoadAnimCue( AnimatedModel& model, const std::string& animName, File& opened_file );

    // events are called by cues
    const std::shared_ptr< const std::vector< std::pair< uint, std::string > > > GetEventList( const std::string& modelName );

private:
    HashList< AssetManifest > accessory_manifest; //!< used for .bm1anim, .cue
    HashList< AssetData< std::vector< std::pair< uint, std::string > > > > events_manifest;
    static bool instantiated;
};

extern ModelManager modelManager;

#endif  // SRC_RENDERING_MODELS_MODELMANAGER_H_
