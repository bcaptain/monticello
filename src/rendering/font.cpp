// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./font.h"
#include "../ui/widget.h"
#include "../game.h"

const float FONT_DEFAULT_SIZE(10);
const float FONT_LETTER_TEXTURE_SIZE(64);
const int FONT_LETTER_ROWS(16);
const int FONT_LETTER_COLS(FONT_LETTER_ROWS);
const float FONT_LETTER_TEX_COORD_INTERVAL = 1.0f / FONT_LETTER_ROWS;

Vec2f Font::letter[256];

Font::Font( void )
    : name()
    , material(nullptr)
    {
}

Font::Font( const char *nam )
    : name(nam)
    , material(nullptr)
    {
}

Font::Font( const std::string& nam )
    : name(nam)
    , material(nullptr)
    {
}

Font::Font( const Font& other )
    : name(other.name)
    , material(other.material)
    {
}

Font& Font::operator=( const Font& other ) {
    name = other.name;
    material = other.material;
    return *this;
}

void Font::Init( void ) {
    int x, i;
    float row;

    // **** generate UV offsets for letters within the font image

    // ** row 16 (top)
    row = 1.0f - FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( x='a'; x<='m'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    // ** row 15
    row -= FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( ; x<='z'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    // ** row 14
    row -= FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( x='A'; x<='M'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    // ** row 13
    row -= FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( ; x<='Z'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    // ** row 12
    row -= FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( x='0'; x<='9'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    // ** row 11
    row -= FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( x='!'; x<='/'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );
        
    x=':';
    letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    // ** row 10
    row -= FONT_LETTER_TEX_COORD_INTERVAL;
    i=0;
    for ( x=';'; x<='@'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    for ( x='['; x<='`'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );

    for ( x='{'; x<='~'; ++x, ++i )
        letter[x].Set( FONT_LETTER_TEX_COORD_INTERVAL*i, row );
}

void Font::AssertValid( const std::shared_ptr< const Font >& fnt, const char* fontname ) {
    if (!fnt) {
        DIE("Font not found: %s.\n", fontname );
        return;
    }

    if (!fnt->material) {
        DIE("Font has no material: %s.\n", fontname );
        return;
    }

    if (!fnt->material->GetDiffuse()) {
        DIE("Font's material (%s) has no diffuse: %s\n", fnt->material->GetName().c_str(), fontname );
        return;
    }
}

std::shared_ptr< const Material > Font::GetMaterial( void ) const {
    return material;
}

void Font::SetMaterial( const std::shared_ptr< const Material >& mtl ) {
    material = mtl;
}

void Font::SetName( const char *nam ) {
    name=nam;
}

void Font::SetName( const std::string& nam ) {
    name=nam;
}

std::string Font::GetName( void ) const {
    return name;
}

Vec2f Font::GetCharCoord( const unsigned short int i ) {
    return letter[i];
}

FontInfo::FontInfo( void )
    : size_ui_px( FONT_DEFAULT_SIZE )
    , pos( static_cast<int>(FONT_DEFAULT_POSITION_X), static_cast<int>(FONT_DEFAULT_POSITION_Y) )
    , fontname("courier")
    , color(0,0,0)
    , opacity(1)
    {

}

FontInfo::FontInfo( const float sz, const Vec2f& position, const char* fnt, const Vec3f& col, const float opac )
    : size_ui_px(sz)
    , pos(position)
    , fontname(fnt)
    , color(col)
    , opacity(opac)
    {

}

FontInfo::FontInfo( const FontInfo& other )
    : size_ui_px(other.size_ui_px)
    , pos(other.pos)
    , fontname(other.fontname)
    , color(other.color)
    , opacity(other.opacity)
    {

}

FontInfo& FontInfo::operator=( const FontInfo& other ) {
    size_ui_px = other.size_ui_px;
    pos = other.pos;
    fontname = other.fontname;
    color = other.color;
    opacity = other.opacity;
    return *this;
}
