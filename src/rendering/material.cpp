// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./material.h"
#include "./materialManager.h"

Material::Material( void )
    : name()
    , diffuse(nullptr)
    {
}

Material::Material( const std::string& nam, const std::shared_ptr< const Texture >& _diffuse )
    : name(nam)
    , diffuse(_diffuse)
    {
}
Material::Material( const std::string& nam )
    : name(nam)
    , diffuse(nullptr)
    {
}

Material::Material( const Material& other )
    : name(other.name)
    , diffuse(other.diffuse)
    {
}

Material& Material::operator=( const Material& other ) {
    name = other.name;
    diffuse = other.diffuse;

    return *this;
}

std::shared_ptr< const Texture > Material::GetDiffuse( void ) const {
    return diffuse;
}

void Material::SetDiffuse( const std::shared_ptr< const Texture >& to ) {
    diffuse = to;
}

std::string Material::GetName( void ) const {
    return name;
}

void Material::SetName( const std::string& to ) {
    name = to;
}

void MapSaveMaterial( FileMap& saveFile, LinkList< std::string >& string_pool, const std::shared_ptr< const Material >& material ) {
    if ( !material ) {
        saveFile.parser.WriteBool( false );
        return;
    }

    saveFile.parser.WriteBool( true );
    saveFile.parser.WriteUInt( string_pool.AppendUnique( material->GetName() ) );
}

std::shared_ptr< const Material > MapLoadMaterial( FileMap& saveFile, const std::vector< std::string >& string_pool ) {
    if ( !saveFile.parser.ReadBool() )
        return nullptr;

    uint idx;
    saveFile.parser.ReadUInt( idx );
    const std::string* strp = &string_pool[idx];
    ASSERT( strp );

    return materialManager.Get( strp->c_str() );
}

uint Material::GetDiffuseWidth( void ) const {
    if ( !diffuse )
        return 0;
    return diffuse->GetWidth();
}

uint Material::GetDiffuseHeight( void ) const {
    if ( !diffuse )
        return 0;
    return diffuse->GetHeight();
}
