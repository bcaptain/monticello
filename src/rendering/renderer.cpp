// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./renderer.h"
#include "./vbo.h"
#include "./camera.h"
#include "../particles/effectsManager.h"
#include "../game.h"
#include "../editor/editor.h"
#include "../console.h"
#include "./fontManager.h"
#include "./shapes.h"
#include "../nav/navMesh.h"
#include "./models/modelManager.h"
#include "./materialManager.h"
#include "../entities/actor.h"

class Renderer;
Renderer *renderer=nullptr;
Shaders *shaders=nullptr;

bool Renderer::CheckGLError( const char* message ) {
    GLenum error = glGetError();

    if ( error != 0 ) {
        while ( error != 0 ) {
            ERR("GL error %i %s\n", error, message );
            error = glGetError();
        }

        DIE("An OpenGL error occured, shutting down.\n");
        return true;
    }
    
    return false;
}

Renderer::Renderer( void )
    : glinfo()
    , view()
    , compass()
    , RenderEffectData()
    , modelView(1)
    , projection(1)
    , viewMatrix()
    , inverseViewMatrix()
    , viewport()
    , vbo_framely_lines( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_framely_lines_colors( new VBO<float>( 4,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_framely_points( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_framely_points_sizes( new VBO<float>( 1,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_framely_points_colors( new VBO<float>( 4,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_framely_tris( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_framely_tris_colors( new VBO<float>( 4,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_lines( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_lines_colors( new VBO<float>( 4,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_points( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_points_sizes( new VBO<float>( 1,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_points_colors( new VBO<float>( 4,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_tris( new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_debug_framely_tris_colors( new VBO<float>( 4,VBOChangeFrequencyT::FREQUENTLY) )
    , vbo_fps(nullptr)
    , vbo_fps_uv(nullptr)
    , frame(0)
    , effects( EffectT::NONE )
    , camera()
    , shaders()
    , skybox()
    , modelView_atfullzoom()
    , modelView_atnozoom()
    , dirLights()
    , pointLights()
    , projectors()
    , crosshair_origin()
    , backgroundDarkness(0.0f)
{
    ::shaders = &shaders;
}

Renderer::~Renderer( void ) {
    delete vbo_framely_lines;
    delete vbo_framely_lines_colors;
    delete vbo_framely_points;
    delete vbo_framely_points_colors;
    delete vbo_framely_points_sizes;
    delete vbo_framely_tris;
    delete vbo_framely_tris_colors;

    delete vbo_debug_framely_lines;
    delete vbo_debug_framely_lines_colors;
    delete vbo_debug_framely_points;
    delete vbo_debug_framely_points_colors;
    delete vbo_debug_framely_points_sizes;
    delete vbo_debug_framely_tris;
    delete vbo_debug_framely_tris_colors;

    delete vbo_fps;
    delete vbo_fps_uv;
    pointLights.clear();
    dirLights.clear();
    projectors.clear();
    SDL_ShowCursor(true);
}

int Renderer::Init( void ) {
    CMSG_LOG("---- Renderer Init ----\n");

    glinfo.Init();

    if ( globalVals.GetBool( gval_i_gl ) )
        glinfo.Print();

    UpdateWindowSize();

    //glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    glFrontFace(GL_CW);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glDepthFunc(GL_LEQUAL);
    glClearDepth(1.0);
    //glAlphaFunc ( GL_GREATER, 0.0f );
    //glEnable ( GL_ALPHA_TEST );
    Particle::Init();
    glEnable( GL_POINT_SMOOTH );
    CheckGLError( "during init" );

    return 0;
}

void Renderer::PushMatrixMV( void ) {
    // if the capacity is not big enough to store another matrix, modelView[x] will be come invalidated after a resize
    if ( modelView.capacity() <= modelView.size() ) {
        modelView.reserve( modelView.size() + 3 );
    }

    modelView.emplace_back( modelView[modelView.size()-1] );
}

void Renderer::PopMatrixMV( void ) {
    if ( modelView.size() == 1 ) {
        ASSERT_MSG(false, "attempted to pop the last modelview matrix");
        return;
    }
    modelView.pop_back();
}

void Renderer::PushMatrixPJ( void ) {
    // if the capacity is not big enough to store another matrix, modelView[x] will be come invalidated after a resize
    if ( projection.capacity() <= projection.size() ) {
        projection.reserve( projection.size() + 3 );
    }

    projection.emplace_back( projection[projection.size()-1] );
}

void Renderer::PopMatrixPJ( void ) {
    if ( projection.size() == 1 ) {
        ASSERT_MSG(false, "attempted to pop the last projection matrix");
        return;
    }
    projection.pop_back();
}

void Renderer::AddLight( Entity& light ) {
    std::shared_ptr< Entity > light_wptr = light.GetWeakPtr().lock();
    ASSERT( light_wptr );

    const auto type = light.GetObjectTypeID();
    switch ( type ) {
        case TypeID_DirLight:
            dirLights.emplace_back( DirLightInfo( std::static_pointer_cast< DirLight >( light_wptr ) ) );
            break;
        case TypeID_PointLight:
            pointLights.emplace_back( PointLightInfo( std::static_pointer_cast< PointLight >( light_wptr ) ) );
            break;
        case TypeID_Light:
            FALLTHROUGH;
        default:
            ERR("Renderer::AddLight(): Unknown Light type ( class type %u )\n", type );
            break;
    }
}

void Renderer::AddProjector( Entity& projector ) {
    std::shared_ptr< Entity > projector_wptr = projector.GetWeakPtr().lock();
    ASSERT(projector_wptr);
    projectors.emplace_back( ProjectorInfo( std::static_pointer_cast< Projector >( projector_wptr ) ) );
}

void Renderer::RemoveProjector( const std::shared_ptr<Entity>& projector ) {
    for ( uint i=0; i<projectors.size(); ++i ) {
        auto check = projectors[i].projector.lock();
        if ( check == projector ) {
            PopSwap( projectors, i );
            return;
        }
    }
}

void Renderer::ToggleFullScreen( void ) {
    sdl.ToggleFullScreen();
    renderer->UpdateWindowSize();
}

void Renderer::SetFullScreen( const bool whether ) {
    sdl.SetFullScreen( whether );
}

void Renderer::InitProjectionMatrix( void ) {
    Projection().Perspective(
        globalVals.GetFloat( gval_v_fov ),
        globalVals.GetFloat( gval_v_width ),
        globalVals.GetFloat( gval_v_height ),
        globalVals.GetFloat( gval_v_nearPlane ),
        globalVals.GetFloat( gval_v_farPlane )
    );
}

void Renderer::UpdateWindowSize( void ) {
    const int width = globalVals.GetInt( gval_v_width );
    const int height = globalVals.GetInt( gval_v_height );

    sdl.SetWindowSize( width, height );

    glViewport(0,0,width,height);
    InitProjectionMatrix();
    glGetIntegerv( GL_VIEWPORT, viewport );
    CMSG_LOG("Resolution set to: %ix%i\n", width,height);
    CMSG_LOG("Viewport set to: %i,%i,%i,%i\n",viewport[0],viewport[1],viewport[2],viewport[3]);
    view.Init();
    CheckGLError( "resizing scene" );
}

void Renderer::DebugThink( void ) {
    if ( !game->IsPaused() ) {
        ClearEditorFramelyShapes();
    }

    if ( !globalVals.GetBool( gval_v_keepFramelyShapes ) )
        ClearFramelyShapes();
}

void Renderer::PrepareModelViewForScene( void ) {
    ModelView().LoadIdentity();
    modelView_atfullzoom.LoadIdentity();
    modelView_atnozoom.LoadIdentity();

    // set zoom distance
    modelView_atfullzoom.Translate(0,0, camera.GetZoom() );
    if ( game->editor_open ) {
        ModelView().Translate(0,0, camera.GetZoom() );
    } else {
        ModelView().Translate(0,0, camera.GetCurZoom() );
    }

    // set camera tilt around X axis
    ModelView().Rotate( camera.GetTilt(), 1, 0, 0 );
    modelView_atfullzoom.Rotate( camera.GetTilt(), 1, 0, 0 );
    modelView_atnozoom.Rotate( camera.GetTilt(), 1, 0, 0 );
    
    // set camera rotation around vertical axis
    ModelView().Rotate( camera.GetRot(), 0, 0, 1 );
    modelView_atfullzoom.Rotate( camera.GetRot(), 0, 0, 1 );
    modelView_atnozoom.Rotate( camera.GetRot(), 0, 0, 1 );

    if ( !game->editor_open ) {
        const Vec3f cam_focus_offset( globalVals.GetVec3f(gval_cam_focalOffset) );
        // move to focal point
        ModelView().Translate( cam_focus_offset );
        modelView_atfullzoom.Translate( cam_focus_offset );
        modelView_atnozoom.Translate( cam_focus_offset );

        camera.Update();
        ProcessRenderEffects();
    } else {
        // save the matrix, transposed (mouse panning will need this)
        Mat3f i = ModelView();
        i.Transpose();
        camera.SetMousePanMatrix( i );
    }
    
    Vec3f focus_origin = camera.GetFocusOrigin();
    focus_origin.z = 0;
    ModelView().Translate( -focus_origin );
    modelView_atfullzoom.Translate( -focus_origin );
    modelView_atnozoom.Translate( -focus_origin );
    
    viewMatrix = ModelView();
    inverseViewMatrix = viewMatrix;
    inverseViewMatrix.Invert();
}

void Renderer::DrawGLScene( const bool clear_screen ) {

    if ( clear_screen ) {
        glClearColor(backgroundDarkness, backgroundDarkness, backgroundDarkness, 1.0f);
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
        glEnable( GL_DEPTH_TEST ); // this is to be always enabled at the exit of every function.
    }
    
    PrepareModelViewForScene();
    
    CheckGLError( "before render scene" );

    DrawOpaqueThings();
    DrawTransparentThings();
    DrawWorldUI();
    DrawEditor();
    DrawDebugMode();
    DrawHUDUI();
    
    UpdateFrame();
    
    CheckGLError( "after rendering scene" );
}

void Renderer::UpdateFrame( void ) {
    sdl.SwapBuffers();
#ifdef MONTICELLO_DEBUG
    if ( globalVals.GetBool( gval_v_pauseAfterRender ) ) {
        game->Pause( PauseStateT::PausedByEditor );
        globalVals.SetBool( gval_v_pauseAfterRender, false );
        globalVals.SetBool( gval_v_keepFramelyShapes, true);
    } else if ( !game->IsPaused() ) {
        globalVals.SetBool( gval_v_keepFramelyShapes, false );
    }
#endif //MONTICELLO_DEBUG
    ++frame;
}

void Renderer::DrawOpaqueThings( void ) {
    const bool frustum_culling = globalVals.GetBool( gval_v_frustCull );
    
    //skybox.PackVBO();
    //skybox.DrawVBO();
    
    if ( frustum_culling ) {
        view.DrawOpaqueEnts();
    } else {
        game->entTree->DrawOpaqueEnts(); // draws everything
    }
}

void Renderer::DrawTransparentThings( void ) {
    glDepthMask( GL_FALSE );

        EntityTree::DrawDeferredEnts(); // includes transparent entities
        effectsManager.ProcessAndDraw();

    glDepthMask( GL_TRUE );

    // ** draw crosshair
    if( globalVals.GetBool( gval_d_crossHair ) ) {
        const Vec3f cam_focus_offset( globalVals.GetVec3f(gval_cam_focalOffset) );
        Vec3f crosshair_offset(0,0,cam_focus_offset.z); // we only want the height from the camera's focus origin}
        crosshair_offset += globalVals.GetVec3f( gval_crosshairPos );
        crosshair_origin = camera.GetOriginAtNoZoom( crosshair_offset );
        DrawPoint( crosshair_origin, Color4f(1,0,0,1), 20 );
    }
}

void Renderer::DrawEditor( void ) {
    if ( globalVals.GetBool( gval_d_navMesh ) )
        NavMesh::DrawVBO_AllMeshes();
    
    if ( globalVals.GetBool( gval_d_light ) )
        DrawLightEmissions();
        
    if ( globalVals.GetBool(gval_d_worldOrigin ) )
        DrawPoint( Vec3f(0,0,0), Color4f(1,1,0,1), 20 );
        
    if ( globalVals.GetBool( gval_d_globalAxis ) )
        DrawGlobalAxis();

#ifdef MONTICELLO_EDITOR
    if ( editor && game->editor_open ) {
        editor->PackVBO();
        editor->Draw();
    }
#endif // MONTICELLO_EDITOR
}

void Renderer::DrawEntity( Entity& ent ) {
    if ( game->GetRenderFrame() < ent.GetLastDrawFrame() )
        return; // entities can exist in multiple voxels
        
    ent.PackVBO();
    
    PushMatrixMV();
        ModelView().Translate( ent.GetOrigin() );
        
        const auto angle = ent.bounds.primitive->GetAngles();
        if ( !Maths::Approxf(angle, Vec3f(0,0,0) ) ) {
            ModelView().RotateZYX( angle );
        }
            
        ent.DrawVBO();

    PopMatrixMV();
    
    ent.SetLastDrawFrame( frame );
}

void Renderer::DrawSprite( Sprite& sprite ) {
    if ( game->GetRenderFrame() < sprite.GetLastDrawFrame() ) // this isn't likely to happen but it's cheap enough to check
        return;

    PushMatrixMV();
        ModelView().Translate( sprite.GetOrigin() );
        
        // we must keep translation but discard rotation in order for text to always face the screen
        Vec3f translation( ModelView().GetTranslation() );
        ModelView().LoadIdentity();
        ModelView().SetTranslation( translation );
        
        renderer->ModelView().Scale( 1.0f,-1.0f,1.0f );

        sprite.DrawVBO();
        sprite.SetLastDrawFrame( frame );
        
    PopMatrixMV();
}

EffectT Renderer::GetEffectID( const std::string& effectName ) const {
    if ( effectName == "shake" ) {
        return EffectT::SHAKE;
    }

    ERR("Renderer::GetEffectID(): unknown effect name: %s\n", effectName.c_str());
    return EffectT::NONE;
}

void Renderer::ProcessRenderEffects( void ) {
    uint i=0;
    while ( i<RenderEffectData.size() ) {
        if ( game->GetGameTime() - RenderEffectData[i].startTime > RenderEffectData[i].duration ) {
            effects &= ~RenderEffectData[i].effect;
            RenderEffectData.erase(std::cbegin(RenderEffectData)+static_cast<int>(i));
            continue;
        }

        switch ( static_cast< EffectT_BaseType >( RenderEffectData[i].effect ) ) {
            case static_cast< EffectT_BaseType >( EffectT::SHAKE ):
                EvalShake( RenderEffectData[i].intensity );
                break;
            default:
                break;
        }

        ++i;
    }
}

void Renderer::EvalShake( const float shake_amount ) {
    if ( BitwiseAnd( effects, EffectT::SHAKE ) )
        return;

    // all of the values in this function are magic numbers that just "felt the best"
    // values of x/y and x are negligibly different because it adds a bit of unpredictable movement. otherwise it just looks mechanical if the values are identical

    const uint curTime = game->GetGameTime();

    // ** this sets up the rotation (circling of the camera around it's origin)

    // this determines the speed at which the "jitter destination points" moves the circle that the camera shakes
    const float xy_rotational_frequency = 97;
    const float xy_time = curTime / xy_rotational_frequency;

    // this determines the speed at which the camera zoomes in and out
    const float z_inout_frequency = 133;
    const float z_time = -curTime / z_inout_frequency; // making Z negative just for spice

    Vec3f tran( sinf(xy_time), cosf(xy_time), sinf(z_time) );

    const float xy_distanceMultiplier = 0.15f; // this determines the size of the circle around which the camera shakes
    const float z_distanceMultiplier = 0.21f; // this determines the distance the camera zooms in and out
    tran.x *= xy_distanceMultiplier;
    tran.y *= xy_distanceMultiplier;
    tran.z *= z_distanceMultiplier;

    // ** this does the jittering
    const float xy_jitterFrequency = 17; // this determines the speed at which the camera jerks from one point to another (the lower, the higher the frequency)
    const float z_jitterFrequency = 27;
    tran.x *= sinf( curTime / xy_jitterFrequency );
    tran.y *= sinf( curTime / xy_jitterFrequency );
    tran.z *= sinf( curTime / z_jitterFrequency );

    tran *= shake_amount;

    ModelView().Translate( tran );
}

void Renderer::ActivateEffect( const std::string& effectName, const uint duration, const float _intensity ) {
    ActivateEffect( renderer->GetEffectID( effectName ), duration, _intensity );
}

void Renderer::ActivateEffect( const EffectT _effect, const uint duration, const float _intensity ) {
    if ( _effect == EffectT::NONE )
        return;

    effects |= _effect;
    const bool FOREVER = 0;

    if ( duration == FOREVER )
        return;

    RenderEffectData.emplace_back(
        _effect
        , game->GetGameTime()
        , duration
        , _intensity
    );
}

#ifdef MONTICELLO_EDITOR
    void Renderer::DrawLine_Debug( const Vec3f& start, const Vec3f& end, const Color4f& color_start, const Color4f& color_end ) {
        vbo_debug_framely_lines->Pack( start );
        vbo_debug_framely_lines->Pack( end );
        vbo_debug_framely_lines_colors->Pack( color_start );
        vbo_debug_framely_lines_colors->Pack( color_end );
    }

    void Renderer::DrawPoint_Debug( const Vec3f& at, const Color4f& color, const float size ) {
        vbo_debug_framely_points->Pack( at );
        vbo_debug_framely_points_colors->Pack( color );
        vbo_debug_framely_points_sizes->Pack( size );
    }

    void Renderer::DrawPoly_Debug( const std::vector< Vec3f >& ordered_verts, const Color4f& color ) {
        const uint a=0;
        for ( uint b=1, c=2; c<ordered_verts.size(); ++b, ++c ) {
            DrawTri( ordered_verts[a], ordered_verts[b], ordered_verts[c], color, color, color );
        }
    }

    void Renderer::DrawTri_Debug( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& colorA, const Color4f& colorB, const Color4f& colorC ) {
        vbo_debug_framely_tris->Pack( A );
        vbo_debug_framely_tris->Pack( B );
        vbo_debug_framely_tris->Pack( C );
        vbo_debug_framely_tris_colors->Pack( colorA );
        vbo_debug_framely_tris_colors->Pack( colorB );
        vbo_debug_framely_tris_colors->Pack( colorC );
    }

    void Renderer::DrawTri_Debug( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& color ) {
        vbo_debug_framely_tris->Pack( A );
        vbo_debug_framely_tris->Pack( B );
        vbo_debug_framely_tris->Pack( C );
        vbo_debug_framely_tris_colors->PackRepeatedly( 3, color );
    }

    void Renderer::DrawLine( const Vec3f& start, const Vec3f& end, const Color4f& color_start, const Color4f& color_end ) {
        vbo_framely_lines->Pack( start );
        vbo_framely_lines->Pack( end );
        vbo_framely_lines_colors->Pack( color_start );
        vbo_framely_lines_colors->Pack( color_end );
    }

    void Renderer::DrawPoint( const Vec3f& at, const Color4f& color, const float size ) {
        vbo_framely_points->Pack( at );
        vbo_framely_points_colors->Pack( color );
        vbo_framely_points_sizes->Pack( size );
    }
    
    void Renderer::DrawSphere( const uint num_subdivides, const float radius, const Vec3f& origin) {
        PackVBO_Sphere( num_subdivides, radius, origin, *vbo_framely_lines, vbo_framely_lines_colors );
    }

    void Renderer::DrawPoly( const std::vector< Vec3f >& ordered_verts, const Color4f& color ) {
        const uint a=0;
        for ( uint b=1, c=2; c<ordered_verts.size(); ++b, ++c ) {
            DrawTri( ordered_verts[a], ordered_verts[b], ordered_verts[c], color, color, color );
        }
    }

    void Renderer::DrawTri( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& colorA, const Color4f& colorB, const Color4f& colorC ) {
        vbo_framely_tris->Pack( A );
        vbo_framely_tris->Pack( B );
        vbo_framely_tris->Pack( C );
        vbo_framely_tris_colors->Pack( colorA );
        vbo_framely_tris_colors->Pack( colorB );
        vbo_framely_tris_colors->Pack( colorC );
    }

    void Renderer::DrawTri( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& color ) {
        vbo_framely_tris->Pack( A );
        vbo_framely_tris->Pack( B );
        vbo_framely_tris->Pack( C );
        vbo_framely_tris_colors->PackRepeatedly( 3, color );
    }
#endif // MONTICELLO_EDITOR

void Renderer::DrawFramelyLines( VBO<float>* vbo_shape, VBO<float>* vbo_colors ) {
    if ( vbo_shape->Num() < 1 )
        return;

    if ( vbo_colors->Num() < 1 )
        return;

    vbo_shape->MoveToVideoCard();
    vbo_colors->MoveToVideoCard();

    if ( !vbo_shape->Finalized() || !vbo_colors->Finalized() ) {
        WARN("Couldn't draw framely lines.\n");
        return;
    }

    shaders.UseProg(  GLPROG_GRADIENT  );
    shaders.SendData_Matrices();
    shaders.SetAttrib( "vColor", *vbo_colors );
    shaders.SetAttrib( "vPos", *vbo_shape );
    shaders.DrawArrays( GL_LINES, 0, vbo_shape->Num() );
}

void Renderer::DrawFramelyPoints( VBO<float>* vbo_shape, VBO<float>* vbo_sizes, VBO<float>* vbo_colors ) {
    if ( ! vbo_shape || vbo_shape->Num() < 1 )
        return;

    if ( !vbo_sizes || vbo_sizes->Num() < 1 )
        return;

    if ( !vbo_colors || vbo_colors->Num() < 1 )
        return;

    vbo_shape->MoveToVideoCard();
    vbo_colors->MoveToVideoCard();
    vbo_sizes->MoveToVideoCard();

    if ( !vbo_shape->Finalized() || !vbo_colors->Finalized() || !vbo_sizes->Finalized() ) {
        WARN("Couldn't draw framely points.\n");
        return;
    }

    glEnable( GL_VERTEX_PROGRAM_POINT_SIZE );
        shaders.UseProg(  GLPROG_POINTS  );
        shaders.SendData_Matrices();
        shaders.SetAttrib( "vColor", *vbo_colors );
        shaders.SetAttrib( "vSize", *vbo_sizes );
        shaders.SetAttrib( "vPos", *vbo_shape );
        shaders.DrawArrays( GL_POINTS, 0, vbo_shape->Num() );
    glDisable( GL_VERTEX_PROGRAM_POINT_SIZE );
}

void Renderer::DrawFramelyTris( VBO<float>* vbo_shape, VBO<float>* vbo_uv ) {
    if ( !vbo_shape || vbo_shape->Num() < 1 )
        return;

    if ( !vbo_uv || vbo_uv->Num() < 1 )
        return;

    vbo_shape->MoveToVideoCard();
    vbo_uv->MoveToVideoCard();

    if ( !vbo_shape->Finalized() || !vbo_uv->Finalized() ) {
        WARN("Couldn't draw framely tris.\n");
        return;
    }

    shaders.UseProg(  GLPROG_GRADIENT  );
    shaders.SendData_Matrices();
    shaders.SetAttrib( "vColor", *vbo_uv );
    shaders.SetAttrib( "vPos", *vbo_shape );
    shaders.DrawArrays( GL_TRIANGLES, 0, vbo_shape->Num() );
}

void Renderer::DrawLightEmissions( void ) {
    DrawLightEmissions( pointLights );
    DrawLightEmissions( dirLights );
}

void Renderer::DrawGlobalAxis( void ) {
    Vec3f offset(0.75f,0.5f,-1.3f);
    offset *= inverseViewMatrix;
    const float scale = 0.18f;
    
    const std::vector<Vec3f> axes {
        Vec3f(0,0,0) * scale + offset
        , Vec3f(1,0,0) * scale + offset
        , Vec3f(0,1,0) * scale + offset
        , Vec3f(0,0,1) * scale + offset
    };
    DrawLine( Line3f(axes[0],axes[1]), Color4f(1,0,0,1), Color4f(1,0,0,1) );
    DrawLine( Line3f(axes[0],axes[2]), Color4f(0,1,0,1), Color4f(0,1,0,1) );
    DrawLine( Line3f(axes[0],axes[3]), Color4f(0,0,1,1), Color4f(0,0,1,1) );
}

void Renderer::DrawCompass( void ) {
    ModelDrawInfo di;

    if ( ! compass.HasDrawModel() ) {
        compass.SetModel( modelManager.Get("test_arrow") );
    }

    if ( !compass.HasDrawModel() )
        return;

    bool set_d_models_false = false;
    if ( !globalVals.GetBool( gval_d_models ) ) {
        set_d_models_false = true;
        globalVals.SetBool( gval_d_models, true );
    }

    PushMatrixMV();

        const float height = static_cast< float >( globalVals.GetInt( gval_v_height ) );
        const float width = static_cast< float >( globalVals.GetInt( gval_v_width ) );
        const float aspect = width / height;

        ModelView().LoadIdentity();
        ModelView().Translate(0.5f*aspect,-0.5f, -1.0f);
        ModelView().Rotate( camera.GetTilt(), 1, 0, 0 );
        ModelView().Rotate( camera.GetRot(), 0, 0, 1 );
        ModelView().Scale(0.1f,0.1f,0.1f);

        compass.Draw(di);

    PopMatrixMV();

    if ( set_d_models_false )
        globalVals.SetBool( gval_d_models, false );
}

void Renderer::DrawFPS( void ) {
    const char* fontname = "courier";

    std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
    Font::AssertValid( fnt, fontname );

    FontInfo fi;
    fi.size_ui_px = 16;

    if ( ! vbo_fps ) {
        vbo_fps = new VBO<float>( 2, VBOChangeFrequencyT::FREQUENTLY );
    }

    if ( ! vbo_fps_uv ) {
        vbo_fps_uv = new VBO<float>( 2, VBOChangeFrequencyT::FREQUENTLY );
    }

    vbo_fps->Clear();
    vbo_fps_uv->Clear();
    std::string tmp( std::to_string( game->GetFPS() ) );
    String::TrimTrailing( tmp, "0" );

    vbo_fps->PackText( *vbo_fps_uv, tmp.c_str(), fi );
    vbo_fps->MoveToVideoCard();
    vbo_fps_uv->MoveToVideoCard();

    if ( ! vbo_fps->Finalized() || ! vbo_fps_uv->Finalized() ) {
        ERR("Couldn't display FPS.\n");
        globalVals.SetBool( gval_d_fps, false );
    }

    shaders.UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
    shaders.SendData_Matrices();
    shaders.SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );
    shaders.SetUniform1f("fWeight", 1.0f );
    shaders.SetUniform4f("vColor", Color4f(0,1,0,1) );
    shaders.SetAttrib( "vUV", *vbo_fps_uv );
    shaders.SetAttrib( "vPos", *vbo_fps );
    shaders.DrawArrays( GL_QUADS, 0, vbo_fps->Num() );
}

void Renderer::DrawDebugMode( void ) {
    game->entTree->DrawDebug();

    DrawFramelyLines( vbo_debug_framely_lines, vbo_debug_framely_lines_colors );
    DrawFramelyPoints( vbo_debug_framely_points, vbo_debug_framely_points_sizes, vbo_debug_framely_points_colors );
    DrawFramelyTris( vbo_debug_framely_tris, vbo_debug_framely_tris_colors );

    DrawFramelyLines( vbo_framely_lines, vbo_framely_lines_colors );
    DrawFramelyPoints( vbo_framely_points, vbo_framely_points_sizes, vbo_framely_points_colors );
    DrawFramelyTris( vbo_framely_tris, vbo_framely_tris_colors );
}

void Renderer::ClearFramelyShapes( void ) {
    vbo_framely_lines->Clear();
    vbo_framely_lines_colors->Clear();
    vbo_framely_points->Clear();
    vbo_framely_points_colors->Clear();
    vbo_framely_points_sizes->Clear();
    vbo_framely_tris->Clear();
    vbo_framely_tris_colors->Clear();
}

void Renderer::ClearEditorFramelyShapes( void ) {
    vbo_debug_framely_lines->Clear();
    vbo_debug_framely_lines_colors->Clear();
    vbo_debug_framely_points->Clear();
    vbo_debug_framely_points_colors->Clear();
    vbo_debug_framely_points_sizes->Clear();
    vbo_debug_framely_tris->Clear();
    vbo_debug_framely_tris_colors->Clear();
}

void Renderer::SetupScreenForUI( void ) {
    const float height = static_cast< float >( globalVals.GetInt( gval_v_height ) );
    const float width = static_cast< float >( globalVals.GetInt( gval_v_width ) );

    ModelView().LoadIdentity();

    // ** 0,0 is top left of screen
    const float left= -1.0f / height * width;
    ModelView().Translate( left, 1, -1.0f);
}

void Renderer::ScaleScreenTo800x600( void ) {
    const float height = static_cast< float >( globalVals.GetInt( gval_v_height ) );
    const float width = static_cast< float >( globalVals.GetInt( gval_v_width ) );
    const float aspect = width / height;

    // scale based on 800x600
    float scalex = 2.0f/800.0f;
    float scaley = -2.0f/600.0f;
    float scalez = 2.0f/700.0f;

    // scale to fit aspect screen resolution
    scalex *= aspect;

    ModelView().Scale( scalex, scaley, scalez );
}

inline void Renderer::DrawWorldUI( void ) {
    const bool culling = glIsEnabled( GL_CULL_FACE );
    int culling_mode;
    glGetIntegerv( GL_CULL_FACE_MODE, &culling_mode );
    glEnable( GL_CULL_FACE );
    glCullFace( GL_BACK );
        
        glDisable( GL_DEPTH_TEST );
        
            game->DrawWorldUI();
            
        glEnable( GL_DEPTH_TEST );
        
    glCullFace( static_cast< GLenum >( culling_mode ) );
    if ( !culling )
        glDisable( GL_CULL_FACE );

    CheckGLError( "after drawing world UI" );
}

inline void Renderer::DrawHUDUI( void ) {
    // ** backup culling and enable it
    const bool culling = glIsEnabled( GL_CULL_FACE );
    int culling_mode;
    glGetIntegerv( GL_CULL_FACE_MODE, &culling_mode );
    glEnable( GL_CULL_FACE );
    glCullFace( GL_BACK );

        PushMatrixMV();
        PushMatrixPJ();
            const float ui_fov = 90;
            Projection().Perspective(
                ui_fov,
                globalVals.GetFloat( gval_v_width ),
                globalVals.GetFloat( gval_v_height ),
                globalVals.GetFloat( gval_v_nearPlane ),
                globalVals.GetFloat( gval_v_farPlane )
            );

            // For UI, screen is treated as 800x600 units regardless of resolution with y axis is flipped

            SetupScreenForUI();
            ScaleScreenTo800x600();

            game->DrawHUDUI();

            glDisable( GL_DEPTH_TEST );

                for ( int i=0; i<SPRITE_LAYERS; ++i )
                    DrawSpriteList( game->sprites[i].list );

                if ( console->Object::IsVisible() )
                    console->DrawVBO();

                if ( globalVals.GetBool( gval_d_fps ) )
                    DrawFPS();

            glEnable( GL_DEPTH_TEST );


        PopMatrixPJ();
        PopMatrixMV();

    glCullFace( static_cast< GLenum >( culling_mode ) );
    if ( !culling )
        glDisable( GL_CULL_FACE );

    if ( globalVals.GetBool( gval_d_compass ) )
        DrawCompass();

    CheckGLError( "after drawing game UI" );
}

template< typename TYPE >
void Renderer::DrawSpriteList( const TYPE& splist ) {
    // we defer rendering of windows until everyhting else is drawn, so window elements don't get drawn underneath things behind them.
    std::vector< Sprite* > deferred;
    std::vector< Vec3f > deferred_tran;

    deferred.reserve( splist.size() );
    deferred_tran.reserve( splist.size() + 1 );

    for ( Sprite* sprite : splist ) {

        if ( !sprite || !sprite->IsVisible() )
            continue;

        if ( sprite->children ) {
            deferred.push_back( sprite );
            deferred_tran.push_back( ModelView().GetTranslation() );
            continue;
        }

        Vec3f origin = sprite->GetOrigin();

        ModelView().Translate( origin.x, origin.y, 0.0f );

        if ( !game->IsPaused() )
            sprite->Think();
        sprite->DrawVBO();

        ModelView().Translate( -origin.x, -origin.y, 0.0f );

    }

    // now draw all deferred elements
    const auto tran = ModelView().GetTranslation();

        for ( uint i=0; i<deferred.size(); ++i ) {
            ModelView().SetTranslation( deferred_tran[i] );
            Vec3f origin = deferred[i]->GetOrigin();
            ModelView().Translate( origin.x, origin.y, 0.0f );
            deferred[i]->DrawVBO();
        }

    ModelView().SetTranslation( tran );
}

void Renderer::DrawBoxOutLine( const Box3D& box, const Color4f& color ) {

    // ** near face
    DrawLine( box.VertAt(BOX_NEAR_LEFT_TOP_INDEX),     box.VertAt(BOX_NEAR_RIGHT_TOP_INDEX), color, color );
    DrawLine( box.VertAt(BOX_NEAR_RIGHT_TOP_INDEX),    box.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX), color, color );
    DrawLine( box.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX), box.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX), color, color );
    DrawLine( box.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX),  box.VertAt(BOX_NEAR_LEFT_TOP_INDEX), color, color );

    // ** far face
    DrawLine( box.VertAt(BOX_FAR_LEFT_TOP_INDEX),     box.VertAt(BOX_FAR_RIGHT_TOP_INDEX), color, color );
    DrawLine( box.VertAt(BOX_FAR_RIGHT_TOP_INDEX),    box.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX), color, color );
    DrawLine( box.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX), box.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX), color, color );
    DrawLine( box.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX),  box.VertAt(BOX_FAR_LEFT_TOP_INDEX), color, color );

    // ** lines connecting far and near faces
    DrawLine( box.VertAt(BOX_FAR_LEFT_TOP_INDEX),     box.VertAt(BOX_NEAR_LEFT_TOP_INDEX), color, color );
    DrawLine( box.VertAt(BOX_FAR_RIGHT_TOP_INDEX),    box.VertAt(BOX_NEAR_RIGHT_TOP_INDEX), color, color );
    DrawLine( box.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX),  box.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX), color, color );
    DrawLine( box.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX), box.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX), color, color );
}

Vec3f Renderer::GetResolution( void ) const {
    return sdl.GetResolution();
}

void Renderer::SendShaderData_Lights( void ) {

    int numDirLights = 0;
    for( uint i=0; i < dirLights.size(); i++ ) {
        std::shared_ptr< DirLight > dl = dirLights[i].light.lock();
        if ( !dl )
            continue;
        
        numDirLights += 1;
        std::string lightIndex = std::string( "dirLights[" ) + std::to_string(i) + std::string( "]" );
        
        shaders.SetUniform3f( std::string( lightIndex + std::string(".direction") ).c_str() , dl->GetDirection() * ViewMatrix() );
        shaders.SetUniform3f( std::string( lightIndex + std::string(".direction") ).c_str() , dl->GetDirection() );
        shaders.SetUniform3f( std::string( lightIndex + std::string(".diffuse") ).c_str() , dl->GetColor() ); //target
        shaders.SetUniform3f( std::string( lightIndex + std::string(".ambient") ).c_str() , dl->GetAmbientStrength() );
    }
    
    shaders.SetUniform1i( "numDirLights", numDirLights );
    
    int numPointLights = 0;
    for( uint i=0; i < pointLights.size(); i++ ) {
        std::shared_ptr< PointLight > pl = pointLights[i].light.lock();
        if ( !pl )
            continue;
        
        numPointLights += 1;
        std::string lightIndex = std::string( "pointLights[" ) + std::to_string(i) + std::string( "]" );
    
        shaders.SetUniform3f( std::string( lightIndex + std::string(".position") ).c_str() , pl->GetOrigin() * ViewMatrix() );
        shaders.SetUniform3f( std::string( lightIndex + std::string(".diffuse") ).c_str() , pl->GetColor() );
        shaders.SetUniform3f( std::string( lightIndex + std::string(".ambient") ).c_str() , pl->GetAmbientStrength() );
        shaders.SetUniform1f( std::string( lightIndex + std::string(".constant") ).c_str() , pl->Attenuation().constant );
        shaders.SetUniform1f( std::string( lightIndex + std::string(".quadratic") ).c_str() , pl->Attenuation().quadratic );
        shaders.SetUniform1f( std::string( lightIndex + std::string(".linear") ).c_str() , pl->Attenuation().linear );
        
        // The following is used in an alternative attenuation calculation method. see "animTexMultiLight.fs"
        //shaders.SetUniform1f( std::string( lightIndex + std::string(".radius") ).c_str() , pl->GetRadius() ); 
    }

    shaders.SetUniform1i( "numPointLights", numPointLights );
}

Vec3f Renderer::GetCrosshairOrigin( void ) const {
    return crosshair_origin;
}

void Renderer::DeactivateEffect( const EffectT _effect ) {
    effects &= ~_effect;
}

const GLint* Renderer::GetViewPort( void ) const {
    return viewport;
}

#ifdef MONTICELLO_EDITOR
    void Renderer::DrawLine( const Line3f& line, const Color4f& color_start, const Color4f& color_end ) {
        DrawLine( line.vert[0], line.vert[1], color_start, color_end );
    }

    void Renderer::DrawLine_Debug( const Line3f& line, const Color4f& color_start, const Color4f& color_end ) {
        DrawLine_Debug( line.vert[0], line.vert[1], color_start, color_end );
    }
#endif // MONTICELLO_EDITOR

Mat4f& Renderer::ModelView( void ) {
    const std::size_t s = modelView.size();
    ASSERT(s >= 1);
    return modelView[s-1];
}

Mat4f& Renderer::Projection( void ) {
    const std::size_t s = projection.size();
    ASSERT(s >= 1);
    return projection[s-1];
}

Mat4f& Renderer::ViewMatrix( void ) {
    return viewMatrix;
}

Mat4f& Renderer::InverseViewMatrix( void ) {
    return inverseViewMatrix;
}
