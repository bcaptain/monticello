// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./shaders.h"
#include "../game.h"
#include "../rendering/renderer.h"

const uint NO_SHADER_PROGRAM=0;

const bool DEBUG_GL_PERFORMANCE = false;

std::vector< uint > Shaders::boundAttributes;

Shaders::Shaders( void )
    : manifest()
    , vertexShaders()
    , fragmentShaders()
    , glProgs()
    , cur_prog_name()
    , currentProg(NO_SHADER_PROGRAM)
    {
}

Shaders::~Shaders( void ) {
    FreeAll();
}

void Shaders::Register( const std::string& path, const char* container ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();

    const std::string name = String::GetFileFromPath( path );
    // we keep the extention for shaders, as a vertex shader may be named the same as a fragment shader, for example

    if ( manifest.Set( name.c_str(), AssetManifest( name, path, container ) ) )
        OverrideWarning("Shader Manifest", name, path, container );
}

void Shaders::LoadAllShaders( void ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();

    for ( const auto& data : manifest ) {
        // we don't set to false, since the previously loaded asset will remain loaded if this load fails
        if ( ! String::RightIs( data.second.GetPath(), ".fs" ) )
            if ( ! String::RightIs( data.second.GetPath(), ".vs" ) )
                continue;
        Load( data.second );
    }
}

void Shaders::LoadAllProgs( void ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();

    for ( const auto& data : manifest ) {
        // we don't set to false, since the previously loaded asset will remain loaded if this load fails
        if ( String::RightIs( data.second.GetPath(), ".glprog" ) )
            LoadGLProg( data.second );
    }
}

bool Shaders::Load( const AssetManifest& data ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    File file;
    return OpenAssetFile( "Shader", data, file ) && Load( file, String::RightIs( data.GetPath(), ".fs" ) );
}

void Shaders::PrintShaderLog( const GLuint& obj, const char* path, const std::string& shader_source ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    int len=0;

    glGetShaderiv( obj, GL_COMPILE_STATUS, &len);
    if ( len != GL_TRUE )
        ERR("Shader %s: could not compile\n", path);

    len=0;
    glGetShaderiv( obj, GL_INFO_LOG_LENGTH, &len );

    if ( len < 1 )
        return;

    int size=0;
    char* log = new char[len+1]();
    glGetShaderInfoLog( obj, len, &size, log );

    if ( len > 1 ) {
        ERR("Shader %s has produced a log of length\n %i, outputting in log.\n", path, len);
        LOG( "BEGIN SHADER LOG\n\n" );
        LOG("%.*s", len, log );
        LOG( "\nEND SHADER LOG\n\n" );
        
        ERR("Shader %s source outputting in log.\n", path );
        LOG( "BEGIN SHADER SOURCE\n\n" );
        std::string source_line;
        std::size_t line_number=0;
        for ( uint i=0; i<shader_source.size(); ++i ) {
            if ( shader_source[i] == '\n' || i == shader_source.size()-1 ) {
                LOG( "%u: %.*s\n", line_number, source_line.size(), source_line.c_str() );
                source_line.clear();
                ++line_number;
            } else {
                source_line += shader_source[i];
            }
        }
        LOG( "\nEND SHADER SOURCE\n\n" );
        
        DIE("Exiting.");
    }

    DELNULLARRAY( log );
}

void Shaders::PrintProgramLog( const GLuint& obj, const char* path ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    int len=0;

    glGetProgramiv( obj, GL_LINK_STATUS, &len);
    if ( len != GL_TRUE )
        ERR("Shaders: could not link program %s\n", path);

    len=0;
    glGetProgramiv( obj, GL_INFO_LOG_LENGTH, &len );

    if ( len < 1 )
        return;

    int size=0;
    char* log = new char[len+1]();
    glGetProgramInfoLog( obj, len, &size, log );

    if ( len > 1 ) {
        ERR("Shaders: prog %s has produced a log of length\n %i, outputting in log.\n", path, len);
        LOG( "BEGIN LOG\n\n" );
        LOG("%.*s", len, log );
        LOG( "\nEND LOG\n\n" );
        DIE("Exiting.");
    }

    DELNULLARRAY( log );
}

void Shaders::UseProg( const Hash& hashkey ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();

    const char* name = hashkey.first;

    // clear out all attributes that were set by the previous program
    for ( uint i=0; i< boundAttributes.size(); ++i )
        glDisableVertexAttribArray( boundAttributes[i] );
    boundAttributes.clear();

    if ( name == nullptr || name[0] == '\0' ) {
        DIE("Shaders: cannot use prog, empty parameter name\n");
    }

    const uint* p = glProgs.Get( hashkey );

    if ( !p ) {
        currentProg = NO_SHADER_PROGRAM;
        DIE("Shaders: no prog loaded by this name: %s\n", name);
    }

    currentProg = *p;
    glUseProgram( currentProg );
    cur_prog_name = name;
}

bool Shaders::LoadGLProg( const AssetManifest& data ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    File file;
    return OpenAssetFile( "GLProg", data, file ) && LoadGLProg( file );
}

bool Shaders::LoadGLProg( File& opened_glprog_file ) {
    CMSG_LOG("Loading GLProg %s from %s\n"
        , opened_glprog_file.GetFilePath().c_str()
        , opened_glprog_file.GetFileContainerName_Full().c_str()
    );
    
    if ( DEBUG_GL_PERFORMANCE ) glFinish();

    // ** make sure it's open

    if ( ! opened_glprog_file.IsOpen() ) {
        ERR("Shaders: Couldn't open prog %s\n", opened_glprog_file.GetFilePath().c_str() );
        return false;
    }

    // ** get the name from the filename

    const std::string name = String::TrimExtentionFromFileName( String::GetFileFromPath( opened_glprog_file.GetFilePath() ) );

    if ( name.size() == 0 ) {
        ERR("Shaders: Couldn't determine prog name: %s.\n", opened_glprog_file.GetFilePath().c_str() );
        return false;
    }

    // ** load it

    std::string fs,vs,tmp,ext;
    ParserText parser( opened_glprog_file );

    while ( parser.ReadWord(tmp) ) {
        String::Trim( tmp );
        ext = String::Right(tmp, 3 );
        if (ext == ".vs" ) {
            vs = tmp;
        } else if (ext == ".fs" ) {
            fs = tmp;
        } else {
            ERR("Shaders: Unknown shader type (%s) in glprog %s\n", tmp.c_str(), opened_glprog_file.GetFilePath().c_str() );
            return false;
        }
    }

    vs = String::TrimExtentionFromFileName( vs );
    fs = String::TrimExtentionFromFileName( fs );

    if ( fs.empty() ) {
        ERR("Shaders: No Fragment Shader specified in gl prog file %s\n", opened_glprog_file.GetFilePath().c_str() );
        return false;
    }

    if ( vs.empty() ) {
        ERR("Shaders: No Vertex Shader specified in gl prog file %s\n", opened_glprog_file.GetFilePath().c_str() );
        return false;
    }

    return MakeProg( name.c_str(), fs.c_str(), vs.c_str(), opened_glprog_file.GetFilePath().c_str() ); // warns about Override
}

bool Shaders::MakeProg( const char* name, const char* fsname, const char* vsname, const char* path ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    if ( glProgs.Get( name ) ) {
        WARN("Override GLP: %s with %s\n", name, path);
    }

    const uint* vsid = vertexShaders.Get( vsname );
    if ( !vsid ) {
        ERR("Shaders: Vertex shader %s not found  (file %s)\n", vsname, path);
        return false;
    }

    const uint* fsid = fragmentShaders.Get( fsname );
    if ( !fsid ) {
        ERR("Shaders: Fragment shader %s not found (file %s)\n", fsname, path);
        return false;
    }

    GLuint prog = glCreateProgram();
    glAttachShader( prog, *vsid );
    glAttachShader( prog, *fsid );
    glLinkProgram( prog );
    PrintProgramLog( prog, path );
    glProgs.Set( name, prog );

    return true;
}

void Shaders::ReadShaderFile( File& opened_shader_file, std::string& out ) const {
    out.reserve( out.size() + static_cast< uint >( opened_shader_file.GetSize() ) );
    
    ParserText parser( opened_shader_file );
    
    std::string word;
    do {
        while ( parser.ReadWordOnLine( word ) ) {
            
            if ( word == "#include" ) {
                std::string include_name;
                parser.ReadWordOnLine( include_name );
                const AssetManifest* asset_info = manifest.Get( include_name.c_str() );
                File include_file;
                
                if ( asset_info && OpenAssetFile( "GLSL Include", *asset_info, include_file ) ) {
                    ReadShaderFile( include_file, out );
                    continue;
                }
                
                DIE("ReadShaderFile: %s from %s: Couldn't open #include file: %s\n"
                    , opened_shader_file.GetFilePath().c_str()
                    , opened_shader_file.GetFileContainerName_Full().c_str()
                    , include_name.c_str() );
            }
            
            // add spaces only before each word, and never after whitespaced or newlines
            if ( out.size() > 0 && !ParserText::IsWhiteSpaceOrNewline( out[out.size()-1] ) )
                out += " ";
                
            out += word;
        }
        
        // don't add consecutive newlines
        if ( out.size() > 0 && !ParserText::IsNewline( out[out.size()-1] ) )
            out += '\n';
            
    } while ( parser.SkipToNextLine() );
}

bool Shaders::Load( File& opened_shader_file, bool fragmentShader ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    CMSG_LOG("Loading shader %s from %s\n"
        , opened_shader_file.GetFilePath().c_str()
        , opened_shader_file.GetFileContainerName_Full().c_str()
    );
    
    // ** Make sure the file is open

    if ( !opened_shader_file.IsOpen() || !opened_shader_file.ReadToBuffer() ) {
        ERR("Shaders: Could not open shader file: %s\n", opened_shader_file.GetFilePath().c_str() );
        return false;
    }

    // ** Make sure we're at the beginning of the file

    opened_shader_file.SeekSet( 0 );

    // ** The shader's name is the filename without the path or extension

    const std::string shader_name = String::TrimExtentionFromFileName( String::GetFileFromPath( opened_shader_file.GetFilePath().c_str() ) );

    // ** If there is already a shader with this name, warn that we are going to override it

    const bool asset_existed = fragmentShader
        ? ( fragmentShaders.Get( shader_name.c_str() ) != nullptr )
        : ( vertexShaders.Get( shader_name.c_str() ) != nullptr );

    // ** Load the shader file
    
    std::string shader_source;
    ReadShaderFile( opened_shader_file, shader_source );
    
    if ( shader_source.size() >= MAX_INT32 )
        DIE("Shaders::Load: Shader file too big: %s from %s\n"
        , opened_shader_file.GetFilePath().c_str()
        , opened_shader_file.GetFileContainerName_Full().c_str()
    );

    const GLuint sid = glCreateShader( fragmentShader ? GL_FRAGMENT_SHADER : GL_VERTEX_SHADER);
    const char* ptr = shader_source.data();
    
    const int size = static_cast< int >( shader_source.size() );
    glShaderSource( sid, 1, &ptr, &size );
    glCompileShader( sid );
    PrintShaderLog( sid, opened_shader_file.GetFilePath().c_str(), shader_source );

    if ( asset_existed ) {
        OverrideWarning("Shader", shader_name, opened_shader_file.GetFilePath(), opened_shader_file.GetFileContainerName().c_str() );
    }

    if ( fragmentShader ) {
        fragmentShaders.Set(shader_name.c_str(), sid );
    } else {
        vertexShaders.Set(shader_name.c_str(), sid );
    }

    return true; // the snader may not have been compiled properly, but it was found so we return true
}

void Shaders::SendData_Matrices( void ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    SendData_ModelViewMat( renderer->ModelView().data );
    SendData_ProjectionMat( renderer->Projection().data );
}

int Shaders::GetUniformLoc( const char* loc_name ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    ASSERT( currentProg );

    const int loc = glGetUniformLocation( currentProg, loc_name );

    if ( loc < 0 )
        DIE("Shaders: Uniform %s does not exist in shader %s\n", loc_name, cur_prog_name.c_str() );

    return loc;
}

void Shaders::SendData_ModelViewMat( float mv[16] ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc("M");
    glUniformMatrix4fv(loc ,1, GL_FALSE, mv );
}

void Shaders::SendData_ProjectionMat( float pj[16] ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc( "P");
    glUniformMatrix4fv( loc, 1, GL_FALSE, pj );
}

void Shaders::SetTexture( const uint texture_id, const uint texture_unit, const char* uniform_var ) const {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    if ( texture_unit > GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS )
        DIE("Shaders: Attempted to use texture unit %u, but there are only %i.\n", texture_unit, GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS );

    const int loc = GetUniformLoc( uniform_var );
    glActiveTexture( GL_TEXTURE0 + texture_unit ); // this the correct way to call according to https://stackoverflow.com/questions/8866904/need-help-understanding-the-differences-and-relationship-between-glactivetexture
    glUniform1i( loc, static_cast< GLint >( texture_unit ) );
    glBindTexture( GL_TEXTURE_2D, texture_id );

    renderer->CheckGLError( "sending texture data to shader" );
}

void Shaders::FreeAll( void ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    FreeList( glProgs );
    FreeList( vertexShaders );
    FreeList( fragmentShaders );
}

void Shaders::FreeList( HashList< uint >& mainlist ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    for ( auto& pair : mainlist ) {
        uint& id = pair.second;

        if ( glIsShader( id ) ) {
            // glDetachShader( id ); // we don't need to detach because all programs detach when they are deleted
            glDeleteShader( id );
        } else if ( glIsProgram( id ) ) {
            glDeleteProgram( id );
        }
    }

    // ** then we can clear the list
    mainlist.Clear();
}

void Shaders::SetUniform1i( const char* loc_name, const int a ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform1i( loc, a );
}

void Shaders::SetUniform1f( const char* loc_name, const float a ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform1f( loc, a );
}

void Shaders::SetUniform2f( const char* loc_name, const float a, const float b ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform2f( loc, a,b );
}

void Shaders::SetUniform3f( const char* loc_name, const float a, const float b, const float c ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform3f( loc, a,b,c );
}

void Shaders::SetUniform3f( const char* loc_name, const Vec3f& vec ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform3f( loc, vec.x,vec.y,vec.z );
}

void Shaders::SetUniform4f( const char* loc_name, const float a, const float b, const float c, const float d ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform4f( loc, a,b,c,d );
}

void Shaders::SetUniform2x4f( const char* loc_name, const std::size_t num, const GLenum whether, const float *array ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    ASSERT( array );
    const int loc = GetUniformLoc(loc_name);
    glUniformMatrix2x4fv( loc, static_cast< int >( num ), whether, array );
}

void Shaders::SetUniform4f( const char* loc_name, const Color4f& color ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    const int loc = GetUniformLoc(loc_name);
    glUniform4f( loc, color.r,color.g,color.b,color.a );
}

void Shaders::SetUniform2uiv( const char* loc_name, const std::size_t num,  const uint *array ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    ASSERT( array );
    const int loc = GetUniformLoc(loc_name);
    glUniform2uiv(loc, static_cast<GLsizei>(num), array);
}

void Shaders::SetUniform1iv( const char* loc_name, const std::size_t num,  const int *array ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    ASSERT( array );
    const int loc = GetUniformLoc(loc_name);
    glUniform1iv(loc, static_cast<GLsizei>(num), array);
}

void Shaders::SetUniformMatrix4fv( const char* loc_name, const std::size_t num, const GLenum whether, const float *array ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    ASSERT( array );
    const int loc = GetUniformLoc(loc_name);
    glUniformMatrix4fv(loc, static_cast<GLsizei>(num), whether, array);
}

void Shaders::SetUniformMatrix3fv( const char* loc_name, const std::size_t num, const GLenum whether, const float *array ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    ASSERT( array );
    const int loc = GetUniformLoc(loc_name);
    glUniformMatrix3fv(loc, static_cast<GLsizei>(num), whether, array);
}

void Shaders::DrawArrays( GLenum mode, GLint first, uint count ) {
    if ( DEBUG_GL_PERFORMANCE ) glFinish();
    glDrawArrays( mode, first, static_cast< GLsizei >( count ) );
    renderer->CheckGLError("calling Shaders::DrawArrays()");
}

uint Shaders::GetCurrentProg( void ) const {
    return currentProg;
}

bool Shaders::LoadVertexShader( File& opened_shader_file ) {
    return Load( opened_shader_file, false );
}

bool Shaders::LoadFragmentShader( File& opened_shader_file ) {
    return Load( opened_shader_file, true );
}

uint Shaders::GetAttribLoc( const char* loc_name ) const {
    ASSERT( currentProg );
    int attrib = glGetAttribLocation( currentProg, loc_name );
    if ( attrib < 0 )
        DIE("Attribute %s does not exist in shader %s\n", loc_name, cur_prog_name.c_str() );

    return static_cast< uint>( attrib );
}
