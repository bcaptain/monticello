// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./animCue.h"

AnimCue::AnimCue( void )
    : event( AnimEventT::NONE )
    , frame( 0 )
    , arguments()
{ }

void AnimCue::Unset( void ) {
    event = AnimEventT::NONE;
    frame = 0;
    arguments.clear();
}

bool AnimCue::SetType( const std::string &typeName, const std::vector< std::pair< uint, std::string > >* eventList ) {
    if ( eventList ) {
        for ( auto const & entry : *eventList ) {
            if ( entry.second == typeName ) {
                event = static_cast< AnimEventT >( entry.first );
                return true;
            }
        }
    }
    
    if ( typeName == "launchProjectile" ) {
        event = AnimEventT::LAUNCH_PROJECTILE;
    } else if ( typeName == "doAttack1" ) {
        event = AnimEventT::GENERIC_ATTACK_1;
    } else if ( typeName == "addAnimState" ) {
        event = AnimEventT::ADD_ANIMSTATE;
    } else if ( typeName == "remAnimState" ) {
        event = AnimEventT::REM_ANIMSTATE;
    } else if ( typeName == "fireBullet" ) {
        event = AnimEventT::FIRE_BULLET;
    } else if ( typeName == "playSound" ) {
        event = AnimEventT::PLAY_SOUND;
    } else if ( typeName == "spawnEffect" ) {
        event = AnimEventT::SPAWN_EFFECT;
    } else if ( typeName == "chargeForward" ) {
        event = AnimEventT::CHARGE_FORWARD;
    } else if ( typeName == "disableTurn" ) {
        event = AnimEventT::DISABLE_TURNING;
    } else if ( typeName == "enableTurn" ) {
        event = AnimEventT::ENABLE_TURNING;
    } else if ( typeName == "disableMove" ) {
        event = AnimEventT::DISABLE_MOVING;
    } else if ( typeName == "enableMove" ) {
        event = AnimEventT::ENABLE_MOVING;
    } else if ( typeName == "disableTouchDamage" ) {
        event = AnimEventT::DISABLE_TOUCH_DAMAGE;
    } else if ( typeName == "enableTouchDamage" ) {
        event = AnimEventT::ENABLE_TOUCH_DAMAGE;
    } else if ( typeName == "setMoveSpeedBoost" ) {
        event = AnimEventT::MOVE_SPEED_BOOST;
    } else if ( typeName == "doSpecial1" ) {
        event = AnimEventT::SPECIAL_1;
    } else if ( typeName == "doSpecial2" ) {
        event = AnimEventT::SPECIAL_2;
    } else if ( typeName == "teleDash" ) {
        event = AnimEventT::TELEDASH;
    } else if ( typeName == "renderEffect" ) {
        event = AnimEventT::RENDER_EFFECT;
    } else if ( typeName == "enableTurnAnimOverride" ) {
        event = AnimEventT::ENABLE_TURN_ANIMATION_OVERRIDE;
    } else if ( typeName == "die" ) {
        event = AnimEventT::DIE;
    } else if ( typeName == "interact" ) {
        event = AnimEventT::INTERACT;
    } else if ( typeName == "setOpacity" ) {
        event = AnimEventT::SET_OPACITY;
    } else if ( typeName == "spawnEntity" ) {
        event = AnimEventT::SPAWN_ENTITY;
    } else if ( typeName == "disableAttachmentVisibility" ) {
        event = AnimEventT::DISABLE_ATTACHMENT_VISIBILITY;
    } else if ( typeName == "stun" ) {
        event = AnimEventT::STUN;
    }  else if ( typeName == "wakeUp" ) {
        event = AnimEventT::WAKE_UP;
    } else {
        event = AnimEventT::NONE;
        return false;
    }

    return true;
}

bool AnimCue::operator==( const AnimCue& other ) const {
    return other.frame == frame;
}

bool AnimCue::operator>( const AnimCue& other ) const {
    return frame > other.frame;
}

bool AnimCue::operator<( const AnimCue& other ) const {
    return frame < other.frame;
}

bool AnimCue::operator>=( const AnimCue& other ) const {
    return frame >= other.frame;
}

bool AnimCue::operator<=( const AnimCue& other ) const {
    return frame <= other.frame;
}
