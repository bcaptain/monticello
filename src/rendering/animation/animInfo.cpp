// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "animInfo.h"
#include "armatureInfo.h"
#include "animation.h"
#include "../renderer.h"
#include "../../game.h"

extern const uint ANIM_FRAME_DURATION_MS;
constexpr const bool animInfoDebug = false;
extern const uint ANIM_PRIORITY_NOT_ANIMATING;

AnimChannelSelection::AnimChannelSelection( const AnimChannelT _channel, const uint _priority )
    : channel( _channel )
    , priority( _priority )
{ }

AnimChannelSelection::AnimChannelSelection( void )
    : channel( AnimChannelT::Channel_0 )
    , priority( 0 )
{ }

AnimSelection::AnimSelection( const char* _anim_name, const AnimStateT _anim_state, const AnimOptT _opts, const std::vector<AnimChannelSelection>& _channel_selections )
    : anim_name( _anim_name )
    , channel_selections( _channel_selections )
    , animState( _anim_state )
    , opts( _opts )
{ }

AnimSelection::AnimSelection( void )
    : anim_name()
    , channel_selections()
    , animState( AnimStateT::None )
    , opts( AnimOptT::BlendInto )
{ }

AnimChannelStates::AnimChannelStates( void )
    : animName{{}}
    , priority{{}} // initializes all to priority = 0
    , animState{{}} // initializes all to AnimStateT::None = 0
    , animating{{}}  // all false
{ }

AnimChannelStates::AnimChannelStates( const AnimChannelStates& other )
    : AnimChannelStates()
{
    operator=(other);
}

AnimChannelStates::AnimChannelStates( AnimChannelStates&& other_rref )
    : AnimChannelStates()
{
    operator=( std::move( other_rref ) );
}

AnimChannelStates& AnimChannelStates::operator=( const AnimChannelStates& other ) {
    animName = other.animName;
    priority = other.priority;
    animState = other.animState;
    animating = other.animating;

    return *this;
}

AnimChannelStates& AnimChannelStates::operator=( AnimChannelStates&& other_rref ) {
    animName = std::move(other_rref.animName);
    priority = other_rref.priority;
    animState = other_rref.animState;
    animating = other_rref.animating;

    return *this;
}

AnimInfo::AnimInfo( ModelInfo& _owner )
    : animChannelStates()
    , boneArray(0)
    , armatureInfos()
    , owner( &_owner )
    , masterChannel( AnimChannelT::Legs )
    , bonePositionsComputed( false )
{ }

AnimInfo::AnimInfo( const AnimInfo& other )
    : animChannelStates( other.animChannelStates )
    , boneArray( other.boneArray )
    , armatureInfos( other.armatureInfos )
    , owner( other.owner )
    , masterChannel( other.masterChannel )
    , bonePositionsComputed( other.bonePositionsComputed )
{
    UpdateOwners();
}

AnimInfo::AnimInfo( AnimInfo&& other_rref )
    : animChannelStates( other_rref.animChannelStates )
    , boneArray( std::move( other_rref.boneArray ) )
    , armatureInfos( std::move( other_rref.armatureInfos ) )
    , owner( other_rref.owner )
    , masterChannel( other_rref.masterChannel )
    , bonePositionsComputed( other_rref.bonePositionsComputed )
{
    UpdateOwners();
}

AnimInfo& AnimInfo::operator=( const AnimInfo& other) {
    animChannelStates = other.animChannelStates;
    boneArray = other.boneArray;
    armatureInfos = other.armatureInfos;
    owner = other.owner;
    masterChannel = other.masterChannel;
    bonePositionsComputed = other.bonePositionsComputed;

    UpdateOwners();

    return *this;
}

AnimInfo& AnimInfo::operator=( AnimInfo&& other_rref ) {
    animChannelStates = std::move( other_rref.animChannelStates );
    boneArray = std::move( other_rref.boneArray );
    armatureInfos = std::move( other_rref.armatureInfos );
    owner = other_rref.owner;
    masterChannel = other_rref.masterChannel;
    bonePositionsComputed = other_rref.bonePositionsComputed;

    UpdateOwners();

    return *this;
}

void AnimInfo::UpdateOwners( void ) {
    for ( auto& arm : armatureInfos )
        arm.SetOwner( *this );
}

void AnimInfo::ModelChanged( void ) {
    const std::shared_ptr< const Model > model = owner->GetModel();
    if ( ! model )
        return;

    if ( !model->IsAnimated() ) {
        armatureInfos.resize(0);
        boneArray.resize(0);
        return;
    }

    const AnimatedModel* anim_model = static_cast< const AnimatedModel* >( model.get() );
    if ( !anim_model ) {
        armatureInfos.resize(0);
        return;
    }

    if ( anim_model->NumChannels() == 0 ) {
        DIE("AnimatedModel has no channels: %s\n", model->GetName().c_str() );
        return;
    }

    armatureInfos.resize( anim_model->NumChannels() );
    for ( uint i=0; i<armatureInfos.size(); ++i ) {
        armatureInfos[i].SetOwner( *this );
        armatureInfos[i].SetChannel( anim_model->GetChannelID(i) );
    }

    AllocateBoneArray();
}

const ArmatureInfo* AnimInfo::GetArmatureInfo( const AnimChannelT channel ) const {
    ASSERT( static_cast< AnimChannelT_BaseType >( channel ) < MAX_ANIM_CHANNELS );
    ASSERT( owner );
    const auto & mdl = owner->GetModel();
    const AnimatedModel* anim_model = static_cast< const AnimatedModel* >( mdl.get() );
    const uint index = anim_model->GetChannelIndex( channel );
    return &armatureInfos[index];
}

ArmatureInfo* AnimInfo::GetArmatureInfo( const AnimChannelT channel ) {
    return const_cast< ArmatureInfo* >( const_cast< const AnimInfo* >( this )->GetArmatureInfo( channel ) );
}

void AnimInfo::AllocateBoneArray( void ) {
    ASSERT( owner );
    const auto & mdl = owner->GetModel();
    if ( !mdl || !mdl->IsAnimated() )
        return;

    const AnimatedModel* anim_model = static_cast< const AnimatedModel* >( mdl.get() );
    const uint boneCount = anim_model->GetNumBones();
    boneArray.resize(boneCount * NUM_FLOATS_PER_BONE); // if the array is already this size, it's okay
}

bool AnimInfo::FindIDByName( const std::string& boneName, Uint8& boneID_out) const {
    for ( auto& armInfo : armatureInfos )
        if ( armInfo.FindIDByName(boneName, boneID_out) )
            return true;

    ERR("AnimInfo::FindIDByName. BoneName %s not found in Model %s\n", boneName.c_str(), owner->GetModel()->GetName().c_str());

    return false;
}

const ArmatureInfo* AnimInfo::GetMasterChannelArmatureInfo( void ) const {
    const ArmatureInfo* masterChannelArmInfo = nullptr;
    ASSERT( owner );
    masterChannelArmInfo = owner->GetArmatureInfo( masterChannel );
    return masterChannelArmInfo;
}

bool AnimInfo::ComputeBonePos( void ) {
    bonePositionsComputed = false;

    const ArmatureInfo* masterChannelArmInfo = GetMasterChannelArmatureInfo();
    const Armature* masterChannelArm = nullptr;
    Uint16 masterChannelFrame = 0;

    if ( masterChannelArmInfo ) {
        masterChannelArm = masterChannelArmInfo->GetCurAnim();
        masterChannelFrame = masterChannelArmInfo->GetCurFrame();
    }

    for ( uint i=0; i<armatureInfos.size(); ++i ) {

        if ( !armatureInfos[i].GetCurAnim() )
            continue; // not currently animating

        auto const offsetArm = masterChannelArmInfo != &armatureInfos[i] ? &masterChannelArm->GetRootBone_Ref() : nullptr;
        if ( !armatureInfos[i].ComputeBonePos( offsetArm, masterChannelFrame ) ) {
                ERR("Couldn't not build bone uniforms for animation %s on model %s.\n", offsetArm->GetName().c_str(), GetModel()->GetName().c_str());
            return false;
        }
    }
    bonePositionsComputed = true;
    return true;
}

void PackQuat_XYZW( std::vector<float>& list, const Quat &quat, std::size_t start_index );
void PackQuat_XYZW( std::vector<float>& list, const Quat &quat, std::size_t start_index ) {
    ASSERT( list.size() >= start_index + 4 );

    list[start_index] = quat.x;
    list[++start_index] = quat.y;
    list[++start_index] = quat.z;
    list[++start_index] = quat.w;
}

// packs Real, then Dual (*_RD)
void PackDualQuat_XYZW_RD( std::vector<float>& list, const Dualquat &dualquat, const std::size_t start_index );
void PackDualQuat_XYZW_RD( std::vector<float>& list, const Dualquat &dualquat, const std::size_t start_index ) {
    ASSERT( list.size() >= start_index + NUM_FLOATS_PER_BONE );

    PackQuat_XYZW( list, dualquat.rot, start_index );
    PackQuat_XYZW( list, dualquat.tran, start_index+4 );
}

void AnimInfo::PackBoneArray( const Dualquat& arm_bone_dq, const std::size_t start_index ) {
    PackDualQuat_XYZW_RD( boneArray, arm_bone_dq, start_index );
}

bool AnimInfo::SendShaderInfo( void ) const {
    if ( armatureInfos.size() < 1 )
        return false;

    const Armature* curAnim = armatureInfos[0].GetCurAnim();
    if ( !curAnim )
        return false;

    const bool animIsFinished = armatureInfos[0].GetCurKeyFrame() == curAnim->NumKeyFrames();
    if ( curAnim == nullptr || animIsFinished )
        return false;

    if ( !BonePosComputed() ) {
        const auto model = GetModel();
        std::string modelName;
        if ( model )
            modelName = model->GetName();
        ERR("Cannot animate armature %s, it's bone uniform array is not built.\n", modelName.c_str() );
        return false;
    }

    ASSERT( NUM_FLOATS_PER_BONE > 0 );
    const std::size_t num_matrices = boneArray.size() / NUM_FLOATS_PER_BONE;
    shaders->SetUniform2x4f("Bone_DQ", num_matrices, GL_FALSE, boneArray.data() );
    return true;
}

void AnimInfo::DrawSkel( void ) const {
    for ( uint i=0; i<armatureInfos.size(); ++i)
        armatureInfos[i].DrawSkel( true );
}

#ifdef MONTICELLO_DEBUG
void AnimInfo::TestModel_Animate( void ) {
    if ( armatureInfos.size() < 1 )
        return;
    TestModel_Animate( armatureInfos[0].GetCurAnimName().c_str() );
}

void AnimInfo::TestModel_Animate( const char* anim_name ) {
    if ( !anim_name || anim_name[0] == '\0') {
        globalVals.SetBool(gval_d_testAnim, false);
        return;
    }

    const float animSpeed = globalVals.GetFloat(gval_d_animSpeed, 1.0f);

    for ( uint i=0; i < armatureInfos.size(); ++i) {
        auto & arm = armatureInfos[i];

        if ( arm.GetCurAnimName() != anim_name ) {
            if ( ! arm.ChangeAnim( AnimOptT::None, anim_name ) ) {
                ERR("Editor::Think_TestAnimModel(): Couldn't change testmodel animation\n");
                globalVals.SetBool(gval_d_testAnim, false);
            }

        } else if ( !arm.IsAnimDone() ) {
            if ( arm.ContinueAnim( AnimOptT::None, true, animSpeed ) ) {
                continue;
            }
        }

        arm.BeginAnim( AnimOptT::None, true );
    }
}
#endif // MONTICELLO_DEBUG

bool AnimInfo::DoAnim( const AnimSelection& animSelection, const AnimChannelSelection& channelSelection, const AnimChannelT matchToChannel, const float animSpeed ) {
    ASSERT( owner );

    const auto entity = owner->GetOwner();
    const auto c = static_cast< AnimChannelT_BaseType >( channelSelection.channel );
    ASSERT( c < MAX_ANIM_CHANNELS );

    ArmatureInfo *armInfo = GetArmatureInfo(channelSelection.channel);
    if ( !armInfo || animSelection.anim_name == "" ) {
        animChannelStates.animating[c] = false;
        animChannelStates.animName[c] = "";
        animChannelStates.priority[c] = ANIM_PRIORITY_NOT_ANIMATING;
        animChannelStates.animState[c] = AnimStateT::None;
        return false;
    }

    const bool playingAlready = animChannelStates.animState[c] == animSelection.animState;
    if ( !playingAlready ) {
        if ( ResetAnim( animSelection, channelSelection ) ) {
            MatchFrameToMasterChannel( animSelection, channelSelection, matchToChannel );
            if ( entity ) {
                CheckAnimCue( *entity, *armInfo, channelSelection );
            }
            return true;
        }
    }

    animChannelStates.animating[c] = true;
    animChannelStates.animName[c] = animSelection.anim_name;
    animChannelStates.priority[c] = channelSelection.priority;
    animChannelStates.animState[c] = animSelection.animState;

    const bool noAnimPause = entity && entity->GetNoAnimPause();

    if ( armInfo->ContinueAnim( animSelection.opts, noAnimPause, animSpeed ) ) {
        if constexpr ( animDebugExtra ) {
            Printf("%u %s chan %u: Continuing anim %s at speed %f\n", game->GetGameTime(), GetModel()->GetName().c_str(), c, animChannelStates.animName[c].c_str(), static_cast< double >( animSpeed ) );
        }

        if ( entity ) {
            CheckAnimCue( *entity, *armInfo, channelSelection );
        }

        return true;
    }

    if ( entity ) {
        entity->AnimationCompleted( animSelection.animState, channelSelection.channel );
    }

    const bool loopAnim = BitwiseAnd( animSelection.opts, AnimOptT::Loop ) && armInfo->BeginAnim( animSelection.opts, noAnimPause );
    if ( !loopAnim ) {
        if constexpr ( animInfoDebug ) {
            Printf("%u %s chan %u: Completed anim %s\n", game->GetGameTime(), GetModel()->GetName().c_str(), c, armInfo->GetCurAnimName().c_str() );
        }

        animChannelStates.animating[c] = false;
        animChannelStates.animName[c] = "";
        animChannelStates.priority[c] = 0;
        animChannelStates.animState[c] = AnimStateT::None;

        return false; // not looping forever
    }

    if constexpr ( animInfoDebug ) {
        Printf("%u %s chan %u: Looping anim %s at speed %f\n", game->GetGameTime(), GetModel()->GetName().c_str(), c, armInfo->GetCurAnimName().c_str(), static_cast< double >( animSpeed ) );
    }

    return true;
}

const Armature* AnimInfo::GetAnim_NoErr( const std::string& animName, const AnimChannelT channel ) const {
    ASSERT( owner );
    auto model = owner->GetDrawModel();
    if ( ! model )
        return nullptr;

    const auto animModel = static_cast< const AnimatedModel* >( model.get() );
    ASSERT( animModel );

    return animModel->GetAnim_NoErr( animName, channel );
}

bool AnimInfo::MatchFrameToMasterChannel( const AnimSelection& animSelection, const AnimChannelSelection& channelSelection, const AnimChannelT toChannel ) {
    const auto c = static_cast< AnimChannelT_BaseType >( channelSelection.channel );

    ArmatureInfo *const armInfo = GetArmatureInfo(channelSelection.channel);
    if ( ! armInfo )
        return false;

    // ** play the same randomization and synchronize with frames if playing the same animation

    ArmatureInfo *otherArmInfo = GetArmatureInfo( toChannel );
    if ( !otherArmInfo )
        return false;

    const auto master_c = static_cast< AnimChannelT_BaseType >( toChannel );
    if ( animChannelStates.animState[master_c] != animSelection.animState )
        return false;

    const auto otherAnim = otherArmInfo->GetCurAnim();
    if ( !otherAnim )
        return false;

    const auto frame = otherArmInfo->GetCurFrame();
    
    if ( frame == 0 )
        return false; // the other anim just started, no need to match

    if constexpr ( animInfoDebug ) {
        Printf("%u %s chan %u: Matched anim %s to frame %u of channel %u\n", game->GetGameTime(), GetModel()->GetName().c_str(), c, animSelection.anim_name.c_str(), frame, master_c );
    }

    armInfo->SetAnimFrame( *otherArmInfo );
    return true;
}

bool AnimInfo::ResetAnim( const AnimSelection& animSelection, const AnimChannelSelection& channelSelection ) {
    ArmatureInfo *armInfo = GetArmatureInfo(channelSelection.channel);
    if ( ! armInfo )
        return false;

    const auto c = static_cast< AnimChannelT_BaseType >( channelSelection.channel );

    ASSERT( owner );
    const auto entity = owner->GetOwner();

    ASSERT( c < MAX_ANIM_CHANNELS );
    animChannelStates.animating[c] = false;
    animChannelStates.animName[c].clear();
    animChannelStates.priority[c] = ANIM_PRIORITY_NOT_ANIMATING;
    animChannelStates.animState[c] = AnimStateT::None;

    const bool prevAnimCompleted = armInfo->IsAnimDone();
    //const std::string prevAnimName = armInfo->GetCurAnimName();

    const std::string prevAnim = armInfo->GetCurAnimName();

    if ( entity ) {
        entity->StopAllAnimCueEntityEffects( channelSelection.channel );
    }

    if ( ! armInfo->ChangeAnim( animSelection.opts, animSelection.anim_name ) )
        return false;

    if ( ! prevAnimCompleted ) {
        if ( entity ) {
            entity->AnimationInterrupted( animSelection.animState, channelSelection.channel );
        }

        if constexpr ( animInfoDebug ) {
            Printf("%u %s chan %u: Interrupting anim %s by anim %s\n", game->GetGameTime(), GetModel()->GetName().c_str(),
            static_cast<AnimChannelT_BaseType>( channelSelection.channel ), prevAnim.c_str(), armInfo->GetCurAnimName().c_str() );
        }
    } else {
        if constexpr ( animInfoDebug ) {
            Printf("%u %s chan %u: Resetting anim %s\n", game->GetGameTime(), GetModel()->GetName().c_str(), c, animSelection.anim_name.c_str() );
        }
    }

    if ( ! armInfo->BeginAnim( animSelection.opts, entity ? entity->GetNoAnimPause() : false ) )
        return false;

    // Some animations may have collision models for different animations. IE, an opened door and a closed door.
    ASSERT( owner );
    if ( entity && !entity->bounds.IsSphere() ) { // entities with spheres will not utilize this feature.
        owner->SetClipModel( animSelection.anim_name );
    }

    if ( entity ) {
        CheckAnimCue( *entity, *armInfo, channelSelection );
    }

    animChannelStates.animating[c] = true;
    animChannelStates.animName[c] = animSelection.anim_name;
    animChannelStates.priority[c] = channelSelection.priority;
    animChannelStates.animState[c] = animSelection.animState;

    return true;
}

void AnimInfo::CheckAnimCue( Entity& ent, ArmatureInfo& armatureInfo, const AnimChannelSelection& channelSelection ) const {
    armatureInfo.ProcessCues( ent, armatureInfo.GetCurFrame() );

    for ( uint i=owner->NumAttachments(); i > 0;  ) {
        --i;
        const auto attachment = owner->GetAttachment(i);

        if ( attachment->GetBaseChannel() != static_cast<AnimChannelT_BaseType>( channelSelection.channel ) )
            continue;

        if ( !attachment->IsVisible() )
            continue;

        if ( !attachment->HasChannel( channelSelection.channel ) )
            continue;

        ArmatureInfo* arm = attachment->GetArmatureInfo(channelSelection.channel);
        if ( !arm )
            continue;

        arm->ProcessCues( ent, arm->GetCurFrame() );
    }
}

/*
Each channel of a model should have a unique priority amongst all the others in its own channel,
except for if there are overloads of the animation for use with different attachments.

Any priority less than 500 will drop the animState unless another channel is currently playing
the same animation on a priority >=500.

Animation priority rule-of-thumb:
    NOT_ANIMATING = 0
    IDLE = 1000
    UNNECESSARY = 3000
    // anything less than 5000 will be removed from animationsToTry during Animate() unless another channel is playing the same animation on a priority >= 5000
    MOVEMENT = 7000
    ACTION = 8000
    UNINTERRUPTIBLE = 9000
*/

void AnimInfo::Animate( const std::vector< AnimSelection >& anims, const float animSpeed ) {
    if constexpr ( animInfoDebug ) {
        Printf("\n\n%u %s: Entered NewAnimate()\n", game->GetGameTime(), GetModel()->GetName() );
    }
        
    ASSERT( owner );
    auto ent = owner->GetOwner();
    ASSERT( ent );
    
    owner->animationsToTry_lock = true;
    auto & animationsToTry = owner->animationsToTry;
    
    animationsToTry.emplace_back( ent->GetAnimStateForAttachment( AnimStateT::Idle ) );
    
    auto & animationsToInsert = owner->animationsToInsert;
    auto & animationsToRemove = owner->animationsToRemove;
    
    for ( auto const anim : animationsToInsert )
        animationsToTry.emplace_back( anim );
    animationsToInsert.clear();
    
    for ( auto const anim : animationsToRemove )
        PopSwap_Value( animationsToTry, anim );
    animationsToRemove.clear();
        
    PopSwapDuplicates( animationsToTry );
    std::sort( animationsToTry.begin(), animationsToTry.end() );
    
    AnimStateT animsPlaying[20]; //todo: if we ever need to play more than 20 animtions at once (which would be a crazy lot) this would need to grow
    uint numAnimsPlaying=0;
    
    if constexpr ( animInfoDebug ) {
        Printf("Will Try: \n");
        for ( uint i=0; i<animationsToTry.size(); ++i ) {
            const AnimStateT tryState = animationsToTry[i];
            const uint animIndex = ent->GetAnimIndex( tryState );
            ASSERT_MSG( animIndex != MAX_INT32, "You need to override Entity::GetAnimIndex() and add this attachment to it.\n" )
            const auto & anim = anims[animIndex];
            
            Printf("  %?\n", anim.anim_name);
        }
    }    

    for ( uint i=0; i<animationsToTry.size(); ++i ) {
        const AnimStateT tryState = animationsToTry[i];

        const uint animIndex = ent->GetAnimIndex( tryState );
        ASSERT_MSG( animIndex != MAX_INT32, "You need to override Entity::GetAnimIndex() and add this attachment to it. The attachment needs to have the same animation as well..\n" )
        const auto & anim = anims[animIndex];

        if constexpr ( animInfoDebug ) {
            Printf("%? %?:NewAnimate(): Checking anim %?\n", game->GetGameTime(), GetModel()->GetName(), anim.anim_name );
        }
        
        for ( uint c=0; c<anim.channel_selections.size(); ++c ) {
            auto & chan = anim.channel_selections[c];
            

            if constexpr ( animInfoDebug ) {
                Printf("%? %?:NewAnimate(): Checking chan %?.\n", game->GetGameTime(), GetModel()->GetName(), c );
            }
            
            if ( animChannelStates.animating[c] ) {
                if ( chan.priority <= animChannelStates.priority[c] ) {
                    if ( animChannelStates.animState[c] != anim.animState ) {

                        //todo we need to allow the animation to be interrupted if it has finished
                        //if ( BitwiseAnd( animationsToTry[i], animChannelStates.animState[c] ) )
                        if ( Contains( animationsToTry, animChannelStates.animState[c] ) ) {
                            if constexpr ( animInfoDebug ) {
                                Printf("%? %?:NewAnimate(): chan %? will NOT try to interrupt %? with %?\n", game->GetGameTime(), GetModel()->GetName(), c, animChannelStates.animName[c], anim.anim_name);
                            }

                            continue;
                        } else {
                            if constexpr ( animInfoDebug ) {
                                Printf("%? %?:NewAnimate(): chan %? WILL try to interrupt %? with %?\n", game->GetGameTime(), GetModel()->GetName(), c, animChannelStates.animName[c], anim.anim_name);
                            }
                        }
                    }
                }
            }

            #ifdef MONTICELLO_DEBUG
                // make sure all attachments can play the anim, too
                ASSERT( owner );
                for ( uint a=owner->NumAttachments(); a > 0;  ) {
                    --a;
                    const auto attachment = owner->GetAttachment(a);

                    if ( !attachment->IsVisible() )
                        continue;

                    if ( !attachment->HasChannel( chan.channel ) )
                        continue;

                    if ( attachment->GetBaseChannel() != static_cast<AnimChannelT_BaseType>( chan.channel ) )
                        continue;

                    const auto attachment_anim_info = attachment->GetAnimInfo();
                    ASSERT( attachment_anim_info );
                    if ( !attachment_anim_info->GetAnim_NoErr(anim.anim_name.c_str(), chan.channel) ) {
                        std::string modelName = owner->GetDrawModel() ? owner->GetDrawModel()->GetName().c_str() : "";
                        std::string attModelName = owner->GetDrawModel() ? owner->GetDrawModel()->GetName().c_str() : "";
                        WARN("Note: Model %?'s attachment model %? doesn't have anim %?, not using it.\n", modelName.c_str(), attModelName.c_str(), anim.anim_name.c_str() );
                        continue;
                    }
                }
            #endif // MONTICELLO_DEBUG

            /*todo: If we wind up with models that have more than two channels, we will have  to cycle through each of them to
            check whether they are playing the same animation, for which to MatchFrameToMasterChannel to.
            */
            AnimChannelT otherChannel;
            if ( anim.channel_selections.size() > 1 && c == 0) {
                otherChannel = anim.channel_selections[1].channel;
            } else {
                otherChannel = anim.channel_selections[0].channel;
            }

            //todo: priority
            if ( DoAnim( anim, anim.channel_selections[c], otherChannel, animSpeed ) ) {
                if constexpr ( animInfoDebug ) {
                    Printf("%? %?: DoAnim(true): chan %? %?\n", game->GetGameTime(), GetModel()->GetName(), c, anim.anim_name);
                }
                
                if ( anim.channel_selections[c].priority >= 5000 ) {
                    animsPlaying[numAnimsPlaying++] = anim.animState;
                    if constexpr ( animInfoDebug ) {
                        Printf("%? %?: SAVING ANIM STATE: %?\n", game->GetGameTime(), GetModel()->GetName(), anim.anim_name );
                    }
                }
                
                continue;
            }
            
            if ( c == 1 ) {
                if ( anim.anim_name == "attackLeftWithNewspaper" ) {
                    globalVals.Set("test", "test");
                }
            }

            if constexpr ( animInfoDebug ) {
                Printf("%? %?: DoAnim(false): chan %? %?\n", game->GetGameTime(), GetModel()->GetName(), c, anim.anim_name);
            }

        }
    }
    
    /*todoanimb: what happens if the anim the torso is on gets removed from the list and whatever we have in animationsToTry
    doesn't have a high enough priority? Does DoAnim never get called?
    */

    animationsToTry.clear();
    
    for ( uint a=0; a<numAnimsPlaying; ++a )
        animationsToTry.emplace_back( animsPlaying[a] );
        
    owner->animationsToTry_lock = false;

    if constexpr ( animInfoDebug ) {
        Printf("%? %?: Finished NewAnimate()\n", game->GetGameTime(), GetModel()->GetName() );
    }

}

std::size_t AnimInfo::GetBoneArraySize( void ) const {
    return boneArray.size();
}

void AnimInfo::SetOwner( ModelInfo& _owner ) {
    owner = &_owner;
}

const ModelInfo* AnimInfo::GetOwner( void ) const {
    return owner;
}

ModelInfo* AnimInfo::GetOwner( void ) {
    return owner;
}

std::shared_ptr< const Model > AnimInfo::GetModel( void ) const {
    ASSERT( owner );
    return owner->GetModel();
}

std::size_t AnimInfo::GetNumArmatures( void ) const {
    return armatureInfos.size();
}

bool AnimInfo::BonePosComputed( void ) const {
    return bonePositionsComputed;
}

std::vector< ArmatureInfo >& AnimInfo::GetArmatureInfos( void ) {
    return armatureInfos;
}


