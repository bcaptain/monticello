// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CLIB_MODELANIM_H_
#define SRC_CLIB_MODELANIM_H_

extern const uint ANIM_FRAME_DURATION_MS; //!< the number of miliseconds a single frame lasts
extern const uint ANIM_FRAMES_PER_SEC; //!< actual animation frame rate
extern const uint ANIM_DRAW_FRAMES_PER_SEC; //!< the frame rate that animations will draw on screen.

#include "../../base/main.h"
#include "./armatureInfo.h"
#include "./animCue.h"
#include "./bonePos.h"
#include "../../preload.h"

struct ComputedBoneDQ {
    ComputedBoneDQ( void ) : dq(), computed(false) {}
    Dualquat dq;
    bool computed;
};

class ModelManager;
class DamageInfo;

template<typename TYPE>
class VBO;

class BoneKeyFrame {
    friend class ParserBM1Anim; // fills this class with data
public:
    BoneKeyFrame( void );
    BoneKeyFrame( const BoneKeyFrame& other ) = default;
    ~BoneKeyFrame( void );

public:
    BoneKeyFrame& operator=( const BoneKeyFrame& other ) = default;

public:
    Uint16 GetFrame( void ) const;
    Vec3f GetHead( void ) const;
    Vec3f GetTail( void ) const;
    Quat GetQuat( void ) const;
    Dualquat GetDualQuat( void ) const;
    Vec3f GetTranslation( void ) const;

private:
    void SetFrame( const Uint16 to );
    void SetHead( const Vec3f& to );
    void SetTail( const Vec3f& to );
    void SetRotation( const Quat& to );
    void SetDualQuat( const Dualquat& to );
    void NormalizeQuat( void );

private:
    Dualquat orientation;
    Vec3f head;  //!< Can be used as the bones 'location' that keyframe.  Different from teePoseHead
    Vec3f tail;  //!< Used for drawing bone names and standing pose.
    Uint16 frame;
};

class Bone {
    friend class ParserBM1Anim; // fills the class with data

public:
    Bone( void ) = delete;
    Bone( const Bone& other );
    explicit Bone( const Armature& owner );
    explicit Bone( Bone* _parent );
    Bone( Bone&& other_rref );
    ~Bone( void );

public:
    Bone& operator=( const Bone& other );
    Bone& operator=( Bone&& other_rref );

private:
    void SetName( const std::string& str ); //!< Root bone's name must be unique among its childrens' names

public:
    void SetOwner( const Armature& _owner ); // todobrandon: this should only be allowed to be called by Armature. //todoattorney
    const Armature* GetOwner( void ) const; //!< never null
    
    std::string GetName( void ) const;
    Uint8 GetID( void ) const; //!< ID is unique per bone (in a single skeleton)
    AnimChannelT GetChannel( void ) const;
    void DrawAllNameVBOs( void ) const;

    void SetBindPose( const Dualquat& to );
    Dualquat GetBindPose( void ) const;
    
    void SetInvBindPose( const Dualquat& to );
    Dualquat GetInvBindPose( void ) const;

    void SetTeePoseHead( const Vec3f& to );
    Vec3f GetTeePoseHead( void ) const;
    
    void SetTeePoseTail( const Vec3f& to );
    Vec3f GetTeePoseTail( void ) const;
    
    uint NumChildren( void ) const;
    
    const BoneKeyFrame* GetKeyFrame( const std::size_t idx ) const;
    
    const Bone* GetParent( void ) const;
    const Bone* GetChild( const uint index ) const;
    
    const Bone* FindBone( const Uint8 boneID ) const;
    const Bone* FindBone( const std::string& boneName ) const;
    bool FindIDByName( const std::string& name, Uint8& boneID_out ) const; //!< Returns False if bone not in armature.

    bool IsBonePosCached( const uint frame ) const;
    void CacheBonePos( const Dualquat& boneDQ, const uint frame ) const;
    bool GetCachedPos( const Uint8 boneID, const uint frame, Dualquat& dq_out ) const;
    ComputedBoneDQ GetCachedPos( const uint frame ) const;

private:
    void DrawNameVBO( const Bone& arm ) const;
    void PackNameVBO( void ) const;
    void SetID( const Uint8 _id );

private:
    const Armature* owner;
    Uint8 id; //!< The index of this bone
    Dualquat inverseBindPos; //!< Inverse bind pose for this bone.
    Dualquat bind_pose; //!< Bind pose for this bone.

    Vec3f teePoseHead; //!< Used to store T_Pose positions of bones head. Needed to have the skeleton draw correctly for animations where the first frame isn't T-pose
    Vec3f teePoseTail; //!< Used to store T_Pose positions of bones tail. Needed to have the skeleton draw correctly for animations where the first frame isn't T-pose

    Bone* parent;
    std::vector< Bone > children; //!< These are pointers so that we can construct them with specific number of elements.
    std::string boneName; //!< The bone's name; the root bone's name will be unique among its childrens' names
    std::vector< BoneKeyFrame > keyframes; //!< Actual keyframes in the animation
private:
    mutable std::mutex bone_dq_mutex;
    mutable std::vector< ComputedBoneDQ > bone_dq;
private:
    VBO<float>* vbo_bonename;
    VBO<float>* vbo_bonename_uv;
};

class Armature {
    friend class ParserBM1Anim; // fills the class with data

public:
    Armature( void );
    Armature( const Armature& other ) = delete;
    Armature( Armature&& other_rref );

public:
    Armature& operator=( const Armature& other );
    Armature& operator=( Armature&& other_rref );

private:
    void SetName( const std::string& str ); //!< Only call on the root bone!
    void SetAnimID( const std::size_t newID );

    void AttachToBoneID( const Uint8 id );
    void AttachToPointOnBone( const float to );

    void SetBoneCount( const std::size_t cnt );

public:
    void SetAnimName( const std::string& str ); //!< Only call on the root bone!
    bool PlayingSameAnim( const Armature& anim ) const;
    void DrawAllNameVBOs( void ) const;
    
    const Bone* FindBone( const Uint8 boneID ) const;
    const Bone* FindBone( const std::string& boneName ) const;
    bool FindIDByName( const std::string& name, Uint8& boneID_out ) const; //!< Returns False if bone not in armature.
    
    bool GetCachedPos( const Uint8 boneID, const uint frame, Dualquat& dq_out ) const;

    std::string GetAnimName( void ) const;
    void AddCue( const AnimCue& cue );
    std::size_t NumKeyFrames( void ) const; //This is a zero indexed
    const Link< AnimCue >* GetFirstCue() const;

    std::size_t GetAttachedToBoneID( void ) const;
    float GetAttachPointDistFromHead( void ) const;
    bool IsAttachedToBone( void ) const;
    
    const Bone& GetRootBone_Ref( void ) const;
    std::size_t GetBoneCount( void ) const;
    AnimChannelT GetChannel( void ) const;
    uint GetNumAnimFrames( void ) const;

private:
    std::size_t boneCount;
    std::string animName;
    uint animID; //!< unique to the animName amongst all models
    Uint16 numKeyFrames;
    LinkList< AnimCue > cues;
    bool isAttachedToBone;
    std::size_t attachedToBoneID; //!< The index of the bone in another armature that this root bone is attached to (if isAttachedToBone)
    float attachedToPointOnBone; //!< 0 to 1, head to tail
    AnimChannelT channel;
    Bone root;
};

#endif  // SRC_CLIB_MODELANIM_H_
