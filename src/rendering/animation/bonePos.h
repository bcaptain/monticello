// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifndef SRC_CLIB_BONEPOS_H_
#define SRC_CLIB_BONEPOS_H_

#include "./clib/src/warnings.h"
#include "../../base/main.h"

struct BonePos {
    BonePos( const Vec3f _head, const Vec3f _tail );
    BonePos( void );
    
    Vec3f head;
    Vec3f tail;
};

#endif // SRC_CLIB_BONEPOS_H_

