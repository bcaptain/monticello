// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "bonePos.h"


BonePos::BonePos( void )
    : head()
    , tail()
{}

BonePos::BonePos( const Vec3f _head, const Vec3f _tail )
    : head( _head )
    , tail( _tail )
{}
