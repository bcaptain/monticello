// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifndef SRC_ANIMINFO_H_
#define SRC_ANIMINFO_H_

#include "../../base/main.h"
#include "armatureInfo.h"
#include "../animation/bonePos.h"
#include "../models/modelInfo.h"

const uint NUM_FLOATS_PER_BONE = 8;
const uint NUM_FLOATS_4X4_MATRIX = 16;
const uint ANIM_PRIORITY_NOT_ANIMATING = 0;

class AnimInfo;
typedef uint AnimChannelT_BaseType;

typedef Uint32 AnimStateT_BaseType;
enum class AnimStateT : AnimStateT_BaseType {
    None = 0
    , Idle
    , WalkForward
    , RunForward
    , Attack1
    , Attack2
    , Interact
    , Hurt
    , Die
    , Special1
    , Special2
    , Special3
    , Special4
    , Special5
    , Special6
    , Special7
    , Special8
    , Special9
    , Special10
    , Special11
    , Special12
    , Special13
    , Special14
    , Special15
    , Special16
    , Special17
    , Special18
    , Special19
    , Special20
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( AnimStateT, AnimStateT_BaseType )

struct AnimChannelSelection {
    AnimChannelSelection( void );
    AnimChannelSelection( const AnimChannelT _channel, const uint _priority );
    
    AnimChannelT channel;
    uint priority;
};

typedef Uint32 AnimOptT_BaseType;
enum class AnimOptT : AnimOptT_BaseType {
    None=0
    , BlendInto=1
    , Loop=1<<1
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( AnimOptT, AnimOptT_BaseType )
  
struct AnimSelection {
    AnimSelection( void );
    AnimSelection( const char* _anim_name, const AnimStateT _anim_state, const AnimOptT _opts, const std::vector<AnimChannelSelection>& _channel_selections );
        
    std::string anim_name;
    std::vector<AnimChannelSelection> channel_selections;
    AnimStateT animState;
    AnimOptT opts;
};

/*!

Animation \n\n

**/

class Animation {
public:
    Animation( void ) = default;
    ~Animation( void ) = default;
    Animation( const Animation& other ) = default;

public:
    Animation& operator=( const Animation& other ) = default;

};

//! Used to keep track of what a particular animchannel is doing. The array indices refer to the channel number.
class AnimChannelStates {
public:
    AnimChannelStates( void );
    
    AnimChannelStates( const AnimChannelStates& other );
    AnimChannelStates( AnimChannelStates&& other_rref );
    
    AnimChannelStates& operator=( const AnimChannelStates& other );
    AnimChannelStates& operator=( AnimChannelStates&& other_rref );

public:
    //todo: would be nice if these were vectors instead of arrays 
    std::array< std::string, MAX_ANIM_CHANNELS > animName; //!< the name of the anim without any numbers after it (eg, if we are playing "melee2" then animName is "melee"
    std::array< uint, MAX_ANIM_CHANNELS > priority;
    std::array< AnimStateT, MAX_ANIM_CHANNELS > animState;
    std::bitset< MAX_ANIM_CHANNELS > animating;
};

class AnimInfo {
public:
    AnimInfo( void ) = delete;
    explicit AnimInfo( ModelInfo& _owner );
    AnimInfo( const AnimInfo& other );
    AnimInfo( AnimInfo&& other_rref );
    ~AnimInfo( void ) = default;

public:
    AnimInfo& operator=( const AnimInfo & other);
    AnimInfo& operator=( AnimInfo && other_rref);

public:
    const ArmatureInfo* GetArmatureInfo( const AnimChannelT channel ) const;
    ArmatureInfo* GetArmatureInfo( const AnimChannelT channel );
    
    std::size_t GetBoneArraySize( void ) const;
    std::vector< ArmatureInfo >& GetArmatureInfos( void );
    void ModelChanged( void );
    std::size_t GetNumArmatures( void ) const;
    bool FindIDByName( const std::string& boneName, Uint8& boneID_out) const; //!< returns false if bone is not in armatures.
    bool BonePosComputed( void ) const;
    std::shared_ptr< const Model > GetModel( void ) const;
    const ModelInfo* GetOwner( void ) const;
    ModelInfo* GetOwner( void );
    void SetOwner( ModelInfo& _owner );
    bool ComputeBonePos( void );
    bool SendShaderInfo( void ) const;
    void PackBoneArray( const Dualquat& arm_bone_dq, const std::size_t start_index );
    void DrawSkel( void ) const;
    void Animate( const std::vector< AnimSelection >& anims, const float animSpeed );
    
private:
    void AllocateBoneArray( void );
    void CheckAnimCue( Entity& ent, ArmatureInfo& armatureInfo, const AnimChannelSelection& channelSelection ) const;
    bool DoAnim( const AnimSelection& animSelection, const AnimChannelSelection& channelSelection, const AnimChannelT matchToChannel, const float animSpeed = 1 );
    bool MatchFrameToMasterChannel( const AnimSelection& animSelection, const AnimChannelSelection& channelSelection, const AnimChannelT toChannel );
    bool ResetAnim( const AnimSelection& animSelection, const AnimChannelSelection& channelSelection );
    
public:
    const Armature* GetAnim( const std::string& animName, const AnimChannelT channel ) const;
    const Armature* GetAnim_NoErr( const std::string& animName, const AnimChannelT channel ) const;

#ifdef MONTICELLO_EDITOR 
    void TestModel_Animate( const char* anim_name );
    void TestModel_Animate( void );
#endif // MONTICELLO_EDITOR

private:
    void UpdateOwners( void );
    const ArmatureInfo* GetMasterChannelArmatureInfo( void ) const;

private:
    AnimChannelStates animChannelStates;
    std::vector<float> boneArray; /*!< Uniform array for passing interpolated bone dualQuats to shaders. */
    std::vector< ArmatureInfo > armatureInfos;
    ModelInfo* owner; //!< will never be null
    AnimChannelT masterChannel; //!< Legs is usually the master, (meaning torso is affixed to legs)
    bool bonePositionsComputed; //NOTE: This gets set to true if any armature has packed its animated data into boneArray.
};

#endif // SRC_ANIMINFO_H_
