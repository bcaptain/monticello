// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "../../game.h"
#include "../vbo.h"
#include "../renderer.h"
#include "./animation.h"
#include "../fontManager.h"
#include "../font.h"

const uint ANIM_DRAW_FRAMES_PER_SEC = 30;
const uint ANIM_FRAMES_PER_SEC = 30;
const uint ANIM_FRAME_DURATION_MS = 1000 / ANIM_FRAMES_PER_SEC;

BoneKeyFrame::BoneKeyFrame( void )
    : orientation()
    , head()
    , tail()
    , frame(0)
{ }

BoneKeyFrame::~BoneKeyFrame( void ) {
}

Bone::Bone( const Armature& _owner )
    : owner( &_owner )
    , id(0)
    , inverseBindPos()
    , bind_pose()
    , teePoseHead()
    , teePoseTail()
    , parent(nullptr)
    , children()
    , boneName()
    , keyframes()
    , bone_dq_mutex()
    , bone_dq()
    , vbo_bonename( new VBO<float>(2, VBOChangeFrequencyT::RARELY ) )
    , vbo_bonename_uv( new VBO<float>(2, VBOChangeFrequencyT::RARELY ) )
{
    std::lock_guard< std::mutex > bone_dq_mutex_lock( bone_dq_mutex );
    bone_dq.reserve(ANIM_DRAW_FRAMES_PER_SEC);
}

Bone::Bone( Bone* _parent )
    : owner( _parent->owner )
    , id(0)
    , inverseBindPos()
    , bind_pose()
    , teePoseHead()
    , teePoseTail()
    , parent( _parent )
    , children()
    , boneName()
    , keyframes()
    , bone_dq_mutex()
    , bone_dq()
    , vbo_bonename( new VBO<float>(2, VBOChangeFrequencyT::RARELY ) )
    , vbo_bonename_uv( new VBO<float>(2, VBOChangeFrequencyT::RARELY ) )
{
    ASSERT( parent );
}

Bone::~Bone( void ) {
    delete vbo_bonename;
    delete vbo_bonename_uv;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter" // we know
#pragma GCC diagnostic ignored "-Weffc++" // we know
#pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn" // no
Bone::Bone( const Bone& other ) {
    ASSERT( false ); // copying animations is unnecessary, complicated, and expensive.
}

Bone& Bone::operator=( const Bone& other ) {
    ASSERT( false ); // copying animations is unnecessary, complicated, and expensive.
    return *this;
}

#pragma GCC diagnostic pop

Bone::Bone( Bone&& other_rref )
    : owner( other_rref.owner )
    , id(other_rref.id)
    , inverseBindPos(other_rref.inverseBindPos)
    , bind_pose(other_rref.bind_pose)
    , teePoseHead(other_rref.teePoseHead)
    , teePoseTail(other_rref.teePoseTail)
    , parent(other_rref.parent)
    , children( std::move(other_rref.children) )
    , boneName( std::move(other_rref.boneName) )
    , keyframes( std::move(other_rref.keyframes ) )
    , bone_dq_mutex()
    , bone_dq( std::move( other_rref.bone_dq ) )
    , vbo_bonename( other_rref.vbo_bonename )
    , vbo_bonename_uv( other_rref.vbo_bonename_uv )
{
    other_rref.vbo_bonename = nullptr;
    other_rref.vbo_bonename_uv = nullptr;
}

Bone& Bone::operator=( Bone&& other_rref ) {
    std::lock_guard< std::mutex > bone_dq_lock( bone_dq_mutex );
    
    owner = other_rref.owner;
    id = other_rref.id;
    inverseBindPos = other_rref.inverseBindPos;
    bind_pose = other_rref.bind_pose;
    teePoseHead = other_rref.teePoseHead;
    teePoseTail = other_rref.teePoseTail;
    parent = other_rref.parent;
    children = std::move(other_rref.children);
    boneName = std::move(other_rref.boneName);
    keyframes = std::move(other_rref.keyframes);
    bone_dq = std::move( other_rref.bone_dq );
    vbo_bonename = other_rref.vbo_bonename;
    vbo_bonename_uv = other_rref.vbo_bonename_uv;

    other_rref.vbo_bonename = nullptr;
    other_rref.vbo_bonename_uv = nullptr;

    return *this;
}

void Bone::PackNameVBO( void ) const {
    // once we're packed, we're good to go
    if ( vbo_bonename->Finalized() )
        return;

    FontInfo fi;
    fi.size_ui_px = 0.05f;
    std::string text( GetName() );
    text += " id: ";
    text += std::to_string( static_cast<unsigned int>(GetID()) );
    vbo_bonename->PackText( *vbo_bonename_uv, text.c_str(), fi );

    vbo_bonename->MoveToVideoCard();
    vbo_bonename_uv->MoveToVideoCard();

    for ( uint i = 0; i < NumChildren(); ++i )
        GetChild(i)->PackNameVBO();
}

void Bone::DrawAllNameVBOs( void ) const {

    glDisable( GL_DEPTH_TEST );

    shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
    shaders->SetUniform1f("fWeight", 1.0f );

    PackNameVBO();
    DrawNameVBO( *this );

    glEnable( GL_DEPTH_TEST );
}

void Bone::DrawNameVBO( const Bone& arm ) const {
    if ( ! arm.vbo_bonename->Finalized() )
        return;

    if ( ! arm.vbo_bonename_uv->Finalized() )
        return;

    const char* fontname = "courier";

    std::shared_ptr< const Font > fnt = fontManager.Get( fontname );
    Font::AssertValid( fnt, fontname );

    renderer->PushMatrixMV();
        // bone name will be drawn starting at the center of the bone
        Vec3f center_bone_local( arm.GetKeyFrame(0)->GetTail() );
        center_bone_local -= arm.GetKeyFrame(0)->GetHead();
        center_bone_local /= 2;

        Vec3f center_bone_world( arm.GetKeyFrame(0)->GetHead() );
        center_bone_world += center_bone_local;

        renderer->ModelView().Translate( center_bone_world );

        // we must keep translation but discard rotation in order for text to always face the screen
        Vec3f translation( renderer->ModelView().GetTranslation() );
        renderer->ModelView().LoadIdentity();
        renderer->ModelView().SetTranslation( translation );

        // flip Y axis, since UI is usually for drawing with 0,0 as top-left of screen
        renderer->ModelView().Scale( 1.0f,-1.0f,1.0f );

        shaders->SendData_Matrices();
        shaders->SetTexture( fnt->GetMaterial()->GetDiffuse()->GetID() );

    renderer->PopMatrixMV();

    shaders->SetUniform4f("vColor", Color4f(0,1,0,1) );
    shaders->SetAttrib( "vUV", *arm.vbo_bonename_uv );
    shaders->SetAttrib( "vPos", *arm.vbo_bonename );
    shaders->DrawArrays( GL_QUADS, 0, arm.vbo_bonename->Num() );

    for ( uint i = 0; i < arm.NumChildren(); ++i )
        DrawNameVBO( *arm.GetChild(i) );
}

const BoneKeyFrame* Bone::GetKeyFrame( const std::size_t idx ) const {
    if ( keyframes.size() < 1 )
        return nullptr;

    const std::size_t last_frame_index = keyframes.size() - 1;
    if ( idx > last_frame_index ) {
        ERR("Attempted to get keyframe %i and the last keyframe index is %i.\n", idx, last_frame_index );
        return &keyframes[last_frame_index];
    }
    return &keyframes[idx];
}

bool Bone::FindIDByName( const std::string& name, Uint8& boneID_out ) const {
    if (boneName == name) {
        boneID_out = id;
        return true;
    }

    for ( uint i = 0; i < NumChildren(); ++i ) {
        if (children[i].FindIDByName(name, boneID_out))
            return true;
    }

    return false; //bone not found in armature
}


bool Bone::GetCachedPos( const Uint8 boneID, const uint frame, Dualquat& dq_out ) const {
    if ( frame >= bone_dq.size() )
        return false;

    if ( boneID == id) {
        if ( ! bone_dq[frame].computed ) {
            return false;
        }
        
        std::lock_guard< std::mutex > bone_dq_lock( bone_dq_mutex );

        dq_out = bone_dq[frame].dq;
        return true;
    }
    
    for ( auto const & arm : children ) {
        if (arm.GetCachedPos( boneID, frame, dq_out ) )
            return true;
    }
        
    return false;
}

const Bone* Bone::FindBone( const std::string& _boneName ) const {
    if ( _boneName == boneName ) {
        return this;
    }

    for ( auto const & child : children ) {
        const Bone* bone = child.FindBone(_boneName);
        if ( bone ) {
            return bone;
        }
    }

    return nullptr; //bone not found in this or any children
}

const Bone* Bone::FindBone( const Uint8 boneID ) const {
    if ( boneID == id ) {
        return this;
    }

    for ( auto const & child : children ) {
        const Bone* bone = child.FindBone(boneID);
        if ( bone ) {
            return bone;
        }
    }

    return nullptr; //bone not found in this or any children
}

ComputedBoneDQ Bone::GetCachedPos( const uint frame ) const {
    std::lock_guard< std::mutex > bone_dq_lock( bone_dq_mutex );

    if ( frame >= bone_dq.size() || !bone_dq[frame].computed ) {
        WARN( "Bone::GetCachedPos(frame): DQ has not been intialized, returning default.\n" );
        return {};
    }

    return bone_dq[frame];
}

bool Bone::IsBonePosCached( const uint frame ) const {
    std::lock_guard< std::mutex > bone_dq_lock( bone_dq_mutex );

    return frame < bone_dq.size() && bone_dq[frame].computed;
}

void Bone::CacheBonePos( const Dualquat& boneDQ, const uint frame ) const {
    std::lock_guard< std::mutex > bone_dq_lock( bone_dq_mutex );

    if ( frame >= bone_dq.size() ) {
        ASSERT( owner->NumKeyFrames() > 0 );
        auto const keyFrame = GetKeyFrame( owner->NumKeyFrames()-1 );
        ASSERT( keyFrame );
        const uint numFrames = static_cast<uint>(keyFrame->GetFrame()+1);
        ASSERT( frame < numFrames );
        bone_dq.resize( numFrames );
    }

    bone_dq[frame].dq = boneDQ;
    bone_dq[frame].computed = true;
}

void Bone::SetOwner( const Armature& _owner ) {
    owner = &_owner;
    for ( auto & child : children )
        child.SetOwner( _owner );
}

void BoneKeyFrame::NormalizeQuat( void ) {
    orientation.rot.Normalize();
}

Quat BoneKeyFrame::GetQuat( void ) const {
    return orientation.rot;
}

void BoneKeyFrame::SetRotation( const Quat& to ) {
    orientation.rot = to;
}

Dualquat BoneKeyFrame::GetDualQuat( void ) const {
    return orientation;
}

void BoneKeyFrame::SetDualQuat( const Dualquat& to ) {
    orientation = to;
}

Vec3f BoneKeyFrame::GetTranslation( void ) const {
    return orientation.GetTranslation();
}

Vec3f BoneKeyFrame::GetHead( void ) const {
    return head;
}

void BoneKeyFrame::SetHead( const Vec3f& to ) {
    head = to;
}

Vec3f BoneKeyFrame::GetTail( void ) const {
    return tail;
}

void BoneKeyFrame::SetTail( const Vec3f& to ) {
    tail = to;
}

Uint16 BoneKeyFrame::GetFrame( void ) const {
    return frame;
}

void BoneKeyFrame::SetFrame( const Uint16 to ) {
    frame = to;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter" // we know
#pragma GCC diagnostic ignored "-Weffc++" // we know
#pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn" // no

    Armature& Armature::operator=( const Armature& other ) {
        ASSERT( false ); // copying animations is unnecessary, complicated, and expensive.
        return *this;
    }

#pragma GCC diagnostic pop

void Armature::AddCue( const AnimCue& cue ) {
    for ( Link< AnimCue > *link = cues.GetFirst(); link != nullptr; link = link->GetNext() ) {
        if ( link->Data() > cue ) {
            link->AddBefore( cue );
            return;
        }
    }

    cues.Append( cue );
}

Armature::Armature( Armature&& other_rref )
    : boneCount(other_rref.boneCount)
    , animName( std::move(other_rref.animName) )
    , animID( std::move(other_rref.animID) )
    , numKeyFrames(other_rref.numKeyFrames)
    , cues( std::move(other_rref.cues) )
    , isAttachedToBone( other_rref.isAttachedToBone ) //todoBrandon - check
    , attachedToBoneID( other_rref.attachedToBoneID ) //todoBrandon - check
    , attachedToPointOnBone( other_rref.attachedToPointOnBone ) //todoBrandon - check
    , channel( other_rref.channel )
    , root( std::move( other_rref.root ) )
{
    root.SetOwner( *this );
}

Armature& Armature::operator=( Armature&& other_rref ) {
    boneCount = other_rref.boneCount;
    animName = std::move(other_rref.animName);
    animID = std::move(other_rref.animID);
    numKeyFrames = other_rref.numKeyFrames;
    cues = std::move(other_rref.cues);
    root = std::move(other_rref.root);
    isAttachedToBone = other_rref.isAttachedToBone;
    attachedToBoneID = other_rref.attachedToBoneID;
    attachedToPointOnBone = other_rref.attachedToPointOnBone;
    channel = other_rref.channel;
    root.SetOwner( *this );
    return *this;
}

Armature::Armature( void )
    : boneCount( 0 )
    , animName( "" )
    , animID(0)
    , numKeyFrames(0)
    , cues()
    , isAttachedToBone(false)
    , attachedToBoneID(0)
    , attachedToPointOnBone(0)
    , channel(AnimChannelT::Channel_0)
    , root(*this)
{ }

void Armature::AttachToBoneID( const Uint8 id ) { //todob brandon: pretty sure this only is used for root bone. This should probably be in Armature class.
    isAttachedToBone = true;
    attachedToBoneID = id;
}

std::size_t Armature::GetAttachedToBoneID( void ) const {
    ASSERT( isAttachedToBone );
    return attachedToBoneID;
}

float Armature::GetAttachPointDistFromHead( void ) const {
    ASSERT( isAttachedToBone );
    return attachedToPointOnBone;
}

uint Armature::GetNumAnimFrames( void ) const {
    const BoneKeyFrame* lastKeyFrame = root.GetKeyFrame( numKeyFrames - 1 );
    ASSERT(lastKeyFrame);
    return lastKeyFrame->GetFrame();
}

Vec3f Bone::GetTeePoseHead( void ) const {
    return teePoseHead;
}

Vec3f Bone::GetTeePoseTail( void ) const {
    return teePoseTail;
}

const Bone* Bone::GetParent( void ) const {
    return parent;
}

Dualquat Bone::GetInvBindPose( void ) const {
    return inverseBindPos;
}

Dualquat Bone::GetBindPose( void ) const {
    return bind_pose;
}

void Bone::SetName( const std::string& str ) {
    boneName = str;
}

std::string Bone::GetName( void ) const {
    return boneName;
}

Uint8 Bone::GetID( void ) const {
    return id;
}

void Bone::SetID( const Uint8 _id ) {
    id = _id;
}

void Bone::SetInvBindPose( const Dualquat& to ) {
    inverseBindPos = to;
}

void Bone::SetBindPose( const Dualquat& to ) {
    bind_pose = to;
}

void Bone::SetTeePoseHead(const Vec3f& to) {
    teePoseHead = to;
}

void Bone::SetTeePoseTail(const Vec3f& to) {
    teePoseTail = to;
}

uint Bone::NumChildren( void ) const {
    return children.size();
}

const Bone* Bone::GetChild( const uint index ) const {
    return &children[index];
}

AnimChannelT Bone::GetChannel( void ) const {
    return owner->GetChannel();
}

const Armature* Bone::GetOwner( void ) const {
    return owner;
}

AnimChannelT Armature::GetChannel( void ) const {
    return channel;
}

bool Armature::IsAttachedToBone( void ) const {
    return isAttachedToBone;
}

void Armature::AttachToPointOnBone( const float to ) {
    attachedToPointOnBone = to;
}

bool Armature::GetCachedPos( const Uint8 boneID, const uint frame, Dualquat& dq_out ) const {
    return root.GetCachedPos( boneID, frame, dq_out );
}
    
const Bone* Armature::FindBone( const Uint8 boneID ) const {
    return root.FindBone( boneID );
}

const Bone* Armature::FindBone( const std::string& boneName ) const {
    return root.FindBone( boneName );
}

bool Armature::FindIDByName( const std::string& name, Uint8& boneID_out ) const { //!< Returns False if bone not in armature.
    return root.FindIDByName( name, boneID_out );
}

const Bone& Armature::GetRootBone_Ref( void ) const {
    return root;
}

std::string Armature::GetAnimName( void ) const {
    return animName;
}

std::size_t Armature::GetBoneCount( void ) const {
    return boneCount;
}

void Armature::SetBoneCount( const std::size_t cnt ) {
    boneCount = cnt;
}

const Link< AnimCue >* Armature::GetFirstCue() const {
    return cues.GetFirst();
}

std::size_t Armature::NumKeyFrames( void ) const {
    return numKeyFrames;
}

void Armature::SetAnimName( const std::string& str ) {
    animName = str;
}

void Armature::SetAnimID( const std::size_t newID ) {
    animID = newID;
}

bool Armature::PlayingSameAnim( const Armature& anim ) const {
    return animID == anim.animID;
}

void Armature::DrawAllNameVBOs( void ) const {
    root.DrawAllNameVBOs();
}
