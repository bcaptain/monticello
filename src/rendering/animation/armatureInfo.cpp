// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./armatureInfo.h"
#include "./animation.h"
#include "../../game.h"
#include "../renderer.h"
#include "../animation/animInfo.h"
#include "../fontManager.h"

constexpr const bool armInfoDebug = false;
constexpr const bool armInfoDebugExtra = false;
constexpr const bool animBlendDebug = false;

ArmatureInfo::~ArmatureInfo( void ) {
    delete vbo_skel;
}

ArmatureInfo::ArmatureInfo( void )
    : offset()
    , curKeyFrame(0)
    , curFrameStartTime(0)
    , isAnimating(false)
    , curAnim( nullptr )
    , prevAnim(nullptr)
    , prevAnimFrame(0)
    , curBlendFrame(0)
    , blendToKeyFrame(0)
    , vbo_skel( new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY) )
    , curFrame(0)
    , owner( nullptr )
    , cue(nullptr)
    , channel()
{ }

ArmatureInfo::ArmatureInfo( AnimInfo& _owner, const AnimChannelT _channel )
    : offset()
    , curKeyFrame(0)
    , curFrameStartTime(0)
    , isAnimating(false)
    , curAnim( nullptr )
    , prevAnim(nullptr)
    , prevAnimFrame(0)
    , curBlendFrame(0)
    , blendToKeyFrame(0)
    , vbo_skel( new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY) )
    , curFrame(0)
    , owner( &_owner )
    , cue(nullptr)
    , channel( _channel )
{ }

ArmatureInfo::ArmatureInfo( const ArmatureInfo& other )
    : offset( other.offset )
    , curKeyFrame(other.curKeyFrame)
    , curFrameStartTime(other.curFrameStartTime)
    , isAnimating(other.isAnimating)
    , curAnim( other.curAnim )
    , prevAnim( other.prevAnim )
    , prevAnimFrame( other.prevAnimFrame )
    , curBlendFrame( other.curBlendFrame )
    , blendToKeyFrame( other.blendToKeyFrame )
    , vbo_skel( new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY) )
    , curFrame( other.curFrame )
    , owner( other.owner )
    , cue( other.cue )
    , channel( other.channel )
{ }

ArmatureInfo& ArmatureInfo::operator=( const ArmatureInfo& other ) {
    offset = other.offset;
    curKeyFrame = other.curKeyFrame;
    curFrameStartTime = other.curFrameStartTime;
    isAnimating = other.isAnimating;
    curAnim = other.curAnim;
    prevAnim = other.prevAnim;
    prevAnimFrame = other.prevAnimFrame;
    curBlendFrame = other.curBlendFrame;
    blendToKeyFrame = other.blendToKeyFrame;
    cue = other.cue;
    curFrame = other.curFrame;
    owner = other.owner;
    channel = other.channel;

    curFrame = other.curFrame;
    vbo_skel->Clear();

    return *this;
}

void ArmatureInfo::DrawSkel( const bool draw_animated ) const {
    if ( ! curAnim )
        return;

    PackBoneVBO();
    shaders->UseProg(  GLPROG_ANIM_SKEL );
    ASSERT( owner );
    const AnimInfo* animInfo = GetOwner();

    if ( draw_animated ) {
        animInfo->SendShaderInfo();
    }
    
    glDisable(GL_DEPTH_TEST);

        shaders->SendData_Matrices();
        shaders->SetAttrib( "vPos", *vbo_skel );
        shaders->DrawArrays( GL_LINES, 0, vbo_skel->Num() );

        if ( globalVals.GetBool( gval_d_boneNames ) ) {
            curAnim->DrawAllNameVBOs();
        }

    glEnable(GL_DEPTH_TEST);
}

bool ArmatureInfo::HasChannel( const AnimChannelT _channel ) const {
    ASSERT( owner );
    std::shared_ptr< const Model > model = owner->GetModel();
    if ( ! model || !model->IsAnimated() )
        return false;

    const AnimatedModel* animModel = static_cast< const AnimatedModel* >( model.get() );
    ASSERT( animModel );

    return animModel->HasChannel( _channel );
}

const Armature* ArmatureInfo::GetAnim_NoErr( const std::string& animName, const AnimChannelT _channel ) const {
    ASSERT( owner );
    std::shared_ptr< const Model > model = owner->GetModel();
    if ( ! model || !model->IsAnimated() )
        return nullptr;

    const AnimatedModel* animModel = static_cast< const AnimatedModel* >( model.get() );
    ASSERT( animModel );

    return animModel->GetAnim_NoErr( animName, _channel );
}

uint ArmatureInfo::GetNumAnimFrames( const std::string& animName, const AnimChannelT _channel ) const {
    const Armature* arm = GetAnim( animName, _channel );
    ASSERT( arm );
    return arm->GetNumAnimFrames();
}

const Armature* ArmatureInfo::GetAnim( const std::string& animName, const AnimChannelT _channel ) const {
    ASSERT( owner );
    std::shared_ptr< const Model > model = owner->GetModel();
    if ( ! model || !model->IsAnimated() )
        return nullptr;

    const AnimatedModel* animModel = static_cast< const AnimatedModel* >( model.get() );
    ASSERT( animModel );

    return animModel->GetAnim( animName, _channel );
}

void ArmatureInfo::SetAnimFrame( const ArmatureInfo& armatureAnimToMatch ) {
    ASSERT( curAnim->GetAnimName() == armatureAnimToMatch.curAnim->GetAnimName() );
    
    curKeyFrame = armatureAnimToMatch.curKeyFrame;
    curFrameStartTime = armatureAnimToMatch.curFrameStartTime;
    curFrame = armatureAnimToMatch.curFrame;
    
    const uint blendFrames = globalVals.GetUInt( gval_d_animBlend );
    if ( blendFrames > 0 ) {
//todobrandon: this should make the blend take the same amount of time to reach the destination keyframe as the other channel.
//todobrandon: however, the torso (Brute character) is disappearing for a couple frames and not sure why...
//        const BoneKeyFrame *nextdKeyFrame_p = curAnim->GetKeyFrame(curKeyFrame+1);
//        const Uint16 nextKeyFrameNum = nextKeyFrame_p->GetFrame();
//        ASSERT( nextKeyFrameNum > curFrame );
//        curBlendFrame = nextKeyFrameNum - curFrame;
// todo:update: I think we should have a separate SetAnimFrame and BlendToAnimFrame
        blendToKeyFrame = armatureAnimToMatch.curKeyFrame+1;
    }

    // ** now any attachments

    if ( !owner )
        return;

    ModelInfo* modelInfo = owner->GetOwner();
    if ( ! modelInfo )
        return;

    for ( uint i=0; i<modelInfo->NumAttachments(); ++i ) {
        ModelInfo* attachment = modelInfo->GetAttachment(i);
        
        if ( attachment->GetBaseChannel() != static_cast<AnimChannelT_BaseType>( channel ) )
            continue;
        
        if ( !attachment->IsVisible() )
            continue;
            
        if ( !attachment->HasChannel( channel ) )
            continue;

        if ( attachment->IsVisible() ) {
            if ( ArmatureInfo* arm = attachment->GetArmatureInfo(channel) ) {
                ASSERT( arm );
                arm->SetAnimFrame( armatureAnimToMatch );
            }
        }
    }
}

bool ArmatureInfo::ChangeAnim( const AnimOptT _opts, const std::string& anim, const bool randomAnimSelection ) {
    ASSERT( owner );
    std::shared_ptr< const Model > model = owner->GetModel();
    if ( ! model || !model->IsAnimated() )
        return false;

    const AnimatedModel* animModel = static_cast< const AnimatedModel* >( model.get() );
    
    if ( randomAnimSelection ) {
        curAnim = animModel->GetRandomAnim( anim, channel );
    } else {
        curAnim = animModel->GetAnim( anim, channel );
    }

    if ( !curAnim )
        return false;
    
    const bool doBlendFrames = prevAnim && BitwiseAnd( _opts, AnimOptT::BlendInto );
    const uint numBlendFrames = doBlendFrames ? globalVals.GetUInt(gval_d_animBlend) : 0;
    if ( numBlendFrames > 0 ) {
        curBlendFrame = numBlendFrames;
        blendToKeyFrame = 0;
        if constexpr ( armInfoDebug ) {
            Printf("%u %s chan %u: Blending from anim %s frame %u to %s\n", game->GetGameTime(), GetOwner()->GetModel()->GetName().c_str(), static_cast<AnimChannelT_BaseType>( channel ),
                prevAnim->GetAnimName().c_str(), prevAnimFrame, curAnim->GetAnimName().c_str() );
        }
    } else if constexpr ( armInfoDebug ) {
        Printf("%u %s chan %u: Changed anim to %s\n", game->GetGameTime(), GetOwner()->GetModel()->GetName().c_str(), static_cast<AnimChannelT_BaseType>( channel ), curAnim->GetAnimName().c_str() );
    }

    // ** now any attachments

    ModelInfo* modelInfo = owner->GetOwner();
    if ( !modelInfo )
        return true;

    for ( uint i=0; i<modelInfo->NumAttachments(); ++i ) {
        auto att = modelInfo->GetAttachment(i);
        if ( att->IsVisible() ) {
            
            if ( att->GetBaseChannel() != static_cast<AnimChannelT_BaseType>( channel ) )
                continue;
            
            if ( !att->IsVisible() )
                continue;
                
            if ( ArmatureInfo* arm = modelInfo->GetAttachment(i)->GetArmatureInfo(channel) ) {
                if constexpr ( armInfoDebug )
                    Printf("  attachment %s chan %u: ", arm->GetOwner()->GetModel()->GetName().c_str(), static_cast<uint>(channel) );
                arm->ChangeAnim( _opts, anim, false );
            }
        }
    }
            
    return true;
}

bool ArmatureInfo::BeginAnim( const AnimOptT _opts, const bool animateDuringPause ) {
    if constexpr ( armInfoDebug ) {
        Printf("%u %s ArmatureInfo: Animation Starting\n", game->GetGameTime(), GetOwner()->GetModel()->GetName().c_str() );
    }
    
    if ( !curAnim ) {
        ERR("%s ArmatureInfo::BeginAnim() : animation must be set first!\n", GetOwner()->GetModel()->GetName().c_str());
        return false;
    }

    StartAnimating();
    ASSERT( IsAnimating() );

    curKeyFrame = 0;
    curFrame = 0;
    curFrameStartTime = animateDuringPause ? game->GetRenderTime() : game->GetGameTime();

    // ** now any attachments

    if ( !owner )
        return true;

    ModelInfo* modelInfo = owner->GetOwner();
    if ( ! modelInfo )
        return true;

    for ( uint i=0; i<modelInfo->NumAttachments(); ++i ) {
        auto att = modelInfo->GetAttachment(i);
        
        if ( att->GetBaseChannel() != static_cast<AnimChannelT_BaseType>( channel ) )
            continue;
        
        if ( !att->IsVisible() )
            continue;
            
        if ( ArmatureInfo* arm = att->GetArmatureInfo(channel) ) {
            if constexpr ( armInfoDebug )
                Printf("  attachment %s chan %u: ", arm->GetOwner()->GetModel()->GetName().c_str(), static_cast<uint>(channel) );
            if ( !arm->IsAnimating() ) {
                arm->ChangeAnim( _opts, curAnim->GetAnimName(), false );
            }
            arm->BeginAnim( _opts, animateDuringPause );
        }
    }

    return true;
}

void ArmatureInfo::ContinueAttachmentAnims( const AnimOptT _opts, const bool noAnimPause, const float animSpeed ) {
    if ( !owner )
        return;

    ModelInfo* modelInfo = owner->GetOwner();
    if ( ! modelInfo )
        return;

    for ( uint i=0; i<modelInfo->NumAttachments(); ++i ) {
        auto att = modelInfo->GetAttachment(i);
        
        if ( att->GetBaseChannel() != static_cast<AnimChannelT_BaseType>( channel ) )
            continue;
        
        if ( !att->IsVisible() )
            continue;
            
        if ( ArmatureInfo* arm = att->GetArmatureInfo(channel) ) {
            if ( !arm->IsAnimating() || arm->IsAnimDone() ) {
                arm->ChangeAnim( _opts, curAnim->GetAnimName(), false );
                arm->BeginAnim( _opts, noAnimPause );
            }
            arm->ContinueAnim( _opts, noAnimPause, animSpeed );
        }
    }
}

bool ArmatureInfo::ContinueAnim( const AnimOptT _opts, const bool noAnimPause, const float animSpeed ) {
    if ( ! IsAnimating() )
        return false;

    if ( curAnim == nullptr ) {
        WARN("ArmatureInfo::nextFrame(): Not on an animation.\n");
        return false;
    }
    
    if ( ! ComputeCurFrame( noAnimPause, animSpeed ) )
        return false;
        
    ContinueAttachmentAnims( _opts, noAnimPause, animSpeed );
        
    return !IsAnimDone();
}

const ArmatureInfo* ArmatureInfo::GetArmatureAttachedTo( void ) const {
    if ( !owner )
        return nullptr;

    const ModelInfo* modelInfo = owner->GetOwner();
    if ( ! modelInfo )
        return nullptr;

    const ModelInfo* base = modelInfo->GetBaseModelInfo();
    if ( base == nullptr )
        return nullptr; // we are not an attachment, we are the base

    const AnimInfo* baseAnimInfo = base->GetAnimInfo();
    if ( ! baseAnimInfo )
        return nullptr;

    return baseAnimInfo->GetArmatureInfo( channel );
}

const ModelInfo* ArmatureInfo::GetBaseModelInfo( void ) const {
    if ( !owner )
        return nullptr;

    const ModelInfo* modelInfo = owner->GetOwner();
    if ( ! modelInfo )
        return nullptr;

    return modelInfo->GetBaseModelInfo();
}

bool ArmatureInfo::GetAttachmentPos( Vec3f& attachPos_out ) const {
    if ( ! curAnim )            
        return false;
//    auto const & rootBone = curAnim->GetRootBone_Ref(); //todobrandon:GetAttachmentPos - does this cause the crash?

    if ( ! curAnim->IsAttachedToBone() )
        return false;

    const ArmatureInfo *const baseArmInfo = GetArmatureAttachedTo();
    if ( ! baseArmInfo )
        return false;
        
    Dualquat armChain_offset_dq;
    GetArmatureChainOffset( armChain_offset_dq );
    
    const Uint8 bone_index = curAnim->GetAttachedToBoneID();
    const Armature *const baseArm = baseArmInfo->GetCurAnim();
    if ( !baseArm )
        return false;
    
    const Bone *const attachment_bone = baseArm->FindBone( bone_index );
    if ( !attachment_bone )
        return false;
    
    auto const bonePos = attachment_bone->GetCachedPos(curFrame);
    
    Vec3f attach_point_tee_pose, attach_point_moved, attach_point_movement;
    attach_point_tee_pose = ( attachment_bone->GetTeePoseTail() - attachment_bone->GetTeePoseHead() )  * curAnim->GetAttachPointDistFromHead() + attachment_bone->GetTeePoseHead();
    attach_point_moved = attach_point_tee_pose * bonePos.dq;
    attach_point_movement = attach_point_moved - attach_point_tee_pose; 
    
    Vec3f armChain_offset = armChain_offset_dq.GetTranslation();
    attachPos_out = attach_point_movement + attach_point_tee_pose + armChain_offset;
    
    return true;
}

bool ArmatureInfo::GetArmatureChainOffset( Dualquat& arm_offset_out ) const {
    
    const ArmatureInfo *const baseArmInfo = GetArmatureAttachedTo();
    
    if ( !baseArmInfo )
        return false;
        
    arm_offset_out = arm_offset_out * baseArmInfo->GetOffset();
    
    baseArmInfo->GetArmatureChainOffset( arm_offset_out );
    
    return true;
}

bool ArmatureInfo::ComputeBonePos( const Bone* rootBone, const Uint16 rootFrame ) {
    if ( ! curAnim )
        return false;

    ASSERT( owner );
    Vec3f attach_offset;

    // ** we translate the bone either to a base armature, or if it's a attachment, to whatever it's attached to.

    // use the base armature if there is one
    if ( !GetAttachmentPos(attach_offset) ) {
        // there was no base armature, so try to attach ourselves to our master channel (if we have one )
        
        // use the current interpolated position of rootBone's as the offset (torso may be affixed to legs, for example)
        //todobrandon: ; or an attachment model to it's base model
        
        if ( rootBone ) {
            if ( rootBone->IsBonePosCached( rootFrame ) ) {
                attach_offset = rootBone->GetCachedPos(rootFrame).dq.GetTranslation();
            } else {
                WARN("ArmatureInfo::ComputeBonePos(): could not link to root skeleton (frame %u), it has not been drawn & computed yet.\n", rootFrame);
            }
        } else {
            if ( armInfoDebugExtra ) {
                Printf("Linking arm %s root skeleton (frame %u) failed, it is not on an animation\n", curAnim->GetAnimName().c_str(), rootFrame );
            }
        }
    }
    
    offset = Dualquat( attach_offset, Quat() );

    const Dualquat parent_dq; // identity
    
    if ( prevAnim && curBlendFrame > 0 ) {
        Uint32 numBlendFrames = globalVals.GetUInt(gval_d_animBlend);
        if ( numBlendFrames > 30 ) {
            WARN("%s is too high, setting to a sane default.\n", gval_d_animBlend.first);
            numBlendFrames = 3;
            globalVals.SetUInt(gval_d_animBlend, numBlendFrames);
        }
        const std::size_t fromFrame = prevAnimFrame;
        const Armature* fromAnim = prevAnim;
        const Armature* toAnim = curAnim;
        const float delta = static_cast< float >( numBlendFrames-curBlendFrame ) / numBlendFrames;
        
        BlendBonePos( fromFrame, fromAnim->GetRootBone_Ref(), toAnim->GetRootBone_Ref(), blendToKeyFrame, delta, parent_dq );
        return true;
    }
    
    const std::size_t fromKeyFrame = GetCurKeyFrame();
    const std::size_t fromFrame = curFrame;
    const Armature* fromAnim = curAnim;
    const Armature* toAnim = curAnim;
    const std::size_t toKeyFrame = fromKeyFrame+1;
    const float delta = GetKeyFrameDelta();
    curBlendFrame = 0;
    
    ComputeBonePos( fromFrame, fromAnim->GetRootBone_Ref(), fromKeyFrame, toAnim->GetRootBone_Ref(), toKeyFrame, delta, parent_dq );
    return true;
}

bool ArmatureInfo::GetCachedPos( const Uint8 boneID, const uint frame, Dualquat& dq_out ) const {
    return curAnim->GetCachedPos( boneID, frame, dq_out );
}

void ArmatureInfo::PackBoneVBO( void ) const {
    if ( !vbo_skel || !curAnim ) {
        ERR("ArmatureInfo::PackBoneVBO: No Anim.\n");
        return;
    }

    vbo_skel->Clear();
    PackBoneVBO( curAnim->GetRootBone_Ref() );
    vbo_skel->MoveToVideoCard();
}

std::string ArmatureInfo::GetCurAnimName( void ) const {
    if ( !curAnim )
        return std::string();

    return curAnim->GetAnimName();
}

bool ArmatureInfo::IsAnimDone( void ) const {
    return !curAnim || ( curKeyFrame >= curAnim->NumKeyFrames() );
}

void ArmatureInfo::UpdateVBO( void )  {
    if ( vbo_skel )
        vbo_skel->Clear();
}

void ArmatureInfo::StartAnimating( void ) {
    if ( curAnim ) {
        isAnimating = true;
        cue = curAnim->GetFirstCue();
    } else {
        StopAnimating();
    }
}

void ArmatureInfo::ProcessCues( Entity& ent, const uint currentFrame ) {
    while ( cue ) {
        const AnimCue& cur_cue = cue->Data();
        if ( currentFrame == cur_cue.frame ) {
            
            if constexpr ( animCueDebug ) {
                Printf("%u %s chan %u Cue: Anim %s Frame %u:", game->GetGameTime(), GetOwner()->GetModel()->GetName().c_str(), static_cast<int>(channel), GetCurAnimName().c_str(), cur_cue.frame );
                for ( auto& str : cur_cue.arguments )
                    Printf( " %s", str.c_str() );
                Printf("\n");
            }
            
            ent.ProcessAnimCue( cur_cue, channel );
            cue = cue->GetNext();
        } else {
            break;
        }
    }
}

void ArmatureInfo::StopAnimating( void ) {
    isAnimating = false;
    curAnim = nullptr;
    cue = nullptr;
}

//packs 4x4 matrix
void Pack4X4Matrix( std::vector<float>& list, const Mat4f& mat, const std::size_t start_index );
void Pack4X4Matrix( std::vector<float>& list, const Mat4f& mat, const std::size_t start_index ) {
    ASSERT( list.size() >= start_index + NUM_FLOATS_4X4_MATRIX );
    
    for( uint cnt = 0; cnt < NUM_FLOATS_4X4_MATRIX; cnt++ )
        list[start_index + cnt] = mat.data[cnt];
}

Uint32 ArmatureInfo::GetCurFrame( void ) const {
    return curFrame;
}

bool ArmatureInfo::ComputeCurFrame( const bool noAnimPause, const float animSpeed ) {
    if ( !curAnim || curAnim->NumKeyFrames() < 1 ) {
        curFrame = 0;
        return false;
    }

    const float framesPerSec = ANIM_FRAMES_PER_SEC * animSpeed;
    const float sec_perFrame = 1.0f/framesPerSec;
    const Uint32 ms_perFrame = static_cast< Uint32 >( sec_perFrame*1000 );
    
    const Uint32 curTime = noAnimPause ? game->GetRenderTime() : game->GetGameTime();
    
    if ( curTime-curFrameStartTime < ms_perFrame )
        return true;
    
    if ( curBlendFrame > 0 ) {
        --curBlendFrame;
        
        if constexpr ( armInfoDebug ) {
            Printf("%u %s chan %u: ComputeCurFrame: Blend frame counting down, currently: %u\n", game->GetGameTime(), GetOwner()->GetModel()->GetName().c_str(), static_cast<AnimChannelT_BaseType>( channel ), curBlendFrame );
        }
        
        return true;
    }
    
    prevAnim = curAnim;
    prevAnimFrame = curFrame;
    
    ++curFrame;
    curFrameStartTime = curTime;
    auto const & rootBone = curAnim->GetRootBone_Ref();
    
    if ( curKeyFrame+2 < curAnim->NumKeyFrames() ) {
        if ( curFrame >= rootBone.GetKeyFrame(curKeyFrame+1)->GetFrame() ) {
            ++curKeyFrame;
        }
        return true;
    }
    
    // we are approaching our very last keyframe
            
    if ( curFrame+1 > rootBone.GetKeyFrame(curKeyFrame+1)->GetFrame() ) {
        return false; // our next frame is our last frame. signal to the caller that this animation is ending this frame
    }

    return true;
}

Dualquat ArmatureInfo::CacheBonePos( const uint frame, const Bone& armFrom, const Uint16 armFrom_keyFrame, const Bone& armTo, const Uint16 armTo_keyFrame, const float timeDelta, const Dualquat& parent_dq ) {
    const auto interpToPos = armTo.GetKeyFrame(armTo_keyFrame);
    const auto from = armFrom.GetKeyFrame(armFrom_keyFrame);

    ASSERT( interpToPos );
    ASSERT( from );
      
    const Dualquat to = interpToPos->GetDualQuat();
    const Dualquat interp = Dualquat::GetInterpolated( from->GetDualQuat(), to, timeDelta );
    const auto curBone = parent_dq * armTo.GetBindPose() * interp * armTo.GetInvBindPose();
    
    armTo.CacheBonePos( curBone, frame );
    return curBone;
}

Dualquat ArmatureInfo::GetBlendedBonePos( const uint frame, const Bone& armFrom, const Bone& armTo, const Uint16 armTo_keyFrame, const float timeDelta, const Dualquat& parent_dq ) {
    // ** first we need to get the toframe/keyframe bone into object space, because the cached fromframe already is
    const auto interpToPos = armTo.GetKeyFrame(armTo_keyFrame);
    ASSERT( interpToPos );
    
    const Dualquat to = interpToPos->GetDualQuat();
    const Dualquat to_objectSpace = parent_dq * armTo.GetBindPose() * to * armTo.GetInvBindPose();
    
    const auto from = armFrom.GetCachedPos( frame );

    return from.dq.GetInterpolated( to_objectSpace, timeDelta );
}

void ArmatureInfo::BlendBonePos( const uint frame, const Bone& armFrom, const Bone& armTo, const Uint16 armTo_keyFrame, const float timeDelta, const Dualquat& parent_dq ) {
    ASSERT( curAnim );
    const uint idx = armTo.GetID()*8u; // bones are zero-indexed
    ASSERT( idx < owner->GetBoneArraySize() );
    
    if ( animBlendDebug ) {
        Printf("%u %s chan %u: BlendBonePos: Blending from %s frame %u to %s keyframe %u\n",
            game->GetGameTime(), GetOwner()->GetModel()->GetName().c_str(), static_cast<AnimChannelT_BaseType>( armTo.GetChannel() ), 
            armFrom.GetOwner()->GetAnimName().c_str(), frame, armTo.GetOwner()->GetAnimName().c_str(), armTo_keyFrame );
    }
    
    ASSERT( armFrom.IsBonePosCached( frame ) );
    const Dualquat curBone = GetBlendedBonePos( frame, armFrom, armTo, armTo_keyFrame, timeDelta, parent_dq );

    owner->PackBoneArray( offset * curBone, idx );

    for (uint i = 0; i < armTo.NumChildren(); i++)
        BlendBonePos( frame, *armFrom.GetChild( i ), *armTo.GetChild( i ), armTo_keyFrame, timeDelta, curBone );
}

void ArmatureInfo::ComputeBonePos( const uint frame, const Bone& armFrom, const Uint16 armFrom_keyFrame, const Bone& armTo, const Uint16 armTo_keyFrame, const float timeDelta, const Dualquat& parent_dq ) {
    ASSERT( curAnim );
    const uint idx = armTo.GetID()*8u; // bones are zero-indexed
    ASSERT( idx < owner->GetBoneArraySize() );
    
    Dualquat curBone;
    if ( armTo.IsBonePosCached( frame ) ) {
        curBone = armTo.GetCachedPos( frame ).dq;
    } else {
        curBone = CacheBonePos( frame, armFrom, armFrom_keyFrame, armTo, armTo_keyFrame, timeDelta, parent_dq );
    }
    
    owner->PackBoneArray( offset * curBone, idx );
    
    for (uint i = 0; i < armTo.NumChildren(); i++)
        ComputeBonePos( frame, *armFrom.GetChild( i ), armFrom_keyFrame, *armTo.GetChild( i ), armTo_keyFrame, timeDelta, curBone );
}

void ArmatureInfo::PackBoneVBO( const Bone& animBone ) const {

/* Todo: Jesse 08/20/13 After building Test mesh this was changed
     vbo_skel = vec4's in shader
              = vec4(vec3(x,y,z), boneID)
     so the bone vertexes would know which bone they belonged to.
*/

    ASSERT( vbo_skel );
    vbo_skel->Pack(animBone.GetTeePoseHead() );
    vbo_skel->Pack(animBone.GetID() );
    vbo_skel->Pack(animBone.GetTeePoseTail() );
    vbo_skel->Pack(animBone.GetID() );

    for ( std::size_t i = 0; i < animBone.NumChildren(); ++i )
        PackBoneVBO( *animBone.GetChild(i) );
}

float ArmatureInfo::GetKeyFrameDelta( void ) const {
    ASSERT( curAnim );
    
    auto const & rootBone = curAnim->GetRootBone_Ref();
    const BoneKeyFrame *currKeyFrame_p = rootBone.GetKeyFrame(curKeyFrame);
    const BoneKeyFrame *nextKeyFrame_p = rootBone.GetKeyFrame(curKeyFrame+1);
    
    const Uint16 curKeyFrameNum = currKeyFrame_p->GetFrame();
    const Uint16 nextKeyFrameNum = nextKeyFrame_p->GetFrame();
    
    if ( curFrame <= curKeyFrameNum )
        return 0.0f;
    
    const float framesInKey = nextKeyFrameNum - curKeyFrameNum;
    const float framesSinceCurKeyFrame = curFrame - curKeyFrameNum;
    
    const float percentageThroughKey = framesSinceCurKeyFrame / framesInKey;
   
    return percentageThroughKey;
}

bool ArmatureInfo::FindIDByName( const std::string& boneName, Uint8& boneID_out ) const {
    if (!curAnim) {
        ERR("ArmatureInfo::FindIDByName, curAnim doesn't exist for model %s\n", owner->GetOwner()->GetModel()->GetName().c_str());
        return false;
    }

    return curAnim->FindIDByName(boneName, boneID_out);
}

BonePos ArmatureInfo::GetBonePos( const Uint8 _id ) const {
    if ( ! curAnim ) {
        WARN("GetBonePos: not found by id (%u), not on an anim.\n", _id);
        return {};
    }
    
    const auto bone = curAnim->FindBone( _id );
    if ( ! bone ) {
        WARN("GetBonePos: bone with id %u not found\n", _id );
        return {};
    }
    
    return GetBonePos_Helper( *bone );
}

BonePos ArmatureInfo::GetBonePos( const std::string& _name ) const {
    if ( ! curAnim ) {
        WARN("GetBonePos: not found by name (%s), not on an anim.\n", _name.c_str());
        return {};
    }
    
    const auto bone = curAnim->FindBone( _name );
    if ( ! bone ) {
        WARN("GetBonePos: bone \"%s\" not found\n", _name.c_str() );
        return {};
    }
    
    return GetBonePos_Helper( *bone );
}

BonePos ArmatureInfo::GetBonePos_Helper( const Bone& bone ) const {
    // if the animation is doing a blendFrame, it'll return the position of the frame it is blending TO
    
    if ( ! curAnim ) {
        WARN("GetBonePos: bone %s:%u not yet animated\n", bone.GetName().c_str(), bone.GetID()  );
        return {};
    }
    
    Dualquat dq;
    const auto id = bone.GetID();
    const auto frame = GetCurFrame();
    if ( ! bone.GetCachedPos( id, frame, dq ) ) {
        // if this isn't cached yet, iet's try to get the previous frame's position
        if ( frame > 0 )
        if ( ! bone.GetCachedPos( id, frame-1, dq ) ) {
            WARN("GetBonePos: not cached yet (id:%u name:%s)\n", id, bone.GetName().c_str());
            return {};
        }
    }
    
    return {
        bone.GetTeePoseHead() * (offset*dq),
        bone.GetTeePoseTail() * (offset*dq)
    };
}

void ArmatureInfo::SetChannel( const AnimChannelT to ) {
    channel = to;
}

void ArmatureInfo::SetOwner( AnimInfo& _owner ) {
    owner = &_owner;
}

bool ArmatureInfo::IsAnimating( void ) const {
    return isAnimating;
}

void ArmatureInfo::ModelChanged( void ) {
    StopAnimating();
}

bool ArmatureInfo::HasValidAnim( void ) const {
    return curAnim != nullptr;
}

const Dualquat ArmatureInfo::GetOffset( void ) const {
    return offset;
}

bool ArmatureInfo::IsBoneNameInArmature( const std::string& boneName ) const {
    Uint8 not_used;
    return FindIDByName( boneName, not_used );
}
