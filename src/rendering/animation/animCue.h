// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_ANIMATION_ANIMEVENT_H
#define SRC_RENDERING_ANIMATION_ANIMEVENT_H

#include "../../base/main.h"
#include "../vbo.h"

typedef Uint32 AnimEventT_BaseType;
enum class AnimEventT : AnimEventT_BaseType {
    NONE = 0
    , SPECIAL_1 = 1
    , SPECIAL_2
    , SPECIAL_3
    , SPECIAL_4
    , SPECIAL_5
    , SPECIAL_6
    , SPECIAL_7
    , SPECIAL_8
    , SPECIAL_9
    , SPECIAL_10
    , SPECIAL_11
    , SPECIAL_12
    , SPECIAL_13
    , SPECIAL_14
    , SPECIAL_15
    , SPECIAL_16
    , SPECIAL_17
    , SPECIAL_18
    , SPECIAL_19
    , GENERIC_ATTACK_1
    , LAUNCH_PROJECTILE
    , PLAY_SOUND
    , SPAWN_EFFECT
    , DISABLE_TURNING
    , ENABLE_TURNING
    , DISABLE_MOVING
    , ENABLE_MOVING
    , ENABLE_TOUCH_DAMAGE
    , DISABLE_TOUCH_DAMAGE
    , MOVE_SPEED_BOOST
    , CHARGE_FORWARD
    , TELEDASH
    , RENDER_EFFECT
    , FIRE_BULLET
    , ENABLE_TURN_ANIMATION_OVERRIDE
    , DISABLE_TURN_ANIMATION_OVERRIDE
    , ADD_ANIMSTATE
    , REM_ANIMSTATE
    , DIE
    , INTERACT
    , SET_OPACITY
    , SPAWN_ENTITY
    , DISABLE_ATTACHMENT_VISIBILITY
    , STUN
    , WAKE_UP
};

struct AnimCue {
    AnimCue( void );

    bool operator==( const AnimCue& other ) const;
    bool operator>( const AnimCue& other ) const;
    bool operator<( const AnimCue& other ) const;
    bool operator>=( const AnimCue& other ) const;
    bool operator<=( const AnimCue& other ) const;

    bool SetType( const std::string &typeName, const std::vector< std::pair< uint, std::string > >* eventList ); //!< returns false if the type is not recognized

    void Unset( void );

    AnimEventT event;
    Uint32 frame;
    std::vector< std::string > arguments;
};

#endif // SRC_RENDERING_ANIMATION_AnimEventT::H
