// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifndef SRC_RENDERING_ANIMATION_ARMATUREINFO_H_
#define SRC_RENDERING_ANIMATION_ARMATUREINFO_H_

#include "../../base/main.h"
#include "../models/animatedModel.h"
#include "../material.h"
#include "./bonePos.h"

// animDebug things
extern const uint ANIM_FRAMES_PER_SEC;
constexpr const bool animDebug = false;
constexpr const bool animDebugExtra = false;
constexpr const bool animCueDebug = false;
constexpr const bool undoDebug = false;

typedef Uint32 AnimEventT_BaseType;
enum class AnimEventT : AnimEventT_BaseType;

class Entity;

class Bone;
struct AnimCue;
class AnimInfo;
template< typename TYPE >
class VBO;

typedef Uint32 AnimOptT_BaseType;
enum class AnimOptT : AnimOptT_BaseType;

/*!

ArmatureInfo \n\n

Information needed to draw an animated model

*/

class ArmatureInfo {
    friend class AnimInfo; //todoattorney

public:
    ArmatureInfo( void ); //!< don't call this, use the other ctors instead. this one will abort the program.
    ArmatureInfo( AnimInfo& other, const AnimChannelT _channel );
    ArmatureInfo( const ArmatureInfo& other );
    ~ArmatureInfo( void );

public:
    ArmatureInfo& operator=( const ArmatureInfo& other);

public:
    void PackBoneVBO( void ) const;
    void PackBoneVBO( const Bone& animBone ) const; //!< recursive
    void DrawSkel( const bool draw_animated = true ) const; //!< if draw_animated is false, skel will draw as tee pose

    bool IsAnimating( void ) const;
    void StartAnimating( void );
    void StopAnimating( void );
    void ModelChanged( void );
    std::string GetCurAnimName( void ) const;
    bool IsAnimDone( void ) const;
    void UpdateVBO( void );
    void ProcessCues( Entity& ent, const uint curFrame );
    bool FindIDByName( const std::string& boneName, Uint8& boneID_out ) const;
    bool IsBoneNameInArmature( const std::string& boneName ) const;
    Uint32 IsBlendingFrames( void ) const { return curBlendFrame > 0; }
    void StopBlendingFrames( void ) { curBlendFrame = 0; }

private:
    bool ComputeCurFrame( const bool noAnimPause = false, const float animSpeed = 1.0f ); //!< returns false when there is no curFrame anymore
public:
    Uint32 GetCurFrame( void ) const;

    float GetKeyFrameDelta( void ) const; //Normalized time between curKeyframe and nextKeyframe

    const ArmatureInfo* GetArmatureAttachedTo( void ) const; //!< if we are an attachment, get the ArmatureInfo for the main body. otherwise nullptr is returned.
    const ModelInfo* GetBaseModelInfo( void ) const; //!< if we are an attachment, get the ModelInfo for the main body. otherwise nullptr is returned.
    
    bool GetArmatureChainOffset( Dualquat& arm_offset_out ) const;
    
    bool ComputeBonePos( const Bone* rootBone, const Uint16 rootFrame );
    
    void ComputeBonePos( const uint frame, const Bone& arm_one, const Uint16 arm_one_keyFrame, const Bone& arm_two, const Uint16 arm_two_keyFrame, const float timeDelta, const Dualquat& parent_dq );
    bool GetCachedPos( const Uint8 boneID, const uint frame, Dualquat& dq_out ) const;
    Dualquat CacheBonePos( const uint frame, const Bone& arm_one, const Uint16 arm_one_keyFrame, const Bone& arm_two, const Uint16 arm_two_keyFrame, const float timeDelta, const Dualquat& parent_dq );
    
    void BlendBonePos( const uint frame, const Bone& armFrom, const Bone& armTo, const Uint16 armTo_keyFrame, const float timeDelta, const Dualquat& parent_dq );
    Dualquat GetBlendedBonePos( const uint frame, const Bone& armFrom, const Bone& armTo, const Uint16 armTo_keyFrame, const float timeDelta, const Dualquat& parent_dq );
    
    bool GetAttachmentPos( Vec3f& attachPos_out ) const;

    void SetOwner( AnimInfo& _owner );
    void SetChannel( const AnimChannelT to );

    bool HasValidAnim( void ) const;
    const Armature* GetAnim_NoErr( const std::string& animName, const AnimChannelT _channel ) const;
    const Armature* GetAnim( const std::string& animName, const AnimChannelT _channel ) const;
    bool HasChannel( const AnimChannelT _channel ) const;

    const Armature* GetCurAnim( void ) const { return curAnim; }
    uint GetNumAnimFrames( const std::string& animName, const AnimChannelT _channel ) const;
    std::size_t GetCurKeyFrame( void ) const { return curKeyFrame; }
    
    const AnimInfo* GetOwner( void ) const { return owner; }
    
    const Dualquat GetOffset( void ) const;

private:
    bool ChangeAnim( const AnimOptT _opts, const std::string& anim, const bool randomAnimSelection = true );
    bool BeginAnim( const AnimOptT _opts, const bool animateDuringPause );
    bool ContinueAnim( const AnimOptT _opts, const bool noAnimPause, const float animSpeed );
    void ContinueAttachmentAnims( const AnimOptT _opts, const bool noAnimPause, const float animSpeed );
    
    void SetAnimFrame( const ArmatureInfo& armatureAnimToMatch );
public:
    BonePos GetBonePos( const Uint8 _id ) const;
    BonePos GetBonePos( const std::string& _name ) const;
private:
    BonePos GetBonePos_Helper( const Bone& bone ) const;
    
private:
    Dualquat offset; //!< set every frame by the masterChannel so we can keep torso attached to legs, etc.
    std::size_t curKeyFrame; //!< current keyFrame animation is interpolating FROM.
    Uint32 curFrameStartTime;
    bool isAnimating;
    const Armature* curAnim;
    const Armature* prevAnim; //!< for anim blending
    std::size_t prevAnimFrame; //!< for anim blending
    std::size_t curBlendFrame; //!< counts down to zero
    std::size_t blendToKeyFrame; //!< the keyframe we'll blend to
    VBO<float>* vbo_skel;
    Uint16 curFrame;
    AnimInfo* owner;
    const Link< AnimCue > *cue;
    AnimChannelT channel;
};

#endif // SRC_RENDERING_ANIMATION_ARMATUREINFO_H_
