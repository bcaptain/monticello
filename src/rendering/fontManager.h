// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_RENDERING_FONTMANAGER_H_
#define SRC_RENDERING_FONTMANAGER_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../files/manifest.h"
#include "../files/assetManager.h"

class Font;

class FontManager : public AssetManager< AssetData<Font>, Font > {
public:
    FontManager( void );

private:
    bool Load( File& opened_font_file ) override;
    using AssetManager< AssetData<Font>,Font >::Load;

private:
    static bool instantiated;
};

extern FontManager fontManager;

#endif  // SRC_RENDERING_FONTMANAGER_H_
