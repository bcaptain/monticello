// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_FONT_H_
#define SRC_RENDERING_FONT_H_

extern const float FONT_DEFAULT_SIZE;
extern const float FONT_LETTER_TEXTURE_SIZE;
extern const int FONT_LETTER_ROWS;
extern const int FONT_LETTER_COLS;
extern const float FONT_LETTER_TEX_COORD_INTERVAL;

#include "../base/main.h"

class Widget;
class Material;
class Texture;
class FontManager;

class Font {
    friend class FontManager; // initializes this class. //todoattorney

public:
    Font( void );
    explicit Font( const char *nam );
    explicit Font( const std::string& nam );
    ~Font( void ) = default;
    Font( const Font& other );

public:
    Font& operator=( const Font& other );

public:
    static void    AssertValid( const std::shared_ptr< const Font >& fnt, const char* fontname );

    void    SetName( const char *nam );
    void    SetName( const std::string& nam );

    std::string    GetName( void ) const;

    static Vec2f GetCharCoord( const unsigned short int i ); //!< get first coordinate for a letter

    std::shared_ptr< const Material > GetMaterial( void ) const;
    void SetMaterial( const std::shared_ptr< const Material >& mtl );

    static void Init( void );

private:
    std::string name;
    std::shared_ptr< const Material > material;
    static Vec2f letter[256];
};

class FontInfo {
public:
    FontInfo( void );
    FontInfo( const float sz, const Vec2f& position, const char* fnt, const Vec3f& col, const float opac = 1 );
    FontInfo( const FontInfo& other );

public:

    FontInfo& operator=( const FontInfo& other );
public:
    float size_ui_px;
    Vec2f pos;
    std::string fontname;
    Vec3f color;
    float opacity;
};

#endif  // SRC_RENDERING_FONT_H_
