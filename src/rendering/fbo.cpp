// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./renderer.h"
#include "./fbo.h"

FBO::FBO( const FBO& other )
    : Material( other )
    , fbo_id(other.fbo_id)
    , tex_id(other.tex_id)
    , depth_id(other.depth_id)
    , width(other.width)
    , height(other.height)
{ }

FBO& FBO::operator=( const FBO& other ) {
    Material::operator=( other );

    fbo_id = other.fbo_id;
    tex_id = other.tex_id;
    depth_id = other.depth_id;
    width = other.width;
    height = other.height;

    return *this;
}

FBO::~FBO( void ) {
    if ( fbo_id.use_count() > 1 )
        return;

    ASSERT( fbo_id > 0 );
    glDeleteFramebuffers(1, fbo_id.get());
    fbo_id = 0;

    if ( depth_id > 0 ) {
        glDeleteRenderbuffers(1, &depth_id);
        depth_id = 0;
    }
}

FBO::FBO( const int xwidth, const int yheight )
    : Material()
    , fbo_id(std::make_shared<uint>(0))
    , tex_id(std::make_shared<uint>(0))
    , depth_id(0)
    , width( xwidth )
    , height( yheight )
{
    // ** create the frame buffer
    glGenFramebuffers(1, fbo_id.get());
    renderer->CheckGLError( "calling glGenFramebuffers for FBO" );

    glBindFramebuffer(GL_FRAMEBUFFER, *fbo_id);
    renderer->CheckGLError( "calling glBindFramebuffer for FBO" );

        GLenum DrawBuffers[1] = {GL_COLOR_ATTACHMENT0};
        const GLsizei drawBufferSize = 1;
        glDrawBuffers(drawBufferSize, DrawBuffers);
        renderer->CheckGLError( "calling glDrawBuffers for FBO" );

        *tex_id = CreateTexBuffer();
        depth_id = CreateDepthBuffer();

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    if ( glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE ) {
        DIE("Unable to create FBO\n");
    }

    diffuse = std::make_shared<const Texture>(
        globalVals.GetUInt( gval_v_width  ), globalVals.GetUInt( gval_v_height )
        , tex_id
        , GL_RGBA
        , TEX_NORMAL
        , false
        , glDeleteRenderbuffers
    );
    ASSERT( diffuse );

    name = "fbo_material";
}

uint FBO::CreateTexBuffer( void ) const {
    uint id=0;
    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA, width, height, 0,GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    //in OpenGL Version 3.2 or later, use this line instead:
    // glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, *id, 0);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, id, 0);

    renderer->CheckGLError( "creating texture buffer for FBO" );
    return id;
}


uint FBO::CreateDepthBuffer( void ) const {
    uint id=0;
    glGenRenderbuffers(1, &id);
    glBindRenderbuffer(GL_RENDERBUFFER, id);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, id);

    renderer->CheckGLError( "creating depth buffer for FBO" );
    return id;
}

void FBO::Unbind( void ) const {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void FBO::Bind( void ) const {
    if ( fbo_id <= 0 ) {
        DIE("Could not bind FBO, does not exist.\n");
        return;
    }

    glBindFramebuffer(GL_FRAMEBUFFER, *fbo_id);
    renderer->CheckGLError( "binding FBO" );

    if ( tex_id > 0 ) {
        glBindTexture(GL_TEXTURE_2D, *tex_id);
        renderer->CheckGLError( "binding texture buffer for FBO" );
    }

    if ( depth_id > 0 ) {
        glBindRenderbuffer(GL_RENDERBUFFER, depth_id);
        renderer->CheckGLError( "binding depth buffer for FBO" );
    }

//    glViewport(0,0,width,height);
    renderer->CheckGLError( "setting viewport for FBO" );
}

int FBO::GetWidth( void ) const {
    return width;
}

int FBO::GetHeight( void ) const {
    return height;
}
