// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "./font.h"
#include "./fontManager.h"
#include "../filesystem.h" // for OverrideWarning()
#include "./materialManager.h"

bool FontManager::instantiated(false);

FontManager fontManager;

FontManager::FontManager( void )
    : AssetManager( "Font" )
{
    if ( instantiated ) {
        DIE("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

bool FontManager::Load( File& opened_file ) {
    if ( ! opened_file.IsOpen() ) {
        ERR("Font file not opened: %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    AssetData< Font >* manifest_s = manifest.Get( name.c_str() );
    if ( !manifest_s ) {
        DIE("Couldn't load font file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }
    
    auto font = std::make_shared< Font >();

    font->SetName( name );
    font->SetMaterial( materialManager.Get( name.c_str() ) );
 
    ParserText parser( opened_file );
    parser.SetCommentIndicator('#');
    
    std::string entry, parm;

    while ( parser.ReadWord( entry ) ) {

        if ( entry == "mtl" ) {
            parser.ReadWord( parm );

            // don't load a texture twice
            auto mtl = materialManager.Get( parm.c_str() );

            if ( !mtl ) {
                ERR("Material texture does not exist: %s for font material: %s.\n", parm, opened_file.GetFilePath() );
                parser.SkipToNextLine();
            } else {
                font->SetMaterial( mtl );
            }
            
            continue;
        }
        
        ERR("Malformed font file %s, unknown entry: %s\n", opened_file.GetFilePath(), entry );
        parser.SkipToNextLine();
    }

    auto asset_existed = manifest_s->data != nullptr;
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );
        
    manifest_s->data = font;

    CMSG_LOG("Loaded font: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );

    return true;
}
