// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_RENDERER_H_
#define SRC_RENDERING_RENDERER_H_

class Renderer;
extern Renderer *renderer;

class Shaders;
extern Shaders *shaders;

class Widget;
class Sprite;
class Renderer;

#include "../base/main.h"
#include "./camera.h"
#include "../particles/particles.h"
#include "../particles/effect.h"
#include "./shaders.h"
#include "./textureManager.h"
#include "./material.h"
#include "./cubeMap.h"
#include "../entities/light/allLights.h"
#include "../entities/projector.h"

typedef int EffectT_BaseType;
enum class EffectT : EffectT_BaseType {
    NONE = 0
    , SHAKE = 1
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( EffectT, EffectT_BaseType )

struct PointLightInfo {
    PointLightInfo() = delete;
    PointLightInfo( const std::weak_ptr< PointLight > light_pointer ) : light( light_pointer ) {}
    std::weak_ptr< PointLight > light;
};

struct DirLightInfo {
    DirLightInfo() = delete;
    DirLightInfo( const std::weak_ptr< DirLight > light_pointer ) : light( light_pointer ) {}
    std::weak_ptr< DirLight > light;
};

struct ProjectorInfo {
    ProjectorInfo() = delete;
    ProjectorInfo( const std::weak_ptr< Projector > projector_pointer ) : projector( projector_pointer ) {}
    std::weak_ptr< Projector > projector;
};

class Renderer {

    struct RenderEffectData {
        RenderEffectData( void ) : effect(EffectT::NONE), startTime(0), duration(0), intensity(0) { }
        RenderEffectData( const EffectT _effect, const uint _stopTime, uint const _duration, const float _intensity ) : effect(_effect), startTime(_stopTime), duration(_duration), intensity(_intensity) { }
        EffectT effect;
        uint startTime;
        uint duration;
        float intensity;
    };

public:
    Renderer( void );
    ~Renderer( void );
    Renderer( const Renderer& other ) = delete;

public:
    Renderer& operator=( const Renderer& other ) = delete;

public:
    int Init( void );

private:
    void InitProjectionMatrix( void );

public:
    bool CheckGLError( const char* message ); //!< returns true if an error occured
    void UpdateWindowSize( void );
    void KillGLWindow( void );
    void ToggleFullScreen( void );
    void SetFullScreen( const bool whether );
    Vec3f GetResolution( void ) const; //!< Z will be zero

    const GLint* GetViewPort( void ) const; //!< viewport is an int[4]

    const Mat3f& GetMousePanMatrix( void ) const;
    void ResetMousePanMatrix( void );

    void PrepareModelViewForScene( void );
    void DrawGLScene( const bool clear_screen = true );
    void UpdateFrame( void );
    void DebugThink( void );
    
    template< typename TYPE >
    void DrawSpriteList( const TYPE& splist );

    EffectT GetEffectID( const std::string& effectName ) const;
    void ActivateEffect( const std::string& effectName, const uint duration, const float _intensity );
    void ActivateEffect( const EffectT, const uint duration, const float _intensity );
    void DeactivateEffect( const EffectT );
    void ProcessRenderEffects( void );
    void EvalShake( const float shake_amount ); //!< shake_amount should be from 0.0 to 1.0

    void SetupScreenForUI( void );
    void ScaleScreenTo800x600( void );

    void AddLight( Entity& light );
    void AddProjector( Entity& projector );
    void RemoveProjector( const std::shared_ptr< Entity >& projector );

    void SendShaderData_Lights( void );
    
private:
    void DrawWorldUI( void );
    void DrawHUDUI( void );

public:
    void PushMatrixMV( void );
    void PopMatrixMV( void );
    Mat4f& ModelView( void );
    Mat4f& Projection( void );
    Mat4f& ViewMatrix( void );
    Mat4f& InverseViewMatrix( void );

    void PushMatrixPJ( void );
    void PopMatrixPJ( void );

    #ifdef MONTICELLO_EDITOR
        void DrawLine_Debug( const Vec3f& start, const Vec3f& end, const Color4f& color_start, const Color4f& color_end ); //!< will draw a line until the next game frame.
        void DrawLine_Debug( const Line3f& line, const Color4f& color_start, const Color4f& color_end ); //!< will draw a line until the next game frame.
        void DrawPoint_Debug( const Vec3f& at, const Color4f& color, const float size ); //!< will draw a point until the next game frame.
        void DrawTri_Debug( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& color ); //!< will draw a tri until the next game frame.
        void DrawTri_Debug( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& colorA, const Color4f& colorB, const Color4f& colorC ); //!< will draw a tri until the next game frame.
        void DrawPoly_Debug( const std::vector< Vec3f >& ordered_verts, const Color4f& color ); //!< will draw a tri for one game frame.

        void DrawLine( const Vec3f& start, const Vec3f& end, const Color4f& color_start, const Color4f& color_end ); //!< will draw a line for one real frame.
        void DrawLine( const Line3f& line, const Color4f& color_start, const Color4f& color_end ); //!< will draw a line for one real frame.
        void DrawPoint( const Vec3f& at, const Color4f& color, const float size ); //!< will draw a point for one real frame.'
        void DrawSphere( const uint num_subdivides, const float radius, const Vec3f& origin);
        void DrawTri( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& color ); //!< will draw a tri for one real frame.
        void DrawTri( const Vec3f& A, const Vec3f& B, const Vec3f& C, const Color4f& colorA, const Color4f& colorB, const Color4f& colorC ); //!< will draw a tri for one real frame.
        void DrawPoly( const std::vector< Vec3f >& ordered_verts, const Color4f& color ); //!< will draw a tri for one real frame.
    #endif // MONTICELLO_EDITOR
private:
    void DrawFramelyLines( VBO<float>* vbo_shape, VBO<float>* vbo_colors ); //!< Draws all lines sent to DrawLine, and then removes them for the next frame
    void DrawFramelyPoints( VBO<float>* vbo_shape, VBO<float>* vbo_sizes, VBO<float>* vbo_colors ); //!< Draws all points sent to DrawPoint, and then removes them for the next frame
    void DrawFramelyTris( VBO<float>* vbo_shape, VBO<float>* vbo_uv ); //!< Draws the all tris sent to DrawTri, and then removes them for the next frame
    void DrawLightEmissions( void ); //!< Draws all light emissions, radius circles for pointlights, cones for spotlights, etc.
    
    template< typename LIGHT_TYPE >
    void DrawLightEmissions( std::vector< LIGHT_TYPE >& light_list );
    
    void DrawGlobalAxis( void ); //!< Draws a local representation of the global axis (for orientation)
public:
    void ClearFramelyShapes( void ); //!< called once before a game render (not if game is paused)
    void ClearEditorFramelyShapes( void ); //!< called once before a game render (not if game is paused)

public:
    void DrawBoxOutLine( const Box3D& box, const Color4f& color ); //!< calls DrawLine() for each edge of the box

private:
    void DrawCompass( void );
    void DrawFPS( void );
    void DrawEditor( void );
    void DrawOpaqueThings( void );
    void DrawTransparentThings( void );
    void DrawDebugMode( void );

public:
    void DrawEntity( Entity& ent ); //!< translated to entity's origin and draws it
    void DrawSprite( Sprite& sprite ); //!< translated to sprite's origin and draws it
    Vec3f GetCrosshairOrigin( void ) const;

private:
    OpenGLInfo glinfo;
    ViewFrustum view;
    ModelInfo compass;
    std::vector< RenderEffectData > RenderEffectData;
    std::vector< Mat4f > modelView; //!< matrix stack, last element is "top" of the stack
    std::vector< Mat4f > projection; //!< matrix stack, last element is "top" of the stack
    Mat4f viewMatrix; //!< the last view matrix that was rendered
    Mat4f inverseViewMatrix;
    GLint viewport[4]; //!< float array is x,y,width,height
    VBO<float>* vbo_framely_lines;
    VBO<float>* vbo_framely_lines_colors;
    VBO<float>* vbo_framely_points;
    VBO<float>* vbo_framely_points_sizes;
    VBO<float>* vbo_framely_points_colors;
    VBO<float>* vbo_framely_tris;
    VBO<float>* vbo_framely_tris_colors;

    VBO<float>* vbo_debug_framely_lines;
    VBO<float>* vbo_debug_framely_lines_colors;
    VBO<float>* vbo_debug_framely_points;
    VBO<float>* vbo_debug_framely_points_sizes;
    VBO<float>* vbo_debug_framely_points_colors;
    VBO<float>* vbo_debug_framely_tris;
    VBO<float>* vbo_debug_framely_tris_colors;

    VBO<float>* vbo_fps;
    VBO<float>* vbo_fps_uv;
    uint frame;
    EffectT effects;
public:
    Camera camera;
    Shaders shaders;
    CubeMap skybox;
    Mat4f modelView_atfullzoom;
    Mat4f modelView_atnozoom;
    std::vector< DirLightInfo > dirLights; //!< a list of lights in the entityTree. They are added upon spawn and referenced here when drawing all models
    std::vector< PointLightInfo > pointLights; //!< a list of lights in the entityTree. They are added upon spawn and referenced here when drawing all models
    std::vector< ProjectorInfo > projectors; //!< a list of projectors in the entityTree. They are added upon spawn and referenced here when drawing all models
    //std::vector< std::weak_ptr< SpotLight > > spotLights; //TODO
private:
    Vec3f crosshair_origin;
public:
    float backgroundDarkness;
};

template< typename LIGHT_TYPE >
void Renderer::DrawLightEmissions( std::vector< LIGHT_TYPE >& lights ) {
    for ( uint i=0; i<lights.size(); ++i ) {
        const auto & light = lights[i].light.lock();
        if ( !light ) {
            PopSwap( lights, i-- );
            continue;
        }
        light->DrawEmission();
    }
}

#endif  // SRC_RENDERING_RENDERER_H_
