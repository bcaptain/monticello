// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./camera.h"
#include "./renderer.h"
#include "./../game.h"
#include "../input/input.h"
#include "../misc.h"

const float CAMERA_DEFAULT_ZOOM = -13.0f;
const float CAMERA_DEFAULT_TILT = 310.0f; 
const float CAMERA_DEFAULT_ROT = 0;
const float CAMERA_DEFAULT_RADIUS = 0.5f;
const Vec3f CAMERA_DEFAULT_FOCALPOINT_OFFSET(0.0f,0.0f,0.0f);

Camera::Camera( void )
    : mouse_pan_matrix()
    , tilt_mat()
    , focus_origin()
    , follow_entity()
    , curZoom()
    {

}

void Camera::Update( void ) {
    if ( !globalVals.GetBool( gval_cam_tether, true ) )
        return;
        
    std::shared_ptr< const Entity > ent = follow_entity.lock();
    if ( !ent )
        return;
    
    const float tetherSlack = globalVals.GetFloat( gval_cam_tetherSlack );
    
    const Vec3f entOrigin = ent->GetOrigin();
    
    if ( Maths::Approxf( tetherSlack, 0 ) ) {
        LookAt( entOrigin );
        return;
    }
    
    const Vec3f tetherVec = (-focus_origin) - entOrigin;
    const float tetherDist = tetherVec.Len();
    
    if ( tetherDist <= tetherSlack )
        return;
    
    const Vec3f tetherDir = tetherVec / tetherDist;
    
    const Vec3f newPos = entOrigin + tetherDir*tetherSlack;
    LookAt( newPos );
}

void Camera::Reset( void ) {
    if ( globalVals.GetBool( gval_cam_noReset ) )
        return;
        
    globalVals.SetFloat( gval_cam_zoom, CAMERA_DEFAULT_ZOOM );
    globalVals.SetFloat( gval_cam_rot, CAMERA_DEFAULT_ROT );
    mouse_pan_matrix.LoadIdentity();
    globalVals.SetFloat( gval_cam_tilt, CAMERA_DEFAULT_TILT );
    globalVals.SetVec3f( gval_cam_focalOffset, CAMERA_DEFAULT_FOCALPOINT_OFFSET );
    tilt_mat.LoadIdentity();
}

Vec3f Camera::NudgeFocusOrigin( const Vec3f &nudge ) {
    return focus_origin += nudge * mouse_pan_matrix;
}

void Camera::NudgeRot( const float nudge ) {
    float rot = globalVals.GetFloat( gval_cam_rot, CAMERA_DEFAULT_ROT );
    rot = fmodf(rot+nudge, 360);
    globalVals.SetFloat( gval_cam_rot, rot );
}

void Camera::Unfollow( const Entity& ent ) {
    if ( const auto & following = follow_entity.lock() )
        if ( following->GetIndex() == ent.GetIndex() )
            follow_entity.reset();
}

void Camera::LookAt( const Vec3f &tr ) {
    //mouse_pan_matrix.Multiply( tr, focus_origin );
    focus_origin.x = -tr.x;
    focus_origin.y = -tr.y;
    focus_origin.z = -tr.z;
}

bool Camera::LookAtGroup( const LinkList< std::weak_ptr< Entity> > &group ) {
    if ( group.Num() < 1 )
        return false;
 
    Vec3f point;
    GetGroupOrigin( group, point );

    LookAt( point );

    return true;
}

Vec3f Camera::GetOrigin( const Mat4f& modelView, const Vec3f offset ) const {
    Mat3f rot_invert( modelView );
    rot_invert.Transpose();
    const Vec3f tran_invert( -(modelView.GetTranslation() - offset) );
    return tran_invert * rot_invert;
}

Vec3f Camera::GetOrigin( const Vec3f offset ) const {
    return GetOrigin( renderer->ModelView(), offset );
}

Vec3f Camera::GetOriginAtMaximumZoom( void ) const {
    return GetOrigin( renderer->modelView_atfullzoom );
}

Vec3f Camera::GetOriginAtNoZoom( const Vec3f offset ) const {
    return GetOrigin( renderer->modelView_atnozoom, offset );
}

void Camera::UnsetFollowEntity( const Entity* ent ) {
    if ( ! ent )
        return;
    
    if ( auto e = follow_entity.lock() )
        if ( e->GetIndex() == ent->GetIndex() )
            UnsetFollowEntity();
}

void Camera::Rotate( const float amt ) {
    float rot = globalVals.GetFloat( gval_cam_rot, CAMERA_DEFAULT_ROT );
    rot += amt;
    SetRotation( Maths::NormalizeAngle( rot ) );
}

void Camera::SetRotation( const float val ) {
    globalVals.SetFloat( gval_cam_rot, Maths::NormalizeAngle( val ) );
}

void Camera::Tilt( const float amt ) {
    float tilt = globalVals.GetFloat( gval_cam_tilt, CAMERA_DEFAULT_TILT );
    tilt += amt;
    SetTilt( Maths::NormalizeAngle( tilt ) );
}

void Camera::SetTilt( const float val ) {
    float wrap = Maths::NormalizeAngle( val );

    // clamp tilt so that camera doesn't flip upside down

    const float degreeLimit = 0;
    const float maxAngle = 360-degreeLimit;
    const float minAngle = 180+degreeLimit;

    if ( wrap < minAngle && wrap >= 90 ) {
        wrap = minAngle;
    } else if ( wrap < 90 || wrap >= maxAngle ) {
        wrap = maxAngle;
    }

    globalVals.SetFloat( gval_cam_tilt, wrap );
}

void Camera::NudgeZoom( const float nudge ) {
    float zoom = globalVals.GetFloat( gval_cam_zoom, CAMERA_DEFAULT_ZOOM );
    zoom += nudge;
    globalVals.SetFloat( gval_cam_zoom, zoom );
}


void Camera::SetZoom( const float to ) {
    globalVals.SetFloat( gval_cam_zoom, to );
}

float Camera::GetRot( void ) const {
    return globalVals.GetFloat( gval_cam_rot, CAMERA_DEFAULT_ROT );
}

float Camera::GetTilt( void ) const {
    return globalVals.GetFloat( gval_cam_tilt, CAMERA_DEFAULT_TILT );
}

float Camera::GetZoom( void ) const {
    return globalVals.GetFloat( gval_cam_zoom, CAMERA_DEFAULT_ZOOM );
}

float Camera::GetCurZoom( void ) const {
    if ( globalVals.GetBool( gval_cam_collision ) ) {
        return curZoom;
    }

    return GetZoom();
}

#include "../commandsys.h"

void Camera::FadeViewBlockingEntities( void ) {
    if ( globalVals.GetBool( gval_d_noFade ) )
        return;

    const Vec3f cam_focus_origin( GetOriginAtNoZoom() );

    // ** create a capsle from camera to the camera's focus point
    IntersectData coldata;
    coldata.desired_type = TypeID_Module;
    coldata.opts |= MATCH_VISIBLE_ONLY;
    coldata.opts |= MATCH_ENTS_WITH_MODELS_ONLY;
    coldata.opts |= MATCH_TEST_ONLY_BOUNDS;
    
    const auto lenVec = GetOrigin() - cam_focus_origin;
    const auto radius = globalVals.GetFloat("hash_cam_radius", CAMERA_DEFAULT_RADIUS);
    const auto primitive = Primitive_Capsule( Capsule( cam_focus_origin, lenVec.GetNormalized(), radius, lenVec.Len() ) );

    // ** debug code
    if ( globalVals.GetBool(gval_cam_drawCap) ) {
        auto const shape = primitive.GetShape();
        globalVals.SetBool(gval_cam_drawCap,0);
        // display the capsule in real space
        CommandSys::delshape({"delshape", "cameracapsule"});
        CommandSys::capsule(
            {
                "capsule",
                "cameracapsule",
                 String::VecToString(shape.origin),
                 String::VecToString(shape.normal),
                 std::to_string(shape.radius),
                 std::to_string(shape.length),
            }
        );
    }

    // ** get all the modules that collide with the capsule
    std::vector< CollisionItem > items;
    game->entTree->GetEntityTraceCollisions( coldata, primitive, &items );
    
    constexpr const bool debug_viewblockers = false;

    if constexpr ( debug_viewblockers ) {
        CMSG("\n");
    }
    
    for ( auto const & item : items ) {
        if constexpr ( debug_viewblockers ) {
            if ( auto ent = item.ent.lock() ) {
                CMSG("Fading viewblocking ent: %s\n", ent->GetIdentifier().c_str() );
            }
        }
        
        game->entTree->MakeEntTransparentForNextFrame( item.ent );
    }
}

void Camera::MoveCameraInFrontOfViewblockingEntities( void ) {
    const float zoom = GetZoom();
    curZoom = zoom;

    const Vec3f cam_focus_origin( GetOriginAtNoZoom() );
    const Vec3f camOriginAtMaxZoon = GetOriginAtMaximumZoom();

    const auto sphereRadius = globalVals.GetFloat( gval_cam_radius , CAMERA_DEFAULT_RADIUS);
    auto const primitive = Primitive_Sphere( Sphere( cam_focus_origin, sphereRadius ) );
    
    SweepData coldata;
    coldata.vel = camOriginAtMaxZoon - primitive.GetOrigin();
    coldata.desired_type = TypeID_Module;
    coldata.opts |= MATCH_NEAREST;
    coldata.opts |= MATCH_VISIBLE_ONLY;
    coldata.opts |= MATCH_ENTS_WITH_MODELS_ONLY;
    //coldata.opts |= MATCH_TEST_ONLY_BOUNDS;

    if ( globalVals.GetBool(gval_cam_drawSphere) ) {
        globalVals.SetBool(gval_cam_drawSphere,0);
        // display the sphere in real space
        CommandSys::delshape({"delshape", "camerasphere"});
        CommandSys::sphere(
            {
                "sphere",
                "camerasphere",
                String::VecToString(camOriginAtMaxZoon),
                std::to_string(primitive.GetRadius() ),
            }
        );
    }

    // ** debug code
    if ( globalVals.GetBool(gval_cam_drawCap) ) {
        globalVals.SetBool(gval_cam_drawCap,0);
        // display the capsule in real space
        CommandSys::delshape({"delshape", "cameracapsule"});
        CommandSys::capsule(
            {
                "capsule",
                "cameracapsule",
                String::VecToString(primitive.GetOrigin()),
                String::VecToString(coldata.vel.GetNormalized()),
                std::to_string(primitive.GetRadius() ),
                std::to_string(coldata.vel.Len()),
            }
        );
    }

    // ** get all the modules that collide with the sphere
    std::vector< CollisionItem > entList;
    CollisionReport3D report( CollisionCalcsT::Intersection );
    report.all_collisions_out = &entList;

    if ( game->entTree->GetEntitySweepCollisions( report, coldata, primitive ) ) {
        ASSERT(entList.size() > 0);
        //CMSG("Positioning camera for viewblocking ent: %s(%i)\n", entList.GetFirst()->Data()->GetEntName().c_str(), entList.GetFirst()->Data()->GetIndex() ); // debug
        curZoom = zoom * report.time;
    }
}

void Camera::Think( void ) {
    //FadeViewBlockingEntities();

    if ( globalVals.GetBool( gval_cam_collision ) ) {
        MoveCameraInFrontOfViewblockingEntities();
    }
}

Vec3f Camera::GetFocusOriginOffset( void ) const {
    return globalVals.GetVec3f( gval_cam_focalOffset, CAMERA_DEFAULT_FOCALPOINT_OFFSET );
}

Vec3f Camera::GetDir( void ) const {
    return Vec3f::GetNormalized( GetOriginAtNoZoom() - GetOriginAtMaximumZoom() );
}
const Mat3f& Camera::GetMousePanMatrix( void ) const {
    return mouse_pan_matrix;
}

void Camera::SetMousePanMatrix( const Mat3f& to ) {
    mouse_pan_matrix = to;
}

std::shared_ptr< const Entity > Camera::GetFollowEntity( void ) const {
    return follow_entity.lock();
}

Vec3f Camera::GetFocusOrigin( void ) const {
    return -focus_origin;
}

void Camera::SetFollowEntity( const std::weak_ptr< const Entity >& ent ) {
    follow_entity=ent;
}

void Camera::UnsetFollowEntity( void ) {
    follow_entity.reset();
}
