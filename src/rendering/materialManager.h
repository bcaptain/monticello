// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_RENDERING_MATERIALMANAGER_H_
#define SRC_RENDERING_MATERIALMANAGER_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../files/assetManager.h"

class Material;

/*!

MaterialManager \n\n

holds all the materials for the game

**/

class MaterialManager : public AssetManager< AssetData<Material>, Material > {
public:
    MaterialManager( void );

public:
    std::shared_ptr< const Material > Get( const char* name ) override;

private:
    bool Load( File& opened_model_file ) override;
    using AssetManager< AssetData<Material>,Material >::Load;

private:
    static bool instantiated;
};

extern MaterialManager materialManager;

#endif  // SRC_RENDERING_MATERIALMANAGER_H_
