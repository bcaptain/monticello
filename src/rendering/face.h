// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_FACET_H_
#define SRC_CLIB_FACET_H_

#include "./clib/src/warnings.h"
#include "./vertex.h"
#include "./material.h"

extern const std::size_t FACE2D_VERT_CW_TOP_LEFT;
extern const std::size_t FACE2D_VERT_CW_TOP_RIGHT;
extern const std::size_t FACE2D_VERT_CW_BOTTOM_RIGHT;
extern const std::size_t FACE2D_VERT_CW_BOTTOM_LEFT;

extern const std::size_t FACE2D_VERT_CCW_TOP_LEFT;
extern const std::size_t FACE2D_VERT_CCW_TOP_RIGHT;
extern const std::size_t FACE2D_VERT_CCW_BOTTOM_RIGHT;
extern const std::size_t FACE2D_VERT_CCW_BOTTOM_LEFT;

// bitwise
extern const char TEXTURE_SANDARD;
extern const char TEXTURE_FLIP_Y;
extern const char TEXTURE_FLIP_X;

template< typename TYPE >
class FaceT {
public:
    FaceT( void );
    FaceT( const FaceT<TYPE>& other );
    ~FaceT( void );

    void Resize( const std::size_t num );
    void BuildFromTexInfo( const std::shared_ptr< const Texture >& texinfo ); //!< center will be at 0,0

    void BuildRect( float width, float height );

    void SetColor( const float x, const float y, const float z );
    void SetColor( const Vec3f& to );
    void SetOpacity( const float to ); //!< sets opacity to all vertices
    float GetOpacity( void ) const; //!< returns first vertice's opacity

    FaceT<TYPE>& operator=( const FaceT<TYPE>& other );
    void SizeMult( const TYPE& by );

    std::size_t NumVert( void ) const;

    static float GetWidth( const FaceT<TYPE>& face );
    static float GetHeight( const FaceT<TYPE>& face );

public:
    TYPE* vert;

private:
    std::size_t numVert;
};

template< typename TYPE >
inline std::size_t FaceT<TYPE>::NumVert( void ) const {
    return numVert;
}

template< typename TYPE >
void FaceT<TYPE>::Resize( const std::size_t newNum ) {
    TYPE* newVert = new TYPE[newNum];

    // copy any old data over
    if ( vert ) {
        numVert = std::min(newNum, numVert);
        while ( numVert > 0 ) {
            --numVert;
            newVert[numVert] = vert[numVert];
        }
    }

    numVert = newNum;
    DELNULLARRAY( vert );
    vert = newVert;
}

template< typename TYPE >
FaceT<TYPE>::~FaceT( void ) {
    DELNULLARRAY( vert );
}

template< typename TYPE >
FaceT<TYPE>& FaceT<TYPE>::operator=( const FaceT<TYPE>& other ) {
    if ( this == &other )
        return *this;

    if ( other.vert ) {
        DELNULLARRAY( vert );
        vert = new Vertex2D[ other.numVert ];

        for ( numVert=0; numVert < other.numVert; ++numVert )
            vert[numVert] = other.vert[numVert];

    } else {
        DELNULLARRAY( vert );
    }

    return *this;
}

template< typename TYPE >
FaceT<TYPE>::FaceT( void )
    : vert(nullptr)
    , numVert(0)
    {
}

template< typename TYPE >
FaceT<TYPE>::FaceT( const FaceT<TYPE>& other )
    : vert(nullptr)
    , numVert(0)
    {
    vert = new VertexT<TYPE>[other.numVert];
    for ( numVert=0; numVert < other.numVert; ++numVert )
        vert[numVert] = other.vert[numVert];
}

template< typename TYPE >
void FaceT<TYPE>::SizeMult( const TYPE& by ) {
    for ( std::size_t i=0; i<numVert; ++i )
        vert[i] *= by;
}

template< typename TYPE >
void FaceT<TYPE>::BuildFromTexInfo( const std::shared_ptr< const Texture >& ti ) {
    if ( !ti )
        return;

    BuildRect( ti->GetWidth(), ti->GetHeight() );
}

template< typename TYPE >
void FaceT<TYPE>::BuildRect( float width, float height ) {
    Resize( 4 );

    // top left screen is at 0,0

    vert[FACE2D_VERT_CW_TOP_RIGHT].co.x = width;
    vert[FACE2D_VERT_CW_BOTTOM_LEFT].co.y = height;
    vert[FACE2D_VERT_CW_BOTTOM_RIGHT].co.y = height;
    vert[FACE2D_VERT_CW_BOTTOM_RIGHT].co.x = width;

    vert[FACE2D_VERT_CW_TOP_LEFT].uv.Set(0,1);
    vert[FACE2D_VERT_CW_TOP_RIGHT].uv.Set(1,1);
    vert[FACE2D_VERT_CW_BOTTOM_RIGHT].uv.Set(1,0);
    vert[FACE2D_VERT_CW_BOTTOM_LEFT].uv.Set(0,0);
}

template< typename TYPE >
void FaceT<TYPE>::SetColor( const float x, const float y, const float z ) {
    for ( std::size_t v=0; v<numVert; ++v )
        vert[v].color.Set(x,y,z);
}

template< typename TYPE >
void FaceT<TYPE>::SetColor( const Vec3f& to ) {
    for ( std::size_t v=0; v<numVert; ++v )
        vert[v].color = to;
}

template< typename TYPE >
void FaceT<TYPE>::SetOpacity( const float to ) {
    for ( std::size_t v=0; v<numVert; ++v )
        vert[v].opacity = to;
}

template< typename TYPE >
float FaceT<TYPE>::GetOpacity( void ) const {
    if ( numVert < 1 )
        return 0.0f;
    return vert[0].opacity;
}

typedef FaceT<Vertex2D> Face2D;

#endif  //__CLIB_FACET
