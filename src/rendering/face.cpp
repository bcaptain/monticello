// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./face.h"

const std::size_t FACE2D_VERT_CW_TOP_LEFT = 0;
const std::size_t FACE2D_VERT_CW_TOP_RIGHT = 1;
const std::size_t FACE2D_VERT_CW_BOTTOM_RIGHT = 2;
const std::size_t FACE2D_VERT_CW_BOTTOM_LEFT = 3;

const std::size_t FACE2D_VERT_CCW_TOP_LEFT = 0;
const std::size_t FACE2D_VERT_CCW_TOP_RIGHT = 3;
const std::size_t FACE2D_VERT_CCW_BOTTOM_RIGHT = 2;
const std::size_t FACE2D_VERT_CCW_BOTTOM_LEFT = 1;

typedef FaceT<Vertex2D> Face2D;

template<>
float Face2D::GetHeight( const Face2D& face ) {
    if ( face.numVert == 0 )
        return 0;

    float _min = face.vert[0].co.y;
    float _max = face.vert[0].co.y;

    for ( std::size_t i=1; i<face.numVert; ++i ) {
        _min = std::fminf(_min,face.vert[i].co.y);
        _max = std::fmaxf(_max,face.vert[i].co.y);
    }

    return fabsf( _max - _min );
}

template<>
float Face2D::GetWidth( const Face2D& face ) {
    if ( face.numVert == 0 )
        return 0;

    float _min = face.vert[0].co.x;
    float _max = face.vert[0].co.x;

    for ( std::size_t i=1; i<face.numVert; ++i ) {
        _min = std::fminf(_min,face.vert[i].co.x);
        _max = std::fmaxf(_max,face.vert[i].co.x);
    }

    return fabsf( _max - _min );
}
