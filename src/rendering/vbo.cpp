// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./vbo.h"
#include "./models/model.h"
#include "./animation/animInfo.h"
#include "./animation/armatureInfo.h"
#include "./models/modelManager.h"
#include "./font.h"
#include "../ui/widget.h"
#include "../math/geometry/3d/box.h"

const std::size_t VBO_DATA_SEGMENT_SIZE = 32; // a default increment amount

template<>
void VBO<float>::PackFace2DVerts( const Face2D& face ) {
    ASSERT( things_per_element == 2 );

    float *dat = PointerForPack( face.NumVert() * 2 );

    for ( std::size_t v=0; v<face.NumVert(); ++v ) {
        dat[v*2] = face.vert[v].co.x;
        dat[v*2+1] = face.vert[v].co.y;
    }
}

template<>
void VBO<float>::PackFace2DTexCoords( const Face2D& face ) {
    ASSERT( things_per_element == 2 );

    float *dat = PointerForPack( face.NumVert() * 2 );

    for ( std::size_t v=0; v<face.NumVert(); ++v ) {
        dat[v*2] = face.vert[v].uv.x;
        dat[v*2+1] = face.vert[v].uv.y;
    }
}

template<>
void VBO<float>::PackFace2DColors( const Face2D& face ) {
    ASSERT( things_per_element == 4 );

    float *dat = PointerForPack( face.NumVert() * 4 );

    for ( std::size_t v=0; v<face.NumVert(); ++v ) {
        dat[v*4] = face.vert[v].color.x;
        dat[v*4+1] = face.vert[v].color.y;
        dat[v*4+2] = face.vert[v].color.z;
        dat[v*4+3] = face.vert[v].opacity;
    }
}

template<>
uint VBO<float>::PackText( VBO<float>& vbo_uv, const char* str, const FontInfo& fi, Vec2f* next_position, const Widget* container ) {
    ASSERT( things_per_element == 2 );
    ASSERT( vbo_uv.things_per_element == 2 );

    Face2D face;
    face.BuildRect( fi.size_ui_px, fi.size_ui_px );

    const Vec2f pos_max(
        container ? container->GetWidth() : 0,
        container ? container->GetHeight() : 0
    );

    Vec2f *pos( next_position == nullptr ? new Vec2f(0.0f,0.0f) : next_position );

    const std::size_t len = String::Len(str);
    std::size_t i;
    uint current_line_width=0;
    uint num_faces_packed = 0;
    uint current_line=1;

    for ( std::size_t c=0; c<len; ++c ) {

        if ( container ) {
            ++current_line_width;

            if ( current_line_width * fi.size_ui_px > pos_max.x ) {
                current_line_width = 1;
                // position down a line
                pos->x = fi.pos.x;
                pos->y += fi.size_ui_px;
                ++current_line;
            }

            if ( current_line * fi.size_ui_px > pos_max.y ) {
                break;
            }
        }

        switch ( str[c] ) {
            case ' ':
                pos->x += fi.size_ui_px;
                break;
            case '\t':
                pos->x += fi.size_ui_px * 4;
                break;
            case '\n':
                // position down a line
                pos->x = fi.pos.x;
                pos->y += fi.size_ui_px;
                current_line_width = 1;
                ++current_line;
                break;
            default:
                // ** set up UV

                face.vert[FACE2D_VERT_CW_BOTTOM_LEFT].uv = Font::GetCharCoord( static_cast< uchar >( str[c] ) );
                face.vert[FACE2D_VERT_CW_BOTTOM_RIGHT].uv = face.vert[FACE2D_VERT_CW_BOTTOM_LEFT].uv;
                face.vert[FACE2D_VERT_CW_TOP_RIGHT].uv = face.vert[FACE2D_VERT_CW_BOTTOM_LEFT].uv;
                face.vert[FACE2D_VERT_CW_TOP_LEFT].uv = face.vert[FACE2D_VERT_CW_BOTTOM_LEFT].uv;

                face.vert[FACE2D_VERT_CW_BOTTOM_RIGHT].uv.x += FONT_LETTER_TEX_COORD_INTERVAL;
                face.vert[FACE2D_VERT_CW_TOP_RIGHT].uv.x += FONT_LETTER_TEX_COORD_INTERVAL;
                face.vert[FACE2D_VERT_CW_TOP_RIGHT].uv.y += FONT_LETTER_TEX_COORD_INTERVAL;
                face.vert[FACE2D_VERT_CW_TOP_LEFT].uv.y += FONT_LETTER_TEX_COORD_INTERVAL;

                // this doesn't actually do anything
//                face.SetColor( fi.color );
  //              face.SetOpacity( fi.opacity );

                for ( i=0; i<4; ++i ) {
                    face.vert[i].co.x += pos->x;
                    face.vert[i].co.y += pos->y;
                }

                PackFace2DVerts( face );
                vbo_uv.PackFace2DTexCoords( face );
                ++num_faces_packed;

                for ( i=0; i<4; ++i ) {
                    face.vert[i].co.x -= pos->x;
                    face.vert[i].co.y -= pos->y;
                }

                pos->x += fi.size_ui_px;


                break;
        }
    }

    if ( next_position == nullptr )
        delete pos;

    return num_faces_packed;
}

template<>
bool VBO<float>::PackNormals( const std::vector<MeshVert>& verts, const float length ) {
    
    for ( auto const& v : verts ) { //packs lines
        Pack( v.co ); //line begin
        Pack( v.co + ( v.normal * length) ); //line end
    }
    
    return true;
}

template<>
inline uint VBO<float>::PackText( VBO<float>& vbo_uv, const char* str, Vec2f* next_position, const Widget* container ) {
    FontInfo fi;
    return PackText( vbo_uv, str, fi, next_position, container );
}

template<>
void VBO<float>::BindToAttrib( const uint attrib ) const {
    ASSERT( finalized );

    glEnableVertexAttribArray( attrib );
    glBindBuffer(GL_ARRAY_BUFFER, buffer_id );
    glVertexAttribPointer( attrib, static_cast< int >( things_per_element ), GL_FLOAT, GL_FALSE, 0, nullptr ); //todo: this should be uint, but GL can't decide
}

void GenerateCircleVerts( const uint num_subdivides, const float radius, std::vector<Vec3f>& verts ) {
    ASSERT( num_subdivides >= 5 );
    
    // create circle on x/y plane
    Vec3f tmp;
    
    for( uint a = 0; a < 360; a += 360 / num_subdivides ) {
        const float rot = a * DEG_TO_RAD_F;
        
        tmp.x = cosf( rot ) * radius;
        tmp.y = sinf( rot ) * radius;
        tmp.z = 0.0f;
        
        verts.emplace_back( tmp );
    }
}

void PackAlignedCircleVBO( const uint num_subdivides, const float radius, VBO<float>& vbo_vert, VBO<float>& vbo_color, const Color4f& color,  const Vec3f& normal ) {
    ASSERT( num_subdivides >= 3 );
    
    if ( normal == Vec3f(0.0f,0.0f,0.0f) ) {
        ERR( "PackAlignedCylinderVBO received a normal of zero length (i.e. no direction)\n" );
        return;
    }

    std::vector<Vec3f> verts;
    GenerateCircleVerts( num_subdivides, radius, verts );
    
    //calculate rotation quat to align this circle vertices to the requested axis
    Vec3f circAxisZ = Vec3f(0.0f,0.0f,1.0f);
    if ( circAxisZ != Maths::Fabsf( normal ) ) { //if target axis is parallel to the original axis, no rotation necessary, cross product will be 0
        Vec3f targetAxis = normal.GetNormalized();
        Vec3f rotAxis = circAxisZ.Cross( targetAxis ).GetNormalized();
        float rotRadians = acosf( circAxisZ.Dot( targetAxis ) );
        
        Quat alignmentQuat( Quat::FromAxis( rotRadians, rotAxis ) );
        
        for( auto& vert : verts ) //apply alignnment to all verts
            vert *= alignmentQuat;
    }
    
    //Pack circle for GL_LINES
    for( std::vector<Vec3f>::size_type i = 0; i < verts.size(); i++ ) {
        vbo_vert.Pack( verts[i] );
        if ( i != verts.size() - 1 ) {
            vbo_vert.Pack( verts[i+1] );
        } else {
            vbo_vert.Pack( verts[0] ); //if at the end, complete the circle
        }
        vbo_color.PackRepeatedly(2, color );
    }
}

void PackVBO_Capsule( const uint num_subdivides, const float radius, const float length, const Vec3f& origin, const Vec3f& normal, VBO<float>& vbo_vert, VBO<float> *vbo_color ) {
    ERR("Not Implemented: PackVBO_Capsule\n"); //toimplement
}

void PackVBO_Cylinder( const uint num_subdivides, const float radius, const float length, const Vec3f& origin, const Vec3f& normal, VBO<float>& vbo_vert, VBO<float> *vbo_color ) {
    const Vec3f cyl_end( origin + normal*length);
    const Vec3f perp_normal( normal.z, -normal.x, normal.y );
    const Vec3f dir1( perp_normal * radius ); // perpendicular to normal
    const Vec3f dir2 = perp_normal.Cross( normal ) * radius; // perpendicular to both dir1 and cyl's centerline
    
    const Vec3f top_color(1,0,0);
    const Vec3f bottom_color(0,1,0);
    const Vec3f front_color(0,0,1);
    
    // start at bottom
    vbo_vert.Pack( origin );
    if ( vbo_color ) {
        vbo_color->Pack( bottom_color ); vbo_color->Pack(1);
    }
    
    for ( uint i=0; i<num_subdivides*2; i+=2 ) {
        const float theta1 = (-0.0f-i) / (num_subdivides-1) * PI_F;
        const float theta2 = (-1.0f-i) / (num_subdivides-1) * PI_F;
        const float theta3 = (-2.0f-i) / (num_subdivides-1) * PI_F;
        
        const float sin_theta1 = sinf(theta1);
        const float sin_theta2 = sinf(theta2);
        const float sin_theta3 = sinf(theta3);
        
        const float cos_theta1 = cosf(theta1);
        const float cos_theta2 = cosf(theta2);
        const float cos_theta3 = cosf(theta3);
        
        // ** zipper up
        vbo_vert.Pack( origin + dir1*sin_theta2 + dir2*cos_theta2 ); // bottom side
        vbo_vert.Pack( origin + dir1*sin_theta1 + dir2*cos_theta1 ); // bottom side
        vbo_vert.Pack( cyl_end + dir1*sin_theta2 + dir2*cos_theta2 ); // top side
        vbo_vert.Pack( cyl_end + dir1*sin_theta1 + dir2*cos_theta1 ); // top side
        vbo_vert.Pack( cyl_end ); // top
        
        // ** zipper back down
        vbo_vert.Pack( cyl_end + dir1*sin_theta3 + dir2*cos_theta3 ); // top side
        vbo_vert.Pack( cyl_end + dir1*sin_theta2 + dir2*cos_theta2 ); // top side
        vbo_vert.Pack( origin + dir1*sin_theta3 + dir2*cos_theta3 );  // bottom side
        vbo_vert.Pack( origin + dir1*sin_theta2 + dir2*cos_theta2 ); // bottom side
        
        // end at bottom
        vbo_vert.Pack( origin ); // end at bottom
        
        if ( vbo_color ) {
            // ** zipper up
            vbo_color->Pack( bottom_color + front_color * fabsf( sin_theta2 ) ); vbo_color->Pack(1);
            vbo_color->Pack( bottom_color + front_color * fabsf( sin_theta1 ) ); vbo_color->Pack(1);
            vbo_color->Pack( top_color + front_color * fabsf( sin_theta2 ) ); vbo_color->Pack(1);
            vbo_color->Pack( top_color + front_color * fabsf( sin_theta1 ) ); vbo_color->Pack(1);
            vbo_color->Pack( top_color ); vbo_color->Pack(1);
            
            // ** zipper back down
            vbo_color->Pack( top_color + front_color * fabsf( sin_theta3 ) ); vbo_color->Pack(1);
            vbo_color->Pack( top_color + front_color * fabsf( sin_theta2 ) ); vbo_color->Pack(1);
            vbo_color->Pack( bottom_color + front_color * fabsf( sin_theta3 ) ); vbo_color->Pack(1);
            vbo_color->Pack( bottom_color + front_color * fabsf( sin_theta2 ) ); vbo_color->Pack(1);
            
            // end at bottom
            vbo_color->Pack( bottom_color ); vbo_color->Pack(1);
        }
    }
}

void PackVBO_Sphere( const uint num_subdivides, const float radius, const Vec3f& origin, VBO<float>& vbo_vert, VBO<float> *vbo_color ) {
    ASSERT( num_subdivides >= 5 );

    if ( vbo_color )
        vbo_color->Clear();
    vbo_vert.Clear();

    // color will be the same as vertex data. this makes a different color for each axis direction

    uint idx=0,idx2=0;
    Vec3f **verts = new Vec3f*[num_subdivides];

    const float alpha = 0.5f; // we would like to see through the sphere somewhat

    for ( idx=0; idx<num_subdivides; ++idx ) {

        // azimuth
        float phi = idx;
        phi /= num_subdivides-1;
        phi *= 2.0f * PI_F;

        verts[idx] = new Vec3f[num_subdivides];

        // draw a circle on the x/y axis of the specified size
        for ( idx2=0; idx2<num_subdivides; ++idx2 ) {

            // elevation
            float theta = idx2;
            theta /= num_subdivides-1;
            theta *= PI_F;

            verts[idx][idx2].z = cosf( theta ) * radius;
            verts[idx][idx2].x = sinf( theta ) * cosf( phi ) * radius;
            verts[idx][idx2].y = sinf( theta ) * sinf( phi ) * radius;
            
            verts[idx][idx2] += origin;
        }
    }
    
    // pack it. zipper down and up the sphere all around
    bool last_down = true;
    for ( idx2=0; idx2<num_subdivides-1; ++idx2 ) { // width/breadth
        // down
        last_down = true;
        for ( idx=0; idx<num_subdivides; ++idx ) {
            // clockwise
            vbo_vert.Pack( verts[idx][idx2+1] );
            vbo_vert.Pack( verts[idx][idx2] );
            if ( vbo_color ) {
                vbo_color->Pack( verts[idx][idx2+1] );
                vbo_color->Pack( alpha );
                vbo_color->Pack( verts[idx][idx2]);
                vbo_color->Pack( alpha );
            }
        }

        // up
        ++idx2;
        if ( idx2 < num_subdivides-1 ) {
            last_down = false;

            idx=num_subdivides;
            while ( idx > 0 ) {
                --idx;
                // clockwise
                vbo_vert.Pack( verts[idx][idx2] );
                vbo_vert.Pack( verts[idx][idx2+1] );
                if ( vbo_color ) {
                    vbo_color->Pack( verts[idx][idx2] );
                    vbo_color->Pack( alpha );
                    vbo_color->Pack( verts[idx][idx2+1] );
                    vbo_color->Pack( alpha );
                }
            }
        }
    }

    // the last zipper up or down connects the ends with the beginnings
    int addend;
    if ( last_down ) {
        idx=num_subdivides-1;
        addend = -1;
    } else {
        idx=0;
        addend = 1;
    }

    // connect 'em
    for ( ; idx < num_subdivides; idx += static_cast< uint >( addend ) ) {
        vbo_vert.Pack( verts[idx][num_subdivides-1] );
        vbo_vert.Pack( verts[idx][0] );
        if ( vbo_color ) {
            vbo_color->Pack( verts[idx][num_subdivides-1] );
            vbo_color->Pack( alpha );
            vbo_color->Pack( verts[idx][0] );
            vbo_color->Pack( alpha );
        }
    }

    for ( idx=0; idx<num_subdivides; ++idx )
        delete [] verts[idx];

    delete [] verts;

    vbo_vert.MoveToVideoCard();
    if ( vbo_color ) {
        vbo_color->MoveToVideoCard();
    }
}

void PackVBO_BoxVerts( const AABox3D& aabox, VBO<float>& vbo_vert, VBO<float> *vbo_color ) {
    PackVBO_BoxVerts( Box3D( aabox ), vbo_vert, vbo_color );
}

void PackVBO_BoxVerts( const Box3D& box, VBO<float>& vbo_vert, VBO<float> *vbo_color ) {
    vbo_vert.Clear();
    
    // pack a triangle strip of the box
    vbo_vert.Pack( box.VertAt( BOX_NEAR_LEFT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_RIGHT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_NEAR_LEFT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_LEFT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_LEFT_TOP_INDEX ) );
    vbo_vert.Pack( box.VertAt( BOX_FAR_RIGHT_TOP_INDEX ) );

    vbo_vert.MoveToVideoCard();
    
    if ( ! vbo_color )
        return;
    
    vbo_color->Clear();
        
    vbo_color->Pack( Color4f(0,1,0) );
    vbo_color->Pack( Color4f(0,0,0) );
    vbo_color->Pack( Color4f(1,1,0) );
    vbo_color->Pack( Color4f(1,0,0) );
    vbo_color->Pack( Color4f(1,0,1) );
    vbo_color->Pack( Color4f(0,0,0) );
    vbo_color->Pack( Color4f(0,0,1) );
    vbo_color->Pack( Color4f(0,1,0) );
    vbo_color->Pack( Color4f(0,1,1) );
    vbo_color->Pack( Color4f(1,1,0) );
    vbo_color->Pack( Color4f(1,1,1) );
    vbo_color->Pack( Color4f(1,0,1) );
    vbo_color->Pack( Color4f(0,1,1) );
    vbo_color->Pack( Color4f(0,0,1) );
    
    vbo_color->MoveToVideoCard();
}

void PackVBO_BoxUVs( VBO<float>& vbo_uv ) {
    const uint array_size = 28;
    const float right=1, left=0, top=1, bottom=0;
    const float array[array_size]{
        left, bottom,
        right, bottom,
        left, bottom,
        right, bottom,
        right, top,
        right, bottom,
        right, top,
        left, bottom,
        left, top,
        left, bottom,
        left, top,
        right, top,
        left, top,
        left, bottom
    };
        
    vbo_uv.PackArray( array_size, array );
    vbo_uv.MoveToVideoCard();
}

bool PackVBO_Model( VBO<float>& vbo_vert, VBO<float>& vbo_uv, VBO<float>& vbo_norm, VBO<float>* vbo_mesh_boneID, VBO<float>* vbo_mesh_boneWeights, const Model& mdl ) {
    bool ret = false;
    
    for ( uint m=0; m<mdl.NumMeshes(); ++m )
        if ( PackVBO_Mesh( vbo_vert, vbo_uv, vbo_norm, vbo_mesh_boneID, vbo_mesh_boneWeights, mdl, m ) )
            ret = true;
    
    if ( ! ret )
        return false;
    
    vbo_vert.MoveToVideoCard();
    vbo_uv.MoveToVideoCard();
    vbo_norm.MoveToVideoCard();
    
    const bool draw_animated = mdl.IsAnimated() && vbo_mesh_boneID && vbo_mesh_boneWeights;

    if ( draw_animated ) {
        vbo_mesh_boneID->MoveToVideoCard();
        vbo_mesh_boneWeights->MoveToVideoCard();
    }
    
    return true;
}

bool PackVBO_Mesh( VBO<float>& vbo_vert, VBO<float>& vbo_uv, VBO<float>& vbo_norm, VBO<float>* vbo_mesh_boneID, VBO<float>* vbo_mesh_boneWeights, const Model& mdl, Uint8 meshID ) {
    const Mesh* mesh = mdl.GetMesh( meshID );
    if ( mesh == nullptr ) {
        ERR("Couldn't pack mesh for model %s, invalid mesh index %i.\n", mdl.GetName(), meshID );
        return false;
    }

    std::size_t numFaces = mesh->faces.size();
    if ( numFaces < 1 ) {
        WARN("No Faces in Mesh #%u for Model: %s\n", meshID, mdl.GetName() );
        return false;
    }

    const Vec2f *tex;
    const ModelFace *face;
    const Vec3f* v;
    const Vec3f* vn;
    const bool draw_animated = mdl.IsAnimated() && vbo_mesh_boneID && vbo_mesh_boneWeights;
    const uchar MAX_GROUP_COUNT_PER_VERTEX = 2;

    float *uv_list=nullptr, *vert_list=nullptr, *norm_list=nullptr, *bone_weight_list=nullptr;
    uchar *bone_id_list=nullptr;
    std::size_t index_alloc=0, faces_read=0;

    while ( numFaces>0 ) {
        --numFaces;
        face = &mesh->faces[numFaces];
        ASSERT( face );
        std::size_t vertNum = face->vert.Num();

        ++faces_read;

        if ( vertNum > index_alloc ) {
            delete[] uv_list;
            delete[] vert_list;
            delete[] norm_list;

            index_alloc = vertNum;
            uv_list = new float[index_alloc*2]();
            vert_list = new float[index_alloc*3]();
            norm_list = new float[index_alloc*3]();

            if ( draw_animated ){
                delete[] bone_id_list;
                delete[] bone_weight_list;
                bone_id_list = new uchar[index_alloc*2]();
                bone_weight_list = new float[index_alloc*2]();
            }
        }

        ASSERT( uv_list && vert_list && norm_list );

        std::size_t vert_packed=0;
        std::size_t uv_packed=0;
        std::size_t norm_packed=0;
        std::size_t bone_id_packed = 0;
        std::size_t bone_weight_packed = 0;

        for ( auto const& vertInfo : face->vert ) {
            v = &mesh->verts[ vertInfo.vertIndex ].co;

            // texture coord
            if ( vertInfo.texIndex ) {
                tex = &mesh->texs[*vertInfo.texIndex];
                if ( tex ) {
                    uv_list[uv_packed++]=tex->x;
                    uv_list[uv_packed++]=tex->y;
                } else {
                    WARN("Did not find UV coords for Model: %s.\n", mdl.GetName().c_str() );
                    uv_packed+=2;
                }
            }

            if ( vertInfo.normIndex ) {
                // vertInfo.normIndex is expected to be the same as vertInfo.vertIndex
                
                uint vertex_normal_index = *vertInfo.normIndex; //todouvbug - this have anything to do with loaded-from-map model uv bug??
                vn = &mesh->verts[*vertInfo.normIndex].normal;
                if ( vn ) {
                    norm_list[norm_packed++]=vn->x;
                    norm_list[norm_packed++]=vn->y;
                    norm_list[norm_packed++]=vn->z;
                }
            }

/*
            // face normal
            if ( face->norm )
                glNormal3f( face->norm->x, face->norm->y, face->norm->z );
*/
            // vertex
            vert_list[vert_packed++]=v->x;
            vert_list[vert_packed++]=v->y;
            vert_list[vert_packed++]=v->z;

            // vertex Bone_ID_and_Weight
            if ( draw_animated ) {

                uchar* bID = mesh->verts[vertInfo.vertIndex].bone_ids;
                float* bWgt = mesh->verts[vertInfo.vertIndex].bone_weights;
                uchar numGroups = mesh->verts[vertInfo.vertIndex].numGroups;

                if ( numGroups > 2 ) {
                    ERR("Vertex %i of face %i of model %s has %i groups. It should have less than 3.\n", vert_packed, faces_read, mdl.GetName().c_str(), numGroups);

                    const AssetData< Model >* data = modelManager.GetManifestData( mdl.GetName().c_str() );
                    if ( !data ) {
                        DIE("Invalid model: %s, vertex %i is in %i groups (which is < 1 or > 2). it is also not listed in the manifest.\n", mdl.GetName().c_str(), vert_packed, numGroups );
                    } else {
                        DIE("Invalid model: %s, vertex %i is in %i groups (which is < 1 or > 2). manifest path is: %s from %s.\n", mdl.GetName().c_str(), vert_packed, numGroups, data->GetPath().c_str(), data->GetContainer().empty() ? "filesystem": data->GetContainer().c_str() );
                    }
                    return false;

                    // this should be enabled if we're not going to DIE() on fatal error.
                    // if ( numGroups > 2 )
                    //     numGroups = 2; // don't write past the bounds of bone_id_list or bone_weight_list
                }

                ASSERT(bone_id_list && bone_weight_list);

                for (uchar group = 0; group < numGroups; group++ ) {
                    bone_id_list[bone_id_packed++] = bID[group];
                    bone_weight_list[bone_weight_packed++] = bWgt[group];
                }

                if (numGroups < MAX_GROUP_COUNT_PER_VERTEX) {
                    uchar groupsToPack = MAX_GROUP_COUNT_PER_VERTEX - numGroups;
                    for (uchar group = 0; group < groupsToPack; group++) {
                        bone_id_list[bone_id_packed++] = 0;
                        bone_weight_list[bone_weight_packed++] = 0.0f;
                    }
                }
            }
        }

        //When cycling through multiple armatures we don't want to pack the VBO twice, because vbo_vert.num goes out of range
        if (!vbo_vert.Finalized()) {
            vbo_vert.PackArray( vert_packed, vert_list );
            vbo_uv.PackArray( uv_packed, uv_list );
            vbo_norm.PackArray( norm_packed, norm_list);
        }

        if ( draw_animated ) {
            ASSERT( vbo_mesh_boneID );
            ASSERT( vbo_mesh_boneWeights );

            for (std::size_t packIdx = 0; packIdx < bone_id_packed; packIdx ++) {
                float boneIDmember = static_cast<float>(bone_id_list[packIdx]);
                vbo_mesh_boneID->Pack(boneIDmember); //todo: GL_UPGRADE, get rid of static_cast for loop when boneID's are converted to uchars (instead of floats).
            }
            vbo_mesh_boneWeights->PackArray(bone_weight_packed, bone_weight_list);
        }
    }

    delete[] uv_list;
    delete[] vert_list;
    delete[] norm_list;
    delete[] bone_id_list;
    delete[] bone_weight_list;

    return true;
}
