// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_RENDERING_SHADERS_H_
#define SRC_RENDERING_SHADERS_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../sdl/sdl.h"
#include "../files/manifest.h"

#define PrecomputeHash_GLProg( name, shaderName, description_unused ) \
    constexpr const std::pair< const char *const, const std::size_t > GLPROG_##name( #shaderName, Hashes::OAT_CONSTEXPR( #shaderName ) );
    
PrecomputeHash_GLProg( MINIMAL, minimal, "" )
PrecomputeHash_GLProg( MINIMAL_2D, minimal2d, "" )
PrecomputeHash_GLProg( GRADIENT, gradient, "gradient color" )
PrecomputeHash_GLProg( MULTITEX, multitex, "blend textures" )
PrecomputeHash_GLProg( TEXTURE, textured, "basic textured" )
PrecomputeHash_GLProg( TEXTURE_2D, textured2d, "" )
PrecomputeHash_GLProg( COLORIZE_TEXTURE, colorizetex, "change all color from a uniform texture to one provided" )
PrecomputeHash_GLProg( COLORIZE_TEXTURE_2D, colorizetex2d, "" )
PrecomputeHash_GLProg( MULTICOLORIZE_TEXTURE, multicolortex, "same as colorizetex" )
PrecomputeHash_GLProg( MULTICOLORIZE_TEXTURE_2D, multicolortex2d, "" )
PrecomputeHash_GLProg( PARTICLE, particle, "" )
PrecomputeHash_GLProg( ANIMATION, animation, "" )
PrecomputeHash_GLProg( ANIM_SKEL, animSkel, "" )
PrecomputeHash_GLProg( ANIM_MESH, animMesh, "" )
PrecomputeHash_GLProg( SPHERE, sphere, "" )
PrecomputeHash_GLProg( LASER, laser, "" )
PrecomputeHash_GLProg( POINTS, points, "" )
PrecomputeHash_GLProg( POINTS_VIEW_DEBUG, pointsViewDebug, "" )
PrecomputeHash_GLProg( SKYBOX, skybox, "" )
PrecomputeHash_GLProg( ANIM_DIR_LIGHT, animDirLight, "" )
PrecomputeHash_GLProg( ANIM_TEX_DIR_LIGHT, animTexDirLight, "" )
PrecomputeHash_GLProg( ANIM_TEX_POINT_LIGHT, animTexPointLight, "" )
PrecomputeHash_GLProg( ANIM_TEX_POINT_COLORED_LIGHT, animTexPointColoredLight, "" )
PrecomputeHash_GLProg( ANIM_TEX_MULTI_LIGHT, animTexMultiLight, "" )
PrecomputeHash_GLProg( PROJTEX, texproj, "texture projection" ) //Testing
PrecomputeHash_GLProg( METER_TEX_DIMMER_RGB_MASK, meterTexDimRGBMask, "2 tex (diffuse) and (gradient mask)" ) //Will decrease the brightness of the colors from the diffuse texture
PrecomputeHash_GLProg( METER_TEX_HARD_ALPHA_MASK, meterTexHardAlphaMask, "2 tex (diffuse) and (gradient mask)" ) //Will change the alpha of the texture to 0 where masked

extern const uint NO_SHADER_PROGRAM;

template< typename TYPE >
class VBO;

class Shaders {
public:
    Shaders( void );
    Shaders( const Shaders& other ) = delete;
    ~Shaders( void );

public:
    void PrintShaderLog( const GLuint& obj, const char* path, const std::string& shader_source ) const;
    void PrintProgramLog( const GLuint& obj, const char* path ) const;
    bool MakeProg( const char* name, const char* fsname, const char* vsname, const char* path );

    void Register( const std::string& path, const char* container = "" /* blank means filesystem */ );
    void LoadAllShaders( void ); //!< won't load if already loaded from same path
    void LoadAllProgs( void ); //!< won't load if already loaded from same path

    void UseProg( const Hash& hashkey );
    uint GetAttribLoc( const char* loc_name ) const;

    template< typename TYPE >
    void SetAttrib( const char* loc_name, const VBO<TYPE>& vbo ) const;

    int GetUniformLoc( const char* loc_name ) const; //!< todo: this should return uint, but GL can't seem to decide if locations are int or uint. using int to avoid warnings. update: reutrns -1 on error, so it looks like this should be int

    void SetUniform1i( const char* loc_name, const int a );
    void SetUniform1f( const char* loc_name, const float a );
    void SetUniform1iv( const char* loc_name, const std::size_t num, const int *array );
    void SetUniform2f( const char* loc_name, const float a, const float b );
    void SetUniform2uiv( const char* loc_name, const std::size_t num,  const uint *array );
    void SetUniform3f( const char* loc_name, const float a, const float b, const float c );
    void SetUniform3f( const char* loc_name, const Vec3f& vec );
    void SetUniform4f( const char* loc_name, const float a, const float b, const float c, const float d );
    void SetUniform4f( const char* loc_name, const Color4f& color );
    void SetUniform2x4f( const char* loc_name, const std::size_t num, const GLenum whether, const float *array );
    void SetUniformMatrix4fv( const char* loc_name, const std::size_t num, const GLenum transpose,  const float *array );
    void SetUniformMatrix3fv( const char* loc_name, const std::size_t num, const GLenum transpose,  const float *array );

    void SendData_Matrices( void ) const;
    void SendData_ProjectionMat( float pj[16] ) const;
    void SendData_ModelViewMat( float mv[16] ) const;
    void SetTexture( const uint texture_id, const uint texture_unit=0, const char* uniform_var="tex0" ) const;
    uint GetCurrentProg( void ) const;

    void DrawArrays( GLenum mode, GLint first, uint count );

private:
    void FreeAll( void );
    void FreeList( HashList< uint >& mainlist );

    bool LoadVertexShader( File& opened_shader_file ); //!< takes a nonconst File because it will call ReadToBuffer. Warns if shader overrides another.
    bool LoadFragmentShader( File& opened_shader_file ); //!< takes a nonconst File because it will call ReadToBuffer. Warns if shader overrides another.

    bool Load( const AssetManifest& data ); //!< load either a vertex or fragment shader
    bool Load( File& opened_shader_file, bool fragmentShader ); //! vertex shader if fragmentShader is set to false, otherwise load frag

    bool LoadGLProg( const AssetManifest& data );
    bool LoadGLProg( File& opened_glprog_file );
    
    void ReadShaderFile( File& opened_shader_file, std::string& out ) const;

private:
    HashList< AssetManifest > manifest;
    HashList< uint > vertexShaders;
    HashList< uint > fragmentShaders;
    HashList< uint > glProgs;
    std::string cur_prog_name;
    uint currentProg;

    static std::vector< uint > boundAttributes;
};

template< typename TYPE >
void Shaders::SetAttrib( const char* loc_name, const VBO<TYPE>& vbo ) const {
    uint attrib = GetAttribLoc( loc_name );

    if ( !vbo.Finalized() ) {
        DIE("VBO is not finalized for sending attribute %s to shader %s\n", loc_name, cur_prog_name.c_str() );
        return;
    }

    glEnableVertexAttribArray( attrib );
    glBindBuffer(GL_ARRAY_BUFFER, vbo.GetBufferID() );
    glVertexAttribPointer( attrib, static_cast< int >( vbo.GetThingsPerItem() ), vbo.GetGLType(), GL_FALSE, 0, nullptr ); //todo: this should be uint, but GL can't decide
    boundAttributes.push_back( attrib );
}

#endif  // SRC_RENDERING_SHADERS_H_
