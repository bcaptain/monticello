// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./renderer.h"
#include "./drawInfo.h"

DrawInfo::DrawInfo( void )
    : material()
    , opacity(1)
    , colorize(1,1,1,0)
{ }
