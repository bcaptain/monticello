// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./materialManager.h"
#include "../filesystem.h" // for OverrideWarning()
#include "./renderer.h"

bool MaterialManager::instantiated( false );
MaterialManager materialManager;

MaterialManager::MaterialManager( void )
    : AssetManager( "Material" )
{
    if ( instantiated ) {
        ERR_DIALOG("Only one instantiation of %sManager is allowed\n", asset_type_name.c_str());
    }
    instantiated = true;
}

bool MaterialManager::Load( File& opened_file ) {
    // ** make sure it's open

    if ( ! opened_file.IsOpen() ) {
        ERR("Couldn't open model file %s\n", opened_file.GetFilePath().c_str() );
        return false;
    }

    const std::string name( GetAssetNameFromFilePath( opened_file.GetFilePath().c_str() ) );

    AssetData< Material >* manifest_s = manifest.Get( name.c_str() );
    if ( !manifest_s ) {
        DIE("Couldn't load material file, it was not manifested: %s from %s\n", opened_file.GetFilePath().c_str(), opened_file.GetFileContainerName_Full().c_str() );
        return false;
    }

    auto mtl = std::make_shared< Material >();
    mtl->SetName( name );

    // ** parse the file

    ParserText parser( opened_file );
    parser.SetCommentIndicator('#');
    std::string entry, parm;

    while ( parser.ReadWord( entry ) ) {

        if ( entry == "d" ) {
            parser.ReadWord( parm );

            // don't load a texture twice
            std::shared_ptr< const Texture > ti = textureManager.Get( parm.c_str() );

            if ( !ti ) {
                ERR("Diffuse texture does not exist: %s for material: %s.\n", parm, opened_file.GetFilePath() );
            } else {
                mtl->SetDiffuse( ti );
            }
            
            continue;
        }
        
        ERR("Malformed material file %s, unknown entry: %s\n", opened_file.GetFilePath(), entry );
        parser.SkipToNextLine();
    }
    
    const bool asset_existed = manifest_s->data != nullptr;
    if ( asset_existed )
        OverrideWarning(asset_type_name.c_str(), name, opened_file );
        
    manifest_s->data = mtl;

    CMSG_LOG("Loaded material: %s from %s\n", opened_file.GetFilePath(), opened_file.GetFileContainerName_Full() );

    return true;
}

std::shared_ptr< const Material > MaterialManager::Get( const char* name ) {
    std::shared_ptr< const Material > ret = AssetManager< AssetData<Material>,Material >::Get( name );
    if ( ret != nullptr )
        return ret;
    return AssetManager< AssetData<Material>,Material >::Get( "notex" );
}
