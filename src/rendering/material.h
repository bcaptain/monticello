// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CLIB_MATERIAL_H_
#define SRC_CLIB_MATERIAL_H_

#include "../base/main.h"
#include "./texinfo.h"

class Material {
public:
    Material( void );
    explicit Material( const std::string& nam );
    Material( const std::string& nam, const std::shared_ptr< const Texture >& _diffuse );
    Material( const Material& other );
    virtual ~Material( void ) {};

public:
    Material& operator=( const Material& other );

public:
    void SetDiffuse( const std::shared_ptr< const Texture >& to );

    std::shared_ptr< const Texture > GetDiffuse( void ) const;
    uint GetDiffuseWidth( void ) const;
    uint GetDiffuseHeight( void ) const;

    std::string GetName( void ) const;
    void SetName( const std::string& to );

protected:
    std::string name;
    std::shared_ptr< const Texture > diffuse;
};

std::shared_ptr< const Material > MapLoadMaterial( FileMap& saveFile, const std::vector< std::string >& string_pool );
void MapSaveMaterial( FileMap& saveFile, LinkList< std::string >& string_pool, const std::shared_ptr< const Material >& material );

#endif  // SRC_CLIB_MATERIAL
