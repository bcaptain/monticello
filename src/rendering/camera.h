// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_RENDERING_CAMERA_H_
#define SRC_RENDERING_CAMERA_H_

#include "./clib/src/warnings.h"
#include "../entities/entity.h"

extern const float CAMERA_DEFAULT_TILT;
extern const float CAMERA_DEFAULT_ZOOM;
extern const float CAMERA_DEFAULT_ROT;
extern const Vec3f CAMERA_DEFAULT_FOCALPOINT_OFFSET;

class Renderer;

typedef Uint8 CameraStateT_BaseType;
enum class CameraState :CameraStateT_BaseType {
    Free
    , TetheredToPlayer
};

/*!

Camera \n\n
Camera that travels around the XY axis

**/

class Camera {
public:
    Camera( void );
    Camera( const Camera& other ) = delete;

public:
    Camera& operator=( const Camera& other ) = delete;

public:

    void SetFollowEntity( const std::weak_ptr< const Entity >& ent ); //!< follow ent is the entity to stay in center of screen at all times
    std::shared_ptr< const Entity > GetFollowEntity( void ) const;
    void Unfollow( const Entity& ent ); //!< unfollow the "follow entity" if it is the same one as the argument

    Vec3f NudgeFocusOrigin( const Vec3f& nudge ); //!< setting the focus origin is only effective if there is no follow entity
    Vec3f GetFocusOrigin( void ) const;
    void LookAt( const Vec3f& origin ); //!< only effective if there is no follow entity
    bool LookAtGroup( const LinkList< std::weak_ptr< Entity> >& group ); //!< only effective if there is no follow entity

    Vec3f GetOrigin( const Vec3f offset = Vec3f(0,0,0) ) const; //!< get the actual position of the camera as it currently is

    Vec3f GetOriginAtMaximumZoom( void ) const; //!< get the actual position of the camera if it is at ideal zoom
    Vec3f GetOriginAtNoZoom( const Vec3f offset = Vec3f(0,0,0) ) const; //!< get the focal point of the camera during gameplay
    Vec3f GetOrigin( const Mat4f& modelView, const Vec3f offset = Vec3f(0,0,0) ) const; //!< get the actual position of the camera if it were to use this modelView matrix

    Vec3f GetFocusOriginOffset( void ) const; //!< The offset is applied only when following the player

public:
    const Mat3f& GetMousePanMatrix( void ) const;
    void SetMousePanMatrix( const Mat3f& to );

    float GetRot( void ) const;

    void SetZoom( const float to );
    float GetZoom( void ) const; //!< get where the camera wants to be (after it finishes any transition it's in the middle of)
    float GetCurZoom( void ) const; //!< get the actual zoom level of the camera
    void NudgeZoom( const float nudge );

    void NudgeRot( const float nudge );

    void Tilt( const float amt );
    void SetTilt( const float val );
    float GetTilt( void ) const;

    void Rotate( const float amt );
    void SetRotation( const float val );

    void Reset( void );
    void UnsetFollowEntity( void );
    void UnsetFollowEntity( const Entity* ent );

    void Think( void ); //!< tell the camera to update it's position if it happens to be transitioning from one place to another

    Vec3f GetDir( void ) const;
    
    void Update( void );

private:
    void FadeViewBlockingEntities( void );
    void MoveCameraInFrontOfViewblockingEntities( void );

private:
    Mat3f mouse_pan_matrix; //!< used to tell input how to use the mouse for panning, etc
    Mat3f tilt_mat;
    Vec3f focus_origin;
    std::weak_ptr< const Entity > follow_entity; //!< if not null, this is the entity that will always be in the center of the screen
    float curZoom;
    
};


#endif  // SRC_RENDERING_CAMERA_H_
