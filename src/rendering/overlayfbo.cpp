// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./renderer.h"
#include "./overlayfbo.h"

OverlayFBO::OverlayFBO( void )
    : vbo_vert( VBO<float>( 2,VBOChangeFrequencyT::RARELY ) )
    , vbo_uv( VBO<float>( 2,VBOChangeFrequencyT::RARELY) )
    , fbo( FBO( globalVals.GetInt( gval_v_width  ), globalVals.GetInt( gval_v_height ) ) )
{
    renderer->CheckGLError( "OverlayFBO decl start" );
    
    vbo_vert.PackQuad2D_CCW_BR( 2, -1, -1 );
    if ( ! vbo_vert.Finalized() )
        vbo_vert.MoveToVideoCard();
    ASSERT( vbo_vert.Finalized() );

    vbo_uv.PackQuad2D_CCW_BR();
    if ( ! vbo_uv.Finalized() )
        vbo_uv.MoveToVideoCard();
    ASSERT( vbo_uv.Finalized() );

    renderer->CheckGLError( "OverlayFBO decl finish" );
}

void OverlayFBO::Bind( void ) {
    renderer->CheckGLError( "OverlayFBO Bind start" );
    fbo.Bind();

    glClearColor(0, 0, 0, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearDepth(1);
    glClear(GL_DEPTH_BUFFER_BIT);

    renderer->CheckGLError( "OverlayFBO Bind finish" );
}

void OverlayFBO::Unbind( void ) {
    renderer->CheckGLError( "OverlayFBO Unbind start" );
    fbo.Unbind();
    renderer->CheckGLError( "OverlayFBO Unbind finish" );
}

void OverlayFBO::DrawToScreen( const float opacity ) {
//    glClearColor(renderer->backgroundDarkness, renderer->backgroundDarkness, renderer->backgroundDarkness, 1.0f);
//    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glEnable( GL_DEPTH_TEST ); // this is to be always enabled at the exit of every function.
    
    // Draw FadeOut FBO
    if ( !fbo.GetDiffuse() ) {
        DIE("Could not find material or diffuse when drawing sprite.\n");
        return;
    }

    shaders->UseProg(  GLPROG_COLORIZE_TEXTURE_2D  );
    shaders->SendData_Matrices();
    shaders->SetTexture( fbo.GetDiffuse()->GetID() );
    shaders->SetAttrib( "vPos", vbo_vert );
    shaders->SetAttrib( "vUV", vbo_uv );
    shaders->SetUniform4f("vColor", renderer->backgroundDarkness, renderer->backgroundDarkness, renderer->backgroundDarkness, 1 );
    shaders->SetUniform1f("fWeight", opacity );
    shaders->DrawArrays( GL_QUADS, 0, vbo_vert.Num() );
    
    glClear( GL_DEPTH_BUFFER_BIT );
}

void OverlayFBO::SetupMatricesForDrawToScreen( void ) {
    renderer->ModelView().LoadIdentity();
    renderer->Projection().LoadIdentity();
}
