// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_ORDER
#define SRC_RENDERING_ORDER

typedef Uint8 UI_LayerT_BaseType;
enum UI_LayerT : UI_LayerT_BaseType {
    UI_LAYER_BOTTOM = 0
    , UI_LAYER_GAMEUI = 1
    , UI_LAYER_GAMEUI_DIALOG = 2
    , UI_LAYER_EDITORUI = 3
    , UI_LAYER_EDITORUI_DIALOG = 4
    , UI_LAYER_EDITORUI_TOOLTIP = 5
    , UI_LAYER_PROGRAM_DIALOG = 6
    , UI_LAYER_PROGRAM_WINDOW = 7
    , UI_LAYER_PROGRAM_WINDOW_DIALOG = 8
    , UI_LAYER_TOP = 9
};

#endif // SRC_RENDERING_ORDER
