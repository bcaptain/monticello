// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_TEXTURES_H_
#define SRC_RENDERING_TEXTURES_H_

class Texture;
class Renderer;

#include "../base/main.h"
#include "../files/manifest.h"
#include "../files/assetManager.h"

class TextureManager : public AssetManager< AssetData<Texture>, Texture > {
public:
    TextureManager( void );

public:
    uint Num( void );
    void Reload( void );

private:
    bool Load( File& opened_texture_file ) override;
    using AssetManager< AssetData<Texture>,Texture >::Load;
    bool LoadImageData( File& opened_texture_file, ImageData &image_data_out );
    bool BindImageData( Texture& ti, File& texture_file, const uint id, const ImageData& image_data );

    bool GenerateImageFileDef( File& opened_texture_file, ImageFileDef& imageDef_out );

private:
    static bool instantiated;
};

extern TextureManager textureManager;

#endif  // SRC_RENDERING_TEXTURES_H_
