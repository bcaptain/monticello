// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./renderer.h"
#include "./vbo.h"
#include "./shapes.h"
#include "./font.h"
#include "./fontManager.h"

const float SPHERE_RADIUS_DEFAULT = 1.0f; // meters/units
const float SPHERE_RADIUS_MINIMUM = EP;

const float BOX_BOUNDS_DEFAULT = 0.5f;
const float BOX_BOUNDS_MIN = 0.1f;
