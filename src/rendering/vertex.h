// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CLIB_VERTEXT_H_
#define SRC_CLIB_VERTEXT_H_

#include "../base/main.h"

template< typename TYPE >
class VertexT {
public:
    VertexT( void );
    VertexT( const VertexT& other );
    ~VertexT( void ) = default;

public:
    VertexT& operator=( const VertexT& other );

public:
    TYPE    co;
    Vec2f   uv;
    Vec3f   color;
    float   opacity;
};

template< typename TYPE >
VertexT<TYPE>::VertexT( void )
    : co()
    , uv(0,0)
    , color(0,0,0)
    , opacity(1)
    {

}

template< typename TYPE >
VertexT<TYPE>::VertexT( const VertexT<TYPE>& other )
    : co(other.co)
    , uv(other.uv)
    , color(other.color)
    , opacity(other.opacity)
{ }

template< typename TYPE >
VertexT<TYPE>& VertexT<TYPE>::operator=( const VertexT<TYPE>& other ) {
    co = other.co;
    color = other.color;
    uv = other.uv;
    opacity = other.opacity;
    return *this;
}

typedef VertexT<Vec2f> Vertex2D;

#endif  //__CLIB_VERTEXT
