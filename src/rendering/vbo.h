// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_RENDERING_VBO_H_
#define SRC_RENDERING_VBO_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include <GL/glew.h>

class Model;
class ModelInfo;
class FontInfo;
class MeshVert;

template< typename TYPE >
class VertexT;

template< typename TYPE >
class Vec2t;

typedef Vec2t<float> Vec2f;

typedef VertexT<Vec2f> Vertex2D;

template< typename TYPE >
class FaceT;

typedef FaceT<Vertex2D> Face2D;

class Widget;

extern const std::size_t VBO_DATA_SEGMENT_SIZE; //!< a default increment amount

typedef decltype(GL_DYNAMIC_DRAW) VBOChangeFrequencyT_BaseType;
enum class VBOChangeFrequencyT : VBOChangeFrequencyT_BaseType {
    FREQUENTLY = GL_DYNAMIC_DRAW,
    RARELY = GL_STATIC_DRAW
};

template<typename TYPE>
class VBO {
public:
    explicit VBO( const std::size_t things_per_item, const VBOChangeFrequencyT draw_type );

    VBO( void ) = delete;
    VBO( const VBO& other ) = delete;
    VBO( VBO&& other_rref );
    ~VBO( void );
private:
    void Construct( void );

public:
    VBO& operator=( const VBO& other ) = delete;
    VBO& operator=( VBO&& other_rref );

public:
    void Clear( void ); //!<  this clears the entire VBO. it will not be drawable after this happens.
    void ClearClientData( void ); //!<  clears only the temporary data that has been moved to the video card via MoveToVideoCard()

    void Pack( const TYPE& item );
    void Pack( const Vec3t<TYPE>& item );
    void Pack( const Color4f& item );

    void PackQuad2D_CCW_BR( const float scale = 1.0f, const float x_offset = 0, const float y_offset = 0 );
    void PackQuad2D_CW_BR( const float scale = 1.0f, const float x_offset = 0, const float y_offset = 0 );
    
    bool PackNormals( const std::vector<MeshVert>& verts, const float length );

    void PackArray( const std::size_t items_length, const TYPE* items );
    void PackArray( const std::size_t items_length, const Vec3t<TYPE>* items );

    void PackRepeatedly( const std::size_t pack_times, const TYPE& item );
    void PackRepeatedly( const std::size_t pack_times, const Vec3t<TYPE>& item );
    void PackRepeatedly( const std::size_t pack_times, const Color4f& item );

    void PackFace2DVerts( const Face2D& face );
    void PackFace2DTexCoords( const Face2D& face );
    void PackFace2DColors( const Face2D& face );

    uint PackText( VBO<float>& vbo_uv, const char* str, const FontInfo& fi, Vec2f* position = nullptr, const Widget* container = nullptr ); //!< returns how many faces were packed (in one vbo)
    uint PackText( VBO<float>& vbo_uv, const char* str, Vec2f* position = nullptr, const Widget* container = nullptr ); //!< returns how many faces were packed (in one vbo)

    std::size_t Num( void ) const;

    bool MoveToVideoCard( void );
    bool Finalized( void ) const;
    void BindToAttrib( const uint attrib ) const;

    uint GetThingsPerItem( void ) const;
    uint GetBufferID( void ) const;
    GLenum GetGLType( void ) const;

private:
    TYPE* PointerForPack( const std::size_t pack_count ); //!<  return a pointer to manually set data in the array. everything else internally is automatically handled based on pack_count
    void EnsureAllocated( std::size_t num_to_ensure );
    std::size_t GetMemSize( void ) const;
    void PartialAdd( const std::size_t x );
    std::size_t NextPackIndex( void ) const;
    void Packed( std::size_t how_many );

private:
    std::size_t things_per_element;
    std::size_t num; //!< the number of elements
    uint partial_adds; //!< if we don't have enough to make en element and +1 to num, store it here
    std::size_t allocated; //!< how much memory is allocated
    uint buffer_id;
    TYPE* data;
    VBOChangeFrequencyT drawtype; //!< GL_DYNAMIC_DRAW / GL_STATIC_DRAW
    GLenum glType;
    bool finalized; //!< whether this VBO has been sent to the video card
};

void GenerateCircleVerts( const uint num_subdivides, const float radius, std::vector<Vec3f>& verts );
void PackAlignedCircleVBO( const uint num_subdivides, const float radius, VBO<float>& vbo_vert, VBO<float>& vbo_color, const Color4f& color, const Vec3f& normal );
void PackVBO_Sphere( const uint num_subdivides, const float radius, const Vec3f& origin, VBO<float>& vbo_vert, VBO<float> *vbo_color = nullptr );
void PackVBO_Cylinder( const uint num_subdivides, const float radius, const float length, const Vec3f& origin, const Vec3f& normal, VBO<float>& vbo_vert, VBO<float> *vbo_color = nullptr );
void PackVBO_Capsule( const uint num_subdivides, const float radius, const float length, const Vec3f& origin, const Vec3f& normal, VBO<float>& vbo_vert, VBO<float> *vbo_color = nullptr );
void PackVBO_BoxVerts( const AABox3D& aabox, VBO<float>& vbo_vert, VBO<float> *vbo_color = nullptr );
void PackVBO_BoxVerts( const Box3D& box, VBO<float>& vbo_vert, VBO<float> *vbo_color = nullptr );
void PackVBO_BoxUVs( VBO<float>& vbo_uv );
bool PackVBO_Model( VBO<float>& vbo_vert, VBO<float>& vbo_uv, VBO<float>& vbo_norm, VBO<float>* vbo_mesh_boneID, VBO<float>* vbo_mesh_boneWeights, const Model& mdl );
bool PackVBO_Mesh( VBO<float>& vbo_vert, VBO<float>& vbo_uv, VBO<float>& vbo_norm, VBO<float>* vbo_mesh_boneID, VBO<float>* vbo_mesh_boneWeights, const Model& mdl, Uint8 meshID );

template<typename TYPE>
inline uint VBO<TYPE>::GetThingsPerItem( void ) const {
    return things_per_element;
}

template<typename TYPE>
inline uint VBO<TYPE>::GetBufferID( void ) const {
    return buffer_id;
}

template<typename TYPE>
inline GLenum VBO<TYPE>::GetGLType( void ) const {
    return glType;
}

// construct methos used only once, and used in this header, putting them here is okay, and also necessary.
template<>
inline void VBO<float>::Construct( void ) {
    glType = GL_FLOAT;
}

template<>
inline void VBO<int>::Construct( void ) {
    glType = GL_INT;
}

template<>
inline void VBO<uint>::Construct( void ) {
    glType = GL_UNSIGNED_INT;
}

template<typename TYPE>
VBO<TYPE>::VBO( const std::size_t things_per_item, const VBOChangeFrequencyT draw_type )
    : things_per_element(things_per_item)
    , num(0)
    , partial_adds(0)
    , allocated(0)
    , buffer_id(0)
    , data(nullptr)
    , drawtype( draw_type )
    , glType( GL_INVALID_VALUE )
    , finalized(false)
{
    Construct();
    glGenBuffers( 1, &buffer_id );
}

template<typename TYPE>
void VBO<TYPE>::ClearClientData( void ) {
    DELNULLARRAY( data );
    allocated=0;
}

template<typename TYPE>
VBO<TYPE>::~VBO( void ) {
    if ( buffer_id != 0 )
        glDeleteBuffers(1, &buffer_id);
    ClearClientData();
}

template<typename TYPE>
inline bool VBO<TYPE>::Finalized( void ) const {
    return finalized;
}

template<typename TYPE>
inline std::size_t VBO<TYPE>::Num( void ) const {
    return num;
}

template<typename TYPE>
void VBO<TYPE>::Clear( void ) {
    // if we may pack again soon, keep the allocation
    if ( drawtype == VBOChangeFrequencyT::RARELY ) {
        ClearClientData();
    }
    num=0;
    finalized = false;
}

template<typename TYPE>
inline std::size_t VBO<TYPE>::GetMemSize( void ) const {
    return things_per_element * num * sizeof(TYPE);
}

template<typename TYPE>
void VBO<TYPE>::EnsureAllocated( std::size_t num_to_ensure ) {
    if ( num_to_ensure <= allocated )
        return;

    // create new allocation
    num_to_ensure += VBO_DATA_SEGMENT_SIZE; //extra padding so we don't have to reallocate too often
        TYPE *newData = new TYPE[ num_to_ensure ]();

    // copy over any old data
    for ( std::size_t i=0; i<allocated; ++i )
        newData[i] = data[i];

    // replace old data with new data
    DELNULLARRAY( data );
    data = newData;
    allocated = num_to_ensure;
}

template<typename TYPE>
inline std::size_t VBO<TYPE>::NextPackIndex( void ) const {
    return num*things_per_element + partial_adds;
}

template<typename TYPE>
void VBO<TYPE>::Packed( std::size_t how_many ) {
    while ( how_many > 0 ) {
        if ( how_many < things_per_element ) {
            PartialAdd( how_many );
            break;
        } else {
            ++num;
            how_many -= things_per_element;
        }
    }
}

template<typename TYPE>
TYPE* VBO<TYPE>::PointerForPack( const std::size_t pack_count ) {
    const std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index + pack_count );
    Packed( pack_count );
    return &data[pack_index];
}

template<typename TYPE>
void VBO<TYPE>::PackRepeatedly( const std::size_t pack_times, const TYPE& item ) {
    const std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index + pack_times );

    // append the items to the data
    for ( std::size_t i=0; i < pack_times; ++i )
        data[pack_index+i] = item;

    Packed( pack_times );
}

template<typename TYPE>
void VBO<TYPE>::PackArray( const std::size_t items_length, const TYPE* items ) {
    const std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index + items_length );

    // append the items to the data
    for ( std::size_t i=0; i < items_length; ++i )
        data[pack_index+i] = items[i];

    Packed( items_length );
}

template<typename TYPE>
void VBO<TYPE>::PackRepeatedly( const std::size_t pack_times, const Vec3t<TYPE>& item ) {
    const std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index + pack_times*3 );

    // append the item to the data
    std::size_t i=0;
    while ( i < pack_times*3 ) {
        data[pack_index+i] = item.x; ++i;
        data[pack_index+i] = item.y; ++i;
        data[pack_index+i] = item.z; ++i;
    }

    Packed( pack_times*3 );
}

template<typename TYPE>
void VBO<TYPE>::PackArray( const std::size_t items_length, const Vec3t<TYPE>* items ) {
    const std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index + items_length*3 );

    // append the items to the data
    std::size_t i = 0;
    std::size_t p = 0;
    while ( i < items_length ) {
        data[pack_index+p] = items[i].x; ++p;
        data[pack_index+p] = items[i].y; ++p;
        data[pack_index+p] = items[i].z; ++p;
        ++i;
    }

    Packed( items_length*3 );
}

template<typename TYPE>
void VBO<TYPE>::Pack( const Vec3t<TYPE>& item ) {
    std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index+3 );

    // append the items to the data
    data[pack_index++] = item.x;
    data[pack_index++] = item.y;
    data[pack_index] = item.z;

    Packed( 3 );
}

template<typename TYPE>
void VBO<TYPE>::PackRepeatedly( const std::size_t pack_times, const Color4f& item ) {
    std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index + pack_times*4 );

    // append the items to the data
    std::size_t i=0;
    while ( i < pack_times*4 ) {
        data[pack_index+i] = item.r; ++i;
        data[pack_index+i] = item.g; ++i;
        data[pack_index+i] = item.b; ++i;
        data[pack_index+i] = item.a; ++i;
    }

    Packed( pack_times * 4 );
}

template<typename TYPE>
void VBO<TYPE>::Pack( const Color4f& item ) {
    std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index+4 );

    // append the items to the data
    data[pack_index++] = item.r;
    data[pack_index++] = item.g;
    data[pack_index++] = item.b;
    data[pack_index] = item.a;

    Packed( 4 );
}

template<typename TYPE>
void VBO<TYPE>::Pack( const TYPE& item ) {
    const std::size_t pack_index = NextPackIndex();
    EnsureAllocated( pack_index+1 );

    // append the item to the data
    data[pack_index] = item;

    Packed( 1 );
}

template<typename TYPE>
void VBO<TYPE>::PackQuad2D_CW_BR( const float scale, const float x_offset, const float y_offset ) {
    const float UV_BOTTOM = 0.0f + y_offset;
    const float UV_LEFT = 0.0f + x_offset;
    const float UV_TOP = (1.0f * scale) + y_offset;
    const float UV_RIGHT = (1.0f * scale) + x_offset;

    Pack( UV_BOTTOM );
    Pack( UV_RIGHT );

    Pack( UV_BOTTOM );
    Pack( UV_LEFT );

    Pack( UV_TOP );
    Pack( UV_LEFT );

    Pack( UV_TOP );
    Pack( UV_RIGHT );

}

template<typename TYPE>
void VBO<TYPE>::PackQuad2D_CCW_BR( const float scale, const float x_offset, const float y_offset ) {
    const float UV_BOTTOM = 0.0f + y_offset;
    const float UV_LEFT = 0.0f + x_offset;
    const float UV_TOP = (1.0f * scale) + y_offset;
    const float UV_RIGHT = (1.0f * scale) + x_offset;

    Pack( UV_BOTTOM );
    Pack( UV_RIGHT );

    Pack( UV_TOP );
    Pack( UV_RIGHT );

    Pack( UV_TOP );
    Pack( UV_LEFT );

    Pack( UV_BOTTOM );
    Pack( UV_LEFT );
}

template<typename TYPE>
void VBO<TYPE>::PartialAdd( const std::size_t x ) {
    partial_adds += x;
    std::size_t new_partial_things = static_cast< std::size_t >( fmodf(partial_adds,things_per_element) );
    num += ( partial_adds - new_partial_things ) / things_per_element;
    partial_adds = new_partial_things;
}

template<typename TYPE>
bool VBO<TYPE>::MoveToVideoCard( void ) {
    if ( finalized )
        return true;

    if ( num <= 0 ) {
        return false;
    }

    if ( buffer_id == 0 ) {
        ERR("Could not move VBO data to video card, no buffer created.\n");
        return false;
    }

    if ( partial_adds != 0 ) {
        WARN("Moving incomplete VBO to Video Card.\n");
    }

    glBindBuffer(GL_ARRAY_BUFFER, buffer_id );
    glBufferData(GL_ARRAY_BUFFER, static_cast< int >( GetMemSize() ), data, static_cast<GLenum>( drawtype ) ); //todo: memsize should be uint, but GL takes an int. avoiding the warning here.

    // if we may pack again soon, keep the allocation
    if ( drawtype == VBOChangeFrequencyT::RARELY ) {
        ClearClientData();
    }

    finalized = true;

    return true;
}

template< typename TYPE >
VBO<TYPE>::VBO( VBO&& other_rref )
    : things_per_element( other_rref.things_per_element )
    , num( other_rref.num )
    , partial_adds( other_rref.partial_adds )
    , allocated( other_rref.allocated )
    , buffer_id( other_rref.buffer_id )
    , data( other_rref.data )
    , drawtype( other_rref.drawtype )
    , glType( other_rref.glType )
    , finalized( other_rref.finalized )
{
    other_rref.data = nullptr;
    other_rref.allocated = 0;
}

template< typename TYPE >
VBO<TYPE>& VBO<TYPE>::operator=( VBO<TYPE>&& other_rref ) {

    things_per_element = other_rref.things_per_element;
    num = other_rref.num;
    partial_adds = other_rref.partial_adds;
    buffer_id = other_rref.buffer_id;
    drawtype = other_rref.drawtype;
    glType = other_rref.glType;
    finalized = other_rref.finalized;
    
    delete[] data;
    data = other_rref.data;
    other_rref.data = nullptr;
    
    allocated = other_rref.allocated;
    other_rref.allocated = 0;
    
    return *this;
}

#endif  // SRC_RENDERING_VBO_H_
