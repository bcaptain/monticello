// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_DRAWINFO
#define SRC_RENDERING_DRAWINFO

#include "../base/main.h"
#include "./material.h"

struct DrawInfo {
    DrawInfo( void );
    DrawInfo( const DrawInfo& other ) = default;
    DrawInfo& operator=( const DrawInfo& other ) = default;
    virtual ~DrawInfo( void ) {}

    std::shared_ptr< const Material > material;
    float opacity;
    Color4f colorize;
};

#endif  // SRC_RENDERING_DRAWINFO
