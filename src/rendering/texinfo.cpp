// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./texinfo.h"
#include "./renderer.h"

const uint RMASK=0xFF000000;
const uint GMASK=0x00FF0000;
const uint BMASK=0x0000FF00;
const uint AMASK=0x000000FF;

const int TEX_NORMAL = 0;
const int TEX_PARTICLE = 1;

const std::size_t DEFAULT_TEXTURE_SIZE = 512;
const std::size_t PIXELS_PER_METER = 64;

Texture::~Texture( void ) {
    if ( id.use_count() != 1 )
        return;

    if ( cleanupFunc )
        ( *cleanupFunc )( 1, id.get() );

    // if cleanupFunc is unset, it is handled elsewhere in the code
}

Texture::Texture( void )
    : cleanupFunc( &glDeleteTextures )
    , width(0)
    , height(0)
    , id( new uint( 0 ) )
    , format(0)
    , imgType(TEX_NORMAL)
    , mipmap(true)
    {
}

Texture::Texture( const Texture& other )
    : cleanupFunc( &glDeleteTextures )
    , width( other.width )
    , height( other.height )
    , id( other.id )
    , format( other.format )
    , imgType( other.imgType )
    , mipmap( other.mipmap )
    {
}

Texture::Texture( const uint _width, const uint _height, const std::shared_ptr<uint>& _id, const int _format, const int _imgType, const bool has_mipmap, const TEXTURE_CLEANUP_FUNC _cleanupFunc )
    : cleanupFunc( _cleanupFunc )
    , width( _width )
    , height( _height )
    , id( _id )
    , format( _format )
    , imgType( _imgType )
    , mipmap( has_mipmap )
    {
}

Texture& Texture::operator=( const Texture& other ) {
    cleanupFunc = other.cleanupFunc;
    width = other.width;
    height = other.height;
    id = other.id;
    format = other.format;
    imgType = other.imgType;
    mipmap = other.mipmap;
    return *this;
}

bool Texture::Bind( const ImageData& image_data, const uint to_id ) {
    renderer->CheckGLError( "Binding Texture, start" );
    
    format = image_data.GetFormat();
    width = image_data.GetWidth();
    height = image_data.GetHeight();

    glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

    if ( id && id.use_count() == 1 ) {
        glDeleteTextures( 1, id.get() );
        *id = to_id;
    } else {
        id = std::make_shared< uint >( to_id );
    }

    glBindTexture( GL_TEXTURE_2D, *id );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );    
    //glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER ); //TODO : Should be able to set this parameter when needed
    //glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER ); //TODO : Should be able to set this parameter when needed
    
    
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

    bool ret = true;
    
    if ( mipmap && globalVals.GetBool(gval_v_mipmap) ) {
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
        
        const float log2 = std::log(2.0f);
        const int mipx = static_cast< int >( static_cast<float>( std::log(width) ) / log2 );
        const int mipy = static_cast< int >( static_cast<float>( std::log(height) ) / log2 );
        const int num_mipmaps = std::max( mipx, mipy);
        
        if ( num_mipmaps < 5 )
            WARN("texture %s (id %u) is very small for mipmaps (has %u): %ix%i\n", image_data.GetName().c_str(), *id, num_mipmaps, width, height);

        GLPixelFormatT_BaseType sized_format;
        switch ( format ) {
            case GLPixelFormat_RGBA:
                sized_format = GL_RGBA8;
                break;
            case GLPixelFormat_RGB:
                sized_format = GL_RGB8;
                break;
            default:
                ERR("invalid texture format (%u), for attempted texture to id %u\n", format, to_id );
                ret = false;
                break;
        }
        
        if ( ret ) {
            glTexStorage2D(GL_TEXTURE_2D, num_mipmaps, static_cast< GLenum >( sized_format ), static_cast< int >( width ), static_cast< int >( height ));
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, static_cast< int >( width ), static_cast< int >( height ), static_cast< GLenum >( format ), GL_UNSIGNED_BYTE, image_data.GetBuffer() );
            glGenerateMipmap(GL_TEXTURE_2D);
        }
    } else {
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexImage2D( GL_TEXTURE_2D, 0, format, static_cast< int >( width ), static_cast< int >( height ), 0, static_cast< GLenum >( format ), GL_UNSIGNED_BYTE, image_data.GetBuffer() );
    }
    
    if ( ret )
        ret = !renderer->CheckGLError( "Binding Texture, finish" );
    
    if ( !ret ) {
        glDeleteTextures( 1, id.get() );
        *id = 0;
    }
    
    return ret;
}

void Texture::SetCleanupFunction( const TEXTURE_CLEANUP_FUNC func ) {
    cleanupFunc = func;
}

uint Texture::GetWidth( void ) const {
    return width;
}

uint Texture::GetHeight( void ) const {
    return height;
}

uint Texture::GetID( void ) const {
    return (!id) ? 0 : *id;
}

int Texture::GetFormat( void ) const {
    return format;
}
