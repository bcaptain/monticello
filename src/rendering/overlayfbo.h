// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_OVERLAYFBO_H_
#define SRC_RENDERING_OVERLAYFBO_H_

#include "./vbo.h"
#include "./fbo.h"

class FBO;

struct OverlayFBO {
    OverlayFBO( void );
    
    void Bind( void );
    void Unbind( void );
    
    void SetupMatricesForDrawToScreen( void );
    void DrawToScreen( const float opacity );
    
    VBO<float> vbo_vert;
    VBO<float> vbo_uv;
    FBO fbo;
};

#endif  // SRC_RENDERING_OVERLAYFBO_H_
