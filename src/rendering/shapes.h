// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_RENDERING_SHAPES_H
#define SRC_RENDERING_SHAPES_H

extern const float SPHERE_RADIUS_DEFAULT;
extern const float SPHERE_RADIUS_MINIMUM;

extern const float BOX_BOUNDS_DEFAULT;
extern const float BOX_BOUNDS_MIN;

#include "../base/main.h"

typedef uchar PrimitiveShapeT_BaseType;
enum class PrimitiveShapeT : PrimitiveShapeT_BaseType {
    SPHERE = 0
    , BOX3D
    , AABOX3D
    , LINE3F
    , CAPSULE
    , CYLINDER
    , VEC3F
};

#endif // SRC_RENDERING_SHAPES_H
