// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#ifndef SRC_CLIB_CUBEMAP_H_
#define SRC_CLIB_CUBEMAP_H_

#include "../base/main.h"
#include "../sdl/sdl.h"

class Material;
class Mesh;
template<typename TYPE>
class VBO;

class CubeMap {

public:
    CubeMap( void );
    CubeMap( const CubeMap& other );
    virtual ~CubeMap(void);
    
public:
    std::string GetName( void ) const;
    CubeMap& operator=( const CubeMap& other ) = default;
    
private:
    void SetName( const char* str );
    void BindImageDataToTarget( const ImageData& image_data, const GLenum target );
    
public:
    const Mesh* GetMesh( const std::size_t idx ) const;
    Mesh* GetMesh( const std::size_t idx );
    
    bool SetMap( const char* str );
    
    void PackVBO( void );
    void DrawVBO( void ) const;
    
private:
    std::string name;
    VBO<float>* vbo_vert;
    Mesh* mesh;
    std::vector< std::shared_ptr < const Material > > faces;  //6 Faces [Right,Left,Top,Bottom,Back,Front]
    std::shared_ptr < const Material > face_rt;
    std::shared_ptr < const Material > face_lf;
    std::shared_ptr < const Material > face_up;
    std::shared_ptr < const Material > face_dn;
    std::shared_ptr < const Material > face_bk;
    std::shared_ptr < const Material > face_ft;
};

#endif //SRC_CLIB_CUBEMAP_H_
