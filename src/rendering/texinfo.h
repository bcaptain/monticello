// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_CLIB_TEXINFO_H_
#define SRC_CLIB_TEXINFO_H_

#include "../base/main.h"

extern const uint RMASK;
extern const uint GMASK;
extern const uint BMASK;
extern const uint AMASK;

extern const int TEX_NORMAL;
extern const int TEX_PARTICLE;

extern const std::size_t DEFAULT_TEXTURE_SIZE;
extern const std::size_t PIXELS_PER_METER;

/*!

Texture \n\n

**/

#ifdef _WIN32
    typedef void( __attribute__((__stdcall__)) *TEXTURE_CLEANUP_FUNC)( int, const uint * );
#else // _WIN32
    typedef void(*TEXTURE_CLEANUP_FUNC)( int, const uint * );
#endif // _WIN32

class Texture {
public:
    Texture( void );
    Texture( const Texture& other );
    Texture( const uint _width, const uint _height, const std::shared_ptr<uint>& _id, const int _format, const int _imgType, const bool has_mipmap, const TEXTURE_CLEANUP_FUNC _cleanupFunc ); //! for custom textures
    ~Texture( void );

public:
    Texture& operator=( const Texture& other );

public:
    bool Bind( const ImageData& image_data, const uint to_id );

    uint GetID( void ) const;

    int GetFormat( void ) const;
    void FreeIfLastReference( void );

    uint GetWidth( void ) const;
    uint GetHeight( void ) const;

private:
    void SetCleanupFunction( const TEXTURE_CLEANUP_FUNC func );

private:
    TEXTURE_CLEANUP_FUNC cleanupFunc;
    uint width;
    uint height;
    std::shared_ptr< uint > id;
    int format; //!< GL_RGBA or GL_RGB
    int imgType; //!< whether a particle or a regular texture
    bool mipmap;
};

#endif  //__CLIB_TEXINFO
