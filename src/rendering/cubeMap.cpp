// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./cubeMap.h"
#include "./vbo.h"
#include "../game.h"
#include "./renderer.h"
#include "./materialManager.h"
#include "./textureManager.h"

CubeMap::CubeMap( void )
    : name()
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::RARELY ) )
    , mesh( nullptr )
    , faces()
    , face_rt()
    , face_lf()
    , face_up()
    , face_dn()
    , face_bk()
    , face_ft()
{ 
    PackVBO();
}

CubeMap::CubeMap( const CubeMap& other )
    : name( other.name )
    , vbo_vert( new VBO<float>( 3, VBOChangeFrequencyT::RARELY ) )
    , mesh( other.mesh )
    , faces( other.faces )
    , face_rt(other.face_rt)
    , face_lf(other.face_lf)
    , face_up(other.face_up)
    , face_dn(other.face_dn)
    , face_bk(other.face_bk)
    , face_ft(other.face_ft)
{ }

CubeMap::~CubeMap( void ) {
    delete mesh;
    delete vbo_vert;
    //Cleanup, anything else? -Jesse
}

bool CubeMap::SetMap( const char* str ) {
    bool cubeMapLoaded(true);
    
    //*** Load
    std::shared_ptr< const Material > test = materialManager.Get("starting_skybox_rt");
//    faces[0] = materialManager.Get("starting_skybox_rt");
//    faces[1] = materialManager.Get("starting_skybox_lf");
//    faces[2] = materialManager.Get("starting_skybox_up");
//    faces[3] = materialManager.Get("starting_skybox_dn");
//    faces[4] = materialManager.Get("starting_skybox_bk");
//    faces[5] = materialManager.Get("starting_skybox_ft");
      
    face_rt = materialManager.Get("starting_skybox_rt");
    face_lf = materialManager.Get("starting_skybox_lf");
    face_up = materialManager.Get("starting_skybox_up");
    face_dn = materialManager.Get("starting_skybox_dn");
    face_bk = materialManager.Get("starting_skybox_bk");
    face_ft = materialManager.Get("starting_skybox_ft");

    if ( !face_rt || !face_lf || !face_up || !face_dn || !face_bk || !face_ft )
        cubeMapLoaded = false;

    if ( cubeMapLoaded ) {
        SetName( str );
    } else {
        ERR("Unable to set skybox to cubemap %s\n", str );
        return false;
    }

    return true;
}

void CubeMap::PackVBO( void ) {
    if (vbo_vert->Finalized())
        return;

    vbo_vert->Pack( Vec3f(-1.0f,  1.0f, -1.0f) ); //Right Face
    vbo_vert->Pack( Vec3f(-1.0f, -1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(1.0f, -1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(1.0f, -1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f,  1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f, -1.0f) );

    vbo_vert->Pack( Vec3f(-1.0f, -1.0f,  1.0f) ); //Left Face
    vbo_vert->Pack( Vec3f(-1.0f, -1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f, -1.0f,  1.0f) );

    vbo_vert->Pack( Vec3f(1.0f, -1.0f, -1.0f) );  //Top Face
    vbo_vert->Pack( Vec3f(1.0f, -1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(1.0f,  1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(1.0f, -1.0f, -1.0f) );

    vbo_vert->Pack( Vec3f(-1.0f, -1.0f,  1.0f) ); //Bottom Face
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f, -1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f, -1.0f,  1.0f) );

    vbo_vert->Pack( Vec3f(-1.0f,  1.0f, -1.0f) );  //Back Face
    vbo_vert->Pack( Vec3f( 1.0f,  1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f,  1.0f, -1.0f) );

    vbo_vert->Pack( Vec3f(-1.0f, -1.0f, -1.0f) );  //Front Face
    vbo_vert->Pack( Vec3f(-1.0f, -1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f, -1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f, -1.0f, -1.0f) );
    vbo_vert->Pack( Vec3f(-1.0f, -1.0f,  1.0f) );
    vbo_vert->Pack( Vec3f( 1.0f, -1.0f,  1.0f) );
    
    vbo_vert->MoveToVideoCard();
}

void CubeMap::DrawVBO( void ) const {
    shaders->UseProg(   GLPROG_SKYBOX );
    
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
    //replace with for loop when faces[0] = materialManager.Get... is working
    
    //Bind image data.. need to fix the way this is loaded by the texture manager.. or have a way to access the data after the texture is loaded.
//    BindImageDataToTarget( *textureManager.Get("starting_skybox_rt")->data, static_cast < GLenum >( GL_TEXTURE_CUBE_MAP_POSITIVE_X ));
//    BindImageDataToTarget( *textureManager.Get("starting_skybox_lf")->data, static_cast < GLenum >( GL_TEXTURE_CUBE_MAP_NEGATIVE_X ));
//    BindImageDataToTarget( *textureManager.Get("starting_skybox_up")->data, static_cast < GLenum >( GL_TEXTURE_CUBE_MAP_POSITIVE_Y ));
//    BindImageDataToTarget( *textureManager.Get("starting_skybox_dn")->data, static_cast < GLenum >( GL_TEXTURE_CUBE_MAP_NEGATIVE_Y ));
//    BindImageDataToTarget( *textureManager.Get("starting_skybox_bk")->data, static_cast < GLenum >( GL_TEXTURE_CUBE_MAP_POSITIVE_Z ));
//    BindImageDataToTarget( *textureManager.Get("starting_skybox_ft")->data, static_cast < GLenum >( GL_TEXTURE_CUBE_MAP_NEGATIVE_Z ));
    
    //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    //glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}

void CubeMap::BindImageDataToTarget( const ImageData& image_data, const GLenum target ) {
    GLint format = image_data.GetFormat();
    GLsizei width = image_data.GetWidth();
    GLsizei height = image_data.GetHeight();
    
    glTexImage2D( target, 0, format, width, height, 0, static_cast< GLenum >( format ), GL_UNSIGNED_BYTE, image_data.GetBuffer() );
}

std::string CubeMap::GetName( void ) const {
    return name;
}

void CubeMap::SetName(const char* str) {
    name = str;
}
