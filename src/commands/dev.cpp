// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../commandsys.h"
#include "../game.h"
#include "../entities/entityInfoManager.h"
#include "../input/input.h"
#include "../rendering/renderer.h"
#include "../rendering/shapes.h"
#include "../entities/actor.h"
#include "../editor/editor.h"
#include "../editor/addMode.h"
#include "../editor/undo/undo_entityAdd.h"
#include "../ui/dialog/common.h"

void CommandSys::Dev_Init( void ) {
        commands.Set("tet", &tet );
        commands.Set("tri", &tri );
        commands.Set("line", &line );
        commands.Set("point", &point );
        commands.Set("capsule", &capsule );
        commands.Set("sphere", &sphere );
        commands.Set("shape", &shape );
        commands.Set("aabox", &aabox );
        commands.Set("delshape", &delshape );
        commands.Set("delallshapes", &delallshapes );
        commands.Set("dellastshape", &dellastshape );
        commands.Set("nameshape", &nameshape );
        commands.Set("namelastshape", &namelastshape );

#ifdef MONTICELLO_EDITOR
    commands.Set("anim", &anim );
    commands.Set("editor", &devmode );
#endif // MONTICELLO_EDITOR

    commands.Set("give", &give );
    commands.Set("spawn", &spawn );
    commands.Set("new", &newent );
    commands.Set("break", &dbreak );
    commands.Set("debug", &debug );
    commands.Set("resaveMaps", &resaveMaps );
}

#ifdef MONTICELLO_EDITOR
void CommandSys::devmode( [[maybe_unused]] const std::vector< std::string >& args ) {
    Dialog_ToggleEditor();
}
#endif // MONTICELLO_EDITOR

void CommandSys::dbreak( [[maybe_unused]] const std::vector< std::string >& args ) {
    debugOutput();
}

void CommandSys::debug( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: debug [ents/mergemodel/shatter]\n");
        return;
    }

    if ( args[1] == "ents" ) {
        debug_print_entity_indices();
        return;
    }

    if ( args[1] == "mergemodel" ) {
        debug_merge_models();
        return;
    }
    
    if ( args[1] == "shatter" ) {
        debug_shatter_flat();
        return;
    }
}

void CommandSys::spawn( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: spawn [entname_of_entity]\n");
        return;
    }

    auto ent = entityInfoManager.LoadAndSpawn( args[1].c_str(), Input::GetWorldMousePos() + Vec3f(0,0,OFFSET_RADIUS_BUFFER) ); // add a small epsilon so it doesn't intersect floors
    if ( editor && ent )
        new Undo_EntityAdd( editor->undo_history, *ent );
}

void CommandSys::newent( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: new [classname_of_entity]\n");
        return;
    }

    if (!game )
        return;

    if ( ! is_valid_classname( args[1] ) ) {
        ERR("That is not a valid class name.\n");
        return;
    }

    auto ent = game->NewEnt( get_typeid( args[1] ) );
    if ( ! ent )
        return;

    ent->SetOrigin( Input::GetWorldMousePos() + Vec3f(0,0,0.1f) ); // add a small epsilon so it doesn't intersect floors
    ent->Spawn();
}

void CommandSys::resaveMaps( const std::vector<std::string>& args ) {
    game->ResaveAllMaps();
}

void CommandSys::give( const std::vector<std::string>& args ) {
    const std::string usage_error("usage: give [health/item/val/etc] ...\n");

    if ( args.size() < 2 ) {
        ERR( usage_error.c_str() );
        return;
    }

    std::shared_ptr< Actor > player = game->GetPlayer();
    if ( ! player ) {
        WARN("There is no player to give to\n");
        return;
    }

    if ( args[1].compare( "health" ) == 0 ) {
        if ( args.size() == 3 ) {
            const int amount = String::ToInt( args[2] );
            CMSG_LOG("Giving player %i health\n", amount );
            player->SetHealth( amount );
        } else {
            ERR("usage: give health [how_much]\n");
        }
        return;
    }

    if ( args[1].compare( "ammo" ) == 0 ) {
        if ( args.size() > 2 ) {
            const auto ammoName = args[2];
            
            if ( args.size() > 3 ) {
                const int amount = String::ToInt( args[3] );
                CMSG_LOG("Giving player %i %s ammo\n", amount, ammoName.c_str() );
                if ( player->GiveAmmo( ammoName, amount ) ) {
                    CMSG("Player now has %u of ammotype %s.\n", player->GetAmmoQuantity(ammoName), ammoName.c_str() );
                }
                return;
            }
            
            CMSG("Player has %u of ammotype %s.\n", player->GetAmmoQuantity(ammoName), ammoName.c_str() );
        }
        ERR("usage: give ammo [ammoName] [how_much]\n");
        
        return;
    }

    if ( args[1].compare( "item" ) == 0 ) {
        if ( args.size() >= 3 ) {
            for ( uint i=2; i<args.size(); ++i ) {
                CMSG_LOG("Giving player item %s\n", args[i].c_str() );
                player->inventory.Add( Item( args[i].c_str(), args[i].c_str(), ItemTypeT::NORMAL ) );
            }
        } else {
            ERR("usage: give item [space_separated_item_list]\n");
        }
        return;
    }

    if ( args[1].compare( "attachment" ) == 0 ) {
        if ( args.size() == 4 ) {
            CMSG_LOG("Giving player attachment: %s / %S \n", args[2].c_str(), args[3].c_str() );
            player->AddAttachment( args[2], args[3], 1 );
        } else {
            ERR("usage: give attachment [pos] [name]\n");
        }
        return;
    }

    if ( args[1].compare( "val" ) == 0 ) {
        if ( args.size() == 4 ) {
            CMSG_LOG("Giving player value: %s \n", args[2].c_str() );
            player->entVals.Set( args[2].c_str(), args[3] ); //toNamedSetter
        } else {
            ERR("usage: give val [key] [value]\n");
        }
        return;
    }

    ERR( usage_error.c_str() );
}

#ifdef MONTICELLO_EDITOR
void CommandSys::anim( const std::vector<std::string>& args ) {
    if ( args.size() != 3 ) {
        ERR("usage: anim: [modelName] [animName]\n");
        return;
    }

    if ( ! editor ) {
        EditorNotOpenError();
        return;
    }

    if ( ! editor->SetTestAnimModel( args[1].c_str() ) ) {
        ERR("Couldn't set testModel to %s\n", args[1].c_str());
        globalVals.SetBool(gval_d_testAnim, false);
        return;
    }

    editor->UpdateTestAnimMaterial();
    if ( ! editor->SetTestAnim( args[2].c_str() ) ) {
        ERR("Couldn't set testAnim to %s\n", args[2].c_str());
        globalVals.SetBool(gval_d_testAnim, false);
        return;
    }

    globalVals.SetBool(gval_d_testAnim, true);
}

#endif // MONTICELLO_EDITOR

void CommandSys::DrawAABox( [[maybe_unused]] const std::string& name_unused, const std::string* six_floats, const Vec3f& origin ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    AABox3D aabox(
        String::ToFloat( six_floats[0] )
        , String::ToFloat( six_floats[1] )
        , String::ToFloat( six_floats[2] )
        , String::ToFloat( six_floats[3] )
        , String::ToFloat( six_floats[4] )
        , String::ToFloat( six_floats[5] )
    );

    aabox.min += origin;
    aabox.max += origin;

    const Box3D box( aabox );

    CMSG("Axis-Aligned Box added.\n");
}

void CommandSys::DrawShape( const char* shape_kind, [[maybe_unused]] const std::string& name_unused, const std::string* verts, const std::size_t num_verts ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    std::vector< Vec3f > vert;
    vert.reserve(num_verts);

    for ( std::size_t i = 0; i<num_verts; ++i ) {
        if ( ! String::ToVec3f( verts[i], vert[i] ) ) {
            ERR("vertex %i is invalid: %s\n", i, verts[i].c_str());
            return;
        }
    }

    CMSG("%s added.\n", shape_kind);
}

void CommandSys::point( const std::vector< std::string >& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    const std::size_t num_verts = 1;
    switch ( args.size() ) {
        case 2:
            DrawShape( "Point", "", args.data()+1, num_verts );
            break;
        case 3:
            DrawShape( "Point", args[1], args.data()+2, num_verts );
            break;
        default:
            ERR("usage: point [optioanl_name] [vec3f]\n");
            return;
    }
}

void CommandSys::sphere( const std::vector< std::string >& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
//    const std::size_t num_verts = 1;
    switch ( args.size() ) {
        case 3:
//            DrawShape( "Sphere", "", args.data()+1, num_verts, PrimitiveShapeT::SPHERE, String::ToFloat( args[2] ) );
            break;
        case 4:
//            DrawShape( "Sphere", args[1], args.data()+2, num_verts, PrimitiveShapeT::SPHERE, String::ToFloat( args[3] ) );
            break;
        default:
            ERR("usage: sphere [optioanl_name] [vec3f_origin] [float_radius]\n");
            return;
    }
}

void CommandSys::aabox( const std::vector< std::string >& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    bool ret = false;
    Vec3f origin;
    const std::string* str_origin;
    switch ( args.size() ) {
        case 8:
            str_origin = &args[1];
            ret = String::ToVec3f( args[1], origin );
            if ( ret )
                DrawAABox( "", args.data()+2, origin );
            break;
        case 9:
            str_origin = &args[2];
            ret = String::ToVec3f( args[2], origin );
            if ( ret )
                DrawAABox( args[1], args.data()+3, origin );
            break;
        default:
            ERR("usage: aabox [optioanl_name] [origin] [min_x] [max_x] [min_y] [max_y] [min_z] [max_z]\n");
            return;
    }
    if ( ! ret ) {
        ERR( "Invalid origin: %s\n", str_origin->c_str() );
    }
    return;
}

void CommandSys::capsule( const std::vector< std::string >& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;

    std::string name;
    uint param = 1;

    switch ( args.size() ) {
        case 5:
            break;
        case 6:
            name = args[1];
            ++param;
            break;
        default:
            ERR("usage: capsule [optioanl_name] [origin] [normal] [radius] [len]\n");
            return;
    }
}

void CommandSys::line( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    const std::size_t num_verts = 2;
    switch ( args.size() ) {
        case 3:
            DrawShape( "Line", "", args.data()+1, num_verts );
            break;
        case 4:
            DrawShape( "Line", args[1], args.data()+2, num_verts );
            break;
        default:
            ERR("usage: line [optional_name] [vec3f] [vec3f]\n");
            return;
    }
}

void CommandSys::tri( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    const std::size_t num_verts = 3;
    switch ( args.size() ) {
        case 4:
            DrawShape( "Triangle", "", args.data()+1, num_verts );
            break;
        case 5:
            DrawShape( "Triangle", args[1], args.data()+2, num_verts );
            break;
        default:
            ERR("usage: tri [optional_name] [vec3f] [vec3f] [vec3f]\n");
            return;
    }
}

void CommandSys::tet( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    const std::size_t num_verts = 4;
    switch ( args.size() ) {
        case 5:
            DrawShape( "Tetrahedron", "", args.data()+1, num_verts );
            break;
        case 6:
            DrawShape( "Tetrahedron", args[1], args.data()+2, num_verts );
            break;
        default:
            ERR("usage: tet [optional_name] [vec3f] [vec3f] [vec3f] [vec3f]\n");
            return;
    }
}

void CommandSys::shape( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    if ( args.size() < 2 ) {
        ERR("usage: shape [optional_name] [vec3f] ... [vec3f]\n");
        return;
    }

    Vec3f test;
    if ( String::ToVec3f( args[1], test ) ) {
        DrawShape( "Shape", "", args.data()+1, args.size()-1 );
        return;
    }

    DrawShape( "Shape", args[1], args.data()+2, args.size()-2 );
}

void CommandSys::delallshapes( [[maybe_unused]] const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
}

void CommandSys::dellastshape( [[maybe_unused]] const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
}

void CommandSys::delshape( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    if ( args.size() < 2 ) {
        ERR("usage: delshape [index_or_name] ... \n");
        return;
    }
}

void CommandSys::namelastshape( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    if ( args.size() < 2 ) {
        ERR("usage: namelastshape [newname] \n");
        return;
    }
}

void CommandSys::nameshape( const std::vector<std::string>& args ) {
    WARN("This command is not implemented yet.\n"); //toimplement
    return;
    
    if ( args.size() < 3 ) {
        ERR("usage: nameshape [oldname_or_index] [newname] \n");
        return;
    }

//    const std::string& oldname = args[1];
//    const std::string& newname = args[2];
}
