// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../commandsys.h"

#include "../console.h"

void CommandSys::Math_Init( void ) {
    commands.Set("norm", &norm );
    commands.Set("normalize", &normalize );
    commands.Set("mod", &mod );
    commands.Set("dot", &dot );
    commands.Set("cross", &cross );
    commands.Set("cos", &cos );
    commands.Set("acos", &acos );
    commands.Set("sin", &sin );
    commands.Set("asin", &asin );
    commands.Set("tan", &tan );
    commands.Set("atan", &atan );
    commands.Set("sqrt", &sqrt );
    commands.Set("sq", &sq );
    commands.Set("len", &len );
}

void CommandSys::cos( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: cos [float]\n");
        return;
    }

    last_float = cosf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("cos %f = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::acos( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: acos [float]\n");
        return;
    }

    last_float = acosf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("acos %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::sin( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: sin [float]\n");
        return;
    }

    last_float = sinf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("sin %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::asin( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: asin [float]\n");
        return;
    }

    last_float = asinf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("asin %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::tan( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: tan [float]\n");
        return;
    }

    last_float = tanf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("tan %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::atan( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: atan [float]\n");
        return;
    }

    last_float = atanf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("atan %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::sqrt( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: sqrt [float]\n");
        return;
    }

    last_float = sqrtf( GetFloat( args[1] ) );
    Console::PrintFloat( last_float );
    LOG("sqrt %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::sq( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: sq [float]\n");
        return;
    }

    const float val1 = GetFloat( args[1] );
    last_float = val1 * val1;
    Console::PrintFloat( last_float );
    LOG("sq %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::len( const std::vector< std::string >& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: len [vec3f]\n");
        return;
    }

    Vec3f a;
    if ( ! GetVec3f( args[1], a ) )
        return;

    last_float = a.Len();
    Console::PrintFloat( last_float );
    LOG("len %s = %f\n", args[1].c_str(), static_cast<double>(last_float) );
}

void CommandSys::cross( const std::vector<std::string>& args ) {
    if ( args.size() != 3 ) {
        ERR("usage: cross [vec3f] [vec3f]\n");
        return;
    }

    Vec3f a;
    if ( ! GetVec3f( args[1], a ) )
        return;

    Vec3f b;
    if ( ! GetVec3f( args[2], b ) )
        return;

    last_vec = a.Cross( b );
    float f[3]{
        last_vec.x,
        last_vec.y,
        last_vec.z
    };
    Console::PrintFloats( f, 3 );
    LOG("cross (%s) (%s) = (%f,%f,%f)\n", args[1].c_str(), args[2].c_str(),
        static_cast<double>(last_vec.x),
        static_cast<double>(last_vec.y),
        static_cast<double>(last_vec.z)
    );
}

void CommandSys::mod( const std::vector<std::string>& args ) {
    if ( args.size() != 3 ) {
        ERR("usage: mod [int] [int]\n");
        return;
    }

    last_float = GetInt(args[1]) % GetInt(args[2]);
    Console::PrintFloat( last_float );
    LOG("mod %s %s = %f\n", args[1].c_str(), args[2].c_str(), static_cast<double>(last_float) );
}

void CommandSys::dot( const std::vector<std::string>& args ) {
    if ( args.size() != 3 ) {
        ERR("usage: dot [vec3f] [vec3f]\n");
        return;
    }

    Vec3f a;
    if ( ! GetVec3f( args[1], a ) )
        return;

    Vec3f b;
    if ( ! GetVec3f( args[2], b ) )
        return;

    last_float = a.Dot( b );
    Console::PrintFloat( last_float );
    LOG("dot %s %s = %f\n", args[1].c_str(), args[2].c_str(), static_cast<double>(last_float) );
}

void CommandSys::norm( const std::vector<std::string>& args ) {
    if ( args.size() != 4 ) {
        ERR("usage: norm [vec3f] [vec3f] [vec3f]\n");
        return;
    }

    Vec3f a;
    if ( ! GetVec3f( args[1], a ) )
        return;

    Vec3f b;
    if ( ! GetVec3f( args[2], b ) )
        return;

    Vec3f c;
    if ( ! GetVec3f( args[3], c ) )
        return;

    last_vec = Vec3f::GetNormalized( (a-b).Cross(a-c) );
    float fcw[3]{last_vec.x,last_vec.y,last_vec.z};
    Console::PrintFloats( fcw, 3, " clockwise (engine default. negate for ccw)" );
    LOG("norm %s = %f\n", args[1].c_str(), static_cast<double>(last_vec.x),static_cast<double>(last_vec.y),static_cast<double>(last_vec.z) );
}

void CommandSys::normalize( const std::vector<std::string>& args ) {
    if ( args.size() != 2 ) {
        ERR("usage: normalize [vec3f]\n");
        return;
    }

    Vec3f a;
    if ( ! GetVec3f( args[1], a ) )
        return;

    last_vec = Vec3f::GetNormalized( a );
    float fcw[3]{last_vec.x,last_vec.y,last_vec.z};
    Console::PrintFloats( fcw, 3, " normalized" );
    LOG("norm %s = %f\n", args[1].c_str(), static_cast<double>(last_vec.x),static_cast<double>(last_vec.y),static_cast<double>(last_vec.z) );
}

int CommandSys::GetInt( const std::string& str ) {
    return (str == "last") ? static_cast<int>(last_float) : String::ToInt( str );
}

float CommandSys::GetFloat( const std::string& str ) {
    return (str == "last") ? last_float : String::ToFloat( str );
}

uint CommandSys::GetUInt( const std::string& str ) {
    return (str == "last") ? static_cast<uint>(last_float) : String::ToUInt( str );
}

bool CommandSys::GetVec3f( const std::string& str, Vec3f& out ) {
    if ( str == "last" )
        out = last_vec;

    if ( String::ToVec3f(str,out) )
        return true;

    ERR("Invalid Vec3f: %s\n", str.c_str() );
    return false;
}
