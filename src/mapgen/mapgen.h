// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../math/geometry/3d/aabox.h"

typedef uint NeighborCellID_BaseType;
enum class NeighborCellID : NeighborCellID_BaseType {
    ANY
    , DIFFERENT
};

struct MapCellPos {
    MapCellPos( const Vec3f& _origin, const uint _col, const uint _row ) : origin( _origin ), col( _col ), row( _row ) {}
    Vec3f origin;
    uint col;
    uint row;
};

typedef uint DoorSideT_BaseType;
enum class DoorSideT : DoorSideT_BaseType {
    NONE = 0
    , TOP = 1
    , BOTTOM = 2
    , LEFT = 4
    , RIGHT = 8
    , ALL = TOP | BOTTOM | LEFT | RIGHT
};

BITWISE_OPERATORS_FOR_ENUM_CLASS( DoorSideT, DoorSideT_BaseType )

struct MapCellInfo {
    MapCellInfo( const uint _corridorID ) : sidesWithDoor( DoorSideT::NONE ), corridorID( _corridorID ) {}
    DoorSideT sidesWithDoor;
    uint corridorID;
};

struct PrefabMap {
    PrefabMap( void ) = default;
    PrefabMap( const PrefabMap& other ) = delete;
    PrefabMap( const Vec2f& _size, const std::string& _name );
    PrefabMap& operator=( const PrefabMap& other ) = delete;
    
    PrefabMap( PrefabMap && other_rref );
    PrefabMap& operator=( PrefabMap && other_rref );
    
    Vec2f size;
    std::string name;
    std::vector< std::unique_ptr< FileMap > > file; //!< vector for randomization assets
    std::shared_ptr<uint> min_use_count; //!< min number of times this prefab will _attempt_ to be created in a generated map
    std::shared_ptr<uint> max_use_count; //!< min number of times this prefab will be created in a generated map. 0 for infinite.
};

class GeneratedRoom {
public:
    GeneratedRoom( void );
    GeneratedRoom( const GeneratedRoom& other );
    GeneratedRoom& operator=( const GeneratedRoom& other );

    GeneratedRoom( GeneratedRoom && other_rref );
    GeneratedRoom& operator=( GeneratedRoom && other_rref );
    
    AABox3D bounds;
    std::vector< std::shared_ptr< Entity > > ents;
};

class MapGen {
public:
    static bool Load( File& openedGenFile );
    static AABox3D GetRoomBounds( const std::vector< std::shared_ptr< Entity > >& ents );

private:
    static bool GenerateSquareMap( const std::vector< PrefabMap >& prefabs, const Vec2f& cellSize, const Vec2f& gridSize, const std::string& map_theme );
    static bool LoadPrefabs( std::vector< PrefabMap >& prefab_info );
    static uint CreateRoom( const std::vector< PrefabMap >& prefabs, std::vector< uint >& prefabIndices, std::vector< std::vector< GeneratedRoom > >& roomGrid, const MapCellPos& cell );
    static void RandomlyRotateRooms( const std::vector< std::vector< GeneratedRoom > >& roomGrid );
    static void SetRoomDimensions( std::vector< std::vector< GeneratedRoom > >& roomGrid );
    
    static void CreateAllDoors( std::vector< std::vector< GeneratedRoom > >& roomGrid );
    static bool CreateDoorOnRandomSide( std::vector< std::vector< GeneratedRoom > >& roomGrid, std::vector< std::vector< MapCellInfo > >& cells, const uint col, const uint row, const NeighborCellID matchID );
    static bool CreateDoorBetweenRooms( GeneratedRoom& room_from, GeneratedRoom& room_to, const DoorSideT side );
    static void ConnectIslands( std::vector< std::vector< GeneratedRoom > >& roomGrid, std::vector< std::vector< MapCellInfo > >& cells );
    static void ConnectRooms( std::vector< uint >& allCorridorIDs, std::vector< std::vector< MapCellInfo > >& cells, std::vector< std::vector< GeneratedRoom > >& roomGrid, const uint col, const uint row );
    
    static uint CombineAllWallSegs( std::vector< std::vector< GeneratedRoom > >& roomGrid, const std::string& map_theme ); //!< returns how many 1x1 walls were combined
    static uint CombineWallSegs( std::vector< std::shared_ptr< Entity > >& wall, const Vec3f& facing_dir, const std::string& map_theme ); //!< returns how many 1x1 walls were combined
    static void RemoveEnts( std::vector< std::shared_ptr< Entity > >& ents_to_remove, std::vector< std::shared_ptr< Entity > >& from );
    
    static void SpawnEntCopies( const std::vector< std::vector< GeneratedRoom > >& roomGrid, const std::string& map_theme );
};
