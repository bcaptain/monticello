// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "../game.h"
#include "../entities/entity.h"
#include "../entities/entityInfoManager.h"
#include "../misc.h"
#include "../files/assetManager.h"
#include "./mapgen.h"

PrefabMap::PrefabMap( const Vec2f& _size, const std::string& _name )
    : size( _size )
    , name( _name )
    , file()
    , min_use_count( std::make_shared<uint>( 0 ) )
    , max_use_count( std::make_shared<uint>( 0 ) )
{ }

PrefabMap::PrefabMap( PrefabMap && other_rref )
    : size( other_rref.size )
    , name( other_rref.name )
    , file( std::move( other_rref.file ) )
    , min_use_count( std::move( other_rref.min_use_count ) )
    , max_use_count( std::move( other_rref.max_use_count ) )
{ }

PrefabMap& PrefabMap::operator=( PrefabMap && other_rref ) {
    size = other_rref.size;
    name = other_rref.name;
    file = std::move( other_rref.file );
    min_use_count = std::move( other_rref.min_use_count );
    max_use_count = std::move( other_rref.max_use_count );
    return *this;
}

GeneratedRoom::GeneratedRoom( void )
    : bounds()
    , ents()
{}

GeneratedRoom::GeneratedRoom( const GeneratedRoom& other )
    : bounds( other.bounds )
    , ents( other.ents )
{}

GeneratedRoom& GeneratedRoom::operator=( const GeneratedRoom& other ) {
    bounds = other.bounds;
    ents = other.ents;
    return *this;
}

GeneratedRoom::GeneratedRoom( GeneratedRoom && other_rref )
    : bounds( other_rref.bounds )
    , ents( std::move( other_rref.ents ) )
{}

GeneratedRoom& GeneratedRoom::operator=( GeneratedRoom && other_rref ) {
    bounds = other_rref.bounds;
    ents = std::move( other_rref.ents );
    return *this;
}

bool MapGen::Load( File& openedGenFile ) {
    if ( ! openedGenFile.IsOpen() )
        return false;
        
    ParserText parser( openedGenFile );
    parser.SetCommentIndicator('#');
    
    // ** we load in all the possible arguments before processing, so we can do them in the right order.
    
    std::string cellSizeEntry;
    std::string gridSizeEntry;
    std::vector< PrefabMap > prefabs;
    std::string map_theme;
    
    std::vector< std::string > line;
    std::string word;
    
    Vec2f prefab_size;
    
    bool found_1x1_prefab = false;
    
    while ( !openedGenFile.EndOfFile() ) {
        line.clear();
        while ( parser.ReadWordOnLine( word ) )
            line.emplace_back( word );
            
        parser.SkipToNextNonblankLine();

        switch ( line.size() ) {
            case 2:
                if ( line[0] == "theme" ) {
                    map_theme = line[1];
                    continue;
                } else if ( line[0] == "gridSize" ) {
                    gridSizeEntry = line[1];
                    continue;
                } else if ( line[0] == "cellSize" ) {
                    cellSizeEntry = line[1];
                    continue;
                }
                break;
            case 5:
                if ( line[0] == "prefab" ) {
                    String::ToVec2f( line[1], prefab_size );
                    if ( prefab_size.x < 1 || prefab_size.y < 1 ) {
                        ERR("MapGen::LoadMapGenerator: %s: Invalid prefab size: %s.\n", openedGenFile.GetFilePath().c_str(), line[1].c_str() );
                        continue;
                    }
                    prefabs.emplace_back( prefab_size, line[4] );
                    *prefabs[prefabs.size()-1].min_use_count = String::ToUInt( line[2] );
                    *prefabs[prefabs.size()-1].max_use_count = String::ToUInt( line[3] );
                    if ( ! found_1x1_prefab && line[1] == "1,1" && line[2] == "0" && line[3] == "0" ) {
                        found_1x1_prefab = true;
                    }
                    continue;
                }
                break;
            case 0: continue;
            default: break;
        }
        word.clear();
        for ( uint i=0; i<line.size(); ++ i ) {
            word += line[i];
            word += " ";
        }
        ERR("MapGen::LoadMapGenerator: %s: Unknown entry: %s.\n", openedGenFile.GetFilePath().c_str(), word.c_str() );
    }

    if ( !found_1x1_prefab ) {
        ERR("MapGen::LoadMapGenerator: %s: no \"prefab 1,1 0 0\" entry provided.\n", openedGenFile.GetFilePath().c_str() );
        return false;
    }

    if ( map_theme.size() == 0 ) {
        ERR("MapGen::LoadMapGenerator: %s: no theme entry provided.\n", openedGenFile.GetFilePath().c_str() );
        return false;
    }

    if ( gridSizeEntry.size() == 0 ) {
        ERR("MapGen::LoadMapGenerator: %s: no gridSize entry provided.\n", openedGenFile.GetFilePath().c_str() );
        return false;
    }

    if ( cellSizeEntry.size() == 0 ) {
        ERR("MapGen::LoadMapGenerator: %s: no cellSize entry provided.\n", openedGenFile.GetFilePath().c_str() );
        return false;
    }

    Vec2f gridSize;
    const std::vector< std::string > gridSizeVec =String::ParseStringList( gridSizeEntry, "x" ); 
    if ( gridSizeVec.size() == 2 ) {
        gridSize.x=String::ToUInt( gridSizeVec[0] );
        gridSize.y=String::ToUInt( gridSizeVec[1] );
    }
    
    if ( gridSize.x < 1 || gridSize.y < 1 ) {
        ERR("MapGen::LoadMapGenerator: %s: invalid gridSize: %s.\n", openedGenFile.GetFilePath().c_str(), gridSizeEntry.c_str() );
    }

    Vec2f cellSize;
    std::vector< std::string > cellSizeVec =String::ParseStringList( cellSizeEntry, "x" ); 
    if ( cellSizeVec.size() == 2 ) {
        cellSize.x=String::ToUInt( cellSizeVec[0] );
        cellSize.y=String::ToUInt( cellSizeVec[1] );
    }
    
    if ( cellSize.x < 1 || cellSize.y < 1 ) {
        ERR("MapGen::LoadMapGenerator: %s: invalid cellSize: %s.\n", openedGenFile.GetFilePath().c_str(), cellSizeEntry.c_str() );
        return false;
    }
    
    if ( ! LoadPrefabs( prefabs ) )
        return false;
    
    return GenerateSquareMap( prefabs, cellSize, gridSize, map_theme );
}

bool MapGen::LoadPrefabs( std::vector< PrefabMap >& prefabs ) {
    if ( prefabs.size() < 1 )
        return false;
        
    uint successful_loads=0;
    
    for ( uint p=0; p<prefabs.size(); ++p ) {
        auto & prefab = prefabs[p];
        
        prefab.file.emplace_back ( std::make_unique< FileMap >() );
        auto & file = *prefab.file[prefab.file.size() - 1];
        
        // ** load the file
        
        if ( !OpenAssetFile( "Prefab", prefab.name.c_str(), game->map_manifest, file ) ) {
            ERR("Could not find prefab map %s\n", prefab.name.c_str() );
            ShuffleErase( prefabs, p );
            --p;
            continue;
        }

        if ( !file.Load() ) {
            ERR("Could not load prefab map %s\n", prefab.name.c_str() );
            ShuffleErase( prefabs, p );
            --p;
            continue;
        }
        
        CMSG_LOG("Prefab map %s loaded from %s\n", file.GetFilePath().c_str(), file.GetFileContainerName_Full().c_str() );
        ++successful_loads;
            
        // load any randomizations of the file
        prefab.file.emplace_back( std::make_unique< FileMap >() );
        uint idx=1;
        std::string prefab_name_random( prefab.name );
        prefab_name_random += std::to_string( idx );
        while ( OpenAssetFile_NoErr( "Prefab", prefab_name_random.c_str(), game->map_manifest, *prefab.file[prefab.file.size()-1] ) ) {
            auto const & rnd_prefab = *prefab.file[prefab.file.size()-1];
            
            if ( prefab.file[prefab.file.size()-1]->Load() ) {
                CMSG_LOG("Prefab map %s loaded from %s\n", rnd_prefab.GetFilePath().c_str(), rnd_prefab.GetFileContainerName_Full().c_str() );
                ++successful_loads;
                prefab.file.emplace_back( std::make_unique< FileMap >() );
            } else {
                ERR("Could not load prefab map %s\n", prefab.name.c_str() );
            }
            
            // set up the next randomization
            prefab_name_random = prefab.name;
            prefab_name_random += std::to_string( ++idx );
        }
        
        prefab.file.resize( prefab.file.size()-1 ); // the last one is not loaded
    }
    
    return successful_loads > 0;
}

uint MapGen::CreateRoom( const std::vector< PrefabMap >& prefabs, std::vector< uint >& prefabIndices, std::vector< std::vector< GeneratedRoom > >& roomGrid, const MapCellPos& cell ) {
    const uint rnd = Random::UInt( 0, prefabIndices.size()-1 );
    uint prefabIdx = prefabIndices[ rnd ];
    auto const & prefab = prefabs[prefabIdx];
    uint fileIdx = Random::UInt( 0, prefab.file.size()-1 );

    for ( auto const & ent : prefab.file[fileIdx]->GetEntList_Ref() ) {
        auto new_ent = ent->CloneShared(); 
        ASSERT( new_ent );
        new_ent->NudgeOrigin( cell.origin );
        roomGrid[cell.col][cell.row].ents.emplace_back( new_ent );
    }
    
    return rnd;
}

bool MapGen::GenerateSquareMap( const std::vector< PrefabMap >& prefabs, const Vec2f& cellSize, const Vec2f& gridSize, const std::string& map_theme ) {
    const uint gridSizeX = static_cast<uint>(gridSize.x);
    const uint gridSizeY = static_cast<uint>(gridSize.y);
    
    const Vec2f offset_end( cellSize.x*(gridSize.x/2), cellSize.y*(gridSize.y/2) );
    const Vec2f offset_start( -offset_end );
    
    auto prefab1x1Indices = std::vector< uint >{};
    auto mandatoryIndices = std::vector< uint >{};

// ** attempting to implement rooms bigger than 1x1:
//    auto roomIDs = std::vector< std::vector< uint > >( gridSizeX, std::vector< uint >( gridSizeY, 0 ) ); // a zero room ID means there is no room
//    auto availableRoomIndices = std::vector< int >{}; // indices in roomIDs that are equal to 0 
//    availableRoomIndices.reserve( gridSizeX * gridSizeY );
//    for (uint i=0; i< gridSize.x * gridSize.y; ++i )
//        availableRoomIndices.emplace_back(i);

    // ** get prefab indices
    for ( uint i=0; i<prefabs.size(); ++i ) {
        while ( *prefabs[i].min_use_count > 0 ) {
            mandatoryIndices.emplace_back(i);
            --(*prefabs[i].min_use_count);
        }
        
        if ( prefabs[i].size == Vec2f(1,1) ) {
            prefab1x1Indices.emplace_back(i);
            continue;
        }
    }
    
    if ( prefab1x1Indices.size() < 1 ) {
        ERR("MapGen::GenerateSquareMap: No 1x1 prefabs given.\n");
        return false;
    }
    
    // ** create the cell information

    std::vector< MapCellPos > cells;
    cells.reserve( gridSizeX * gridSizeY );
    
    uint col = 0;
    Vec3f offset( -offset_end.x, -offset_end.y, 0 );
    while ( offset.x < offset_end.x ) {
        offset.y = offset_start.y;
        uint row = 0;
        while ( offset.y < offset_end.y ) {
            const Vec3f halfCellSize(cellSize.x/2, cellSize.y/2, 0); // add half cell size because the room's origin is in the center of the room
            cells.emplace_back( offset + halfCellSize, col, row );
            
            offset.y += cellSize.y;
            ++row;
        }
        offset.x += cellSize.x;
        ++col;
    }
    
    std::random_shuffle( cells.begin(), cells.end() ); // randomize order that cells will be created in
    
    // ** fill map in with mandatory prefabs
    std::vector< std::vector< GeneratedRoom > > roomGrid( gridSizeX, std::vector< GeneratedRoom >( gridSizeY ) );
    while ( cells.size() > 0 ) {
        if ( mandatoryIndices.size() < 1 )
            break;

        const uint prefabIdx = CreateRoom( prefabs, mandatoryIndices, roomGrid, cells[cells.size()-1] );
        ShuffleErase( mandatoryIndices, prefabIdx );
        
        cells.pop_back();
    }
    
    // ** fill map in with 1x1 prefabs
    for ( auto const & cell : cells ) {
        const uint prefabIdx = CreateRoom( prefabs, prefab1x1Indices, roomGrid, cell );
        
        if ( *prefabs[prefabIdx].max_use_count > 0 ) {
            if ( *prefabs[prefabIdx].max_use_count > 1 ) {
                --(*prefabs[prefabIdx].max_use_count);
            } else {
                ShuffleErase( prefab1x1Indices, prefabIdx );
            }
        }
    }

    SetRoomDimensions( roomGrid );
    RandomlyRotateRooms( roomGrid );
    CreateAllDoors( roomGrid );
    CombineAllWallSegs( roomGrid, map_theme );
    SpawnEntCopies( roomGrid, map_theme );
    return true;
}

std::shared_ptr< Entity > GetGenericWallFacingDir( std::vector< std::shared_ptr< Entity > >& ents, const Vec3f& unit_dir );
std::shared_ptr< Entity > GetGenericWallFacingDir( std::vector< std::shared_ptr< Entity > >& ents, const Vec3f& unit_dir ) {
    for ( uint e = 0; e<ents.size(); ++e ) {
        auto & ent = ents[e];
        if ( ent->GetEntName() != "generic_wall" )
            continue;
        
        if ( ent->GetFacingDir() == unit_dir ) {
            return ent;
        }
    }
    
    return {};
}

std::shared_ptr< Entity > GetGenericAdjacentWall( std::vector< std::shared_ptr< Entity > >& ents, const Entity& curWall, const Vec3f& unit_dir );
std::shared_ptr< Entity > GetGenericAdjacentWall( std::vector< std::shared_ptr< Entity > >& ents, const Entity& curWall, const Vec3f& unit_dir ) {
    Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( curWall.bounds.primitive.get() );
    const auto box3d = box->GetOrientedBox3D();
    const Vec3f neighborPos( box3d.GetCenter() + unit_dir );
    
    std::vector< uint > idx;
    idx.reserve( ents.size() );
    for ( uint e=0; e<ents.size(); ++e )
        idx.emplace_back( e );
    std::random_shuffle( idx.begin(), idx.end() );

    for ( uint e = 0; e<idx.size(); ++e ) {
        auto & ent = ents[idx[e]];
        if ( ent->GetEntName() != "generic_wall" ) {
            continue;
        }

        if ( ent->bounds.primitive->Intersects( neighborPos ) ) {
            return ent;
        }
    }
    
    return {};
}

bool MapGen::CreateDoorBetweenRooms( GeneratedRoom& room_from, GeneratedRoom& room_to, const DoorSideT side ) {
    Vec3f dir;
    Vec3f dir_perpendicular;
    
    switch ( side ) {
        case DoorSideT::BOTTOM:
            dir.Set(0,-1,0);
            dir_perpendicular.Set(1,0,0);
            break;
        case DoorSideT::TOP:
            dir.Set(0,1,0);
            dir_perpendicular.Set(1,0,0);
            break;
        case DoorSideT::LEFT:
            dir.Set(-1,0,0);
            dir_perpendicular.Set(0,1,0);
            break;
        case DoorSideT::RIGHT:
            dir.Set(1,0,0);
            dir_perpendicular.Set(0,1,0);
            break;
        case DoorSideT::NONE:
            FALLTHROUGH;
        case DoorSideT::ALL:
            FALLTHROUGH;
        default:
            ERR("MapGen::CreateDoorOnRandomSide: Invalid Room Side.\n");
            return false;
    }

    // ** find the generic entities in the first room that will be part of our door
    std::vector< std::shared_ptr< Entity > > cur_ents;
    
    // any of the starting room's walls facing away from the adjacent room will do
    auto ent = GetGenericWallFacingDir( room_from.ents, -dir );
    if ( !ent ) {
        return false;
    }
    cur_ents.emplace_back( ent );

    // find starting room's walls connected to the first entity we found
    auto first_ent = ent;
    while ( cur_ents.size() < 4 && ent ) {
        if ( ( ent = GetGenericAdjacentWall( room_from.ents, *ent, dir_perpendicular ) ) ) {
            cur_ents.emplace_back( ent );
        }
    }
    
    // if we didn't find enough going that direction, try the other
    ent = first_ent;
    while ( cur_ents.size() < 4 && ent ) {
        if ( ( ent = GetGenericAdjacentWall( room_from.ents, *ent, -dir_perpendicular ) ) ) {
            cur_ents.emplace_back( ent );
        }
    }
    
    // we need 4 entities to make a door
    if ( cur_ents.size() < 4 )
        return false;
        
    // ** find the generic entities in the second room that will be part of our door
    std::vector< std::shared_ptr< Entity > > adjacent_ents;
    
    // for each of the 4 entities in the starting room, we need to find the wall entitie directly behind them in the adjacent room
    for ( auto const & from_ent : cur_ents ) {
        if ( ( ent = GetGenericAdjacentWall( room_to.ents, *from_ent, dir ) ) ) {
            adjacent_ents.emplace_back( ent );
        } else {
            GetGenericAdjacentWall( room_to.ents, *from_ent, dir );
            return false;
        }
    }
    
    // ** make the door
    
    for ( auto const & remove_ent : cur_ents ) {
        for ( uint e=0; e<room_from.ents.size(); ++e ) {
            auto const & from_ent = room_from.ents[e];
            if ( remove_ent->GetIndex() == from_ent->GetIndex() ) {
                ShuffleErase( room_from.ents, e );
                break;
            }
        }
    }

    for ( auto const & remove_ent : adjacent_ents ) {
        for ( uint e=0; e<room_to.ents.size(); ++e ) {
            auto const & to_ent = room_to.ents[e];
            if ( remove_ent->GetIndex() == to_ent->GetIndex() ) {
                ShuffleErase( room_to.ents, e );
                break;
            }
        }
    }
    
    return true;
}

bool MapGen::CreateDoorOnRandomSide( std::vector< std::vector< GeneratedRoom > >& roomGrid, std::vector< std::vector< MapCellInfo > >& cells, const uint col, const uint row, const NeighborCellID matchID ) {
    ASSERT( roomGrid.size() > 0 );
    ASSERT( roomGrid[0].size() > 0 );
    
    struct DoorConnection {
        DoorConnection( const uint _col, const uint _row, const DoorSideT _doorSide, const DoorSideT _neighborDoorSide ) : col(_col), row(_row), doorSide(_doorSide), neighborDoorSide(_neighborDoorSide) {}
        uint col;
        uint row;
        DoorSideT doorSide;
        DoorSideT neighborDoorSide;
    };
    
    std::vector< DoorConnection > possibleConnections;
    possibleConnections.reserve(4);
    
    if ( col > 0 && !BitwiseAnd(cells[col][row].sidesWithDoor, DoorSideT::LEFT) ) {
        const uint n_row = row;
        const uint n_col = col-1;
        
        if ( ( matchID == NeighborCellID::ANY ) || cells[n_col][n_row].corridorID != cells[col][row].corridorID ) {
            possibleConnections.emplace_back(n_col,n_row,DoorSideT::LEFT,DoorSideT::RIGHT);
        }
    }
    
    if ( col < roomGrid.size()-1 && !BitwiseAnd(cells[col][row].sidesWithDoor, DoorSideT::RIGHT) ) {
        const uint n_row = row;
        const uint n_col = col+1;
        
        if ( ( matchID == NeighborCellID::ANY ) || cells[n_col][n_row].corridorID != cells[col][row].corridorID ) {
            possibleConnections.emplace_back(n_col,n_row,DoorSideT::RIGHT,DoorSideT::LEFT);
        }
    }
    
    if ( row < roomGrid[0].size()-1 && !BitwiseAnd(cells[col][row].sidesWithDoor, DoorSideT::TOP) ) {
        const uint n_row = row+1;
        const uint n_col = col;
        
        if ( ( matchID == NeighborCellID::ANY ) || cells[n_col][n_row].corridorID != cells[col][row].corridorID ) {
            possibleConnections.emplace_back(n_col,n_row,DoorSideT::TOP,DoorSideT::BOTTOM);
        }
    }
    
    if ( row > 0 && !BitwiseAnd(cells[col][row].sidesWithDoor, DoorSideT::BOTTOM) ) {
        const uint n_row = row-1;
        const uint n_col = col;
        
        if ( ( matchID == NeighborCellID::ANY ) || cells[n_col][n_row].corridorID != cells[col][row].corridorID ) {
            possibleConnections.emplace_back(n_col,n_row,DoorSideT::BOTTOM,DoorSideT::TOP);
        }
    }
    
    std::random_shuffle( possibleConnections.begin(), possibleConnections.end() );
    
    for ( auto const & connection : possibleConnections ) {
        if ( CreateDoorBetweenRooms( roomGrid[col][row], roomGrid[connection.col][connection.row], connection.doorSide ) ) {
            cells[col][row].sidesWithDoor |= connection.doorSide;
            cells[connection.col][connection.row].sidesWithDoor |= connection.neighborDoorSide;
            return true;
        }
    }
    
    if ( globalVals.GetBool( gval_d_mapDebug ) )
        WARN("MapGen::CreateDoor: Could not create door%s.\n", matchID == NeighborCellID::DIFFERENT ? " when connecting islands" : "" );
        
    return false;
}

void MapGen::CreateAllDoors( std::vector< std::vector< GeneratedRoom > >& roomGrid ) {
    // ** set up the initial corridoor IDs and cells
    auto cells = std::vector< std::vector< MapCellInfo > >( roomGrid[0].size() );
    cells.resize( roomGrid.size() );
    
    // ** make sure every room has at least one door.
    for ( uint col=0; col<cells.size(); ++col ) {
        for ( uint row=0; row<cells[col].size(); ++row ) {
            auto & cell = cells[col][row];
            if ( cell.sidesWithDoor != DoorSideT::NONE )
                continue;
                
            CreateDoorOnRandomSide( roomGrid, cells, col, row, NeighborCellID::ANY );
        }
    }
    
    ConnectIslands( roomGrid, cells );
}

void MapGen::ConnectIslands( std::vector< std::vector< GeneratedRoom > >& roomGrid, std::vector< std::vector< MapCellInfo > >& cells ) {
    // in order to prevent islands from being formes, we have to keep track of how many disparate corridors there are.
    // we start by assigning each and every cell it's own unique corridor ID.
    // then we'll go through and start adding doors, and sharing corridor IDs with rooms connected by doors.
    // we'll know we have no islands when there's only one corridor ID left.
    
    const uint numCells = roomGrid.size() * roomGrid[0].size();
    
    auto allCorridorIDs = std::vector< uint >{};
    allCorridorIDs.reserve( numCells );
    
    uint curID=0;
    for ( uint col=0; col<roomGrid.size(); ++col ) {
        for ( uint row=0; row<roomGrid[col].size(); ++row ) {
            cells[col].emplace_back( curID );
            allCorridorIDs.emplace_back( curID );
            ++curID;
        }
    
    }
    
    bool noVaidPossibleConnections = false;
    while ( noVaidPossibleConnections == false || allCorridorIDs.size() > 1 ) {
        // ** do initial check of all cell IDs for connected rooms
        for ( uint col=0; col<cells.size(); ++col ) {
            for ( uint row=0; row<cells[col].size(); ++row ) {
                ConnectRooms( allCorridorIDs, cells, roomGrid, col, row );
            }
        }
        
        CMSG_LOG("Total islands constructed: %u. Will attempt to connect them.\n", allCorridorIDs.size() );
        
        // ** connect all the islands by connecting them with two separate doors (in hopes to create a loop occasionally)
        
        // organize each island into it's own index of a vector
        std::vector< std::vector< std::pair<uint,uint> > > islands;
        islands.resize( numCells );
        for ( uint col=0; col<cells.size(); ++col ) {
            for ( uint row=0; row<cells[col].size(); ++row ) {
                auto const & cell = cells[col][row];
                islands[cell.corridorID].emplace_back( col, row );
            }
        }
        
        noVaidPossibleConnections = true; // will set to false if we find one
        
        // attempt to connect islands
        std::random_shuffle( islands.begin(), islands.end() ); //shuffle corridors
        uint connections=0;
        while ( islands.size() > 0 ) {
            const uint idx = islands.size()-1;
            const uint numWantedConnections = islands[idx].size() > 1 ? 2 : 1;
            std::random_shuffle( islands[idx].begin(), islands[idx].end() ); // shuffle rooms within the first corridoor
            
            for ( auto cell : islands[idx] ) {
                if ( ! CreateDoorOnRandomSide( roomGrid, cells, cell.first, cell.second, NeighborCellID::DIFFERENT ) )
                    continue;
                
                noVaidPossibleConnections = false;
                
                if ( ++connections == numWantedConnections )
                    break;
            }
            
            if ( ++connections == numWantedConnections )
                    break;
                    
            islands.pop_back();
        }
    }
    
    if ( allCorridorIDs.size() > 1 ) {
        ERR("Some places of the map are unreachable\n");
    }
}

void MapGen::ConnectRooms( std::vector< uint >& allCorridorIDs, std::vector< std::vector< MapCellInfo > >& cells, std::vector< std::vector< GeneratedRoom > >& roomGrid, const uint col, const uint row ) {
    uint& my_id = cells[col][row].corridorID;
    
    if ( BitwiseAnd( cells[col][row].sidesWithDoor, DoorSideT::BOTTOM ) ) {
        const uint n_col = col;
        const uint n_row = row-1;
        uint& neighbor_id = cells[n_col][n_row].corridorID;
        
        if ( neighbor_id != my_id ) {
            allCorridorIDs.erase( std::remove( std::begin(allCorridorIDs), std::end(allCorridorIDs), neighbor_id ), std::end(allCorridorIDs) );
            neighbor_id = my_id;
            ConnectRooms( allCorridorIDs, cells, roomGrid, n_col, n_row ); // tell our other neighbors to match us
        }
    }
    
    if ( BitwiseAnd( cells[col][row].sidesWithDoor, DoorSideT::TOP ) ) {
        const uint n_col = col;
        const uint n_row = row+1;
        uint& neighbor_id = cells[n_col][n_row].corridorID;
        
        if ( neighbor_id != my_id ) {
            allCorridorIDs.erase( std::remove( std::begin(allCorridorIDs), std::end(allCorridorIDs), neighbor_id ), std::end(allCorridorIDs) );
            neighbor_id = my_id;
            ConnectRooms( allCorridorIDs, cells, roomGrid, n_col, n_row ); // tell our other neighbors to match us
        }
    }
    
    if ( BitwiseAnd( cells[col][row].sidesWithDoor, DoorSideT::LEFT ) ) {
        const uint n_col = col-1;
        const uint n_row = row;
        uint& neighbor_id = cells[n_col][n_row].corridorID;
        
        if ( neighbor_id != my_id ) {
            allCorridorIDs.erase( std::remove( std::begin(allCorridorIDs), std::end(allCorridorIDs), neighbor_id ), std::end(allCorridorIDs) );
            neighbor_id = my_id;
            ConnectRooms( allCorridorIDs, cells, roomGrid, n_col, n_row ); // tell our other neighbors to match us
        }
    }
    
    if ( BitwiseAnd( cells[col][row].sidesWithDoor, DoorSideT::RIGHT ) ) {
        const uint n_col = col+1;
        const uint n_row = row;
        uint& neighbor_id = cells[n_col][n_row].corridorID;
        
        if ( neighbor_id != my_id ) {
            allCorridorIDs.erase( std::remove( std::begin(allCorridorIDs), std::end(allCorridorIDs), neighbor_id ), std::end(allCorridorIDs) );
            neighbor_id = my_id;
            ConnectRooms( allCorridorIDs, cells, roomGrid, n_col, n_row ); // tell our other neighbors to match us
        }
    }
}

uint MapGen::CombineWallSegs( std::vector< std::shared_ptr< Entity > >& wall, const Vec3f& facing_dir, const std::string& map_theme ) {
    if ( wall.size() < 2 )
        return 0;
    
    // ** get the origin of the new wall segment
    // get the farthest origin in the direction perpendicular (clockwise) of the facing direction. this is the origin.
    const Vec3f origDir = facing_dir.PerpCW_ZAxis();
    std::vector< Vec3f > origins;
    origins.reserve( wall.size() );
    for ( auto const & w : wall ) {
        origins.emplace_back( w->GetOrigin() );
    }
    const uint origin_idx = VertCloud::GetFarthestVertIndex( origins.data(), origins.size(), origDir );
    const Vec3f & newSegOrigin = origins[origin_idx];
    
    // ** spawn the new segmenet
    std::string wallName( map_theme );
    wallName += "_wall";
    wallName += std::to_string( wall.size() );
    wallName += "_straight";
    auto new_ent = entityInfoManager.Load( wallName.c_str(), newSegOrigin );
    if ( ! new_ent ) {
        ERR("MapGen::CombineAllWallSegs: Could not create wallseg: %s\n", wallName.c_str() );
        return 0;
    }
    
    new_ent->bounds.primitive->SetAngle( wall[0]->bounds.primitive->GetAngles() );
    new_ent->Spawn();
    return wall.size();
}

void MapGen::RemoveEnts( std::vector< std::shared_ptr< Entity > >& ents_to_remove, std::vector< std::shared_ptr< Entity > >& from ) {
    for ( auto const & combine : ents_to_remove ) {
        for ( uint i=0; i<from.size(); ++i ) {
            if ( combine->GetIndex() == from[i]->GetIndex() ) {
                ShuffleErase( from, i );
                // todomapgen: we should --i here!?
                break;
            }
        }
    }
}

uint MapGen::CombineAllWallSegs( std::vector< std::vector< GeneratedRoom > >& roomGrid, const std::string& map_theme ) {
    const std::vector< Vec3f > dirs{
        Vec3f(0,1,0),
        Vec3f(0,-1,0),
        Vec3f(1,0,0),
        Vec3f(-1,0,0)
    };
    const std::vector< int > unit_direction{-1,1};
    const std::vector< Vec3f > perps{
        Vec3f(Random::Element(unit_direction),0,0),
        Vec3f(Random::Element(unit_direction),0,0),
        Vec3f(0,Random::Element(unit_direction),0),
        Vec3f(0,Random::Element(unit_direction),0)
    };
    
    
    uint segs_combined = 0;
    std::vector< std::shared_ptr< Entity > > wall;
    wall.reserve(4);
    
    for ( auto & col : roomGrid ) {
        for ( auto & cell : col ) {
            uint dir=0;
            while ( dir < 4 ) {
                // randomize order in which entities will be checked
                std::vector<uint> idx;
                idx.reserve( cell.ents.size() );
                for ( uint i=0; i<cell.ents.size(); ++i )
                    idx.emplace_back( i );
                std::random_shuffle( idx.begin(), idx.end() );
                
                for ( uint e=0; e<idx.size(); ++e ) {
                    auto const & ent = cell.ents[e];
                    wall.clear();
                    
                    if ( ent->GetEntName() != "generic_wall" )
                        continue;
                        
                    if ( ent->GetFacingDir() != dirs[dir] )
                        continue;
                    
                    wall.emplace_back(ent);
                    
                    // find a stretch of wall segments going in the specified direction
                    auto check_ent = ent;
                    for ( uint i=1; i<3; ++i ) {
                        check_ent = GetGenericAdjacentWall( cell.ents, *check_ent, perps[dir] );
                        if ( !check_ent ) {
                            break;
                        }
                        
                        wall.emplace_back(check_ent);
                        if ( wall.size() >= 4 )
                            break;
                    }

                    // check the other direction along the wall if we didn't find the max amount
                    if ( wall.size() < 4 ) {
                        check_ent = ent;
                        for ( uint i=1; i<3; ++i ) {
                            check_ent = GetGenericAdjacentWall( cell.ents, *check_ent, -perps[dir] );
                            
                            if ( !check_ent ) {
                                break;
                            }
                            
                            wall.emplace_back(check_ent);
                            if ( wall.size() >= 4 )
                                break;
                        }
                    }
                        
                    if ( wall.size() < 2 )
                        continue;
                    
                    break;
                }
                
                // if any walls were combined, check again for more to combine in the same direction
                if ( CombineWallSegs( wall, dirs[dir], map_theme ) ) {
                    RemoveEnts( wall, cell.ents );
                    continue;
                }
                    
                ++dir;
            }
        }
    }
    
    if ( globalVals.GetBool( gval_d_mapDebug ) )
        CMSG("MapGen::CombineAllWallSegs: %u total wallsegs combined.\n", segs_combined );
        
    return segs_combined;
}

void MapGen::RandomlyRotateRooms( const std::vector< std::vector< GeneratedRoom > >& roomGrid ) {
    Vec3f room_min(0,0,0);
    Vec3f room_max(0,0,0);
    
    const uint rotations_size = 4;
    const float rotations[rotations_size] = {0,90,180,270};
    const Mat3f rotation_mats[rotations_size] = {
        {}
        , Mat3f( Vec3f(0,0,rotations[1]) )
        , Mat3f( Vec3f(0,0,rotations[2]) )
        , Mat3f( Vec3f(0,0,rotations[3]) )
    };
    
    for ( auto const & col : roomGrid ) {
        for ( auto const & cell : col ) {
            const uint rnd = Random::UInt(0,rotations_size-1);
            if ( rnd == 0 )
                continue;
                
            const Vec3f room_center = ( ( cell.bounds.max - cell.bounds.min ) / 2 ) + cell.bounds.min;
            for ( auto & ent : cell.ents ) {
                ent->bounds.primitive->NudgeYaw( rotations[rnd] );
                Vec3f origin = ent->GetOrigin();
                origin -= room_center;
                origin *= rotation_mats[rnd];
                origin += room_center;
                ent->SetOrigin( origin );
            }
        }
    }

}

AABox3D MapGen::GetRoomBounds( const std::vector< std::shared_ptr< Entity > >& ents ) {
    AABox3D bounds;
    
    for ( uint e=0; e<ents.size(); ++e ) {
        if ( ! ents[e] )
            continue;
            
        auto const & ent = ents[e];

        Vec3f ent_min;
        Vec3f ent_max;
        
        if ( ent->bounds.IsSphere() ) {
            Primitive_Sphere* sphere = static_cast< Primitive_Sphere* >( ent->bounds.primitive.get() );
            ent_max = ent_min = ent->GetOrigin();
            ent_min += sphere->GetRadius();
            ent_max -= sphere->GetRadius();
        } else if ( ent->bounds.IsAABox3D() ) {
            Primitive_AABox3D* box = static_cast< Primitive_AABox3D* >( ent->bounds.primitive.get() );
            const auto box3d = box->GetOrientedBox3D();
            ent_min.x = box3d.GetFarthestVert( Vec3f(-1,0,0) ).x;
            ent_min.y = box3d.GetFarthestVert( Vec3f(0,-1,0) ).y;
            ent_min.z = box3d.GetFarthestVert( Vec3f(0,0,-1) ).z;
            
            ent_max.x = box3d.GetFarthestVert( Vec3f(1,0,0) ).x;
            ent_max.y = box3d.GetFarthestVert( Vec3f(0,1,0) ).y;
            ent_max.z = box3d.GetFarthestVert( Vec3f(0,0,1) ).z;
        } else {
            ERR("RandomlyRotateRooms: Unknown/Unimplemented bounds shape for entity %s.\n", ent->GetIdentifier().c_str() );
        }
        
        if ( e == 0 ) {
            bounds.min = ent_min;
            bounds.max = ent_max;
            continue;
        }
        
        bounds.min.x = std::min( ent_min.x, bounds.min.x );
        bounds.min.y = std::min( ent_min.y, bounds.min.y );
        bounds.min.z = std::min( ent_min.z, bounds.min.z );
        
        bounds.max.x = std::max( ent_max.x, bounds.max.x );
        bounds.max.y = std::max( ent_max.y, bounds.max.y );
        bounds.max.z = std::max( ent_max.z, bounds.max.z );
    }
    
    return bounds;
}

void MapGen::SetRoomDimensions( std::vector< std::vector< GeneratedRoom > >& roomGrid ) {
    for ( auto & col : roomGrid ) {
        for ( auto & cell : col ) {
            cell.bounds = GetRoomBounds( cell.ents );
        }
    }
}

void MapGen::SpawnEntCopies( const std::vector< std::vector< GeneratedRoom > >& roomGrid, const std::string& map_theme ) {
    for ( auto const & col : roomGrid ) {
        for ( auto const & cell : col ) {
            for ( auto const & ent : cell.ents ) {
                std::shared_ptr< Entity > new_ent;
                if ( ent->GetEntName() == "generic_wall" ) {
                    // ** find an entity based on the generic one
                    std::string wallName( map_theme );
                    wallName += "_wall1_straight";
                    new_ent = entityInfoManager.Load( wallName.c_str(), ent->GetOrigin() );
                    if ( ! new_ent ) {
                        ERR("MapGen::SpawnEntCopies: Could not create wallseg: %s\n", wallName.c_str() );
                    } else {
                        new_ent->bounds.primitive->SetAngle( ent->bounds.primitive->GetAngles() );
                    }
                } else {
                    // ** create an exact copy of the entity
                    new_ent = ent->CloneShared();
                    game->InsertEntity( new_ent );
                }
                
                if ( new_ent ) {
                    new_ent->Spawn();
                }
            }
        }
    }
}
