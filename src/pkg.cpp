// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#include "./pkg.h"
#include "./game.h"
#include "./sound.h"
#include "./rendering/renderer.h"
#include "./rendering/models/modelManager.h"
#include "./rendering/materialManager.h"
#include "./rendering/fontManager.h"
#include "./damage/damageManager.h"
#include "./mapscripts/mapscriptManager.h"
#include "./entities/entityInfoManager.h"
#include "./particles/effectsManager.h"

void Packages::ManifestAll( void ) {
    CMSG_LOG("---- Manifest All Packages ----\n");

    ScanDir( DataDir.c_str(), &Packages::ManifestZip );
    ScanDir( DataDir.c_str(), &Packages::ManifestDir );

    // make sure notex is loaded
    if ( ! materialManager.Get("notex") )
        DIE("Could not find default texture \"notex\"\n");
        
    modelManager.ManifestAttachments();
}

void Packages::ManifestZip( const char* zip_path ) {
    if ( ! String::RightIs( zip_path, ".zip" ) )
        return; // don't warn, this is actually the proper function to check whether it's a zip file.

    FileZip zip_file( zip_path );
    if ( ! zip_file.IsOpen() ) {
        ERR("Couldn't Manifest Package: %s\n", zip_path );
        return;
    }

    std::string file_path;
    struct zip_stat stat;
    memset( &stat, 0, sizeof( stat ) );

    const Uint64 num_files = zip_file.NumFiles(); // we use Uint64 because that's what the ZIP specification calls for, according to libzip
    for ( Uint64 i=0; i<num_files; ++i )
        if ( zip_file.GetPathFromIndex( i, file_path ) )
            Manifest( file_path.c_str(), zip_path );
}

void Packages::ScanDir( const char* dir, ManifestFuncPtr func ) {
    ASSERT( dir != nullptr && dir[0] != '\0' );

    LinkList< std::string > files;
    FileSystem::ListDir(dir, files);
    files.SortData();

    std::string file_path;

    for ( auto& filename : files ) {
        if ( filename[ 0 ] == '.' )
            continue;

        file_path = dir;

        if ( file_path[ file_path.size() - 1] != '/' )
            file_path += '/';

        file_path += filename;
        func( file_path.c_str() );
    }
}

void Packages::ManifestDir( const char* file_path ) {
    if ( !FileSystem::Exists( file_path ) ) {
        ERR("Can't manifest file or directory, it doesn't exist: %s.\n", file_path);

    } else if ( FileSystem::IsDir( file_path ) ) {
        ScanDir( file_path, &ManifestDir ); // recurse

    } else {
        Manifest( file_path );
    }
}

void Packages::Manifest( const char* file_path, const char* file_container ) {

    if ( String::RightIs( file_path, ".vs" ) || String::RightIs( file_path, ".fs" ) ) {
        shaders->Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".glprog" ) ) {
        shaders->Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".glsl" ) ) {
        shaders->Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".ent" ) ) {
        entityInfoManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".dmg" ) ) {
        damageManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".mps" ) ) {
        mapScriptManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".tga" ) ) {
        textureManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".mtl" ) ) {
        materialManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".map" ) ) {
        game->RegisterMap( file_path, file_container );

    } else if ( String::RightIs( file_path, ".gen" ) ) {
        game->RegisterGen( file_path, file_container );

    } else if ( String::RightIs( file_path, ".ms" ) ) {
        game->RegisterMoverScript( file_path, file_container );

    } else if ( String::RightIs( file_path, ".fnt" ) ) {
        fontManager.Register( file_path, file_container );

    } else if ( FileExt_IsSoundFile( file_path ) ) {
        soundManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".fx" ) ) {
        effectsManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".bm1" ) ) {
        modelManager.Register( file_path, file_container );

    } else if ( String::RightIs( file_path, ".bm1anim" ) ) {
        modelManager.RegisterAnim( file_path, file_container );

    } else if ( String::RightIs( file_path, ".cue" ) ) {
        modelManager.RegisterAnimCue( file_path, file_container );

    } else if ( String::RightIs( file_path, ".events" ) ) {
        modelManager.RegisterAnimEvents( file_path, file_container );

    } else if ( String::RightIs( file_path, ".cfg" ) ) {
        // ignored here
    } else {
        WARN("Unknown file %s from %s.\n", file_path, file_container );
    }
}
