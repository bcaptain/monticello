// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./base/main.h"
#include "./game.h"
#include "./sound.h"
#include "./editor/editor.h"
#include "./pkg.h"
#include "./console.h"
#include "./rendering/renderer.h"
#include "./rendering/models/modelManager.h"
#include "./rendering/materialManager.h"
#include "./rendering/fontManager.h"
#include "./rendering/overlayfbo.h"
#include "./commandsys.h"
#include "./messages.h"
#include "./entities/actor.h"
#include "./entities/entityInfoManager.h"
#include "./particles/effectsManager.h"
#include "./damage/damageManager.h"
#include "./mapscripts/mapscriptManager.h"
#include "./nav/navMesh.h"
#include "./input/input.h"
#include "./ui/dialog/common.h"
#include "./gameui/crafting/items.h"
#include "./menuui/binds.h"
#include "./gameui/hud/stats.h"
#include "./menuui/menuui.h"
#include "./gameui/levelSummary.h"
#include "./script/moverScript.h"
#include "./sprites/spriteList.h"
#include "./entities/triggers/trigger_door.h"
#include "./entities/modules/flat.h"
#include "./mapgen/mapgen.h"
#include "./preload.h"

const std::string MONTICELLO_MARK("0"); // # Each mark represents a major public release of the monticello engine
const std::string MONTICELLO_VERSION("0.6.4"); // #versioning is Mark.Major.Minor

const std::string DataDir("./data/");

SDL sdl;

void CloseTopWindow_Click( void ) {
    if ( ! game )
        return;

    game->CloseTopWindow();
}

void OpenUIPage_Click( const std::vector< std::string >& pageName ) {
    if ( ! game )
        return;

    if ( ! VerifyArgNum( "OpenUIPage_Click", pageName, 1 ) )
        return;

    game->OpenUIPage( pageName[0] );
}

Game::Game( void )
    : world_ui()
    , hud_stats( new HUD_Stats )
    , ui_timer( new GameUI_Timer )
    , ui_infestLevel( new GameUI_InfestLevel )
    , entTree(nullptr)
    , sprites( new SpriteList[SPRITE_LAYERS] )
    , game_ui_pages()
    , gen_manifest()
    , map_manifest()
    , moverScript_manifest()
    , scheduler()
    , loaded_map()
    , next_map()
    , mapscript()
    , state(GameStateT::INIT)
    , player()
    , safezone()
    , prevFPSUpdate(0)
    , randnum()
    , preConsoleMessages()
    , entityEventStack()
    , all_entities()
    , awake_entities()
    , ents_to_delete()
    , game_events()
    , pause_state(PauseStateT::Unpaused)
#ifdef MONTICELLO_EDITOR
    , editor_open(false)
#endif // MONTICELLO_EDITOR
    , runOneFrame(false)
    , isInitialized(false)
    {

    ASSERT( !game ); // the global variable. we should have only one game object.
}

Window* Game::GetTopWindowOrDialog( void ) const {
    std::vector< UI_LayerT_BaseType > layers;
    layers.reserve(7);
    layers.push_back(UI_LAYER_GAMEUI);
    layers.push_back(UI_LAYER_GAMEUI_DIALOG);
    layers.push_back(UI_LAYER_EDITORUI);
    layers.push_back(UI_LAYER_EDITORUI_DIALOG);
    layers.push_back(UI_LAYER_PROGRAM_DIALOG);
    layers.push_back(UI_LAYER_PROGRAM_WINDOW);
    layers.push_back(UI_LAYER_PROGRAM_WINDOW_DIALOG);
    return GetTopWindow(layers);
}

Window* Game::GetTopWindow( void ) const {
    std::vector< UI_LayerT_BaseType > layers;
    layers.reserve(3);
    layers.push_back(UI_LAYER_GAMEUI);
    layers.push_back(UI_LAYER_EDITORUI);
    layers.push_back(UI_LAYER_PROGRAM_WINDOW);

    return GetTopWindow(layers);
}

Window* Game::GetTopDialog( void ) const {
    std::vector< UI_LayerT_BaseType > layers;
    layers.reserve(3);
    layers.push_back(UI_LAYER_GAMEUI_DIALOG);
    layers.push_back(UI_LAYER_EDITORUI_DIALOG);
    layers.push_back(UI_LAYER_PROGRAM_DIALOG);
    layers.push_back(UI_LAYER_PROGRAM_WINDOW_DIALOG);

    // (dialogs are also windows)
    return GetTopWindow(layers);
}

Window* Game::GetTopWindow( const std::vector< UI_LayerT_BaseType >& ordered_layers ) const {
    // find the front-most window and set the focusWidget to the first tabstop
    for ( uint l=ordered_layers.size(); l>0; ) {
        --l;
        const uint layer = ordered_layers[l];

        for ( uint i=sprites[layer].list.size(); i>0; ) {
            --i;

            Sprite* sprite = sprites[layer].list[i];

            if ( !sprite || !sprite->IsVisible() )
                continue;

            if ( auto window = dynamic_cast< Window* >( sprite ) ) {
                return window;
            }
        }
    }

    return nullptr;
}

std::shared_ptr< GameUI_Page > Game::GetUIPage( const std::string& pageName ) {
    for ( auto page : game_ui_pages ) {
        if ( page && page->GetName() == pageName ) {
            return page;
        }
    }

    ERR("Couldn't find UI Page: %s\n", pageName.c_str() );
    return nullptr;
}

bool Game::OpenUIPage( const std::shared_ptr< GameUI_Page >& page ) {
    if ( page ) {
        page->Show();
        return true;
    }

    ERR("Game::OpenUIPage() could not find page.\n" );
    return false;
}

bool Game::OpenUIPage( const std::string& pageName ) {
    auto page = GetUIPage( pageName );

    if ( ! page ) {
        return false;
    }

    if ( ! OpenUIPage( page ) ) {
        ERR("Problem showing UI page: %s\n", pageName.c_str() );
        return false;
    }

    return true;
}

void Game::InitUIPages( void ) {
    game_ui_pages.emplace_back( std::make_shared< UI_Items >() );
    game_ui_pages.emplace_back( std::make_shared< UI_Binds >() );
    game_ui_pages.emplace_back( std::make_shared< UI_MainMenu >() );
    game_ui_pages.emplace_back( std::make_shared< UI_LevelSummary >() );
}

bool Game::CloseTopWindow( void ) {
    Window* win = GetTopWindowOrDialog();
    if ( win ) {
        if ( Dialog* dlg = dynamic_cast< Dialog* >( win ) ) {
            dlg->Decline();
            return true;
        } else {
            if ( GameUI_Page* page = win->GetPage() ) {
                page->Hide();
                return true;
            }
        }
    }

    return false;
}

void Game::PressEscape( void ) {
    if ( CloseTopWindow() ) // escape/menuOpen is simply "cancel" on dialogs or close any top windows
        return;

    OpenUIPage("mainMenu"); // no window or dialog was open, so let's show the main menu
}

void Game::DrawHUDUI( void ) {
    hud_stats->Draw();
    if ( !IsPaused() ) {
        ui_timer->Draw();
        ui_infestLevel->Draw();
    }
}

void Game::ResetLevelTimer( void ) {
    ui_timer->Reset();
    globalVals.SetBool( gval_s_levelTimerExpired, false );
}

void Game::AddLevelTime( const uint seconds ) {
    ui_timer->AddTime( seconds );
}

void Game::SubtractLevelTime( const uint seconds ) {
    ui_timer->SubtractTime( seconds );
}

void Game::DecreaseInfestLevel( const int amt ) {
    ui_infestLevel->DecreaseInfestLevel( amt );
    
    if ( ui_infestLevel->GetInfestLevel() <= 0 )
        game->QueueEvent_DisplayLevelSummary();
}

void Game::IncreaseInfestLevel( const int amt ) {
    ui_infestLevel->IncreaseInfestLevel( amt );
}

int Game::GetInfestLevel( void ) const {
    return ui_infestLevel->GetInfestLevel();
}

void Game::AddWorldUI( std::shared_ptr<Sprite>& sprite ) {
    world_ui.Append( sprite );
}

void Game::DrawWorldUI( void ) {
    //Vec3f orientation = -renderer->camera.GetDir();
    
    auto link = world_ui.GetFirst();
    while ( link ) {
        auto sprite = link->Data().lock();
        if ( !sprite ) {
            link = link->Del(); // Sprite has left the world, remove our reference to it
            continue;
        }
        renderer->DrawSprite( *sprite );
        link = link->GetNext();
    }
}

bool Game::RespawnPlayer( Entity& playerEnt ) {
    if ( auto zone = safezone.lock() ) {
        if ( zone->bounds.IsAABox3D() ) {
            CMSG_LOG("Respawning Player.\n");
            const Box3D box = zone->bounds.GetOrientedBox3D();
            playerEnt.SetOrigin( box.GetCenter() );
            playerEnt.RestoreHealth();
            playerEnt.Migrate();
            playerEnt.SetDestructible( true );
            playerEnt.SetKilled( false );
            UnmarkEntityForDeletion( playerEnt.GetWeakPtr().lock() );
            return true;
        }
    }

    return false;
}

Game::~Game( void ) {
}

Vec3f GetNormalizedEntTreeDimension( const Vec3f& size );
Vec3f GetNormalizedEntTreeDimension( const Vec3f& size ) {
    const float sane_default_minimum_size = 100;
    int x = static_cast<int>( std::fmaxf( fabsf( size.x ), sane_default_minimum_size ) );
    int y = static_cast<int>( std::fmaxf( fabsf( size.y ), sane_default_minimum_size ) );
    int z = static_cast<int>( std::fmaxf( fabsf( size.z ), sane_default_minimum_size ) );
    
    // tree size must be divisible by 2
    if ( x % 2 ) x += 1;
    if ( y % 2 ) y += 1;
    if ( z % 2 ) z += 1;
    
    return {
        static_cast<float>(x),
        static_cast<float>(y),
        static_cast<float>(z)
    };
}

void Game::InitEntTree( void ) {
    const int sane_default_subdivisions = 2;
    int numSubdivisions = globalVals.GetInt( gval_g_octreeSubs );
    if ( numSubdivisions <= 0 )
        numSubdivisions = sane_default_subdivisions;
    globalVals.SetInt( gval_g_octreeSubs, numSubdivisions );
    
    const Vec3f size = GetNormalizedEntTreeDimension( globalVals.GetVec3f( gval_g_octreeSize ) );
    
    CMSG_LOG("Octree: Initializing. Subs=%i, size=%i,%i,%i.\n", numSubdivisions, static_cast<int>(size.x), static_cast<int>(size.y), static_cast<int>(size.z) );

    globalVals.SetVec3f( gval_g_octreeSize, size );
    
    LinkList< std::shared_ptr< Entity > > entsToReinsert;
    if ( entTree && entTree->NumSubEntities() > 0 ) {
        CMSG_LOG("Octree: Saving entities for reinsertion.\n");
        
        for ( auto const & ent : all_entities ) {
            if ( auto sptr = ent ) {
                if ( sptr->GetNumVoxelsOccupied() > 0 ) {
                    sptr->LeaveAllVoxels();
                    entsToReinsert.Append( sptr );
                }
            }
        }
    }
    
    entTree.reset();
    entTree = std::make_unique< EntityTree >();
    entTree->Build( nullptr, static_cast<uint>( numSubdivisions ), 0, -size.x/2, size.x/2, -size.y/2, size.y/2, -size.z/2, size.z/2 );
    
    if ( entsToReinsert.Num() > 0 ) {
        CMSG_LOG("Octree: Reinserting entities.\n");
        for ( auto& ent : entsToReinsert ) {
            if ( ent ) {
                entTree->TryInsert( *ent );

                if ( ent->GetNumVoxelsOccupied() < 1 ) {
                    WARN("Octree: Entity is outside the new dimensions. Deleting it.\n");
                    MarkEntityForDeletion( ent );
                }
            }
        }
    }

    if ( globalVals.GetBool( gval_d_octree ) )
        CommandSys::Execute("set d_octree 1"); // update drawn voxels

    if ( globalVals.GetBool( gval_d_voxelTrace ) )
        CommandSys::Execute("set d_voxelTrace 1"); // update drawn trace

#ifdef MONTICELLO_EDITOR
    if ( editor ) {
        editor->Update_Grid();
    }
#endif // MONTICELLO_EDITOR

    CMSG_LOG("Octree: Done initializing.\n");
}

void Game::Deinit( void ) {
    SaveConfig();
    ClearMapNow();
    Input::DeinitJoysticks();
    Preload::ClearForShutdown();
    Particle::Deinit();
    soundManager.UnloadAll();
    delete hud_stats;
    entTree.reset();
    delete[] sprites;
}

void Game::Init( void ) {
    Input::InitButtonIDsAndNames();

    LoadSafeConfigDefaults();
    LoadConfig();

    InitEntTree();

    sdl.Init();

    renderer = new Renderer;
    shaders = &renderer->shaders;
    
    Packages::ManifestAll();

    shaders->LoadAllShaders();
    shaders->LoadAllProgs();
    renderer->Init();

    Font::Init();

    #ifdef MONTICELLO_DEBUG
        // after renderer and fonts are set up, we can now set a fatalError_hook to show a dialog when the game crashes
        void Dialog_FatalError( const char* msg );
        fatalError_hook = &Dialog_FatalError;
    #endif // MONTICELLO_DEBUG

    console = new Console;
    console->Init();
    
    //DEBUG : Initialize Skybox
    renderer->skybox.SetMap("Starting_Skybox");
    
    Input::InitJoysticks();

    InitUIPages();
    
    #ifdef CLIB_UNIT_TEST
        Types_UnitTest();
    #endif // CLIB_UNIT_TEST

    ProcessStartupArgs( developerArgs.data(), static_cast<int>( developerArgs.size() ) );
    ProcessStartupArgs( Arg::args, Arg::arg_num );

    ProcessPostStartupArgs( developerArgs.data(), static_cast<int>( developerArgs.size() ) );
    ProcessPostStartupArgs( Arg::args, Arg::arg_num );

    if ( globalVals.GetBool("a_menu") ) {
        OpenUIPage( "mainMenu" );
    }

    UpdateCursor();

    // in case startup args told us to load a map
    if ( state == GameStateT::MAP_LOAD ) {
        LoadNextMap();
    }

    state = GameStateT::READY;
    isInitialized = true;

    CommandSys::GameIsReady();
}

bool Game::SetEntTreeSubdivisions( const uint subs ) {
    if ( subs > 10 ) {
        ERR("Octree: Not setting subdivisions to %u, that's too many.", subs);
        return false;
    }
    
    const uint curSubs = entTree->NumSubdivisions();
    if ( curSubs == subs ) {
        CMSG_LOG("Octree: Not resizing, already has %u subdivisions\n", curSubs );
        return true;
    }
    
    globalVals.SetUInt(gval_g_octreeSubs, subs );
    InitEntTree();
    return true;
}

bool Game::SetEntTreeDimensions( const uint dimx, const uint dimy, const uint dimz ) {
    const Vec3f newSize = GetNormalizedEntTreeDimension( Vec3f(dimx,dimy,dimz) );

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal"
        const Vec3f curSize = entTree->GetSize();
        if ( curSize.x == newSize.x && curSize.y == newSize.y && curSize.z == newSize.z ) {
            CMSG_LOG("Octree: Not resizing, already of size %i,%i,%i.\n", static_cast<int>(curSize.x), static_cast<int>(curSize.y), static_cast<int>(curSize.z) );
            return true;
        }
    #pragma GCC diagnostic pop
    
    globalVals.SetVec3f(gval_g_octreeSize, newSize );
    
    InitEntTree();
    
    return true;
}

void Game::LoadSafeConfigDefaults( void ) {
    globalVals.SetBool(gval_v_frustCull, true );
    globalVals.SetFloat(gval_v_fov, 75.0f );
    globalVals.SetFloat(gval_v_nearPlane, 0.1f );
    globalVals.SetFloat(gval_v_farPlane, 2000.0f );
    globalVals.SetInt(gval_v_width, 1024 );
    globalVals.SetInt(gval_v_height, 768 );
    globalVals.SetBool(gval_v_fullscreen, false );
    globalVals.SetBool(gval_v_mipmap, true );

    // default game initialization info options
    globalVals.SetBool( gval_i_gl, true);

    // default allowed actions
    globalVals.SetBool( gval_a_editor, true );
    globalVals.SetBool( gval_a_console, true );

    // ** some game defaults
    globalVals.SetBool( gval_d_projections, true);
    globalVals.SetBool( gval_d_models, true);
    globalVals.SetBool( gval_d_ghost, true);
    globalVals.SetBool( gval_d_grid, true);
    globalVals.SetFloat( gval_g_friction, 14.0f );
    globalVals.SetFloat( gval_grav_force, 9.8f );
    globalVals.SetFloat( gval_grav_terminal, 55 );
    globalVals.SetVec3f( gval_grav_dir, Vec3f(0,0,-1) );
    globalVals.SetFloat( gval_axis_buttonThreshold, 0.5f );
    globalVals.SetBool( gval_axis1_invert, true );
    globalVals.SetBool( gval_axis3_invert, true );

    // ** set up camera
    globalVals.SetFloat( gval_cam_zoom, CAMERA_DEFAULT_ZOOM );
    globalVals.SetFloat( gval_cam_tilt, CAMERA_DEFAULT_TILT );
    globalVals.SetFloat( gval_cam_rot, CAMERA_DEFAULT_ROT );
    globalVals.SetVec3f( gval_cam_focalOffset, CAMERA_DEFAULT_FOCALPOINT_OFFSET );

    // ** AI stuff
    globalVals.SetBool( gval_ai_flowfield, true );
}

void SaveConfigHelper( const std::string& bindType, const std::vector< BindInfo >& binds, std::vector< std::string >& data );
void SaveConfigHelper( const std::string& bindType, const std::vector< BindInfo >& binds, std::vector< std::string >& data ) {
    for ( uint i=0; i<binds.size(); ++i ) {
        if ( i >= SDL_NUM_SCANCODES )
            break;

        for ( uint b=0; b  <binds[i].all.size(); ++b ) {
            if ( binds[i].all[b].action.size() < 1 )
                continue;

            std::string line( "bind " );
            line += bindType;
            line += " ";

            for ( auto const & btn : binds[i].all[b].combo ) {
                line += btn;
                line += " ";
            }

            line += binds[i].all[b].action;

            data.emplace_back( line );
        }
    }
}

void Game::SaveConfig( void ) {

    std::vector< std::string > old_data;
    old_data.reserve( 128 );

    // ** Load in existing config, ignoring "bind" and "set" lines
    std::string cfgPath( DataDir );
    cfgPath += "config.cfg";

    CMSG_LOG("Saving cfg: %s\n", cfgPath.c_str() );

    File file( cfgPath.c_str() );

    if ( file.IsOpen() ) {
        ParserText parser( file );
        //parser.SetCommentIndicator('\0');

        std::string line;
        while ( ! file.EndOfFile() ) {
            parser.ReadUnquotedString( line );

            // don't keep comments
            String::Trim( line );
            if ( line[0] == '#' ) {
                parser.SkipToNextNonblankLine();
                continue;
            }

            // don't keep binds
            std::string bind = String::Left( line, 5 );
            String::Trim( bind );
            if ( bind == "bind" ) {
                parser.SkipToNextNonblankLine();
                continue;
            }

            // dont keep sets
            std::string set = String::Left( line, 4 );
            String::Trim( set );
            if ( set == "set" ) {
                parser.SkipToNextNonblankLine();
                continue;
            }

            old_data.emplace_back( line );
            parser.SkipToNextNonblankLine();
        }
    }
    file.Close();

    // ** Create config data for binds

    file.Open( cfgPath.c_str(), "wb" );
    if ( !file.IsOpen() ) {
        ERR("Couldn't open config file %s for write.\n", cfgPath.c_str() );
        return;
    }

    ParserText parser( file );

    std::vector< std::string > new_data;
    new_data.reserve( 128 );
    
    SaveConfigHelper( "ui", ui_key_binds, new_data );
    SaveConfigHelper( "ui", ui_joy_binds, new_data );
    SaveConfigHelper( "ui", ui_axis_binds, new_data );
    SaveConfigHelper( "ui", ui_mouse_binds, new_data );

    SaveConfigHelper( "game", game_key_binds, new_data );
    SaveConfigHelper( "game", game_joy_binds, new_data );
    SaveConfigHelper( "game", game_axis_binds, new_data );
    SaveConfigHelper( "game", game_mouse_binds, new_data );

    SaveConfigHelper( "ed", editor_key_binds, new_data );
    SaveConfigHelper( "ed", editor_joy_binds, new_data );
    SaveConfigHelper( "ed", editor_axis_binds, new_data );
    SaveConfigHelper( "ed", editor_mouse_binds, new_data );

    SaveConfigHelper( "ed_add", editor_addmode_key_binds, new_data );
    SaveConfigHelper( "ed_add", editor_addmode_joy_binds, new_data );
    SaveConfigHelper( "ed_add", editor_addmode_axis_binds, new_data );
    SaveConfigHelper( "ed_add", editor_addmode_mouse_binds, new_data );

    SaveConfigHelper( "ed_nav", editor_navmeshmode_key_binds, new_data );
    SaveConfigHelper( "ed_nav", editor_navmeshmode_joy_binds, new_data );
    SaveConfigHelper( "ed_nav", editor_navmeshmode_axis_binds, new_data );
    SaveConfigHelper( "ed_nav", editor_navmeshmode_mouse_binds, new_data );

    SaveConfigHelper( "ed_select", editor_selectmode_key_binds, new_data );
    SaveConfigHelper( "ed_select", editor_selectmode_joy_binds, new_data );
    SaveConfigHelper( "ed_select", editor_selectmode_axis_binds, new_data );
    SaveConfigHelper( "ed_select", editor_selectmode_mouse_binds, new_data );

    // ** Create config data for sets
    
    uint i=0;
    auto lnk = globalVals.GetFirst( i );
    while ( lnk ) {
        std::string line("set ");
        line += lnk->Data().first.c_str();
        line += " ";
        line += lnk->Data().second.c_str();

        new_data.emplace_back( line );

        lnk = globalVals.GetNext( lnk, i );
    }

    // sort it and save it

    std::sort( new_data.begin(), new_data.end() );
    
    const std::string warning1( "# This file is generated by the game and will be over-written.\n" );
    const std::string warning2( "# All 'bind' and 'set' commands will be read, modified, sorted, and written again.\n" );
    const std::string warning3( "# Any other console commands added to this file will be written at the very end.\n" );

    parser.WriteStr( warning1 );
    parser.WriteStr( warning2 );
    parser.WriteStr( warning3 );

    for ( auto const & str : new_data ) {
        parser.WriteStr( str );
        parser.WriteChar( '\n' );
    }

    for ( auto const & str : old_data ) {
        parser.WriteStr( str );
        parser.WriteChar( '\n' );
    }

    CMSG_LOG("Saved cfg: %s\n", cfgPath.c_str() );
}

void Game::LoadConfig( void ) {

    std::string config_cfg( DataDir );
    config_cfg += "config.cfg";
    File file( config_cfg.c_str() );

    if ( ! file.IsOpen() ) {
        WARN("No config file found, using default.cfg\n");

        config_cfg = DataDir;
        config_cfg += "defaults.cfg";

        file.Open( config_cfg.c_str() );

        if ( ! file.IsOpen() ) {
            WARN("No default config file found, you should manully set binds or reinstall the game.\n");
            return;
        }
    }

    CMSG_LOG("Loading cfg: %s\n", config_cfg.c_str() );

    ParserText parser( file );
    parser.SetCommentIndicator('#');

    std::string line;
    while ( ! file.EndOfFile() ) {
        parser.ReadUnquotedString( line );
        CommandSys::Execute( line.c_str() );
        parser.SkipToNextNonblankLine();
    }

    CMSG_LOG("Loaded cfg: %s\n", config_cfg.c_str() );
}

void Game::UpdateCursor( void ) const {
    bool show_cursor = false;

    const bool in_dev_mode = editor && editor_open;
    if ( in_dev_mode )
        show_cursor = true;

    if ( !sdl.GetFullScreen() )
        show_cursor = true;

    if ( console && console->IsVisible() )
        show_cursor = true;

    SDL_ShowCursor(show_cursor);
}

#ifdef MONTICELLO_EDITOR
void Game::ToggleEditor( void ) {
    if ( ! globalVals.GetBool( gval_a_editor ) )
        return;

    const bool in_dev_mode = ! editor_open;

    // console globalVals to turn on when editor opens, off when it closes
    std::vector< std::string > toggleVars {
        "d_bounds"
#ifndef JESSE_EDITOR_SETTINGS
        , "d_triggerLines"
        , "d_bindLines"
#endif
    };
    
    if ( in_dev_mode ) {
        if ( !editor ) {
            new Editor; // destructs elsewhere
        }

        for ( auto & var : toggleVars ) {
            globalVals.SetBool( var.c_str(), true );
        }

    } else {
        renderer->camera.Reset();
        for ( auto & var : toggleVars ) {
            globalVals.SetBool( var.c_str(), false );
        }
    }

    if ( editor ) {
        editor->ShowUI( in_dev_mode );
        if ( in_dev_mode ) {
            Pause( PauseStateT::PausedByEditor );
        } else {
            Pause( PauseStateT::PausedByPlayer ); // keep the game paused after we close the editor
            Unpause( PauseStateT::PausedByEditor );
        }
    }

    editor_open = in_dev_mode;
    UpdateCursor();
}
#endif // MONTICELLO_EDITOR

void Game::RegisterGen( const std::string& path, const char* container ) {
    const std::string name = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );

    if ( gen_manifest.Set( name.c_str(), AssetManifest( name, path, container ) ) )
        OverrideWarning("MapGen Manifest", name, path, container );
}

void Game::RegisterMap( const std::string& path, const char* container ) {
    const std::string name = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );

    if ( map_manifest.Set( name.c_str(), AssetManifest( name, path, container ) ) )
        OverrideWarning("Map Manifest", name, path, container );
}

void Game::RegisterMoverScript( const std::string& path, const char* container ) {
    const std::string name = String::TrimExtentionFromFileName( String::GetFileFromPath( path ) );

    if ( moverScript_manifest.Set( name.c_str(), AssetManifest( name, path, container ) ) )
        OverrideWarning("MoverScript Manifest", name, path, container );
}

void Game::ClearMap( void ) {
    state = GameStateT::MAP_CLEAR;
}

void Game::ResaveAllMaps( void ) {
    Pause( PauseStateT::PausedByLoad );
    for ( auto const & map : map_manifest ) {
        LoadMap( map.first );
        if ( LoadNextMap() )
            SaveLoadedMap();
    }
    Unpause( PauseStateT::PausedByLoad );
}

void Game::ClearMapNow( void ) {
#ifdef MONTICELLO_EDITOR
    if ( editor ) {
        editor->Map_Clearing();
    }
#endif // MONTICELLO_EDITOR

    scheduler.ResetGameTime();
    game->ResetLevelTimer();

    loaded_map = "";
#ifdef MONTICELLO_EDITOR
    if ( editor )
        editor->Map_Loaded( loaded_map.c_str() );
#endif // MONTICELLO_EDITOR

    // Load Map Script
    mapscript.Erase();

    effectsManager.drawn.DelAll();
    UnsetPlayer();
    renderer->camera.UnsetFollowEntity();

    DeleteMarkedEntities();

    all_entities.clear();
    awake_entities.clear();
    entityEventStack.clear();

    entTree->Clear();
    NavMesh::meshes.DelAll();
    loaded_map.erase();
    safezone.reset();
    
    Input::Clear(); // in case a button is still being pressed that SDL forgot (happens sometimes when the game takes a while to process things)
}

void Game::ReloadMap( void ) {
    state = GameStateT::MAP_LOAD;
    next_map = loaded_map;
}

void Game::LoadMap( const std::string& name ) {
    state = GameStateT::MAP_LOAD;
    next_map = name;
}

void Game::LoadMap( const char *name ) {
    LoadMap( std::string( name ) );
}

bool Game::LoadNextMap( void ) {
    Pause( PauseStateT::PausedByLoad );
    
    //menu.HideDiedMessage();
    //menu.HideDoneMessage();
    //menu.ShowLoadingBar("Loading Map...");
    
    CMSG_LOG("Loading Map: %s\n", next_map.c_str() );

    ClearMapNow();

    // ** first try to open a .map file
    FileMap mapFile;
    const std::string mapName = String::TrimExtentionFromFileName( String::GetFileFromPath( next_map ) );
    if ( map_manifest.Get( mapName.c_str() ) ) {   
        OpenAssetFile( "Map", mapName.c_str(), map_manifest, mapFile );
        if ( mapFile.IsOpen() ) {
            return LoadMap( mapFile );
        }
    }
    
    // ** the .map file doesn't exist, so see if a .gen file exists
    File genFile;
    const std::string genName = String::TrimExtentionFromFileName( String::GetFileFromPath( next_map ) );    
    OpenAssetFile( "Generated Map", genName.c_str(), gen_manifest, genFile );
    if ( MapGen::Load( genFile ) ) {
        MapLoadFinished();
        return true;
    }
        
    ERR("Could not find map: %s.\n", mapName.c_str());
    return false;
}

bool Game::LoadMap( FileMap& openedMapFile ) {
    if ( openedMapFile.Load() ) {
        loaded_map = next_map.c_str();
        CMSG_LOG("Map %s loaded from %s\n", openedMapFile.GetFilePath().c_str(), openedMapFile.GetFileContainerName_Full().c_str() );
    } else {
        ERR("Could not load map %s\n", openedMapFile.GetFilePath().c_str() );
        loaded_map = "";
        return false;
    }
    
    SetEntTreeDimensions( openedMapFile.GetMapWidth(), openedMapFile.GetMapLength(), openedMapFile.GetMapHeight() );
    
    for ( auto& ent : openedMapFile.entities ) {
        InsertEntity( ent );
        ent->Spawn();
    }
    
    ui_infestLevel->Update();

    openedMapFile.SetPlayer();
    openedMapFile.SetFollowEnt();

    // Load Map Script
// todo maybe
//    mapscript.Erase();
//    if ( auto mps = mapScriptManager.Get( openedMapFile.GetFileName().c_str() ) ) {
//        mps->Clone( mapscript );
//    }
    
    MapLoadFinished();
    return true;
}

bool Game::MapLoadFinished( void ) {
    //menu.HideLoadingBar();
    next_map.erase();
    
#ifdef MONTICELLO_EDITOR
    if ( editor )
        editor->Map_Loaded( loaded_map.c_str() );
#endif // MONTICELLO_EDITOR

    CMSG_LOG("Map Load Finished: %s\n", next_map.c_str() );
    Unpause( PauseStateT::PausedByLoad );
    return true;
}

void Game::UnloadUnusedAssets( void ) {
    const char* unloading = "Unloading Unused Assets...\n";
    CMSG_LOG(unloading);
    //menu.ShowLoadingBar(unloading);
    entityInfoManager.UnloadUnusedAssets(); // free entities first, they use models, materials, effects, and damage
    damageManager.UnloadUnusedAssets();
    mapScriptManager.UnloadUnusedAssets();
    modelManager.UnloadUnusedAssets(); // free models before materials, they use materials
    effectsManager.UnloadUnusedAssets(); // free effects before materials, they use materials
    materialManager.UnloadUnusedAssets(); // free materials before textures, they use textures
    fontManager.UnloadUnusedAssets();
    textureManager.UnloadUnusedAssets();
    soundManager.UnloadUnusedAssets();
    //menu.HideLoadingBar();
}

bool Game::SaveMapAs( const std::string& name ) {
    return SaveMapAs( name.c_str() );
}

#ifdef MONTICELLO_EDITOR
bool Game::CheckIfOkayToSaveMap( void ) const {
    if ( scheduler.GetGameFrame() > 1 ) {
        const std::string error_message("Saving map not allowed!\nThe game has already started, saving would corrupt it.\n\nMap not saved.");
        auto dlg = Dialog::Create(error_message.c_str());
        auto cancel = [](){};
        dlg->AddButton( new DialogButton<decltype(cancel) >( "Ok", *dlg, cancel, ButtonTypeT::Accept ) );
        ERR( error_message.c_str() );
        return false;
    }

    return true;
}

bool Game::SaveMapAs( const char *name ) {
    if ( ! CheckIfOkayToSaveMap() )
        return false;

    //menu.ShowLoadingBar("Saving...");

    if ( editor )
        editor->Map_Clearing(); // for now, just unselect everythign in the editor because it'll save things like entities highlighted. figure out a better way later...

    FileMap saveFile;
    
    std::string path;
    
    // ** if map already exists, overwrite it
    
    // determine the directory to save to.
    AssetManifest* map = map_manifest.Get( name );
    if ( map && map->GetContainer() == "" ) { 
        // an empty container string indicates the file is already on the filsystem, so use the current path.
        path = map->GetPath();
    } else {
        // the file exists inside a zip file, so we will just create a new map in the default map save location, since filesstem files override those in zips
        path = DataDir;
        path.append("1/map/");
        if ( ! FileSystem::CreateDir( path.c_str() ) )
            return false;

        // ** ensure it has a ".map" extention
        path.append( name );
        const std::string ext = String::ToUpper( String::Right( path, 4 ) );
        if ( ext != ".MAP" )
            path.append(".map");
    }
    
    const std::string tempFile = path + "_";
    
    const bool ret = saveFile.Save( tempFile.c_str() );
    
    // if we didn't crash, the file saved successfully. replace the save file with the temp file
    if ( !FileSystem::Rename( tempFile.c_str(), path.c_str() ) ) {
        
        std::string msg("Could not rename mapfile ");
        msg += tempFile.c_str();
        
        ERR(msg.c_str());

        auto dlg = Dialog::Create( msg.c_str() );
        dlg->AddButton( new DialogButton< void(*)() >( "OK", *dlg, &DialogFunc_Cancel, ButtonTypeT::Decline ) );
        return false;
    }
    
    CMSG_LOG("Renamed: %s to %s\n", tempFile.c_str(), path.c_str() );

    if ( ret ) {
        map_manifest.Set( name, AssetManifest( name, path ) ); // update the manifest (filesystem always overrides zips)
        loaded_map = name; // the saved map is now our loaded map
    }

    if ( editor ) {
        editor->RepopulateMapsDropBox();
        editor->SetSelectedMap( name );
    }

    //menu.HideLoadingBar();
    return ret;
}
#endif // MONTICELLO_EDITOR

extern std::vector< std::string > ALL_TYPES;
void Game::MainLoop( void ) {
    while( state != GameStateT::SHUTDOWN ) {

        if ( runOneFrame ) {
            static PauseStateT prev_state=PauseStateT::Unpaused;
            if ( IsPaused() ) {
                prev_state = pause_state;
                pause_state = PauseStateT::Unpaused;
            } else {
                pause_state = prev_state;
                runOneFrame = false;
            }
        }
        
        renderer->DebugThink();

        UpdateTime();
        Input::ProcessInput();

        ProcessEntities();

        if ( !IsPaused() ) {
            DoGameEvents();
            DoMapScript();
            renderer->camera.Think();
            scheduler.IncrementGameFrame();
            ui_timer->Update();
        }

        renderer->DrawGLScene();

        #ifdef MONTICELLO_EDITOR
                ProcessEditor();
        #endif // MONTICELLO_EDITOR

        CheckGamestate();
    }
}

void Game::UpdateTime( void ) {
    scheduler.UpdateFrame();

    // Update FPS counter every so often
    if ( GetRenderTime() - prevFPSUpdate > ONE_QUARTER_SECOND_U ) {
        if ( globalVals.GetBool( gval_d_fps ) )
            scheduler.CalcFPS();
    }
}

void Game::ProcessEntities( void ) {
    DeleteMarkedEntities();
    CallOnAwakeEntities( &Entity::RunFrame );

    while ( entityEventStack.size() > 0 ) {
        const uint last_index = entityEventStack.size()-1;
        auto const & event = entityEventStack[last_index];
        auto ent = event.ent.lock();
        if ( ent && event.method )
            CALL_METHOD(*ent,event.method)();
        entityEventStack.pop_back();
    }
}


void Game::QueueEvent_EntityMethod( Entity& ent, EntityVoidMethodPtr method ) {
    auto wptr = ent.GetWeakPtr();
    entityEventStack.emplace_back( wptr, method );
}

#ifdef MONTICELLO_EDITOR
void Game::ProcessEditor( void ) {
    if ( editor ) {
        if ( editor_open ) {
            editor->Think();
        }
    }
}
#endif // MONTICELLO_EDITOR

void Game::CheckGamestate( void ) {
    switch ( state ) {
        case GameStateT::MAP_CLEAR:
            ClearMapNow();
            UnloadUnusedAssets();
            state = GameStateT::READY;
            // no need to Preload::Clear(), MAP_CLEAR is only used by the editor
            break;
        case GameStateT::MAP_LOAD:
            LoadNextMap();
            UnloadUnusedAssets();
            Preload::Clear(); //!< clear the preloaded stuff, we only held on to it to keep it loaded during UnloadUnusedAssets()
            state = GameStateT::READY;
            break;
        case GameStateT::INIT:
            break;
        case GameStateT::SHUTDOWN:
            break;
        case GameStateT::ABORT:
            assert(false);
            abort();
        case GameStateT::READY:
            break;
        default: break;
    }
}

void Game::RemoveEntity( const uint index ) {
    ASSERT( index < all_entities.size() );
    renderer->camera.Unfollow( *all_entities[index] );
    PopSwap( all_entities, index );
    Entity::GameAttorney::SetGameIndex( *all_entities[index], index );
}

void Game::DeleteMarkedEntities( void ) {

    for ( uint i=ents_to_delete.size(); i > 0; i=ents_to_delete.size() ) {
        --i;
        
        const auto ent = ents_to_delete[i].lock();
        if ( !ent ) {
            ents_to_delete.pop_back();
            continue;
        }

        if constexpr ( respawnPlayerWhenDead ) {
            // swap player with beginning of list and we'll deal with it later
            // todo: if we have multiple players, we will need to tweak this
            if ( i != 0 && IsPlayer( ent ) ) {
                std::swap(ents_to_delete[0], ents_to_delete[i]);
                continue;
            }
        }
        
        if ( ent->IsDestructible() ) {
            MapScript::EntityKilled( *ent, mapscript.actions );
        }
        
        RemoveEntity( ent->GetIndex() );
        ents_to_delete.pop_back();
    }

    if constexpr ( respawnPlayerWhenDead ) {
        if ( ents_to_delete.size() > 0 && GetState() == GameStateT::READY ) {
            auto player_ent = all_entities[0];
            ASSERT( player_ent );
            
            if ( !RespawnPlayer( *player_ent ) ) {
                RemoveEntity( 0 );
            }
        }
    }
    
    ASSERT( ents_to_delete.size() == 0 );
}

void Game::UnmarkEntityForDeletion( const std::shared_ptr< Entity >& ent ) {
    auto new_end = std::remove_if( begin( ents_to_delete ), end( ents_to_delete ), std::bind( CompareWeakPtr<Entity>, ent, std::placeholders::_1 ) );
    if ( new_end != end( ents_to_delete ) ) {
        ents_to_delete.erase( new_end, end( ents_to_delete ) );
    } else {
        if ( std::find( begin( all_entities ), end( all_entities ), ent ) != end( all_entities ) ) {
            WARN("Entity requested to be unmarked for deletion was not found in the deletion list, but was not deleted.\n");
        } else {
            ERR("Entity requested to be unmarked for deletion was not found in the deletion list.\n");
        }
    }
}

bool Game::IsPausedBy( const PauseStateT _state ) const {
    return BitwiseAnd( pause_state, _state );
}

bool Game::IsPaused( void ) const {
    return pause_state != PauseStateT::Unpaused;
}

bool Game::LoadMoverScript( const char* name, LinkList< MoveOp >& moveOps ) const {
    MoverScript file;
    return OpenAssetFile( "MoverScript", name, moverScript_manifest, file ) && file.Load( moveOps );
}

uint Game::GetAllMapNames( LinkList< std::string >& names ) const {
    for ( const auto& data : map_manifest )
        names.Append( data.second.GetName() );

    return names.Num();
}

void Game::SetPlayer( const std::weak_ptr< Actor >& to ) {
    auto new_player = to.lock();
    auto old_player = player.lock();
    
    if ( new_player == old_player )
        return;
    
    player = to;
    
    if ( !new_player ) {
        UnsetPlayer();
        return;
    }
    
    new_player->SetTeamName( "player" );
    renderer->camera.SetFollowEntity( player );
}

void Game::MarkEntityForDeletion( const std::weak_ptr< Entity >& ent ) {
    auto const locked_ent = ent.lock();
    if ( !locked_ent )
        return;
    
    if ( ContainsWeakPtr( ents_to_delete, locked_ent ) )
        return;
    
    ents_to_delete.emplace_back( ent );
}

void Game::SetSafeZone( const std::weak_ptr< Entity >& to ) {
    safezone = to;
}

void Game::CallOnAllEntities( EntityVoidMethodPtr method ) {
    for ( uint e=0; e<all_entities.size(); ++e )
        if ( all_entities[e] && all_entities[e]->GetNumVoxelsOccupied() > 0 )
            CALL_METHOD(*all_entities[e],method)();
}

void Game::CallOnAwakeEntities( EntityVoidMethodPtr method ) {
    for ( uint e=0; e<awake_entities.size(); ++e ) {
        auto ent = awake_entities[e].lock();
        
        // remove entities that no longer exist or have fallen asleep
        if ( !ent || !ent->IsAwake() ) {
            PopSwap( awake_entities, e-- );

            if ( ent ) {
                Entity::GameAttorney::SetAwakeState( *ent, AwakeStateT::Asleep );
            }
            continue;
        }
        
        if ( ent && ent->GetNumVoxelsOccupied() > 0 ) {
            CALL_METHOD(*ent,method)();
        }
    }
}

bool Game::IsPlayer( const Entity& entity ) const {
    if ( auto sptr = player.lock() )
        return sptr->GetIndex() == entity.GetIndex();
    return false;
}

bool Game::IsPlayer( const std::shared_ptr< Entity >& entity ) const {
    return IsPlayer( entity.get() );
}

bool Game::IsPlayer( const Entity* entity ) const {
    if ( entity )
        return IsPlayer( *entity );
    return false;
}

std::shared_ptr< Entity > Game::GetPlayerEntity( void ) const {
    return std::static_pointer_cast< Entity >( player.lock() );
}

bool Game::ComparesLess_EntNameToEntName( const std::weak_ptr< Entity >& A, const std::shared_ptr< Entity >& B ) {
    return Compare_EntNameToEntName(A,B) < 0;
}

bool Game::ComparesLess_EntNameToStr( const std::weak_ptr< Entity >& A, const std::string& B ) {
    return Compare_EntNameToStr(A,B) < 0;
}

int Game::Compare_EntNameToEntName( const std::weak_ptr< Entity >& A, const std::shared_ptr< Entity >& B ) {
    return Compare_EntNameToStr(A,B->GetName());
}

int Game::Compare_EntNameToStr( const std::weak_ptr< Entity >& A, const std::string& B ) {
    auto const sptr = A.lock();
    ASSERT( sptr );
    return B.compare( sptr ? sptr->GetName() : "" );
}

void Game::AwakenEntity( const std::shared_ptr< Entity >& ent ) {
    if ( ! ent )
        return;
    
    if ( ent->IsAwake() )
        return;

    awake_entities.emplace_back( ent );
    Entity::GameAttorney::SetAwakeState( *ent, AwakeStateT::Awake );
}

void Game::InsertEntity( const std::shared_ptr< Entity >& ent ) {
    ASSERT( ent );
    if ( !ent->GetWeakPtr().expired() ) {
        ERR("Game::InsertEntity: Entity already inserted: %s.\n", ent->GetIdentifier().c_str() );
        return;
    }
    const uint index = all_entities.size();
    all_entities.emplace_back( ent );
    Entity::GameAttorney::SetGameIndex( *ent, index );
    Entity::GameAttorney::SetWeakPtr( *ent, all_entities[index] );
}

void Game::UnsetPlayer( void ) {
    auto p = player.lock();
    
    player.reset();
    
    if ( ! p )
        return;
    
    renderer->camera.UnsetFollowEntity( static_cast< Entity* >( p.get() ) );
    p->SetTeamName( "" );
}

void Game::UnsetPlayer( const Actor* actor ) {
    if ( ! actor )
        return;
    
    if ( auto p = player.lock() )
        if ( p->GetIndex() == actor->GetIndex() )
            UnsetPlayer();
}

void Game::DoMapScript( void ) {
    MapScript::EvalActions( mapscript.actions );
}

void Game::NextFrame( void ) {
    if ( renderer ) {
        renderer->ClearFramelyShapes();
        renderer->ClearEditorFramelyShapes();
    }
    runOneFrame = true;
}

void Game::QueueEvent_DisplayLevelSummary( void ) {
    if ( !globalVals.GetBool( gval_g_noLevelSummary ) ) {
        std::vector< std::weak_ptr< Entity > > entlist;
        entlist.emplace_back( player );
        
        game_events.emplace_back( "levelSummary", entlist );
    }
}

void Game::DoGameEvents( void ) {
    for ( auto & event : game_events ) {
        if ( event.name == "levelSummary" ) {
            DoGameEvent_DisplayLevelSummary( event );
        }
    }
    
    game_events.clear();
}

void Game::DoGameEvent_DisplayLevelSummary( [[maybe_unused]] const GameEvent& event_unused ) {
    auto page = game->GetUIPage( "levelSummary" );
    
    if ( !page ) 
        return;
        
    page->Show();
}

bool Game::IsEditorOpen( void ) const {
    return editor && editor_open;
}

bool Game::IsMenuOpen( void ) const {
    return globalVals.GetBool("s_menu");
}

bool Game::IsConsoleOpen( void ) const {
    return console->IsVisible();
}

constexpr const bool pauseDebug = false;

void Game::TogglePause( const PauseStateT _state ) {
    if constexpr ( pauseDebug ) CMSG_LOG("Toggle Pause.\n" );
    
    // pause button unpauses every bit
    if ( _state == PauseStateT::PausedByPlayer ) {
        if ( IsPaused() ) {
            Unpause( _state );
            return;
        }
    }
    
    if ( ( pause_state & _state ) == PauseStateT::Unpaused ) {
        Pause( _state );
    } else {
        Unpause( _state );
    }
}

void Game::Pause( const PauseStateT _state ) {
    pause_state |= _state;
    
    // because player can unpause at any time (which clears all pause bits) we need to update a few things when the game re-pauses
    if ( IsEditorOpen() ) pause_state |= PauseStateT::PausedByEditor;
    if ( IsMenuOpen() ) pause_state |= PauseStateT::PausedByMenu;
    if ( IsConsoleOpen() ) pause_state |= PauseStateT::PausedByConsole;
    
    CMSG_LOG("Game is%sPaused.\n", IsPaused()?" ":" not ");

    if constexpr ( pauseDebug ) CMSG_LOG("Paused (state %u/%u)\n", static_cast<uint>(_state), static_cast<uint>(pause_state) );
}

void Game::Unpause( const PauseStateT _state ) {
    if ( _state == PauseStateT::PausedByPlayer ) {
        // player can unpause at any time
        pause_state = PauseStateT::Unpaused;
    } else {
        pause_state &= ~(_state|PauseStateT::PausedByPlayer);
    }
    
    CMSG_LOG("Game is%sPaused.\n", IsPaused()?" ":" not ");
    
    if constexpr ( pauseDebug ) CMSG_LOG("UnPaused (state %u/%u)\n", static_cast<uint>(_state), static_cast<uint>(pause_state) );
}

std::shared_ptr< Entity > Game::NewEnt( const TypeID type_id ) {
    auto ent = NewEntityFromID( type_id );
    ASSERT( ent );
    InsertEntity( ent );
    return ent;
}

bool Game::IsInitialized( void ) const {
    return isInitialized;
}

uint Game::NumEnts( void ) const {
    return game->all_entities.size();
}

float Game::GetFPS( void ) const {
    return scheduler.GetFPS();
}

Uint32 Game::GetFrame( void ) const {
    return GetGameFrame();
}

#ifdef MONTICELLO_EDITOR
bool Game::SaveLoadedMap( void ) {
    return SaveMapAs( loaded_map.c_str() );
}
#endif // MONTICELLO_EDITOR

GameStateT Game::GetState( void ) const {
    return state;
}

void Game::SetState( const GameStateT newState ) {
    state = newState;
}

std::shared_ptr< Actor > Game::GetPlayer( void ) const {
    return player.lock();
}

Uint32 Game::GetRenderFrame( void ) const {
    return scheduler.GetRenderFrame();
}

Uint32 Game::GetGameFrame( void ) const {
    return scheduler.GetGameFrame();
}

Uint32 Game::GetGameTime( void ) const {
    return scheduler.GetGameTime();
}

Uint32 Game::GetRenderTime( void ) const {
    return scheduler.GetRenderTime();
}

Uint32 Game::GetRealTime( void ) const {
    return scheduler.GetRealTime();
}

const std::vector< std::shared_ptr< Entity > >& Game::GetEntityListRef( void ) const {
    return all_entities;
}

std::shared_ptr< Entity > Game::NewUnmanagedEnt( const TypeID type_id ) {
    auto ent = NewEntityFromID( type_id );
    return ent;
}
