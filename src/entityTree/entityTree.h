// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_OBJECTTREE_ENTITYTREE_H_
#define SRC_OBJECTTREE_ENTITYTREE_H_

#include "../base/main.h"
#include "./entityTree.h"
#include "./viewFrustum.h"
#include "../math/primitive.h"

template<typename TYPE>
class VBO;
class Entity;
class EntityTree;
class EntityTreeTicket;
class EntityTreeTicketAttorney;
class Primitive;
class IntersectData;
class SweepData;
class CollisionReport3D;
struct CollisionItem;

void SortEntityList( const Vec3f& pos, LinkList< std::weak_ptr< Entity > >& entList );

typedef uint VoxelTypeT_BaseType;
enum class VoxelTypeT : VoxelTypeT_BaseType {
    Occupied
    , All
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Weffc++" // complains about AABox3D not having virutal destructor, which it doesn't need for this class
class EntityTree : private Primitive_AABox3D {
#pragma GCC diagnostic pop

public:
    class EntityTreeTicketAttorney {
        friend class EntityTreeTicket;

    public:
        DISABLE_INSTANTIATION( EntityTreeTicketAttorney )

    private:
        static Link< std::weak_ptr< Entity > >* AddEntity( EntityTree& tree, Entity& entity );
        static void RemoveEntity( EntityTree& tree, Link< std::weak_ptr< Entity > >* occupied_link );
    };

public:
    EntityTree& operator=( const EntityTree& other ) = delete;

public:
    EntityTree( void );
    ~EntityTree( void );
    EntityTree( const EntityTree& other ) = delete;

public:
    void Build( EntityTree* _container, const uint _subdivisions, uint _cur_depth, const float min_x, const float max_x, const float min_y, const float max_y, const float min_z, const float max_z );
    
    uint TryInsert( Entity& ent ); //!< returns how many voxels entity is in
    uint TryMigrate( Entity& ent ); //!< returns how many voxels entity is in

    bool GetEntityTraceCollisions( const IntersectData& coldata, const Primitive& primitive, std::vector< CollisionItem >* entList_out ); //!< returns whether at least one collision occured. only fills entList_out if it is not null
    bool GetEntityTraceCollisions( CollisionReport3D& report, const IntersectData& coldata, const Primitive& primitive ); //!< returns whether at least one collision occured. only fills entList_out if it is not null

    std::shared_ptr< Entity > GetEntitySweepCollisions( const SweepData& coldata, const Primitive& primitive, std::vector< CollisionItem >* entList_out ); //!< returns whether at least one collision occured. only fills entList_out if it is not null
    std::shared_ptr< Entity > GetEntitySweepCollisions( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive ); //!< returns whether at least one collision occured. only fills entList_out if it is not null

    void GetTraceVoxels( const Primitive& primitive, LinkList< EntityTree *>& voxelList_out, const VoxelTypeT );
    void GetSweepVoxels( const Primitive& primitive, const Vec3f& vel, LinkList< EntityTree *>& voxelList_out, const VoxelTypeT );

private:
    void GetMigrationVoxels( const Primitive& primitive, LinkList< EntityTree *>& voxelList_out, std::vector< EntityTree *>& allChecked_out );

public:
    void Clear( void ) const; //!< this just tells every entity in the tree to remove itself

    void DrawOpaqueEnts( void ) const;
    void DrawOpaqueEnts( const EntityTree* voxel ) const;

    static void DrawEntity( Entity& ent );
    static void DrawDeferredEnts( void );
    static void DrawOpaqueEnts_InVoxelList( const LinkList< EntityTree* >& voxelList );
    void DrawDebug( void ) const;

    void ShowAll( const bool whether );
    void ShowTrace( const bool whether );
    
    void MakeEntTransparentForNextFrame( const std::weak_ptr< Entity >& ent );
    
    uint NumSubEntities( void ) const; //!< returns the number of entities whether we are a leaf or a node
    uint NumSubdivisions( void ) const;
    
    static void SortVoxelList( const Vec3f& pos, LinkList< EntityTree *>& voxelList );
     
    auto CheckBounds( const Vec3f& _origin ) const { return shape.CheckBounds( _origin ); }
    auto GetRadius( void ) const { return shape.GetRadius(); }

    auto CheckX( const float x ) const { return shape.CheckX(x); }
    auto CheckY( const float y ) const { return shape.CheckY(y); }
    auto CheckZ( const float z ) const { return shape.CheckZ(z); }

    auto CheckXMin( const float x ) const { return shape.CheckXMin(x); }
    auto CheckYMin( const float y ) const { return shape.CheckYMin(y); }
    auto CheckZMin( const float z ) const { return shape.CheckZMin(z); }

    auto CheckXMax( const float x ) const { return shape.CheckXMax(x); }
    auto CheckYMax( const float y ) const { return shape.CheckYMax(y); }
    auto CheckZMax( const float z ) const { return shape.CheckZMax(z); }

    auto GetCenter( void ) const { return shape.GetCenter(); }
    auto GetSize( void ) const { return shape.GetSize(); }
    
    AABox3D GetShape( void ) const override { return shape; }
    
    const LinkList< std::weak_ptr< Entity > >& GetEntityListRef( void ) const;

private:
    Link< std::weak_ptr< Entity > >* AddEntity( Entity& entity );
    void RemoveEntity( Link< std::weak_ptr< Entity > >* occupied_link );

    void AdjustNumTickets( const int addTo );

    static void Private_GetTraceVoxels( EntityTree& voxel, const Primitive& primitive, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType );
    void Private_GetTraceVoxels( const Primitive& primitive, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType );
    
    static void Private_GetSweepVoxels( EntityTree& voxel, const Primitive& primitive, const Vec3f& vel, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType );
    void Private_GetSweepVoxels( const Primitive& primitive, const Vec3f& vel, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType );
    
    void ConnectNeighbors( void );
    void vboPackOctree( void );

    void vboPackOutline( VBO<float>& vbo, VBO<float>* color = nullptr ) const;
    void vboPackVoxel( VBO<float>& vbo ) const;
    void vboPackOutlineColors( VBO<float>& vbo, const Color4f& color ) const; //!< packs colors for one voxel (that's 8*3 times)

private:
    EntityTree **voxel;
    EntityTree *container;
    LinkList< std::weak_ptr< Entity > > *list;
    Uint32 highlight; //!< will be drawn highlighted if same as highlight_magic_int. prevents us from having to traverse the tree and set this to false
    int num_tickets;
    std::unique_ptr< std::vector< EntityTree* > > neighbors;
    static Uint32 subdivisions;
    static Uint32 highlight_magic_int;
    static LinkList< std::weak_ptr< Entity > > deferred_transparent_ents;
    static LinkList< std::weak_ptr< Entity > > deferred_viewblocking_ents;
    static VBO<float> *vbo_trace;
    static VBO<float> *vbo_trace_color;
    static VBO<float> *vbo_all;
};

#endif  // SRC_OBJECTTREE_ENTITYTREE_H_
