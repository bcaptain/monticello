// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entityTree.h"
#include "../game.h"
#include "../math/collision/collision.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"

Uint32 EntityTree::subdivisions = 0u;
Uint32 EntityTree::highlight_magic_int = MAX_UINT32;
LinkList< std::weak_ptr< Entity > > EntityTree::deferred_transparent_ents;
LinkList< std::weak_ptr< Entity > > EntityTree::deferred_viewblocking_ents;

VBO<float>* EntityTree::vbo_trace = nullptr;
VBO<float>* EntityTree::vbo_trace_color = nullptr;
VBO<float>* EntityTree::vbo_all = nullptr;

const uint MIN_POSSIBLE_TOUCHING_VOXELS = 8;
const uint MAX_POSSIBLE_TOUCHING_VOXELS = 27;

void EntityTree::RemoveEntity( Link< std::weak_ptr< Entity > >* occupied_link ) {
    ASSERT( list );
    ASSERT( occupied_link );
    ASSERT( occupied_link->GetMaster() == list );
    occupied_link->Del();
    AdjustNumTickets( -1 );
}

void EntityTree::AdjustNumTickets( const int addto ) {
    num_tickets += addto;
    ASSERT( num_tickets >= 0 );

    if ( container )
        container->AdjustNumTickets( addto );
}

void EntityTree::Build( EntityTree* _container, const uint _subdivisions, uint _cur_depth, const float min_xx, const float max_xx, const float min_yy, const float max_yy, const float min_zz, const float max_zz ) {
    subdivisions = _subdivisions;
    shape.SetBounds(min_xx, max_xx, min_yy, max_yy, min_zz, max_zz );
    container = _container;

    // only nodes get a list
    if ( ++_cur_depth == _subdivisions ) {
        list = new LinkList< std::weak_ptr< Entity > >;
        neighbors = std::make_unique< std::vector< EntityTree* > >();
        return;
    }

    // ** build the inner cubes from back to front, left to right, top to bottom
    const float back_x = shape.min.x;
    const float front_x = shape.max.x;
    const float middle_x = front_x - (front_x - back_x)/2;

    const float left_y = shape.min.y;
    const float right_y = shape.max.y;
    const float middle_y = right_y - (right_y - left_y)/2;

    const float bottom_z = shape.min.z;
    const float top_z = shape.max.z;
    const float middle_z = top_z - (top_z - bottom_z)/2;

    voxel = new EntityTree*[8];
    for ( uint i=0; i< 8; ++i )
        voxel[i] = new EntityTree;

    voxel[0]->Build( this, _subdivisions, _cur_depth, back_x, middle_x, left_y, middle_y, bottom_z, middle_z ); // back left bottom
    voxel[1]->Build( this, _subdivisions, _cur_depth, back_x, middle_x, left_y, middle_y, middle_z, top_z ); // back left top
    voxel[2]->Build( this, _subdivisions, _cur_depth, back_x, middle_x, middle_y, right_y, bottom_z, middle_z  ); // back right bottom
    voxel[3]->Build( this, _subdivisions, _cur_depth, back_x, middle_x, middle_y, right_y, middle_z, top_z ); // back right top
    voxel[4]->Build( this, _subdivisions, _cur_depth, middle_x, front_x, left_y, middle_y, bottom_z, middle_z  ); // front left bottom
    voxel[5]->Build( this, _subdivisions, _cur_depth, middle_x, front_x, left_y, middle_y, middle_z, top_z ); // front left top
    voxel[6]->Build( this, _subdivisions, _cur_depth, middle_x, front_x, middle_y, right_y, bottom_z, middle_z  ); // front right bottom
    voxel[7]->Build( this, _subdivisions, _cur_depth, middle_x, front_x, middle_y, right_y, middle_z, top_z ); // front right top
    
    if ( container )
        return;
        
    if ( vbo_all ) {
        ShowAll( globalVals.GetBool( gval_d_octree ) );
    }
    
    ConnectNeighbors();
}

EntityTree::EntityTree( void )
    : voxel(nullptr)
    , container(nullptr)
    , list(nullptr)
    , highlight(0)
    , num_tickets(0)
    , neighbors()
    {
}

EntityTree::~EntityTree( void ) {
    if ( voxel ) {
        for ( Uint8 i=0; i<8; ++i )
            delete voxel[i];
    }
    delete[] voxel;
    delete list;
}

void EntityTree::vboPackOctree( void ) {
    ASSERT( vbo_all && !vbo_all->Finalized() );
    vboPackOutline( *vbo_all );

    if ( !voxel ) {
        vboPackVoxel( *vbo_all );
        return;
    }
    
    for ( Uint8 i=0; i<8; ++i )
        voxel[i]->vboPackOctree();
}

void EntityTree::ConnectNeighbors( void ) {
    if ( voxel ) {
        for ( Uint8 i=0; i<8; ++i ) {
            voxel[i]->ConnectNeighbors();
        }
        return;
    }

    const Vec3f increase(0.1f,0.1f,0.1f);
    const Primitive_AABox3D primitive( AABox3D( shape.min-increase, shape.max+increase ) );
    LinkList< EntityTree *> voxelList;
    game->entTree->GetTraceVoxels( primitive, voxelList, VoxelTypeT::All );
    
    ASSERT( voxelList.Num() >= MIN_POSSIBLE_TOUCHING_VOXELS );
    ASSERT( voxelList.Num() <= MAX_POSSIBLE_TOUCHING_VOXELS );
    ASSERT( neighbors );
    
    for ( auto & neighbor : voxelList ) {
        if ( neighbor != this ) {
            neighbors->emplace_back( neighbor );
        }
    }
}

void EntityTree::Private_GetTraceVoxels( EntityTree& voxel, const Primitive& primitive, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType ) {
    switch ( voxelType ) {
        case VoxelTypeT::Occupied:
            if ( voxel.num_tickets < 1 )
                return;

            break;
        case VoxelTypeT::All: FALLTHROUGH;
        default: break;
    }

    // don't recurse voxel if we don't collide with it.
    if ( !primitive.Intersects( voxel ) )
        return;

    if ( vbo_trace ) {
        if ( voxel.num_tickets > 0 ) {
            highlight_magic_int = voxel.highlight = game->GetGameTime();
            voxel.vboPackOutline( *vbo_trace, vbo_trace_color );
        }
    }

    if ( voxel.list ) {
        voxelList.Append( &voxel );
        return;
    }

    // not a node, we have subvoxels.
    for ( Uint8 i=0; i<8; ++i )
        Private_GetTraceVoxels( *voxel.voxel[i], primitive, voxelList, voxelType );
}

void EntityTree::Private_GetSweepVoxels( EntityTree& voxel, const Primitive& primitive, const Vec3f& vel, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType ) {
    switch ( voxelType ) {
        case VoxelTypeT::Occupied:
            if ( voxel.num_tickets < 1 )
                return;

            break;
        case VoxelTypeT::All: FALLTHROUGH;
        default: break;
    }

    // don't recurse voxel if we don't collide with it.
    // Sweeping doesn't include things already intersecting, but sweeping against the entity tree considers whether we're already inside of a voxel as well
    if ( !primitive.Intersects( voxel ) ) {
        CollisionReport3D report;
        if ( !primitive.SweepTo( report, vel, voxel ) )
            return;
    }

    if ( vbo_trace ) {
        if ( voxel.num_tickets > 0 ) {
            highlight_magic_int = voxel.highlight = game->GetGameTime();
            voxel.vboPackOutline( *vbo_trace, vbo_trace_color );
        }
    }

    if ( voxel.list ) {
        voxelList.Append( &voxel );
        return;
    }

    // not a node, we have subvoxels.
    for ( Uint8 i=0; i<8; ++i )
        Private_GetSweepVoxels( *voxel.voxel[i], primitive, vel, voxelList, voxelType );
}

// sort voxel list from distance to pos
void EntityTree::SortVoxelList( const Vec3f& pos, LinkList< EntityTree* >& voxelList ) {
    EntityTree* vox;
    LinkList< float > distList;
    Link< EntityTree* > *itrVox, *lnkVox;
    Link< float > *itrDist, *lnkDist;

    // create a parallel linklist of floats that represent distance from our position
    for ( itrVox = voxelList.GetFirst(); itrVox != nullptr; ) {
        if ( ! itrVox->Data() ) {
            itrVox=itrVox->Del();
            continue;
        }

        vox = itrVox->Data();
        if ( !vox ) {
            itrVox=itrVox->Del();
            continue;
        }

        distList.Append( Vec3f::SquaredLen( vox->shape.GetCenter() - pos ) );
        itrVox = itrVox->GetNext();
    }

    // selection sort the linklists in parallel and then we will be done
    itrVox = voxelList.GetFirst();
    itrDist = distList.GetFirst();
    while ( itrVox != nullptr ) {
        
        Link< EntityTree* > *selectedVox = itrVox;
        Link< EntityTree* > *smallerVox = itrVox;
        Link< float > *selectedDist = itrDist;
        Link< float > *smallerDist = itrDist;

        ASSERT( selectedDist );
        ASSERT( selectedVox );

        lnkDist=itrDist->GetNext();
        lnkVox=itrVox->GetNext();

        while ( lnkDist != nullptr ) {
            if ( lnkDist->Data() < smallerDist->Data() ) {
                smallerDist = lnkDist;
                smallerVox = lnkVox;
            }

            lnkDist=lnkDist->GetNext();
            lnkVox=lnkVox->GetNext();
        }

        if ( smallerDist != selectedDist ) {
            std::swap( selectedDist->Data(), smallerDist->Data() );
            std::swap( selectedVox->Data(), smallerVox->Data() );
        }

        itrVox = itrVox->GetNext();
        itrDist = itrDist->GetNext();
    }
}

// entities at the start of the list will be closest to the pos
void SortEntityList( const Vec3f& pos, LinkList< std::weak_ptr< Entity > >& entList ) {
    LinkList< float > distList;
    Link< std::weak_ptr< Entity > >*itrEnt, *lnkEnt;
    Link< float > *itrDist, *lnkDist;

    // create a parallel linklist of floats that represent distance from our position
    for ( itrEnt = entList.GetFirst(); itrEnt; ) {
        if ( const auto ent = itrEnt->Data().lock() ) {
            distList.Append( Vec3f::SquaredLen(ent->GetOrigin() - pos) );
            itrEnt = itrEnt->GetNext();
        } else {
            itrEnt=itrEnt->Del();
        }
    }

    // selection sort the linklists in parallel and then we will be done
    itrEnt = entList.GetFirst();
    itrDist = distList.GetFirst();
    while ( itrEnt != nullptr ) {
        Link< std::weak_ptr< Entity > > *selectedEnt = itrEnt;
        Link< std::weak_ptr< Entity > > *smallerEnt = itrEnt;
        Link< float > *selectedDist = itrDist;
        Link< float > *smallerDist = itrDist;

        ASSERT( selectedDist );
        ASSERT( selectedEnt );

        lnkDist=itrDist->GetNext();
        lnkEnt=itrEnt->GetNext();

        while ( lnkDist != nullptr ) {
            if ( lnkDist->Data() < smallerDist->Data() ) {
                smallerDist = lnkDist;
                smallerEnt = lnkEnt;
            }

            lnkDist=lnkDist->GetNext();
            lnkEnt=lnkEnt->GetNext();
        }

        if ( smallerDist != selectedDist ) {
            std::swap( selectedDist->Data(), smallerDist->Data() );
            std::swap( selectedEnt->Data(), smallerEnt->Data() );
        }

        itrEnt = itrEnt->GetNext();
        itrDist = itrDist->GetNext();
    }
}

void EntityTree::vboPackOutline( VBO<float>& vbo, VBO<float>* color ) const {
    // if we take a color vbo, this is for a vbo_trace. otherwise it's for vbo_all
    if ( color && ( highlight != highlight_magic_int ) )
        return;

    if ( voxel ) {
        for ( Uint8 i=0; i<8; ++i ) {
            voxel[i]->vboPackOutline( vbo, color );
        }
        return;
    }

    float green_amount;
    float red_amount;
    float opacity;
    
    const int voxEntQtyToMakeFullyRed = globalVals.GetInt( gval_voxelColorWeight, 30 );

    if ( num_tickets >= voxEntQtyToMakeFullyRed ) {
        green_amount = 0;
        red_amount = 1;
        opacity = 1;
    } else {
        const float balance = ( num_tickets == 0 ) ? 0 : static_cast< float >( num_tickets ) / voxEntQtyToMakeFullyRed;

        green_amount = 1.0f - balance;
        red_amount = ( 1.0f / green_amount ) - 1.0f;
        opacity = 0.1f + balance;
    }
    
    vboPackVoxel( vbo );

    if ( color ) {
        vboPackOutlineColors( *color, Color4f( red_amount,green_amount,0,opacity ) );
    }
}

void EntityTree::vboPackVoxel( VBO<float>& vbo ) const {
    Box3D box;

    // convert AABox3D's min/max x/y/z values to coordinates via a box.
    box.Set( shape );

    // bottom face
    vbo.Pack( box.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ) );

    // top face
    vbo.Pack( box.VertAt( BOX_NEAR_LEFT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_LEFT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_LEFT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_RIGHT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_RIGHT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_LEFT_TOP_INDEX ) );

    // sides
    vbo.Pack( box.VertAt( BOX_NEAR_LEFT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_LEFT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_LEFT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_LEFT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_RIGHT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_FAR_RIGHT_BOTTOM_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_RIGHT_TOP_INDEX ) );
    vbo.Pack( box.VertAt( BOX_NEAR_RIGHT_BOTTOM_INDEX ) );
}

void EntityTree::vboPackOutlineColors( VBO<float>& vbo, const Color4f& color ) const {
    // now colors
     vbo.PackRepeatedly( 8*3, color );
}

void EntityTree::DrawOpaqueEnts( const EntityTree* rvoxel ) const {
    if ( !rvoxel )
        return;

    // don't need to draw if there's nothing in it
    if ( rvoxel->num_tickets < 1 )
        return;

    if ( !rvoxel->list ) {
        for ( Uint8 i=0; i<8; ++i ) {
            DrawOpaqueEnts( rvoxel->voxel[i] );
        }

        return;
    }

    std::shared_ptr< const Texture > diffuse;
    std::shared_ptr< const Material > mat;

    // ** only the deepest level in the tree has an ent list
    for ( auto const& ent_wptr : *rvoxel->list ) {
        auto ent = ent_wptr.lock();
        
        if ( !ent || !ent->IsVisible() )
            continue;

        bool defer = false;
        const uint id = ent->GetIndex();
        for ( auto & wptr : deferred_viewblocking_ents ) {
            if ( auto sptr = wptr.lock() ) {
                if ( sptr->GetIndex() == id ) {
                    defer = true;
                    break;
                }
            }
        }

        if ( !defer ) {
            renderer->DrawEntity( *ent );
            continue;
        }

        // save transparent objects, they will be rendered last
        mat = ent->GetMaterial();
        if ( mat ) {
            diffuse = mat->GetDiffuse();
            if ( diffuse ) {
                if ( diffuse->GetFormat() == GL_RGBA ) {
                    deferred_transparent_ents.Append( ent );
                    continue;
                }
            }
        }
    }
}

class Actor : public Entity { };

void EntityTree::Clear( void ) const {
    if ( voxel ) {
        for ( Uint8 i=0; i<8; ++i ) {
            voxel[i]->Clear();
        }
    } else { // if there are no children voxels, we have a list
        ASSERT( list );
        auto link = list->GetFirst();
        while ( link != nullptr ) {
            auto ent = link->Data().lock();
            if ( ent ) {
                link = link->GetNext(); // entity will have removed itself from this list, so we safely go to the next link
                ent->LeaveAllVoxels();
            } else {
                link = link->Del();
            }
        }
    }

    ASSERT( num_tickets == 0 );
}

Link< std::weak_ptr< Entity > >* EntityTree::AddEntity( Entity& entity ) {
    AdjustNumTickets( 1 );
    return list->Append( entity.GetWeakPtr() );
}

void EntityTree::DrawDeferredEnts( void ) {
    // add items between the camera and the player to the transparent list so they can be render-sorted and drawn transparent.
    for ( auto & ent : deferred_viewblocking_ents )
        if ( auto sptr = ent.lock() )
            deferred_transparent_ents.Append( sptr );

    // now sort and draw any transparent entities that were deferred for rendering
    SortEntityList( renderer->camera.GetOrigin(), deferred_transparent_ents );
    Link< std::weak_ptr< Entity > > *elink = deferred_transparent_ents.GetLast();
    while ( elink != nullptr ) {
        if ( auto ent = elink->Data().lock() )
            renderer->DrawEntity( *ent );
        elink = elink->Del_Reverse();
    }

    // we keep the list populated until now because DrawEntity() checks whether each entity is inside of it in order to give these objects transparency
    deferred_transparent_ents.DelAll();
    deferred_viewblocking_ents.DelAll();
}

void EntityTree::ShowAll( const bool whether ) {
    if ( whether ) {
        if ( !vbo_all ) {
            vbo_all = new VBO<float>(3,VBOChangeFrequencyT::RARELY);
        }
        
        vbo_all->Clear();
        vboPackOctree();
    } else {
        DELNULL( vbo_all );
    }
}

void EntityTree::ShowTrace( const bool whether ) {
    if ( whether ) {
        if ( !vbo_trace ) {
            vbo_trace = new VBO<float>(3,VBOChangeFrequencyT::FREQUENTLY);
            ASSERT( ! vbo_trace_color );
            vbo_trace_color = new VBO<float>(4,VBOChangeFrequencyT::FREQUENTLY);
        }
    } else {
        DELNULL( vbo_trace );
        DELNULL( vbo_trace_color );
    }
}

void EntityTree::DrawDebug( void ) const {
    if ( vbo_all ) {
        
        // the Draw function we called above packs the vbo completely
        vbo_all->MoveToVideoCard();
        
        glPointSize( 5 );

        if ( vbo_all->Finalized() ) {
            shaders->UseProg(  GLPROG_MINIMAL  );
            shaders->SendData_Matrices();

            const int color = shaders->GetUniformLoc("vColor");
            glUniform4f(color, 0.0f,0.0f,1.0f,1.0f);

            shaders->SetAttrib( "vPos", *vbo_all );
            shaders->DrawArrays( GL_LINES, 0, vbo_all->Num() );
            
            glPointSize( 1 ); // back to default
        }
    }

    if ( vbo_trace ) {

        ASSERT( vbo_trace_color );

        vbo_trace->MoveToVideoCard();

        if ( vbo_trace->Finalized() ) {
            vbo_trace_color->MoveToVideoCard();
            ASSERT( vbo_trace_color->Finalized() );

            glPointSize( 3 );
            shaders->UseProg(  GLPROG_GRADIENT  );
            shaders->SendData_Matrices();
            shaders->SetAttrib( "vPos", *vbo_trace );
            shaders->SetAttrib( "vColor", *vbo_trace_color );
            shaders->DrawArrays( GL_LINES, 0, vbo_trace->Num() );
            glPointSize( 1 ); // back to default
        }
    }
}

//todo: GetEntityTraceCollisions rename
bool EntityTree::GetEntityTraceCollisions( const IntersectData& coldata, const Primitive& primitive, std::vector< CollisionItem >* entList_out ) {
    CollisionReport3D report( CollisionCalcsT::None );
    report.all_collisions_out = entList_out;
    return GetEntityTraceCollisions( report, coldata, primitive );
}

std::shared_ptr< Entity > EntityTree::GetEntitySweepCollisions( const SweepData& coldata, const Primitive& primitive, std::vector< CollisionItem >* entList_out ) {
    CollisionReport3D report( CollisionCalcsT::None );
    report.all_collisions_out = entList_out;
    return GetEntitySweepCollisions( report, coldata, primitive );
}

std::shared_ptr< Entity > EntityTree::GetEntitySweepCollisions( CollisionReport3D& report, const SweepData& coldata, const Primitive& primitive ) {
    // ** first get the voxels we collides with
    LinkList< EntityTree * > voxelList;
    GetSweepVoxels( primitive, coldata.vel, voxelList, VoxelTypeT::Occupied ); //GetVoxelSweepCollisions

    // ** then get the entities out of each voxel we collide with
    if ( voxelList.Num() < 1 )
        return {};

    return Collision3D::SweepToEntityVoxelList( report, coldata, primitive, voxelList );
}

bool EntityTree::GetEntityTraceCollisions( CollisionReport3D& report, const IntersectData& coldata, const Primitive& primitive ) {
    // ** first get the voxels we collides with
    LinkList< EntityTree * > voxelList;
    GetTraceVoxels( primitive, voxelList, VoxelTypeT::Occupied );

    // ** then get the entities out of each voxel we collide with
    if ( voxelList.Num() < 1 )
        return false;

    return Collision3D::TraceToEntityVoxelList( coldata, primitive, voxelList, report.all_collisions_out ) != nullptr;
}

void EntityTree::GetMigrationVoxels( const Primitive& primitive, LinkList< EntityTree *>& voxelList_out, std::vector< EntityTree *>& allChecked_out ) {
    if ( !AppendUnique( allChecked_out, this ) )
        return;
    
    if ( !primitive.Intersects( *this ) )
        return;
    
    voxelList_out.Append( this );
    ASSERT( neighbors );
    for ( auto const & neighbor : *neighbors ) {
        neighbor->GetMigrationVoxels( primitive, voxelList_out, allChecked_out );
    }
}

void EntityTree::GetTraceVoxels( const Primitive& primitive, LinkList< EntityTree* >& voxelList_out, const VoxelTypeT voxelType ) {
    if ( vbo_trace ) {
        ASSERT( vbo_trace_color );
        vbo_trace->Clear();
        vbo_trace_color->Clear();
    }

    Private_GetTraceVoxels( primitive, voxelList_out, voxelType );

    if ( vbo_trace ) {
        ASSERT( vbo_trace_color );
        vbo_trace->MoveToVideoCard();
        vbo_trace_color->MoveToVideoCard();
    }
}

void EntityTree::GetSweepVoxels( const Primitive& primitive, const Vec3f& vel, LinkList< EntityTree* >& voxelList_out, const VoxelTypeT voxelType ) {
    if ( vbo_trace ) {
        ASSERT( vbo_trace_color );
        vbo_trace->Clear();
        vbo_trace_color->Clear();
    }

    Private_GetSweepVoxels( primitive, vel, voxelList_out, voxelType );

    if ( vbo_trace ) {
        ASSERT( vbo_trace_color );
        vbo_trace->MoveToVideoCard();
        vbo_trace_color->MoveToVideoCard();
    }
}

uint EntityTree::TryMigrate( Entity& ent ) {
    auto & tickets = Entity::EntityTreeAttorney::TicketsRef(ent);
    if ( tickets.Num() < 1 ) {
        return TryInsert( ent );
    }
    
    LinkList< EntityTree* > migratedToVoxels;
    std::vector< EntityTree* > allCheckedVoxels;
    allCheckedVoxels.reserve( MAX_POSSIBLE_TOUCHING_VOXELS );
    for ( auto & ticket : tickets ) {
        auto vox = ticket.GetTree();
        ASSERT( vox );
        vox->GetMigrationVoxels( *ent.bounds.primitive, migratedToVoxels, allCheckedVoxels );
    }

    ent.LeaveAllVoxels();
    
    if ( migratedToVoxels.Num() < 1 ) {
        return TryInsert( ent );
    }
    
    for ( auto vox : migratedToVoxels ) {
        // giving the entity a ticket adds it to the voxel
        tickets.Append( EntityTreeTicket( ent, *vox ) );
    }
    
    return migratedToVoxels.Num();
}

uint EntityTree::TryInsert( Entity& ent ) {
    LinkList< EntityTree* > entList;
    GetTraceVoxels( *ent.bounds.primitive, entList, VoxelTypeT::All );

    Link<EntityTree*>* voxlink = entList.GetFirst();
    while ( voxlink != nullptr ) {
        EntityTree* vox = voxlink->Data();

        // if already in the voxel, don't append (better to cycle the entity's list since he is likely to be in 8 or less voxels)
        if ( ent.IsInVoxel( *vox ) ) {
            voxlink = voxlink->GetNext();
            continue;
        }

        // giving the entity a ticket adds it to the voxel
        auto & tickets = Entity::EntityTreeAttorney::TicketsRef(ent);
        tickets.Append( EntityTreeTicket( ent, *vox ) );

        voxlink = voxlink->GetNext();
    }

    return entList.Num(); // this will be the number of voxels we added to
}

void EntityTree::DrawOpaqueEnts_InVoxelList( const LinkList< EntityTree* >& voxelList ) {
    const Link< EntityTree* >* vox_link = voxelList.GetFirst();

    while ( vox_link ) {
        if ( vox_link->Data() ) {
            vox_link->Data()->DrawOpaqueEnts();
        }
        vox_link = vox_link->GetNext();
    }
}

const LinkList< std::weak_ptr< Entity > >& EntityTree::GetEntityListRef( void ) const {
    ASSERT( list );
    return *list;
}

Link< std::weak_ptr< Entity > >* EntityTree::EntityTreeTicketAttorney::AddEntity( EntityTree& tree, Entity& entity ) {
    return tree.AddEntity( entity );
}

void EntityTree::EntityTreeTicketAttorney::RemoveEntity( EntityTree& tree, Link< std::weak_ptr< Entity > >* occupied_link ) {
    tree.RemoveEntity( occupied_link );
}

uint EntityTree::NumSubEntities( void ) const {
    return num_tickets > 0;
}

uint EntityTree::NumSubdivisions( void ) const {
    return subdivisions;
}

void EntityTree::Private_GetTraceVoxels( const Primitive& primitive, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType ) {
    Private_GetTraceVoxels( *this, primitive, voxelList, voxelType );
}

void EntityTree::Private_GetSweepVoxels( const Primitive& primitive, const Vec3f& vel, LinkList< EntityTree *>& voxelList, const VoxelTypeT voxelType ) {
    Private_GetSweepVoxels( *this, primitive, vel, voxelList, voxelType );
}

void EntityTree::DrawOpaqueEnts( void ) const {
    DrawOpaqueEnts( this );
}

void EntityTree::MakeEntTransparentForNextFrame( const std::weak_ptr< Entity >& ent ) {
    deferred_viewblocking_ents.Append( ent );
}
