// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./viewFrustum.h"
#include "../math/collision/collision.h"
#include "../rendering/renderer.h"
#include "../rendering/vbo.h"
#include "../game.h"

void ViewFrustum::Init( void ) {
    const AABox3D unit_cube(
      -1,1
      ,-1,1
      ,-1,1
    );

    Mat4f mat( renderer->Projection() );
    mat.Invert();
    
    viewFrust = Box3D(
          unit_cube.GetVert_FarLeftTop() * mat
        , unit_cube.GetVert_FarRightTop() * mat
        , unit_cube.GetVert_NearLeftTop() * mat
        , unit_cube.GetVert_NearRightTop() * mat
        , unit_cube.GetVert_NearLeftBottom() * mat
        , unit_cube.GetVert_NearRightBottom() * mat
        , unit_cube.GetVert_FarLeftBottom() * mat
        , unit_cube.GetVert_FarRightBottom() * mat
    );
}

ViewFrustum::ViewFrustum( void )
    : modelView()
    , viewFrust()
    , vbo(nullptr)
{

}

ViewFrustum::~ViewFrustum( void ) {
    DELNULL( vbo );
}

void ViewFrustum::DrawOpaqueEnts( void ) {
    
    const bool frustDetached = globalVals.GetBool( gval_v_frustDetach );
    if ( !frustDetached )
        modelView = renderer->ModelView();

    DrawOutline();
    DrawVisibleOpaqueEnts();
}

void ViewFrustum::DrawOutline( void ) {
    if ( !globalVals.GetBool( gval_d_frust ) )
        return;

    if ( ! vbo )
        vbo = new VBO< float >(3,VBOChangeFrequencyT::RARELY);

    vboPack();
    ASSERT( vbo->Finalized() );

    shaders->UseProg(  GLPROG_MINIMAL  );
    shaders->SendData_Matrices();

    //shaders->SendData_ProjectionMat();
    //const static Mat4f identity_matrix;
    //SendData_ModelViewMat( identity_matrix );

    shaders->SetUniform4f("vColor" , 1,0,1,1 );
    shaders->SetAttrib( "vPos", *vbo );
    shaders->DrawArrays( GL_LINES, 0, vbo->Num() );
}

void ViewFrustum::vboPack( void ) {
    ASSERT( vbo );
    vbo->Clear();

    Mat3f rot_invert( modelView );
    rot_invert.Transpose();
    const Vec3f tran_invert( modelView.GetTranslation() );
    
    const Box3D viewFrust_inModelViewSpace(
        (viewFrust.VertAt(0) -tran_invert) * rot_invert
        , (viewFrust.VertAt(1) -tran_invert) * rot_invert
        , (viewFrust.VertAt(2) -tran_invert) * rot_invert
        , (viewFrust.VertAt(3) -tran_invert) * rot_invert
        , (viewFrust.VertAt(4) -tran_invert) * rot_invert
        , (viewFrust.VertAt(5) -tran_invert) * rot_invert
        , (viewFrust.VertAt(6) -tran_invert) * rot_invert
        , (viewFrust.VertAt(7) -tran_invert) * rot_invert
    );
    
    // if we pack the top, bottom, left, and right sides, that will draw lines on all edges.

    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_TOP_INDEX) );

    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );

    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_LEFT_TOP_INDEX) );

    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_TOP_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_NEAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_BOTTOM_INDEX) );
    vbo->Pack( viewFrust_inModelViewSpace.VertAt(BOX_FAR_RIGHT_TOP_INDEX) );

    vbo->MoveToVideoCard();
}

void ViewFrustum::DrawVisibleOpaqueEnts( void ) const {

    Mat3f rot_opposite( modelView );
    rot_opposite.Transpose();
    const Vec3f translation( modelView.GetTranslation() );
    
    const Box3D viewFrust_inModelViewSpace(
        (viewFrust.VertAt(0) -translation) * rot_opposite
        , (viewFrust.VertAt(1) -translation) * rot_opposite
        , (viewFrust.VertAt(2) -translation) * rot_opposite
        , (viewFrust.VertAt(3) -translation) * rot_opposite
        , (viewFrust.VertAt(4) -translation) * rot_opposite
        , (viewFrust.VertAt(5) -translation) * rot_opposite
        , (viewFrust.VertAt(6) -translation) * rot_opposite
        , (viewFrust.VertAt(7) -translation) * rot_opposite
    );
    
    const Primitive_Box3D primitive( viewFrust_inModelViewSpace );

    LinkList< EntityTree* > voxelList;
    game->entTree->GetTraceVoxels( primitive, voxelList, VoxelTypeT::Occupied );

    // performance_todo: check for entities that are outside of the viewFrust_inModelViewSpace as well?

    if ( globalVals.GetBool( gval_v_renderSort ) ) {
        EntityTree::SortVoxelList( renderer->camera.GetOrigin(), voxelList );
    }
    
    EntityTree::DrawOpaqueEnts_InVoxelList( voxelList );
}
