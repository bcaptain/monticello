// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"

#include "./entityTreeTicket.h"
#include "./entityTree.h"

EntityTreeTicket::EntityTreeTicket( Entity& entity, EntityTree& voxel )
    : occupied_voxel( &voxel )
    , occupied_link( EntityTree::EntityTreeTicketAttorney::AddEntity( voxel, entity ) ) // adds entity to the voxel
{
    ASSERT( occupied_link );
}

EntityTreeTicket::~EntityTreeTicket( void ) {
    if ( occupied_voxel ) {
        ASSERT( occupied_link );
        auto occupied_link_sptr = occupied_link->Data().lock();
        EntityTree::EntityTreeTicketAttorney::RemoveEntity( *occupied_voxel, occupied_link );
    }
}

EntityTreeTicket::EntityTreeTicket( EntityTreeTicket&& other_rref )
    : occupied_voxel( other_rref.occupied_voxel )
    , occupied_link( other_rref.occupied_link )
{
    other_rref.occupied_link = nullptr;
    other_rref.occupied_voxel = nullptr;
}

NORETURN EntityTreeTicket& EntityTreeTicket::operator=( [[maybe_unused]] EntityTreeTicket&& other_rref ) {
    ASSERT_MSG( false, "copying tickets doesn't make sense.\n");
}

bool EntityTreeTicket::IsForVoxel( const EntityTree& voxel ) const {
    return &voxel == occupied_voxel;
}

EntityTree* EntityTreeTicket::GetTree( void ) const {
    return occupied_voxel;
}
