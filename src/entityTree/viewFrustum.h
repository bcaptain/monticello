// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_OBJECTTREE_VIEWFRUSTUM_H_
#define SRC_OBJECTTREE_VIEWFRUSTUM_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "../math/geometry/3d/box.h"

template< typename TYPE >
class VBO;
class EntityTree;

typedef Uint8 FrustumPlaneIndexT_BaseType;
enum class FrustumPlaneIndexT : FrustumPlaneIndexT_BaseType {
    FRUST_PLANE_TOP=0,
    FRUST_PLANE_BOTTOM=1,
    FRUST_PLANE_LEFT=2,
    FRUST_PLANE_RIGHT=3,
    FRUST_PLANE_FFAR=4,
    FRUST_PLANE_NNEAR=5
};

class ViewFrustum {
public:
    ViewFrustum( void );
    ~ViewFrustum( void );
    ViewFrustum( const ViewFrustum& other ) = delete;

public:
    ViewFrustum& operator=( const ViewFrustum& other ) = delete;

public:
    void Init( void );
    void DrawOpaqueEnts( void );

private:
    void DrawVisibleOpaqueEnts( void ) const;
    void DrawOutline( void );
    void vboPack( void );

private:
    Mat4f modelView;
    Box3D viewFrust;
    VBO< float > *vbo;
};

#endif  // SRC_OBJECTTREE_VIEWFRUSTUM_H_
