// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_OBJECTTREE_ENTITYTREETICKET_H_
#define SRC_OBJECTTREE_ENTITYTREETICKET_H_

#include "./clib/src/warnings.h"
#include "../base/main.h"
#include "./entityTreeTicket.h"

class EntityTree;

class EntityTreeTicket {
public:
    EntityTreeTicket( void ) = delete;
    EntityTreeTicket( const EntityTreeTicket& other ) = delete; //!< copying not allowed since it is not reference counted and would invalidate the ticket
    EntityTreeTicket( EntityTreeTicket&& other_rref );
    EntityTreeTicket( Entity& entity, EntityTree& voxel );
    ~EntityTreeTicket( void );

public:
    EntityTreeTicket& operator=( const EntityTreeTicket& other ) = delete; //!< copying not allowed since it is not reference counted and would invalidate the ticket
    EntityTreeTicket& operator=( EntityTreeTicket&& other_rref );

    bool IsForVoxel( const EntityTree& voxel ) const;
    EntityTree* GetTree( void ) const;

private:
    EntityTree* occupied_voxel;
    Link< std::weak_ptr< Entity > >* occupied_link;
};

#endif  // SRC_OBJECTTREE_ENTITYTREETICKET_H_
