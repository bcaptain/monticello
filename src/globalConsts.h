// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_GLOBALCONSTS
#define SRC_GLOBALCONSTS

#include "./base/main.h"

extern std::vector< std::pair< std::string, std::string > > varDescriptions;

constexpr const char *const str_uiJoyOpenMenu("uiJoyOpenMenu"); //!< Generally the Start button
constexpr const char *const str_uiJoyAccept("uiJoyAccept"); //!< Generally the Cross/A button
constexpr const char *const str_uiJoyCancel("uiJoyCancel"); //!< Generally the Circle/B button
constexpr const char *const str_uiJoyUp("uiJoyUp");
constexpr const char *const str_uiJoyDown("uiJoyDown");
constexpr const char *const str_uiJoyLeft("uiJoyLeft");
constexpr const char *const str_uiJoyRight("uiJoyRight");
constexpr const char *const str_uiJoyOpt1("uiJoyOpt1"); //!< Generally the X/Sqaure button
constexpr const char *const str_uiJoyOpt2("uiJoyOpt2"); //!< Generally the Y/Triangle button
constexpr const char *const str_uiJoyBumperL("uiBumperL");
constexpr const char *const str_uiJoyBumperR("uiBumperR");

void PopulateConsoleVarDescriptions( void );

#define PrecomputeHash_GlobalVal( key, description ) \
    constexpr const std::pair< const char *const, const std::size_t > gval_##key( #key, Hashes::OAT_CONSTEXPR( #key ) ); \
    constexpr const std::pair< const char *const, const char *const > gvaldesc_##key( gval_##key.first, description );
    
// Things to Allow
PrecomputeHash_GlobalVal( a_editor, "bool: allow editor" )
PrecomputeHash_GlobalVal( a_console, "bool: allow console")
PrecomputeHash_GlobalVal( a_menu, "bool: whether the menu can be opened")

// States
PrecomputeHash_GlobalVal( s_menu, "bool: whether the menu is open")
PrecomputeHash_GlobalVal( s_levelTimerExpired, "bool: whether the level timer has expired")

// Drawing & Debug
PrecomputeHash_GlobalVal( d_bounds, "bool: draw bounding primitive")
PrecomputeHash_GlobalVal( d_boundsMode, "uint: draw type: 0=lines, 1=solid, 2=gradient")
PrecomputeHash_GlobalVal( d_boundsAlpha, "bool: bounding primitive draw opacity")
PrecomputeHash_GlobalVal( d_bindLines, "bool: draw lines between bound entities and their masters")
PrecomputeHash_GlobalVal( d_boneNames, "bool: draw bone names in armatures")
PrecomputeHash_GlobalVal( d_facingDir, "bool: draw line indicating facing direcion of actors")
PrecomputeHash_GlobalVal( d_playerSpeed, "bool: print player's speed in output window")
PrecomputeHash_GlobalVal( d_aiState, "bool: print current AI state of monsters to console")
PrecomputeHash_GlobalVal( d_fps, "bool: draw frames per sec")
PrecomputeHash_GlobalVal( d_compass, "bool: draw on-screen compass")
PrecomputeHash_GlobalVal( d_ghost, "bool: draw ghost if entity before placing in editor")
PrecomputeHash_GlobalVal( d_grid, "bool: draw x/y grid in editor")
PrecomputeHash_GlobalVal( d_gridOpacity, "bool: opacity of x/y grid in editor (d_grid to turn on)")
PrecomputeHash_GlobalVal( d_testAnim, "bool: whether to draw a model/anim (for testing)")
PrecomputeHash_GlobalVal( d_frust, "bool: draw view frustum")
PrecomputeHash_GlobalVal( d_projectors, "bool: draw projector frustums")
PrecomputeHash_GlobalVal( d_projections, "bool: draw projector's projections")
PrecomputeHash_GlobalVal( d_models, "bool: draw models")
PrecomputeHash_GlobalVal( d_clipModels, "bool: draw collision models")
PrecomputeHash_GlobalVal( d_flatLines, "bool: draw the triangle seams of Flat (floor) entities")
PrecomputeHash_GlobalVal( d_names, "bool: draw entity names" )
PrecomputeHash_GlobalVal( d_vertNormals, "bool: draw Vertex Normals")
PrecomputeHash_GlobalVal( d_vertNormalsLen, "float: length of vertex normals")
PrecomputeHash_GlobalVal( d_noFade, "bool: don't fade entities between player and camera")
PrecomputeHash_GlobalVal( d_skel, "bool: draw model armatures.")
PrecomputeHash_GlobalVal( d_triggerLines, "bool: draw lines between triggers and their targets")
PrecomputeHash_GlobalVal( d_viewCones, "bool: draw actor view cones")
PrecomputeHash_GlobalVal( d_debugPoints, "bool: draw debug glPoints")
PrecomputeHash_GlobalVal( d_crossHair, "bool: draw crossHair (point)")
PrecomputeHash_GlobalVal( d_worldOrigin, "bool: draw point at center of world (0,0,0)")
PrecomputeHash_GlobalVal( d_attackCollision, "bool: draw shape of attack collision from anim cues")
PrecomputeHash_GlobalVal( d_mapDebug, "whether to draw debug info for generated maps.")
PrecomputeHash_GlobalVal( d_axisLineLength, "float: length of orientation axis lines.")
PrecomputeHash_GlobalVal( d_globalAxis, "bool: draw global axis on hud")
PrecomputeHash_GlobalVal( d_playerStatusBar, "bool: draw player status bar on hud")
PrecomputeHash_GlobalVal( d_damageFeedback, "bool: colorize characters for damage feedback")

//Playtesting Only
PrecomputeHash_GlobalVal( pt_moveSpeedAdjust, "float : adjustable value added to moveSpeed")

// Drawing & Debugging Octree
//todo PrecomputeHash_GlobalVal( d_voxelDensity("d_voxelDensity, "bool: draw each voxel, colorized for entity density" )
PrecomputeHash_GlobalVal( d_voxelTrace, "bool: draw the voxels that were last checked for collision (might be slow)" )
PrecomputeHash_GlobalVal( d_octree, "bool: draw each octree outline")
PrecomputeHash_GlobalVal( voxelColorWeight, "uint: number of entities in voxel before it is colored red (debug)")
PrecomputeHash_GlobalVal( g_octreeSize, "Vec3f: dimensions of octree in meters. each axis must be > 0.")
PrecomputeHash_GlobalVal( g_octreeSubs, "Vec3f: number of octree subdivisions per axis")

// Editor
PrecomputeHash_GlobalVal( e_pan_scale, "float from 0 to 1: how fast screen pans in editor mode")
PrecomputeHash_GlobalVal( e_zoom_scale, "float: distance the camera moves when zooming in editor mode")
PrecomputeHash_GlobalVal( e_snapGridDistance, "float: increment at which entities will snap to the grid when in snap-to mode, can be 1.0 or 0.5")

// Other
PrecomputeHash_GlobalVal( god, "bool: whether player can be damaged")
PrecomputeHash_GlobalVal( volume, "uint: 0 to 100, global volume level.")
PrecomputeHash_GlobalVal( crosshairPos, "float: adjusted each frame by the joystick.")

// Game Stuff
PrecomputeHash_GlobalVal( g_creep, "bool: whether to allow player to move more slowly than full move speed")
PrecomputeHash_GlobalVal( g_noClip, "bool: turn off clipping (and gravity)")
PrecomputeHash_GlobalVal( g_autorun, "bool: Whether default movement is to run instead of walk")
PrecomputeHash_GlobalVal( g_faceMoveDir, "bool: Whether player faces the direction he is moving")
PrecomputeHash_GlobalVal( g_friction, "float: speed loss in units-per-second at 100% friction.")
PrecomputeHash_GlobalVal( g_noTrample, "bool: whether to disallow actors from walking over one another.")
PrecomputeHash_GlobalVal( g_maxIncline, "float (degrees): maximum surface angle to allow actors to walk up.")
PrecomputeHash_GlobalVal( g_throwVel, "float: velocity that actors throw things")
PrecomputeHash_GlobalVal( g_noLevelSummary, "bool: ignore level finished conditions")

PrecomputeHash_GlobalVal( grav_disable, "bool: turn off gravity")
PrecomputeHash_GlobalVal( grav_force, "float: gravity force in meters per sec, per sec.")
PrecomputeHash_GlobalVal( grav_dir, "vec3f: normalized direction of gravity force")
PrecomputeHash_GlobalVal( grav_terminal, "float: meters/sec at which gravity will cease downward acceleration.")

// Input Basics
PrecomputeHash_GlobalVal( i_mouseInvertVert, "bool: whether to invert vertical axis.")
PrecomputeHash_GlobalVal( i_mouseSens, "bool: mouse sensitivity multiplier.")

// Video
PrecomputeHash_GlobalVal( i_gl, "bool: print OpenGL info at program start")
PrecomputeHash_GlobalVal( i_glext, "bool: print OpenGL extentions info at program start")
PrecomputeHash_GlobalVal( v_frustDetach, "bool: detach view frustum from the camera")
PrecomputeHash_GlobalVal( v_fullscreen, "bool: whether game is in fullscreen")
PrecomputeHash_GlobalVal( v_borderlessWindow, "bool: whether game window should draw a border")
PrecomputeHash_GlobalVal( v_cullFace, "string (none,back,front,both): which faces to cull")
PrecomputeHash_GlobalVal( v_farPlane, "float: OpenGL far plane distance")
PrecomputeHash_GlobalVal( v_fov, "float: field of view")
PrecomputeHash_GlobalVal( v_height, "uint: video height")
PrecomputeHash_GlobalVal( v_nearPlane, "float: OpenGL near plane distance")
PrecomputeHash_GlobalVal( v_width, "uint: video width")
PrecomputeHash_GlobalVal( v_frustCull, "bool: whether to use frustum culling")
PrecomputeHash_GlobalVal( v_launchOnDisplay, "uint: Monitor on which to launch game 0:PRIMARY, 1:SECONDARY, etc..")
PrecomputeHash_GlobalVal( v_mipmap, "bool: whether to initalize textures with mipmaps. default: 1")
PrecomputeHash_GlobalVal( v_renderSort, "bool: whether to ort voxels for rendering")
PrecomputeHash_GlobalVal( v_pauseAfterRender, "bool: whether pause after sdl.SwapBuffers()")
PrecomputeHash_GlobalVal( v_keepFramelyShapes, "bool: whether Framely Shapes are cleared after drawing")

// Light
PrecomputeHash_GlobalVal( d_lights, "bool: draw light entities")
PrecomputeHash_GlobalVal( d_light, "bool: draw light")
PrecomputeHash_GlobalVal( v_light, "bool: render using lighting shaders (requires lights entities in map)")

// Joy Config
PrecomputeHash_GlobalVal( d_joyInput, "bool: show controller input.")
PrecomputeHash_GlobalVal( axis_buttonThreshold, "float (0 to 1): axis is considered a pressed button if axis is pushed >/= this value.")
PrecomputeHash_GlobalVal( axis0_sens, "float: joystick axis0 sensitivity multiplier.")
PrecomputeHash_GlobalVal( axis1_sens, "float: joystick axis1 sensitivity multiplier.")
PrecomputeHash_GlobalVal( axis2_sens, "float: joystick axis2 sensitivity multiplier.")
PrecomputeHash_GlobalVal( axis3_sens, "float: joystick axis3 sensitivity multiplier.")
PrecomputeHash_GlobalVal( axis4_sens, "float: joystick axis4 sensitivity multiplier.")
PrecomputeHash_GlobalVal( axis5_sens, "float: joystick axis5 sensitivity multiplier.")
PrecomputeHash_GlobalVal( axis0_deadzone, "float (0 to 1): left axis movement below this value will be ignored.")
PrecomputeHash_GlobalVal( axis1_deadzone, "float (0 to 1): left axis movement below this value will be ignored.")
PrecomputeHash_GlobalVal( axis2_deadzone, "float (0 to 1): left axis movement below this value will be ignored.")
PrecomputeHash_GlobalVal( axis3_deadzone, "float (0 to 1): left axis movement below this value will be ignored.")
PrecomputeHash_GlobalVal( axis4_deadzone, "float (0 to 1): left axis movement below this value will be ignored.")
PrecomputeHash_GlobalVal( axis5_deadzone, "float (0 to 1): left axis movement below this value will be ignored.")
PrecomputeHash_GlobalVal( axis0_invert, "bool: whether to invert axis 0")
PrecomputeHash_GlobalVal( axis1_invert, "bool: whether to invert axis 1")
PrecomputeHash_GlobalVal( axis2_invert, "bool: whether to invert axis 2")
PrecomputeHash_GlobalVal( axis3_invert, "bool: whether to invert axis 3")
PrecomputeHash_GlobalVal( axis4_invert, "bool: whether to invert axis 4")
PrecomputeHash_GlobalVal( axis5_invert, "bool: whether to invert axis 5")

// Flow Field
PrecomputeHash_GlobalVal( d_ffPush, "bool: draw push vector (path finding)")
PrecomputeHash_GlobalVal( d_flowfield, "bool: draw vector flow-field (path finding)")
PrecomputeHash_GlobalVal( ai_flowfield, "bool: whether to use hybrid flowfield/navmesh for pathfinding actors.")
PrecomputeHash_GlobalVal( ai_ffPush, "bool: if ai_flowfield is on, this turns on local obstacle avoidance using a push vector.")
PrecomputeHash_GlobalVal( ai_disable, "bool: Whether AI is enabled/disabled")
PrecomputeHash_GlobalVal( ai_autoFindPlayer, "bool: enemies will be aware of player at all times")

// Nav Mesh
PrecomputeHash_GlobalVal( d_navAll, "bool: draw all debug information about navmesh pathfinding")
PrecomputeHash_GlobalVal( d_navLines, "bool: draw navmesh pathfinding lines")
PrecomputeHash_GlobalVal( d_navMesh, "bool: draw navmesh in editor mode")
PrecomputeHash_GlobalVal( d_navOccupied, "bool: highlight navmesh polies that are not empty")
PrecomputeHash_GlobalVal( d_navPolies, "bool: draw navmesh pathfinding navmesh polies")
PrecomputeHash_GlobalVal( d_navPortals, "bool: draw navmesh pathfinding navmesh portals")

// Camera
PrecomputeHash_GlobalVal( cam_collision, "bool: whether to place camera in front of entities between it and the focus entity")
PrecomputeHash_GlobalVal( cam_zoom, "float (meters): distance of camera from it's focus origin")
PrecomputeHash_GlobalVal( cam_tilt, "float: Degrees that the camera is tilted.")
PrecomputeHash_GlobalVal( cam_rot, "float: Degrees that the camera is rotated.")
PrecomputeHash_GlobalVal( cam_radius, "float: the radius of the camera and its collision capsule.")
PrecomputeHash_GlobalVal( cam_drawCap, "bool: set to true to draw the camera collision capsule for the current frame.")
PrecomputeHash_GlobalVal( cam_drawSphere, "bool: set to true to draw the camera collision sphere for the current frame.")
PrecomputeHash_GlobalVal( cam_focalOffset, "vec3f: the point in the world that the camera looks at and rotates around (relative to any \"follow entity\"/player.")
PrecomputeHash_GlobalVal( cam_noReset, "bool: Prevent camera position from resetting - ever")
PrecomputeHash_GlobalVal( cam_moveRelative, "bool: rotate player movement vectors relative to camera rotation")
PrecomputeHash_GlobalVal( cam_tether, "bool: whether to move camera relative to an entity")
PrecomputeHash_GlobalVal( cam_tetherSlack, "float: distance between player and camera before it moves with the player")

PrecomputeHash_GlobalVal( d_animSpeed, "float: speed to play test anim")
PrecomputeHash_GlobalVal( d_animBlend, "uint: number of blend frames between animations")
PrecomputeHash_GlobalVal( d_animBlendFull, "bool: if true, will run d_animBlend at full frame rate")


#endif  // SRC_GLOBALCONSTS

