// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./clib/src/warnings.h"
#ifndef SRC_INVENTORY_H
#define SRC_INVENTORY_H

const char* icon_steamthrower = "item_steamthrower";

#endif  // SRC_INVENTORY_H
