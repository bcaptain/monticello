// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_SOUND_H_
#define SRC_SOUND_H_

#include "./clib/src/warnings.h"
#include "./base/main.h"
#include "./files/manifest.h"
#include "./files/assetManager.h"

extern const int SDL_AUDIO_MONO;
extern const int SDL_AUDIO_STEREO;
extern const int MONTICELLO_SOUND_AUDIOCHANNELS;
extern const int MONTICELLO_SOUND_FREQUENCY;
extern const Uint16 MONTICELLO_SOUND_FORMAT;
extern const int MONTICELLO_SOUND_CHUNKSIZE;
extern const int MONTICELLO_SOUND_CHANNELS;
extern const int MAX_HEARABLE_DISTANCE_FROM_CAMERA;
extern const int MAX_HEARABLE_DISTANCE_FROM_CAMERA_SQ;
extern const int MAX_VOLUME;

struct Mix_Chunk;

class SoundData {
public:
    SoundData( void ) : name(), chunk(nullptr) {}
    SoundData( const SoundData& other ) : name( other.name ), chunk( other.chunk ) {}
    SoundData& operator=( const SoundData& other ) { name = other.name; chunk = other.chunk; return *this; }
    ~SoundData( void ) = default;

public:
    void SetName( const std::string to ) { name = to; }
    std::string GetName( void ) const { return name; } 
    
public:
    std::string name;
    Mix_Chunk* chunk;
};

void FreeMixChunk( void* chunk );

/*!

SoundManager \n\n

holds all the sounds for the game

**/

class SoundManager : public AssetManager< AssetData<SoundData>, SoundData > {
public:
    SoundManager( void );

public:
    bool Load( const std::string& handle );
    void UnloadAll( void );
    void BeingUnloaded( SoundData& asset ) override;

    bool Play( const char *handle, const int volume=MAX_VOLUME, const int loop_times=0 ); //!< volume is between 0 and 128
    bool PlayAt( const Vec3f& origin, const char *handle, const int volume=MAX_VOLUME ); //!< volume is between 0 and 128

private:
    bool Load( File& opened_sound_file ) override; //!< Load WAV, OGG, MP3, MOD, MIDI (Anything supported by Mix_LoadWAV)
    using AssetManager< AssetData<SoundData>,SoundData >::Load;
    int GetVolumeForDistance( const Vec3f& origin, const int volume );

private:
    static bool instantiated;
};

extern SoundManager soundManager;
#endif  // SRC_SOUND_H_

