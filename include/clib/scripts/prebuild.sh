#!/bin/bash

g++ --version

if [[ -e "../../../run/libclib.so" ]] ; then
	rm -f "../../../run/libclib.so"
fi
if [[ -e "../../../lib/lin/libclib.a" ]] ; then
	rm -f "../../../lib/lin/libclib.a"
fi
exit 0
