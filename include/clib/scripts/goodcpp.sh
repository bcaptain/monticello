#/bin/bash

# todo: acknowledge template class types

tmpfile="/tmp/goodcpp.tmp"
classesAll="/tmp/classesAll.tmp"
cppfiles="/tmp/cppfiles.tmp"
hfiles="/tmp/hfiles.tmp"
allfiles="/tmp/allfiles.tmp"

TOKEN="([a-zA-Z0-9]+)"
ARGS="(\(\s*[a-zA-Z0-9]+.*\))"

CONSTRUCTOR_WITH_NAMESPACE="$TOKEN::\1"
DESTRUCTOR_WITH_NAMESPACE="$TOKEN::~\1"
ANYSTRUCTOR_WITH_NAMESPACE="$TOKEN::(?~)\1"
FUNCTION="$TOKEN($ARGS)\s*;"
COPY_OPERATOR_DEF="\soperator=\s*\("
CLASSNAME="[a-zA-Z_][a-zA-Z_0-9_]*"

derived_classes=()
nonpoly_classes=()
base_classes=()

CONSTRUCTOR_WITH_NAMESPACE_NO_VOID="$CONSTRUCTOR_WITH_NAMESPACE\(\s*\)"
DESTRUCTOR_NO_VOID="~$TOKEN\(\s*\)"

# ****
#
# Sed/Grep helpers
#
# ****

function remove_multiline_comment_oneline {
	sed 's/[/][*].*[*][/]//g'
}

function remove_multiline_comment {
	sed 's/[/][*].*//g'
}

function remove_comment {
	sed 's/[/][/].*//g'
}

function remove_all_comments {
	remove_multiline_comment_oneline |
		remove_multiline_comment |
		remove_comment
}

function get_class_decls {
	egrep '(class|struct)\s+([a-zA-Z_][a-zA-Z_0-9]*)' "${@}" -Rh |
		egrep -v 'friend\s+(class|struct)'
}

function extract_base_classname_from_def {
	sed 's/[^:]*\:\(.*\)/\1/g' |
		sed 's/\s*public\s*//g' |
		sed 's/\s*private\s*//g' |
		sed 's/\s*virtual\s*//g' |
		sed 's/\s*{\s*//g'
}

function extract_derived_classname_from_def {
	sed 's/\s*\('"$CLASSNAME"'\).*/\1/g'
}

function prune_return_type_from_copy_operator_def {
	sed 's/.*\s\([a-zA-Z_][a-zA-Z0-9_<>]*\)[&\*]*\s\s*operator=.*/\1/g' |
		sed 's/<.*>//g'
}

# ****
#
# Checks
#
# ****

#check for single-arguemnt constructors without explicit
#todo: find a way to find constructors inside class definitions
#todo: this creates false positives for manually calling destructors
# find copy constructors that dont call baseclass copyconstructor in itinializer list
function Check_StructorsWithoutVoid {
	egrep "$CONSTRUCTOR_WITH_NAMESPACE_NO_VOID" "${@}" -Rn > "$tmpfile"
	egrep "$DESTRUCTOR_NO_VOID" "${@}" -Rn >> "$tmpfile"
	while read problem; do
		echo "Problem: arg-less d/ctor without 'void': $problem"	
	done < "$tmpfile"
}

function Check_DefNotVirtual {
	sed "$SED_REMOVE_MULTILINE_COMMENT" | remove_all_comments | grep -v '::' | grep -v '=\s*delete' | grep -v virtual
}

function Check_DefNotOverride {
	sed "$SED_REMOVE_MULTILINE_COMMENT" | remove_all_comments | grep -v '::' | grep -v '=\s*delete' | grep -v override
}

function Check_DerivedCopyOpNotOverride {
	egrep "$COPY_OPERATOR_DEF" -Rn "${@}" | Check_DefNotOverride > "$tmpfile"
	while read problem; do
		classname=` echo "$problem" | prune_return_type_from_copy_operator_def`

		# don't report for base classes
		doit=1
		for base in ${base_classes[@]} ; do
			if [[ "$base" == "$classname" ]] ; then
				doit=0
				break
			fi
		done
		
		if [[ "$doit" == "1" ]] ; then
			# don't report for nonpolymorphic classes
			for nonpoly in ${nonpoly_classes[@]} ; do
				if [[ "$nonpoly" == "$classname" ]] ; then
					doit=0
					break				
				fi
			done
			if [[ "$doit" == "1" ]] ; then
				echo "Taste: derived class's copy operator not declared override: $problem"	
			fi
		fi
	done < "$tmpfile"
}

function Check_BaseCopyOpNotVirtual {
	egrep "$COPY_OPERATOR_DEF" -Rn "${@}" | Check_DefNotVirtual > "$tmpfile"
	
	while read problem; do
		classname=` echo "$problem" | prune_return_type_from_copy_operator_def`
		
		# don't report for derived classes
		doit=1
		for derived in ${derived_classes[@]} ; do
			if [[ "$derived" == "$classname" ]] ; then
				doit=0
				break
			fi
		done
		
		if [[ "$doit" == "1" ]] ; then
			# don't report for nonpolymorphic classes
			for nonpoly in ${nonpoly_classes[@]} ; do
				if [[ "$nonpoly" == "$classname" ]] ; then
					doit="0"
					break
				fi
			done
			if [[ "$doit" == "1" ]] ; then
				echo "Problem: polymorphic base class's copy operator not virtual: $problem"	
			fi
		fi
	done < "$tmpfile"
}

function Check_EnfIfNoComment {
	egrep "#endif" -Rn "${@}" > "$tmpfile"
	
	while read problem; do
		echo "$problem" | grep "//" 1>/dev/null 2>/dev/null
		if [[ "$?" == "0" ]] ; then
			continue
		fi
		
		echo "$problem" | grep "/\*" 1>/dev/null 2>/dev/null
		if [[ "$?" == "0" ]] ; then
			continue
		fi
		
		echo "Problem: #endif without comment indicating what if(n)def if belongs to: ${problem[@]}"	

	done < "$tmpfile"
}

# ****
#
# Get Data From Files
#
# ****

function find_files {
	rm -f "$cppfiles"
	rm -f "$hfiles"
	rm -f "$allfiles"

	for item in "${@}" ; do
		find "$item" -iname '*.cpp' >> "$cppfiles"
		find "$item" -iname '*.h' >> "$hfiles"

		cat "$cppfiles" >> "$allfiles"
		cat "$hfiles" >> "$allfiles"
	done
}

function find_all_classes_in_files {
	rm -f "$classesAll"
	
	{
		egrep '^\s*(class|struct)\s+('"$CLASSNAME"')' `cat $allfiles` -Rh |
			egrep -v 'friend\s+class' |
			remove_all_comments |
			grep -v template |
			grep -v \, |
			grep -v \; |
			sed 's/\s*class\s\s*//g' |
			sed 's/\s*struct\s\s*//g'
	} > "$classesAll"
}

function store_classes {
	while read class ; do
		echo "$class" | grep \: 1>/dev/null 2>/dev/null
		ispoly=$?
		base=`echo "$class" | extract_base_classname_from_def`
		derived=`echo "$class" | extract_derived_classname_from_def`

		baseclasses=""
		if [[ "$ispoly" == "0" ]] ; then
			derived_classes[${#derived_classes[@]}]="$derived"
			if [[ "$base" != "" ]] ; then
				base_classes[${#base_classes[@]}]="$base"
			else
				echo "ERROR - could not determine what kind of class $derived is"
				exit
			fi
		else
			nonpoly_classes[${#nonpoly_classes[@]}]="$base"
		fi
	done < "$classesAll"
}

function sort_classes {
	# remove occurances of any class from nonpoly_classes that exists in base_classes
	for (( b=0; b < ${#base_classes[@]}; b++ )) ; do
		for (( n=0; n < ${#nonpoly_classes[@]}; n++ )) ; do
			if [[ "${nonpoly_classes[$n]}" == "${base_classes[$b]}" ]] ; then
				
				for (( x=$n; x<${#nonpoly_classes[@]-1}; x++ )) ; do
					nonpoly_classes[$x]="${nonpoly_classes[$x+1]}";
				done
				unset nonpoly_classes[${#nonpoly_classes[@]-1}]
				n=`expr $n-1`
				
				break
			fi
		done
	done

	# remove occurances of any class from nonpoly_classes that exists in derived_classes
	for (( b=0; b < ${#derived_classes[@]}; b++ )) ; do
		for (( n=0; n < ${#nonpoly_classes[@]}; n++ )) ; do
			if [[ "${nonpoly_classes[$n]}" == "${derived_classes[$b]}" ]] ; then
				
				for (( x=$n; x<${#nonpoly_classes[@]-1}; x++ )) ; do
					nonpoly_classes[$x]="${nonpoly_classes[$x+1]}";
				done
				unset nonpoly_classes[${#nonpoly_classes[@]-1}]
				n=`expr $n-1`
				
				break
			fi
		done
	done
		
	# sort the file
	cat "$classesAll" | sort -u > "$classesAll"2
	mv "$classesAll"2 "$classesAll"
}

# ****
#
# Main
#
# ****

function go_main {
	find_files "${@}"
	
	if [[ "`cat $allfiles`" == "" ]] ; then
		echo "No files found"
		exit
	fi
	
	find_all_classes_in_files
	store_classes
	sort_classes
	
	echo "Counted ${#base_classes[@]} base class definitions"
	echo "Counted ${#derived_classes[@]} derived class definitions"
	echo "Counted ${#nonpoly_classes[@]} non-polymorphic class definitions"
	echo ""

	echo "BASE CLASSES: ${base_classes[@]}"
	echo ""
	echo "DERIVED CLASSES: ${derived_classes[@]}"
	echo ""
	echo "NONPOLY CLASSES: ${nonpoly_classes[@]}"
	echo ""

	Check_StructorsWithoutVoid `cat $allfiles`
	#Check_DerivedCopyOpNotOverride `cat $allfiles`
	#Check_BaseCopyOpNotVirtual `cat $allfiles`
	Check_EnfIfNoComment `cat $allfiles`
}

go_main "${@}"
