// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_STRINGS_H_
#define SRC_CLIB_STRINGS_H_

#include <cstring>
#include <string>
#include <algorithm>
#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"

extern const std::string STR_WHITESPACE;

bool CharIsOneOf( const char c, const char* these );

template< typename TYPE >
using StringConversionFuncPtr = TYPE (*)(const std::string& str);

namespace String {
    std::vector<std::string> ListifyString( const char* str ); //!< puts each word or quoted sequence into a list as indivdual elements
    std::vector<std::string> ParseStringList( const std::string& str, const std::string& delimiter = EMPTY_STRING );
    std::string FromVector( const std::vector< std::string >& vec, const char delimiter=' ' );

    std::string Right( const std::string str, const std::size_t i ); //!< return a length of string from the right side
    std::string Left( const std::string str, const std::size_t i ); //!< return a length of string from the left side
    std::string Sub( const std::string str, const std::size_t from, const std::size_t to );//!< return string from one index to the next

    bool LeftIs( const std::string str, const std::string& value ); //!< return whether left-hand side of str matches value exactly
    bool RightIs( const std::string str, const std::string& value ); //!< return whether right-hand side of str matches value exactly

    std::string ToUpper( const std::string& str );
    std::string ToLower( const std::string& str );
    
    std::string Replace( const std::string& str, const std::string& replaceThis, const std::string& withThis );

    std::string VecToString( const Vec3f& value, const char seperator=',' );
    std::string VecToString( const Vec2f& value, const char seperator=',' );

    bool IsInt( const char* str );
    bool IsUInt( const char* str );
    bool IsUInt( const std::string& str );
    bool IsInt( const std::string& str );

    int ToInt( const char *str, const int base );
    int ToInt( const char *str );
    int ToInt( const std::string& str, const int base );
    int ToInt( const std::string& str );
    uint ToUInt( const char *str, const int base  );
    uint ToUInt( const char *str );
    uint ToUInt( const std::string& str );
    uint ToUInt( const std::string& str, const int base );
    float ToFloat( const char *str );
    float ToFloat( const std::string& str );
    double ToDouble( const char *str );
    double ToDouble( const std::string& str );
    bool ToBool( const char *str ); //!< our bools are either TRUE, FALSE, any capitalization of them, or 0 or 1
    bool ToBool( const std::string& str ); //!< our bools are either TRUE, FALSE, any capitalization of them, or 0 or 1
    
    template< typename TYPE, uint NUM_COMMAS >
    bool ToVec( const std::string& vecstr, TYPE& out );
    
    template< typename TYPE >
    TYPE To( const std::string& str );
    
    std::string TrimExtentionFromFileName( const std::string& str );
    std::string GetFileFromPath( const std::string& str );
    std::string GetDirFromPath( std::string path );
    std::string GetFilePath( const std::string& path );
    std::string RemoveBackDirs( const std::string& path );

    void TrimTrailingNum( std::string& str, const std::size_t count );
    void TrimLeadingNum( std::string& str, const std::size_t count );
    void TrimTrailing( std::string& str, const char* these = STR_WHITESPACE.c_str() );
    void TrimLeading( std::string& str, const char* these = STR_WHITESPACE.c_str() );
    void Trim( std::string& str, const char* these = STR_WHITESPACE.c_str() );
    
    void TruncateFloat( const std::string& str, uint digits, std::string& out );
    std::string TruncateFloat( const std::string& str, const uint digits );

    std::size_t Len( const char* cstr );

    int ICmp( const char* str, const char* str2 );
    int Cmp( const char* str, const char* str2 );
    
    template< typename TYPE, size_t SIZE >
    std::string ToString( const TYPE (&a)[SIZE] );

    template< typename TYPE >
    std::string ToString( const TYPE& x );

    template <class ...Args>
    std::vector<std::string> ToStringVector(const Args&... args);

    template< typename TYPE, size_t SIZE >
    std::string ToString( const TYPE (&x)[SIZE] );
}

template<>
inline std::string String::ToString( const char *const & x ) { return x; }

template<>
inline std::string String::ToString( char *const & x ) { return x; }

template<>
inline std::string String::ToString( char const & x ) { return std::string()+x; }

template<>
inline std::string String::ToString( const std::string& x ) { return x; }

template< typename TYPE >
std::string String::ToString( const TYPE& x ) { return std::to_string(x); }

template< typename TYPE, size_t SIZE >
std::string String::ToString( const TYPE (&x)[SIZE] ) { return x; }

template <class ...Args>
std::vector<std::string> String::ToStringVector(const Args&... args) {
    return { String::ToString(args)...};
}

template<>
inline int String::To< int >( const std::string& str ) {
    return String::ToInt( str );
}

template<>
inline uint String::To< uint >( const std::string& str ) {
    return String::ToUInt( str );
}

template<>
inline float String::To< float >( const std::string& str ) {
    return String::ToFloat( str );
}

template<>
inline bool String::To< bool >( const std::string& str ) {
    return String::ToBool( str );
}

template<>
inline double String::To< double >( const std::string& str ) {
    return String::ToDouble( str );
}

template<>
inline std::string String::To< std::string >( const std::string& str ) {
    return str;
}

template< typename TYPE, uint NUM_COMMAS >
bool String::ToVec( const std::string& vecstr, TYPE& out ) {
    uint commas=0;
    bool decimal=false;
    
    constexpr const uint max_digits = 20;
    
    if ( vecstr.size() > max_digits )
        return false; //invalid format, number is too long
        
    char tmp[max_digits];
    uint tmp_i=0;
        
    for ( uint i=0; i<vecstr.size(); ++i ) {
        switch ( vecstr[i] ) {
            case ',':
                decimal = false;
                if ( commas > NUM_COMMAS )
                    return false; // invalid format, only one comma in a vec2
                    
                tmp[tmp_i]='\0';
                out[commas++]=ToFloat(tmp);
                tmp_i=0;
                break;
            case '.':
                if ( decimal )
                    return false; // invalid format, only one decimal allowed in a number
                decimal = true;
                tmp[tmp_i++]='.';
                break;
            case '0': FALLTHROUGH;
            case '1': FALLTHROUGH;
            case '2': FALLTHROUGH;
            case '3': FALLTHROUGH;
            case '4': FALLTHROUGH;
            case '5': FALLTHROUGH;
            case '6': FALLTHROUGH;
            case '7': FALLTHROUGH;
            case '8': FALLTHROUGH;
            case '9':
                tmp[tmp_i++]=vecstr[i];
                break;
            default:
                return false; // invalid format
        }
    }
    
    if ( commas != NUM_COMMAS )
        return false;

    tmp[tmp_i]='\0';
    out[NUM_COMMAS] = ToFloat(tmp);
    
    return true;
}
#endif  // SRC_CLIB_STRINGS

//
//template< typename TYPE >
//std::string String::ToString( const TYPE& t );
