// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_H_
#define SRC_CLIB_MATH_H_

#include <cmath>

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"
#include "./math/values.h"

extern const float EP;
extern const float EP_TEN_THOUSANDTH;
extern const float EP_THOUSANDTH;
extern const float EP_HUNDREDTH;
extern const float EP_HUNDREDTH_CENTIMETER;
extern const float EP_TENTH_CENTIMETER;
extern const float EP_HALF_CENTIMETER;
extern const float EP_ONE_CENTIMETER;
extern const float EP_THREE_CENTIMETERS;
extern const float EP_MIL;
extern const double PI_D;
extern const float PI_F;
extern const double PI_OVER_180_D;
extern const float PI_OVER_180_F;
extern const float PI_OVER_360_F;
extern const float HALF_PI_OVER_180_F;
extern const double ONE_EIGHTY_OVER_PI_D;
extern const float ONE_EIGHTY_OVER_PI_F;
extern const float RAD_TO_DEG_F;
extern const float DEG_TO_RAD_F;

template< typename TYPE >
constexpr inline TYPE Squared( const TYPE val ) {
    return val * val;
}

template< typename TYPE >
inline TYPE Round( const TYPE val );

template< typename TYPE >
inline TYPE RoundToNearestHalf( const TYPE val );

template< typename TYPE >
inline TYPE RoundToNearestTenth( const TYPE val );

template< typename TYPE >
inline TYPE Round( const TYPE val ) {
    return floorf( val + 0.5f );
}

template< typename TYPE >
inline TYPE RoundToNearestHalf( const TYPE val ) {
    return floorf(( val * 2 ) + 0.5f ) / 2;
}

template< typename TYPE >
inline TYPE RoundToNearestTenth( const TYPE val ) {
    return roundf( val * 10 ) / 10;
}

namespace Maths {
    typedef Uint8 QuadraticReturnT_BaseType;
    enum class QuadraticReturnT : QuadraticReturnT_BaseType {
        ONE_SIMPLE = 0 //!< one simple solution
        , TWO_SIMPLE = 1 //!< two simple solutions
        , TWO_COMPLEX = 2 //!< one simple solution and one complext solution //todo: correct?
        , INVALID = 3
    };
}

namespace Maths {
    QuadraticReturnT SolveQuadratic( const float a, const float b, const float c, float& solution_one, float* solution_two, const bool calculate_complex = false );
    bool GetLowestRoot( const float a, const float b, const float c, float& lowestRoot ); //!< gets lowest root of a quadratic formula. returns false if there are no non-imaginary roots
    float NormalizeAngle( const float angle, const float minmax = 360 );
    Vec3f Fabsf( const Vec3f& vec );

    template< typename NUMERATOR, typename DENOMINATOR >
    NUMERATOR Divide( const NUMERATOR numerator, const DENOMINATOR denominator );

    template< typename TYPE >
    TYPE Clamp( const TYPE& value, const TYPE& minimum, const TYPE& maximum );

    #ifdef CLIB_UNIT_TEST
        void UnitTest( void );
        void UnitTest_SolveQuadratic( void );
    #endif // CLIB_UNIT_TEST
}

template< typename NUMERATOR, typename DENOMINATOR >
NUMERATOR Maths::Divide( const NUMERATOR numerator, const DENOMINATOR denominator ) {
    if ( Maths::Approxf( numerator, 0 ) || Maths::Approxf( denominator, 0 ) )
        return 0;

    return numerator / denominator;
}

/*
dividend divided by divisor equals quotient
minuend minus subtrahend equals difference
addend plus addend equals sum
multiplicand (or coefficient) times multiplier equals product
*/

/*
// slower than standard library
inline int Math::IntAbs( const int myint ) {
    __asm__ __volatile__
    (
    "mov 8(%ebp),%eax\n\t" // put myint in return value
    "cmp $0,%eax\n\t"
    "jge abs_done\n\t" // if ret >= than zero, return!
    "xor %eax,%eax\n\t" // ret = 0
    "sub 8(%ebp),%eax\n\t" // ret -= myint
    "abs_done:\n\t"
    );
}
*/

//todo: do and test
/* AnglesToVec {
    vec.x = -sin(angle.y*PI_OVER_180)*cos(angle.x*PI_OVER_180);
    vec.y = sin(angle.x*PI_OVER_180);
    vec.z = cos(angle.y*PI_OVER_180)*cos(angle.x*PI_OVER_180);
}
*/

template< typename TYPE >
TYPE Maths::Clamp( const TYPE& value, const TYPE& minimum, const TYPE& maximum ) {
    ASSERT( maximum > minimum );

    if ( value < minimum )
        return minimum;

    if ( value > maximum )
        return maximum;

    return value;
}

#include "./math/values.h"
#include "./math/vec2t.h"
#include "./math/vec3t.h"
#include "./math/mat.h"
#include "./math/quat.h"
#include "./math/dualquat.h"
#include "./math/circle.h"
#include "./math/aasquare.h"

#endif  //SRC_CLIB_MATH_H_
