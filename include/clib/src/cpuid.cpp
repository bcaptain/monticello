// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./warnings.h"
#include "./cpuid.h"
#include "./strings.h"

void CPUID_Helper::exec_func(unsigned i) {
    asm volatile (
        "cpuid"

        // OUTPUT
        : "=a" (registers[RegisterT::EAX])
        , "=b" (registers[RegisterT::EBX])
        , "=c" (registers[RegisterT::ECX])
        , "=d" (registers[RegisterT::EDX])

        // INPUT
        : "a" (i)
        , "c" (0)
    );
}

CPUID_Helper::CPUID_Helper( void )
    : registers()
    , vendor()
    , stepping(0)
    , model(0)
    , family(0)
    , type(0)
    , features_ebx(0)
    , features_ecx(0)
    , features_edx(0)
    , signature(0)
{ }

void CPUID_Helper::Go( void ) {
    LoadVendor(); // have to get vendor first so we know how to interpret the rest of the data
    LoadSignature();
}

void CPUID_Helper::LoadVendor( void ) {
    exec_func( CPUID_OPTION_VENDOR_ID );

    strncpy( &vendor[0], reinterpret_cast< const char* >( &registers[RegisterT::EBX] ), 4 );
    strncpy( &vendor[4], reinterpret_cast< const char* >( &registers[RegisterT::EDX] ), 4 );
    strncpy( &vendor[8], reinterpret_cast< const char* >( &registers[RegisterT::ECX] ), 4 );
    vendor[12] = '\0';
}

void CPUID_Helper::LoadSignature( void ) {
    exec_func( CPUID_OPTION_FEATURE_BITS );

    stepping = ( registers[RegisterT::EAX] & CPUID_STEPPING_MASK ) >> CPUID_STEPPING_SHIFT;
    model = ( registers[RegisterT::EAX] & CPUID_MODEL_MASK ) >> CPUID_MODEL_SHIFT;
    family = ( registers[RegisterT::EAX] & CPUID_FAMILY_MASK ) >> CPUID_FAMILY_SHIFT;
    type = ( registers[RegisterT::EAX] & CPUID_PROCESSOR_TYPE_MASK ) >> CPUID_PROCESSOR_TYPE_SHIFT;

    uint ext_model = ( registers[RegisterT::EAX] & CPUID_EXTENDED_MODEL_MASK ) >> CPUID_EXTENDED_MODEL_SHIFT;
    uint ext_family = ( registers[RegisterT::EAX] & CPUID_EXTENDED_FAMILY_MASK ) >> CPUID_EXTENDED_FAMILY_SHIFT;

    /*  Intel signatures can be translated to processor names here:
        http://software.intel.com/en-us/articles/intel-architecture-and-processor-identification-with-cpuid-model-and-family-numbers */
    signature = registers[RegisterT::EAX];

    //if ( String::Cmp( vendor, "GenuineIntel" ) == 0 ) {
        // This if-block is correct for intel, but as far as I can figure AMD calculates it the same way.

        /*  Intel CPUID: http://download.intel.com/design/processor/applnots/24161832.pdf
            Page 229, table 3-19, and pseudo code that follows there */

        // AMD CPUID: http://support.amd.com/us/Embedded_TechDocs/25481.pdf

        if ( family == 0xF )
            family += ext_family;

        if ( family == 0x6 || family == 0xF )
            model += ext_model << 4; // ext_model value is the left half of the byte, model is the right half
    //}

    // ** we can parse this later if we have to, but for now it will suffice just to spit out the values of the registers. They're all described in the PDFs
    features_ebx = registers[RegisterT::EBX];
    features_ecx = registers[RegisterT::ECX];
    features_edx = registers[RegisterT::EDX];
}
