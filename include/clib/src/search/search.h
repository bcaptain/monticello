// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ARGS_H_
#define SRC_CLIB_ARGS_H_

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

#endif // SRC_CLIB_ARGS_H_
