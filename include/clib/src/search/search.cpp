// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"

/**************
 *
 * Linear Search
 *
 * **********/
/* C#
private void btnLinearSearch_Click(object sender, EventArgs e)
{
    if (mytype == typeof(int))
    {
        int i = 0;
        int.TryParse(txtSearchParameters.Text, out i);
        LinearSearch<int>(data, i);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        LinearSearch<std::string>(data, txtSearchParameters.Text);
        DisplayData<std::string>();
    }
}

private bool LinearSearch<TYPE>(ArrayList myDataStructure, TYPE val) where TYPE : IComparable<TYPE>
{
    int i;
    for (i = 0; i < myDataStructure.Count; ++i )
    {
        if ( ( (TYPE) myDataStructure[i] ).CompareTo( (TYPE) val ) == 0)
            break;
    }

    // if the loop made it completely through the list, it didnt find it.
    if ( i == myDataStructure.Count ) {
        MessageBox.Show("Could not find \"" + val + "\"!");
        return false;
    }

    MessageBox.Show("Found \"" + val + "\" at index " + i);
    return true;
}
*/
/**************
 *
 * Recursive Linear Search
 *
 * **********/
/* C#
private void btnRevursiveLinearSearch_Click(object sender, EventArgs e)
{
    int i = -1;
    if (mytype == typeof(int) )
    {
        i = 0;
        int.TryParse(txtSearchParameters.Text, out i);
        i = RecursiveLinearSearch<int>(data, i);
    }
    else if (mytype == typeof(std::string))
    {
        i = RecursiveLinearSearch<std::string>(data, txtSearchParameters.Text);
    }

    if (i == -1)
    {
        MessageBox.Show("Could not find \"" + txtSearchParameters.Text + "\"!");
        return;
    }

    MessageBox.Show("Found \"" + txtSearchParameters.Text + "\" at index " + i);
}

private int RecursiveLinearSearch<TYPE>(ArrayList myDataStructure, TYPE val, int i = 0) where TYPE : IComparable<TYPE>
{
    if (i >= myDataStructure.Count)
        return -1;

    if (((TYPE)myDataStructure[i]).CompareTo( val ) == 0 )
        return i;

    ++i
    return RecursiveLinearSearch( myDataStructure, val, i );
}
*/
/************
 *
 * Binary Search
 *
 * **********/
/* C#
private void btnBinarySarch_Click(object sender, EventArgs e)
{
    DoBubbleSort();

    if (mytype == typeof(int))
    {
        int i = 0;
        int.TryParse(txtSearchParameters.Text, out i);
        BinarySearch<int>(data, i);
    }
    else if (mytype == typeof(std::string))
    {
        BinarySearch<std::string>(data, txtSearchParameters.Text);
        DisplayData<std::string>();
    }
}

private bool BinarySearch<TYPE>(ArrayList myDataStructure, TYPE val) where TYPE : IComparable<TYPE>
{
    int bottom = 0;
    int top = myDataStructure.Count - 1;
    int middle = top / 2;

    for (int i = 0; i < myDataStructure.Count - 1; ++i )
    {
        if (middle < 0)
            break;

        if (((TYPE)myDataStructure[middle]).CompareTo(val) == 0)
        {
            MessageBox.Show("Found \"" + val + "\" at index " + middle);
            return true;
        }

        if (val.CompareTo((TYPE)myDataStructure[middle]) < 0)
        {
            top = middle;
        }
        else
        {
            bottom = middle;
        }

        if (bottom + 1 == top && middle == top)
        {
            middle = bottom;
        }
        else
        {
            middle = top - ((top - bottom) / 2);
        }
    }

    MessageBox.Show("Could not find \"" + val + "\"!");
    return false;
}
*/
/************
 *
 * Recursive Binary Search
 *
 * **********/
/* C#
private void btnRecursiveBinarySearch_Click(object sender, EventArgs e)
{
    DoBubbleSort();

    int i=-1;
    if (mytype == typeof(int))
    {
        i = 0;
        int.TryParse(txtSearchParameters.Text, out i);
        i = RecursiveBinarySearch(data, i);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        i = RecursiveBinarySearch<std::string>(data, txtSearchParameters.Text);
        DisplayData<std::string>();
    }

    if (i == -1)
        MessageBox.Show("Could not find \"" + txtSearchParameters.Text + "\"!");
    else
        MessageBox.Show("Found \"" + txtSearchParameters.Text + "\" at index " + i);
}

private int RecursiveBinarySearch<TYPE>(ArrayList myDataStructure, TYPE val, int bottom = 0, int top = -1, int middle = -1, int i=0) where TYPE : IComparable<TYPE>
{
    if (i >= myDataStructure.Count - 1)
        return -1;

    if ( top == -1 )
        top = myDataStructure.Count - 1;

    if ( middle == -1 )
        middle = top / 2;

    if (middle < 0)
        return -1;

    if (((TYPE)myDataStructure[middle] ).CompareTo( val ) == 0 )
        return middle;

    if (val.CompareTo((TYPE)myDataStructure[middle]) < 0)
        top = middle;
    else
        bottom = middle;

    if (bottom + 1 == top && middle == top)
        middle = bottom;
    else
        middle = top - ((top - bottom) / 2);

    ++i
    return RecursiveBinarySearch( myDataStructure, val, bottom, top, middle, i);
}
*/
