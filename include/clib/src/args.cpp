// Copyright 2010-2019 Brandon Captain. You may not copy this work.

// todo: this class needs a look-over

#include "./warnings.h"
#include "./args.h"
#include "./strings.h"

int Arg::arg_num=0;
char **Arg::args;

bool Arg::findarg( const char *arg, bool remove ) {
    for (--arg_num; arg_num >= 0; --arg_num )
        if ( !String::Cmp(arg, args[arg_num]) )
            if ( remove )
                return true;

    return false;
}

int Arg::getarg_int( const char *arg ) {
    int tot=arg_num;

    for (--arg_num; arg_num >= 0; --arg_num ) {
        if ( !String::Cmp(arg, args[arg_num]) ) {
            ++arg_num;
            if ( arg_num < tot )
                return atoi(args[arg_num]);
        }
    }

    return 0;
}

char* Arg::getarg_str( const char *arg ) {
    int tot=arg_num;

    for (--arg_num; arg_num >= 0; --arg_num ) {
        if ( !String::Cmp(arg, args[arg_num]) ) {
            args[arg_num][0]='\0';
            ++arg_num;

            if ( arg_num < tot )
                return args[arg_num];

            return nullptr;
        }
    }

    return nullptr;
}
