// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CPUID_H
#define SRC_CPUID_H

#include "./warnings.h"
#include "./macros.h"
#include "./std.h"
#include "./strings.h"
#include "./types.h"

class CPUID_Helper {
    // http://forum.osdev.org/viewtopic.php?f=1&t=23445
    // http://msdn.microsoft.com/en-us/library/hskdteyh%28v=vs.80%29.aspx

    // ** the contents of EAX after calling for Processor Info / Feature Bits
    typedef Uint32 CPUID_FeatureBitShiftsT_BaseType;
    enum CPUID_FeatureBitShiftsT : CPUID_FeatureBitShiftsT_BaseType {
        CPUID_STEPPING_SHIFT = 0, // bits 3:0
        CPUID_MODEL_SHIFT = 4, // bits 7:4
        CPUID_FAMILY_SHIFT = 8, // bits 11:8
        CPUID_PROCESSOR_TYPE_SHIFT = 12, // bits 13:12
        CPUID_EXTENDED_MODEL_SHIFT = 16, // bits 19:16
        CPUID_EXTENDED_FAMILY_SHIFT = 20, // bits 27:20
    };

    // ** the contents of EAX after calling for Processor Info / Feature Bits
    typedef Uint32 CPUID_FeatureBitMasksT_BaseType;
    enum CPUID_FeatureBitMasksT : CPUID_FeatureBitMasksT_BaseType {
        CPUID_STEPPING_MASK = 15 << CPUID_STEPPING_SHIFT, // bits 3:0
        CPUID_MODEL_MASK = 15 << CPUID_MODEL_SHIFT, // bits 7:4
        CPUID_FAMILY_MASK = 15 << CPUID_FAMILY_SHIFT, // bits 11:8
        CPUID_PROCESSOR_TYPE_MASK = 3 << CPUID_PROCESSOR_TYPE_SHIFT, // bits 13:12
        CPUID_EXTENDED_MODEL_MASK = 15 << CPUID_EXTENDED_MODEL_SHIFT, // bits 19:16
        CPUID_EXTENDED_FAMILY_MASK = 255 << CPUID_EXTENDED_FAMILY_SHIFT // bits 27:20
    };

    typedef std::size_t CPUID_OptionT_BaseType;
    enum CPUID_OptionT : CPUID_OptionT_BaseType {
        CPUID_OPTION_VENDOR_ID = 0,
        CPUID_OPTION_FEATURE_BITS = 1,
        CPUID_OPTION_CACHE_AND_TLB_INFO = 2,
        CPUID_OPTION_PROCESSOR_SERIAL_NUMBER = 3
    };

    typedef Uint8 CPUID_CPU_BrandT_BaseType;
    enum CPUID_CPU_BrandT : CPUID_CPU_BrandT_BaseType {
        UNKNOWN  = 0,
        AMD = 1,
        INTEL = 2
    };

    typedef std::size_t RegisterT_BaseType;
    enum RegisterT : RegisterT_BaseType {
        EAX = 0,
        EBX = 1,
        ECX = 2,
        EDX = 3
    };

public:
    CPUID_Helper( void );

public:
    void Go( void );

private:
    void LoadVendor( void );
    void LoadSignature( void );

    void exec_func( unsigned i );

private:
    uint32_t registers[4];

public:
    char vendor[13]; // CPU IDs are 12 bytes
    uint stepping;
    uint model;
    uint family;
    uint type;
    uint features_ebx;
    uint features_ecx;
    uint features_edx;
    uint signature;
};

#endif  // SRC_CPUID_H
