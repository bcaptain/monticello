// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_HASH_H_
#define SRC_CLIB_HASH_H_

#include <cstdlib>
#include <cassert>
#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

class Hashes {
public:
    static uint OAT ( const char *key, const uint size=1009 ); //!< generate hash index for key
    static uint OAT_C( const char *key, const uint size=1000 );
    static uint Sum( const char *key, const uint size=1000 );

    #ifdef CLIB_UNIT_TEST
        static void UnitTest( void );
    #endif //CLIB_UNIT_TEST

private:
    static constexpr uint oat_part_one( const std::size_t& h, const char c ) {
        return ( h + static_cast<unsigned int>( c ) );
    }

    static constexpr uint oat_part_two( const std::size_t& h ) {
        return ( h << 10 );
    }

    static constexpr uint oat_part_three( const std::size_t& h ) {
        return ( h >> 6 );
    }

    static constexpr uint oat_part_four( const std::size_t& h ) {
        return ( h << 3 );
    }

    static constexpr uint oat_part_five( const std::size_t& h ) {
        return ( h >> 11 );
    }

    static constexpr uint oat_part_six( const std::size_t& h ) {
        return ( h << 15 );
    }

    static constexpr std::size_t string_length( const char* str, std::size_t index = 0 ) {
        return ( str == nullptr || str[index] == '\0' ) ? 0 : 1 + string_length( str, index+1 );
    }

public:
    static constexpr uint OAT_CONSTEXPR( const char* str, const std::size_t size=1009, const std::size_t idx=0, const std::size_t h=0 ) {
        return (
            ( idx == string_length( str ) ) ? (
                (
                    (
                        ( h + oat_part_four( h ) ) ^
                        oat_part_five( h + oat_part_four( h ) )
                    )
                    +
                    oat_part_six(
                        ( h + oat_part_four( h ) ) ^
                        oat_part_five( h + oat_part_four( h ) )
                    )
                )

                 % size
            ) : (

                OAT_CONSTEXPR( str, size, idx+1,
                    (
                        oat_part_one( h, str[idx] ) +
                        oat_part_two( h + static_cast< unsigned int>( str[idx] ) )
                    )
                    ^
                    oat_part_three( oat_part_one( h, str[idx] ) +
                                oat_part_two( oat_part_one( h, str[idx] ) )
                    )
                )

            )
        );
    }
};


using Hash = std::pair< const char*, std::size_t >;

#endif  //SRC_CLIB_HASH
