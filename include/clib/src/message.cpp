// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <cstdarg>
#include <cstring>
#include <iostream>
#include <iomanip>
#include "./warnings.h"
#include "./message.h"
#include "./strings.h"
#include "./math/vec2t-dec.h"
#include "./math/vec3t-dec.h"
#include "./math/vec3t-dec.h"
#include "./math/vec3t-dec.h"
#include "./math/mat.h"
#include "./math/quat.h"

void (*error_hook)( const char* str ) = nullptr;
void (*warning_hook)( const char* str ) = nullptr;
void (*message_hook)( const char* str ) = nullptr;
void (*fatalError_hook)( const char* str ) = nullptr;

uint AppendFormattedValue( std::string& msg, const uint fmt_index, const std::string fmt, const std::vector< std::string >& vec, uint& v ) {
    if ( v >= vec.size() )
        return 0;

    const uint f = fmt_index;
    switch ( fmt[fmt_index] ) {
        case '#': // print multiple formatted values
            if ( f+3 < fmt.size() ) {
                char * ptr;
                uint times = strtoul( &fmt[f+2], &ptr, 10 );
                uint ret = 2; // skip the separator and format identifier
                ret += ptr - &fmt[f+2]; // skip the length of the number of times we want to skip
                ret += AppendFormattedValue(msg,f+3,fmt,vec,v); // we only skip the subformat once, since it's only listed once (and it will be the same amount for every item)
                ++v;
                while ( times > 0 ) {
                    msg += fmt[f+1]; // append the separator
                    AppendFormattedValue(msg,f+3,fmt,vec,v); // we only skip the subformat once, since it's only listed once (and it will be the same amount for every item)
                    ++v;
                    --times;
                }
                return ret;
            }
            return 0;
        case '?': // "auto"
            FALLTHROUGH;
        case 's': // String of characters
            msg += vec[v];
            return 0;
        case '%': //A % followed by another % character will write a single % to the stream.
            msg += '%';
            return 0;
        case 'b': //bool
            msg += vec[v][0] == '0' ? "False" : "True ";
            return 0;
        case 'q':
            msg += String::ToString( String::ToQuat( vec[v] ) );
            return 0;
        case 'v':
            msg += String::ToString( String::ToVec2f( vec[v] ) );
            return 0;
        case 'V':
            msg += String::ToString( String::ToVec2f( vec[v] ) );
            return 0;
        // case 'm': // todo?
        // case 'M': // todo?
        case 'd':
            FALLTHROUGH;
        case 'i':
            msg += String::ToString( String::ToInt( vec[v] ) );
            return 0;
        case 'c': // Character
            msg += vec[v][0];
            return 0;
        case 'f': { // float
            if ( f < fmt.size() + 2 ) {
                if ( fmt[f+1] == '.' ) {
                    const char places[2]{fmt[f+2],'\0'};
                    const float digits = String::ToFloat( places );
                    const std::string number = String::TruncateFloat( vec[v], digits );
                    msg += String::ToString( number );
                    return 2;
                }
            }
            msg += String::ToString( String::ToFloat( vec[v] ) );
            return 0;
        }
        case 'u': // Unsigned decimal integer
            msg += String::ToString( String::ToUInt( vec[v] ) );
            return 0;
        case 'p': // Pointer address
            FALLTHROUGH; // https://stackoverflow.com/questions/2369541/where-is-p-useful-with-printf#2369593
        case 'X': // Unsigned hexadecimal integer (uppercase)
            FALLTHROUGH;
        case 'x': // Unsigned hexadecimal integer
            {
                std::stringstream stream;
                stream << std::hex << String::ToUInt(vec[v].c_str(), 10);
                std::string pp;
                stream >> pp;
                if ( fmt[f] == 'X' )
                    pp = String::ToUpper(pp);
                std::string ret;
                if ( fmt[f] == 'p' )
                    while ( (pp.size() + ret.size()) % 8 )
                        ret += '0';
                msg += ret + pp;
                return 0;
            }
        default:
            std::cerr << "Unknown switch \'" << fmt[f] << "\' in message format: \"" << fmt << "\" %s\n";
            return 0;
    }
    
    // todo: case 'u' // double
    // todo: case 'o' // Unsigned octal
    // todo: case 'F'
    // todo: case 'e' // Scientific notation (mantissa/exponent), lowercase
    // todo: case 'E' // Scientific notation (mantissa/exponent), uppercase
    // todo: case 'g' // Use the shortest representation: %e or %f
    // todo: case 'G' // Use the shortest representation: %E or %F
    // todo: case 'a' // Hexadecimal floating point, lowercase
    // todo: case 'A' // Hexadecimal floating point, uppercase 
}

/*
Examples:
    Printf("%f.4",9870.123456789); // prints truncated float: "9870.1234"
    Printf("%#,3u",1,2,3); // prints three comma-separated unsigned integers: "1,2,3"
    Printf("%# 3f.2\n",1.12345f,2.876f,3.4f); // prints three space-separated truncated floats: "1.12 2.87 3.40"
*/
std::string Printfv( const std::string& fmt, const std::vector< std::string >& vec ) {
    // ** pre-allocate a sane size
    uint capacity = fmt.size();
    uint switches=0;
    for ( uint i=0; i<vec.size(); ++i ) {
        capacity += vec[i].size();
        ++switches;
    }

    std::string msg;
    msg.reserve(capacity);
    
    uint a=0;
    
    for ( uint i=0; i<fmt.size(); ++i ) {
        if ( (fmt[i]=='%') && (a<vec.size()) ) {
            if ( ++i >= fmt.size() )
                break; // string ended with a '%'
                
            if ( vec[a].size() < 1 )
                continue;

            if ( fmt[i] == '\\' ) {
                if ( ++i >= fmt.size() )
                    break;
                msg += fmt[i];
                continue;
            }

//  todo: todo:parray                
//            if ( fmt[i] == '[' ) {
//                for (++i; i<fmt.size(); ++i ) {
//                    if ( fmt[i] ==']' )
//                        break;
//                    std::string arr += fmt[i];
//                }
//                const uint x=String::ToUInt(arr);
//                for ( uint b=0; b<x && ++i<fmt.size(); ++b, )
//                    AppendFormattedValue( msg, fmt[i], vec, a );
//            }
                
            i += AppendFormattedValue( msg, i, fmt, vec, a );
            ++a;
        } else {
            msg+=fmt[i];
        }
    }
    
    return msg;
}

void ClearLog( void ) {
    auto file = fopen("log.txt", "w");
    if ( file )
        fclose( file );
}
