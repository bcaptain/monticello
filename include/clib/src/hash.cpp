// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./warnings.h"
#include "./hash.h"
#include "./math/values.h"

// One-At-a-Time hash.
// algorythm inspired by http://www.eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx
// allegedly a 'simpler version' of the Bob Jenkins hash.

// because this function sets the return value via assembly, we turn this warning off temporarily

#ifdef CLIB_UNIT_TEST
    void Hashes::UnitTest( void ) {
        const char* test1="test string one";
        const char* test2="test string 2 12345&*^4f;'";
        const char* test3="t3";

        const uint size1 = 1009;
        const uint size2 = 127;

        std::size_t a,b,c;

        a = OAT( test1, size1 );
        b = OAT_C( test1, size1 );
        c = OAT_CONSTEXPR( test1, size1 );
        ASSERT( a == b && b == c );
        a = OAT( test2, size1 );
        b = OAT_C( test2, size1 );
        c = OAT_CONSTEXPR( test2, size1 );
        ASSERT( a == b && b == c );
        a = OAT( test3, size1 );
        b = OAT_C( test3, size1 );
        c = OAT_CONSTEXPR( test3, size1 );
        ASSERT( a == b && b == c );
        a = OAT( test1, size2 );
        b = OAT_C( test1, size2 );
        c = OAT_CONSTEXPR( test1, size2 );
        ASSERT( a == b && b == c );
        a = OAT( test2, size2 );
        b = OAT_C( test2, size2 );
        c = OAT_CONSTEXPR( test2, size2 );
        ASSERT( a == b && b == c );
        a = OAT( test3, size2 );
        b = OAT_C( test3, size2 );
        c = OAT_CONSTEXPR( test3, size2 );
        ASSERT( a == b && b== c );
    }

    uint Hashes::OAT_C( const char *key, const uint size ) {
        // this is original c code used for assembly in Hashes::OAT()

        ASSERT( size != 0 && key != nullptr && Maths::IsPrime(size) );

        unsigned int h = 0;

        for ( std::size_t i = 0; key[i] != '\0'; ++i ) {
            h += static_cast< unsigned int >( key[i] );
            h += ( h << 10 );
            h ^= ( h >> 6 );
        }

        h += ( h << 3 );
        h ^= ( h >> 11 );
        h += ( h << 15 );

        return h % size;
    }

    uint Hashes::Sum( const char *key, const uint size ) {
        // this is original c code used for assembly in Hashes::OAT()

        if ( size == 0 || key == nullptr || key[0] == '\0' ) {
            ASSERT( false );
            return 0;
        }

        unsigned int h = 0;

        for ( uint i=0; key[i] != '\0'; ++i ) {
            h += static_cast< unsigned int >( key[i] );
        }

        return h % size;
    }

#endif // CLIB_UNIT_TEST

uint Hashes::OAT( const char *key, const uint size ) {
    ASSERT( size > 0 && key != nullptr && key[0] != '\0' && Maths::IsPrime(size) );

    const std::size_t len = String::Len( key );
    Uint32 ret;

    asm volatile (
        "xor %%eax,%%eax \n\t" // h = 0

        "1: \n\t"

            // h += key[i];
            "xor %%edx,%%edx \n\t" // edx = 0
            "addb (%%ecx),%%dl \n\t" // edx += key[0]
            "add %%edx,%%eax \n\t" // h += edx

            "inc %%ecx \n\t" // ++key

            // h += ( h << 10 );
            "mov %%eax,%%edx \n\t" // edx = h
            "shl $10,%%edx \n\t" // edx <<= 10
            "add %%edx,%%eax \n\t" // h += edx

            // h ^= ( h >> 6 );
            "mov %%eax,%%edx \n\t" // edx = h
            "shr $6,%%edx \n\t" // edx >>= 6;
            "xor %%edx,%%eax \n\t" // h ^= edx

            // if ( ecx != '\0' ) goto 1b;
            "cmpb $0,(%%ecx) \n\t"
            "jne 1b \n\t"

        // h += ( h << 3 );
        "mov %%eax,%%edx \n\t" // edx = h
        "shl $3,%%edx \n\t" // edx <<= 3
        "add %%edx,%%eax \n\t" // h += edx

        // h ^= ( h >> 11 );
        "mov %%eax,%%edx \n\t" // edx = h
        "shr $11,%%edx \n\t" // edx >>= 11
        "xor %%edx,%%eax \n\t" // h ^= edx

        // h += ( h << 15 );
        "mov %%eax,%%edx \n\t" // edx = h
        "shl $15,%%edx \n\t" // edx <<= 15
        "add %%edx,%%eax \n\t" // h += edx

        // OUTPUT
        : "=a" ( ret ) // eax = the hash

        // INPUT
        : "c" ( key ) // ecx = key (the string)
        , "b" ( len ) // ebx = len (length of string)

        // CLOBBERS
        : "edx" // a register used for temp variables
    );

    return ret % size;
}
