// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <cassert>
#include "./warnings.h"
#include "./strings.h"
#include "./message.h"
#include "./assert.h"
#include "./math/vec2t-dec.h"
#include "./math/vec3t-dec.h"

const char CSTR_WHITESPACE[3]{' ','\t','\0'};
const std::string STR_WHITESPACE( CSTR_WHITESPACE );

bool CharIsOneOf( const char c, const char* these ) {
    ASSERT( these );
    int i=0;
    while ( these[i] != '\0' ) {
        if ( c == these[i] ) {
            return true;
        }
        ++i;
    }
    return false;
}

std::string String::ToUpper( const std::string& str ) {
    std::string ret;
    ret.resize( str.size() );
    std::transform( std::begin(str), std::end(str), std::begin(ret), std::ptr_fun<int, int>( std::toupper ) );
    return ret;
}

std::string String::ToLower( const std::string& str ) {
    std::string ret;
    ret.resize( str.size() );
    std::transform( std::begin(str), std::end(str), std::begin(ret), std::ptr_fun<int, int>( std::tolower ) );
    return ret;
}

std::string String::Replace( const std::string& str, const std::string& replaceThis, const std::string& withThis ) {
    std::string ret;
    ret.reserve(str.size() + ( withThis.size() ));
    for ( uint i=0; i<str.size(); ++i ) {
        if ( i<str.size()-replaceThis.size() ) {
            if ( str.substr(i,replaceThis.size()) == replaceThis ) {
                ret += withThis;
                i += replaceThis.size();
                --i;
                continue;
            }
        }
        ret += str[i];
    }
    return ret;
}

bool String::LeftIs( const std::string str, const std::string& value ) {
    return Left(str, value.size()) == value;
}

bool String::RightIs( const std::string str, const std::string& value ) {
    return Right(str, value.size()) == value;
}

// return a length of string from the right side
std::string String::Right( const std::string str, const std::size_t i ) {
    if ( i > str.size() )
        return str;
    return str.substr(str.size()-i, str.size());
}

// return a length of string from the left side
std::string String::Left( const std::string str, const std::size_t i ) {
    return str.substr(0, i);
}

//! return string from one index to the next
std::string String::Sub( const std::string str, const std::size_t from, const std::size_t to ) {
    if ( from > to || to > str.size() )
        return std::string();

    return str.substr(from, to-from+1);
}

std::string String::VecToString( const Vec3f& value, const char seperator ) {
    return std::to_string( value.x ) + seperator +
        std::to_string( value.y ) + seperator +
        std::to_string( value.z );
}

std::string String::VecToString( const Vec2f& value, const char seperator ) {
     return std::to_string( value.x ) + seperator +
        std::to_string( value.y );
}

float String::ToFloat( const char *str ) {
    return strtof( str, nullptr );
}

double String::ToDouble( const char *str ) {
    return strtod( str, nullptr );
}

int String::ToInt( const char *str, const int base ) {
    return strtol( str, nullptr, base );
}

uint String::ToUInt( const char *str, const int base ) {
    return strtoul( str, nullptr, base );
}

std::string String::TrimExtentionFromFileName( const std::string& str ) {
    char c;

     // a period at beginning of the file name is not an extention, we stop checking at index 0
    std::size_t i=str.size();
    while ( i > 0 ) {
        --i;

        c = str[i];
        if ( c == '/')
            return str;

        if ( c == '\\')
            return str;

        if ( c == '.') {
            // a period at beginning of the file name is not an extention
            if ( i == 0 )
                return str;

            c = str[i-1];

            if ( c == '/')
                return str;

            if ( c == '\\')
                return str;

            return str.substr( 0, i );
        }
    }
    return str;
}

std::size_t String::Len( const char* cstr ) {
    return cstr ? std::strlen(cstr) : 0;
}

// our bools are either TRUE, FALSE, any capitalization of them, or 0 or 1
bool String::ToBool( const char *str ) {
    switch ( Len( str ) ) {
        case 1:
            return str[0] != '0';
        case 4:
            if ( str[0] == 't' || str[1] == 'T' )
                if ( str[1] == 'r' || str[2] == 'R' )
                    if ( str[2] == 'u' || str[3] == 'U' )
                        if ( str[3] == 'e' || str[4] == 'E' )
                            return true;
            FALLTHROUGH;
        default:
            return false;
    }
}

std::string String::GetDirFromPath( std::string path ) {
    for ( unsigned int i=path.size(); i>0;  ) {
        --i;
        if ( path[i] == '/' ) {
            if ( path.size() == 1 ) {
                return "/";
            }

            path.pop_back();
            break;
        }
        path.pop_back();
    }

    if ( path.size() < 1 )
        path = "./";

    return path;
}

std::string String::GetFileFromPath( const std::string& str ) {
    const std::size_t len = str.size();
    std::size_t i = len;

    while ( i>0 ) {
        --i;
        if ( str[i] == '/' || str[i] == '\\' ) {
            return Sub( str, i+1, len );
        }
    }
    return str;
}

std::string String::RemoveBackDirs( const std::string& path ) {
    std::string ret = path;
    
    uint slashIndex = 0;
    bool foundSlashIndex = false;
    for ( uint i=0; i<path.size()-2; ++i ) {
        if ( path.substr(i,4) == "/../" ) {
            if ( !foundSlashIndex ) {
                i+=3; // skip the /.. because we have no information about the previous directory
                continue;
            }
            
            ret = String::Sub( path, 0, slashIndex);
            ret += RemoveBackDirs( path.substr( i+4 ).c_str() );
            break;
        }
        if ( path[i] == '/' ) {
            slashIndex = i;
            foundSlashIndex = true;
        }
    }
    
    return ret;
}

std::string String::GetFilePath( const std::string& path ) {
    std::size_t i=path.size();
    while ( i > 0 ) {
        --i;
        if ( path[i] == '/' ) {
            return Sub( path, 0, i );
        }
    }

    return std::string("");
}

void String::TrimTrailingNum( std::string& str, const std::size_t count ) {
    auto len = str.size();
    if ( count >= len ) {
        str.resize(0);
    } else {
        str.resize(len-count);
    }
}

void String::TrimTrailing( std::string& str, const char* these ) {
    ASSERT( these );
    auto len = str.size();
    if ( len < 1)
        return;

    while ( CharIsOneOf( str[len-1], these) )
        --len;

    str.resize(len);
}

bool String::IsInt( const char* str ) {
    if ( !str )
        return false;

    const char* c = str;

    if ( *c == '\0' )
        return false; // empty

    if ( *c == '-' ) {
        // negative
        ++c;
        if ( *c == '\0' ) {
            return false; // just a dash
        }
    }

    for ( ; *c != '\0'; ++c )
        if ( !std::isdigit( *c ) )
            return false;

    return true;
}

bool String::IsUInt( const char* str ) {
    return str && str[0] != '-' && IsInt( str );
}

void String::TrimLeadingNum( std::string& str, const std::size_t count ) {
    if ( count > str.size() ) {
        str.clear();
        return;
    }
    str = str.substr( count, str.size() );
}

void String::TrimLeading( std::string& str, const char* these ) {
    ASSERT( these );
    str.assign(
        std::find_if( std::cbegin(str), std::cend(str), [&these](const char& c){return !CharIsOneOf(c, these);} )
        , std::cend(str)
    );
}

void String::Trim( std::string& str, const char* these ) {
    TrimTrailing( str, these );
    TrimLeading( str, these );
}

int String::ICmp( const char* str, const char* str2 ) {
    if ( str == nullptr || str2 == nullptr )
        return str == str2;
    std::string a(str);
    ToUpper( a );
    std::string b(str2);
    ToUpper( b );
    return a.compare( b );
}

int String::Cmp( const char* str, const char* str2 ) {
    if ( str == nullptr || str2 == nullptr )
        return -1;
    return std::string(str).compare( str2 );
}

bool String::IsUInt( const std::string& str ) {
    return IsUInt( str.c_str() );
}

bool String::IsInt( const std::string& str ) {
    return IsInt( str.c_str() );
}

bool String::ToBool( const std::string& str ) {
    return ToBool( str.c_str() );
}

float String::ToFloat( const std::string& str ) {
    return ToFloat( str.c_str() );
}

double String::ToDouble( const std::string& str ) {
    return ToDouble( str.c_str() );
}

int String::ToInt( const std::string& str, const int base ) {
    return ToInt( str.c_str(), base );
}

uint String::ToUInt( const std::string& str, const int base ) {
    return ToUInt( str.c_str(), base);
}

int String::ToInt( const char *str ) {
    return ToInt( str, 10 );
}

int String::ToInt( const std::string& str ) {
    return ToInt( str, 10 );
}

uint String::ToUInt( const char *str ) {
    return ToInt( str, 10 );
}

uint String::ToUInt( const std::string& str ) {
    return ToInt( str, 10 );
}

void String::TruncateFloat( const std::string& str, uint digits, std::string& out ) {
    out.reserve(str.size());
    for ( uint i=0; i<str.size(); ++i ) {
        out += str[i];
        
        if ( str[i]=='.' ) {
            for ( digits += ++i; i<digits; ++i ) 
                out += str[i];
            
            return;
        }
    }
}

std::string String::TruncateFloat( const std::string& str, const uint digits ) {
    std::string ret;
    TruncateFloat( str, digits, ret );
    return ret;
}
    

std::vector< std::string > String::ParseStringList( const std::string& str, const std::string& delimiter ) {
    std::vector<std::string> ret;
    std::size_t start = 0;

    while( true ) {
        std::size_t end = str.find(delimiter, start);
        if ( end == std::string::npos ) {
            if ( start < str.length() ) {
                ret.emplace_back( str.substr(start, end-start) );
            }
            break;
        }

        ret.emplace_back( str.substr(start, end-start) );
        start = end + delimiter.length();
    }

    return ret;
}

// todo: analyze other string/quote parsing snippets and merge/replace
std::vector<std::string> String::ListifyString( const char* str ) {
    std::size_t len = String::Len( str );
    std::string tmp;
    std::vector<std::string> list;

    if ( len < 1 )
        return list;

    bool foundStart=false;
    bool foundQuote=false;
    for ( std::size_t i=0; i<len; ++i ) {
        char chr = str[i];

        switch ( chr ) {
            case '\"':
                if ( foundQuote ) {
                    list.push_back( tmp );
                    tmp.erase();
                    foundQuote = foundStart = false;
                } else {
                    foundQuote = foundStart = true;
                }
                break;
            case ' ':
            case '\t':
                if ( !foundQuote ) {
                    if ( foundStart ) {
                        foundStart = false;
                        list.push_back( tmp );
                        tmp.erase();
                    }
                    break;
                } else {
                    FALLTHROUGH;
                }
            default:
                if ( !foundStart ) {
                    foundStart = true;
                }
                tmp += chr;
        }
    }
    if ( foundStart ) {
        list.push_back( tmp );
    }

    return list;
}

std::string String::FromVector( const std::vector< std::string >& vec, const char delimiter ) {
    std::string ret;
    
    if ( vec.size() > 1 ) {
        uint size = vec.size() * 3; // space enough to assum each item will have two quotes and a delimiter
        for ( auto item : vec )
            size += item.size();
        
        ret.reserve(size);
        
        ret = vec[0];
    
        for ( uint i=1; i<vec.size(); ++i ) {
            ret += delimiter;
            
            const bool quote = Contains( vec[i], ' ' );
            if ( quote )
                ret += '"';
                
            ret += vec[i];
            
            if ( quote )
                ret += '"';
        }
    }
        
    return ret;
}
