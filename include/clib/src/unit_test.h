// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef CLIB_SRC_UNIT_TEST_H
#define CLIB_SRC_UNIT_TEST_H

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

#ifdef CLIB_UNIT_TEST

    void CLibUnitTest( void );

#endif // CLIB_UNIT_TEST

#endif // CLIB_SRC_UNIT_TEST_H
