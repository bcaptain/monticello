// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./mat.h"

namespace String {
    std::string Mat4fToString( const Mat4f& m ) {
        return
            std::to_string(m.M(0,0))+','+std::to_string(m.M(0,1))+','+std::to_string(m.M(0,2))+','+std::to_string(m.M(0,3))+
            std::to_string(m.M(1,0))+','+std::to_string(m.M(1,1))+','+std::to_string(m.M(1,2))+','+std::to_string(m.M(1,3))+
            std::to_string(m.M(2,0))+','+std::to_string(m.M(2,1))+','+std::to_string(m.M(2,2))+','+std::to_string(m.M(2,3))+
            std::to_string(m.M(3,0))+','+std::to_string(m.M(3,1))+','+std::to_string(m.M(3,2))+','+std::to_string(m.M(3,3))
        ;
    }
}

Vec3f Mat4f::operator*( const Vec3f& vec ) const {
    Vec3f ret;
    float w;

    ret.x = vec.x*M(0,0) + vec.y*M(0,1) + vec.z*M(0,2) + M(0,3);
    ret.y = vec.x*M(1,0) + vec.y*M(1,1) + vec.z*M(1,2) + M(1,3);
    ret.z = vec.x*M(2,0) + vec.y*M(2,1) + vec.z*M(2,2) + M(2,3);
    w = vec.x*M(3,0) + vec.y*M(3,1) + vec.z*M(3,2) + M(3,3);

    if ( !Maths::Approxf( w, 0.0f, EP_MIL ) ) {
        ret.x /= w;
        ret.y /= w;
        ret.z /= w;
    }
    
    return ret;
}

Vec3f Mat4f::VecTimesMat4( const Vec3f& vec ) const {
    Vec3f ret;
    float w;
    
    ret.x = vec.x*M(0,0) + vec.y*M(1,0) + vec.z*M(2,0) + M(3,0);
    ret.y = vec.x*M(0,1) + vec.y*M(1,1) + vec.z*M(2,1) + M(3,1);
    ret.z = vec.x*M(0,2) + vec.y*M(1,2) + vec.z*M(2,2) + M(3,2);
    w = vec.x*M(0,3) + vec.y*M(1,3) + vec.z*M(2,3) + M(3,3);
        
    if ( !Maths::Approxf( w, 0.0f, EP_MIL ) ) {
        ret.x /= w;
        ret.y /= w;
        ret.z /= w;
    }
    
    return ret;
}

Mat4f Mat4f::operator*( const Mat4f& multiplier ) const {
    Mat4f tmp;
    Multiply(multiplier, tmp);
    return tmp;
}

// loads identity by default
Mat4f::Mat4f( void )
    : Mat3f(
        new float[16]{
            1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,0,1
        }, 4
      )
{ }

Mat4f::Mat4f( const Vec3f& from_angles_ZYX )
    : Mat4f()
{
    RotateZYX( from_angles_ZYX );
}

Mat4f::~Mat4f( void ) {
    if ( !data )
        return;
    DELNULLARRAY( data );
}

#ifdef TMP
    #error "macro TMP was already defined"
#endif //TMP

#define TMP(x) static_cast< const float >(mat[(x)])
Mat4f::Mat4f( const double mat[16] )
    : Mat3f( new float[16]{
                TMP(0), TMP(1), TMP(2), TMP(3),
                TMP(4), TMP(5), TMP(6), TMP(7),
                TMP(8), TMP(9), TMP(10), TMP(11),
                TMP(12), TMP(13), TMP(14), TMP(15)
            }, 4
      )
{ }
#undef TMP

Mat4f::Mat4f( const float mat[16] )
    : Mat3f( new float[16]{
                mat[0], mat[1], mat[2], mat[3],
                mat[4], mat[5], mat[6], mat[7],
                mat[8], mat[9], mat[10], mat[11],
                mat[12], mat[13], mat[14], mat[15]
            }, 4
      )
{ }

Mat4f::Mat4f( const Dualquat& dq ) : Mat3f(new float[16]{}, 4)
{
    SetRotationFromQuat(dq.rot);
    SetTranslation(dq.GetTranslation());  //todo: this could be set initialized above
    data[15] = 1.0f;  //todo: this could be set initialized above
}

Mat4f::Mat4f( const Vec3f& tran, const Quat& rot )
    : Mat3f(new float[16]{}, 4)
{
    SetRotationFromQuat(rot);
    SetTranslation(tran); //todo: this could be set initialized above
    data[15] = 1.0f; //todo: this could be set initialized above
}

Mat4f::Mat4f( const Mat4f& other )
    : Mat3f( new float[16]{
                other.data[0], other.data[1], other.data[2], other.data[3],
                other.data[4], other.data[5], other.data[6], other.data[7],
                other.data[8], other.data[9], other.data[10], other.data[11],
                other.data[12], other.data[13], other.data[14], other.data[15]
            }, 4
      )
{ }

Mat4f::Mat4f( Mat4f&& other_rref )
    : Mat3f( other_rref.data, 4 )
{
    ASSERT( other_rref.dim == 4 );
    other_rref.data = nullptr;
}

Mat4f::Mat4f( const Mat3f& other )
    : Mat3f( new float[16]{
                other.data[0], other.data[1], other.data[2], 0.0f,
                other.data[3], other.data[4], other.data[5], 0.0f,
                other.data[6], other.data[7], other.data[8], 0.0f,
                0.0f, 0.0f,0.0f, 1.0f
            }, 4
      )
{ }

void Mat4f::LoadIdentity( void ) {
    // floats are promoted to double in variadic functions
    const std::initializer_list<float> val {
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0
    };
    std::copy( val.begin(), val.end(), data );
}

Mat4f& Mat4f::operator=( const Mat4f& other ) {
    for ( std::size_t i=0; i<16; ++i )
        data[i] = other.data[i];

    return *this;
}

Mat4f& Mat4f::operator=( Mat4f&& other_rref ) {
    ASSERT( dim == 4 );
    ASSERT( other_rref.dim == 4 );
    delete[] data;
    data = other_rref.data;
    other_rref.data = nullptr;
    return *this;
}

Mat4f& Mat4f::operator=( const double mat[16] ) {
    for ( std::size_t i=0; i<16; ++i )
        data[i] = static_cast<float>( mat[i] );

    return *this;
}

Mat4f& Mat4f::operator=( const float mat[16] ) {
    for ( std::size_t i=0; i<16; ++i )
        data[i] = mat[i];

    return *this;
}

void Mat4f::Multiply( const Mat4f& other, Mat4f& out ) const {
    for ( uint c=0; c<4; ++c ) {
        for ( uint r=0; r<4; ++r ) {
            out.M(c,r) =
                M(0,r) * other.M(c,0) +
                M(1,r) * other.M(c,1) +
                M(2,r) * other.M(c,2) +
                M(3,r) * other.M(c,3);
        }
    }
}

Vec3f Mat4f::MultiplyWithPerspective( const Vec3f& vec ) const {
    Vec3f ret( vec * *static_cast< const Mat3f* >( this ) );
    ret.x += data[12];
    ret.y += data[13];
    ret.z += data[14];
    float w = data[3]*vec.x + M(1,3)*vec.y + M(2,3)*vec.z;
    if ( !Maths::Approxf( w, 0.0f ) ) {
        ret.x /= w;
        ret.y /= w;
        ret.z /= w;
    }
    return ret;
}

Mat4f& Mat4f::operator*=( const Mat4f& other ) {
    Mat4f tmp;
    Multiply(other, tmp);
    return *this = tmp;
}

void Mat4f::ZeroTranslation( void ) {
    data[12] = 0.0f;
    data[13] = 0.0f;
    data[14] = 0.0f;
}

void Mat4f::Translate( const float x, const float y, const float z ) {
    data[12] += x*data[0] + y*data[4] + z*data[8];
    data[13] += x*data[1] + y*data[5] + z*data[9];
    data[14] += x*data[2] + y*data[6] + z*data[10];
    data[15] += x*data[3] + y*data[7] + z*data[11];
}

void Mat4f::Set(const float mat[16] ) {
    for ( std::size_t x=0; x<4; ++x )
        data[x] = mat[x];
}

void Mat4f::Scale( const float x, const float y, const float z ) {
    Mat3f::Scale(x,y,z);
    data[3] *= x;
    data[7] *= y;
    data[11] *= z;
}


bool Mat4f::operator==( const Mat4f& other ) const {
    return Approx( other, EP_TEN_THOUSANDTH );
}

bool Mat4f::Approx( const Mat4f& other, const float epsilon ) const {
    std::size_t r;
    for ( r=0; r<16; ++r )
        if ( ! Maths::Approxf( data[r], other.data[r], epsilon ) )
            return false;

    return true;
}

void Mat4f::Transpose( void ) {
    std::swap( M(0,1), M(1,0) );
    std::swap( M(0,2), M(2,0) );
    std::swap( M(1,2), M(2,1) );
    
    std::swap( M(0,3), M(3,0) );
    std::swap( M(1,3), M(3,1) );
    std::swap( M(2,3), M(3,2) );
}

bool Mat4f::Invert( void ) {
    // FORMULA:
    //
    // A' = (1/d) * J
    //
    // A' is inverse matrix
    // A is original matrix
    // J is adjugate matrix of C (meaning the transpose of C)
    // C is cofactor matrix of N
    // N is the matrix of Minors of A
    // The minors are the determinants of the 3x3 submatrix for each cell in A, comprised by ignoring the row and column it blongs to.
    // d = determinant of original matrix
    
    Mat4f tmp( *this );
    
    // ** Find the minor of each element.
    // The minor is the determinant of the 3x3 submatrix resulting from covering up the row and column the element belongs to.
    //
    // along the way, we also cofactor the minors. The mathematical representation of this is (-1^(r+c)), where r and c are the
    // current row you're generating a submatrix for. An easier way to think of this is to apply a checker-board of negative 
    // signs to the the values in the 4x4 matrix like so:
    //
    // + - + -
    // - + - +
    // + - + -
    // - + - +
    
    // column 0 ( far left )
    tmp.M(0,0) =
        M(1,1)*M(2,2)*M(3,3) - M(1,3)*M(2,2)*M(3,1)
        + M(2,1)*M(3,2)*M(1,3) - M(2,3)*M(3,2)*M(1,1)
        + M(3,1)*M(1,2)*M(2,3) - M(3,3)*M(1,2)*M(2,1);
        
    tmp.M(0,1) =
        - M(1,0)*M(2,2)*M(3,3) + M(1,3)*M(2,2)*M(3,0)
        - M(2,0)*M(3,2)*M(1,3) + M(2,3)*M(3,2)*M(1,0)
        - M(3,0)*M(1,2)*M(2,3) + M(3,3)*M(1,2)*M(2,0);
        
    tmp.M(0,2) =
        M(1,0)*M(2,1)*M(3,3) - M(1,3)*M(2,1)*M(3,0)
        + M(2,0)*M(3,1)*M(1,3) - M(2,3)*M(3,1)*M(1,0)
        + M(3,0)*M(1,1)*M(2,3) - M(3,3)*M(1,1)*M(2,0);
        
    tmp.M(0,3) =
        - M(1,0)*M(2,1)*M(3,2) + M(1,2)*M(2,1)*M(3,0)
        - M(2,0)*M(3,1)*M(1,2) + M(2,2)*M(3,1)*M(1,0)
        - M(3,0)*M(1,1)*M(2,2) + M(3,2)*M(1,1)*M(2,0);
        
    // column 1
    tmp.M(1,0) =
        - M(0,1)*M(2,2)*M(3,3) + M(0,3)*M(2,2)*M(3,1)
        - M(2,1)*M(3,2)*M(0,3) + M(2,3)*M(3,2)*M(0,1)
        - M(3,1)*M(0,2)*M(2,3) + M(3,3)*M(0,2)*M(2,1);
        
    tmp.M(1,1) =
        M(0,0)*M(2,2)*M(3,3) - M(0,3)*M(2,2)*M(3,0)
        + M(2,0)*M(3,2)*M(0,3) - M(2,3)*M(3,2)*M(0,0)
        + M(3,0)*M(0,2)*M(2,3) - M(3,3)*M(0,2)*M(2,0);
        
    tmp.M(1,2) =
        - M(0,0)*M(2,1)*M(3,3) + M(0,3)*M(2,1)*M(3,0)
        - M(2,0)*M(3,1)*M(0,3) + M(2,3)*M(3,1)*M(0,0)
        - M(3,0)*M(0,1)*M(2,3) + M(3,3)*M(0,1)*M(2,0);
        
    tmp.M(1,3) =
        M(0,0)*M(2,1)*M(3,2) - M(0,2)*M(2,1)*M(3,0)
        + M(2,0)*M(3,1)*M(0,2) - M(2,2)*M(3,1)*M(0,0)
        + M(3,0)*M(0,1)*M(2,2) - M(3,2)*M(0,1)*M(2,0);
        
    // column 2
    tmp.M(2,0) =
        M(0,1)*M(1,2)*M(3,3) - M(0,3)*M(1,2)*M(3,1)
        + M(1,1)*M(3,2)*M(0,3) - M(1,3)*M(3,2)*M(0,1)
        + M(3,1)*M(0,2)*M(1,3) - M(3,3)*M(0,2)*M(1,1);
        
    tmp.M(2,1) =
        - M(0,0)*M(1,2)*M(3,3) + M(0,3)*M(1,2)*M(3,0)
        - M(1,0)*M(3,2)*M(0,3) + M(1,3)*M(3,2)*M(0,0)
        - M(3,0)*M(0,2)*M(1,3) + M(3,3)*M(0,2)*M(1,0);
        
    tmp.M(2,2) =
        M(0,0)*M(1,1)*M(3,3) - M(0,3)*M(1,1)*M(3,0)
        + M(1,0)*M(3,1)*M(0,3) - M(1,3)*M(3,1)*M(0,0)
        + M(3,0)*M(0,1)*M(1,3) - M(3,3)*M(0,1)*M(1,0);
        
    tmp.M(2,3) =
        - M(0,0)*M(1,1)*M(3,2) + M(0,2)*M(1,1)*M(3,0)
        - M(1,0)*M(3,1)*M(0,2) + M(1,2)*M(3,1)*M(0,0)
        - M(3,0)*M(0,1)*M(1,2) + M(3,2)*M(0,1)*M(1,0);
        
    // column 3 ( far right )
    tmp.M(3,0) =
        - M(0,1)*M(1,2)*M(2,3) + M(0,3)*M(1,2)*M(2,1)
        - M(1,1)*M(2,2)*M(0,3) + M(1,3)*M(2,2)*M(0,1)
        - M(2,1)*M(0,2)*M(1,3) + M(2,3)*M(0,2)*M(1,1);
        
    tmp.M(3,1) =
        M(0,0)*M(1,2)*M(2,3) - M(0,3)*M(1,2)*M(2,0)
        + M(1,0)*M(2,2)*M(0,3) - M(1,3)*M(2,2)*M(0,0)
        + M(2,0)*M(0,2)*M(1,3) - M(2,3)*M(0,2)*M(1,0);
        
    tmp.M(3,2) =
        - M(0,0)*M(1,1)*M(2,3) + M(0,3)*M(1,1)*M(2,0)
        - M(1,0)*M(2,1)*M(0,3) + M(1,3)*M(2,1)*M(0,0)
        - M(2,0)*M(0,1)*M(1,3) + M(2,3)*M(0,1)*M(1,0);
        
    tmp.M(3,3) =
        M(0,0)*M(1,1)*M(2,2) - M(0,2)*M(1,1)*M(2,0)
        + M(1,0)*M(2,1)*M(0,2) - M(1,2)*M(2,1)*M(0,0)
        + M(2,0)*M(0,1)*M(1,2) - M(2,2)*M(0,1)*M(1,0);
    
    tmp.Transpose();
    
    // ** determinant is easy to calculate since we've already done most of the work:
    const float det =
        M(0,0)*tmp.M(0,0) +
        M(0,1)*tmp.M(1,0) +
        M(0,2)*tmp.M(2,0) +
        M(0,3)*tmp.M(3,0);
    
    // ** it's possible the matrix has no inverse:
    if ( Maths::Approxf( det, 0.0f, EP_MIL ) )
        return false;
    
    const float det_reciprocol = 1.0f / det;
    
    for (uint c = 0; c < 16; ++c)
        data[c] = tmp.data[c] * det_reciprocol;
    
    return true;
}
        
void Mat4f::Frustum( const float left, const float right, const float bottom, const float top, const float xnear, const float xfar ) {
    // reference material: Appendix F - OpenGL Programming Guide.pdf
    // reference material: http://www.glprogramming.com/red/appendixf.html
    
    // reference material: glFrustum.gif
    // reference material: http://www.glprogramming.com/red/images/Image23.gif
    
    const float n2 = xnear * 2;
    data[0]=n2/(right-left);
    data[1]=0;
    data[2]=0;
    data[3]=0;

    data[4]=0;
    data[5]=n2/(top-bottom);
    data[6]=0;
    data[7]=0;

    data[8]=(right+left)/(right-left);
    data[9]=(top+bottom)/(top-bottom);
    data[10]=(-(xfar+xnear))/(xfar-xnear);
    data[11]=-1;

    data[12]=0;
    data[13]=0;
    data[14]=(-(n2*xfar*xnear))/(xfar-xnear);
    data[15]=0;
}

// http://www.glprogramming.com/red/appendixf.html "Appendix F - OpenGL Programming Guide.html"
// http://www.glprogramming.com/red/images/Image23.gif
void Mat4f::FrustumInverse( const float left, const float right, const float bottom, const float top, const float xnear, const float xfar ) {
    const float n2 = xnear * 2;
    data[0]=(right-left)/n2;
    data[1]=0;
    data[2]=0;
    data[3]=0;

    data[4]=0;
    data[5]=(top-bottom)/n2;
    data[6]=0;
    data[7]=0;

    data[8]=0;
    data[9]=0;
    data[10]=0;
    data[11]=(-(xfar-xnear))/(n2*xfar*xnear);

    data[12]=(right+left)/n2;
    data[13]=(top+bottom)/n2;
    data[14]=-1;
    data[15]=(xfar+xnear)/(n2*xfar*xnear);
}

// inspired by gluPerspective function source, from Mesa project
void Mat4f::Perspective( const float fov, const float x, const float y, const float xnear, const float xfar ) {
    const float aspect = x / y;
    const float t = xnear * tanf(fov * PI_OVER_360_F);
    Frustum(-t * aspect, t * aspect, -t, t, xnear, xfar);
}

void Mat4f::PerspectiveInverse( const float fov, const float x, const float y, const float xnear, const float xfar ) {
    const float aspect = x / y;
    const float t = xnear * tanf(fov * PI_OVER_360_F);
    FrustumInverse(-t * aspect, t * aspect, -t, t, xnear, xfar);
}

void Mat4f::Orthographic( const float bottom, const float top, const float left, const float right, const float xnear, const float xfar ) {
    data[0] = 2/(right - left);
    data[5] = 2/(top - bottom);
    data[10] = -2/(xfar - xnear);
    data[12] = -(right + left)/(right - left);
    data[13] = -(top + bottom)/(top - bottom);
    data[14] = -(xfar + xnear)/(xfar - xnear);
}

void Mat4f::OrthographicInverse( const float bottom, const float top, const float left, const float right, const float xnear, const float xfar ) {
    data[0] = (right - left)/2;
    data[5] = (top - bottom)/2;
    data[10] =  (xfar - xnear)/-2;
    data[12] = (right + left)/2;
    data[13] = (top + bottom)/2;
    data[14] = (xfar + xnear)/2;
}

void Mat4f::SetTranslation( const float x, const float y, const float z ) {
    data[12] = x;
    data[13] = y;
    data[14] = z;
}

void Mat4f::SetRotationFromQuat( const Quat& rot ) {
    float two_xy(2 * rot.x * rot.y)
        , two_wz(2 * rot.w * rot.z)
        , two_xz(2 * rot.x * rot.z)
        , two_yz(2 * rot.y * rot.z)
        , two_wx(2 * rot.w * rot.x)
        , two_wy(2 * rot.w * rot.y);

    if ( Maths::Approxf(two_xy, 1.0f, 0.001f)) { two_xy = 1.0f;}
    if ( Maths::Approxf(two_wz, 1.0f, 0.001f)) { two_wz = 1.0f;}
    if ( Maths::Approxf(two_xz, 1.0f, 0.001f)) { two_xz = 1.0f;}
    if ( Maths::Approxf(two_yz, 1.0f, 0.001f)) { two_yz = 1.0f;}
    if ( Maths::Approxf(two_wx, 1.0f, 0.001f)) { two_wx = 1.0f;}
    if ( Maths::Approxf(two_wy, 1.0f, 0.001f)) { two_wy = 1.0f;}

    Quat twoSq( 2 * rot.x * rot.x
         , 2 * rot.y * rot.y
         , 2 * rot.z * rot.z
         , 2 *rot.w * rot.w);

    if ( Maths::Approxf(twoSq.x, 0.0f, 0.001f)) { twoSq.x = 0.0f;}
    if ( Maths::Approxf(twoSq.y, 0.0f, 0.001f)) { twoSq.y = 0.0f;}
    if ( Maths::Approxf(twoSq.z, 0.0f, 0.001f)) { twoSq.z = 0.0f;}
    if ( Maths::Approxf(twoSq.w, 0.0f, 0.001f)) { twoSq.w = 0.0f;}

    if ( Maths::Approxf(twoSq.x, 1.0f, 0.001f)) { twoSq.x = 1.0f;}
    if ( Maths::Approxf(twoSq.y, 1.0f, 0.001f)) { twoSq.y = 1.0f;}
    if ( Maths::Approxf(twoSq.z, 1.0f, 0.001f)) { twoSq.z = 1.0f;}
    if ( Maths::Approxf(twoSq.w, 1.0f, 0.001f)) { twoSq.w = 1.0f;}

    data[0] = 1 - twoSq.y - twoSq.z;
    data[1] = two_xy + two_wz;
    data[2] = two_xz - two_wy;
    data[3] = 0;
    data[4] = two_xy - two_wz;
    data[5] = 1 - twoSq.x - twoSq.z;
    data[6] = two_yz + two_wx;
    data[7] = 0;
    data[8] = two_xz + two_wy;
    data[9] = two_yz - two_wx;
    data[10] = 1 - twoSq.x - twoSq.y;
    data[11] = 0;
}

void Mat4f::SetFromDualQuat( const Dualquat& dq ) {
    SetRotationFromQuat(dq.rot);
    SetTranslation(dq.GetTranslation());
    data[15] = 1.0f;
}

Vec3f Mat4f::GetTranslation( void ) const {
    return { data[12], data[13], data[14] };
}

void Mat4f::Translate( const Vec3f& vec ) {
    Translate(vec.x, vec.y, vec.z);
}

void Mat4f::SetTranslation( const Vec3f& translation ) {
    SetTranslation( translation.x, translation.y, translation.z );
}

Mat3f Mat4f::GetRotationMatrix( void ) const {
    return *static_cast<const Mat3f*>(this);
}

void Mat4f::ZeroRotation( void ) {
    Mat3f::LoadIdentity();
}

void Mat4f::Scale( const float xyz ) {
    Scale( xyz, xyz, xyz );
}

#ifdef CLIB_UNIT_TEST

    void Mat4f::UnitTest( void ) {
        Mat4f::UnitTest_Ctor();
        Mat4f::UnitTest_ConstructFromQuat();
        Mat4f::UnitTest_MultiplyWithMatrix();
        Mat4f::UnitTest_MultiplyWithVector();
        Mat4f::UnitTest_Rotate();
        Mat4f::UnitTest_RotateXYZ();
        Mat4f::UnitTest_Invert();
    }

    void Mat4f::UnitTest_Ctor( void ) {
        const float tmp[16]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        const Mat4f a( tmp );
        const Mat4f b(a);
        ASSERT( a.Approx(b) );

        const Mat4f c(a.data);
        ASSERT( a.Approx(c) );
    }

    void Mat4f::UnitTest_MultiplyWithMatrix( void ) {

        const float tmp2[16]{95,90,85,80,75,70,65,60,55,50,45,40,35,30,25,20};
        const Mat4f multiplicand(tmp2);
        const float tmp3[16]{2350,2700,3050,3400,1790,2060,2330,2600,1230,1420,1610,1800,670,780,890,1000};
        const Mat4f mult_result_should_be(tmp3);

        const float tmp[16]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
        const Mat4f a( tmp );
        Mat4f result;
        a.Multiply( multiplicand, result );
        ASSERT( result.Approx(mult_result_should_be) );
        
    }

    void Mat4f::UnitTest_MultiplyWithVector( void ) {
        const Vec3f vec(6,13,53);
        Mat4f mat;
        mat.Rotate( -123, 1,0,0 );
        mat.Translate( -11,12,-13 );
        mat.Rotate( -13, 0,0,1 );
        mat.Scale( 1.4f,1.3f,1.2f );
        mat.Rotate( 23, 0,1,0 );

        Vec3f test = vec * *static_cast<const Mat3f*>( &mat );
        test.x += mat.data[12];
        test.y += mat.data[13];
        test.z += mat.data[14];

        const Vec3f vecXmat_expected( 28.584922f, 24.832307f, -40.218761f );
        ASSERT( vec*mat == vecXmat_expected );
        
        const Vec3f matXvec_expected( -0.068680f, 0.141200f, 0.022904f );
        ASSERT( mat*vec == matXvec_expected );
    }

    void Mat4f::UnitTest_Rotate( void ) {
        Mat4f result;

        float rot_x_result[16]{1,0,0,0,0,0,-1,0,0,1,0,0,0,0,0,1};
        Mat4f rot_x_mat;
        result = rot_x_result;
        rot_x_mat.Rotate( -90, 1,0,0 ); // neg 90 around x axis
        ASSERT( rot_x_mat.Approx(result) );

        float rot_y_result[16]{0,0,1,0,0,1,0,0,-1,0,0,0,0,0,0,1};
        Mat4f rot_y_mat;
        result = rot_y_result;
        rot_y_mat.Rotate( -90, 0,1,0 ); // neg 90 around y axis
        ASSERT( rot_y_mat.Approx(result) );

        float rot_z_result[16]{0,-1,0,0,1,0,0,0,0,0,1,0,0,0,0,1};
        Mat4f rot_z_mat;
        result = rot_z_result;
        rot_z_mat.Rotate( -90, 0,0,1 ); // neg 90 around z axis
        ASSERT( rot_z_mat.Approx(result) );

        Vec3f vec_x_180(0,1,0);
        Mat4f mat_x_180;
        mat_x_180.Rotate(180,1,0,0);
        ASSERT( vec_x_180 * mat_x_180 == Vec3f(0,-1,0) );
    }

    void Mat4f::UnitTest_ConstructFromQuat( void ) {
        const float trans_quat_result[16]{1,0,0,0,0,0,1,0,0,-1,0,0,1,2,3,1};
        const Mat4f result( trans_quat_result );
        const Quat qRot(0.707f,0.0f,0.0f,0.707f);
        const Vec3f translate(1.0f,2.0f,3.0f);
        const Mat4f mat_from_trans_and_quat(translate, qRot);
        ASSERT( mat_from_trans_and_quat.Approx(result, 0.001f));
    }

    void Mat4f::UnitTest_RotateXYZ( void ) {
        const float rot_axisAngle_result[16]{0,0,-1,0,0,1,0,0,1,0,0,0,1,0,0,1}; // (1,0,0) Rotated 90 degrees on Y
        const float rot_axisAngle_test[16]{1,0,0,0,0,1,0,0,0,0,1,0,1,0,0,1}; //Identity matrix translated 1 unit on X
        const Mat4f result( rot_axisAngle_result );
        const Vec3f axisAngle(0,90,0);
        Mat4f rot_axisAngle_mat( rot_axisAngle_test );
        rot_axisAngle_mat.RotateXYZ( axisAngle );
        ASSERT( rot_axisAngle_mat.Approx(result) );
    }

    void Mat4f::UnitTest_Invert( void ) {
        Mat4f invert;
        invert.Scale(1.7f,2.8f,-3.5f);
        invert.Rotate(-14.4f,1,0,0);
        invert.Rotate(52.45f,0,1,0);
        invert.Rotate(-73.6f,0,0,1);
        invert.Translate(-5.45f,55.76f,43.23f);        
        invert.Invert();
       
        float invert_data2_expected[16]{
            0.101220f,
            0.343916f,
            0.466366f,
            -0.000000f,
            
            -0.351730f,
            0.030117f,
            0.054130f,
            -0.000000f,
            
            -0.006217f,
            0.230539f,
            -0.168659f,
            -0.000000f,
            
            5.450000f,
            -55.759998f,
            -43.230000f,
            1.000000f
        };
        
        const Mat4f invert2( invert_data2_expected );
        ASSERT( invert2.Approx( invert ) );
        
        invert.Rotate(2.45f,0,1,0);
        invert.Rotate(-4.4f,1,0,0);
        invert.Translate(-1.45f,-6.76f,-4.23f);
        invert.Rotate(-7.6f,0,0,1);
        invert.Translate(-1.45f,-6.76f,-4.23f);
        invert.Rotate(-7.6f,0,0,1);
        
        invert.Invert();
        
        float invert_data3_expected[16]{
            1.004769f,
            -2.571063f,
            -0.274948f,
            -0.000000f,

            0.837655f,
            0.243848f,
            2.873681f,
            -0.000000f,

            1.233385f,
            0.933096f,
            -1.968097f,
            -0.000000f,

            94.720985f,
            81.743118f,
            85.114090f,
            1.000000f
        };
        
        const Mat4f invert3( invert_data3_expected );
        ASSERT( invert3.Approx( invert ) );
    }

#endif //CLIB_UNIT_TEST
