// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_VALUES_H_
#define SRC_CLIB_MATH_VALUES_H_

#include <cmath>
#include <vector>
#include "../message.h"
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./vec3t-dec.h"
#include "../assert.h"

extern const float EP;
extern const float EP_MIL;
extern const float EP_HUNDREDTH;
extern const float EP_THOUSANDTH;
extern const float EP_TEN_THOUSANDTH;
extern const double PI_D;
extern const float PI_F;
extern const double PI_OVER_180_D;
extern const float PI_OVER_180_F;
extern const float HALF_PI_OVER_180_F;
extern const double ONE_EIGHTY_OVER_PI_D;
extern const float ONE_EIGHTY_OVER_PI_F;
extern const float RAD_TO_DEG_F;
extern const float DEG_TO_RAD_F;
extern const float PI_OVER_360_F;

template< typename TYPE >
class Vec3t;

typedef Vec3t<float> Vec3f;

namespace Maths {

    #ifdef CLIB_UNIT_TEST
        void UnitTest_Math( void );
        void UnitTest_IsPrime( void );

    #endif // CLIB_UNIT_TEST


    template < typename TYPE>
    constexpr bool IsPowerOf2( const TYPE x );

    bool Approxf( const float a, const float b, const float range = EP_TEN_THOUSANDTH );
    bool Approxf( const Vec3f& a, const Vec3f& b, const float range = EP_TEN_THOUSANDTH );
    bool Approxf( const Vec2f& a, const Vec2f& b, const float range = EP_TEN_THOUSANDTH );

    // IsPrime_Recursive
        template < typename TYPE >
        constexpr bool IsPrime_Recursive( const TYPE num );
        
        template < typename TYPE >
        constexpr bool IsPrime_Recursive_Helper( const TYPE num, const TYPE denom );
    
    // IsPrime_ConstExpr
        template < int interrogate, int denominator >
        struct IsPrime_ConstExpr_Helper {
            static const bool result = (interrogate%denominator) && IsPrime_ConstExpr_Helper<interrogate,denominator-1>::result;
        };

        template < int interrogate >
        struct IsPrime_ConstExpr_Helper<interrogate,1> {
            static const bool result = true;
        };

        template < int interrogate >
        struct IsPrime_ConstExpr {
            static const bool result = IsPrime_ConstExpr_Helper<interrogate,interrogate-1>::result;
        };
    
    // IsPrime_Iterative
        template < typename TYPE >
        bool IsPrime_Iterative( const TYPE interrogate );

    // IsPrime_Iterative_CalcPreceedingPrimes
        template < typename TYPE >
        bool IsPrime_Iterative_CalcPreceedingPrimes( const TYPE interrogate );
    
    // IsPrime Selection of Choice
        template < typename TYPE >
        bool IsPrime( const TYPE interrogate );
}

template < typename TYPE >
inline bool Maths::IsPrime( const TYPE interrogate ) {
    return IsPrime_Iterative<TYPE>( interrogate );
}

template < typename TYPE >
constexpr bool Maths::IsPrime_Recursive_Helper( const TYPE num, const TYPE denom ) {
    return ( denom == num ) || ( ( num % denom ) && IsPrime_Recursive_Helper( num, denom+1 ) );
}

template < typename TYPE >
constexpr bool Maths::IsPrime_Recursive( const TYPE num ) {
    return num%2 && num%3 && num%5 && num%7 && IsPrime_Recursive_Helper( num, 11 );
}

template < typename TYPE >
bool Maths::IsPrime_Iterative_CalcPreceedingPrimes( const TYPE interrogate ) {
    // todo: the for loop doesn't need to check division against values greater than or equal to (interrogate/2)
    std::vector<TYPE> primes;
    primes.reserve(48);
    primes.push_back(2);

    int check = 3;
    for ( TYPE i=primes.size()-1; i>=0; --i ) {
        if ( check % primes[i] == 0 ) {
            // ** it's not prime
            if ( check == interrogate )
                return false;

            // check next
            i=primes.size()-1;
            ++check;
            continue;
        }

        if ( i > 0 )
            continue;

        // ** reached the end, it's prime
        if ( check == interrogate )
            return true;

        primes.push_back(check);

        // check next
        i=primes.size()-1;
        ++check;
    }
    
    ASSERT(false); // this should never reach here
    return false;
}

template < typename TYPE >
bool Maths::IsPrime_Iterative( const TYPE interrogate ) {
    if ( interrogate <= 0 )
        return false;
        
    if ( interrogate == 1 )
        return true;

    for ( TYPE i=2; i<= static_cast<TYPE>( ceilf( static_cast<float>(interrogate)/i ) ); ++i )
        if ( interrogate % i == 0 )
                return false;
    
    return true;
}

// http://stackoverflow.com/questions/600293/how-to-check-if-a-number-is-a-power-of-2
template < typename TYPE>
constexpr bool Maths::IsPowerOf2( const TYPE x ) {
    return (x != 0) && ((x & (x - 1)) == 0);
}

#endif  //SRC_CLIB_MATH_VALUES
