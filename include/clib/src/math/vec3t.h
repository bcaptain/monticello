// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_VEC_H_3T
#define SRC_CLIB_MATH_VEC_H_3T

#include <cmath>
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "../maths.h"
#include "./values.h"
#include "./vec2t-dec.h"
#include "./vec3t-dec.h"
#include "./mat.h"

namespace String {
    template<>
    inline Vec3f To< Vec3f >( const std::string& str ) {
        return String::ToVec3f( str );
    }

    template<>
    inline std::string ToString<Vec3f>( const Vec3f& v ) {
        return VecToString(v,',');
    }
}

template<typename TYPE>
Vec3t<TYPE>::Vec3t( void ) : x(0.0f), y(0.0f), z(0.0f) {
}

template<typename TYPE>
Vec3t<TYPE>::Vec3t( const TYPE nx, const TYPE ny, const TYPE nz)
    : x(nx), y(ny), z(nz)
{ }

template<typename TYPE>
Vec3t<TYPE>::Vec3t( const TYPE all )
    : x(all), y(all), z(all)
{ }

template<typename TYPE>
inline Vec3t<TYPE> Vec3t<TYPE>::operator-(void) const {
    return Vec3t<TYPE>(-x, -y, -z);
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator*( const TYPE val ) const {
    return Vec3t<TYPE>(val * x, val * y, val * z);
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator*( const Vec3t<TYPE> &other ) const {
    return Vec3t<TYPE>(x * other.x, y * other.y, z * other.z);
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator*=( const TYPE a ) {
    x *= a;
    y *= a;
    z *= a;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator*=( const Vec3t<TYPE> &other ) {
    x *= other.x;
    y *= other.y;
    z *= other.z;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator+( const Vec3t<TYPE> &other ) const {
    return Vec3t<TYPE>(x + other.x, y + other.y, z + other.z);
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator+( const TYPE val ) const {
    return Vec3t<TYPE>(x + val, y + val, z + val);
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator+=( const Vec3t<TYPE> &other ) {
    x += other.x;
    y += other.y;
    z += other.z;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator+=( const TYPE val ) {
    x += val;
    y += val;
    z += val;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator-( const Vec3t<TYPE> &other ) const {
    return Vec3t<TYPE>(x - other.x, y - other.y, z - other.z);
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator-( const TYPE val ) const {
    return Vec3t<TYPE>(x - val, y - val, z - val);
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator-=( const TYPE val ) {
    x -= val;
    y -= val;
    z -= val;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator-=( const Vec3t<TYPE> &other ) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator/( const TYPE val ) const {
    return Vec3t<TYPE> (
        Maths::Divide( x, val )
        , Maths::Divide( y, val )
        , Maths::Divide( z, val )
    );
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator/( const Vec3t<TYPE> &other ) const {
    return Vec3t<TYPE> (
        Maths::Divide( x, other.x )
        , Maths::Divide( y, other.y )
        , Maths::Divide( z, other.z )
    );
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator/=( const Vec3t<TYPE> &other ) {
    x = Maths::Divide( x, other.x );
    y = Maths::Divide( y, other.y );
    z = Maths::Divide( z, other.z );
    return *this;
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::operator/=( const TYPE val ) {
    x = Maths::Divide( x, val );
    y = Maths::Divide( y, val );
    z = Maths::Divide( z, val );
    return *this;
}

template<typename TYPE>
inline bool Vec3t<TYPE>::operator!=( const Vec3t<TYPE> &other) const {
    return !operator==(other);
}

template<typename TYPE>
inline bool Vec3t<TYPE>::operator!=( const TYPE val) const {
    return !operator==(val);
}

template<typename TYPE>
bool Vec3t<TYPE>::operator==( const Vec3t<TYPE> &other) const {
    return (
        Maths::Approxf( x, other.x ) &&
        Maths::Approxf( y, other.y ) &&
        Maths::Approxf( z, other.z )
    );
}

template<typename TYPE>
bool Vec3t<TYPE>::operator==( const TYPE val) const {
    return (
        Maths::Approxf( x, val ) &&
        Maths::Approxf( y, val ) &&
        Maths::Approxf( z, val )
    );
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::Set( const TYPE a, const TYPE b, const TYPE c ) {
    x=a;
    y=b;
    z=c;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::SetNormalized( const TYPE a, const TYPE b, const TYPE c ) {
    Set(a,b,c);
    Normalize();
    return *this;
}

template<typename TYPE>
Vec3t<TYPE>& Vec3t<TYPE>::Set( const TYPE a, const TYPE b ) {
    x=a;
    y=b;
    return *this;
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::Cross( const Vec3t<TYPE> &other ) const {
    return Vec3t<TYPE>(
        y * other.z - other.y * z
        , z * other.x - other.z * x
        , x * other.y - other.x * y
    );
}

template<typename TYPE>
TYPE Vec3t<TYPE>::Dot( const Vec3t<TYPE> &other ) const {
    return (x * other.x) + (y * other.y) + (z * other.z);
}

template<typename TYPE>
void Vec3t<TYPE>::Zero( void ) {
    x=0;
    y=0;
    z=0;
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::Cross( const Vec3t<TYPE> &first, const Vec3t<TYPE> &second ) {
    return Vec3t<TYPE>(
         second.y * first.z - first.y * second.z
        , second.z * first.x - first.z * second.x
        , second.x * first.y - first.x * second.y
    );
}

template<typename TYPE>
inline Vec2t<TYPE> Vec3t<TYPE>::ToVec2( void ) const {
    return Vec2t<TYPE>(x,y);
}

template<typename TYPE>
inline Vec3t<TYPE> Vec3t<TYPE>::PerpCCW_ZAxis( void ) const {
    return {-y, x, 0.0f};
}

template<typename TYPE>
inline Vec3t<TYPE> Vec3t<TYPE>::PerpCW_ZAxis( void ) const {
    return {y, -x, 0.0f};
}

template<typename TYPE>
float Vec3t<TYPE>::Len( void ) const {
    return sqrtf( x*x + y*y + z*z );
}

template<typename TYPE>
float Vec3t<TYPE>::SquaredLen( void ) const {
    return x*x + y*y + z*z;
}

template<typename TYPE>
float Vec3t<TYPE>::Len( const Vec3f& vec ) {
    return vec.Len();
}

template<typename TYPE>
float Vec3t<TYPE>::SquaredLen( const Vec3f& vec ) {
    return vec.SquaredLen();
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::GetNormalized( const Vec3t<TYPE>& vec ) {
    return vec.GetNormalized();
}

template<typename TYPE>
void Vec3t<TYPE>::Normalize( void ) {
    operator/=( Len() );
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::GetNormalized( void ) const {
    return *this / Len();
}

template<typename TYPE>
float Vec3t<TYPE>::RadiansBetween( const Vec3f& vec1, const Vec3f& vec2 ) const {
    const float ret = acosf( vec1.Dot( vec2 ) );
    if ( std::isnormal(ret) )
        return ret;
    return 0;
}

template<typename TYPE>
float Vec3t<TYPE>::RadiansBetween( const Vec3f& vec ) const {
    return RadiansBetween( *this, vec );
}

template<typename TYPE>
float Vec3t<TYPE>::DegreesBetween( const Vec3f& vec1, const Vec3f& vec2 ) const {
    return RadiansBetween(vec1, vec2) * RAD_TO_DEG_F;
}

template<typename TYPE>
float Vec3t<TYPE>::DegreesBetween( const Vec3f& vec ) const {
    return DegreesBetween(*this, vec);
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::Interp( const Vec3t<TYPE>& to, const float delta) const {
    return Vec3t<TYPE>((to * delta) + (*this * (1.0f - delta)));
}

template<typename TYPE>
TYPE Vec3t<TYPE>::operator[]( const std::size_t index ) const {
    switch ( index ) {
        case 0: return x;
        case 1: return y;
        case 2: return z;
        default:
            DIE("Invalid vertex index: %u\n", index);
            return z; // return last element in list, if user wishes to not abort the program
    };
}

template<typename TYPE>
TYPE& Vec3t<TYPE>::operator[]( const std::size_t index ) {
    switch ( index ) {
        case 0: return x;
        case 1: return y;
        case 2: return z;
        default:
            DIE("Invalid vertex index: %u\n", index);
            return z; // return last element in list, if user wishes to not abort the program
    };
}

template<typename TYPE>
Vec3t<TYPE> Vec3t<TYPE>::operator*( const Quat& quat ) const {
    return quat * *this;
}

#endif  //SRC_CLIB_MATH_VEC3T
