// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_DUALQUAT_H
#define SRC_CLIB_MATH_DUALQUAT_H

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./quat.h"

class Dualquat {

public:
    Dualquat( void );
    Dualquat( Dualquat const & other );
    Dualquat( const float _rot[4], const float _tran[4] );
    Dualquat( const Quat& _rot, const Quat& _tran);
    Dualquat( const Quat& _rot);
    Dualquat( const Mat4f& mat ); //! left unexplicit for Dualquat::FromMatrix() as implicit brace-init-list construction
    Dualquat( const Vec3f& translation, const Quat& _rot );
    explicit Dualquat( const Vec3f& translation );
    ~Dualquat( void ) = default;

public:
    Dualquat& operator=(const Dualquat &other);
    Dualquat( const Vec3f& translation, const Mat3f& mat );

    bool operator==( const Dualquat& other ) const;
    bool operator!=( const Dualquat& other ) const;

    Dualquat operator*( const Dualquat& other ) const;

    Dualquat operator*( float val ) const;
    Dualquat& operator*=( float val );

    Dualquat operator+(const Dualquat& other ) const;
    Dualquat& operator+=(const Dualquat& other );

public:
    void SetFromMatrix( const Mat4f& mat );

    void Normalize( void ); //! Normalizes both "Real" and "Dual" parts.
    void BecomeConjugate( void ); //! There are multiple ways to get a conjugate of a DQ.  We're using DQ' =  Qr' + Qd'
    void CalcDual_RotTran(const Vec3f& vec);
    void CalcDual_TranRot(const Vec3f& vec);
    
    Quat GetDual_RotTran(const Vec3f& vec) const;
    Quat GetDual_TranRot(const Vec3f& vec) const;

    static Quat GetDual_RotTran(const Vec3f& vec, const Quat& _rot);
    static Quat GetDual_TranRot(const Vec3f& vec, const Quat& _rot);
    
    Vec3f GetTranslation( void ) const; //! Returns the translation of a DQ. Translation = 2 * Qd * Qr'
    float Dot(const Dualquat& other) const;
    
    Dualquat GetInterpolated( const Dualquat& from, float delta ) const;

    #ifdef CLIB_UNIT_TEST
        static void UnitTest( void );
    #endif // CLIB_UNIT_TEST

    static Dualquat FromMatrix( const Mat4f& mat );
    static Dualquat GetInterpolated( const Dualquat& from, const Dualquat& to, float delta );
    
public:
    void Print( void ) const;

public:
    Quat rot; //! "Real" part. A rotation quaternion, it must be a unit quaternion
    Quat tran; //! "Dual" part. A quaternion that represents translation. \see GetTranslation()
};

Vec3f operator*(const Vec3f& vec, Dualquat multiplier_byVal );
Vec3f& operator*=(Vec3f& vec, const Dualquat& multiplier );

inline Dualquat Dualquat::FromMatrix( const Mat4f& mat ) {
    return { mat };
}

#endif // SRC_CLIB_MATH_DUALQUAT_H
