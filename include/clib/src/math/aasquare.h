// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_AASQUARE_H_
#define SRC_CLIB_MATH_AASQUARE_H_

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./vec3t.h"
#include "./vec2t.h"

class AASquare2v {
public:
    AASquare2v( const Vec2f& _mins, const Vec2f& _maxs );
    AASquare2v( const AASquare2v& other ) = default;
    AASquare2v& operator=( const AASquare2v& other ) = default;

public:
    Vec2f mins, maxs;
};

class AASquare4v {
public:
    AASquare4v( const AASquare4v& other ) = default;
    AASquare4v( const Vec2f& _mins, const Vec2f& _maxs );
    AASquare4v( const AASquare2v& other );
    
public:
    AASquare4v& operator=( const AASquare4v& other ) = delete;

public:
    const Vec2f tl,tr,bl,br;
};


#endif  //SRC_CLIB_MATH_AASQUARE_H_
