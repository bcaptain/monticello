// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./circle.h"

Circle2f::Circle2f( void )
    : Circle2f( Vec2f(0,0), 0.0f )
{
}

Circle2f::Circle2f( const Vec2f& _origin, const float _radius )
    : origin( _origin )
    , radius( _radius )
{ }

Circle3f::Circle3f( void )
    : Circle3f( Vec3f(0,0,0), 0.0f, Vec3f(0,0,1) )
{
}

Circle3f::Circle3f( const Vec3f& _origin, const float _radius, const Vec3f& _axis )
    : origin( _origin )
    , radius( _radius )
    , axis( _axis )
{ }
