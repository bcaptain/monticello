// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./vec3t.h"
#include "./strings.h"

template class Vec3t<float>;

#ifdef CLIB_UNIT_TEST
    template<>
    void Vec3t<float>::UnitTest( void ) {

        float ninety = Vec3f(0,1,0).DegreesBetween( Vec3f(1,0,0) );
        ASSERT( Maths::Approxf( ninety, 90.0f ) );

        ninety = Vec3f(0,2,0).DegreesBetween( Vec3f(2,0,0) );
        ASSERT( Maths::Approxf( ninety, 90.0f ) );

        ninety = Vec3f(0,1,0).DegreesBetween( Vec3f(2,0,0) );
        ASSERT( Maths::Approxf( ninety, 90.0f ) );
    }
#endif // CLIB_UNIT_TEST

template <>
void Vec3t<float>::Print( void ) const {
    Printf("%f,%f,%f",static_cast<double>(x),static_cast<double>(y),static_cast<double>(z));
}

template<>
inline Vec3t<float> Vec3t<float>::operator*( const Mat3f& mat ) const {
    return mat.VecTimesMat3( *this );
}

template<>
inline Vec3t<float> Vec3t<float>::operator*( const Mat4f& mat ) const {
    return mat.VecTimesMat4( *this );
}

template<>
Vec3t<float>& Vec3t<float>::operator*=( const Mat3f& mat ) {
    return operator=( *this * mat );
}

template<>
Vec3t<float>& Vec3t<float>::operator*=( const Mat4f& mat ) {
    return operator=( *this * mat );
}

template<>
Vec3t<float>& Vec3t<float>::operator*=( const Quat& quat ) {
    return operator=( *this * quat );
}

namespace String {
    bool ToVec3f( const std::string& vecstr, Vec3f& out ) {
        return ToVec< Vec3f, 2 >(vecstr, out);
    }

    Vec3f ToVec3f( const std::string& str ) {
        Vec3f ret;
        ToVec3f( str, ret );
        return ret;
    }
}
