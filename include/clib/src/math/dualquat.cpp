// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"

#include "./dualquat.h"
#include "./mat.h"

Dualquat::Dualquat( void )
    : rot()
    , tran(0,0,0,0)
{ }

Dualquat::Dualquat(Dualquat const & other)
    : rot(other.rot)
    , tran(other.tran)
{ }

Dualquat& Dualquat::operator=(const Dualquat & other)
{
    rot = other.rot;
    tran = other.tran;

    return *this;
}

Dualquat::Dualquat( const float real[4], const float dual[4] )
    : rot(real[0],real[1],real[2],real[3])
    , tran(dual[0],dual[1],dual[2],dual[3])
{ }

Dualquat::Dualquat(const Quat& _rot, const Quat& _tran)
    : rot(_rot)
    , tran(_tran)
{ }

Dualquat::Dualquat(const Quat& _rot)
    : rot(_rot)
    , tran()
{ }

Dualquat::Dualquat(const Mat4f& mat )
    : rot(mat)
    , tran( GetDual_RotTran(mat.GetTranslation()) )
{ }

Dualquat::Dualquat(const Vec3f& translation, const Quat& _rot )
    : rot(_rot)
    , tran(GetDual_RotTran( translation ))
{ }

Dualquat::Dualquat(const Vec3f& translation )
    : rot()
    , tran(GetDual_RotTran( translation ))
{ }

Dualquat::Dualquat(const Vec3f& translation, const Mat3f& mat )
    : rot(mat)
    , tran(GetDual_RotTran( translation ))
{ }

void Dualquat::SetFromMatrix( const Mat4f& mat ) {
    rot.SetFromMatrix(mat);
    CalcDual_RotTran(mat.GetTranslation());
}

void Dualquat::Normalize( void ) {
    
    float mag_sq = rot.SquaredLen();
    
    while( !Maths::Approxf( mag_sq, 1.0f, EP_TEN_THOUSANDTH ) ) {

        const float mag = sqrtf( mag_sq );
        
        rot /= mag;
        tran /= mag;
        
        mag_sq = rot.SquaredLen();
    }
}

void Dualquat::Print( void ) const {
    Printf("rot : %f,%f,%f,%f\n", static_cast<double>( rot.x ), static_cast<double>( rot.y ), static_cast<double>( rot.z ), static_cast<double>( rot.w ) );
    Printf("tran : %f,%f,%f,%f\n", static_cast<double>( tran.x ), static_cast<double>( tran.y ), static_cast<double>( tran.z ), static_cast<double>( tran.w ) );
}

void Dualquat::BecomeConjugate( void ) {
    rot.BecomeConjugate();
    tran.BecomeConjugate();
}

Quat Dualquat::GetDual_TranRot(const Vec3f& translation, const Quat& _rot) {
    return _rot * Quat(translation.x/2,translation.y/2,translation.z/2,0);
}

Quat Dualquat::GetDual_RotTran(const Vec3f& translation, const Quat& _rot) {
    return (Quat(translation.x,translation.y,translation.z,0.0f) * _rot ) * 0.5f;
}

Quat Dualquat::GetDual_TranRot(const Vec3f& translation) const {
    return GetDual_TranRot( translation, rot );
}

Quat Dualquat::GetDual_RotTran(const Vec3f& translation) const {
    return GetDual_RotTran( translation, rot );
}

void Dualquat::CalcDual_TranRot(const Vec3f& translation) {
    tran = GetDual_TranRot( translation );
}

void Dualquat::CalcDual_RotTran(const Vec3f& translation) {
    tran = GetDual_RotTran( translation );
}

//Isolate translation from DualQuat = 2 * Qd * Qr'
Vec3f Dualquat::GetTranslation( void ) const {
    Quat conjugate(rot);
    conjugate.BecomeConjugate();
    
    const Quat q_tran = ( tran * 2 ) * conjugate;
    
    return {q_tran.x, q_tran.y, q_tran.z};
}

Dualquat& Dualquat::operator+=(const Dualquat& other ) {
    rot += other.rot;
    tran += other.tran;
    return *this;
}

Dualquat Dualquat::operator+(const Dualquat& other ) const {
    return {
        rot + other.rot
        , tran + other.tran
    };
}

//Order of transformation in q1 * q2 is (out * other)
Dualquat Dualquat::operator*( const Dualquat& other ) const {
    Dualquat tmpDQ(
        rot * other.rot
        , rot * other.tran
    );

    tmpDQ.tran += tran * other.rot;
    tmpDQ.Normalize();
    return tmpDQ;
}

Vec3f& operator*=(Vec3f& vec, const Dualquat& multiplier ) {
    return vec = vec * multiplier;
}

//Applying dualquat to a vector, DQ * Vec3 * DQ'
Vec3f operator*(const Vec3f& vec, Dualquat multiplier_byVal ) {
    multiplier_byVal.Normalize();

    const Quat vecQ(vec.x,vec.y,vec.z,0);
    Quat thisConj( multiplier_byVal.rot );
    thisConj.BecomeConjugate();

    const Quat tmp( multiplier_byVal.rot * vecQ );
    const Quat tmp2( tmp * thisConj );

    const Vec3f translate = multiplier_byVal.GetTranslation();

    return {
        tmp2.x + translate.x
        , tmp2.y + translate.y
        , tmp2.z + translate.z
    };
}

//Scalar Multiply
Dualquat& Dualquat::operator*=( float val ) {
    return *this = *this * val;
}

Dualquat Dualquat::operator*( float val ) const {
    return {
        rot * val,
        tran * val
    };
}

float Dualquat::Dot(const Dualquat& other) const {
    return rot.w * other.rot.w + rot.x * other.rot.x + rot.y * other.rot.y + rot.z * other.rot.z;
}

Dualquat Dualquat::GetInterpolated( const Dualquat& to, float delta ) const {
    return GetInterpolated( *this, to, delta );
}

Dualquat Dualquat::GetInterpolated( const Dualquat& from, const Dualquat& to, float delta ) {
    const float dot = from.Dot(to);

    Dualquat modified(to);
    if (dot < 0.0f ) {
        // the rotation will take the "longest route"
        // negate the values to make it take the "shortest route"
        modified.rot.x *= -1;
        modified.rot.y *= -1;
        modified.rot.z *= -1;
        modified.rot.w *= -1;
    }

    const float deltaReciprocal = 1.0f - delta;
    Dualquat ret( ( from * deltaReciprocal ) + (modified * delta) );
    ret.Normalize();
    return ret;
}

bool Dualquat::operator==( const Dualquat& other ) const {
    return( Maths::Approxf(rot.x, other.rot.x)
            && Maths::Approxf(rot.y, other.rot.y)
            && Maths::Approxf(rot.z, other.rot.z)
            && Maths::Approxf(rot.w, other.rot.w)
            && Maths::Approxf(tran.x, other.tran.x)
            && Maths::Approxf(tran.y, other.tran.y)
            && Maths::Approxf(tran.z, other.tran.z)
            && Maths::Approxf(tran.w, other.tran.w)
           );
}

#ifdef CLIB_UNIT_TEST

    void DualQuat_UnitTest_Normalizing();
    void DualQuat_UnitTest_Normalizing( void ) {
        
        const Vec3f v_trans( 0.5f, -10, 3 );
        const Quat q_rot( 0.38178005814552307f, -0.07785799354314804f, 0.039777547121047974f, 0.9201086163520813f ); // Rot_Degrees(45,18,35) using 'ZYX' rotation
        const Quat q_rot_scaled( Quat( 1.1453402042388916f, -0.23357395827770233f, 0.11933264136314392f, 2.7603259086608887f ) * 3.2f );
        
        const Dualquat dq_scaled( v_trans, q_rot_scaled );
        
        const Dualquat dq_nonScaled( v_trans, q_rot );
        
        const float dq_nonScaled_rot_mag = dq_nonScaled.rot.Len();
        const float dq_nonScaled_rot_mag_sq = dq_nonScaled.rot.SquaredLen();
        
        ASSERT( Maths::Approxf( dq_nonScaled_rot_mag, dq_nonScaled_rot_mag_sq ) );
        
        Dualquat b( dq_scaled );
        b.Normalize();
        
        const float b_rot_mag = b.rot.Len();
        const float b_rot_mag_sq = b.rot.SquaredLen();
        
        ASSERT( Maths::Approxf( b_rot_mag, b_rot_mag_sq ) );
        ASSERT( Maths::Approxf( b_rot_mag, dq_nonScaled_rot_mag_sq ) );
        
        const Vec3f b_translation = b.GetTranslation();
        
        ASSERT( b_translation == v_trans );
    
        Dualquat b_conjugate = b;
        b_conjugate.BecomeConjugate();
        
        const Dualquat b_conjugate_test = b * b_conjugate;
        const Dualquat dq_identity;
        
        ASSERT( b_conjugate_test == dq_identity );
        
    }

    void DualQuat_UnitTest_Construction();
    void DualQuat_UnitTest_Construction( void ) {
        //Applying 45 deg rotation to a 45 deg rotation, no translation
            const float real[4] {0.3826834f, 0, 0, 0.9238795f};  //45 Degreees on X
            const float dual_none[4] {0,0,0,0};

            const Dualquat a(real, dual_none );
            const Dualquat b( a );
            const Dualquat c( b * a );
            const float real2[4] {0.7071068f, 0.0f, 0.0f,0.7071068f};  //90 Degrees on X
            const Dualquat trueAns( real2, dual_none );
            ASSERT( c == trueAns );
        
        //Dualquat from Translation Vector and Rotation Quaternion
            const Vec3f v_translate( 1.0f,2.0f,-3.0f );
            const Quat q_rot_XYZ( 0.4039383f,0.0241791f,0.3314897f,0.8522698f ); // Rot_Degrees(45,18,35) using 'XYZ' rotation
            
            const Dualquat d( v_translate , q_rot_XYZ );  // Rot(45,18,35) Translate(1,2,-3)
            Dualquat e( d.rot, Quat(0.0f,0.0f,0.0f,0.0f));
            e.CalcDual_RotTran( v_translate );

            ASSERT( d == e);
    }

    void DualQuat_UnitTest_Transformation();
    void DualQuat_UnitTest_Transformation( void ) {
        
        //Note : Combining dualquaternions, like combining matrices is done right to left ( i.e. dq_trans * dq_rot = rotation followed by translation )
        
        const Vec3f v_translate( 1.0f,2.0f,-3.0f );
        const Quat q_rot_XYZ( 0.4039383f,0.0241791f,0.3314897f,0.8522698f ); // Rot_Degrees(45,18,35) using 'XYZ' rotation
        
        //Combining DualQuats
            const Dualquat dq_rot( Vec3f(), q_rot_XYZ ); // Rot_Degrees(45,18,35) using 'XYZ' Rotation
            const Dualquat dq_trans( v_translate, Quat() );  // Translate(1,2,-3)
            Dualquat dq_trans_calcDual( Quat( 0,0,0,1 ), Quat( 0,0,0,0 ) ); //Identity
            dq_trans_calcDual.CalcDual_RotTran( v_translate );
        
        //DQ( Rotation, No Translation ) * DQ( No Rotation, Translation )
            //Note : This is translating, then rotating

            const Dualquat dq_answer_rot_times_trans( Quat( 0.4039383f, 0.0241791f, 0.3314897f, 0.8522698f ), Quat( 0.05837655f, 1.6239221f, -0.8865559f, 0.2710863f ) );
            const Dualquat rot_times_trans = dq_rot * dq_trans;
            
            ASSERT( rot_times_trans == dq_answer_rot_times_trans );

        //DQ( No Rotation, Translation ) * DQ( Rotation, No Translation )
            
            const Dualquat dq_answer_trans_times_rot( Quat( 0.4039383f, 0.0241791f, 0.3314897f, 0.8522698f ), Quat( 0.79389325f, 0.0806175f, -1.6702534f, 0.2710863f ) );
            const Dualquat trans_times_rot = dq_trans * dq_rot;

            ASSERT( trans_times_rot == dq_answer_trans_times_rot );
        
        //Moving a point with : DQ( Rotation, No Translation ) * DQ( No Rotation, Translation )
            //Caution : This is translating, then rotating (non-standard use)
            const Vec3f vec_rot_trans = Vec3f() * rot_times_trans;  //This result is a ZYX rotation in Blender, Blender is 'backwards'
            const Vec3f rot_trans_extracted_trans = rot_times_trans.GetTranslation();  //ZYX rotation in Blender
            
            const Vec3f vec_rot_trans_answer( -1.23899829f, 3.50986075f, -0.38178879f );
            
            ASSERT( vec_rot_trans == vec_rot_trans_answer );
            ASSERT( rot_trans_extracted_trans == vec_rot_trans_answer );
        
        //Moving a point with :  DQ( No Rotation, Translation ) * DQ( Rotation, No Translation )
            const Vec3f vec_trans_rot = Vec3f() * trans_times_rot;
            const Vec3f trans_rot_extracted_trans = trans_times_rot.GetTranslation();
            
            ASSERT( vec_trans_rot == v_translate );
            ASSERT( trans_rot_extracted_trans == v_translate );
        
        //Using CalcDual() = No Change because dq_trans and dq_trans_calcDual have no rotation
            //DQ_Rot * DQ_Trans - Caution : This is translating, then rotating (non-standard use)
            Dualquat rot_times_trans_calcDual( q_rot_XYZ, Quat(0,0,0,0 ) );
            rot_times_trans_calcDual = rot_times_trans_calcDual * dq_trans_calcDual;
            
            const Vec3f vec_rot_trans_calcDual = Vec3f() * rot_times_trans_calcDual; 
            const Vec3f rot_trans_calcDual_extracted_trans = rot_times_trans_calcDual.GetTranslation();
            
            ASSERT( vec_rot_trans_calcDual == vec_rot_trans_answer );
            ASSERT( rot_trans_calcDual_extracted_trans == vec_rot_trans_answer );
            
            // DQ_Trans * DQ_Rot
            Dualquat trans_times_rot_calcDual( q_rot_XYZ, Quat(0,0,0,0 ) );
            trans_times_rot_calcDual = dq_trans_calcDual * trans_times_rot_calcDual;
        
            const Vec3f vec_trans_rot_calcDual = Vec3f() * trans_times_rot_calcDual; 
            const Vec3f trans_rot_calcDual_extracted_trans = trans_times_rot_calcDual.GetTranslation();
            
            ASSERT( vec_trans_rot_calcDual == v_translate );
            ASSERT( trans_rot_calcDual_extracted_trans == v_translate );
        
        //Combining Separate DualQuats : 1 TransRot * 1 TransRot
            const Dualquat dq_combined_answer( Quat( 0.6885288f, 0.04121423f, 0.5650373f, 0.4527276f ), Quat( 1.572226761148f, 0.150524966716f, -2.667288429f, 0.92415463358f ) );
            const Vec3f vec_moved_point_after_dq_combined_transform( -0.23899835348129272f, 5.509860515594482f, -3.381788969039917f);
                      
            const Dualquat dq_combined_trans_rot = trans_times_rot * trans_times_rot;
            
            ASSERT( dq_combined_trans_rot == dq_combined_answer );
            
            const Vec3f vec_dq_combined = Vec3f() * dq_combined_trans_rot;
            
            ASSERT( vec_dq_combined == vec_moved_point_after_dq_combined_transform );
    }

    void DualQuat_UnitTest_Conjugates();
    void DualQuat_UnitTest_Conjugates( void ) {
         
        //Conjugate Multiplication, ( DQ_1 * DQ_2 ) * inverse_DQ_2 == DQ_1
        const Vec3f vec_translation( 2.0f, 14.0f, -4.0f );
        const Dualquat d_orig( vec_translation, Quat(0.4039383f,0.0241791f,0.3314897f,0.8522698f) ); // Rot(45,18,35) Translate(1,2,3)
        const Dualquat dq_transform( vec_translation, Quat( 0.3826834f, 0, 0, 0.9238795f ) ); //45 Degrees on X, Translate(2,14,-4)
        Dualquat inv_dq_transform( dq_transform );
        inv_dq_transform.BecomeConjugate();
        
        const Dualquat dq_identity;
        const Dualquat dq_identity_test = dq_transform * inv_dq_transform;
        const Dualquat dq_identity_test2 = inv_dq_transform * dq_transform; //Commutativity
        ASSERT( dq_identity_test == dq_identity );
        ASSERT( dq_identity_test2 == dq_identity );
        
        Dualquat dq_conj_test;
        dq_conj_test = d_orig * dq_transform;
        dq_conj_test = dq_conj_test * inv_dq_transform;
        
        dq_conj_test.Normalize();
        
        ASSERT( dq_conj_test == d_orig );
    }
    
    void DualQuat_UnitTest_GetTranslation();
    void DualQuat_UnitTest_GetTranslation( void ) {
        //Getting the translation out of a DualQuat
        const Vec3f vec_translation( 2.0f, 14.0f, -4.0f );
        const Dualquat quat_test( vec_translation, Quat(0.4039383f,0.0241791f,0.3314897f,0.8522698f) ); // Rot(45,18,35)
        
        const Vec3f dq_translation = quat_test.GetTranslation();
        
        ASSERT( dq_translation == vec_translation );
    }
    
    void DualQuat_UnitTest_DQ_from_Matrix_Conversion();
    void DualQuat_UnitTest_DQ_from_Matrix_Conversion( void ) {
        const Vec3f v_translate( 1.0f,2.0f,-3.0f );
        const Quat q_rot_XYZ( 0.4039383f,0.0241791f,0.3314897f,0.8522698f ); // Rot_Degrees(45,18,35) using 'XYZ' rotation
        const Dualquat d( v_translate , q_rot_XYZ );  // Rot(45,18,35) Translate(1,2,-3)
        
        Mat4f m1;
        Dualquat dq_fromMat;
        m1.Rotate(45.0f,1.0f,0.0f,0.0f);
        m1.Rotate(18.0f,0.0f,1.0f,0.0f);
        m1.Rotate(35.0f,0.0f,0.0f,1.0f);
        m1.SetTranslation( v_translate );

        dq_fromMat = dq_fromMat.FromMatrix(m1);

        ASSERT( d == dq_fromMat );
    }

    void Dualquat::UnitTest( void ) {
        
        DualQuat_UnitTest_Construction();
        DualQuat_UnitTest_Normalizing();
        DualQuat_UnitTest_Transformation();
        DualQuat_UnitTest_Conjugates();
        DualQuat_UnitTest_GetTranslation();
        DualQuat_UnitTest_DQ_from_Matrix_Conversion();
         
    }

#endif // CLIB_UNIT_TEST
