// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_VEC_H_2T_DEC
#define SRC_CLIB_MATH_VEC_H_2T_DEC

#include <cmath>

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./values.h"

namespace String {
    bool ToVec2f( const std::string& str, Vec2f& out );
    Vec2f ToVec2f( const std::string& str );
    
    template< typename TYPE >
    TYPE To( const std::string& str );

    template< typename TYPE, uint NUM_COMMAS >
    bool ToVec( const std::string& vecstr, TYPE& out );
}

template<typename TYPE>
class Vec2t {
public:
    Vec2t<TYPE>( void );
    Vec2t<TYPE>( const Vec2t<TYPE> &other ) = default;
    Vec2t<TYPE>( const TYPE xx, const TYPE yy );
    explicit Vec2t<TYPE>( const TYPE all );

    Vec2t<TYPE>& operator=( const Vec2t<TYPE> &other ) = default;

    Vec2t<TYPE> operator-(void) const;
    Vec2t<TYPE> operator-( const Vec2t<TYPE> &other ) const;
    Vec2t<TYPE> operator-( const TYPE val ) const;
    Vec2t<TYPE>& operator-=( const Vec2t<TYPE> &other );
    Vec2t<TYPE>& operator-=( const TYPE val );

    Vec2t<TYPE> operator+( const Vec2t<TYPE> &other ) const;
    Vec2t<TYPE> operator+( const TYPE ) const;
    Vec2t<TYPE>& operator+=( const Vec2t<TYPE> &other );
    Vec2t<TYPE>& operator+=( const TYPE val );

    Vec2t<TYPE> operator*( const Vec2t<TYPE> &other ) const;
    Vec2t<TYPE> operator*( const TYPE val ) const;
    Vec2t<TYPE>& operator*=( const Vec2t<TYPE> &other );
    Vec2t<TYPE>& operator*=( const TYPE val );

    Vec2t<TYPE> operator/( const Vec2t<TYPE> &other ) const;
    Vec2t<TYPE> operator/( const TYPE val ) const;
    Vec2t<TYPE>& operator/=( const Vec2t<TYPE> &other );
    Vec2t<TYPE>& operator/=( const TYPE val );

    bool operator==( const Vec2t<TYPE> &b ) const;
    bool operator==( const TYPE b ) const;
    bool operator!=( const Vec2t<TYPE> &b ) const;
    bool operator!=( const TYPE b ) const;

    TYPE operator[] ( const std::size_t index ) const;
    TYPE& operator[] ( const std::size_t index );

    void Zero( void );

    Vec2t<TYPE> PerpCCW( void ) const;
    Vec2t<TYPE> PerpCW( void ) const;

    Vec2t<TYPE>& Set( const TYPE a, const TYPE b );

    TYPE Cross( const Vec2t<TYPE> &other ) const;
    TYPE Dot( const Vec2t<TYPE> &other ) const;
    Vec3t<TYPE> ToVec3( void ) const;

    void Normalize( void );
    NODISCARD Vec2f GetNormalized( void );
    float Len( void ) const; //!< not as fast as SquaredLen() due to sqrtf call
    float SquaredLen( void ) const; //!< faster than Len() due to no sqrtf call
    static float SquaredLen( const Vec2t<TYPE> &vec ); //!< faster than Len() due to no sqrtf call
    static float Len( const Vec2t<TYPE> &vec );

public:
    TYPE x,y;
};

template<typename TYPE>
class Vec2t;

typedef Vec2t<float> Vec2f;

#endif  //SRC_CLIB_MATH_VEC2T_DEC
