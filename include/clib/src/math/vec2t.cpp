// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./vec2t.h"
#include "./strings.h"

template class Vec2t<float>;

namespace String {

    bool ToVec2f( const std::string& vecstr, Vec2f& out ) {
        return ToVec< Vec2f, 1 >(vecstr, out);
    }

    Vec2f ToVec2f( const std::string& str ) {
        Vec2f ret;
        ToVec2f( str, ret );
        return ret;
    }
}
