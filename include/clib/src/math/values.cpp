// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./values.h"
#include "./vec2t-dec.h"
#include "./vec2t-dec.h"

template bool Maths::IsPrime< signed int >( const signed int num );
template bool Maths::IsPrime< unsigned int >( const unsigned int num );

template bool Maths::IsPowerOf2( const Uint16 x );

bool Maths::Approxf( const Vec3f& a, const Vec3f& b, const float range ) {
    return (
        Approxf( a.x, b.x, range ) &&
        Approxf( a.y, b.y, range ) &&
        Approxf( a.z, b.z, range )
    );
}

bool Maths::Approxf( const Vec2f& a, const Vec2f& b, const float range ) {
    return (
        Approxf( a.x, b.x, range ) &&
        Approxf( a.y, b.y, range )
    );
}

bool Maths::Approxf( const float a, const float b, const float range ) {
    return ( fabsf(a-b) < range );
}


#ifdef CLIB_UNIT_TEST
    void Maths::UnitTest_Math( void ) {
        UnitTest_IsPrime();
    }
    
    void Maths::UnitTest_IsPrime( void ) {
        const std::vector< int > primes({2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199});
        std::vector< int > nonprimes;
        for ( int i=2; i<199; ++i )
            if ( !Contains(primes, i ) )
                nonprimes.push_back( i );
        
        for ( int prime : primes )
            ASSERT( Maths::IsPrime( prime ) );
        
        for ( int nonprime : nonprimes )
            ASSERT( !Maths::IsPrime( nonprime ) );
    }

#endif // CLIB_UNIT_TEST
