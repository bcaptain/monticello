// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./mat.h"

namespace String {
    std::string Mat3fToString( const Mat3f& m ) {
        return
            std::to_string(m.M(0,0))+','+std::to_string(m.M(0,1))+','+std::to_string(m.M(0,2))+
            std::to_string(m.M(1,0))+','+std::to_string(m.M(1,1))+','+std::to_string(m.M(1,2))+
            std::to_string(m.M(2,0))+','+std::to_string(m.M(2,1))+','+std::to_string(m.M(2,2))
        ;
    }
}

Vec3f Mat3f::VecTimesMat3( const Vec3f& vec ) const {
    // data[0], [1], and [2] are always the same regardless of whether a mat3 or mat4, so no need to call M()
    return {
        M(0,0)*vec.x + M(1,0)*vec.y + M(2,0)*vec.z
        , M(0,1)*vec.x + M(1,1)*vec.y + M(2,1)*vec.z
        , M(0,2)*vec.x + M(1,2)*vec.y + M(2,2)*vec.z
    };
}

Vec3f Mat3f::operator*( const Vec3f& vec ) const {
    // data[0], [1], and [2] are always the same regardless of whether a mat3 or mat4, so no need to call M()
    return {
        M(0,0)*vec.x + M(0,1)*vec.y + M(0,2)*vec.z
        , M(1,0)*vec.x + M(1,1)*vec.y + M(1,2)*vec.z
        , M(2,0)*vec.x + M(2,1)*vec.y + M(2,2)*vec.z
    };
}

Mat3f::Mat3f( float* float_array_to_acquire, const std::size_t array_size )
    : data( float_array_to_acquire )
    , dim( array_size )
{ }

// loads identity by default
Mat3f::Mat3f( void )
    : data( new float[9] {
            1,0,0,
            0,1,0,
            0,0,1
      } )
    , dim(3)
    {
}

Mat3f::Mat3f( const Vec3f& from_angles_ZYX )
    : Mat3f()
{
    RotateZYX( from_angles_ZYX );
}

Mat3f::~Mat3f( void ) {
    DELNULLARRAY( data );
}

#ifdef TMP
    #error "macro TMP was already defined"
#endif //TMP

#define TMP(x) static_cast< const float >( mat[(x)] )
Mat3f::Mat3f( const double mat[9] )
    : data ( new float[9]{
            // column 0
            TMP(0),
            TMP(1),
            TMP(2),
            
            // column 1
            TMP(3),
            TMP(4),
            TMP(5),
            
            // column 2
            TMP(6),
            TMP(7),
            TMP(8)
      } )
    , dim(3)
{
}
#undef TMP

Mat3f::Mat3f( const float mat[9] )
    : data( new float[9]{
            // column 0
            mat[0],
            mat[1],
            mat[2],
            
            // column 1
            mat[3],
            mat[4],
            mat[5],
            
            // column 2
            mat[6],
            mat[7],
            mat[8]
      } )
    , dim(3)
{
}

Mat3f::Mat3f( const Mat4f& mat )
    : data ( new float[9] {
            // column 0
            mat.M(0,0),
            mat.M(0,1),
            mat.M(0,2),

            // column 1
            mat.M(1,0),
            mat.M(1,1),
            mat.M(1,2),

            // column 2
            mat.M(2,0),
            mat.M(2,1),
            mat.M(2,2)
    } )
    , dim(3)
{
}

Mat3f::Mat3f( Mat3f&& other_rref )
    : data( other_rref.data )
    , dim( 3 )
{
    ASSERT( other_rref.dim == 3 );
    other_rref.data = nullptr;
}

Mat3f::Mat3f( const Mat3f& mat )
    : data ( new float[9] {
            // col0
            mat.M(0,0),
            mat.M(0,1),
            mat.M(0,2),

            // col1
            mat.M(1,0),
            mat.M(1,1),
            mat.M(1,2),

            // col2
            mat.M(2,0),
            mat.M(2,1),
            mat.M(2,2)
      } )
    , dim(3)
{
}

Mat3f::Mat3f( const Quat& quat)
    : Mat3f()
{
    float two_xy(2 * quat.x * quat.y)
        , two_wz(2 * quat.w * quat.z)
        , two_xz(2 * quat.x * quat.z)
        , two_yz(2 * quat.y * quat.z)
        , two_wx(2 * quat.w * quat.x)
        , two_wy(2 * quat.w * quat.y);

    Quat twoSq( 2 * quat.x * quat.x
         , 2 * quat.y * quat.y
         , 2 * quat.z * quat.z
         , 2 *quat.w * quat.w);

    M(0,0) = 1 - twoSq.y - twoSq.z;
    M(0,1) = two_xy + two_wz;
    M(0,2) = two_xz - two_wy;
    
    M(1,0) = two_xy - two_wz;
    M(1,1) = 1 - twoSq.x - twoSq.z;
    M(1,2) = two_yz + two_wx;
    
    M(2,0) = two_xz + two_wy;
    M(2,1) = two_yz - two_wx;
    M(2,2) = 1 - twoSq.x - twoSq.y;
}

void Mat3f::Print( void ) const {
    for ( uint r=0; r<dim; ++r ) {
        for ( uint c=0; c<dim; ++c ) {
            Printf("%f,", static_cast<double>( M(c,r) ) );
        }
        Printf("\n");
    }
}

void Mat3f::LoadIdentity( void ) {
    // floats promoted to double in variadic funcs
    const std::initializer_list<float> val {
        1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        0.0, 0.0, 1.0
    };
    std::copy( val.begin(), val.end(), data );
}

Mat3f& Mat3f::operator=( const Mat3f& other ) {
    std::size_t c,r;
    for ( r=0; r<3; ++r )
        for ( c=0; c<3; ++c )
            M(c,r) = other.M(c,r);

    return *this;
}

Mat3f& Mat3f::operator=( Mat3f&& other_rref ) {
    ASSERT( dim == 3 );
    ASSERT( other_rref.dim == 3 );
    delete[] data;
    data = other_rref.data;
    return *this;
}

Mat3f& Mat3f::operator=( const double mat[9] ) {
    ASSERT_MSG(dim == 3, "operator= on matrices of different dimensions.\n");

    for ( std::size_t x=0; x<9; ++x )
        data[x] = static_cast<float>( mat[x] );

    return *this;
}

Mat3f& Mat3f::operator=( const float mat[9] ) {
    ASSERT_MSG(dim == 3, "operator= on matrices of different dimensions.\n");

    for ( std::size_t x=0; x<9; ++x )
        data[x] = mat[x];

    return *this;
}

void Mat3f::Multiply( const Mat3f& mat, Mat3f& out ) const {
    ASSERT( &mat != &out );

    for ( std::size_t c=0; c<3; ++c ) {
        out.M(c,0) = M(0,0) * mat.M(c,0) + M(1,0) * mat.M(c,1) + M(2,0) * mat.M(c,2);
        out.M(c,1) = M(0,1) * mat.M(c,0) + M(1,1) * mat.M(c,1) + M(2,1) * mat.M(c,2);
        out.M(c,2) = M(0,2) * mat.M(c,0) + M(1,2) * mat.M(c,1) + M(2,2) * mat.M(c,2);
    }
}

Mat3f& Mat3f::operator*=( const float multiplier ) {
    std::size_t col, row;
    for ( col = 0; col < 3; ++col )
        for ( row = 0; row < 3; ++row )
            M(col,row) *= multiplier;

    return *this;
}

bool Mat3f::Invert( void ) {
    // reference material: Inverse of a Matrix using Minors, Cofactors and Adjugate.pdf
    
    Mat3f mat;

    ComputeMinors( mat );
    mat.ComputeCofactors();
    
    const float det = ComputeDeterminant( mat );
    if ( Maths::Approxf( det, 0.0f, EP_MIL ) )
        return false;
    const float det_reciprocol = 1.0f / det;
    
    mat.Transpose(); // mat is now the adjugate
    
    for (uint c = 0; c < 3; ++c)
        for (uint r = 0; r < 3; ++r)
            M(c,r) = mat.M(c,r) * det_reciprocol;
    
    return true;
}

void Mat3f::Transpose( void ) {
    std::swap( M(0,1), M(1,0) );
    std::swap( M(0,2), M(2,0) );
    std::swap( M(1,2), M(2,1) );
}

void Mat3f::Rotate( const float a_degree, float x, float y, float z ) {
    Mat3f temp_object(*this);
    DoRotation( a_degree, x, y, z, temp_object ); // because we can't construct from derived object ( Mat3f::Mat3f( Mat4 ) )
}

void Mat3f::RotateXYZ( const Vec3f angles ) {
    Rotate( angles.x, 1.0, 0.0, 0.0 );
    Rotate( angles.y, 0.0, 1.0, 0.0 );
    Rotate( angles.z, 0.0, 0.0, 1.0 );
}

void Mat3f::RotateZYX( const Vec3f angles ) {
    Rotate( angles.z, 0.0, 0.0, 1.0 );
    Rotate( angles.y, 0.0, 1.0, 0.0 );
    Rotate( angles.x, 1.0, 0.0, 0.0 );
}

void Mat3f::DoRotation( const float a_degree, float x, float y, float z, Mat3f& temp_object ) {
    if ( Maths::Approxf( a_degree, 0.0f ) )
        return;

    const float rot_radians = a_degree * DEG_TO_RAD_F;
    float c = cosf(rot_radians);
    float s = sinf(rot_radians);
    Mat3f two;

    // **** perform simple rotation on an individual axis

    if ( Maths::Approxf( x, 0.0f ) ) {
        if ( Maths::Approxf( y, 0.0f ) ) {
            if ( !Maths::Approxf( z, 0.0f ) ) {
                two.M(0,0) = c;
                two.M(1,1) = c;
                if ( z < 0.0f ) {
                    two.M(1,0) = s;
                    two.M(0,1) = -s;
                } else {
                    two.M(0,1) = s;
                    two.M(1,0) = -s;
                }
                temp_object.Multiply(two,*this);
            }

            return;
        } else {
            if ( Maths::Approxf( z, 0.0f ) ) {
                two.M(0,0) = c;
                two.M(2,2) = c;
                if ( y < 0.0f ) {
                    two.M(2,0) = -s;
                    two.M(0,2) = s;
                } else {
                    two.M(0,2) = -s;
                    two.M(2,0) = s;
                }

                temp_object.Multiply(two,*this);
                return;
            }
        }
    } else {
        if ( Maths::Approxf( y, 0.0f ) && Maths::Approxf( z, 0.0f ) ) {
            two.M(1,1) = c;
            two.M(2,2) = c;
            if ( x < 0.0f ) {
                two.M(2,1) = s;
                two.M(1,2) = -s;
            } else {
                two.M(1,2) = s;
                two.M(2,1) = -s;
            }

            temp_object.Multiply(two,*this);
            return;
        }
    }

    // **** rotate on an arbitrary axis
    const float len = sqrtf(x*x + y*y + z*z);

    if (len < EP_TEN_THOUSANDTH )
        return;

    // normalize the vector
    x /= len;
    y /= len;
    z /= len;

    const float xx = x*x;
    const float yy = y*y;
    const float zz = z*z;
    const float C = 1.0f-c;

    two.M(0,0)=xx+(yy+zz)*c;
    two.M(0,1)=x*y*C-z*s;
    two.M(0,2)=x*z*C+y*s;

    two.M(1,0)=x*y*C+z*s;
    two.M(1,1)=yy+(xx+zz)*c;
    two.M(1,2)=y*z*C-x*s;

    two.M(2,0)=x*z*C-y*s;
    two.M(2,1)=y*z*C+x*s;
    two.M(2,2)=zz+(xx+yy)*c;

    temp_object.Multiply(two,*this);
}

Mat3f& Mat3f::operator*=( const Mat3f& multiplier ) {
    Mat3f tmp;
    Multiply(multiplier, tmp);
    return *this = tmp;
}

void Mat3f::ComputeCofactors( void ) {
    M(1,0) = -M(1,0);
    M(0,1) = -M(0,1);
    M(2,1) = -M(2,1);
    M(1,2) = -M(1,2);
}

void Mat3f::ComputeMinors( Mat3f& out ) const {
    // left col
    out.M(0,0)=M(1,1)*M(2,2)-M(2,1)*M(1,2);
    out.M(0,1)=M(1,0)*M(2,2)-M(2,0)*M(1,2);
    out.M(0,2)=M(1,0)*M(2,1)-M(2,0)*M(1,1);
    
    // middle col
    out.M(1,0)=M(0,1)*M(2,2)-M(2,1)*M(0,2);
    out.M(1,1)=M(0,0)*M(2,2)-M(2,0)*M(0,2);
    out.M(1,2)=M(0,0)*M(2,1)-M(2,0)*M(0,1);
    
    // right col
    out.M(2,0)=M(0,1)*M(1,2)-M(1,1)*M(0,2);
    out.M(2,1)=M(0,0)*M(1,2)-M(1,0)*M(0,2);
    out.M(2,2)=M(0,0)*M(1,1)-M(1,0)*M(0,1);
}

float Mat3f::ComputeDeterminant( void ) const {
    return
        M(0,0)*M(1,1)*M(2,2) - M(0,2)*M(1,1)*M(2,0) +
        M(1,0)*M(2,1)*M(0,2) - M(1,2)*M(2,1)*M(0,0) +
        M(2,0)*M(0,1)*M(1,2) - M(2,2)*M(0,1)*M(1,0);
}

float Mat3f::ComputeDeterminant( const Mat3f& minors ) const {
    return
        M(0,0) * minors.M(0,0) +
        M(1,0) * minors.M(1,0) +
        M(2,0) * minors.M(2,0);
}

void Mat3f::Scale( const float x, const float y, const float z ) {
    M(0,0) *= x;
    M(0,1) *= x;
    M(0,2) *= x;
    M(1,0) *= y;
    M(1,1) *= y;
    M(1,2) *= y;
    M(2,0) *= z;
    M(2,1) *= z;
    M(2,2) *= z;
}

bool Mat3f::operator==( const Mat3f& other ) const {
    return Approx( other, EP_TEN_THOUSANDTH );
}

bool Mat3f::Approx( const Mat3f& other, const float epsilon ) const {
    std::size_t c,r;
    for ( r=0; r<3; ++r )
        for ( c=0; c<3; ++c )
            if ( ! Maths::Approxf( M(c,r), other.M(c,r), epsilon ) )
                return false;

    return true;
}

float Mat3f::M( const std::size_t col, const std::size_t row ) const {
    ASSERT( col < dim && row < dim );
    return data[row+col*dim];
}

float& Mat3f::M( const std::size_t col, const std::size_t row ) {
    ASSERT( col < dim && row < dim );
    return data[row+col*dim];
}

void Mat3f::Scale( const float xyz ) {
    return Scale( xyz, xyz, xyz );
}

#ifdef CLIB_UNIT_TEST

    void Mat3f::UnitTest( void ) {
        Mat3f::UnitTest_OperatorEqual();
        Mat3f::UnitTest_M();
        Mat3f::UnitTest_Multiply();
        Mat3f::UnitTest_Ctor();
        Mat3f::UnitTest_Rotate();
        Mat3f::UnitTest_Transpose();
        Mat3f::UnitTest_ComputeMinors();
        Mat3f::UnitTest_ComputeCofactors();
        Mat3f::UnitTest_ComputeDeterminant();
        Mat3f::UnitTest_Invert();
    }

    void Mat3f::UnitTest_Multiply( void ) {
        const float tmp[9]{1,2,3,4,5,6,7,8,9};
        const Mat3f a( tmp );
        
        const float tmp2[9]{95,90,85,80,75,70,65,60,55};
        const Mat3f multiplicand(tmp2);
        const float tmp3[9]{1050,1320,1590,870,1095,1320,690,870,1050};
        const Mat3f mult_result_should_be(tmp3);

        Mat3f result;
        a.Multiply( multiplicand, result );
        ASSERT( result.Approx(mult_result_should_be) );
    }

    void Mat3f::UnitTest_M( void ) {
        float test_data[9]{0,1,2,3,4,5,6,7,8};
        Mat3f test(test_data);

        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wfloat-equal"
            ASSERT( test.M(0,0) == 0 );
            ASSERT( test.M(0,1) == 1 );
            ASSERT( test.M(0,2) == 2 );
            ASSERT( test.M(1,0) == 3 );
            ASSERT( test.M(1,1) == 4 );
            ASSERT( test.M(1,2) == 5 );
            ASSERT( test.M(2,0) == 6 );
            ASSERT( test.M(2,1) == 7 );
            ASSERT( test.M(2,2) == 8 );
        #pragma GCC diagnostic pop

        Mat3f test2(test);
        
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wfloat-equal"
            ASSERT( test2.M(0,0) == 0 );
            ASSERT( test2.M(0,1) == 1 );
            ASSERT( test2.M(0,2) == 2 );
            ASSERT( test2.M(1,0) == 3 );
            ASSERT( test2.M(1,1) == 4 );
            ASSERT( test2.M(1,2) == 5 );
            ASSERT( test2.M(2,0) == 6 );
            ASSERT( test2.M(2,1) == 7 );
            ASSERT( test2.M(2,2) == 8 );
        #pragma GCC diagnostic pop
    }

    void Mat3f::UnitTest_OperatorEqual( void ) {
        // ** operator== and operator=
        float test_data[9]{0,1,2,3,4,5,6,7,8};
        Mat3f test(test_data);
        
        Mat3f test2(test);
        ASSERT( test2 == test );
    }

    void Mat3f::UnitTest_Ctor( void ) {
        const float tmp[9]{1,2,3,4,5,6,7,8,9};
        const Mat3f a( tmp );
        const Mat3f b(a);
        ASSERT( a.Approx(b) );

        const Mat3f c(a.data);
        ASSERT( a.Approx(c) );
    }

    void Mat3f::UnitTest_Rotate( void ) {
        Mat3f result;

        const float rot_x_this[9]{1,0,0,0,1,0,0,0,1};
        const float rot_x_result[9]{1,0,0,0,0,-1,0,1,0};
        Mat3f rot_x_mat( rot_x_this );
        result = rot_x_result;
        rot_x_mat.Rotate( -90, 1,0,0 ); // neg 90 around x axis
        ASSERT( rot_x_mat.Approx(result) );

        const float rot_y_this[9]{1,0,0,0,1,0,0,0,1};
        const float rot_y_result[9]{0,0,1,0,1,0,-1,0,0};
        Mat3f rot_y_mat( rot_y_this );
        result = rot_y_result;
        rot_y_mat.Rotate( -90, 0,1,0 ); // neg 90 around y axis
        ASSERT( rot_y_mat.Approx(result) );

        const float rot_z_this[9]{1,0,0,0,1,0,0,0,1};
        const float rot_z_result[9]{0,-1,0,1,0,0,0,0,1};
        Mat3f rot_z_mat( rot_z_this );
        result = rot_z_result;
        rot_z_mat.Rotate( -90, 0,0,1 ); // neg 90 around z axis
        ASSERT( rot_z_mat.Approx(result) );

        const Vec3f vec_x_180(0,1,0);
        Mat3f mat_x_180;
        mat_x_180.Rotate(180,1,0,0);
        ASSERT( vec_x_180 * mat_x_180 == Vec3f(0,-1,0) );
    }

    void Mat3f::UnitTest_Transpose( void ) {
        const float transpose_this[9]{0,1,2,3,4,5,6,7,8};
        const float expected_tran_data[9]{0,3,6,1,4,7,2,5,8};
        Mat3f transpose_this_mat(transpose_this);
        const Mat3f expected_tran(expected_tran_data);
        transpose_this_mat.Transpose();
        ASSERT( transpose_this_mat.Approx( expected_tran ) );
    }

    void Mat3f::UnitTest_ComputeMinors( void ) {
        const float minor_this[9]{3,2,0, 0,0,1, 2,-2,1};
        const float expected_minor_data[9]{ 2,-2,0, 2,3,-10, 2,3,0 };
        const Mat3f expected_minor( expected_minor_data );
        const Mat3f mat( minor_this );
        Mat3f min;
        mat.ComputeMinors( min );
        ASSERT( min.Approx( expected_minor ) );
    }

    void Mat3f::UnitTest_ComputeCofactors( void ) {
        const float cofactor_this[9]{2,-2,0, 2,3,-10, 2,3,0};
        const float expected_cofactor_data[9]{ 2,2,0, -2,3,10, 2,-3,0 };
        const Mat3f expected_cofactor( expected_cofactor_data );
        Mat3f cof( cofactor_this );
        cof.ComputeCofactors();
        ASSERT( cof.Approx( expected_cofactor ) );
    }

    void Mat3f::UnitTest_ComputeDeterminant( void ) {
        const float expected_det_data[9]{ 2,-2,0, 2,3,-10, 2,3,0 };
        const Mat3f expected_det( expected_det_data );
        const float minor_this[9]{3,2,0, 0,0,1, 2,-2,1};
        const Mat3f mat( minor_this );
        ASSERT( Maths::Approxf( mat.ComputeDeterminant( expected_det), 10 ) );
        ASSERT( Maths::Approxf( mat.ComputeDeterminant(), 10 ) );
    }

    void Mat3f::UnitTest_Invert( void ) {
        {
            const float invert_this[9]{3,2,0, 0,0,1, 2,-2,1};
            const float inverted_data[9]{ 0.2f,-0.2f,0.2f, 0.2f,0.3f,-0.3f, 0,1,0 };
            const Mat3f inverted( inverted_data );
            Mat3f invert( invert_this);
            invert.Invert();
            ASSERT( invert.Approx( inverted) );
        }
        
        {
            const float invert_data[9]{1,0,-1, 3,5,3, 2,1,0 };
            const float inverted_data[9]{(1.0f/4)*-3,(1.0f/4)*-1,(1.0f/4)*5, (1.0f/4)*6,(1.0f/4)*2,(1.0f/4)*-6, (1.0f/4)*-7,(1.0f/4)*-1,(1.0f/4)*5 };
            Mat3f invert( invert_data );
            const Mat3f inverted( inverted_data );
            invert.Invert();
            ASSERT( invert.Approx( inverted ) );
        }
        
        {
            const float invert_this[9]{ 1,0,6, 3,2,8, 3,4,0 };
            const float inverted_data[9]{-8,6,-3, 6,-4.5f,2.5f, 1.5f,-1,0.5f};
            const Mat3f inverted( inverted_data );
            Mat3f invert( invert_this);
            invert.Invert();
            ASSERT( invert.Approx( inverted) );
        }
    }

#endif //CLIB_UNIT_TEST
