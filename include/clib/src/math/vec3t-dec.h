// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_VEC_H_3T_DEC
#define SRC_CLIB_MATH_VEC_H_3T_DEC

#include <cmath>

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./values.h"

class Mat3f;
class Mat4f;

namespace String {
    bool ToVec3f( const std::string& str, Vec3f& out );
    Vec3f ToVec3f( const std::string& str );
    
    template< typename TYPE >
    TYPE To( const std::string& str );
    
    template< typename TYPE, uint NUM_COMMAS >
    bool ToVec( const std::string& vecstr, TYPE& out );
}

template<typename TYPE>
class Vec3t {
public:
    Vec3t<TYPE>( void );
    Vec3t<TYPE>( const Vec3t<TYPE> &other ) = default;
    Vec3t<TYPE>( const TYPE nx, const TYPE ny, const TYPE nz );
    explicit Vec3t<TYPE>( const TYPE all );

    Vec3t<TYPE>& operator=( const Vec3t<TYPE> &other ) = default;

    Vec3t<TYPE> operator-(void) const;
    Vec3t<TYPE> operator*( const Vec3t<TYPE> &other ) const;
    Vec3t<TYPE> operator*( const TYPE val ) const;
    Vec3t<TYPE>& operator*=( const Vec3t<TYPE> &other );
    Vec3t<TYPE>& operator*=( const TYPE val );

    Vec3t<TYPE> operator+( const Vec3t<TYPE> &other ) const;
    Vec3t<TYPE> operator+( const TYPE val ) const;
    Vec3t<TYPE>& operator+=( const Vec3t<TYPE> &other );
    Vec3t<TYPE>& operator+=( const TYPE val );

    Vec3t<TYPE> operator-( const Vec3t<TYPE> &other ) const;
    Vec3t<TYPE> operator-( const TYPE val ) const;
    Vec3t<TYPE>& operator-=( const Vec3t<TYPE> &other );
    Vec3t<TYPE>& operator-=( const TYPE val );

    Vec3t<TYPE> operator/( const Vec3t<TYPE> &other ) const;
    Vec3t<TYPE> operator/( const TYPE val ) const;
    Vec3t<TYPE>& operator/=( const TYPE val );
    Vec3t<TYPE>& operator/=( const Vec3t<TYPE> &other );

    Vec3t<TYPE>& operator*=( const Mat3f& mat );
    Vec3t<TYPE> operator*( const Mat3f& mat ) const;

    Vec3t<TYPE>& operator*=( const Mat4f& mat );
    Vec3t<TYPE> operator*( const Mat4f& mat ) const;
        
    Vec3t<TYPE>& operator*=( const Quat& quat ); //!< WARNING : DO NOT USE TO GET ROTATION VECTORS.  This should only be used to rotate a point.
    Vec3t<TYPE> operator*( const Quat& quat ) const; //!< WARNING : DO NOT USE TO GET ROTATION VECTORS.  This should only be used to rotate a point.
    
    bool operator==( const Vec3t<TYPE> &b ) const;
    bool operator==( const TYPE b ) const;
    bool operator!=( const Vec3t<TYPE> &b ) const;
    bool operator!=( const TYPE b ) const;

    TYPE operator[] ( const std::size_t index ) const;
    TYPE& operator[] ( const std::size_t index );

    Vec3t<TYPE>& Set( const TYPE a, const TYPE b, const TYPE c );
    Vec3t<TYPE>& SetNormalized( const TYPE a, const TYPE b, const TYPE c );
    Vec3t<TYPE>& Set( const TYPE a, const TYPE b );

    TYPE Dot( const Vec3t<TYPE> &other ) const;

    Vec3t<TYPE> PerpCCW_ZAxis( void ) const;
    Vec3t<TYPE> PerpCW_ZAxis( void ) const;

    void Zero( void );

    Vec2t<TYPE> ToVec2( void ) const;

    float DegreesBetween( const Vec3f &vec ) const;
    float DegreesBetween( const Vec3f &vec1, const Vec3f &vec2 ) const;
    float RadiansBetween( const Vec3f &vec ) const;
    float RadiansBetween( const Vec3f &vec1, const Vec3f &vec2 ) const;
    Vec3t<TYPE> Interp (const Vec3t<TYPE> &to, const float delta) const;

    Vec3t<TYPE> Cross( const Vec3t<TYPE> &other ) const;
    static Vec3t<TYPE> Cross( const Vec3t<TYPE> &first, const Vec3t<TYPE> &second );

    float Len( void ) const; //!< Not as fast as SquaredLen() due to sqrtf() call
    static float Len( const Vec3f &vec ); //!< Not as fast as SquaredLen() due to sqrtf() call

    float SquaredLen( void ) const; //!< faster than len due to the lack of sqrtf() call
    static float SquaredLen( const Vec3f &vec ); //!< faster than len due to the lack of sqrtf() call

    void Normalize( void );
    
    void Print( void ) const;

    NODISCARD Vec3t<TYPE> GetNormalized( void ) const;
    NODISCARD static Vec3t<TYPE> GetNormalized( const Vec3t<TYPE>& vec );

    #ifdef CLIB_UNIT_TEST
        static void UnitTest( void );
    #endif // CLIB_UNIT_TEST

public:
    TYPE        x,y,z;
};

template<typename TYPE>
class Vec3t;

typedef Vec3t<float> Vec3f;

#endif  //SRC_CLIB_MATH_VEC3T_DEC
