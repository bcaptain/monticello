// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_VEC_H_2T
#define SRC_CLIB_MATH_VEC_H_2T

#include <cmath>

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "../message.h"
#include "../maths.h"
#include "./values.h"
#include "./vec2t-dec.h"
#include "./vec3t-dec.h"

namespace String {
    template<>
    inline Vec2f To< Vec2f >( const std::string& str ) {
        return String::ToVec2f( str );
    }

    template<>
    inline std::string ToString<Vec2f>( const Vec2f& v ) {
        return VecToString(v,',');
    }
}

template<typename TYPE>
Vec2t<TYPE>::Vec2t( void ) : x(0.0f), y(0.0f) {
}

template<typename TYPE>
Vec2t<TYPE>::Vec2t( const TYPE nx, const TYPE ny)
    : x( nx ), y( ny )
{ }

template<typename TYPE>
Vec2t<TYPE>::Vec2t( const TYPE all )
    : x( all ), y( all )
{ }

template<typename TYPE>
inline Vec2t<TYPE> Vec2t<TYPE>::operator-(void) const {
    return Vec2t<TYPE>(-x, -y);
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator*( const TYPE val ) const {
     return Vec2t<TYPE>(val * x, val * y);
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator*( const Vec2t<TYPE> &other ) const {
     return Vec2t<TYPE>(x * other.x, y * other.y );
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator*=( const TYPE val ) {
    x *= val;
    y *= val;
    return *this;
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator*=( const Vec2t<TYPE> &other ) {
    x *= other.x;
    y *= other.y;
    return *this;
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator+( const Vec2t<TYPE> &other ) const {
     return Vec2t<TYPE>(x + other.x, y + other.y );
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator+( const TYPE val ) const {
     return Vec2t<TYPE>(x + val, y + val);
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator+=( const Vec2t<TYPE> &other ) {
    x += other.x;
    y += other.y;
    return *this;
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator+=( const TYPE val ) {
    x += val;
    y += val;
    return *this;
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator-( const Vec2t<TYPE> &other ) const {
     return Vec2t<TYPE>(x - other.x, y - other.y );
}


template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator-( const TYPE val ) const {
     return Vec2t<TYPE>(x - val, y - val);
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator-=( const TYPE val ) {
    x -= val;
    y -= val;
    return *this;
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator-=( const Vec2t<TYPE> &other ) {
    x -= other.x;
    y -= other.y;
    return *this;
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator/( const TYPE val ) const {
    return Vec2t<TYPE> (
        Maths::Divide( x, val )
        , Maths::Divide( y, val )
    );
}

template<typename TYPE>
Vec2t<TYPE> Vec2t<TYPE>::operator/( const Vec2t<TYPE> &other ) const {
    return Vec2t<TYPE> (
        Maths::Divide( x, other.x )
        , Maths::Divide( y, other.y )
    );
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator/=( const Vec2t<TYPE> &other ) {
    x = Maths::Divide( x, other.x );
    y = Maths::Divide( y, other.y );
    return *this;
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::operator/=( const TYPE val ) {
    x = Maths::Divide( x, val );
    y = Maths::Divide( y, val );
    return *this;
}

template<typename TYPE>
Vec2t<TYPE>& Vec2t<TYPE>::Set( const TYPE a, const TYPE b ) {
    x=a;
    y=b;
    return *this;
}

template<typename TYPE>
TYPE Vec2t<TYPE>::Cross( const Vec2t<TYPE> &other ) const {
    return x * other.y - y * other.x;
}

template<typename TYPE>
TYPE Vec2t<TYPE>::Dot( const Vec2t<TYPE> &other ) const {
    return (x * other.x ) + (y * other.y );
}

template<typename TYPE>
void Vec2t<TYPE>::Zero( void ) {
    x=0;
    y=0;
}

template<typename TYPE>
inline Vec3t<TYPE> Vec2t<TYPE>::ToVec3( void ) const {
    return Vec3t<TYPE>(x, y, 0 );
}

template<typename TYPE>
inline Vec2t<TYPE> Vec2t<TYPE>::PerpCCW( void ) const {
    return Vec2t<TYPE>(-y,x);
}

template<typename TYPE>
inline Vec2t<TYPE> Vec2t<TYPE>::PerpCW( void ) const {
    return Vec2t<TYPE>(y,-x);
}

template<typename TYPE>
float Vec2t<TYPE>::Len( void ) const {
    return sqrtf(x*x + y*y);
}

template<typename TYPE>
float Vec2t<TYPE>::SquaredLen( void ) const {
    return x*x + y*y;
}

template<typename TYPE>
float Vec2t<TYPE>::Len( const Vec2t<TYPE> &vec ) {
    return sqrtf(vec.x*vec.x + vec.y*vec.y);
}

template<typename TYPE>
void Vec2t<TYPE>::Normalize( void ) {
    operator/=( Len() );
}

template<typename TYPE>
Vec2f Vec2t<TYPE>::GetNormalized( void ) {
    return *this / Len();
}

template<typename TYPE>
inline float Vec2t<TYPE>::SquaredLen( const Vec2t<TYPE> &vec ) {
    return vec.SquaredLen();
}

template<typename TYPE>
inline bool Vec2t<TYPE>::operator!=( const Vec2t<TYPE> &other) const {
    return !operator==(other);
}

template<typename TYPE>
inline bool Vec2t<TYPE>::operator!=( const TYPE val) const {
    return !operator==(val);
}

template<typename TYPE>
bool Vec2t<TYPE>::operator==( const Vec2t<TYPE> &other) const {
    return (
        Maths::Approxf( x, other.x ) &&
        Maths::Approxf( y, other.y ) 
    );
}

template<typename TYPE>
bool Vec2t<TYPE>::operator==( const TYPE val) const {
    return (
        Maths::Approxf( x, val ) &&
        Maths::Approxf( y, val ) 
    );
}

template<typename TYPE>
TYPE Vec2t<TYPE>::operator[]( const std::size_t index ) const {
    switch ( index ) {
        case 0: return x;
        case 1: return y;
        default:
            DIE("Invalid vec2t index: %u\n", index);
            return {};
    };
}

template<typename TYPE>
TYPE& Vec2t<TYPE>::operator[]( const std::size_t index ) {
    switch ( index ) {
        case 0: return x;
        case 1: return y;
        default:
            DIE("Invalid vec2t index: %u\n", index);
            return y; // return last element in list, if user wishes to not abort the program
    };
}


#endif  //SRC_CLIB_MATH_VEC2T
