// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "../message.h"
#include "../strings.h"
#include "./quat.h"
#include "./mat.h"

namespace String {
    std::string QuatToString( const Quat& q ) {
        return
            std::to_string(q.x)+','+
            std::to_string(q.y)+','+
            std::to_string(q.z)+','+
            std::to_string(q.w)
        ;
    }

    bool ToQuat( const std::string& quatstr, Quat& out ) {
        return ToVec< Quat, 3 >(quatstr, out);
    }

    Quat ToQuat( const std::string& str ) {
        Quat ret;
        ToQuat( str, ret );
        return ret;
    }
}

float Quat::operator[]( const std::size_t index ) const {
    switch ( index ) {
        case 0: return x;
        case 1: return y;
        case 2: return z;
        case 3: return w;
        default:
            DIE("Invalid Quat index: %u\n", index);
            return w; // return last element in list, if user wishes to not abort the program
    };
}

float& Quat::operator[]( const std::size_t index ) {
    switch ( index ) {
        case 0: return x;
        case 1: return y;
        case 2: return z;
        case 3: return w;
        default: DIE("Invalid Quat index: %u\n", index);
            return w; // return last element in list, if user wishes to not abort the program
    };
}

Quat::Quat( const Quat& other )
    : x(other.x), y(other.y), z(other.z), w(other.w)
{ }

Quat::Quat( void )
    : x(0.0f), y(0.0f), z(0.0f), w(1.0f)
{ }

Quat::Quat( const float xx, const float yy, const float zz, const float ww )
    : x(xx), y(yy), z(zz), w(ww)
{ }

Quat::Quat( const Vec3f& vec, float ww)
    : x(vec.x), y(vec.y), z(vec.z), w(ww)
{ }

Quat::Quat( const Mat3f& mat )
    : x(0.0f), y(0.0f), z(0.0f), w(1.0f)
{
    SetFromMatrix(mat);
}

void Quat::Set( const float xx, const float yy, const float zz, const float ww ) {
    x=xx;
    y=yy;
    z=zz;
    w=ww;
}

void Quat::Normalize( void ) {
    
    float mag_sq = SquaredLen();

    while ( !Maths::Approxf( mag_sq, 1.0f, EP_TEN_THOUSANDTH) ) {
        
        const float mag = sqrtf( mag_sq );
        
        x /= mag;
        y /= mag;
        z /= mag;
        w /= mag;

        mag_sq = SquaredLen();
    }
}

void Quat::BecomeConjugate( void ) {
    x = -x;
    y = -y;
    z = -z;
}

Quat& Quat::operator+=( const Quat& other ) {
    x += other.x;
    y += other.y;
    z += other.z;
    w += other.w;
    return *this;
}


Quat& Quat::operator=( const Quat& other ) {
    x = other.x;
    y = other.y;
    z = other.z;
    w = other.w;
    return *this;
}

Quat& Quat::operator-=( const Quat& other ) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
    w -= other.w;
    return *this;
}

Quat Quat::operator+( const Quat& other ) const {
    return {
        x + other.x
        , y + other.y
        , z + other.z
        , w + other.w
    };
}

Quat Quat::operator-( const Quat& other ) const {
    return {
        x - other.x
        , y - other.y
        , z - other.z
        , w - other.w
    };
}

Quat& Quat::operator*=( const Quat& other ) {
    return *this = *this * other;
}

// q1 * q2 applies q2's rotation to q1. this is not commutative
Quat Quat::operator*( const Quat& other ) const {
    return {
        w * other.x + x * other.w + y * other.z - z * other.y
        , w * other.y - x * other.z + y * other.w + z * other.x
        , w * other.z + x * other.y - y * other.x + z * other.w
        , w * other.w - x * other.x - y * other.y - z * other.z
    };
}

// applying rotation to a vector is multiplying the vec by the quat and its conjugate
Vec3f Quat::operator*( const Vec3f& vec ) const {
    // multiply vector as quaternion by this quaternion
    Quat v(vec.x, vec.y, vec.z, 0.0f);
    const Quat tmp( *this * v );

    // multiply result by this quaternion's inverse
    // assuming this quaternion is normalized
    Quat conjugate( *this );
    conjugate.BecomeConjugate();
    v = tmp * conjugate;

    return { v.x, v.y, v.z };
}

Quat& Quat::operator*=( int val ) {
    return *this = *this * val;
}

Quat Quat::operator*( int val ) const {
    return {
        x * val
        , y * val
        , z * val
        , w * val
    };
}

Quat& Quat::operator*=( float val ) {
    return *this = *this * val;
}

Quat Quat::operator*( float val ) const {
    return {
        x * val
        , y * val
        , z * val
        , w * val
    };
}

Quat& Quat::operator/=( float val ) {
    return *this = *this / val;
}

Quat Quat::operator/( float val ) const {
    return {
        x / val
        , y / val
        , z / val
        , w / val
    };
}

float Quat::SquaredLen( void ) const {
    return x*x + y*y + z*z + w*w;
}

void Quat::DotMult(const Quat& other, Quat& out) const {
    out.x = x * other.x;
    out.y = y * other.y;
    out.z = z * other.z;
    out.w = w * other.w;
}

//Returns a Scalar
float Quat::Dot(const Quat& other) const {
    return x * other.x + y * other.y + z * other.z + w * other.w;
}

Quat::Quat( float radians, const Vec3f& axis )
    : x(0), y(0), z(0), w(1)
{
    SetFromAxis( radians, axis );
}

Quat Quat::NormalizedInterpolation( const Quat& other, const float timeDelta ) const {
    const float delta_reciprocal = 1.0f - timeDelta;
    Quat interp;
    
    //if dot is < 0.0f then the rotation will take the "longest route" we negate the values to make it take the "shortest route"
    Quat modified( other );

    if ( Dot(other) < 0.0f) {
        modified.w = -other.w;
        modified.x = -other.x;
        modified.y = -other.y;
        modified.z = -other.z;
    }
    
    interp.w = delta_reciprocal * w + timeDelta * modified.w;
    interp.x = delta_reciprocal * x + timeDelta * modified.x;
    interp.y = delta_reciprocal * y + timeDelta * modified.y;
    interp.z = delta_reciprocal * z + timeDelta * modified.z;
    
    interp.Normalize();
    
    return interp;
}

// essentially we create 3 quats for pitch, yaw, and roll, and just multiply them together
Quat::Quat( float roll_degrees, float pitch_degrees, float yaw_degrees )
    : x(0), y(0), z(0), w(1)
{
    SetFromAngles_ZYX( roll_degrees, pitch_degrees, yaw_degrees );
}

void Quat::SetFromAxis( float radians, const Vec3f& axis ) {
    radians /= 2;
    float sine( sinf(radians) );
    Vec3f vec_n( axis.GetNormalized() );

    x = vec_n.x * sine;
    y = vec_n.y * sine;
    z = vec_n.z * sine;
    w = cosf(radians);
}

void Quat::SetFromAngles_ZYX( float roll_degrees, float pitch_degrees, float yaw_degrees ) {
    pitch_degrees *= HALF_PI_OVER_180_F;
    yaw_degrees *= HALF_PI_OVER_180_F;
    roll_degrees *= HALF_PI_OVER_180_F;

    From_RadAng_Helper_ZYX( *this, roll_degrees, pitch_degrees, yaw_degrees );
}

void Quat::SetFromRadians_ZYX( float roll_radians, float pitch_radians, float yaw_radians ) {
    pitch_radians /= 2;
    roll_radians /= 2;
    yaw_radians /= 2;

    From_RadAng_Helper_ZYX( *this, roll_radians, pitch_radians, yaw_radians );
}

void Quat::SetFromMatrix( const Mat3f& mat ) {
    const float fourXSquaredMinus1 = mat.M(0,0) - mat.M(1,1) - mat.M(2,2);
    const float fourYSquaredMinus1 = mat.M(1,1) - mat.M(0,0) - mat.M(2,2);
    const float fourZSquaredMinus1 = mat.M(2,2) - mat.M(0,0) - mat.M(1,1);
    const float fourWSquaredMinus1 = mat.M(0,0) + mat.M(1,1) + mat.M(2,2);

    int biggestIndex = 0;
    float fourBiggestSquaredMinus1 = fourWSquaredMinus1;

    if (fourXSquaredMinus1 > fourBiggestSquaredMinus1) {
        fourBiggestSquaredMinus1 = fourXSquaredMinus1;
        biggestIndex = 1;
    }

    if (fourYSquaredMinus1 > fourBiggestSquaredMinus1) {
        fourBiggestSquaredMinus1 = fourYSquaredMinus1;
        biggestIndex = 2;
    }

    if (fourZSquaredMinus1 > fourBiggestSquaredMinus1) {
        fourBiggestSquaredMinus1 = fourZSquaredMinus1;
        biggestIndex = 3;
    }

    const float biggestVal = sqrtf(fourBiggestSquaredMinus1 + 1.0f) * 0.5f;
    const float mult = 0.25f / biggestVal;


    switch (biggestIndex) {
        case 1:
            w = (mat.M(1,2) - mat.M(2,1)) * mult;
            x = biggestVal;
            y = (mat.M(0,1) + mat.M(1,0)) * mult;
            z = (mat.M(2,0) + mat.M(0,2)) * mult;
            break;
        case 2:
            w = (mat.M(2,0) - mat.M(0,2)) * mult;
            x = (mat.M(0,1) + mat.M(1,0)) * mult;
            y = biggestVal;
            z = (mat.M(1,2) + mat.M(2,1)) * mult;
            break;
        case 3:
            w = (mat.M(0,1) - mat.M(1,0)) * mult;
            x = (mat.M(2,0) + mat.M(0,2)) * mult;
            y = (mat.M(1,2) + mat.M(2,1)) * mult;
            z = biggestVal;
            break;
        case 0: FALLTHROUGH;
        default:
            w = biggestVal;
            x = (mat.M(1,2) - mat.M(2,1))* mult;
            y = (mat.M(2,0) - mat.M(0,2)) * mult;
            z = (mat.M(0,1) - mat.M(1,0)) * mult;
            break;
    }
}

void Quat::From_RadAng_Helper_ZYX( Quat& quat, float roll, float pitch, float yaw ) {
    const float sinp = sinf(pitch);  
    const float siny = sinf(yaw);
    const float sinr = sinf(roll);
    const float cosp = cosf(pitch);
    const float cosy = cosf(yaw);
    const float cosr = cosf(roll);

    //ZYX Rotation
    quat.x = sinr * cosp * cosy - cosr * sinp * siny;
    quat.y = cosr * sinp * cosy + sinr * cosp * siny;
    quat.z = cosr * cosp * siny - sinr * sinp * cosy;
    quat.w = cosr * cosp * cosy + sinr * sinp * siny;

    quat.Normalize();
}

void Quat::UnitTest_SetFromRadians_XYZ( float roll_radians, float pitch_radians, float yaw_radians ) {
    const float sinp = sinf(pitch_radians / 2);  
    const float siny = sinf(yaw_radians / 2);
    const float sinr = sinf(roll_radians / 2);
    const float cosp = cosf(pitch_radians / 2);
    const float cosy = cosf(yaw_radians / 2);
    const float cosr = cosf(roll_radians / 2);
    
    //Roll - X, Pitch - Y, Yaw - Z, 
  
    //XYZ Rotation
    w = cosr * cosp * cosy - sinr * sinp * siny;
    x = sinr * cosp * cosy + sinp * siny * cosr;
    y = sinp * cosr * cosy - sinr * siny * cosp;
    z = sinr * sinp * cosy + siny * cosr * cosp;
    
    Normalize();
}

void Quat::ToMat4f( float mat4x4_out[16] ) const {
    const float X = x * x * 2.0f;
    const float Y = y * y * 2.0f;
    const float Z = z * z * 2.0f;
    const float xy2 = x * y * 2.0f;
    const float xz2 = x * z * 2.0f;
    const float wx2 = w * x * 2.0f;
    const float wy2 = w * y * 2.0f;
    const float wz2 = w * z * 2.0f;
    const float yz2 = y * z * 2.0f;

    const std::initializer_list<float> val{
        1.0f - (Y + Z), xy2 + wz2, xz2 - wy2, 0.0f,
        xy2 - wz2, 1.0f - (X + Z), yz2 + wx2, 0.0f,
        xz2 + wy2, yz2 - wx2, 1.0f - (X + Y), 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };
    std::copy( val.begin(), val.end(), mat4x4_out );
}

void Quat::ToMat3f( float mat3x3_out[9] ) const {
    const float X = x * x * 2.0f;
    const float Y = y * y * 2.0f;
    const float Z = z * z * 2.0f;
    const float xy2 = x * y * 2.0f;
    const float xz2 = x * z * 2.0f;
    const float wx2 = w * x * 2.0f;
    const float wy2 = w * y * 2.0f;
    const float wz2 = w * z * 2.0f;
    const float yz2 = y * z * 2.0f;

    const std::initializer_list<float> val{
        1.0f - (Y + Z), xy2 + wz2, xz2 - wy2,
        xy2 - wz2, 1.0f - (X + Z), yz2 + wx2,
        xz2 + wy2, yz2 - wx2, 1.0f - (X + Y)
    };

    std::copy( val.begin(), val.end(), mat3x3_out );
}

bool Quat::operator==( const Quat& other ) const {
    return (
        Maths::Approxf( x, other.x )
        && Maths::Approxf( y, other.y )
        && Maths::Approxf( z, other.z )
        && Maths::Approxf( w, other.w )
    );
}

bool Quat::Approx( const Quat& other, const float epsilon ) const {
    return (
        Maths::Approxf( x, other.x, epsilon )
        && Maths::Approxf( y, other.y, epsilon )
        && Maths::Approxf( z, other.z, epsilon )
        && Maths::Approxf( w, other.w, epsilon )
    );
}

void Quat::ToMat4f( Mat4f& out ) const {
    ToMat4f( out.data );
}

void Quat::ToMat3f( Mat3f& out ) const {
    ToMat3f( out.data );
}

void Quat::BecomeInverse( void ) { //!< NOTE: This only works for unit-quaternions
    BecomeConjugate();
}

Quat Quat::GetInverse( void ) const { //!< NOTE: This only works for unit-quaternions
    return GetConjugate();
}

Quat Quat::GetConjugate( void ) const { //!< NOTE: This only works for unit-quaternions
    return { -x, -y, -z, w };
}

bool Quat::operator!=( const Quat& other ) const {
    return !operator==( other );
}

Quat Quat::FromAxis( float radians, const Vec3f& axis ) {
    return { radians, axis };
}

Quat Quat::FromAngles_ZYX( const Vec3f& degrees ) {
    return { degrees.x, degrees.y, degrees.z };
}

Quat Quat::FromAngles_ZYX( float roll_degrees, float pitch_degrees, float yaw_degrees ) {
    return { roll_degrees, pitch_degrees, yaw_degrees };
}

Quat Quat::FromRadians_ZYX( const Vec3f& radians ) {
    return { radians.x * RAD_TO_DEG_F, radians.y * RAD_TO_DEG_F, radians.z * RAD_TO_DEG_F };
}

Quat Quat::FromRadians_ZYX( float roll_radians, float pitch_radians, float yaw_radians ) {
    return { roll_radians * RAD_TO_DEG_F, pitch_radians * RAD_TO_DEG_F, yaw_radians * RAD_TO_DEG_F };
}

Quat Quat::FromMatrix( const Mat3f& mat ) {
    return { mat };
}

float Quat::Len( void ) const {
    return sqrtf( SquaredLen() );
}

#ifdef CLIB_UNIT_TEST
    void Quat_UnitTest_FromAnglesAndRadians( void );
    void Quat_UnitTest_FromAnglesAndRadians( void ) {
        Quat a, b;
        
        //Rotation Order - ZYX
            const Vec3f angles_1( 87, -44, 13 );
            const Quat q_from_angles_1_ZYX( 0.6648893356323242f, -0.19773347675800323f, 0.3323400318622589f, 0.6390412449836731f ); // Rotation_Degrees(87,-44,13) Rotation Order = Z,Y,X
            a = a.FromAngles_ZYX( angles_1 );
            
            ASSERT( a == q_from_angles_1_ZYX );
        
        //Rotation Order - XYZ
            Vec3f angles_1_radians = angles_1 * DEG_TO_RAD_F;
            b.UnitTest_SetFromRadians_XYZ( angles_1_radians.x, angles_1_radians.y, angles_1_radians.z );
            const Quat q_from_angles_1_XYZ( 0.6033678650856018f, -0.3422331213951111f, -0.18006914854049683f, 0.697422981262207f ); // Rotation_Degrees(87,-44,13) Rotation Order = X,Y,Z
            
            ASSERT( b == q_from_angles_1_XYZ );
        
        //.FromAngles_ZYX and .FromRadians_ZYX
            const Vec3f angles_2( 5, 34, -67 );
            const Quat q_from_angles_2( 0.19600139558315277f, 0.22054937481880188f, -0.5379522442817688f, 0.789651095867157f ); // Rotation_Degrees(X,Y,Z) = 5, 34, -67
            a = a.FromAngles_ZYX( angles_2 );
            b = b.FromRadians_ZYX( angles_2 * DEG_TO_RAD_F );
            ASSERT( a == q_from_angles_2 );
            ASSERT( b == q_from_angles_2 );
        
        //Floating point rotation angle
            const Vec3f angles_3( -85, -394, 0.7f );
            const Quat q_from_angles_3( 0.6447412967681885f, 0.21950165927410126f, 0.1932128667831421f, -0.7062552571296692f ); // Rotation_Degrees(X,Y,Z) = -85, -394, 0.7
            a = a.FromAngles_ZYX( angles_3 );
            b = b.FromRadians_ZYX( angles_3 * DEG_TO_RAD_F );
            ASSERT( a == q_from_angles_3 );
            ASSERT( b == q_from_angles_3 );
        
        //Rotation angles >360 degrees
            const Vec3f angles_4( -5354, -3324, 66547 );
            const Quat q_from_angles_4( 0.014977299608290195f, 0.6807860136032104f, 0.07125894725322723f, -0.7288540601730347f); // Rotation_Degrees(X,Y,Z) = -5354, -3324, 66547
            a = a.FromAngles_ZYX( angles_4 );
            b = b.FromRadians_ZYX( angles_4 * DEG_TO_RAD_F );
            ASSERT( a == q_from_angles_4 );
            ASSERT( b == q_from_angles_4 );
        
    }
    
    void Quat_UnitTest_ToMat3fAssignment( void );
    void Quat_UnitTest_ToMat3fAssignment( void ) {
    }
    
    void Quat_UnitTest_MatToQuat();
    void Quat_UnitTest_MatToQuat( void ) {
        //Testing conversion from Mat to Quat
            const Quat a(0.0f,0.7071067690849304f,0.0f,0.707106769084934f);
            Mat3f m1;
            m1.Rotate(90.0f,0.0f,1.0f,0.0f); //90 degrees on the y-axis
            const Quat b(m1);
            ASSERT( a == b );
    }
    
    void Quat_UnitTest_Normalizing();
    void Quat_UnitTest_Normalizing( void ) {
        const Vec3f angles_1( 245, 5720, -900 );
        const Quat norm_quat_from_angles_1( 0.183767631649971f, -0.7925285696983337f, 0.5048964619636536f, 0.28845712542533875f );
        
        Quat a = a.FromAngles_ZYX( angles_1 );
        a *= 3.2f; //Scale quaternion
        Quat b = b.FromAngles_ZYX( angles_1 );
        b *= 1.8f; //Scale quaternion
        
        a.Normalize();
        b.Normalize();
        
        ASSERT( a == norm_quat_from_angles_1 );
        ASSERT( b == norm_quat_from_angles_1 );
        
        const Quat norm_quat_after_multiply( 0.10601811856031418f, -0.45722100138664246f, 0.29128193855285645f, -0.8335850238800049f );
        
        Quat c = a * b;
        
        c.Normalize();
        
        ASSERT( c == norm_quat_after_multiply );
    }
    
    void Quat_UnitTest_MultiplyQuats();
    void Quat_UnitTest_MultiplyQuats( void ) {
        const Vec3f angles_1( 90, -90, 90 );
        const Quat q_from_angles_1( 0.7071067690849304f, 0.0f, 0.7071067690849304f, 0.0f ); // Rotation_Degrees(X,Y,Z) = 90, -90, 90
        const Quat q_mult_answer_1( 0.0f, 0.0f, 0.0f, -1.0f ); //Multiply Rotation( 90, -90, 90 ) by Rotation( 90, -90, 90 )
        const Quat b = Quat::FromAngles_ZYX( angles_1 );
        const Quat a = b;
        const Quat c = a * b;
        ASSERT( c == q_mult_answer_1 );
        
    }
    
    void Quat_UnitTest_Conjugates();
    void Quat_UnitTest_Conjugates( void ) {
        const Vec3f angles_1( -90, 560, 0.9f );
        const Quat q_from_angles_1( -0.11731480062007904f, -0.6973071694374084f, -0.6953783631324768f,  0.12825313210487366f );
        const Quat q_from_angles_1_conjugate( 0.11731480062007904f, 0.6973071694374084f, 0.6953783631324768f,  0.12825313210487366f );
        
        Quat a = a.FromAngles_ZYX( angles_1 );
        Quat b = a;
        b.BecomeConjugate();
        
        ASSERT( b == q_from_angles_1_conjugate );
        
        a.Normalize();
        b.Normalize();
        const Quat c = a * b;
        
        const Quat identity_quat;
        ASSERT( c == identity_quat );
        
    }
    
    void Quat_Test_UnitLengthDrifting();
    void Quat_Test_UnitLengthDrifting( void ) {
        //Test unit length drifting
        const int CALCULATION_LIMIT = 5000;
        Vec3f angles;
        Quat b;
        int loop_cnt = 0;
        
        while ( loop_cnt < CALCULATION_LIMIT ) {
            loop_cnt++;
            angles.x = Random::Float( 0, 360.0f);
            angles.y = Random::Float( 0, 360.0f);
            angles.z = Random::Float( 0, 360.0f);
            
            const Quat a = Quat::FromAngles_ZYX( angles );
            b *= a;
            
            float mag = b.Len();
            if ( fabsf(mag - 1.0f) > EP_TEN_THOUSANDTH ) {
                WARN("UNIT TEST: clib::quat.cpp : Quaternion unit length drift detected, testing failed before CALCULATION_LIMIT reached.\n");
                break;
            }
        }
    }
    
    void Quat::UnitTest( void ) {        
        
        Quat_UnitTest_FromAnglesAndRadians();
        Quat_UnitTest_ToMat3fAssignment();
        Quat_UnitTest_MatToQuat();
        Quat_UnitTest_Normalizing();
        Quat_UnitTest_MultiplyQuats();
        Quat_UnitTest_Conjugates();
        Quat_Test_UnitLengthDrifting();
        
    }
#endif // CLIB_UNIT_TEST
