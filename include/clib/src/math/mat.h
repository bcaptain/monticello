// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_MATRIX
#define SRC_CLIB_MATH_MATRIX

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./vec3t.h"
#include "./vec2t.h"
#include "./dualquat.h"

class Mat4f;

namespace String {
    std::string Mat3fToString( const Mat3f& m );
    std::string Mat4fToString( const Mat4f& m );
}

/*!

Mat3f \n\n

column major

**/
    
class Mat3f {
public:
    Mat3f( void );
    virtual ~Mat3f( void );
    Mat3f( const Mat3f& other );
    explicit Mat3f( const Vec3f& from_angles_ZYX );
    Mat3f( Mat3f&& other_rref );
    Mat3f( const Mat4f& other );
    explicit Mat3f( const double other[9] );
    explicit Mat3f( const float other[9] );
    explicit Mat3f( const Quat& quat );
    Mat3f( float* float_array_to_acquire, const std::size_t array_size );
    static void UnitTest_Ctor( void );

public:
    void Print( void ) const;

public:

    // ** operators are intentionally not virtual. Cast Mat4x4 to Mat3x3 to access only it's 3x3 area.
    
    Mat3f& operator*=( const Mat3f& multiplier );
    Mat3f& operator*=( const float multiplier );
    
    Vec3f operator*( const Vec3f& vec ) const;

    Mat3f& operator=( const Mat3f& other );
    Mat3f& operator=( Mat3f&& other_rref );
    Mat3f& operator=( const double mat[9] );
    Mat3f& operator=( const float mat[9] );

    bool operator==( const Mat3f& other ) const;
    bool operator!=( const Mat3f& other ) const;

    Vec3f VecTimesMat3( const Vec3f& vec ) const;

    static void UnitTest_OperatorEqual( void );
    
    bool Approx( const Mat3f& other, const float epsilon = EP_TEN_THOUSANDTH ) const;

public:
    void Multiply( const Mat3f& other, Mat3f& out ) const;
    static void UnitTest_Multiply( void );

    void LoadIdentity( void );
    
    void ComputeCofactors( void );
    static void UnitTest_ComputeCofactors( void );
    
    void ComputeMinors( Mat3f& out ) const;
    static void UnitTest_ComputeMinors( void );
    
    float ComputeDeterminant( void ) const;
    float ComputeDeterminant( const Mat3f& minors ) const; //!< more efficient if we already have the minors matrix
    static void UnitTest_ComputeDeterminant( void );
    
    void Rotate( const float angle_degree, float x, float y, float z );
    void RotateXYZ( const Vec3f angles );
    void RotateZYX( const Vec3f angles );
    static void UnitTest_Rotate( void );
    
    void Scale( const float xyz );
    void Scale( const float x, const float y, const float z );
    
    bool Invert( void );
    static void UnitTest_Invert( void );
    
    void Transpose( void );
    static void UnitTest_Transpose( void );

    float M( const std::size_t col, const std::size_t row ) const;
    float& M( const std::size_t col, const std::size_t row );
    static void UnitTest_M( void );

    static void UnitTest( void );

protected:
    void DoRotation( float a, float x, float y, float z, Mat3f& temp_object );

public:
    float *data; //!< column major
    std::size_t dim; //!< how many rows and cols
};

/*!

Mat4f \n\n

column major

**/

//#define SRC_MATRIX_DEBUG_H_ 1
class Mat4f : public Mat3f {
public:
    Mat4f( void );
    ~Mat4f( void ) override;
    Mat4f( const Mat3f& other );
    Mat4f( const Mat4f& other );
    explicit Mat4f( const Vec3f& from_angles_ZYX );
    Mat4f( Mat4f&& other_rref );
    explicit Mat4f( const double mat[16] );
    explicit Mat4f( const float mat[16] );
    explicit Mat4f( const Dualquat& dq);
    
    Mat4f( const Vec3f& tran, const Quat& rot );
    static void UnitTest_ConstructFromQuat( void );

public:
    bool operator==( const Mat4f& mat ) const;
    bool operator!=( const Mat4f& mat ) const;

    bool Approx( const Mat4f& mat, const float epsilon = EP_TEN_THOUSANDTH ) const;

    Mat4f& operator*=( const Mat4f& multiplier );
    Mat4f operator*( const Mat4f& other ) const;
    
    Vec3f operator*( const Vec3f& vec ) const;
    Vec3f VecTimesMat4( const Vec3f& vec ) const;

    Mat4f& operator=( const Mat4f& other );
    Mat4f& operator=( Mat4f&& other_rref );
    Mat4f& operator=( const double mat[16] );
    Mat4f& operator=( const float mat[16] );
    

public:
    void LoadIdentity( void );
    void ZeroTranslation( void );
    void ZeroRotation( void );
    void Transpose( void );
    void Set( const float mat[16] );
    void Translate( const Vec3f& vec );
    void Translate( const float x, const float y, const float z );
    void ApplyTranslationTo( Vec3f& vec ) const;
    
    void Scale( const float x, const float y, const float z );
    void Scale( const float xyz );
    
    bool Invert( void );
    static void UnitTest_Invert( void );
    
    Vec3f GetTranslation( void ) const;
    void SetTranslation( const Vec3f& translation );
    void SetTranslation( const float x, const float y, const float z );
    void SetRotationFromQuat( const Quat& rot );
    void SetFromDualQuat( const Dualquat& dq );
    Mat3f GetRotationMatrix( void ) const;

    void Multiply( const Mat4f& other, Mat4f& out ) const;
    static void UnitTest_MultiplyWithMatrix( void );

    Vec3f MultiplyWithPerspective( const Vec3f& vec ) const;

    void Frustum( const float left, const float right, const float bottom, const float top, const float near, const float far );
    void FrustumInverse( const float left, const float right, const float bottom, const float top, const float near, const float far );
    void Perspective( const float fov, const float x,const float y,const float xnear,const float xfar );
    void PerspectiveInverse( const float fov, const float x, const float y, const float xnear, const float xfar );
    void Orthographic( const float bottom, const float top, const float left, const float right, const float xnear, const float xfar);
    void OrthographicInverse( const float bottom, const float top, const float left, const float right, const float xnear, const float xfar);

    static void UnitTest( void );
    static void UnitTest_Ctor( void );
    static void UnitTest_RotateXYZ( void );
    static void UnitTest_MultiplyWithVector( void );
    static void UnitTest_Rotate( void );
};


namespace String {
    template<>
    inline std::string ToString<Mat3f>( const Mat3f& m ) {
        return Mat3fToString( m );
    }

    template<>
    inline std::string ToString<Mat4f>( const Mat4f& m ) {
        return Mat4fToString( m );
    }
}

#endif  //SRC_CLIB_MATH_MATRIX
