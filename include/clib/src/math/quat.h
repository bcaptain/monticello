// Copyright 2010-2019 Brandon Captain. You may not copy this work.


#ifndef SRC_MATH_QUAT
#define SRC_MATH_QUAT

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./vec3t-dec.h"
#include "../random.h"

class Quat {
public:
    Quat( void );
    Quat( const float xx, const float yy, const float zz, const float ww );
    Quat( const Quat& other );
    Quat( const Vec3f& vec, float ww);
    Quat( const Mat3f& mat ); //! left unexplicit for Quat::FromMatrix() as implicit brace-init-list construction

private:
    Quat( float radians, const Vec3f& axis ); //!< use the static function instead, for more clarity at the call-site
    Quat( float roll_degrees, float pitch_degrees, float yaw_degrees ); //!< use the static function instead, for more clarity at the call-site

public:
    Quat& operator=( const Quat& other);

    bool operator==( const Quat& other ) const;
    bool operator!=( const Quat& other ) const;

    Quat operator*( const int val ) const;
    Quat& operator*=( const int val );

    Quat operator*( const float val ) const;
    Quat& operator*=( const float val );
    
    Quat operator/( const float val ) const;
    Quat& operator/=( const float val );

    Vec3f operator*( const Vec3f& val ) const;
    
    Quat operator*( const Quat& other ) const;
    Quat& operator*=( const Quat& other );

    Quat operator+( const Quat& other) const;
    Quat& operator+=( const Quat& other);

    Quat operator-( const Quat& other) const;
    Quat& operator-=( const Quat& other);

    float operator[] ( const std::size_t index ) const;
    float& operator[] ( const std::size_t index );

    bool Approx( const Quat& other, const float epsilon = EP_TEN_THOUSANDTH ) const;

public:
    void Set( const float xx, const float yy, const float zz, const float ww );

    void SetFromAxis( float radians, const Vec3f& axis );
    void SetFromAngles_ZYX( const Vec3f& angles ); 
    void SetFromAngles_ZYX( float roll_degrees, float pitch_degrees, float yaw_degrees ); 
    void SetFromRadians_ZYX( float roll_radians, float pitch_radians, float yaw_radians );
    void SetFromMatrix( const Mat3f& mat );
    
    float SquaredLen( void ) const;
    float Len( void ) const;

    void Normalize( void );
    void BecomeConjugate( void ); //!< NOTE: This only works for unit-quaternions
    void BecomeInverse( void ); //!< NOTE: This only works for unit-quaternions
    Quat GetConjugate( void ) const; //!< NOTE: This only works for unit-quaternions
    Quat GetInverse( void ) const; //!< NOTE: This only works for unit-quaternions
    void DotMult( const Quat& other, Quat& out ) const;
    float Dot( const Quat& other) const;
    Quat NormalizedInterpolation( const Quat& dest, const float timeDelta ) const;
    void ToMat4f( float mat4x4_out[16] ) const;
    void ToMat4f( Mat4f& out ) const;
    void ToMat3f( float mat3x3_out[9] ) const;
    void ToMat3f( Mat3f& out ) const;

    static Quat FromAxis( float radians, const Vec3f& axis );
    static Quat FromAngles_ZYX( const Vec3f& degrees );
    static Quat FromAngles_ZYX( float roll_degrees, float pitch_degrees, float yaw_degrees );
    static Quat FromRadians_ZYX( const Vec3f& radians );
    static Quat FromRadians_ZYX( float roll_radians, float pitch_radians, float yaw_radians );
    static Quat FromMatrix( const Mat3f& mat );

    static void UnitTest( void );
    void UnitTest_SetFromRadians_XYZ( float roll_radians, float pitch_radians, float yaw_radians );  //Unit testing
    
private:
    static void From_RadAng_Helper_ZYX( Quat& quat, float roll, float pitch, float yaw );

public:
    float x,y,z,w;
};

namespace String {
    std::string QuatToString( const Quat& q );

    bool ToQuat( const std::string& str, Quat& out );
    Quat ToQuat( const std::string& str );
    
    template< typename TYPE >
    TYPE To( const std::string& str );

    template< typename TYPE >
    bool ToQuat( const std::string& quatstr, TYPE& out );

    template<>
    inline Quat To< Quat >( const std::string& str ) {
        return String::ToQuat( str );
    }

    template<>
    inline std::string ToString<Quat>( const Quat& q ) {
        return QuatToString( q );
    }
}

#endif  //SRC_MATH_QUAT
