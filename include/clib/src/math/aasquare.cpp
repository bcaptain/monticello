// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./aasquare.h"

AASquare2v::AASquare2v( const Vec2f& _mins, const Vec2f& _maxs ) : mins( _mins ), maxs( _maxs ) {
}

AASquare4v::AASquare4v( const Vec2f& _mins, const Vec2f& _maxs )
    : tl( _mins.x, _maxs.y )
    , tr(_maxs)
    , bl(_mins)
    , br( _maxs.x, _mins.y )
{}

AASquare4v::AASquare4v( const AASquare2v& other )
    : tl( other.mins.x, other.maxs.y )
    , tr(other.maxs)
    , bl(other.mins)
    , br( other.maxs.x, other.mins.y )
{}
