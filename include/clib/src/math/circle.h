// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MATH_CIRCLE_H_
#define SRC_CLIB_MATH_CIRCLE_H_

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./vec3t.h"
#include "./vec2t.h"

class Circle2f {
public:
    Circle2f( void );
    Circle2f( const Vec2f& _origin, const float _radius );
    Circle2f( const Circle2f& other ) = default;
    Circle2f& operator=( const Circle2f& other ) = default;

public:
    Vec2f origin;
    float radius;
};

class Circle3f {
public:
    Circle3f( void );
    Circle3f( const Vec3f& _origin, const float _radius, const Vec3f& _axis );
    Circle3f( const Circle3f& other ) = default;
    Circle3f& operator=( const Circle3f& other ) = default;

public:
    Vec3f origin;
    float radius;
    Vec3f axis;
};

#endif  //SRC_CLIB_MATH_CIRCLE_H_
