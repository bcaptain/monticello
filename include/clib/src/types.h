// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_TYPES_H_
#define SRC_CLIB_TYPES_H_

#include <cstdint>
#include <cmath>
#include <string>
#include <vector>
#include "./warnings.h"

typedef unsigned char uchar;
typedef std::int8_t int8;
typedef std::uint8_t Uint8;
typedef std::int16_t int16;
typedef std::uint16_t Uint16;
typedef std::int32_t int32;
typedef std::uint32_t Uint32;
typedef std::uint32_t Uint;
typedef std::uint32_t uint;
typedef std::int64_t int64;
typedef std::uint64_t Uint64;

typedef Uint32 URegister;

typedef std::pair< std::string, std::string > StringPair;

template<typename TYPE>
class Vec2t;

template<typename TYPE>
class Vec3t;

typedef Vec2t<float> Vec2f;
typedef Vec3t<float> Vec3f;

template<class TYPE_TO, class TYPE_FROM >
inline TYPE_TO union_cast( TYPE_FROM in ) {
    static_assert( sizeof(TYPE_TO) == sizeof(TYPE_FROM), "Types differ in size." );
    union {
        TYPE_TO to;
        TYPE_FROM from;
    } data;
    data.from = in;
    return data.to;
}

class Quat;
class DualQuat;
class AABox3D;
class Box3D;
class Cone3D;
class Sphere;
class Cylinder;
class Capsule;
class Plane3f;

class Line3f;
class Line2f;

class Mat3f;
class Mat4f;

const uint MAX_UINT = std::numeric_limits<uint>::max();
const int MAX_INT = std::numeric_limits<int>::max();

const uint MAX_UINT8 = std::numeric_limits<Uint8>::max();
const int MAX_INT8 = std::numeric_limits<int8>::max();

const uint MAX_UINT16 = std::numeric_limits<Uint16>::max();
const int MAX_INT16 = std::numeric_limits<int16>::max();

const uint MAX_UINT32 = std::numeric_limits<Uint32>::max();
const int MAX_INT32 = std::numeric_limits<int32>::max();

const float MIN_FLOAT = std::numeric_limits<float>::min();
const float MAX_FLOAT = std::numeric_limits<float>::max();

const std::string EMPTY_STRING="";

#endif  // SRC_CLIB_TYPES_H_
