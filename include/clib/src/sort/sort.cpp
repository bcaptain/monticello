// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./sort.h"


/********
 *
 * Quick Sort - BROKEN
 *
 * **********/
/* c#
private void btnQuickSort_Click(object sender, EventArgs e)
{
    if (mytype == typeof(int) )
    {
        QuickSort<int>(data, 0, data.Count - 1);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string) )
    {
        QuickSort<std::string>(data, 0, data.Count - 1);
        DisplayData<std::string>();
    }
}

private void QuickSort<TYPE>( ArrayList myDataStructure, int left, int right ) where TYPE : IComparable<TYPE>
{
    if (left >= right)
        return;

    TYPE pivot = (TYPE)myDataStructure[(right - left) / 2 + left];

    int i = left;
    int k = right;

    while (i <= k)
    {

        // find larger before
        while (((TYPE)myDataStructure[i]).CompareTo((TYPE)pivot) < 0)
            ++i

        // find smaller after
        while (((TYPE)myDataStructure[k]).CompareTo((TYPE)pivot) > 0)
            --k;

        if (i < k)
            DoSwap<TYPE>(myDataStructure, i, k);

        ++i
        --k;
    }

    QuickSort<TYPE>(myDataStructure, left, k);
    QuickSort<TYPE>(myDataStructure, i, right);
}
*/
/**************
 *
 * Bubble Sort
 *
 * **********/
/* c#
private void btnBubbleSort_Click(object sender, EventArgs e)
{
    DoBubbleSort();
}

public void DoBubbleSort()
{
    if (mytype == typeof(int))
    {
        BubbleSort<int>(data);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        BubbleSort<std::string>(data);
        DisplayData<std::string>();
    }
}

private void BubbleSort<TYPE>(ArrayList myDataStructure) where TYPE : IComparable<TYPE>
{
    int num = myDataStructure.Count;
    bool sorted = true;
    for (int i=0; i < num - 1 && sorted; --num )
    {
        sorted = false;

        for (int k=1; k < num; ++k)
        {
            if ( ( (TYPE)myDataStructure[k] ).CompareTo( (TYPE) myDataStructure[k - 1]) >= 0)
                continue;

            DoSwap<TYPE>(myDataStructure, k - 1, k);
            sorted = true;
        }
    }
}
*/
/**************
 *
 * Insertion Sort
 *
 * **********/
/* c#
private void btnInsertionSort_Click(object sender, EventArgs e)
{
    if (mytype == typeof(int))
    {
        InsertionSort<int>(data);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string) )
    {
        InsertionSort<std::string>(data);
        DisplayData<std::string>();
    }
}

private void InsertionSort<TYPE>(ArrayList myDataStructure) where TYPE : IComparable<TYPE>
{
    int k;
    for (int i = 1; i < myDataStructure.Count; ++i )
    {
        for ( k = i; k > 0; --k )
        {
            if ( ( (TYPE) myDataStructure[i] ).CompareTo( (TYPE) myDataStructure[k-1] ) >= 0)
                break;
        }
        if (i != k)
        {
            DoSwap<TYPE>(myDataStructure, k, i);

            // we have to recheck i since it was swapped with something else;
            --i;
        }
    }
}
*/
/**************
 *
 * Knuth Shuffle
 *
 * **********/
/* c#
private void btnKnuthShuffle_Click(object sender, EventArgs e)
{
    KnuthShuffle(data);

    if (mytype == typeof(int))
    {
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        DisplayData<std::string>();
    }
}

private void KnuthShuffle(ArrayList myDataStructure)
{
    if (mytype == typeof(int))
    {
        for (int i = 0; i < myDataStructure.Count; ++i )
            DoSwap<int>(data, i, random.Next(0, myDataStructure.Count-1));
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        for (int i = 0; i < myDataStructure.Count; ++i )
            DoSwap<std::string>(data, i, random.Next(0, myDataStructure.Count-1));
        DisplayData<std::string>();
    }
}
*/
/**************
 *
 * Cocktail Sort
 *
 * **********/
/* C#
private void btnCockTailSort_Click(object sender, EventArgs e)
{
    if (mytype == typeof(int))
    {
        CocktailSort<int>(data);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        CocktailSort<std::string>(data);
        DisplayData<std::string>();
    }
}

private void CocktailSort<TYPE>(ArrayList myDataStructure) where TYPE : IComparable<TYPE>
{
    int first = 0;
    int last = myDataStructure.Count - 1;
    int selection;
    int i;

    if ( last < 1 )
        return;

    while ( true )
    {
        // find larger value if it exists while going up the list
        selection = first;
        for (i = selection + 1; i < last; ++i )
            if (((TYPE)myDataStructure[i]).CompareTo((TYPE)myDataStructure[selection]) > 0)
                selection = i;

        // swap them if 'last' isnt larger
        if (((TYPE)myDataStructure[selection]).CompareTo((TYPE)myDataStructure[last]) > 0)
            DoSwap<TYPE>(myDataStructure, last, selection);

        // index "last" is in position now
        --last;
        if (first == last)
            break;

        // find smaller value if it exists while going down the list
        selection = last;
        for (i = selection - 1; i > first; --i )
            if (((TYPE)myDataStructure[i]).CompareTo((TYPE)myDataStructure[selection]) < 0)
                selection = i;

        // swap them if 'first' isnt smaller
        if (((TYPE)myDataStructure[selection]).CompareTo((TYPE)myDataStructure[first]) < 0)
            DoSwap<TYPE>(myDataStructure, first, selection);

        // index "first" is in position now
        ++first
        if (first == last)
            break;
    }
}
*/
/**************
 *
 * Recursive Cocktail Sort
 *
 * **********/
/* C#
private void btnRecursiveCocltailSort_Click(object sender, EventArgs e)
{
    if (mytype == typeof(int))
    {
        RecursiveCocktailSort<int>(data, 0, data.Count - 1);
        DisplayData<int>();
    }
    else if (mytype == typeof(std::string))
    {
        RecursiveCocktailSort<std::string>(data, 0, data.Count - 1);
        DisplayData<std::string>();
    }
}

private void RecursiveCocktailSort<TYPE>(ArrayList myDataStructure, int first, int last) where TYPE : IComparable<TYPE>
{
    int selection;
    int i;

    if ( last < 1 )
        return;

    // find larger value if it exists while going up the list
    selection = first;
    for (i = selection + 1; i < last; ++i )
        if (((TYPE)myDataStructure[i]).CompareTo((TYPE)myDataStructure[selection]) > 0)
            selection = i;

    // swap them if 'last' isnt larger
    if (((TYPE)myDataStructure[selection]).CompareTo((TYPE)myDataStructure[last]) > 0)
        DoSwap<TYPE>(myDataStructure, last, selection);

    // index "last" is in position now
    --last;
    if (first == last)
        return;

    // find smaller value if it exists while going down the list
    selection = last;
    for (i = selection - 1; i > first; --i )
        if (((TYPE)myDataStructure[i]).CompareTo((TYPE)myDataStructure[selection]) < 0)
            selection = i;

    // swap them if 'first' isnt smaller
    if (((TYPE)myDataStructure[selection]).CompareTo((TYPE)myDataStructure[first]) < 0)
        DoSwap<TYPE>(myDataStructure, first, selection);

    // index "first" is in position now
    ++first
    if (first == last)
        return;

    RecursiveCocktailSort<TYPE>(data, first, last);
}

*/
