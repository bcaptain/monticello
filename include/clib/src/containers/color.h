// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CONTAINERS_COLOR_H_
#define SRC_CONTAINERS_COLOR_H_

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"

class Color4f {
public:
    Color4f( void );
    Color4f( const float red, const float green, const float blue, const float alpha = 1.0f );
    Color4f( const Color4f& other );
    ~Color4f( void ) = default;

public:
    Color4f& operator=( const Color4f& other );

public:
    void Set( const float red, const float green, const float blue, const float alpha = 1.0f );

public:
    float r;
    float g;
    float b;
    float a;
};

#endif  //SRC_CONTAINERS_COLOR_H_
