// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "../clib.h"

#ifdef CLIB_UNIT_TEST
    void UnitTest_LinkList( void ) {
        {
            LinkList< int > list;

            list.Append(0);         ASSERT( list.Num() == 1 );
            list.Append(1);         ASSERT( list.Num() == 2 );
            list.GetLast()->Del();  ASSERT( list.Num() == 1 );
            list.Append(1);         ASSERT( list.Num() == 2 );
            list.Append(2);         ASSERT( list.Num() == 3 );

            int i=0;
            for ( auto& item : list )
                ASSERT( item == i++ );

            list.DelAll();          ASSERT( list.Num() == 0 );
        }
        {
            LinkList< int > list;
            ASSERT( list.GetFirst() == nullptr );
            ASSERT( list.GetLast() == nullptr );

            list.Append( 1 );

            Link< int > *first = list.GetFirst();
            ASSERT( first );
            ASSERT( first->Data() == 1 );

            Link< int > *last = list.GetLast();
            ASSERT( last );
            ASSERT( last->Data() == 1 );

            ASSERT( first == last );
            ASSERT( list.Num() == 1 );
        }
        {
            LinkList< int > list{1,2,3,4,5,6,7,8,9};
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // link FIRST AFTER itself - no change
            LinkList< int > list{1,2,3,4,5,6,7,8,9};
            Link< int >* link = list.GetFirst();
            link->LinkAfter( *link );
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // link FIRST BEFORE itself - no change
            LinkList< int > list{1,2,3,4,5,6,7,8,9};
            Link< int >* link = list.GetFirst();
            link->LinkBefore( *link );
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // link LAST AFTER itself - no change
            LinkList< int > list{1,2,3,4,5,6,7,8,9};
            Link< int >* link = list.GetLast();
            link->LinkAfter( *link );
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // link LAST BEFORE itself - no change
            LinkList< int > list{1,2,3,4,5,6,7,8,9};
            Link< int >* link = list.GetLast();
            link->LinkBefore( *link );
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // put first after last
            LinkList< int > list{9,1,2,3,4,5,6,7,8};
            Link< int >* link = list.GetFirst();
            link->LinkAfter( *list.GetLast() );
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // put first before last
            LinkList< int > list{8,1,2,3,4,5,6,7,9};
            Link< int >* link = list.GetFirst();
            link->LinkBefore( *list.GetLast() );
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            // selection sort
            LinkList< int > list{7,2,9,6,1,4,3,8,5};
            list.SortData();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        { // move forward once
            LinkList< int > list{2,1,3,4,5,6,7,8,9};
            Link< int >* link = list.GetFirst();
            link->MoveForward();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        { // move forward twice
            LinkList< int > list{3,1,2,4,5,6,7,8,9};
            Link< int >* link = list.GetFirst();
            link->MoveForward();
            link->MoveForward();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        { // move backward once
            LinkList< int > list{1,2,3,4,5,6,7,9,8};
            Link< int >* link = list.GetLast();
            link->MoveBackward();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        { // move backward twice
            LinkList< int > list{1,2,3,4,5,6,8,9,7};
            Link< int >* link = list.GetLast();
            link->MoveBackward();
            link->MoveBackward();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            LinkList< int > list{9,1,2,3,4,5,6,7,8};
            Link< int >* link = list.GetFirst();
            link->MoveToLast();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            LinkList< int > list{2,3,4,5,6,7,8,9,1};
            Link< int >* link = list.GetLast();
            link->MoveToFirst();
            int i=1;
            for ( auto& item : list )
                ASSERT( item == i++ );
        }
        {
            LinkList< int > list{9,8,7,6,5,4,3,2,1};
            int i=1;
            for ( auto itr = std::rbegin( list ); itr != std::rend( list ); ++itr )
                ASSERT( *itr == i++ );
        }
        {
            LinkList< int > list{9,8,7,6,5,4,3,2,1};
            int i=9;
            for ( Link< int> *link = list.GetFirst(); link != nullptr; link=link->Del() ) {
                ASSERT( list.Num() == static_cast<uint>(i) );
                ASSERT( link->Data() == i-- );
            }
            ASSERT( list.Num() == 0 );
        }
        {
            LinkList< int > list{1,2,3,4,5,6,7,8,9};
            int i=9;
            for ( Link< int> *link = list.GetLast(); link != nullptr; link=link->Del_Reverse() ) {
                ASSERT( list.Num() == static_cast<uint>(i) );
                ASSERT( link->Data() == i-- );
            }
            ASSERT( list.Num() == 0 );
        }
        { // swap links between two lists
            LinkList< int > list1{11,2,3,4,5,6,7,8,9};
            LinkList< int > list2{1,12,13,14,15,16,17,18,19};
            list1.GetFirst()->SwapPositions( *list2.GetFirst() );
            int i=1;
            for ( Link<int> *link=list1.GetFirst(); link!=nullptr; link=link->GetNext() ) {
                ASSERT( link->Data() == i++ );
                ASSERT( link->GetMaster() == &list1 );
            }
            i=11;
            for ( Link<int> *link=list2.GetFirst(); link!=nullptr; link=link->GetNext() ) {
                ASSERT( link->Data() == i++ );
                ASSERT( link->GetMaster() == &list2 );
            }

        }
        { // swap links between two lists
            LinkList< int > list1{1,2,3,4,5,6,7};
            LinkList< int > list2{11,12,13,14,15,16,17,18,19};

            const uint num1 = list1.Num();
            const uint num2 = list2.Num();

            list1.Swap( list2 );

            ASSERT( list1.Num() == num2 );
            ASSERT( list2.Num() == num1 );

            int i=11;
            for ( Link<int> *link=list1.GetFirst(); link!=nullptr; link=link->GetNext() ) {
                ASSERT( link->Data() == i++ );
                ASSERT( link->GetMaster() == &list1 );
            }
            i=1;
            for ( Link<int> *link=list2.GetFirst(); link!=nullptr; link=link->GetNext() ) {
                ASSERT( link->Data() == i++ );
                ASSERT( link->GetMaster() == &list2 );
            }
        }
    }
#endif // CLIB_UNIT_TEST
