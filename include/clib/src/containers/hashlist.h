// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_CONTAINERS_HASHLIST_H
#define SRC_CLIB_CONTAINERS_HASHLIST_H

#include "../message.h"
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "../strings.h"
#include "../math/values.h"
#include "../hash.h"
#include "./linklist.h"

const std::size_t HASHLIST_DEFAULT_SIZE = 1009;

/*!

HashList \n\n

Stores a (potentially large) array of pointers to linked lists of pairs. The LinkList
is used to prevent collisions in instances where one key hashes to the same value (array
index) as another different key. \n\n

Does not allocate until something is added. \n

**/

#ifdef CLIB_UNIT_TEST
    void UnitTest_HashList( void );
#endif // CLIB_UNIT_TEST

template< typename TYPE >
class HashList {
public:
    HashList( void );
    explicit HashList( const std::size_t sz );
    HashList( const HashList<TYPE> &other );
    HashList( HashList<TYPE>&& other_rref );
    virtual ~HashList( void );

public:
    HashList<TYPE>& operator=( const HashList<TYPE>& other );
    HashList<TYPE>& operator=( HashList<TYPE>&& other_rref );
    HashList<TYPE>& operator=( const std::vector< std::pair<TYPE,TYPE> >& vec );
    HashList<TYPE>& operator==( const HashList<TYPE>& other ) const = delete;
    HashList<TYPE>& operator!=( const HashList<TYPE>& other ) const = delete;

public:
    bool Set( const char * key, const TYPE& val ); //!< returns true if any value existed before the set
    bool Set( const char * key, TYPE&& val_rref ); //!< returns true if any value existed before the set
    bool Set( const char * key ); //!< returns true if any value existed before the set
    bool Set( const Hash& hashkey, const TYPE& val ); //!< returns true if any value existed before the set
    bool Set( const Hash& hashkey, TYPE&& val_rref ); //!< returns true if any value existed before the set

    bool Update( const char * key, const TYPE& val ); //!< returns true if the previous value was different from the provided value
    bool Update( const char * key, TYPE&& val_rref ); //!< returns true if the previous value was different from the provided value
    bool Update( const Hash& hashkey, const TYPE& val ); //!< returns true if the previous value was different from the provided value
    bool Update( const Hash& hashkey, TYPE&& val_rref ); //!< returns true if the previous value was different from the provided value

    TYPE * Get( const char * key );
    TYPE * Get( const Hash& hashkey );

    const TYPE* Get( const char * key ) const;
    const TYPE * Get( const Hash& hashkey ) const;

    void Unset( const char * key ); //!< deletes the pair from memory
    void Unset( const Hash& hashkey ); //!< deletes the pair from memory
    void Unset( const std::size_t hash, const char* key ); //!< deletes the pair from memory

    void Merge( const HashList<TYPE>& other );
    void Merge( const std::vector< std::pair<TYPE,TYPE> >& vec );

private:
    TYPE * Get( const std::size_t hash, const char* key );
    const TYPE * Get( const std::size_t hash, const char* key ) const;

    bool Set( const std::size_t hash, const char* key, const TYPE& val ); //!< returns true if any value existed before the set
    bool Set( const std::size_t hash, const char* key, TYPE&& val_rref ); //!< returns true if any value existed before the set

    bool Update( const std::size_t hash, const char* key, const TYPE& val ); //!< returns true if the value was different from the provided value
    bool Update( const std::size_t hash, const char* key, TYPE&& val_rref ); //!< returns true if the value was different from the provided value

public:
    std::size_t GetTableSize( void ) const;

    void Clear( void );

    std::pair<std::string, TYPE> * GetKVPair( const char * key ); //!< return the container for the key

    uint Remove( const char* key ); //!< returns how many of this key were removed (should be 1)
    void DestroyAndResize( const std::size_t newsize );

    Link<std::pair<std::string, TYPE> >* GetFirst( std::size_t& index ); //!< get the first item. the index is an iterator for the internal array, and will be set and used by this function.
    Link<std::pair<std::string, TYPE> >* GetLast( std::size_t& index );  //!< get the last item. the index is an iterator for the internal array, and will be set and used by this function.
    Link<std::pair<std::string, TYPE> >* GetNext( Link<std::pair<std::string, TYPE> >* link, std::size_t& index ); //!< get the next item in the list. index is the iterator for the internal array, and will be set and used by this function.
    Link<std::pair<std::string, TYPE> >* GetPrevious( Link<std::pair<std::string, TYPE> >* link, std::size_t& index ); //!< get the next item in the list. index is the iterator for the internal array, and will be set and used by this function.

    const Link<std::pair<std::string, TYPE> >* GetFirst( std::size_t& index ) const;  //!< get the first item. the index is an iterator for the internal array, and will be set and used by this function.
    const Link<std::pair<std::string, TYPE> >* GetLast( std::size_t& index ) const;  //!< get the last item. the index is an iterator for the internal array, and will be set and used by this function.
    const Link<std::pair<std::string, TYPE> >* GetNext( const Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) const; //!< get the next item in the list. index is the iterator for the internal array, and will be set and used by this function.
    const Link<std::pair<std::string, TYPE> >* GetPrevious( const Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) const; //!< get the next item in the list. index is the iterator for the internal array, and will be set and used by this function.

    Link<std::pair<std::string, TYPE> >* Del( Link<std::pair<std::string, TYPE> >* link, std::size_t& index );
    Link<std::pair<std::string, TYPE> >* Del_Reverse( Link<std::pair<std::string, TYPE> >* link, std::size_t& index );

protected:
    void    EnsureAllocated( void ); //!< allocate space for hash list. won't reallocate if called twice.

protected:
    LinkList< std::pair<std::string, TYPE> > ** table;
    std::size_t size; //!< size of hash table, use only prime numbers and set only in constructors

public:

// **** for compatibility with STL

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Weffc++" // complains about std::*iterator* not having virutal destructor
    template< typename HASHCLASS, typename LINKTYPE, typename LINK >
    struct IteratorBase : public std::iterator< std::bidirectional_iterator_tag, TYPE > {
    #pragma GCC diagnostic pop
        friend class HashList<TYPE>;

        IteratorBase( void )
            : hash_table_index( 0 )
            , hashlist( nullptr )
            , cur( nullptr )
        { }

        explicit IteratorBase( HASHCLASS* hl )
            : hashlist( hl )
            , hash_table_index( 0 )
            , cur( nullptr )
        { }

        IteratorBase operator++( int ) {
            Link<TYPE> *old = cur;

            if ( hashlist ) {
                cur = hashlist->GetNext(cur,hash_table_index);
                return old;
            }

            cur = nullptr;
            return old;
        }

        IteratorBase operator--( int ) {
            Link<TYPE> *old = cur;

            if ( hashlist ) {
                cur = hashlist->GetPrevious(cur,hash_table_index);

                return old;
            }

            cur = nullptr;
            return old;
        }

        IteratorBase& operator++( void ) {
            if ( hashlist ) {
                cur = hashlist->GetNext(cur,hash_table_index);
                return *this;
            }

            cur = nullptr;
            return *this;
        }

        IteratorBase& operator--( void ) {
            if ( hashlist ) {
                cur = hashlist->GetPrevious(cur,hash_table_index);
                return *this;
            }

            cur = nullptr;
            return *this;
        }

        bool operator==( const IteratorBase& other ) const { return cur == other.cur && hashlist == other.hashlist; }
        bool operator!=( const IteratorBase& other ) const { return !operator==(other); }

        LINKTYPE& operator*( void ) { return cur->Data(); }
        LINKTYPE* operator->( void ) { return &cur->Data(); }

        private:
            HASHCLASS *hashlist;
            std::size_t hash_table_index;
            LINK *cur;
    };

    typedef IteratorBase<
        HashList<TYPE>,
        std::pair<std::string,TYPE>,
        Link< std::pair<std::string,TYPE> >
    > Iterator;

    typedef IteratorBase<
        const HashList<TYPE>,
        const std::pair<std::string,TYPE>,
        const Link< std::pair<std::string,TYPE> >
    > ConstIterator;

    typedef ReverseIteratorBase<
        IteratorBase<
            HashList<TYPE>,
            std::pair<std::string,TYPE>,
            Link< std::pair<std::string,TYPE> >
        >,
        HashList<TYPE>
    > ReverseIterator;
    
    typedef ReverseIteratorBase<
        IteratorBase<
            const HashList<TYPE>,
            const std::pair<std::string,TYPE>,
            const Link< std::pair<std::string,TYPE> >
        >,
        const HashList<TYPE>
    > ConstReverseIterator;
    
    Iterator end( void ) { return Iterator( this ); }
    Iterator begin( void ) {
        Iterator itr( this );
        itr.cur = GetFirst( itr.hash_table_index );
        return itr;
    }

    ConstIterator end( void ) const { return ConstIterator( this ); }
    ConstIterator cbegin( void ) const { return begin(); }
    ConstIterator cend( void ) const { return end(); }
    ConstIterator begin( void ) const {
        ConstIterator itr( this );
        itr.cur = GetFirst( itr.hash_table_index );
        return itr;
    }

    ReverseIterator rend( void ) { return ReverseIterator( this ); }
    ReverseIterator rbegin( void ) {
        ReverseIterator itr( this );
        itr.cur = GetLast( itr.hash_table_index );
        return itr;
    }

    ConstReverseIterator rend( void ) const { return ConstReverseIterator( this ); }
    ConstReverseIterator crbegin( void ) const { return rbegin(); }
    ConstReverseIterator crend( void ) const { return rend(); }
    ConstReverseIterator rbegin( void ) const {
        ConstReverseIterator itr( this );
        itr.cur = GetLast( itr.hash_table_index );
        return itr;
    }
};

template<typename TYPE>
void HashList<TYPE>::DestroyAndResize( const std::size_t newsize ) {
    Clear();
    DELNULLARRAY( table );
    if ( !Maths::IsPrime( newsize ) ) {
        ERR("Bad hashlist size, must be prime. Initializing to default.\n");
        size = HASHLIST_DEFAULT_SIZE;
    } else {
        size = newsize;
    }
}

template<typename TYPE>
HashList<TYPE>::HashList( const HashList<TYPE>& other )
    : table(nullptr)
    , size(0)
{
    *this = other;
}

template<typename TYPE>
HashList<TYPE>::HashList( HashList<TYPE>&& other_rref )
    : table(other_rref.table)
    , size(other_rref.size)
{
    other_rref.table = nullptr;
    other_rref.size = 0;
}

template<typename TYPE>
HashList<TYPE>::HashList( void )
    : table(nullptr)
    , size(0)
{
    // does not allocate until something is added
}

template<typename TYPE>
HashList<TYPE>::HashList( const std::size_t sz )
    : table(nullptr)
    , size(sz)
{
    // does not allocate until something is added
}

template<typename TYPE>
void HashList<TYPE>::EnsureAllocated( void ) {
    if ( table )
        return;

    if ( size == 0 )
        size = HASHLIST_DEFAULT_SIZE;

    if ( !Maths::IsPrime( size ) ) {
        ASSERT( false );
        ERR("Bad hashlist size, must be prime.\n");
        return;
    }

    table = new LinkList< std::pair<std::string, TYPE> >*[size]{ nullptr };
}

template<typename TYPE>
HashList<TYPE>::~HashList( void ) {
    Clear();
    DELNULLARRAY( table );
}

template<typename TYPE>
void HashList<TYPE>::Clear( void ) {
    if ( table )
        for ( std::size_t i=0; i<size; ++i)
            DELNULL( table[i] );
}

template<typename TYPE>
HashList<TYPE>& HashList<TYPE>::operator=( const std::vector< std::pair<TYPE,TYPE> >& vec ) {
    Clear();
    Merge( vec );
    return *this;
}

template<typename TYPE>
HashList<TYPE>& HashList<TYPE>::operator=( HashList<TYPE> &&other_rref ) {
    if ( this != &other_rref ) {
        Clear();
        DELNULLARRAY( table );

        size = other_rref.size;
        table = other_rref.table;

        other_rref.table = nullptr;
        other_rref.size = 0;
    }
    return *this;
}

template<typename TYPE>
HashList<TYPE>& HashList<TYPE>::operator=( const HashList<TYPE> &other ) {
    if ( this == &other )
        return *this;

    if ( !other.table ) { // other hashlist does't exist, so delete this one!
        Clear();
        DELNULLARRAY( table );
        size = other.size;
        return *this;
    } else if ( !table ) { // this hashlist doesn't exist, so create it as same size as other
        size = other.size;
        EnsureAllocated();
    } else if ( other.size == size ) { // sizes are the same, so just clear
        Clear();
    } else { // sizes are different, so delete and reallocate this one
        Clear();
        DELNULLARRAY( table );
        size = other.size;
        EnsureAllocated();
    }

    // copy elements
    uint i = size;
    while ( i>0 ) {
        --i;
        if ( other.table[i] ) {
            table[i] = new LinkList< std::pair<std::string, TYPE> >( *other.table[i] );
        }
    }

    return *this;
}

template<typename TYPE>
inline std::size_t HashList<TYPE>::GetTableSize( void ) const {
    return size;
}

template<typename TYPE>
inline bool HashList<TYPE>::Set( const char * key ) {
    return Set( key, TYPE() );
}

template<typename TYPE>
bool HashList<TYPE>::Set( const char * key, const TYPE& val ) {
    EnsureAllocated();

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return false;

    const std::size_t idx=Hashes::OAT(key, size);
    return Set( idx, key, val );
}

template<typename TYPE>
bool HashList<TYPE>::Set( const char * key, TYPE&& val_rref ) {
    EnsureAllocated();

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return false;

    const std::size_t idx=Hashes::OAT(key, size);
    return Set( idx, key, std::move(val_rref) );
}

template<typename TYPE>
bool HashList<TYPE>::Update( const char * key, const TYPE& val ) {
    EnsureAllocated();

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return false;

    const std::size_t idx=Hashes::OAT(key, size);
    return Update( idx, key, val );
}

template<typename TYPE>
bool HashList<TYPE>::Update( const char * key, TYPE&& val_rref ) {
    EnsureAllocated();

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return false;

    const std::size_t idx=Hashes::OAT(key, size);
    return Update( idx, key, std::move(val_rref) );
}

template<typename TYPE>
std::pair<std::string, TYPE> * HashList<TYPE>::GetKVPair( const char * key ) {
    if ( !table ) // if not initialized yet
        return nullptr;

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return nullptr;

    const std::size_t idx=Hashes::OAT(key, size);

    if ( table[idx] == nullptr )
        return nullptr;

    Link< std::pair<std::string, TYPE> > * list;

    for ( auto& kvp : *table[idx] )
        if ( kvp.first == key )
            return kvp;

    return nullptr;
}

template<typename TYPE>
inline TYPE * HashList<TYPE>::Get( const char* key ) {
    return const_cast< TYPE* >( const_cast< const HashList<TYPE>* >( this )->Get( key ) );
}

template<typename TYPE>
const TYPE * HashList<TYPE>::Get( const char* key ) const {
    if ( !table ) // if not initialized yet
        return nullptr;

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return nullptr;

    const std::size_t idx=Hashes::OAT(key, size);

    return Get( idx, key );
}

template<typename TYPE>
uint HashList<TYPE>::Remove( const char* key ) {
    if ( !table ) // if not initialized yet
        return 0;

    Link< std::pair<std::string, TYPE> > * list;

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return 0;

    const std::size_t idx=Hashes::OAT(key, size);

    if ( table[idx] == nullptr )
        return 0;

    uint deleted = 0;
    list=table[idx]->GetFirst();
    while ( list != nullptr ) {
        if ( list->Data().first == key ) {
            list=list->Del();
            ++deleted;
        } else {
            list=list->GetNext();
        }
    }

    return deleted;
}

template<typename TYPE>
inline bool HashList<TYPE>::Set( const Hash& hashkey, TYPE&& val_rref ) {
    return Set(hashkey.second, hashkey.first, val_rref );
}

template<typename TYPE>
inline bool HashList<TYPE>::Set( const Hash& hashkey, const TYPE& val ) {
    return Set(hashkey.second, hashkey.first, val );
}

template<typename TYPE>
inline bool HashList<TYPE>::Update( const Hash& hashkey, TYPE&& val_rref ) {
    return Update(hashkey.second, hashkey.first, val_rref );
}

template<typename TYPE>
inline bool HashList<TYPE>::Update( const Hash& hashkey, const TYPE& val ) {
    return Update(hashkey.second, hashkey.first, val );
}


template<typename TYPE>
bool HashList<TYPE>::Set( const std::size_t hash, const char* key, const TYPE& val ) {
    EnsureAllocated();

    if ( table[hash] == nullptr ) {
        table[hash]=new LinkList< std::pair<std::string, TYPE> >;
    }else{
        // check if the key already exists
        for ( auto& pair : *table[hash] ) {
            if ( pair.first == key ) {
                pair.second = val;
                return true;
            }
        }

        // we will add a colliding element
    }

    table[hash]->Append( std::pair<std::string, TYPE>(key, val) );
    return false;
}

template<typename TYPE>
bool HashList<TYPE>::Set( const std::size_t hash, const char* key, TYPE&& val_rref ) {
    EnsureAllocated();

    if ( table[hash] == nullptr ) {
        table[hash]=new LinkList< std::pair<std::string, TYPE> >;
    }else{
        // check if the key already exists
        for ( auto& pair : *table[hash] ) {
            if ( pair.first == key ) {
                pair.second = std::move( val_rref );
                return true;
            }
        }

        // we will add a colliding element
    }

    table[hash]->Append( std::pair<std::string, TYPE>(key, std::move(val_rref) ) );
    return false;
}

template<typename TYPE>
bool HashList<TYPE>::Update( const std::size_t hash, const char* key, const TYPE& val ) {
    EnsureAllocated();

    if ( table[hash] == nullptr ) {
        table[hash]=new LinkList< std::pair<std::string, TYPE> >;
    }else{
        // check if the key already exists
        for ( auto& pair : *table[hash] ) {
            if ( pair.first == key ) {
                if ( pair.second == val ) {
                    return false;
                }
                
                pair.second = val;
                return true;
            }
        }

        // we will add a colliding element
    }

    table[hash]->Append( std::pair<std::string, TYPE>(key, val) );
    return true;
}

template<typename TYPE>
bool HashList<TYPE>::Update( const std::size_t hash, const char* key, TYPE&& val_uref ) {
    EnsureAllocated();

    if ( table[hash] == nullptr ) {
        table[hash]=new LinkList< std::pair<std::string, TYPE> >;
    }else{
        // check if the key already exists
        for ( auto& pair : *table[hash] ) {
            if ( pair.first == key ) {
                if ( pair.second == val_uref ) {
                    return false;
                }
                
                pair.second = std::forward( val_uref );
                return true;
            }
        }

        // we will add a colliding element
    }

    table[hash]->Append( std::pair<std::string, TYPE>(key, std::forward(val_uref) ) );
    return true;
}

template<typename TYPE>
void HashList<TYPE>::Unset( const char * key ) {
    EnsureAllocated();

    if ( !key || key[0] == '\0' ) // if key is an invalid value
        return;

    const std::size_t idx=Hashes::OAT(key, size);
    return Unset( idx, key );
}

template<typename TYPE>
inline void HashList<TYPE>::Unset( const Hash& hashkey ) {
    return Unset(hashkey.second, hashkey.first );
}

template<typename TYPE>
void HashList<TYPE>::Unset( const std::size_t hash, const char* key ) {
    EnsureAllocated();

    if ( table[hash] == nullptr )
        return;

    // check if the key already exists
    Link< std::pair<std::string, TYPE> >* link = table[hash]->GetFirst();
    while ( link ) {
        
        if ( link->Data().pair.first == key ) {
            link->Del();
            return;
        }
        
        link = link->GetNext();
    }
}

template<typename TYPE>
inline TYPE * HashList<TYPE>::Get( const Hash& hashkey ) {
    return Get( hashkey.second, hashkey.first );

}

template<typename TYPE>
inline const TYPE * HashList<TYPE>::Get( const Hash& hashkey ) const {
    return Get( hashkey.second, hashkey.first );
}

template<typename TYPE>
TYPE * HashList<TYPE>::Get( const std::size_t hash, const char* key ) {
    return const_cast< TYPE* >( const_cast< const HashList<TYPE>* >( this )->Get( hash, key ) );
}

template<typename TYPE>
const TYPE * HashList<TYPE>::Get( const std::size_t hash, const char* key ) const {
    if ( !table ) // if not initialized yet
        return nullptr;
        
    if ( table[hash] == nullptr )
        return nullptr;

    for ( auto const& pair : *table[hash] )
        if ( pair.first == key )
            return &pair.second;

    return nullptr;
}

template<typename TYPE>
const Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetLast( std::size_t& index ) const {
    if ( !table || size < 1 )
        return nullptr;

    index = size;
    while ( index > 0 ) {
        --index;
        if ( table[index] )
            if ( table[index]->Num() > 0 )
                return table[index]->GetLast();
    }

    return nullptr;
}

template<typename TYPE>
const Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetFirst( std::size_t& index ) const {
    if ( !table || size < 1 )
        return nullptr;

    index = 0;
    while ( index < size ) {
        if ( table[index] )
            if ( table[index]->Num() > 0 )
                return table[index]->GetFirst();
        ++index;
    }

    return nullptr;
}

template<typename TYPE>
const Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetNext( const Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) const {
    if ( link ) {
        link = link->GetNext();
        if ( link )
            return link;
    }

    for ( ++index; index < size; ++index ) {
        if ( table[index] )
            if ( table[index]->Num() > 0 )
                return table[index]->GetFirst();
    }

    return nullptr;
}

template<typename TYPE>
const Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetPrevious( const Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) const {
    if ( link ) {
        link = link->GetPrevious();
        if ( link )
            return link;
    }

    while ( index > 0 ) {
        --index;
        if ( table[index] )
            if ( table[index]->Num() > 0 )
                return table[index]->GetLast();
    }

    return nullptr;
}

template<typename TYPE>
Link<std::pair<std::string, TYPE> >* HashList<TYPE>::Del( Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) {
    if ( link ) {
        link = link->Del();

        if ( link )
            return link;
    }

    return GetNext( link, index );
}

template<typename TYPE>
Link<std::pair<std::string, TYPE> >* HashList<TYPE>::Del_Reverse( Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) {
    if ( link ) {
        link = link->Del_Reverse();

        if ( link )
            return link;
    }

    return GetPrevious( link, index );
}

template<typename TYPE>
Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetFirst( std::size_t& index ) {
    return const_cast< Link<std::pair<std::string, TYPE> >* >(
        const_cast< const HashList<TYPE>* >(this)->GetFirst(index)
    );
}

template<typename TYPE>
Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetLast( std::size_t& index ) {
    return const_cast< Link<std::pair<std::string, TYPE> >* >(
        const_cast< const HashList<TYPE>* >(this)->GetLast(index)
    );
}

template<typename TYPE>
Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetNext( Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) {
    return const_cast< Link<std::pair<std::string, TYPE> >* >(
        const_cast< const HashList<TYPE>* >(this)->GetNext(link, index)
    );
}

template<typename TYPE>
Link<std::pair<std::string, TYPE> >* HashList<TYPE>::GetPrevious( Link<std::pair<std::string, TYPE> >* link, std::size_t& index ) {
    return const_cast< Link<std::pair<std::string, TYPE> >* >(
        const_cast< const HashList<TYPE>* >(this)->GetPrevious(link, index)
    );
}

template<typename TYPE>
void HashList<TYPE>::Merge( const HashList<TYPE>& other ) {
    for ( auto& item : other )
        Set( item.first.c_str(), item.second );
}

template<typename TYPE>
void HashList<TYPE>::Merge( const std::vector< std::pair<TYPE,TYPE> >& vec ) {
    for ( auto& item : vec )
        Set( item.first.c_str(), item.second );
}

template< typename TYPE >
void ClearPointerHashList( HashList< TYPE* >& hashlist ) {
    std::size_t i;
    for ( auto link = hashlist.GetFirst(i); link; link = hashlist.Del( link, i ) )
       DELNULL( link->Data().second );
}

template< typename TYPE >
void FreePointerHashList( HashList< TYPE* >& hashlist, void(*free_func)(void*) ) {
    std::size_t i;
    for ( auto link = hashlist.GetFirst(i); link; link = hashlist.Del( link, i ) )
        (*free_func)( link->Data().second );
}

#endif  // SRC_CLIB_CONTAINERS_HASHLIST_H
