// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_LINKLIST_H_
#define SRC_CLIB_LINKLIST_H_

#include <cstdlib>
#include <cassert>
#include <initializer_list>
#include <iterator>
#include "../warnings.h"
#include "../macros.h"
#include "../std.h"
#include "../strings.h"
#include "../strings.h"
#include "../types.h"

#ifdef CLIB_UNIT_TEST
    void UnitTest_LinkList( void );
#endif // CLIB_UNIT_TEST

/*!

Link_LessThan \n\n

comparison function for link sorting

**/

template<typename TYPE>
class Link;

template<typename TYPE>
bool Link_LessThan( const Link<TYPE>& link, const Link<TYPE>& check ) {
    return link.Data() < check.Data();
}

/*!

Link \n\n

An element in a LinkList

**/

template<typename TYPE>
class LinkList;

template<typename TYPE>
class Link {
    friend class LinkList<TYPE>; // LinkList manages Links

    #ifdef CLIB_UNIT_TEST
        friend void UnitTest_LinkList( void );
    #endif // CLIB_UNIT_TEST

private: // constructing from links is only allowed for a LinkList (which is a friend class)
    Link( void );
    Link( LinkList<TYPE>* themaster, const TYPE& value );
    Link( LinkList<TYPE>* themaster, TYPE&& value_rref );

    // note: copying constructing/assigning and move constructing/assigning does not make sense for this structure
    Link( const Link<TYPE>& other ) = delete;
    Link<TYPE>& operator=( const Link<TYPE>& other ) = delete;

public:
    virtual ~Link( void );

public:
    Link<TYPE>& operator=( const TYPE& element ); //!< returns the current link
    Link<TYPE>& operator=( TYPE&& element_rref ); //!< returns the current link
    operator Link< const TYPE > ( void ) const; //!< conversion Operator
    
    Link<TYPE>& operator==( const Link<TYPE>& other ); //!< compares whether the link is the same memory address, does not compare data
    Link<TYPE>& operator!=( const Link<TYPE>& other ); //!< compares whether the link is the same memory address, does not compare data

public:
    TYPE& Data( void );
    const TYPE& Data( void ) const;

    Link<TYPE>* AddAfter( const TYPE& element ); //!< returns poitner to the new link
    Link<TYPE>* AddAfter( TYPE&& element_rref ); //!< returns poitner to the new link
    Link<TYPE>* AddBefore( const TYPE& element ); //!< returns poitner to the new link
    Link<TYPE>* AddBefore( TYPE && element_rref ); //!< returns poitner to the new link

    void Set( const TYPE& element );
    void Set( TYPE&& element_rref );

    Link<TYPE>* GetNext( void ); //!< return the next link
    Link<TYPE>* GetPrevious( void ); //!< return the previous link

    const Link<TYPE>* GetNext( void ) const;  //!< return the next link as const
    const Link<TYPE>* GetPrevious( void ) const;  //!< return the previous link as const

    LinkList<TYPE>* GetMaster( void ); //!< return the list the this link belongs to (which may change, for example if the link is put into another list)
    const LinkList<TYPE>* GetMaster( void ) const; //!< return the list the this link belongs to (which may change, for example if the link is put into another list)

    int Num( void );

    Link<TYPE>* Del( void ); //!< deletes the link and returns the next
    Link<TYPE>* Del_Reverse( void ); //!< deletes the link and returns the previous

    void MoveForward( void ); //!< move toward the back of the list by one
    void MoveBackward( void ); //!< move toward the fromt of the list by one

    void MoveToFirst( void );
    void MoveToLast( void );

    void SwapPositions( Link< TYPE >& with ); //!< works between two different linklists
    void SwapData( Link< TYPE >& with );

    void LinkAfter( Link<TYPE>& link ); //!< works between two different linklists
    void LinkBefore( Link<TYPE>& link ); //!< works between two different linklists
    void Unlink( void ); //!< remove us from the list but don't delete data

private:
    void AddAfter_Helper( Link<TYPE> *link );
    void AddBefore_Helper( Link<TYPE> *link );

private:
    TYPE val;

    /*! the master will never be deleted until you manually call delete on it,
        this ensures we always have a list, and your variable to that link is
        always valid */
    LinkList<TYPE> *master;

    Link<TYPE> *next;
    Link<TYPE> *previous;
};

template< typename TYPE >
inline Link<TYPE>& Link<TYPE>::operator==( const Link<TYPE>& other ) {
    return this == &other;
}

template< typename TYPE >
inline Link<TYPE>& Link<TYPE>::operator!=( const Link<TYPE>& other ) {
    return !operator==(other);
}

template< typename TYPE >
inline Link<TYPE>::operator Link< const TYPE > ( void ) const {
    return Link< const TYPE >( *this ); // conversion operator
}

template<typename TYPE>
Link<TYPE>::Link( void )
    : val(nullptr)
    , master(nullptr)
    , next(nullptr)
    , previous(nullptr)
    {
}

template<typename TYPE>
void Link<TYPE>::SwapPositions( Link< TYPE >& with ) {
    std::swap( master, with.master );
    std::swap( next, with.next );
    std::swap( previous, with.previous );

    if ( next ) next->previous = this;
    if ( previous ) previous->next = this;

    if ( with.next ) with.next->previous = &with;
    if ( with.previous ) with.previous->next = &with;

    if ( master ) {
        if ( master->first == &with ) {
            master->first = this;
        } else if ( master->last == &with ) {
            master->last = this;
        }
    }

    if ( with.master ) {
        if ( with.master->first == this ) {
            with.master->first = &with;
        } else if ( with.master->last == this ) {
            with.master->last = &with;
        }
    }
}

template<typename TYPE>
inline void Link<TYPE>::SwapData( Link< TYPE >& with ) {
    std::swap( val, with.val );
}

/*
template<typename TYPE>
Link<TYPE>::Link( const Link<TYPE>& other )
    : val( other.val )
    , master( other.master )
    , next( other.next )
    , previous( other.previous )
{ }


template<typename TYPE>
Link<TYPE>::Link( Link<TYPE>&& other_rref )
    : val( std::move(other_rref.val) )
    , master( other_rref.master )
    , next( other_rref.next )
    , previous( other_rrev.previous )
    {
    val = nullptr;
    master = nullptr;
    next = nullptr;
    previous = nullptr;
    if ( previous ) {
        previous->next = this;
    }
    if ( next )
        next->previous = this;
}
*/
template<typename TYPE>
Link<TYPE>::Link( LinkList<TYPE> *themaster, const TYPE& value )
    : val(value)
    , master( themaster )
    , next(nullptr)
    , previous(nullptr)
{
}

template<typename TYPE>
Link<TYPE>::Link( LinkList<TYPE> *themaster, TYPE&& value_rref )
    : val( std::move(value_rref) )
    , master( themaster )
    , next(nullptr)
    , previous(nullptr)
{
}
/*
template<typename TYPE>
Link<TYPE>& Link<TYPE>::operator=( const Link<TYPE>& other ) {
    if ( this != &other )
        val = other.val;

    return *this;
}

template<typename TYPE>
Link<TYPE>& Link<TYPE>::operator=( Link<TYPE>&& other_rref ) {
    if ( this != &other_rref )
        val = std::move( other_rref.val );

    return *this;
}
*/
template<typename TYPE>
Link<TYPE>::~Link( void ) {
    Unlink();
    if ( master )
        --master->num;
}

template<typename TYPE>
Link<TYPE>& Link<TYPE>::operator=( const TYPE& element ) {
    Set(element);
    return *this;
}

template<typename TYPE>
Link<TYPE>& Link<TYPE>::operator=( TYPE&& other_rref ) {
    Set( std::move(other_rref) );
    return *this;
}

template<typename TYPE>
inline const TYPE& Link<TYPE>::Data( void ) const {
    return val;
}

template<typename TYPE>
inline TYPE& Link<TYPE>::Data( void ) {
    return val;
}

template<typename TYPE>
inline Link<TYPE>* Link<TYPE>::GetNext( void ) {
    return next;
}

template<typename TYPE>
inline const Link<TYPE>* Link<TYPE>::GetNext( void ) const {
    return next;
}

template<typename TYPE>
inline Link<TYPE>* Link<TYPE>::GetPrevious( void ) {
    return previous;
}

template<typename TYPE>
inline const Link<TYPE>* Link<TYPE>::GetPrevious( void ) const {
    return previous;
}

template<typename TYPE>
inline void Link<TYPE>::Set( const TYPE& element ) {
    val = element;
}

template<typename TYPE>
inline void Link<TYPE>::Set( TYPE&& element_rref ) {
    val = std::move( element_rref );
}

template<typename TYPE>
Link<TYPE> * Link<TYPE>::AddAfter( TYPE&& element_rref ) {
    Link<TYPE> *link=new Link<TYPE>( master, std::move( element_rref ) );
    AddAfter_Helper( link );
    return link;
}

template<typename TYPE>
Link<TYPE> * Link<TYPE>::AddAfter( const TYPE& element ) {
    Link<TYPE> *link=new Link<TYPE>( master, element );
    AddAfter_Helper( link );
    return link;
}

template<typename TYPE>
void Link<TYPE>::AddAfter_Helper( Link<TYPE> *link ) {
    ASSERT( master );

    // first and last are the same if the num is 1
    if ( master->Num() == 1 ) {
        link->previous = master->first;
        master->first->next = link;
        master->last = link;
        ++master->num;
        return;
    }

    if ( next ) {
        link->next=next;
        next->previous = link;
    }

    link->previous=this;
    next=link;

    if ( this == master->last )
        master->last = link;

    ++master->num;
}

template<typename TYPE>
Link<TYPE> * Link<TYPE>::AddBefore( const TYPE& element ) {
    ASSERT( master );
    Link<TYPE> *link=new Link<TYPE>( master, element );
    AddBefore_Helper( link );
    return link;
}

template<typename TYPE>
Link<TYPE> * Link<TYPE>::AddBefore( TYPE&& element_rref ) {
    ASSERT( master );
    Link<TYPE> *link=new Link<TYPE>( master, std::move( element_rref ) );
    AddBefore_Helper( link );
    return link;
}

template<typename TYPE>
void Link<TYPE>::AddBefore_Helper( Link<TYPE> *link ) {
    ASSERT( master );

    // master's first and last are the same link if there is only one in the list
    if ( master->Num() == 1 ) {
        link->next = master->last;
        master->last->previous = link;
        master->first = link;
        ++master->num;
        return;
    }

    if ( previous ) {
        link->previous=previous;
        previous->next = link;
    }

    link->next=this;
    previous=link;

    if ( this == master->first )
        master->first = link;

    ++master->num;
}

template<typename TYPE>
inline LinkList<TYPE>* Link<TYPE>::GetMaster( void ) {
    return master;
}

template<typename TYPE>
inline const LinkList<TYPE>* Link<TYPE>::GetMaster( void ) const {
    return master;
}

template<typename TYPE>
void Link<TYPE>::Unlink( void ) {
    if ( next ) {
        next->previous = previous;
    } else {
        if ( master ) {
            master->last = previous;
        }
    }

    if ( previous ) {
        previous->next = next;
    } else {
        if ( master ) {
            master->first = next;
        }
    }

    if ( master ) {
        --master->num;
        if ( master->num == 1 ) {
            ASSERT( master->first == master->last );
            master->last = nullptr;
        }
    }

    next = nullptr;
    previous = nullptr;
    master = nullptr;
}

template<typename TYPE>
void Link<TYPE>::LinkAfter( Link<TYPE>& link ) {
    if ( this == &link )
        return;

    Unlink();

    ASSERT( link.master );
    master = link.master;
    ++master->num;

    previous = &link;
    next = link.next;
    link.next = this;

    if ( next ) {
        next->previous = this;
    } else {
        master->last=this;
    }
}

template<typename TYPE>
void Link<TYPE>::LinkBefore( Link<TYPE>& link ) {
    if ( this == &link )
        return;

    Unlink();

    ASSERT( link.master );
    master = link.master;
    ++master->num;

    next = &link;
    previous = link.previous;
    link.previous = this;

    if ( previous ) {
        previous->next = this;
    } else {
        master->first=this;
    }
}

template<typename TYPE>
Link<TYPE>* Link<TYPE>::Del( void ) {
    Link<TYPE>* x = next;
    delete this;
    return x;
}

template<typename TYPE>
Link<TYPE>* Link<TYPE>::Del_Reverse( void ) {
    Link<TYPE>* x = previous;
    delete this;
    return x;
}

template<typename TYPE>
inline void Link<TYPE>::MoveForward( void ) {
    if ( next )
        LinkAfter( *next );
}

template<typename TYPE>
inline void Link<TYPE>::MoveBackward( void ) {
    if ( previous )
        LinkBefore( *previous );
}

template<typename TYPE>
inline void Link<TYPE>::MoveToFirst( void ) {
    if ( master && master->first )
        LinkBefore(*master->first);
}

template<typename TYPE>
inline void Link<TYPE>::MoveToLast( void ) {
    if ( master && master->last )
        LinkAfter(*master->last);
}

/*!

LinkList \n\n

A linked list. This is very similar to Standard C++'s List class in terms of functionality
except that you have two different options for how to iterate. Normal STL iterators, or
using the Link<TYPE> object pointers directly.

**/

template<typename TYPE>
class LinkList {
    friend class Link<TYPE>; // Links need to be able to fuss with LinkList when they delete themselves

public:
    LinkList( void );
    LinkList( std::initializer_list<TYPE> initList );
    explicit LinkList( const TYPE & firstElement );
    LinkList( const LinkList<TYPE>& other );
    LinkList( LinkList<TYPE>&& other_rref );
    virtual ~LinkList( void );

public:
    LinkList<TYPE>& operator=( const LinkList<TYPE>& other );
    LinkList<TYPE>& operator=( LinkList<TYPE>&& other_rref );
    operator LinkList< const TYPE > ( void ) const; //!< conversion Operator

public:
    Link<TYPE>* Find( const TYPE & element, uint* cnt = nullptr );
    const Link<TYPE>* Find( const TYPE & element, uint* cnt = nullptr ) const;

    bool Contains( const TYPE& element ) const;

    Link<TYPE>* Prepend( TYPE && element_rref );
    Link<TYPE>* Prepend( const TYPE & element );
    Link<TYPE>* Prepend( void );

    Link<TYPE>* Append( TYPE && element_rref );
    Link<TYPE>* Append( const TYPE & element );
    Link<TYPE>* Append( void );

    uint AppendUnique( TYPE && element_rref ); //!< appends if not in list. returns index of that item either way.
    uint AppendUnique( const TYPE& element ); //!< appends if not in list. returns index of that item either way.

    Link<TYPE>* GetLast( void );
    Link<TYPE>* GetFirst( void );

    const Link<TYPE>* GetLast( void ) const;
    const Link<TYPE>* GetFirst( void ) const;

    uint Num( void ) const;
    bool Remove( const TYPE & element ); //!< removes one element if found in list
    void DelAll( void );

    void SortData( std::function<bool(const Link<TYPE>& link, const Link<TYPE>& check)> compare_func = Link_LessThan<TYPE> ); //!< selection-sorts data, keeping links in same order. This changes the values of the links' datas
    void Swap( LinkList<TYPE>& other );
    void GiveLinksTo( LinkList<TYPE>& other );
    
private:
    Link<TYPE> *first;
    Link<TYPE> *last;
    uint num;

public:

// **** for compatibility with STL

    inline std::size_t size( void ) const { return Num(); }

    inline Link<TYPE>* push_front( TYPE && element_uref ) { return Prepend( std::forward(element_uref) ); }
    inline Link<TYPE>* push_front( const TYPE & element ) { return Prepend( element ); }
    inline Link<TYPE>* push_front( void ) { return Prepend(); }

    inline Link<TYPE>* push_back( TYPE && element_uref ) { return Append( std::forward(element_uref) ); }
    inline Link<TYPE>* push_back( const TYPE & element ) { return Append( element ); }
    inline Link<TYPE>* push_back( void ) { return Append(); }
    
    inline void clear( void ) { DelAll(); }
    inline void swap( LinkList<TYPE>& other ) { Swap( other ); }

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Weffc++" // complains about std::*iterator* not having virutal destructor
    template< typename LINKTYPE, typename LINK >
    struct IteratorBase : public std::iterator< std::bidirectional_iterator_tag, LINKTYPE > {
    #pragma GCC diagnostic pop
        IteratorBase( LINK *it ) : cur( it ) { }

        IteratorBase operator++( int ) {
            Link<TYPE> *old = cur;

            if ( cur ) {
                cur = cur->GetNext();
                return old;
            }

            cur = nullptr;
            return old;
        }

        IteratorBase operator--( int ) {
            Link<TYPE> *old = cur;

            if ( cur ) {
                cur = cur->GetPrevious();
                return old;
            }

            cur = nullptr;
            return old;
        }

        IteratorBase& operator++( void ) {
            if ( cur ) {
                cur = cur->GetNext();
                return *this;
            }

            cur = nullptr;
            return *this;
        }

        IteratorBase& operator--( void ) {
            if ( cur ) {
                cur = cur->GetPrevious();
                return *this;
            }

            cur = nullptr;
            return *this;
        }

        bool operator==( const IteratorBase& other ) const { return cur == other.cur; }
        bool operator!=( const IteratorBase& other ) const { return !operator==(other); }

        LINKTYPE& operator*( void ) { return cur->Data(); }
        LINKTYPE* operator->( void ) { return &cur->Data(); }

        private:
            LINK *cur;
    };

    typedef IteratorBase< TYPE, Link<TYPE> > Iterator;
    typedef IteratorBase< const TYPE, const Link<TYPE> > ConstIterator;
    typedef ReverseIteratorBase< IteratorBase<TYPE, Link<TYPE> >, Link<TYPE> > ReverseIterator;
    typedef ReverseIteratorBase< IteratorBase<const TYPE, const Link<TYPE> >, const Link<TYPE> > ConstReverseIterator;

    Iterator begin( void ) { return Iterator( GetFirst() ); }
    Iterator end( void ) { return Iterator( nullptr ); }
    ConstIterator begin( void ) const { return ConstIterator( GetFirst() ); }
    ConstIterator end( void ) const { return ConstIterator( nullptr ); }

    ConstIterator cbegin( void ) const { return ConstIterator( GetFirst() ); }
    ConstIterator cend( void ) const { return ConstIterator( nullptr ); }

    ReverseIterator rbegin( void ) { return ReverseIterator( GetLast() ); }
    ReverseIterator rend( void ) { return ReverseIterator( nullptr ); }
    ConstReverseIterator rbegin( void ) const { return ConstReverseIterator( GetLast() ); }
    ConstReverseIterator rend( void ) const { return ConstReverseIterator( nullptr ); }

    ConstReverseIterator crbegin( void ) const { return ConstReverseIterator( GetLast() ); }
    ConstReverseIterator crend( void ) const { return ConstReverseIterator( nullptr ); }
};

template< typename TYPE >
LinkList<TYPE>::LinkList( std::initializer_list<TYPE> initList )
    : first(nullptr)
    , last(nullptr)
    , num(0)
{
    for ( auto& element : initList )
        Append( element );
}

template< typename TYPE >
inline LinkList<TYPE>::operator LinkList< const TYPE > ( void ) const {
    return LinkList< const TYPE >( *this ); // conversion operator
}

template<typename TYPE>
LinkList<TYPE>::LinkList( void )
    : first(nullptr)
    , last(nullptr)
    , num(0) {
}

template<typename TYPE>
LinkList<TYPE>::LinkList( const LinkList<TYPE>& other )
    : first(nullptr)
    , last(nullptr)
    , num(0)
    {
    *this = other;
}

template<typename TYPE>
LinkList<TYPE>::LinkList( LinkList<TYPE>&& other_rref )
    : first(other_rref.first)
    , last(other_rref.last)
    , num(other_rref.num)
{
    other_rref.first = nullptr;
    other_rref.last = nullptr;
    other_rref.num = 0;

    for ( Link<TYPE>* link = first; link != nullptr; link = link->GetNext() )
        link->master = this;
}

template<typename TYPE>
LinkList<TYPE>& LinkList<TYPE>::operator=( LinkList<TYPE>&& other_rref ) {
    if ( this != &other_rref ) {
        DelAll();

        first = other_rref.first;
        last = other_rref.last;
        num = other_rref.num;

        other_rref.first = nullptr;
        other_rref.last = nullptr;
        other_rref.num = 0;

        for ( Link<TYPE>* link = first; link != nullptr; link = link->GetNext() )
            link->master = this;
    }

    return *this;
}

template<typename TYPE>
LinkList<TYPE>& LinkList<TYPE>::operator=( const LinkList<TYPE>& other ) {
    if ( this == &other )
        return *this;

    DelAll();

    for ( auto const& element : other )
        Append( element );

    return *this;
}

template<typename TYPE>
LinkList<TYPE>::~LinkList( void ) {
    DelAll();
}

template<typename TYPE>
inline uint LinkList<TYPE>::Num( void ) const {
    return num;
}

template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Prepend( void ){
    if ( first )
        return first->AddBefore();

    return Append();
}

template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Prepend( const TYPE & element ) {
    if ( first )
        return first->AddBefore( element );

    return Append( element );
}

template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Prepend( TYPE && element_rref ) {
    if ( first )
        return first->AddBefore( std::move( element_rref ) );

    return Append( std::move( element_rref ) );
}

template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Append( void ) {
    if ( num > 0 )
        return GetLast()->AddAfter( TYPE() );

    first = new Link<TYPE>( this, TYPE() );
    ++num;
    return first;
}

template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Append( const TYPE & element ) {
    if ( num > 0 )
        return GetLast()->AddAfter( element );

    first = new Link<TYPE>( this, element );
    ++num;
    return first;
}

template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Append( TYPE && element_rref ) {
    if ( num > 0 )
        return GetLast()->AddAfter( std::move( element_rref ) );

    first = new Link<TYPE>( this, std::move( element_rref ) );
    ++num;
    return first;
}

template<typename TYPE>
bool LinkList<TYPE>::Remove( const TYPE & element ) {
    Link< TYPE > *lnk=Find( element );

    if ( lnk ) {
        lnk->Del();
        return true;
    }

    return false;
}

// cnt is assigned the index relative to the head of the list
template<typename TYPE>
const Link<TYPE>* LinkList<TYPE>::Find( const TYPE & element, uint *cnt ) const {
    if ( cnt )
        *cnt = 0;

    for ( const Link<TYPE>* lnk=first; lnk != nullptr; lnk = lnk->GetNext() ) {
        if ( lnk->Data() == element )
            return lnk;

        if ( cnt )
            ++*cnt;
    }


    return nullptr;
}

// cnt is assigned the index relative to the head of the list
template<typename TYPE>
inline bool LinkList<TYPE>::Contains( const TYPE & element ) const {
    return ::Contains( *this, element );
}

// cnt is assigned the index relative to the head of the list
template<typename TYPE>
Link<TYPE>* LinkList<TYPE>::Find( const TYPE & element, uint *cnt ) {
    return const_cast< Link<TYPE>* >( const_cast< const LinkList<TYPE>* >( this )->Find( element, cnt ) );
}

// returns relative index of element
template<typename TYPE>
uint LinkList<TYPE>::AppendUnique( const TYPE & element ) {
    uint cnt;

    if ( Find(element, &cnt) == nullptr )
        Append(element);

    return cnt;
}

// returns relative index of element
template<typename TYPE>
uint LinkList<TYPE>::AppendUnique( TYPE && element_rref ) {
    uint cnt;

    if ( Find(element_rref, &cnt) == nullptr )
        Append( std::move( element_rref ) );

    return cnt;
}

template<typename TYPE>
inline Link<TYPE>* LinkList<TYPE>::GetLast( void ) {
    return last != nullptr ? last : first;
}

template<typename TYPE>
inline Link<TYPE>* LinkList<TYPE>::GetFirst( void ) {
    return first;
}

template<typename TYPE>
inline const Link<TYPE>* LinkList<TYPE>::GetLast( void ) const {
    return last != nullptr ? last : first;
}

template<typename TYPE>
inline const Link<TYPE>* LinkList<TYPE>::GetFirst( void ) const {
    return first;
}

template<typename TYPE>
void LinkList<TYPE>::DelAll( void ) {
    while ( first )
        first->Del();
    num = 0;
}

template<typename TYPE>
void LinkList<TYPE>::GiveLinksTo( LinkList<TYPE>& other ) {
    if ( this == &other )
        return;

    if ( num == 0 ) {
        return;
    }

    if ( other.num == 0 ) {
        Swap( other );
        return;
    }

    Link<TYPE> *tmp = first;
    while ( tmp != nullptr ) {
        tmp->master = &other;
        tmp = tmp->GetNext();
    }

    tmp = other.GetLast();

    tmp->next = first;
    first->previous = tmp;
    other.last = last;

    first = nullptr;
    last = nullptr;
    num = 0;
}

template<typename TYPE>
void LinkList<TYPE>::Swap( LinkList<TYPE>& other ) {
    if ( this != &other ) {
        std::swap( first, other.first );
        std::swap( last, other.last );
        std::swap( num, other.num );

        Link<TYPE>* link;

        for ( link = first; link != nullptr; link = link->GetNext() )
            link->master = this;

        for ( link = other.first; link != nullptr; link = link->GetNext() )
            link->master = &other;
    }
}

/**

SwapData \n\n

this is implemented as a selection-sort

**/

template<typename TYPE>
void LinkList<TYPE>::SortData( std::function<bool(const Link<TYPE>& link, const Link<TYPE>& check)> compare_func ) {
    Link<TYPE>* selected;
    Link<TYPE>* smaller;
    Link<TYPE>* itr;
    Link<TYPE>* link;

    for ( itr=first; itr != nullptr; itr = itr->GetNext() ) {
        selected = smaller = itr;

        for ( link=itr->GetNext(); link != nullptr; link=link->GetNext() )
            if ( compare_func( *link, *smaller ) )
                smaller = link;

        if (smaller != selected) {
            // we must swap data, not links (if we change the position of theselinks in the list, this algorithm will not work because it will change our position in the iteration )
            selected->SwapData( *smaller );
        }
    }
}

template< typename TYPE >
void ClearPointerLinkList( LinkList< TYPE* >& list );

template< typename TYPE >
void ClearPointerLinkList( LinkList< TYPE* >& list ) {
    Link< TYPE* > *lnk=list.GetFirst();

    while ( lnk != nullptr ) {
        delete lnk->Data();
        lnk = lnk->Del();
    }
}

template< typename TYPE >
void ClearPointerVector( std::vector< TYPE* >& list );

template< typename TYPE >
void ClearPointerVector( std::vector< TYPE* >& list ) {
    for ( auto & ptr : list ) {
        delete ptr;
        ptr = nullptr;
    }

    list.clear();
}

#endif  //SRC_CLIB_LINKLIST
