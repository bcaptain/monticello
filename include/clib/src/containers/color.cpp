// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./color.h"

Color4f::Color4f( void )
    : r(0.0f)
    , g(0.0f)
    , b(0.0f)
    , a(1.0f)
{ }

Color4f::Color4f( const float red, const float green, const float blue, const float alpha )
    : r(red)
    , g(green)
    , b(blue)
    , a(alpha)
{ }

Color4f::Color4f( const Color4f& other )
    : r(other.r)
    , g(other.g)
    , b(other.b)
    , a(other.a)
{ }

Color4f& Color4f::operator=( const Color4f& other ) {
    r = other.r;
    g = other.g;
    b = other.b;
    a = other.a;
    return *this;
}

void Color4f::Set( const float red, const float green, const float blue, const float alpha ) {
    r = red;
    g = green;
    b = blue;
    a = alpha;
}
