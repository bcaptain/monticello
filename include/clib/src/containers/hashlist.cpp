// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "./hashlist.h"

#ifdef CLIB_UNIT_TEST
void UnitTest_HashList( void ) {
    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 0;
        std::size_t index;
        Link< std::pair<std::string, int> > *link = hashlist.GetFirst( index );
        while ( link ) {
            std::pair<std::string, int>& pair = link->Data();

            ASSERT( pair.second == ++i );

            link = hashlist.GetNext( link, index );
        }

        Printf("\n\n");
    }

    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 15;
        std::size_t index;
        Link< std::pair<std::string, int> > *link = hashlist.GetLast( index );
        while ( link ) {
            std::pair<std::string, int>& pair = link->Data();

            ASSERT( pair.second == --i );

            link = hashlist.GetPrevious( link, index );
        }
    }

    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 0;
        std::size_t index;
        Link< std::pair<std::string, int> > *link = hashlist.GetFirst( index );
        while ( link ) {
            std::pair<std::string, int>& pair = link->Data();

            ASSERT( pair.second == ++i );

            link = hashlist.Del( link, index );
        }

        Printf("\n\n");
    }

    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 15;
        std::size_t index;
        Link< std::pair<std::string, int> > *link = hashlist.GetLast( index );
        while ( link ) {
            std::pair<std::string, int>& pair = link->Data();

            ASSERT( pair.second == --i );

            link = hashlist.Del_Reverse( link, index );
        }
    }






    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 0;
        for (  auto itr = std::cbegin(hashlist); itr != std::cend(hashlist); ++itr )
            ASSERT( itr->second == ++i );
    }

    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 15;
        for ( auto itr = std::rbegin(hashlist); itr != std::rend(hashlist); ++itr )
            ASSERT( itr->second == --i );
    }

    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 0;
        for ( auto itr = std::cbegin(hashlist); itr != std::cend(hashlist); ++itr )
            ASSERT( itr->second == ++i );
    }

    {
        HashList< int > hashlist(1009);

        hashlist.Set("one", 13);
        hashlist.Set("thing", 4);
        hashlist.Set("that", 14);
        hashlist.Set("is", 3);
        hashlist.Set("pretty", 2);
        hashlist.Set("awesome", 9);
        hashlist.Set("about", 12);
        hashlist.Set("my", 8);
        hashlist.Set("hashlist", 11);
        hashlist.Set("is", 6);
        hashlist.Set("you", 3);
        hashlist.Set("can", 10);
        hashlist.Set("iterate", 5);
        hashlist.Set("through", 7);
        hashlist.Set("it", 14);
        hashlist.Set("it", 1);

        int i = 15;
        for ( auto itr = std::crbegin(hashlist); itr != std::crend(hashlist); ++itr )
            ASSERT( itr->second == --i );
    }

}
#endif // CLIB_UNIT_TEST
