// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CONTAINERS_DICT_H
#define SRC_CONTAINERS_DICT_H

#include <cstdlib>
#include <cassert>
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "../strings.h"
#include "../hash.h"
#include "./hashlist.h"
#include "../math/vec3t-dec.h"

/*!

DictBase \n\n

a HashList which will translate elements to arbitrary TYPE using std::string as a base

**/

template< typename TYPE >
class DictBase : public HashList< TYPE > {
public:
    DictBase( void ) = default;
    DictBase( const DictBase& other ) = default;
    explicit DictBase( const std::size_t sz );
    ~DictBase( void ) override;

public:
    DictBase& operator=( const DictBase& other ) = default;
    DictBase& operator=( const std::vector< std::pair<TYPE,TYPE> >& vec );

public:
    int GetInt( const char* key, const int default_val=0 ) const;
    uint GetUInt( const char* key, const uint default_val=0 ) const;
    float GetFloat( const char* key, const float default_val=0 ) const; //Todo Brandon From Jesse::Need to add epsilon check for key == 0.0f in order to return default value when intended.
    bool GetBool( const char* key, const bool default_val=false ) const;
    Vec3f GetVec3f( const char* key, const Vec3f& default_val=Vec3f(0,0,0) ) const;
    std::string GetString( const char* key, const std::string& default_val=EMPTY_STRING ) const;

    bool SetInt( const char* key, const int val ); //!< returns true if any value existed before the set
    bool SetUInt( const char* key, const uint uval ); //!< returns true if any value existed before the set
    bool SetFloat( const char* key, const float val ); //!< returns true if any value existed before the set
    bool SetBool( const char* key, const bool val ); //!< returns true if any value existed before the set
    bool SetVec3f( const char* key, const Vec3f& val ); //!< returns true if any value existed before the set

    bool UpdateInt( const char* key, const int val ); //!< returns true if the previous value was different than the provided value
    bool UpdateUInt( const char* key, const uint uval ); //!< returns true if the previous value was different than the provided value
    bool UpdateFloat( const char* key, const float val ); //!< returns true if the previous value was different than the provided value
    bool UpdateBool( const char* key, const bool val ); //!< returns true if the previous value was different than the provided value
    bool UpdateVec3f( const char* key, const Vec3f& val ); //!< returns true if the previous value was different than the provided value

    int GetInt( const Hash& hashkey, const int default_val=0 ) const;
    uint GetUInt( const Hash& hashkey, const uint default_val=0 ) const;
    float GetFloat( const Hash& hashkey, const float default_val=0 ) const;
    bool GetBool( const Hash& hashkey, const bool default_val=false ) const;
    Vec3f GetVec3f( const Hash& hashkey, const Vec3f& default_val=Vec3f(0,0,0) ) const;
    std::string GetString( const Hash& hashkey, const std::string& default_val=EMPTY_STRING ) const;

    //!< Note: if key_hash isn't key's proper hash for this particular dict object it'll be inserted at the wrong place
    bool SetInt( const Hash& hashkey, const int val ); //!< returns true if any value existed before the set
    bool SetUInt( const Hash& hashkey, const uint val ); //!< returns true if any value existed before the set
    bool SetFloat( const Hash& hashkey, const float val ); //!< returns true if any value existed before the set
    bool SetBool( const Hash& hashkey, const bool val ); //!< returns true if any value existed before the set
    bool SetVec3f( const Hash& hashkey, const Vec3f& val ); //!< returns true if any value existed before the set
    
    bool UpdateInt( const Hash& hashkey, const int val ); //!< returns true if the previous value was different than the provided value
    bool UpdateUInt( const Hash& hashkey, const uint val ); //!< returns true if the previous value was different than the provided value
    bool UpdateFloat( const Hash& hashkey, const float val ); //!< returns true if the previous value was different than the provided value
    bool UpdateBool( const Hash& hashkey, const bool val ); //!< returns true if the previous value was different than the provided value
    bool UpdateVec3f( const Hash& hashkey, const Vec3f& val ); //!< returns true if the previous value was different than the provided value

private:
    static int ReturnInt( const std::string* str, const int default_val );
    static uint ReturnUInt( const std::string* str, const uint default_val );
    static bool ReturnBool( const std::string* str, const int default_val );
    static float ReturnFloat( const std::string* str, const float default_val );
    static Vec3f ReturnVec3f( const std::string* str, const Vec3f& default_val );
    static std::string ReturnString( const std::string* str, const std::string& default_val );
};

template< typename TYPE >
inline std::string DictBase<TYPE>::GetString( const char* key, const std::string& default_val ) const {
    return ReturnString( HashList<TYPE>::Get( key ), default_val );
}

template< typename TYPE >
DictBase<TYPE>& DictBase<TYPE>::operator=( const std::vector< std::pair<TYPE,TYPE> >& vec ) {
    HashList<TYPE>::operator=( vec );
    return *this; 
}

template< typename TYPE >
inline bool DictBase<TYPE>::GetBool( const char* key, const bool default_val ) const {
    return ReturnBool( HashList<TYPE>::Get( key ), default_val );
}

template< typename TYPE >
inline int DictBase<TYPE>::GetInt( const char* key, const int default_val ) const {
    return ReturnInt( HashList<TYPE>::Get( key ), default_val );
}

template< typename TYPE >
inline uint DictBase<TYPE>::GetUInt( const char* key, const uint default_val ) const {
    return ReturnUInt( HashList<TYPE>::Get( key ), default_val );
}

template< typename TYPE >
inline float DictBase<TYPE>::GetFloat( const char* key, const float default_val ) const {
    return ReturnFloat( HashList<TYPE>::Get( key ), default_val );
}

template< typename TYPE >
inline Vec3f DictBase<TYPE>::GetVec3f( const char* key, const Vec3f& default_val ) const {
    return ReturnVec3f( HashList<TYPE>::Get( key ), default_val );
}

template< typename TYPE >
inline bool DictBase<TYPE>::GetBool( const Hash& hashkey, const bool default_val ) const {
    return ReturnBool( HashList<TYPE>::Get( hashkey ), default_val );
}

template< typename TYPE >
inline std::string DictBase<TYPE>::GetString( const Hash& hashkey, const std::string& default_val ) const {
    return ReturnString( HashList<TYPE>::Get( hashkey ), default_val );
}

template< typename TYPE >
inline int DictBase<TYPE>::GetInt( const Hash& hashkey, const int default_val ) const {
    return ReturnInt( HashList<TYPE>::Get( hashkey ), default_val );
}

template< typename TYPE >
inline uint DictBase<TYPE>::GetUInt( const Hash& hashkey, const uint default_val ) const {
    return ReturnUInt( HashList<TYPE>::Get( hashkey ), default_val );
}

template< typename TYPE >
inline float DictBase<TYPE>::GetFloat( const Hash& hashkey, const float default_val ) const {
    return ReturnFloat( HashList<TYPE>::Get( hashkey ), default_val );
}

template< typename TYPE >
inline Vec3f DictBase<TYPE>::GetVec3f( const Hash& hashkey, const Vec3f& default_val ) const {
    return ReturnVec3f( HashList<TYPE>::Get( hashkey ), default_val );
}

template< typename TYPE >
DictBase<TYPE>::DictBase( const std::size_t sz )
    : HashList< TYPE >( sz )
{ }

template< typename TYPE >
DictBase<TYPE>::~DictBase( void ) {
}

template< typename TYPE >
std::string DictBase<TYPE>::ReturnString( const std::string* str, const std::string& default_val ) {
    return (!str) ? default_val : std::string(*str);
}

template< typename TYPE >
int DictBase<TYPE>::ReturnInt( const std::string* str, const int default_val ) {
    return (!str) ? default_val : String::ToInt( *str );
}

template< typename TYPE >
uint DictBase<TYPE>::ReturnUInt( const std::string* str, const uint default_val ) {
    return (!str) ? default_val : String::ToUInt( *str );
}

template< typename TYPE >
bool DictBase<TYPE>::ReturnBool( const std::string* str, const int default_val ) {
    return (!str) ? default_val : String::ToBool( *str );
}

template< typename TYPE >
float DictBase<TYPE>::ReturnFloat( const std::string* str, const float default_val ) {
    return (!str) ? default_val : String::ToFloat( *str );
}

template< typename TYPE >
Vec3f DictBase<TYPE>::ReturnVec3f( const std::string* str, const Vec3f& default_val ) {
    if ( !str )
        return default_val;
    Vec3f vec;
    String::ToVec3f( *str, vec );
    return vec;
}

template< typename TYPE >
bool DictBase<TYPE>::SetBool( const char* key, const bool val ) {
    return HashList<TYPE>::Set( key, val ? "1" : "0" );
}

template< typename TYPE >
bool DictBase<TYPE>::SetInt( const char* key, const int val ) {
    return HashList<TYPE>::Set( key, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetUInt( const char* key, const uint val ) {
    return HashList<TYPE>::Set( key, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetFloat( const char* key, const float val ) {
    return HashList<TYPE>::Set( key, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetVec3f( const char* key, const Vec3f& val ) {
    return HashList<TYPE>::Set( key, String::VecToString(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateBool( const char* key, const bool val ) {
    return HashList<TYPE>::Update( key, val ? "1" : "0" );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateInt( const char* key, const int val ) {
    return HashList<TYPE>::Update( key, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateUInt( const char* key, const uint val ) {
    return HashList<TYPE>::Update( key, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateFloat( const char* key, const float val ) {
    return HashList<TYPE>::Update( key, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateVec3f( const char* key, const Vec3f& val ) {
    return HashList<TYPE>::Update( key, String::VecToString(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetBool( const Hash& hashkey, const bool val ) {
    return HashList<TYPE>::Set( hashkey, val ? "1" : "0" );
}

template< typename TYPE >
bool DictBase<TYPE>::SetInt( const Hash& hashkey, const int val ) {
    return HashList<TYPE>::Set( hashkey, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetUInt( const Hash& hashkey, const uint val ) {
    return HashList<TYPE>::Set( hashkey, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetFloat( const Hash& hashkey, const float val ) {
    return HashList<TYPE>::Set( hashkey, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::SetVec3f( const Hash& hashkey, const Vec3f& val ) {
    return HashList<TYPE>::Set( hashkey, String::VecToString(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateBool( const Hash& hashkey, const bool val ) {
    return HashList<TYPE>::Update( hashkey, val ? "1" : "0" );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateInt( const Hash& hashkey, const int val ) {
    return HashList<TYPE>::Update( hashkey, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateUInt( const Hash& hashkey, const uint val ) {
    return HashList<TYPE>::Update( hashkey, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateFloat( const Hash& hashkey, const float val ) {
    return HashList<TYPE>::Update( hashkey, std::to_string(val) );
}

template< typename TYPE >
bool DictBase<TYPE>::UpdateVec3f( const Hash& hashkey, const Vec3f& val ) {
    return HashList<TYPE>::Update( hashkey, String::VecToString(val) );
}

typedef DictBase< std::string > Dict;
typedef std::pair< std::string, std::string > DictEntry;

#endif  //SRC_CONTAINERS_DICT_H
