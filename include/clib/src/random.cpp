// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <random>
#include "./warnings.h"
#include "./types.h"
#include "./random.h"
#include "./math/values.h"

std::random_device Random::rd;
std::default_random_engine Random::engine( rd() );

char Random::FromASCII58To65( const char ch ) {
    const int ASCII_Z = 58;
    if ( ch > ASCII_Z )
        return static_cast<char>( static_cast<int>(ch) + 7 );
    return ch;
}

char Random::FromASCII91To97( const char ch ) {
    const int ASCII_Z = 90;
    if ( ch > ASCII_Z )
        return static_cast<char>( static_cast<int>(ch) + 6 );
    return ch;
}

char Random::Char( void ) {
    return static_cast< char >( Int(-127, 127) );
}

uchar Random::UChar( void ) {
    return static_cast< uchar >( Int(0, 255) );
}

char Random::Char( const int min, const int max ) {
    return static_cast< char >( Int(min,max) );
}

uchar Random::UChar( const uint min, const uint max ) {
    return static_cast< uchar >( UInt(min,max) );
}

char Random::Alpha( void ) {
    return FromASCII91To97( Char( 65,90+26 ) );
}

char Random::AlphaNumeric( void ) {
    char c = Char( 48,57+26+26 );
    c = FromASCII58To65( c );
    c = FromASCII91To97( c );
    return c;
}

char Random::Numeric( void ) {
    return static_cast< char >( Char( 48,57 ) );
}

int Random::Int( const int min, const int max ) {
    std::uniform_int_distribution<int> uniform_dist(min, max);
    return uniform_dist(engine);
}

int Random::Bool( void ) {
    return Random::Int(0, 1) == 1;
}

uint Random::UInt( const uint min, const uint max ) {
    std::uniform_int_distribution<uint> uniform_dist(min, max);
    return uniform_dist(engine);
}

Uint16 Random::UInt16( const Uint16 min, const Uint16 max ) {
    std::uniform_int_distribution<Uint16> uniform_dist(min, max);
    return uniform_dist(engine);
}

float Random::Float( const float min, const float max, const uint digits ) {
    const float d = powf( 10, static_cast<float>(digits) );
    return ( floorf(Float(min,max)*d) ) / d;
}

float Random::Float( const float min, const float max ) {
    std::uniform_real_distribution<float> uniform_dist(min, max);
    return uniform_dist(engine);
}

Vec3f Random::Direction( void ) {
    return Vec3f( Random::Float( -1, 1 ), Random::Float( -1, 1 ), Random::Float( -1, 1 ) ).GetNormalized();
}

Vec3f Random::DirectionXY( void ) {
    return Vec3f( Random::Float( -1, 1 ), Random::Float( -1, 1 ), 0 ).GetNormalized();
}
