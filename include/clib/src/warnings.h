// Copyright 2010-2019 Brandon Captain. You may not copy this work.
#ifndef SRC_CLIB_WARNINGS_H_
#define SRC_CLIB_WARNINGS_H_
    // ** turn these back on when needed
    #pragma GCC diagnostic ignored "-Wconversion" // dilutes code for nearly useless reasons. uncomment this one as needed
    #pragma GCC diagnostic ignored "-Wcomment" // useless and annoying
    #pragma GCC diagnostic ignored "-Wpadded" // we will address these prior to release
    #pragma GCC diagnostic ignored "-Waggregate-return" // useless: "In the GNU apps I'm familiar with (Emacs, coreutils, ...) we simply disable -Waggregate-return. It a completely anachronistic warning, since its motivation was to support backwards compatibility with C compilers that did not allow returning structures. Those compilers are long dead and are no longer of practical concern."
    #pragma GCC diagnostic ignored "-Wsign-conversion" // normally this is fine, but the warnings flood
    #pragma GCC diagnostic ignored "-Winline" // we can't do much about this
    
    #ifndef __clang__
        #pragma GCC diagnostic ignored "-Wsuggest-attribute=pure" // too many warnings, not interested.
        #pragma GCC diagnostic ignored "-Wsuggest-attribute=const" // this thing keeps warning about static methods!
        #pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn" // kind of useless
        #pragma GCC diagnostic ignored "-Wunsafe-loop-optimizations" // not much we can do about this
    #endif // __clang__
    
    #ifndef NO_WARNINGS
        #pragma GCC diagnostic warning "-Wmain"
        #pragma GCC diagnostic warning "-Wall"
        #pragma GCC diagnostic warning "-Wextra"
        #pragma GCC diagnostic warning "-Weffc++"
        #pragma GCC diagnostic warning "-Wswitch"
        #pragma GCC diagnostic warning "-Wswitch-default"
        #pragma GCC diagnostic warning "-Wmissing-include-dirs"
        #pragma GCC diagnostic warning "-Wmissing-declarations"
        #pragma GCC diagnostic warning "-Wunreachable-code"
        #pragma GCC diagnostic warning "-Wundef"
        #pragma GCC diagnostic warning "-Wcast-align"
        #pragma GCC diagnostic warning "-Wredundant-decls"
        #pragma GCC diagnostic warning "-Winit-self"
        #pragma GCC diagnostic warning "-Wnon-virtual-dtor"
        #pragma GCC diagnostic warning "-Wvla"
        #pragma GCC diagnostic warning "-Wtype-limits"
        #pragma GCC diagnostic warning "-Wstrict-overflow=5"
        #pragma GCC diagnostic warning "-Wstrict-overflow"
        #pragma GCC diagnostic warning "-Wpointer-arith"
        #pragma GCC diagnostic warning "-Wpacked"
        #pragma GCC diagnostic warning "-Wold-style-cast"
        #pragma GCC diagnostic warning "-Wmissing-format-attribute"
        #pragma GCC diagnostic warning "-Wmissing-field-initializers"
        #pragma GCC diagnostic warning "-Winvalid-pch"
        #pragma GCC diagnostic warning "-Wignored-qualifiers"
        #pragma GCC diagnostic warning "-Wformat-nonliteral"
        #pragma GCC diagnostic warning "-Wformat=2"
        #pragma GCC diagnostic warning "-Wempty-body"
        #pragma GCC diagnostic warning "-Wdouble-promotion"
        #pragma GCC diagnostic warning "-Wdisabled-optimization"
        #pragma GCC diagnostic warning "-Wctor-dtor-privacy"
        #pragma GCC diagnostic warning "-Wconversion-null"
        #pragma GCC diagnostic warning "-Wcast-qual"
        #pragma GCC diagnostic warning "-Wabi"
        #pragma GCC diagnostic warning "-Waddress"
        #pragma GCC diagnostic warning "-Warray-bounds"
        #pragma GCC diagnostic warning "-Wc++0x-compat"
        #pragma GCC diagnostic warning "-Wchar-subscripts"
        #pragma GCC diagnostic warning "-Wenum-compare"
        #pragma GCC diagnostic warning "-Wformat"
        #pragma GCC diagnostic warning "-Wformat-security"
        #pragma GCC diagnostic warning "-Wmissing-braces"
        #pragma GCC diagnostic warning "-Woverlength-strings"
        #pragma GCC diagnostic warning "-Wparentheses"
        #pragma GCC diagnostic warning "-Wreorder"
        #pragma GCC diagnostic warning "-Wsequence-point"
        #pragma GCC diagnostic warning "-Wsign-compare"
        #pragma GCC diagnostic warning "-Wsign-promo"
        #pragma GCC diagnostic warning "-Wstack-protector"
        #pragma GCC diagnostic warning "-Wstrict-aliasing"
        #pragma GCC diagnostic warning "-Wtrigraphs"
        #pragma GCC diagnostic warning "-Wuninitialized"
        #pragma GCC diagnostic warning "-Wunknown-pragmas"
        #pragma GCC diagnostic warning "-Wunused"
        #pragma GCC diagnostic warning "-Wvariadic-macros"
        #pragma GCC diagnostic warning "-Wvolatile-register-var"
        #pragma GCC diagnostic warning "-Wwrite-strings"
        
        #ifdef MONTICELLO_COMPILER_GNU
            #ifdef _WIN32
                #pragma GCC diagnostic warning "-Wcast-function-type"
            #endif
        #endif
        
        #ifndef __clang__
            #pragma GCC diagnostic warning "-W"
            #pragma GCC diagnostic warning "-Wtrampolines"
            #pragma GCC diagnostic warning "-Wlogical-op"
            #pragma GCC diagnostic warning "-Wclobbered"
            #pragma GCC diagnostic warning "-Wcoverage-mismatch"
            #pragma GCC diagnostic warning "-Wnoexcept"
            #pragma GCC diagnostic warning "-Wpacked-bitfield-compat"
            #pragma GCC diagnostic warning "-Wstrict-null-sentinel"
            #pragma GCC diagnostic warning "-Wsync-nand"
        #endif // __clang__
    #endif // NO_WARNINGS

    #ifdef __clang__
        #pragma GCC diagnostic ignored "-Wdocumentation-unknown-command"
        #pragma GCC diagnostic ignored "-Wunknown-warning-option"
        #pragma GCC diagnostic ignored "-Wc++98-compat"
        #pragma GCC diagnostic ignored "-Wc++98-compat-pedantic"
        #pragma GCC diagnostic ignored "-Wunknown-pragmas"
        #pragma GCC diagnostic ignored "-Wcovered-switch-default"
        #pragma GCC diagnostic ignored "-Wexit-time-destructors"
    #endif // __clang__

    // ** these should always be considered errors
    #pragma GCC diagnostic error "-Wreturn-type"
    #pragma GCC diagnostic error "-Woverloaded-virtual"
    #pragma GCC diagnostic error "-Woverflow"
    #pragma GCC diagnostic error "-Wshadow"
    #pragma GCC diagnostic error "-Wswitch-enum"
    #pragma GCC diagnostic error "-Wfloat-equal"
    #pragma GCC diagnostic error "-Wunused-value"

#endif // SRC_CLIB_WARNINGS_H_
