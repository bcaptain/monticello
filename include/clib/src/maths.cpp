// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <cmath>
#include "./warnings.h"
#include "./maths.h"

const float EP = 0.000797f;
const float EP_TEN_THOUSANDTH = 0.0001f;
const float EP_THOUSANDTH = 0.001f;
const float EP_HUNDREDTH = 0.01f;
const float EP_HUNDREDTH_CENTIMETER = 0.0001f;
const float EP_TENTH_CENTIMETER = 0.001f;
const float EP_HALF_CENTIMETER = 0.005f;
const float EP_ONE_CENTIMETER = 0.01f;
const float EP_THREE_CENTIMETERS = 0.03f;
const float EP_MIL = 0.000001f;
const double PI_D = 3.1415926535897931;
const float PI_F = 3.141593f;
const double PI_OVER_180_D = PI_D / 180.0;
const float PI_OVER_180_F = PI_F / 180.0f;
const float PI_OVER_360_F = PI_F / 360.0f;
const float HALF_PI_OVER_180_F = ( PI_F / 180.0f ) / 2.0f;
const double ONE_EIGHTY_OVER_PI_D = 180.0 / PI_D;
const float ONE_EIGHTY_OVER_PI_F = 180.0f / PI_F;
const float RAD_TO_DEG_F = ONE_EIGHTY_OVER_PI_F;
const float DEG_TO_RAD_F = PI_OVER_180_F;

Vec3f Maths::Fabsf( const Vec3f& vec ) {
    return {
        fabsf( vec.x )
        , fabsf( vec.y )
        , fabsf( vec.z )
    };
}

Maths::QuadraticReturnT Maths::SolveQuadratic( const float a, const float b, const float c, float& solution_one, float* solution_two, const bool compute_complex ) {
    // Quadratic Formula is x = ( -b ± √(b2-4ac) ) / 2a

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal"
    if ( a == 0.0f )
        return Maths::QuadraticReturnT::INVALID;
    #pragma GCC diagnostic pop

    const float discriminant = (b*b) - (a*c*4);

    if ( discriminant > 0.0f ) {
        const float addend = sqrtf( discriminant );
        solution_one = ( -b + addend ) / ( a*2 );
        if ( solution_two ) {
            *solution_two = ( -b - addend ) / ( a*2 );
        }
        return QuadraticReturnT::TWO_SIMPLE;
    }

    if ( discriminant < 0.0f ) {
        if ( compute_complex ) {
            const float addend = sqrtf( -discriminant ); // negate to avoid NaN
            solution_one =  (-b) / (a*2);
            if ( solution_two ) {
                *solution_two = addend / (a*2); // imaginary
            }
        }
        return QuadraticReturnT::TWO_COMPLEX;
    }

    // ** discriminant is zero
    solution_one = (-b) / ( a*2 );
    return QuadraticReturnT::ONE_SIMPLE;
}

bool Maths::GetLowestRoot( const float a, const float b, const float c, float& lowestRoot ) {
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wfloat-equal"
    if ( a == 0.0f )
        return false; // this would not be a quadratic formula
    #pragma GCC diagnostic pop

    float solution_two;
    switch ( SolveQuadratic( a, b, c, lowestRoot, &solution_two ) ) {
        case QuadraticReturnT::TWO_SIMPLE:
            if ( solution_two < lowestRoot )
                lowestRoot = solution_two;
            return true;
        case QuadraticReturnT::ONE_SIMPLE:
            return true;
        case QuadraticReturnT::TWO_COMPLEX: FALLTHROUGH;
        case QuadraticReturnT::INVALID: FALLTHROUGH;
        default:
            return false;
    }
}

float Maths::NormalizeAngle( const float angle, const float minmax ) {  //minmax = 360
    float ret = fmodf( angle, minmax );

    if ( ret < 0 )
        ret += minmax;

    return ret;
}

#ifdef CLIB_UNIT_TEST
    void Maths::UnitTest( void ) {
        UnitTest_SolveQuadratic();
    }

    void Maths::UnitTest_SolveQuadratic( void ) {
        float solution_one, solution_two;

        ASSERT( SolveQuadratic(34,3,31,solution_one,&solution_two, true) == QuadraticReturnT::TWO_COMPLEX );
        ASSERT( Maths::Approxf( solution_one, -0.044117f, 0.000001f ) );
        ASSERT( Maths::Approxf( solution_two, 0.9538439f, 0.000001f ) );

        ASSERT( SolveQuadratic(34,343,31,solution_one,&solution_two ) == QuadraticReturnT::TWO_SIMPLE );
        ASSERT( Maths::Approxf( solution_one, -0.091203f, 0.000001f ) );
        ASSERT( Maths::Approxf( solution_two, -9.997031f, 0.000001f ) );

        ASSERT( SolveQuadratic(-34,343,31,solution_one,&solution_two )  == QuadraticReturnT::TWO_SIMPLE );
        ASSERT( Maths::Approxf( solution_one, -0.0895835f, 0.000001f ) );
        ASSERT( Maths::Approxf( solution_two, 10.177818f, 0.000001f ) );
    }

#endif // CLIB_UNIT_TEST
