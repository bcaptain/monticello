// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ASSERT_H
#define SRC_CLIB_ASSERT_H

#include "./warnings.h"
#include "./macros.h"
#include "./strings.h"

#ifdef ASSERT_MSG
    #error "macro ASSERT_MSG was already defined"
#endif //_ASSERT_MSG

#ifndef NDEBUG
    #define ASSERT( expr ) { \
        if ( (expr) ) { \
            void (0); \
        } else { \
            DIE("Assert Failed: %s:%i: %s: %s", __FILE__, __LINE__, __PRETTY_FUNCTION__, #expr ); \
            abort(); \
        } \
    }
    
    #define ASSERT_MSG( expr, msg ) { \
        if ( (expr) ) { \
            void (0); \
        } else { \
            DIE("Assert Failed: %s:%i: %s: %s: %s", __FILE__, __LINE__, __PRETTY_FUNCTION__, #expr, msg ); \
            abort(); \
        } \
    }
#else
    #define ASSERT( ... ) void (0);
    #define ASSERT_MSG( ... ) void (0);
#endif // NDEBUG

#endif // SRC_CLIB_ASSERT_H
