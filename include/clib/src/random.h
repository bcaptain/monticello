// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_RANDOM_H_
#define SRC_CLIB_RANDOM_H_

#include <random>
#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

typedef unsigned int uint;
typedef unsigned char uchar;

class Random {
public:
    static int Int( const int min, const int max );

    static int Bool( void );

    static uint UInt( const uint min, const uint max );
    static Uint16 UInt16( const Uint16 min, const Uint16 max );

    static float Float( const float min, const float max );
    static float Float( const float min, const float max, const uint digits );

    static char Char( void );
    static char Char( const int min, const int max );

    static uchar UChar( void );
    static uchar UChar( const uint min, const uint max );

    static char Alpha( void );
    static char Numeric( void );
    static char AlphaNumeric( void );

    static Vec3f Direction( void );
    static Vec3f DirectionXY( void );

    template< typename TYPE >
    static TYPE Element( const std::vector< TYPE >& list );

private:
    static char FromASCII91To97( const char ch );
    static char FromASCII58To65( const char ch );

private:
    static std::random_device rd;

public:
    static std::default_random_engine engine;
};

template< typename TYPE >
TYPE Random::Element( const std::vector< TYPE >& list ) {
    if ( list.size() < 1 )
        return TYPE{};

    return list[ Random::UInt( 0, list.size()-1 ) ];
}

#endif  // SRC_CLIB_RANDOM

