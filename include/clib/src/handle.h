// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_HANDLE_H_
#define SRC_CLIB_HANDLE_H_

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

/*!

Handle \n\n

Handle must be assigned a resource by-reference when created. It will then allow any copies of itself to access that resurce by pointer via the Get() function. When the original Handle is destroyed (which happens upon destruction of it's handled resource), all copied handles will return null when Get() is called. 

*/

template< typename TYPE >
class Handle {
public:
    Handle( void ) = delete;
    Handle( const Handle& other );
    Handle( TYPE& resource_ptr );
    ~Handle( void );

public:
    Handle<TYPE>& operator=( const Handle<TYPE>& other );
    
public:
    TYPE* Get( void );

private:
    TYPE** resource;
    bool master;
};

template< typename TYPE >
Handle<TYPE>::Handle( const Handle<TYPE>& other )
    : resource( other.resource )
    , master( false )
{ }

template< typename TYPE >
Handle<TYPE>& Handle<TYPE>::operator=( const Handle<TYPE>& other ) {
    resource = other.resource;
    master = false;
    return *this;
}
    
template< typename TYPE >
Handle<TYPE>::Handle( TYPE& resource_ptr )
    : resource( new TYPE*( &resource_ptr ) )
    , master( true )
{ }
    
template< typename TYPE >
Handle<TYPE>::~Handle( void ) {
    if ( master ) {
        *resource = nullptr;
        delete resource;
    }
}

template< typename TYPE >
TYPE* Handle<TYPE>::Get( void ) {
    if ( resource )
        return *resource;
    return nullptr;
}

#endif  // SRC_CLIB_HANDLE_H_
