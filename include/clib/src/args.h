// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ARGS_H_
#define SRC_CLIB_ARGS_H_

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

//! A set of functions used to easily browse the commandline arguments to this program
namespace Arg {
    extern int arg_num; //!< set this and args to main's arg/v variables one time only at begining of main() to use Arg namespace
    extern char **args; //!< set this and arg_num to main's arg/v variables one time only at begining of main() to use Arg namespace

    bool findarg( const char *arg, bool remove=true ); //!< see if argument was passed and maybe remove it
    int getarg_int( const char *arg ); //!< return int value for an argument
    char* getarg_str( const char *arg ); //!< return str value for an argument
}

#endif  // SRC_CLIB_ARGS
