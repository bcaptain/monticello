// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_EVERYTHING
#define SRC_CLIB_EVERYTHING

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"
#include "./args.h"
#include "./cpuid.h"
#include "./maths.h"
#include "./strings.h"
#include "./bitwise.h"
#include "./bitwiseEnumClass.h"
#include "./random.h"
#include "./containers/color.h"
#include "./containers/linklist.h"
#include "./hash.h"
#include "./containers/hashlist.h"
#include "./containers/dict.h"
#include "./files/filesystem.h"
#include "./files/base/file.h"
#include "./files/base/image.h"
#include "./files/parsers/text.h"
#include "./files/parsers/ssv.h"
#include "./files/parsers/binary.h"
#include "./files/archived.h"
#include "./files/tga.h"
#include "./files/zip.h"
#include "./unit_test.h"

#endif  // SRC_CLIB_EVERYTHING
