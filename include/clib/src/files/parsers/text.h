// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_FILE_TEXT_H_
#define SRC_CLIB_FILE_TEXT_H_

#include <cstdlib>
#include "../../warnings.h"
#include "../../macros.h"
#include "../../std.h"
#include "../../strings.h"
#include "../../bitwise.h"
#include "../../types.h"
#include "../../strings.h"
#include "../base/file.h"

/*!

QuoteStatusT \n\n

Used to keep track of whether you're inside a quoted string

**/

typedef Uint8 QuoteStatusT_BaseType;
enum class QuoteStatusT : QuoteStatusT_BaseType {
    NoQuote = 0
    , QuoteStarted = 1
    , QuoteDone = 2
};

/*!

ParserText \n\n

Human-Readable File parser \n

**/

class ParserText {
public:
    ParserText( void ) = delete;
    ParserText( File& file_to_parse ); //!< file_to_parse is not resource managed
    ParserText( ParserText& other ) = delete;
    virtual ~ParserText( void );

public:
    ParserText& operator=( ParserText& other ) = delete;

public:
    bool SkipToNextLine( void );
    bool SkipToNextNonblankLine( void );
    bool SkipWhitespace( const bool multiline = false );

    static bool IsWhiteSpaceOrNewline( const char c );
    static bool IsWhiteSpace( const char c );
    static bool IsNewline( const char c );
    bool AtEndOfLine( void ) const;
    bool AtBeginningOfLine( void ) const;

    void SetCommentIndicator( const char ind );

    bool ReadWord( std::string& wrd, const bool continueToNextLine = true );
    bool ReadWordOnLine( std::string& wrd ) { return ReadWord( wrd, false ); }
    bool ReadUnquotedString( std::string& str ); //!< newline terminated
    
    uint GetLineNumber( void ) const;

    // ** the following are just inlines that call the same functions in File* file

    void ReadChar( char* data_out, const Uint32 len );
    char ReadChar( void );
    void ReadChar( char& data_out );
    void ReadChar( char* data_out );

    void ReadUChar( uchar* data_out, const Uint32 len );
    uchar ReadUChar( void );
    void ReadUChar( uchar& data_out );
    void ReadUChar( uchar* data_out );

    void WriteChar( const char* data_out, const Uint32 len );
    void WriteChar( const char data_out );
    void WriteStr( const std::string& str );

    std::string GetFilePath( void ) const;

protected:
    char* commentIndicator;
    uint line; //!< this may be inaccurate if you use methods from base classes of this class to  move the 'file pointer', read, etc
    File* file; //!< not resource managed
};

#endif  //SRC_CLIB_FILE_TEXT
