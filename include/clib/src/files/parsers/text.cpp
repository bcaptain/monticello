// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../../warnings.h"
#include "../../message.h"
#include "../../types.h"
#include "./text.h"

ParserText::~ParserText( void ) {
    DELNULL( commentIndicator );
}

ParserText::ParserText( File& file_to_parse )
    : commentIndicator(nullptr)
    , line(1)
    , file( &file_to_parse )
{ }

bool ParserText::SkipToNextLine( void ) {
    char read;

    while ( true ) {
        if ( file->EndOfFile() )
            return false;

        read = file->ReadChar();

        // chew through all newline chars
        if ( IsNewline( read ) ) {
            ++line;
            return true;
        }
    }
}

bool ParserText::SkipToNextNonblankLine( void ) {
    bool foundNewLine = false;
    char read;

    while ( true ) {
        if ( file->EndOfFile() )
            return false;

        read = file->ReadChar();

        // chew through all newline chars
        if ( IsNewline( read ) ) {
            ++line;
            foundNewLine = true;
        } else if ( foundNewLine ) {
            break;
        }
    }

    file->SeekCur( -1 );
    return foundNewLine;
}

// returns false if reaches end of file or cannot find the beginning of the next value
bool ParserText::SkipWhitespace( const bool multiLine ) {
    char read;

    while ( true ) {
        if ( file->EndOfFile() )
            return false;

        read = file->ReadChar();

        if ( IsWhiteSpace( read ) ) {
            continue;
        }

        if ( multiLine ) {
            if ( IsNewline( read ) ) {
                ++line;
                continue;
            }
        } else if ( IsNewline( read ) ) {
            file->SeekCur( -1 );
            return false;
        }

        if ( commentIndicator && read == *commentIndicator ) {
            if ( !multiLine )
                return false;

            if ( ! SkipToNextNonblankLine() ) // skip comment
                return false;

            continue;
        }

        break;
    }

    file->SeekCur( -1 );
    return true;
}

void ParserText::SetCommentIndicator( const char ind ) {
    if ( !ind ) {
        if ( commentIndicator )
            DELNULL( commentIndicator );
    } else {
        if ( !commentIndicator )
            commentIndicator=new char(ind);
        else
            commentIndicator[0] = ind;
    }
}

bool ParserText::IsWhiteSpaceOrNewline( const char c ) {
    return IsWhiteSpace( c ) || IsNewline( c );
}

bool ParserText::IsWhiteSpace( const char c ) {
    switch ( c ) {
        case ' ': FALLTHROUGH;
        case '\t':
        case '\r':
            return true;
        default:
            return false;
    }
}

bool ParserText::ReadWord( std::string& wrd, const bool continueToNextLine ) {
    wrd.erase();

    if ( !SkipWhitespace( continueToNextLine ) ) {
        return false;
    }
    
    while ( commentIndicator && !file->EndOfFile() ) {
        if ( file->PeekChar() != *commentIndicator )
            break;

        if ( !continueToNextLine || !SkipToNextNonblankLine() )
            return false;
    }

    // check for beginning of quote
    QuoteStatusT quote_status = QuoteStatusT::NoQuote;
    if ( !file->EndOfFile() && file->PeekChar() == '"' ) {
        file->ReadChar();
        quote_status = QuoteStatusT::QuoteStarted;

        // we need to read the next char before entering the while loop below

        if ( file->EndOfFile() ) {
            DIE("Parse Error: Quote at end of file %s:%u\n", file->GetFilePath().c_str(), line );
            return false;
        }
    }

    // read each char into a string
    while ( !file->EndOfFile() ) {
        const char read = file->ReadChar();
        if ( quote_status != QuoteStatusT::NoQuote ) {
            if ( read == '"' ) {
                quote_status = QuoteStatusT::QuoteDone;
                break;
            }
        } else {
            if ( IsWhiteSpace(read) || IsNewline(read) ) {
                break;
            }
        }
        wrd += read;
    }

    if ( quote_status == QuoteStatusT::QuoteStarted ) {
        DIE("Parse Error: Did not find end quote in file %s:%u\n", file->GetFilePath().c_str(), line );
        return false;
    }

    // if we didn't read a quoted string, we read one char past the end of the contents we were looking for (a quote is part of quoted contents)
    if ( quote_status == QuoteStatusT::NoQuote && !file->EndOfFile() )
        file->SeekCur( -1 );

    return ( wrd.size() > 0 );
}

bool ParserText::ReadUnquotedString( std::string& str ) {
    str.erase();

     if ( !SkipWhitespace( false ) )
         return false;

    char read;

    // read each char into a string
    for ( read = file->ReadChar(); !file->EndOfFile() && !IsNewline(read); read = file->ReadChar() ) {
        if ( commentIndicator && read == *commentIndicator ) {
            SkipToNextNonblankLine();
            break;
        }

        if ( read != '\r' ) // because this is considered junk whitespace
            str += read;
    }

    if ( !file->EndOfFile() )
        file->SeekCur( -1 ); // we don't continue to the next line

    return ( str.size() > 0 );
}

bool ParserText::AtBeginningOfLine( void ) const {
    if ( file->EndOfFile() )
        return false;

    if ( file->FTell() == 0 )
        return true;

    file->SeekCur( -1 );
    return IsNewline( file->ReadChar() );
}

std::string ParserText::GetFilePath( void ) const {
    return ( file ? file->GetFilePath() : "" );
}

bool ParserText::IsNewline( const char c ) {
    return c == '\n';
}

void ParserText::ReadChar( char* data_out, const Uint32 len ) {
    file->ReadChar( data_out, len );
}

char ParserText::ReadChar( void ) {
    return file->ReadChar();
}

void ParserText::ReadChar( char& data_out ) {
    file->ReadChar( data_out );
}

void ParserText::ReadChar( char* data_out ) {
    file->ReadChar( data_out );
}

void ParserText::ReadUChar( uchar* data_out, const Uint32 len ) {
    file->ReadUChar( data_out, len );
}

uchar ParserText::ReadUChar( void ) {
    return file->ReadUChar();
}

void ParserText::ReadUChar( uchar& data_out ) {
    file->ReadUChar( data_out );
}

void ParserText::ReadUChar( uchar* data_out ) {
    file->ReadUChar( data_out );
}

void ParserText::WriteStr( const std::string& str ) {
    WriteChar( str.c_str(), str.size() );
}

void ParserText::WriteChar( const char* data_out, const Uint32 len ) {
    file->WriteChar( data_out, len );
}

void ParserText::WriteChar( const char data_out ) {
    file->WriteChar( data_out );
}

uint ParserText::GetLineNumber( void ) const {
    return line;
}

bool ParserText::AtEndOfLine( void ) const {
    return IsNewline( file->PeekChar() );
}
