// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_SSVFILE_H_
#define SRC_CLIB_SSVFILE_H_

#include "../../warnings.h"
#include "../../macros.h"
#include "../../types.h"
#include "../../std.h"
#include "../../strings.h"
#include "../../strings.h"
#include "./text.h"

/*!

ParserSSV \n\n

Space Separated Values

**/

class ParserSSV : public ParserText {
public:
    ParserSSV( void ) = delete;
    ParserSSV( File& file_to_parse ); //!< file_to_parse is not resource managed
    ParserSSV( ParserSSV& other ) = delete;
    ~ParserSSV( void ) override { }

public:
    ParserSSV& operator=( ParserSSV& other )  = delete;

public:
    void ReadVec3f( Vec3f& val );
    void ReadVec2f( Vec2f& val );
    void ReadString( std::string& out );
    uint ReadUInt( void );
    int ReadInt( void );
    float ReadFloat( void );

    void ReadUInt( uint& val );
    void ReadInt( int& val );
    void ReadFloat( float& val );

private:
    bool BadFile( void ) const;
    bool FileNotIsOpen( void ) const;
};

#endif  // SRC_CLIB_SSVFILE
