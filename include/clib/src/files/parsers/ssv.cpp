// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../../warnings.h"
#include "../../message.h"
#include "./ssv.h"
#include "../../math/./vec2t-dec.h"

ParserSSV::ParserSSV( File& file_to_parse )
    : ParserText( file_to_parse )
{ }

void ParserSSV::ReadVec3f( Vec3f& val ) {
    std::string tmp;
    if ( ! ReadWordOnLine( tmp ) ) {
        ERR("Parse Error: Couldn't read/find Vec3f in script %s:%u\n", file->GetFilePath().c_str(), line );
        return;
    }

    if ( ! String::ToVec3f( tmp, val ) ) {
        ERR("Parse Error: Expected Vec3f, got somthing else in script %s:%u\n", file->GetFilePath().c_str(), line );
        return;
    }
}

bool ParserSSV::BadFile( void ) const {
    ERR("SSV: Malformed file: %s\n", file->GetFilePath().c_str() );
    return false;
}

void ParserSSV::ReadVec2f( Vec2f& val ) {
    std::string tmp;
    if ( ! ReadWordOnLine( tmp ) ) {
        ERR("Parse Error: Couldn't read/find Vec2f in script %s:%u\n", file->GetFilePath().c_str(), line );
        return;
    }

    if ( ! String::ToVec2f( tmp, val ) ) {
        ERR("Parse Error: Expected Vec2f, got somthing else in script %s:%u\n", file->GetFilePath().c_str(), line );
        return;
    }
}

void ParserSSV::ReadString( std::string& out ) {
    if ( ! ReadWordOnLine( out ) ) {
        ERR("Parse Error: Couldn't read/find string in script %s:%u\n", file->GetFilePath().c_str(), line );
        return;
    }
}

int ParserSSV::ReadInt( void ) {
    std::string tmp;
    if ( ! ReadWordOnLine( tmp ) ) {
        ERR("Parse Error: Couldn't read/find int in script %s:%u\n", file->GetFilePath().c_str(), line );
        return 0;
    }
    return String::ToInt(tmp);
}

float ParserSSV::ReadFloat( void ) {
    std::string tmp;
    if ( ! ReadWordOnLine( tmp ) ) {
        ERR("Parse Error: Couldn't read/find float in script %s:%u\n", file->GetFilePath().c_str(), line );
        return 0;
    }
    return String::ToFloat(tmp);
}

uint ParserSSV::ReadUInt( void ) {
    return static_cast< uint >( ReadInt() );
}

void ParserSSV::ReadUInt( uint& val ) {
    val = static_cast< uint >( ReadInt() );
}

void ParserSSV::ReadInt( int& val ) {
    val = ReadInt();
}

void ParserSSV::ReadFloat( float& val ) {
    val = ReadFloat();
}
