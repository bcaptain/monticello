// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_FILE_BINARY_H_
#define SRC_CLIB_FILE_BINARY_H_

#include <cstdlib>
#include "../../warnings.h"
#include "../../macros.h"
#include "../../std.h"
#include "../../strings.h"
#include "../../bitwise.h"
#include "../../types.h"
#include "../../strings.h"
#include "../base/file.h"
#include "../../math/vec2t-dec.h"
#include "../../math/vec3t-dec.h"
#include "../../math/quat.h"

/*!

ParserBinary \n\n

to read/write binary data

**/

class ParserBinary {
public:
    ParserBinary( void ) = delete;
    ParserBinary( File& file_to_parse ); //!< file_to_parse is not resource managed
    ParserBinary( ParserBinary& other ) = delete;
    virtual ~ParserBinary( void );

public:
    ParserBinary& operator=( ParserBinary& other ) = delete;

public:
    template< typename TYPE >
    TYPE    Read( void );

    template< typename TYPE >
    void    Read( TYPE& data );

    template< typename TYPE >
    void    Write( const TYPE& data );

    int     ReadInt( void );
    uint    ReadUInt( void );
    int8    ReadInt8( void );
    Uint8   ReadUInt8( void );
    int16   ReadInt16( void );
    Uint16  ReadUInt16( void );
    bool    ReadBool( void );
    float   ReadFloat( void );
    Vec2f   ReadVec2f( void );
    Vec3f   ReadVec3f( void );
    Quat    ReadQuat( void );
    std::string  ReadStr( void );
    std::string  ReadStr( const std::size_t len );

    void    ReadInt( int& data );
    void    ReadUInt( Uint& data );
    void    ReadInt8( int8& data );
    void    ReadUInt8( Uint8& data );
    void    ReadInt16( int16& data );
    void    ReadUInt16( Uint16& data );
    void    ReadBool( bool& data );
    void    ReadFloat( float& data );
    void    ReadVec2f( Vec2f& data );
    void    ReadVec3f( Vec3f& data );
    void    ReadQuat( Quat& data );
    void    ReadStr( std::string& data );
    void    ReadStr( std::string& data, const std::size_t len );

    void    WriteInt( const int data );
    void    WriteUInt( const uint data );
    void    WriteInt8( int8 data );
    void    WriteUInt8( Uint8 data );
    void    WriteUInt16( const Uint16 data );
    void    WriteInt16( const int16 data );
    void    WriteBool( const bool data );
    void    WriteFloat( const float data );
    void    WriteVec2f( const Vec2f& data );
    void    WriteVec3f( const Vec3f& data );
    void    WriteQuat( const Quat& data );
    void    WriteStr( const std::string& data );
    void    WriteStr( const char* data, const int size );

    // ** the following are just inlines that call the same functions in File* file

    void ReadChar( char* data_out, const Uint32 len );
    char ReadChar( void );
    void ReadChar( char& data_out );
    void ReadChar( char* data_out );

    void ReadUChar( uchar* data_out, const Uint32 len );
    uchar ReadUChar( void );
    void ReadUChar( uchar& data_out );
    void ReadUChar( uchar* data_out );

    void WriteChar( const char* data_out, const Uint32 len );
    void WriteChar( const char data_out );

protected:
    File*   file; //!< not resource managed
};

template< typename TYPE >
TYPE ParserBinary::Read( void ) {
    TYPE ret;
    Read<TYPE>( ret );
    return ret;
}

template< typename TYPE >
inline void ParserBinary::Read( TYPE& data ) {
    file->clib_fread(&data, sizeof(TYPE), 1 );
}

template< typename TYPE >
inline void ParserBinary::Write( const TYPE& data ) {
    fwrite(&data, sizeof(TYPE), 1, file->GetFilePointer());
}

#endif  //SRC_CLIB_FILE_BINARY
