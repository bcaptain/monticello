// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../../warnings.h"
#include "../../types.h"
#include "./binary.h"

ParserBinary::~ParserBinary( void )
{ }

ParserBinary::ParserBinary( File& file_to_parse )
    : file( &file_to_parse )
{ }

void ParserBinary::WriteVec2f( const Vec2f& data ) {
    fwrite(&data.x, sizeof(float), 1, file->GetFilePointer());
    fwrite(&data.y, sizeof(float), 1, file->GetFilePointer());
}

void ParserBinary::WriteVec3f( const Vec3f& data ) {
    fwrite(&data.x, sizeof(float), 1, file->GetFilePointer());
    fwrite(&data.y, sizeof(float), 1, file->GetFilePointer());
    fwrite(&data.z, sizeof(float), 1, file->GetFilePointer());
}

void ParserBinary::WriteQuat( const Quat& data ) {
    fwrite(&data.x, sizeof(float), 1, file->GetFilePointer());
    fwrite(&data.y, sizeof(float), 1, file->GetFilePointer());
    fwrite(&data.z, sizeof(float), 1, file->GetFilePointer());
    fwrite(&data.w, sizeof(float), 1, file->GetFilePointer());
}

void ParserBinary::WriteStr( const char* data, const int sz ) {
    fwrite(&sz, sizeof(int), 1, file->GetFilePointer());
    fwrite(data, 1, 1, file->GetFilePointer());
}

void ParserBinary::WriteStr( const std::string& data ) {
    std::size_t len = data.size();
    fwrite(&len, sizeof( std::size_t ), 1, file->GetFilePointer());
    fwrite(data.c_str(), 1, static_cast<size_t>(len), file->GetFilePointer());
}

void ParserBinary::ReadStr( std::string& data ) {
    ReadStr( data, ReadUInt() );
}

void ParserBinary::ReadStr( std::string& data, const std::size_t len ) {
    std::vector<char> cstr(len+1);

    file->clib_fread(cstr.data(), 1, static_cast<size_t>(len) );

    cstr[len]='\0';
    data = cstr.data();
}

void ParserBinary::ReadVec2f( Vec2f& data ) {
    file->clib_fread(&data.x, sizeof(float), 1 );
    file->clib_fread(&data.y, sizeof(float), 1 );
}

void ParserBinary::ReadVec3f( Vec3f& data ) {
    file->clib_fread(&data.x, sizeof(float), 1 );
    file->clib_fread(&data.y, sizeof(float), 1 );
    file->clib_fread(&data.z, sizeof(float), 1 );
}

void ParserBinary::ReadQuat( Quat& data ) {
    file->clib_fread(&data.x, sizeof(float), 1 );
    file->clib_fread(&data.y, sizeof(float), 1 );
    file->clib_fread(&data.z, sizeof(float), 1 );
    file->clib_fread(&data.w, sizeof(float), 1 );
}

int ParserBinary::ReadInt( void ) {
    int ret;
    ReadInt( ret );
    return ret;
}

uint ParserBinary::ReadUInt( void ) {
    uint ret;
    ReadUInt( ret );
    return ret;
}

bool ParserBinary::ReadBool( void ) {
    bool ret;
    ReadBool( ret );
    return ret;
}

Uint8 ParserBinary::ReadUInt8( void ) {
    Uint8 ret;
    ReadUInt8( ret );
    return ret;
}

int8 ParserBinary::ReadInt8( void ) {
    int8 ret;
    ReadInt8( ret );
    return ret;
}

Uint16 ParserBinary::ReadUInt16( void ) {
    Uint16 ret;
    ReadUInt16( ret );
    return ret;
}

int16 ParserBinary::ReadInt16( void ) {
    int16 ret;
    ReadInt16( ret );
    return ret;
}

float ParserBinary::ReadFloat( void ) {
    float ret;
    ReadFloat( ret );
    return ret;
}

Vec2f ParserBinary::ReadVec2f( void ) {
    Vec2f ret;
    ReadVec2f( ret );
    return ret;
}

Vec3f ParserBinary::ReadVec3f( void ) {
    Vec3f ret;
    ReadVec3f( ret );
    return ret;
}

Quat ParserBinary::ReadQuat( void ) {
    Quat ret;
    ReadQuat( ret );
    return ret;
}

std::string ParserBinary::ReadStr( const std::size_t len ) {
    std::string ret;
    ReadStr( ret, len );
    return ret;
}

std::string ParserBinary::ReadStr( void ) {
    std::string ret;
    ReadStr( ret );
    return ret;
}

void ParserBinary::ReadChar( char* data_out, const Uint32 len ) {
    file->ReadChar( data_out, len );
}

char ParserBinary::ReadChar( void ) {
    return file->ReadChar();
}

void ParserBinary::ReadChar( char& data_out ) {
    file->ReadChar( data_out );
}

void ParserBinary::ReadChar( char* data_out ) {
    file->ReadChar( data_out );
}

void ParserBinary::ReadUChar( uchar* data_out, const Uint32 len ) {
    file->ReadUChar( data_out, len );
}

uchar ParserBinary::ReadUChar( void ) {
    return file->ReadUChar();
}

void ParserBinary::ReadUChar( uchar& data_out ) {
    file->ReadUChar( data_out );
}

void ParserBinary::ReadUChar( uchar* data_out ) {
    file->ReadUChar( data_out );
}

void ParserBinary::WriteChar( const char* data_out, const Uint32 len ) {
    file->WriteChar( data_out, len );
}

void ParserBinary::WriteChar( const char data_out ) {
    file->WriteChar( data_out );
}

void ParserBinary::WriteUInt8( const Uint8 data ) {
    fwrite(&data, sizeof(Uint8), 1, file->GetFilePointer());
}

void ParserBinary::WriteInt8( const int8 data ) {
    fwrite(&data, sizeof(int8), 1, file->GetFilePointer());
}

void ParserBinary::WriteInt16( const int16 data ) {
    fwrite(&data, sizeof(int16), 1, file->GetFilePointer());
}

void ParserBinary::WriteUInt16( const Uint16 data ) {
    fwrite(&data, sizeof(Uint16), 1, file->GetFilePointer());
}

void ParserBinary::WriteUInt( const uint data ) {
    fwrite(&data, sizeof(uint), 1, file->GetFilePointer());
}

void ParserBinary::WriteInt( const int data ) {
    fwrite(&data, sizeof(int), 1, file->GetFilePointer());
}

void ParserBinary::WriteBool( const bool data ) {
    fwrite(&data, sizeof(bool), 1, file->GetFilePointer());
}

void ParserBinary::WriteFloat( const float data ) {
    fwrite(&data, sizeof(float), 1, file->GetFilePointer());
}

void ParserBinary::ReadInt( int& data ) {
    file->clib_fread(&data, sizeof(int), 1 );
}

void ParserBinary::ReadUInt( uint& data ) {
    file->clib_fread(&data, sizeof(uint), 1 );
}

void ParserBinary::ReadBool( bool& data ) {
    file->clib_fread(&data, sizeof(bool), 1 );
}

void ParserBinary::ReadInt16( int16& data ) {
    file->clib_fread(&data, sizeof(int16), 1 );
}

void ParserBinary::ReadUInt16( Uint16& data ) {
    file->clib_fread(&data, sizeof(Uint16), 1 );
}

void ParserBinary::ReadFloat( float& data ) {
    file->clib_fread(&data, sizeof(float), 1 );
}

void ParserBinary::ReadInt8( int8& data ) {
    file->clib_fread(&data, sizeof(int8), 1 );
}

void ParserBinary::ReadUInt8( Uint8& data ) {
    file->clib_fread(&data, sizeof(Uint8), 1 );
}
