// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../warnings.h"
#include "../message.h"
#include "./tga.h"
#include "./base/image.h"

const int TGA_UNCOMPRESSED = 2;
const int TGA_RLE = 10;

ParserTGA::ParserTGA( File& file_to_parse )
    : ParserBinary( file_to_parse )
{ }

bool ParserTGA::Load( ImageData& imageData_out ) {

    // ** make sure the file is open

    if ( ! file ) {
        ERR("ParserTGA: Something went horribly wrong, ParserTGA's file is null\n");
        return false;
    }

    if ( ! file->IsOpen() ) {
        ERR("Cannot load TGA, file not opened: %s\n", file->GetFilePath().c_str() );
        return false;
    }

    // there's no file signature for a TGA, it just starts off with data. - except maybe as explained here: http://www.ludorg.net/amnesia/TGA_File_Format_Spec.html

    uchar id_len;
    Uint16 map_entries;
    Uint16 map_entry_size;
    uchar *img_id=nullptr;
    uchar *map_data=nullptr;
    std::size_t i;
    uchar tmp[4];

    //todo: greyscale

    // ****************
    //
    // Header
    //
    // ****************

    uchar map_type;
    uchar img_type;

    ReadUChar( id_len );
    ReadUChar( map_type );
    ReadUChar( img_type );

    if ( img_type != TGA_RLE && img_type != TGA_UNCOMPRESSED ) {
        ERR("unknown or unimplimented TGA image type: %s\n", file->GetFilePath().c_str() );
        return false;
    }

    // **** field 4
    ReadChar(); // first byte of map_data
    ReadUInt16( map_entries );
    ReadUInt16( map_entry_size );

    // **** field 5
    ReadInt16( imageData_out.originx );
    ReadInt16( imageData_out.originy );
    ReadUInt16( imageData_out.width );
    ReadUInt16( imageData_out.height );

    if ( !Maths::IsPowerOf2<Uint16>( imageData_out.width ) || !Maths::IsPowerOf2<Uint16>( imageData_out.height ) )
        WARN("Image dimensions not power of 2: %s\n", file->GetFilePath().c_str() );

    ReadChar( imageData_out.bit_depth );
    imageData_out.bpp = static_cast<uint>( imageData_out.bit_depth ) / 8;

    if ( imageData_out.bit_depth == 24 ) {
        imageData_out.format = GLPixelFormat_RGB;
    } else if ( imageData_out.bit_depth == 32 ) {
        imageData_out.format = GLPixelFormat_RGBA;
    }

    ReadChar(); // descriptor

    // ****************
    //
    // Image & Color Map Data
    //
    // ****************

    // **** field 6
    if ( id_len != 0 ) {
        img_id=new uchar[id_len];
        ReadUChar(img_id, id_len);  // byte 20
    }

    // **** field 7
    if ( map_type != 0 ) {
        map_data=new uchar[map_entries*map_entry_size];

        for ( i=0; i<map_entries; ++i )
            ReadUChar(map_data, map_entry_size);  // byte 21-map_entry_size

    } else {
        map_data=nullptr;
    }

    // ****************
    //
    // Image Data
    //
    // ****************

    // **** field 8
    std::size_t packets = imageData_out.bpp * imageData_out.width * imageData_out.height;
    imageData_out.buffer=new uchar[packets+1]; // +1 because ReadUChar( char*, len ) will add a nullchar at index [len]

    if ( img_type == TGA_UNCOMPRESSED ) {
        ReadUChar( imageData_out.buffer, packets - imageData_out.bpp);
        for ( i = 0; i < packets - imageData_out.bpp; i += imageData_out.bpp ) {
            tmp[0]=imageData_out.buffer[i];
            imageData_out.buffer[i]=imageData_out.buffer[i+2];
            imageData_out.buffer[i+2]=tmp[0];
        }
    } else if ( img_type == TGA_RLE ) {
        uint idx,cnt;

        for ( i = 0; i < packets - imageData_out.bpp; ) {
            ReadUChar(tmp); // read packet header

            // get count ( only right 7 bits )
            cnt = static_cast<uint>( static_cast<uchar>( (tmp[0] << 1) ) >> 1 );
            cnt += 1; // This 7 bit value is actually encoded as 1 less than the number of pixels in the packet
            cnt *= imageData_out.bpp;
            cnt += i; // end offset for this pixel(s) in raw image

            if ( tmp[0] >> 7 != 0 ) {
                // an rle packet

                ReadUChar( &imageData_out.buffer[i], imageData_out.bpp); // read pixel

                // GRB to RGB
                tmp[0] = imageData_out.buffer[i];
                imageData_out.buffer[i] = imageData_out.buffer[i+2];
                imageData_out.buffer[i+2] = tmp[0];

                // do run length

                for ( idx = i, i += imageData_out.bpp; i < cnt; i += imageData_out.bpp ) {
                    imageData_out.buffer[i] = imageData_out.buffer[idx];
                    imageData_out.buffer[i+1] = imageData_out.buffer[idx+1];
                    imageData_out.buffer[i+2] = imageData_out.buffer[idx+2];
                    if ( imageData_out.bpp > 3 )
                        imageData_out.buffer[i+3] = imageData_out.buffer[idx+3];
                }

                continue;
            } else {
                // a non-rle packet

                for ( ; i < cnt; i += imageData_out.bpp ) {
                    ReadUChar( &imageData_out.buffer[i], imageData_out.bpp);

                    // GRB to RGB
                    tmp[0] = imageData_out.buffer[i];
                    imageData_out.buffer[i] = imageData_out.buffer[i+2];
                    imageData_out.buffer[i+2] = tmp[0];
                }

                continue;
            }
        }
    }

    // develoepr area
    // extension area
    // footer

    DELNULLARRAY(img_id);
    DELNULLARRAY(map_data);

    return true;
}
