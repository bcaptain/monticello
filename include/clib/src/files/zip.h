// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ZIPFILE_H_
#define SRC_CLIB_ZIPFILE_H_

#include <zip.h>
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "../strings.h"

class File;
class FileArchived;

class FileZip {
public:
    FileZip( void );
    explicit FileZip( const char* zipfile_to_open );
    FileZip( const FileZip& other ) = delete;
    ~FileZip( void );

public:
    FileZip& operator=( const FileZip& other ) = delete;

public:
    bool Open( const char* fname );
    bool IsOpen( void ) const;

    bool GetPathFromIndex( const Uint64 file_index, std::string& path_out ) const;

    bool OpenFile( const char* file_name, File& temp_file_out ) const;
        bool OpenFile( const Uint64 file_index, File& temp_file_out ) const; //!< Overload for OpenFile() that takes an entry index within the archive instead of a filepath

    void Close( void );
    Uint64 NumFiles( void ) const;

private:
    bool SaveToTempFile( const char* temp_file_name, FileArchived& archived_file, File& temp_file_out ) const;
    int ReadFileToBuffer( const char* file_name, FileArchived& archived_file ) const; //!< returns bytes read, or negative for error

public:
    zip *archive;
    std::string archive_name;
};

#endif  // SRC_CLIB_ZIPFILE
