// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_IMAGEFILE_H_
#define SRC_CLIB_IMAGEFILE_H_

#include <GL/glew.h>
#include <GL/gl.h>

#include "../../warnings.h"
#include "../../macros.h"
#include "../../types.h"
#include "../../std.h"
#include "../../strings.h"
#include "../parsers/binary.h"

class ParserTGA;

typedef Uint GLPixelFormatT_BaseType;
enum GLPixelFormatT : GLPixelFormatT_BaseType {
    GLPixelFormat_RGB = GL_RGB
    , GLPixelFormat_RGBA = GL_RGBA
};

class ImageFileDef {
public:
    ImageFileDef( void );
    ImageFileDef( const ImageFileDef& other );
    ~ImageFileDef( void ) = default;

public:
    ImageFileDef& operator=( const ImageFileDef& other );

public:
    std::string name;
    std::string type;
    std::string path;
    bool loaded;
};

class ImageData {
    friend class ParserTGA; // ParserTGA fills this class

public:
    ImageData( void );
    ImageData( const ImageData& other ) = delete;
    ~ImageData( void );

public:
    ImageData& operator=( const ImageData& other ) = delete;

public:
    void Clear( void );

    const uchar* GetBuffer( void ) const;

    Uint16 GetWidth( void ) const;
    Uint16 GetHeight( void ) const;
    int GetFormat( void ) const;
    char GetBitDepth( void ) const;
    uint GetBPP( void ) const;
    
    void SetName( const std::string& to );
    std::string GetName( void ) const;

protected:
    std::string name;
    Uint16 width;
    Uint16 height;
    int16 originx;
    int16 originy;
    int format;
    uchar *buffer;
    uint bpp;
    char bit_depth;
};

#endif  // SRC_CLIB_IMAGEFILE
