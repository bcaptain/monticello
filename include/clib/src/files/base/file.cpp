// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../../warnings.h"
#include "../../message.h"
#include "../../types.h"
#include "./file.h"
#include "../filesystem.h"

File::File( File& other )
    : filepath()
    , container()
    , mode()
    , afile(nullptr)
    , buffer(nullptr)
    , size(-1)
    {
    Open( other.filepath.c_str(), other.mode.c_str() );
}

File::File( void )
    : filepath()
    , container()
    , mode()
    , afile(nullptr)
    , buffer(nullptr)
    , size(-1)
    {
}

File::File( const char* fname, const char * fmode )
    : filepath()
    , container()
    , mode()
    , afile(nullptr)
    , buffer(nullptr)
    , size(-1)
    {
    Open(fname, fmode);
}

File::File( const char* fname )
    : filepath()
    , container()
    , mode()
    , afile(nullptr)
    , buffer(nullptr)
    , size(-1)
    {
    Open(fname, "rb");
}

File::~File( void ) {
    DELNULLARRAY( buffer );
    Close();
}

void File::FFlush( void ) const {
    if ( afile )
        fflush( afile );
}

void File::clib_fread( void * ptr, size_t sz, std::size_t count ) const {
    const std::size_t num_read = EndOfFile() ? 0 : fread(ptr, sz, count, afile );
    if ( num_read != count ) {
        ERR("Could not read all %u bytes (got %u) from file %s from %s before EoF.\n"
            , sz * count
            , num_read
            , filepath.c_str()
            , container.size() == 0 ? "filesystem" : container.c_str()
        );
    }
}

File& File::operator=( File& other ) {
    if ( this == &other )
        return *this;

    Open( other.filepath.c_str(), other.mode.c_str() );
    // buffer intentionally not set
    container = other.container;
    return *this;
}

bool File::EndOfFile( void ) const {
    return FTell( afile ) >= size;
}

bool File::IsOpen( void ) const {
    return afile != nullptr;
}

int32 File::SeekSet( const int32 pos ) {
    return SeekSet(afile, pos);
}

int32 File::SeekCur( const int32 pos ) {
    return SeekCur(afile, pos);
}

int32 File::SeekEnd( const int32 pos ) {
    return SeekEnd(afile, pos);
}

void File::WriteChar( const char* data_out, const uint len ) {
    fwrite(data_out, 1, len, afile);
}

void File::PeekChar( char& data_out ) {
    ReadChar( &data_out );
    SeekCur( -1 );
}

char File::PeekChar( void ) {
    char ret;
    ReadChar( ret );
    SeekCur( -1 );
    return ret;
}

char File::ReadChar( void ) {
    char ret;
    ReadChar( ret );
    return ret;
}

uchar File::ReadUChar( void ) {
    uchar ret;
    ReadUChar( ret );
    return ret;
}

void File::ReadChar( char* data_out, const Uint32 len ) {
    ASSERT( data_out );
    clib_fread(data_out, 1, len );
}

void File::ReadUChar( uchar* data_out, const Uint32 len ) {
    ASSERT( data_out );
    clib_fread(data_out, 1, len );
}

bool File::Open( const char* fname, const char * fmode ) {
    Close();

    filepath = fname;
    container.erase(); // clear means container is filesystem
    mode = fmode;
    size = -1;
    DELNULLARRAY( buffer );

    if ( fname == nullptr )
        return false;

    if ( fname[0] == '\0' )
        return false;

    if ( FileSystem::IsDir( fname ) ) {
        ERR("Cannot open %s as a file because it is a directory.\n", fname);
        return false;
    }

    afile=fopen(fname, mode.c_str() );

    if ( afile == nullptr )
        return false;

    LoadSeekInfo();
    return true;
}

void File::Close( void ) {
    if ( afile != nullptr ) {
        fclose(afile);
        afile = nullptr;
    }
}

int32 File::FTell( void ) const {
    return FTell( afile );
}

bool File::ReadToBuffer( void ) {
    SeekSet(0);

    DELNULLARRAY(buffer);

    if ( EndOfFile() )
        return false;

    size = GetSize();

    buffer = new char[size+1];
    ReadChar( buffer, static_cast< Uint32 >( size ) );
    buffer[size] = '\0';

    return true;
}

bool File::LoadSeekInfo( void ) {
    size = -1;

    if ( afile == nullptr )
        return false;

    const int32 pos = FTell(afile);
    if ( pos < 0 ) {
        ERR("File::LoadSeekInfo: %s\n", strerror( -pos ));
        return false;
    }

    SeekEnd(0);
    size = FTell(afile);
    if ( size < 0 ) {
        ERR("File::LoadSeekInfo: %s\n", strerror( -size ));
        return false;
    }

    SeekSet(pos);
    return true;
}

int32 File::GetSize( FILE *afile ) {
    int32 pos, len;

    pos=FTell(afile);

    if ( SeekEnd(afile, 0) != 0 )
        return 0;

    len=FTell(afile);

    if ( SeekSet(afile, pos) != 0 )
        return 0;

    return len;
}

bool File::GetExists( const char *path ) {
    FILE *file;

    if ( (file=fopen(path, "rb")) == nullptr )
        return false;

    fclose( file );
    return true;
}

const std::string File::GetFileContainerName_Full( void ) const {
    return container.size() > 0 ? container : "filesystem";
}

void File::SetContainerName( const char* _name ) {
    container = _name;
}

FILE* File::GetFilePointer( void ) const {
    return afile;
}

// **** ReadChar

void File::ReadChar( char* data_out ) {
    ReadChar( data_out, 1 );
}

void File::ReadChar( char& data_out ) {
    ReadChar( &data_out, 1 );
}

// **** ReadUChar

void File::ReadUChar( uchar* data_out ) {
    ReadUChar( data_out, 1 );
}

void File::ReadUChar( uchar& data_out ) {
    ReadUChar( &data_out, 1 );
}

// **** WriteChar

void File::WriteChar( const char data ) {
    WriteChar( &data, 1 );
}

// **** Statics

int32 File::SeekSet( FILE *afile, const int32 pos ) {
    return fseek( afile, pos, SEEK_SET );
}

int32 File::SeekCur( FILE *afile, const int32 pos ) {
    return fseek( afile, pos, SEEK_CUR );
}

int32 File::SeekEnd( FILE *afile, const int32 pos ) {
    return fseek( afile, pos, SEEK_END );
}

int32 File::FTell( FILE *afile ) {
    return ftell( afile );
}

// **** Other

int32 File::GetSize( void ) const {
    return size;
}

const std::string File::GetFilePath( void ) const {
    return filepath;
}

const std::string File::GetFileContainerName( void ) const {
    return container;
}

const char* File::GetBuffer( void ) const {
    return buffer;
}
