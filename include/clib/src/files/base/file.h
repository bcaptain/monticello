// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_FILES_H_
#define SRC_CLIB_FILES_H_

#include <cstdlib>
#include "../../warnings.h"
#include "../../macros.h"
#include "../../types.h"
#include "../../std.h"
#include "../../strings.h"
#include "../../bitwise.h"
#include "../../strings.h"

/*!

File \n\n

generic file access functions.

**/

class File {
public:
    File( void );
    explicit File( const char* fname );
    File( const char* fname, const char * fmode );
    File( File& other );
    virtual ~File( void );

public:
    File& operator=( File& other );

public:
    virtual bool Open( const char* fname, const char* fmode="rb" );
    virtual void Close( void );

    virtual int32 FTell( void ) const;
    void FFlush( void ) const;

    virtual void PeekChar( char& data_out );
        virtual char PeekChar( void ); //!< Less efficient override to PeekChar( char& )

    virtual int32 SeekSet( const int32 pos );
    virtual int32 SeekCur( const int32 pos );
    virtual int32 SeekEnd( const int32 pos );

    virtual void ReadChar( char* data_out, const Uint32 len );
        char ReadChar( void ); //!< Less efficient verride to ReadChar( uchar*, const Uint32 )
        void ReadChar( char& data_out ); //!< override to ReadChar( uchar*, const Uint32 )
        void ReadChar( char* data_out ); //!< override to ReadChar( uchar*, const Uint32 )

    virtual void ReadUChar( uchar* data_out, const Uint32 len );
        uchar ReadUChar( void ); //!< Less efficient  to ReadUChar( uchar*, const Uint32 )
        void ReadUChar( uchar& data_out ); //!< override to ReadUChar( uchar*, const Uint32 )
        void ReadUChar( uchar* data_out ); //!< override to ReadUChar( uchar*, const Uint32 )

    virtual void WriteChar( const char* data_out, const Uint32 len );
        void WriteChar( const char data_out );//!< override to WriteChar( const char*, const Uint32* )

    virtual bool IsOpen( void ) const;
    virtual bool EndOfFile( void ) const;

    int32 GetSize( void ) const;

    const std::string GetFilePath( void ) const;
    const std::string GetFileContainerName( void ) const;
    const std::string GetFileContainerName_Full( void ) const;
    void SetContainerName( const char* _name );

    virtual bool ReadToBuffer( void ); //!< set the file pointer to the first char of the file and read the whole file into a buffer. This doesn't affect other Read operations, but the state of the file at the end will be EoF. The buffer will be valid until another call to ReadToBuffer() is made or this object deconstructs or Open() is called again (true for all derived calsses). Note that the contents are unaffected even after closing or a new file.

    const char* GetBuffer( void ) const; //!< get the buffer read in by ReadToBuffer()
    FILE* GetFilePointer( void ) const;

public:
    static int32 GetSize( FILE *afile );
    static bool GetExists( const char *path );
    static int32 FTell( FILE *afile );
    static int32 SeekSet( FILE *afile, const int32 pos );
    static int32 SeekCur( FILE *afile, const int32 pos );
    static int32 SeekEnd( FILE *afile, const int32 pos );

    void clib_fread( void * ptr, size_t sz, size_t count ) const; //!< this is just a wrapper for fread() which memset's our variable to zeros if the file isn't open

protected:
    bool    LoadSeekInfo( void );

protected:
    std::string filepath; //!< full path and file name
    std::string container; //!< example: the path to a particular zip file. Blank if on the filesystem
    std::string mode;
    FILE *afile;
    char *buffer;
    int32 size;
};

#endif  //SRC_CLIB_FILES
