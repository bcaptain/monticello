// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "../../warnings.h"
#include "../../math/vec3t.h"
#include "./image.h"

ImageData::~ImageData( void ) {
    delete[] buffer;
}

void ImageData::Clear( void ) {
    DELNULLARRAY(buffer);

    width = 0;
    height = 0;
    originx = 0;
    originy = 0;
    format = 0;
    bpp = 0;
    bit_depth = 0;
}

ImageData::ImageData( void )
    : name()
    , width(0)
    , height(0)
    , originx(0)
    , originy(0)
    , format(0)
    , buffer(nullptr)
    , bpp(0)
    , bit_depth(0)
    {
}

ImageFileDef::ImageFileDef( void )
    : name()
    , type()
    , path()
    , loaded(false)
    {
}

ImageFileDef::ImageFileDef( const ImageFileDef& other )
    : name(other.name)
    , type(other.type)
    , path(other.path)
    , loaded(other.loaded)
    {

}

ImageFileDef& ImageFileDef::operator=( const ImageFileDef& other ) {
    name = other.name;
    type = other.type;
    path = other.path;
    loaded = other.loaded;
    return *this;
}

void ImageData::SetName( const std::string& to ) {
    name = to;
}
    
std::string ImageData::GetName( void ) const {
    return name;
}

const uchar* ImageData::GetBuffer( void ) const {
    return buffer;
}

Uint16 ImageData::GetWidth( void ) const {
    return width;
}

Uint16 ImageData::GetHeight( void ) const {
    return height;
}

int ImageData::GetFormat( void ) const {
    return format;
}

char ImageData::GetBitDepth( void ) const {
    return bit_depth;
}

uint ImageData::GetBPP( void ) const {
    return bpp;
}
