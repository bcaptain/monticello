// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <cstring>
#include <sys/stat.h>
#include "../warnings.h"
#include "../message.h"
#include "./zip.h"
#include "./archived.h"
#include "./filesystem.h"

FileZip::FileZip( void )
    : archive( nullptr )
    , archive_name()
{ }

FileZip::FileZip( const char* zipfile_to_open )
    : archive( nullptr )
    , archive_name()
    {
    Open( zipfile_to_open );
}

FileZip::~FileZip( void ) {
    Close();
}

bool FileZip::OpenFile( const Uint64 file_index, File& temp_file_out ) const {
    FileArchived archived_file;

    const int stat_error = zip_stat_index( archive, file_index, ZIP_FL_NOCASE, &archived_file.stat );
    if ( stat_error != 0 ) {
        ERR("The file at index %i does not exist in archive %s (error %i)\n", file_index, archive_name.c_str(), stat_error );
        return false;
    }

    // when opening by index, we don't throw an error because it is likely the file we're trying to access at this index is probably just a directory
    if ( archived_file.stat.size < 1 ) {
        return false;
    }

    return SaveToTempFile( archived_file.stat.name, archived_file, temp_file_out );
}

bool FileZip::OpenFile( const char* file_name, File& temp_file_out ) const {
    FileArchived archived_file;

    const int stat_error = zip_stat( archive, file_name, ZIP_FL_NOCASE, &archived_file.stat );
    if ( stat_error != 0 ) {
        ERR("The file %s does not exist in archive %s (error %i)\n", file_name, archive_name.c_str(), stat_error );
        return false;
    }

    if ( archived_file.stat.size < 1 ) {
        ERR("Could not retrieve file %s from archive %s\n", file_name, archive_name.c_str() );
        return false;
    }

    return SaveToTempFile( file_name, archived_file, temp_file_out );
}

bool FileZip::GetPathFromIndex( const Uint64 file_index, std::string& path_out ) const {
    struct zip_stat stat;

    const int stat_error = zip_stat_index( archive, file_index, ZIP_FL_NOCASE, &stat );
    if ( stat_error != 0 ) {
        ERR("The file at index %i does not exist in archive %s (error %i)\n", file_index, archive_name.c_str(), stat_error );
        return false;
    }

    path_out = stat.name;
    return true;
}

bool FileZip::SaveToTempFile( const char* temp_file_name, FileArchived& archived_file, File& temp_file_out ) const {
    if ( ReadFileToBuffer( temp_file_name, archived_file ) < 1 )
        return false;

    std::string path = "./tmp/";
    #ifdef _WIN32
        mkdir( path.c_str() );
    #elif defined( __linux )
        mkdir( path.c_str(), S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP | S_IXUSR | S_IXGRP );
    #else
        #error "Unknown platform"
    #endif // _WIN32

    if ( ! FileSystem::IsDir( path.c_str() ) ) {
        ERR("Invalid temp dir: %s\n", path.c_str() );
        return false;
    }

    path += "monticello/";
    path += temp_file_name;

    const std::string dir = String::GetFilePath( path );

    if ( ! FileSystem::CreateDir( dir.c_str() ) ) {
        ERR("Couldn't create temp file at %s\n", temp_file_name);
        return false;
    }

    temp_file_out.Open( path.c_str(), "wb" );

    if ( ! temp_file_out.IsOpen() ) {
        ERR("Could not open temp file for write: %s\n", path.c_str() );
        return false;
    }

    temp_file_out.WriteChar( archived_file.GetBuffer(), static_cast< uint >( archived_file.GetSize() ) );
    temp_file_out.Close();
    temp_file_out.Open( path.c_str(), "rb" );
    temp_file_out.SetContainerName( archive_name.c_str() );
    return true;
}

int FileZip::ReadFileToBuffer( const char* file_name, FileArchived& archived_file ) const {
    // thie is a private function, so we will always know for certain the information passed in is valid
    zip_file* file = zip_fopen(archive, file_name, 0); // open the file from the archive
    char* buf = archived_file.FileSetup( archived_file.stat, archive_name );

    if ( file == nullptr ) {
        ERR("Couldnot open file %s within archive %s.\n", file_name, archive_name.c_str() );
        return -1;
    }

    // signed int is big enough for any files we'll be dealing with, plus we need to stay signed to return errors
    int bytes_read = static_cast< int >( zip_fread(file, buf, archived_file.stat.size) );

    if ( bytes_read < 1 ) {
        ERR("Could not read file %s within archive %s.\n", file_name, archive_name.c_str() );
    }

    zip_fclose(file); // close file

    return bytes_read;
}

bool FileZip::Open( const char* fname ) {
    int err = 0;
    archive = zip_open(fname, 0, &err);

    if ( archive == nullptr || err != 0 ) {
        Close(); // in case archive is nut null and err is not zero
        ERR("Couldn't find archive %s\n", fname);
        archive_name.erase();
        return false;
    }

    archive_name = fname;
    return true;
}

void FileZip::Close( void ) {
    zip_close( archive );
    archive = nullptr;
    archive_name.erase();
}

bool FileZip::IsOpen( void ) const {
    return archive != nullptr;
}

Uint64 FileZip::NumFiles( void ) const {
    return static_cast<Uint64>( zip_get_num_entries( archive, 0 ) );
}
