// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <memory>
#include "../warnings.h"
#include "../message.h"
#include "./archived.h"

FileArchived::FileArchived( void )
    : stat()
    , pos(-1)
    {
    memset( &stat, 0, sizeof( stat ) );
}

FileArchived::~FileArchived( void ) {
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter" // we know
#pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn" // no
bool FileArchived::Open( const char* fname, const char* fmode ) {
    ASSERT_MSG( false, "FileArchived cann't manually open files, pass it instead fo FileZip::GetFile().\n");
    DIE("Illegal method call: FileArchived::Open()\n");
    return false;
}
#pragma GCC diagnostic pop

void FileArchived::Close( void ) {
    memset( &stat, 0, sizeof( stat ) );
    pos=-1;
}

int32 FileArchived::FTell( void ) const {
    return pos;
}

int32 FileArchived::SeekSet( const int32 val ) {
    pos = val;
    return pos;
}

int32 FileArchived::SeekCur( const int32 val ) {
    pos += val;
    return pos;
}

int32 FileArchived::SeekEnd( const int32 val ) {
    pos = size - val;
    return pos;
}

void FileArchived::PeekChar( char& data_out ) {
    data_out = buffer[pos];
}

char FileArchived::PeekChar( void ) {
    return buffer[pos];
}

void FileArchived::ReadChar( char* data_out, const Uint32 len ) {
    for ( Uint32 p=0; p<len; ++p ) {
        data_out[p] = buffer[pos];
        ++pos;
    }
}

void FileArchived::ReadUChar( uchar* data_out, const Uint32 len ) {
    for ( Uint32 p=0; p<len; ++p ) {
        data_out[p] = static_cast< uchar >( buffer[pos] );
        ++pos;
    }
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter" // we know
#pragma GCC diagnostic ignored "-Wsuggest-attribute=noreturn" // no
void FileArchived::WriteChar( const char* data_out, const Uint32 len ) {
    ASSERT_MSG( false, "You cannot change a file within a ZIP archive in this way.\n");
}

#pragma GCC diagnostic pop

bool FileArchived::EndOfFile( void ) const {
    return pos >= size;
}

bool FileArchived::IsOpen( void ) const {
    return size > 0;
}

char* FileArchived::FileSetup( const struct zip_stat& filestat, const std::string& path_to_zipfile ) {
    DELNULLARRAY( buffer );

    filepath = filestat.name;
    stat = filestat;
    size = filestat.size;
    pos = 0;

    container = path_to_zipfile;

    buffer = new char[size+1];
    buffer[size] = '\0';

    return buffer;
}

bool FileArchived::ReadToBuffer( void ) {
    return true;
}

void FileArchived::GetStat( struct zip_stat& file_stat ) const {
    file_stat = stat;
}
