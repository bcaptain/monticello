// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_ARCHIVED_H
#define SRC_CLIB_ARCHIVED_H

#include    <zip.h>
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./base/file.h"

/*!

FileArchived \n\n

A file that is archived within a zip file. \n
This class derived from class File so that we can treat it like a normal file in normal operations

**/

class FileArchived : public File {
    friend class FileZip; //!< FileZip will fill us with data

public:
    FileArchived( void );
    FileArchived( const FileArchived& other ) = delete;
    ~FileArchived( void ) override;

public:
    FileArchived& operator=( const FileArchived& other ) = delete;

public:
    bool  Open( const char* fname, const char* fmode=nullptr ) override;
    void  Close( void ) override;
    int32 FTell( void ) const override;
    int32 SeekSet( const int32 pos ) override;
    int32 SeekCur( const int32 pos ) override;
    int32 SeekEnd( const int32 pos ) override;

    void PeekChar( char& data_out ) override;
    char PeekChar( void ) override;

    void GetStat( struct zip_stat& file_stat ) const;

    void ReadChar( char* data_out, const Uint32 len ) override;
    void ReadUChar( uchar* data_out, const Uint32 len ) override;
    void WriteChar( const char* data_out, const Uint32 len ) override;

    bool IsOpen( void ) const override;
    bool EndOfFile( void ) const override;

    bool ReadToBuffer( void ) override; //!< dummy function, does nothing since things are already in our buffer. returns true;

private:
    char* FileSetup( const struct zip_stat& filestat, const std::string& path_to_zipfile );

private:
    struct zip_stat stat;
    int pos; //!< the current position in the file, or -1 if no file is opened. note that this can exceed the size of the file, so testing whether pos >= size means the we are at EoF
};

#endif  //SRC_CLIB_ARCHIVED_H
