// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_FILESYSTEM_H_
#define SRC_CLIB_FILESYSTEM_H_


#ifdef _WIN32
    #include <io.h>
#elif defined( __linux )
    #include <sys/io.h>
#else
    #error "Unknown platform"
#endif //_WIN32

#include <unistd.h>
#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "../containers/linklist.h"

class FileSystem {
public:
    static bool ListDir( const char *dir, LinkList<std::string> &files, bool with_relative_path = false );
    static bool IsDir( const char *path );
    static bool Exists( const char *path );
    static bool CreateDir( const char *path ); //!< all are writable/searchable by owner and group
    static bool Rename( const char *from, const char *to );
    static bool Delete( const char *path );
};

#endif  // SRC_CLIB_FILESYSTEM
