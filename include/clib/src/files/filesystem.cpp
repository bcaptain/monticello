// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "../warnings.h"
#include "../message.h"
#include "./filesystem.h"

bool FileSystem::Delete( const char *path ) {
    return std::remove( path ) == 0;
}

bool FileSystem::Rename( const char *from, const char *to ) {
    std::remove( to ); // remove the old file in case the rename call has a problem with it already existing
    
    int e = std::rename( from, to );
    if ( e == 0 )
        return true;
    std::string errno_str = strerror(errno);
    ERR("FileSystem::Rename: Error %i, errno : %s\n", e, errno_str.c_str());
    return false;
}

bool FileSystem::ListDir( const char *dir, LinkList<std::string> &files, bool with_relative_path ) {
    DIR *dp = opendir( dir);
    struct dirent *dirp;

    if ( dp == nullptr ) {
        ERR("Could not open dir %s\n", dir);
        return false;
    }

    std::string file;
    while ( ( dirp = readdir(dp) )  ) {
        if ( with_relative_path ) {
            file = dir;
            if ( file.size() > 0 && file[ file.size()-1 ] != '/' )
                file.append( "/" );
            file.append( dirp->d_name );
        } else {
            file = dirp->d_name;
        }
        files.Append( file );
    }

    closedir(dp);

    return true;
}

bool FileSystem::CreateDir( const char *path ) {
    if ( ! path || path[0] == '\0' )
        return false;

    const std::size_t len = String::Len( path );

    std::vector<char> slashpath(len+1);
    slashpath[len] = '\0';
    int ret;
    uint i=0;

    // skip leading slash
    if ( path[i] == '\\' || path[i] == '/' ) {
        slashpath[0]=path[0];
        ++i;
    }

    for ( ; i<len; ++i ) {
        switch ( path[i] ) {
            case '/':
            case '\\':
                slashpath[i] = '\0';
                if ( ! Exists( slashpath.data() ) ) {

                    #ifdef _WIN32
                        ret = mkdir( slashpath.data() );
                    #elif defined( __linux )
                        ret = mkdir( slashpath.data(), S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP | S_IXUSR | S_IXGRP );
                    #else
                        #error "Unknown platform"
                    #endif // _WIN32

                    if ( ret ) {
                        WARN("Return code %i creating directory: %s\n", ret, slashpath.data() );
                    }
                } else if ( ! IsDir( slashpath.data() ) ) {
                    ERR("Could not create directory, a file exists of the same name: %s\n", slashpath.data() );
                    return false;
                }
                slashpath[i] = path[i];
                break;
            default:
                slashpath[i] = path[i];
                break;
        }
    }

    // if it doesn't end with a slash, we have to make that dir, too
    if ( path[len-1] != '\\' && path[len-1] != '/' ) {
        if ( ! Exists( path ) ) {
            #ifdef _WIN32
                ret = mkdir( path );
            #elif defined( __linux )
                ret = mkdir( path, S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP | S_IXUSR | S_IXGRP );
            #else
                #error "Unknown platform"
            #endif // _WIN32

            if ( ret ) {
                WARN("Return code %i creating directory: %s\n", ret, path );
            }
        } else if ( ! IsDir( path ) ) {
            ERR("Could not create directory, a file exists of the same name: %s\n", path);
            return false;
        }
    }

    return true;
}

bool FileSystem::IsDir( const char *path ) {
    if ( path == nullptr )
        return false;

    if ( !Exists( path ) )
        return false;

    std::string tmp = path;
    if ( tmp.size() < 1 )
        return false;

    if ( tmp[tmp.size()-1] == '/' )
        tmp.resize( tmp.size()-1 );

    std::string errorMessage;
    struct stat status;
    const int ret = stat( tmp.c_str(), &status );


    switch ( ret ) {
        case 0: return ( status.st_mode & S_IFDIR ); // no error
        case EBADF: errorMessage = "fd is bad"; break;
        case EFAULT: errorMessage = "Bad address"; break;
        case ENAMETOOLONG: errorMessage = "path is too long"; break;
        case ENOENT: errorMessage = "A component of path does not exist, or path is an empty string"; break;
        case ENOMEM: errorMessage = "Out of kernel memory"; break;
        case ENOTDIR: errorMessage = "A component of the path prefix of path is not a directory"; break;
#ifndef _WIN32
        case ELOOP: errorMessage = "Too many symbolic links encountered while traversing the path"; break;
        case EOVERFLOW: errorMessage = "path or fd refers to a file whose size, inode number, or number of blocks cannot be represented in, respectively, the types off_t, ino_t, or blkcnt_t."; break; // This error can occur when, for example, an application compiled on a 32-bit platform without -D_FILE_OFFSET_BITS=64 calls stat() on a file whose size exceeds (1<<31)-1 bytes.            default: errorMessage = "Unknown error";
#endif //_WIN32
        default: errorMessage = "unknown error";
    }

    ERR("Error %i calling stat(\"%s\"), file: \"%s\" error message: \"%s\"\n", errno, tmp.c_str(), path, errorMessage.c_str() );
    return false;
}

bool FileSystem::Exists( const char *path ) {
    return ( access( path, 0 ) == 0 );
}
