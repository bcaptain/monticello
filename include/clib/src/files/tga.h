// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_TGAFILE_H_
#define SRC_CLIB_TGAFILE_H_

#include "../warnings.h"
#include "../macros.h"
#include "../types.h"
#include "../std.h"
#include "../strings.h"
#include "./parsers/binary.h"

extern const int TGA_UNCOMPRESSED;
extern const int TGA_RLE;

class ImageData;

/*!

ParserTGA \n\n

This parser is used to load a TGA file and then transfer the data to another class. It is not intended to outlive the file which it parses, nor should it.

**/

class ParserTGA : public ParserBinary {
public:
    ParserTGA( void ) = delete;
    ParserTGA( File& file_to_parse ); //!< file_to_parse is not resource managed
    ParserTGA( ParserTGA& other ) = delete;
    ~ParserTGA( void ) override { }

public:
    ParserTGA& operator=( ParserTGA& other ) = delete;

public:
    bool Load( ImageData& imageData_out );
};

#endif  // SRC_CLIB_TGAFILE
