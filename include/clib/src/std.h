// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_STD
#define SRC_CLIB_STD

#include <memory>
#include <algorithm>
#include <functional>
#include <vector>
#include <numeric>

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"
#include "./assert.h"
#include "./message.h"

template< typename TYPE1, typename TYPE2, typename CONTAINER >
std::pair< TYPE1, TYPE2 >* FindBySecond( CONTAINER& list, const TYPE2& val ) {
    for ( auto & pair : list )
        if ( pair.second == val )
            return &pair;
    return nullptr;
}

template< typename TYPE1, typename TYPE2, typename CONTAINER >
std::pair< TYPE1, TYPE2 >* FindByFirst( CONTAINER& list, const TYPE1& val ) {
    for ( auto & pair : list )
        if ( pair.first == val )
            return &pair;
    return nullptr;
}

template< typename TYPE1, typename TYPE2, typename CONTAINER >
const std::pair< TYPE1, TYPE2 >* FindBySecond_Const( const CONTAINER& list, const TYPE2& val ) {
    for ( auto const & pair : list )
        if ( pair.second == val )
            return &pair;
    return nullptr;
}

template< typename TYPE1, typename TYPE2, typename CONTAINER >
const std::pair< TYPE1, TYPE2 >* FindByFirst_Const( const CONTAINER& list, const TYPE1& val ) {
    for ( auto const & pair : list )
        if ( pair.first == val )
            return &pair;
    return nullptr;
}

//! does not preserve list order, but fast
template < typename LIST >
inline void PopSwap( LIST& list, const unsigned int pos ) {
    ASSERT( pos < list.size() );
    list[pos] = std::move( list[list.size()-1] );
    list.pop_back();
}

//! does not preserve list order, but fast
template < typename LIST >
inline void PopSwapDuplicates( LIST& list) {
    for ( uint i=0; i<list.size(); ++i )
        for ( uint k=i+1; k<list.size(); ++k )
            if ( list[i]==list[k] )
                PopSwap(list,k--);
}

/*! does not preserve list order, but fast.
Removed all elements of a specified value.
Returns how many elements were removed
*/
template < typename LIST, typename VALUE >
inline uint PopSwap_Value( LIST& list, const VALUE value ) {
    const uint prev_size = list.size();
    for ( uint i=0; i<list.size(); ++i )
        if ( list[i] == value )
            PopSwap(list,i--);
    return prev_size - list.size();
}

//! Preserves list order
template < typename LIST >
inline void ShuffleErase( LIST& list, const unsigned int pos ) {
    list.erase( std::begin(list) + static_cast< std::ptrdiff_t >(pos) );
}

template < typename LIST, typename TYPE >
void SetAt( LIST& list, const unsigned int pos, const TYPE& element ) {
    if ( pos == list.size() ) {
        list.emplace_back( element );
        return;
    }
    
    if ( pos >= list.size() ) {
        list.resize(pos+1);
    }
    
    list[pos] = element;
}

template< typename LIST, typename TYPE >
void OverridePairByKey( LIST& dest, const TYPE& source_pair ) {
    for ( auto & dest_pair : dest ) {
        if ( dest_pair.first == source_pair.first ) {
            dest_pair.second = source_pair.second;
            return;
        }
    }
        
    dest.push_back( source_pair );
}

template< typename TYPE >
void OverridePairsByKey( TYPE& dest, const TYPE& source ) {
    for ( const auto & source_pair : source ) {
        OverridePairByKey( dest, source_pair );
    }
}

template< typename TYPE >
bool DifferentSigns( const TYPE one, const TYPE two ) {
    if ( one > 0 ) {
        if ( two > 0 ) {
            return false;
        }
    } else {
        if ( two < 0 ) {
            return false;
        }
    }
    return true;
}

template< typename CONTAINER, typename TYPE >
bool CompareNext( const CONTAINER& vec, const uint start, const uint count, const TYPE value ) {
    for ( uint i=0; i<count; ++i )
        if ( vec[start+i] != value )
            return false;
    return true;
}

//! converts a reverse iterator to a forward iterator
template< typename ReverseIterator >
typename ReverseIterator::iterator_type IteratorReverseToForward( ReverseIterator ritr ) {
    return (++ritr).base();
}

//! converts a forward iterator to a reverse iterator
template< typename ForwardIterator >
std::reverse_iterator<ForwardIterator> IteratorForwardToReverse( ForwardIterator itr ) {
    return std::reverse_iterator<ForwardIterator>(++itr);
}

typedef bool ItrDirTT_BaseType;
//! type for denoting the direction of an iterator in templates
enum class ItrDirT : ItrDirTT_BaseType { FORWARD, REVERSE };

//! returns true if the sequence contains the value
template< typename TYPE >
void RemoveValue( std::vector<TYPE>& vec, const TYPE& val ) {
    vec.erase( std::remove( std::begin(vec), std::end(vec), val ), std::cend(vec) );
}

//! returns true if the sequence contains the value
template< typename ITR, typename TYPE >
bool Contains( const ITR& first, const ITR& last, const TYPE& val ) {
    return std::find( first, last, val ) != last;
}

//! returns true if the container contains the value
template< typename TYPE, typename CONTAINER >
inline bool Contains( const CONTAINER& list, const TYPE& element ) {
    return Contains( std::cbegin(list), std::cend(list), element );
}

//! returns true if the sequence contains the value
template< typename ITR, typename TYPE, typename NUM >
inline bool Contains_n( const ITR& first, const NUM& n, const TYPE& val ) {
    return Contains( first, first+n, val );
}

//! returns the index of the first occurence of the element in the list, or -1 if there is none
template< typename TYPE, typename CONTAINER >
int FindIndex( const CONTAINER& list, const TYPE& element ) {
    int i = 0;

    for ( auto const& item : list ) {
        if ( item == element )
            return i;

        ++i;
    }

    return -1;
}

//! return the average of the values in the array
template< typename TYPE >
TYPE GetAverage( const TYPE* item_array, const std::size_t item_count ) {
    return std::accumulate( item_array, item_array+item_count, TYPE() ) / item_count;
}

//! returns the average of the values in the sequence
template< template< class T, class Alloc = std::allocator<T>  > class LIST, typename TYPE >
TYPE GetAverage( const LIST< TYPE >& list ) {
    return std::accumulate( cbegin( list ), cend( list ), TYPE() ) / list.size();
}

template< typename CONTAINER >
void SortAndRemoveDuplicates( CONTAINER& container ) {
    std::sort( std::begin( container ), std::end( container ) );
    container.erase( std::unique( std::begin( container ), std::end( container ) ), std::end( container ) );
}

template< typename CONTAINER >
inline void Sort( CONTAINER& container ) {
    std::sort( std::begin( container ), std::end( container ) );
}

//! returns true if wptr references the object at obj's address
template< typename TYPE >
bool ContainsObj_helper( const std::weak_ptr< TYPE >& wptr, const TYPE& obj ) {
    if ( const auto & sptr = wptr.lock() )
        return sptr.get() == &obj;
    return false;
}

//! returns true if any in weak_ptr in the sequence references the object at obj's address
template< typename ITR, typename TYPE >
bool ContainsObj( const ITR& first, const ITR& last, const TYPE& obj ) {
    return (
        std::find_if(
            first,
            last,
            std::bind( ContainsObj_helper<TYPE>, std::placeholders::_1, std::ref( obj ) )
        ) != last
    );
}

//! returns true if any weak_ptr in the container references the object at obj's address
template< typename TYPE, typename CONTAINER >
inline bool ContainsObj( const CONTAINER& list, const TYPE& obj ) {
    return ContainsObj( std::cbegin(list), std::cend(list), obj );
}

//! compares whether the weak pointer points to the same resource as the shared pointer
template< typename TYPE >
bool CompareWeakPtr( const std::shared_ptr< TYPE >& sptr, const std::weak_ptr< TYPE >& wptr ) {
    auto const wptr_locked = wptr.lock();
    return wptr_locked && wptr_locked == sptr;
}

//! append to a weakptr list if the weakptr isn't already in it
template< typename LIST, typename TYPE >
void AppendUniqueWptr( LIST& list, const std::shared_ptr<TYPE>& element ) {
    auto check = std::find_if(
        std::cbegin(list),
        std::cend(list),
        std::bind(CompareWeakPtr<TYPE>, element, std::placeholders::_1)
    );

    if ( check == std::cend(list) )
        list.Append( element );
}

//! append to a list if it isn't already in it
template< typename LIST, typename TYPE >
bool AppendUnique( LIST& list, const TYPE& element ) {
    auto check = std::find( std::cbegin(list), std::cend(list), element );
    
    if ( check == std::cend(list) ) {
        list.push_back( element );
        return true;
    }
    
    return false;
}

//! append to a weakptr list if the weakptr isn't already in it
template< typename LIST >
void MergeUnique( LIST& dest, const LIST& source ) {
    for ( auto const & element : source )
        AppendUnique( dest, element );
}

//! append to a weakptr list if the weakptr isn't already in it
template< typename LIST, typename TYPE >
void PushBackUniqueWptr( LIST& list, const std::shared_ptr<TYPE>& element ) {
    auto check = std::find_if(
        std::cbegin(list),
        std::cend(list),
        std::bind(CompareWeakPtr<TYPE>, element, std::placeholders::_1)
    );

    if ( check == std::cend(list) )
        list.push_back( element );
}

template< typename LIST, typename VAL >
void RemoveWeakPtr( LIST& list, const VAL& val ) {
    auto link = list.GetFirst();
    while ( link ) {
        auto sptr = link->Data().lock();
        if ( !sptr ) {
            link = link->GetNext();
            continue;
        }

        if ( sptr == val ) {
            link = link->Del();
        } else {
            link = link->GetNext();
        }
    }
}

template< typename TYPE, typename VAL >
void RemoveWeakPtr( std::vector< std::weak_ptr< TYPE > >& list, const VAL& val ) {
    //list.erase( std::remove( std::begin(list), std::end(list), val ), std::end(list) );
    list.erase(
        std::remove_if(
            begin( list ),
            end( list ),
            std::bind(&CompareWeakPtr<TYPE>, val, std::placeholders::_1 ) )
        , end(list)
    );
}

template< typename LIST, typename VAL >
bool ContainsWeakPtr( const LIST& list, const VAL& val ) {
    for ( const auto & item : list )
        if ( CompareWeakPtr( val, item ) )
            return true;

    return false;
}

template< typename TYPE, typename VAL >
bool ContainsWeakPtr( const std::vector< std::weak_ptr< TYPE > >& list, const VAL& val ) {
    for ( uint i=0; i<list.size(); ++i )
        if ( CompareWeakPtr<TYPE>( val, list[i] ) )
            return true;

    return false;
}

template< typename TYPE >
void EraseIndexBySwap( std::vector< TYPE >& list, const std::size_t index ) {
    const uint iEnd = list.size()-1;

    if ( index > iEnd ) {
        return;
    } else if ( index < iEnd ) {
        std::swap( list[index], list[iEnd] );
    } else {
        // no need to swap, just reduce the size of the list by 1
    }

    list.resize( iEnd );
}

template< typename TYPE >
void EraseIndexByShuffle( std::vector< TYPE >& list, const std::size_t index ) {
    const uint iEnd = list.size()-1;

    for ( std::size_t i = index; i<iEnd-1; ++i ) {
        list[i] = list[i+1];
    }

    list.resize( iEnd );
}

// Reverse the direction of a custom iterator
template< typename ITERATOR, typename TYPE >
struct ReverseIteratorBase : public ITERATOR {
public:
    ReverseIteratorBase( TYPE *it ) : ITERATOR( it ) { }
    ReverseIteratorBase operator++( int i ) { return ITERATOR::operator--(i); }
    ReverseIteratorBase operator--( int i ) { return ITERATOR::operator++(i); }
    ReverseIteratorBase& operator++( void ) { ITERATOR::operator--(); return *this; }
    ReverseIteratorBase& operator--( void ) { ITERATOR::operator++(); return *this; }
};

#endif  // SRC_CLIB_STD
