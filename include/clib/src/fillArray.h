// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./warnings.h"
#ifndef SRC_CLIB_FILLARRAY_H_
#define SRC_CLIB_FILLARRAY_H_

#include "./global.h"
#include "./types.h"
#include "./math/vec3t-dec.h"

template< typename TYPE, typename S >
void FillArray( TYPE* var, std::initializer_list<S> list ) {
    std::size_t i = 0;
    for ( auto& item : list ) {
        var[i] = item;
        ++i;
    }
}

template< typename TYPE, typename ...ARGS >
inline void FillArray( TYPE* var, ARGS&&... args ) {
    FillArray( var, { std::forward<ARGS>(args)... } );
}

#endif // SRC_CLIB_FILLARRAY_H_
