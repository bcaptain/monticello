// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MESSAGE_H_
#define SRC_CLIB_MESSAGE_H_

#ifdef _WIN32
    #include <windows.h>
#endif
#include <iostream>
#include <vector>
#include "./warnings.h"
#include "./macros.h"

namespace String {
    template <class ...Args>
    std::vector<std::string> ToStringVector(const Args&... args);
}

extern void (*error_hook)( const char* str ); //!< Set this to a function to be called when ERR is called
extern void (*warning_hook)( const char* str ); //!< Set this to a function to be called when WARN is called
extern void (*message_hook)( const char* str ); //!< Set this to a function to be called when MSG is called
extern void (*fatalError_hook)( const char* str ); //!< Set this to a function to be called when DIE is called - if set, caller is responsible for aborting program.

void ClearLog( void );

std::string Printfv( const std::string& fmt, const std::vector< std::string >& vec );

template <class ...Args>
void Printf( const std::string& fmt, const Args&... args );

template <class ...Args>
void LOG( const std::string& fmt, const Args&... args );

template <class ...Args>
void ERR( const std::string& _fmt, const Args&... args ); //!< Console Error ( Logs )

template <class ...Args>
void WARN( const std::string& _fmt, const Args&... args ); //!< Console Warning ( Logs )

template <class ...Args>
void CMSG( const std::string& fmt, const Args&... args ); //!< Console Message ( Does not log )

template <class ...Args>
void CMSG_LOG( const std::string& fmt, const Args&... args ); //!< Console Message and Log it

template <class ...Args>
void DIE( const std::string& _fmt, const Args&... args ); //!< Error ( Logs ) And exit program

template <class ...Args>
NORETURN void EXIT( const std::string& _fmt, const Args&... args );//!< Graceful exit(1)

#ifdef _WIN32
    template <class ...Args>
    void WINDOWS_MSGBOX( const std::string& fmt, const Args&... args );
#endif // _WIN32

template <class ...Args>
std::string Getf( const std::string& fmt, const Args&... args );

template < class ...Args>
void Printf( FILE* dest, const std::string& fmt, const Args&... args );

template <class ...Args>
void Printf( const std::string& fmt, const Args&... args ) {
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wformat-security"
        std::cout << Getf( fmt, args... );
    #pragma GCC diagnostic pop
}

template <class ...Args>
std::string Getf( const std::string& fmt, const Args&... args ) {
    const auto vec = String::ToStringVector( args...);
    return Printfv( fmt, vec );
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"
    template < class ...Args>
    void Printf( FILE* dest, const std::string& fmt, const Args&... args ) {
        fprintf( dest, Getf( fmt, args... ).c_str() );
    }
#pragma GCC diagnostic pop

template <class ...Args>
void LOG( const std::string& fmt, const Args&... args ) {
    auto file = fopen("log.txt", "a");

    if ( ! file ) {
        fprintf( stderr, "Error: Couldn't open log for write\n" );
        return;
    }

    const auto msg = Getf( fmt, args... );
    fwrite(msg.c_str(), 1, msg.size(), file );
    fclose( file );
}

template <class ...Args>
void ERR( const std::string& _fmt, const Args&... args ) {
    const std::string fmt = "Error: " + _fmt;
    const std::string msg = Getf( fmt, args... );
    LOG( msg.c_str() );
    Printf( stderr, msg.c_str() );

    if ( error_hook )
        error_hook( msg.c_str() );
}

template <class ...Args>
void WARN( const std::string& _fmt, const Args&... args ) {
    const std::string fmt = "Warning: " + _fmt;
    const std::string msg = Getf( fmt, args... );
    LOG( msg.c_str() );
    Printf( stderr, msg.c_str() );

    if ( warning_hook )
        warning_hook( msg.c_str() );
}

template <class ...Args>
void CMSG( const std::string& fmt, const Args&... args ) {
    const std::string msg = Getf( fmt, args... );
    Printf( stderr, msg.c_str() );

    if ( message_hook )
        message_hook( msg.c_str() );
}

template <class ...Args>
void CMSG_LOG( const std::string& fmt, const Args&... args ) {
    const std::string msg = Getf( fmt, args... );
    LOG( msg.c_str() );
    Printf( stderr, msg.c_str() );

    if ( message_hook )
        message_hook( msg.c_str() );
}

template <class ...Args>
void DIE( const std::string& _fmt, const Args&... args ) {
    const std::string fmt = "Fatal Error: " + _fmt;
    const std::string msg = Getf( fmt, args... );
    LOG( msg.c_str() );
    Printf( stderr, msg.c_str() );

    #ifdef _WIN32
        MessageBox(nullptr,msg.c_str(),"Fatal Error", MB_OK | MB_ICONERROR);
    #endif //_WIN32
    
    if ( fatalError_hook ) {
        fatalError_hook( msg.c_str() );
    } else {
        #ifdef CLIB_DEBUG
            assert(false);
            abort();
        #else // CLIB_DEBUG
            exit(-1);
        #endif // CLIB_DEBUG
    }
}

template <class ...Args>
void EXIT( const std::string& _fmt, const Args&... args ) {
    const std::string fmt = "Exiting: " + _fmt;
    const std::string msg = Getf( fmt, args... );
    LOG( msg.c_str() );
    Printf( stderr, msg.c_str() );

    #ifdef _WIN32
        MessageBox(nullptr,msg.c_str(),"Quitting", MB_OK | MB_ICONERROR);
    #endif //_WIN32

    exit(1);
}

#ifdef _WIN32
    template <class ...Args>
    void WINDOWS_MSGBOX( const std::string& fmt, const Args&... args ) {
        const std::string msg = Getf( fmt, args... );
        MessageBox(nullptr,msg.c_str(),"Message", MB_OK | MB_ICONERROR);
    }
#endif // _WIN32

#endif //SRC_CLIB_MESSAGE_H_
