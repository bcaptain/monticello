// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_BITWISE_H_
#define SRC_CLIB_BITWISE_H_

#include "./warnings.h"
#include "./macros.h"
#include "./std.h"
#include "./strings.h"
#include "./types.h"

#define TOGGLE_BIT( val, bit )                  ( (val) ^= (bit) )
#define INVERT_ALL_BITS( val )                  ( (val) = ~(val) )
#define SET_BIT( val, bit )                     ( (val) |= (1 << (bit) ) )
#define SET_BITS_LOWER_THAN( val, bit )         ( (val) |= ( (1 << (bit) )-1 ) )
#define SET_BITS_HIGHER_OR_EQUAL( val, bit )    ( (val) |= ~( (1 << (bit) )-1 ) )
#define GET_BIT( val, bit )                     ( (val) & (1 << (bit) ) )
#define GET_BITS_LOWER_THAN( val, bit )         ( (val) & ( (1 << (bit) )-1) )
#define GET_BITS_HIGHER_OR_EQUAL( val, bit )    ( (val) & ~( (1 << (bit) )-1) )
#define RETAIN_ONLY_BIT( val, bit )             ( (val) &= (1 << (bit) ) )
#define CLEAR_BIT( val, bit )                   ( (val) &= ~(1 << (bit) ) )
#define RETAIN_BITS_LOWER_THAN                  ( (val) &= ( (1 << (bit) )-1) )
#define RETAIN_ONLY_BITS_HIGHER_OR_EQUAL        ( (val) &= ~( (1 << (bit) )-1) )


template< typename TYPE >
std::string ToBitString( const TYPE& data ) {
    std::string bits;

    const unsigned char *chardata = reinterpret_cast< const unsigned char *>(&data);

    std::size_t i = sizeof(data);
    std::size_t bit;

    while ( i>0 ) {
        --i;
        for ( bit=8; bit > 0; ) {
            --bit;
            bits += chardata[i] & (1<<bit) ? '1' : '0';
        }
    }

    return bits;
}

template std::string ToBitString< const int& >( const int& data );
template std::string ToBitString< const float& >( const float& data );
template std::string ToBitString< const char& >( const char& data );
template std::string ToBitString< const uint& >( const uint& data );
template std::string ToBitString< const uchar& >( const uchar& data );

#endif  // SRC_CLIB_BITWISE
