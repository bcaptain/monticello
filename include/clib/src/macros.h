// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_MACROS_H_
#define SRC_CLIB_MACROS_H_

#include <string>
#include <cassert>
#include <cmath>
#include "./warnings.h"

//todo: (without defining in std headers) #define assert() static_assert(false,"assert is a banned macro, use ASSERT instead.");
#define printf() static_assert(false,"printf is a banned macro, use Printf instead.");
#define snprintf() static_assert(false,"snprintf is a banned macro, use Getf instead.");
#define vsnprintf() static_assert(false,"vsnprintf is a banned macro, use Getf instead.");

template< typename TYPE >
inline TYPE Min3( const TYPE& A, const TYPE& B, const TYPE& C ) {
    return std::min( std::min( A, B ), C );
}

template< typename TYPE >
inline TYPE Max3( const TYPE& A, const TYPE& B, const TYPE& C ) {
    return std::max( std::max( A, B ), C );
}

template< typename TYPE >
inline TYPE Min4( const TYPE& A, const TYPE& B, const TYPE& C, const TYPE& D ) {
    return std::min( std::min( std::min( A, B ), C ), D );
}

template< typename TYPE >
inline TYPE Max4( const TYPE& A, const TYPE& B, const TYPE& C, const TYPE& D ) {
    return std::max( std::max( std::max( A, B ), C ), D );
}

template< typename TYPE >
inline TYPE Min8( const TYPE& A, const TYPE& B, const TYPE& C, const TYPE& D, const TYPE& E, const TYPE& F, const TYPE& G, const TYPE& H ) {
    return Min3( Min3( Min4( A, B, C, D ), E, F), G, H );
}

template< typename TYPE >
inline TYPE Max8( const TYPE& A, const TYPE& B, const TYPE& C, const TYPE& D, const TYPE& E, const TYPE& F, const TYPE& G, const TYPE& H ) {
    return Max3( Max3( Max4( A, B, C, D ), E, F), G, H );
}

inline float Min3f( const float A, const float B, const float C ) {
    return std::fminf( std::fminf( A, B ), C );
}

inline float Max3f( const float A, const float B, const float C ) {
    return std::fmaxf( std::fmaxf( A, B ), C );
}

inline float Min4f( const float A, const float B, const float C, const float D ) {
    return std::fminf( std::fminf( std::fminf( A, B ), C ), D );
}

inline float Max4f( const float A, const float B, const float C, const float D ) {
    return std::fmaxf( std::fmaxf( std::fmaxf( A, B ), C ), D );
}

inline float Min8f( const float A, const float B, const float C, const float D, const float E, const float F, const float G, const float H ) {
    return Min3f( Min3f( Min4f( A, B, C, D ), E, F), G, H );
}

inline float Max8f( const float A, const float B, const float C, const float D, const float E, const float F, const float G, const float H ) {
    return Max3f( Max3f( Max4f( A, B, C, D ), E, F), G, H );
}

#ifdef __clang__
    #define NODISCARD
#else
    #define NODISCARD [[nodiscard]]
#endif // MONTICELLO_COMPILER_VCE2013a

#define DISABLE_COPY( classname ) \
    (classname)( const classname & ) = delete; \
    void operator=( const classname & ) = delete;

#define DISABLE_INSTANTIATION( classname ) \
    (classname)( void ) = delete; \
    (classname)( const classname & ) = delete; \
    (~classname)( void ) = delete; \
    void operator=( const classname & ) = delete;

#define CLONE_METHOD_VIRTUAL( classname ) \
    virtual classname * Clone( void ) const { return new (classname)(*this); } \

#define CLONE_METHOD_OVERRIDE( classname ) \
    classname * Clone( void ) const override { return new (classname)(*this); } \

#define DISABLE_INSTANTIATION_FOR_CRTP( classname ) \
    (classname)( void ) = delete; \
    (classname)( const classname & ) = delete; \
    virtual (~classname)( void ) = delete; \
    void operator=( const classname & ) = delete;

#ifdef __clang__
    #ifdef __i386__
        #define TARGET_ARCH "CLANG_32"
        #define ARCH_32_BIT 1
    #elif defined( __amd64__ )
        #define TARGET_ARCH "CLANG_64"
        #define ARCH_64_BIT 1
    #else
        #define TARGET_ARCH "Unknown"
    #endif // __i386__
#elif defined ( MONTICELLO_COMPILER_GNU )
    #ifdef __i386__
        #define TARGET_ARCH "GNU_32"
        #define ARCH_32_BIT 1
    #elif defined( __amd64__ )
        #define TARGET_ARCH "GNU_64"
        #define ARCH_64_BIT 1
    #else
        #define TARGET_ARCH "Unknown"
    #endif // __i386__
#elif defined( MONTICELLO_COMPILER_VCE2013 )
    #ifdef _M_IX86
        #define TARGET_ARCH "MS_32"
        #define ARCH_32_BIT 1
    #elif defined( _M_X64 )
        #define TARGET_ARCH "MS_64"
        #define ARCH_64_BIT 1
    #else
        #define TARGET_ARCH "Unknown"
    #endif // _M_IX86
#else
    #define TARGET_ARCH "Unknown"
#endif // for TARGET_ARCH

#ifdef __clang__
    #define FALLTHROUGH [[clang::fallthrough]]
#else
    #define FALLTHROUGH [[fallthrough]]
#endif // MONTICELLO_COMPILER_VCE2013

#ifdef OPTIMIZE_LEVEL
    #error "macro OPTIMIZE_LEVEL was already defined"
#endif //OPTIMIZE_LEVEL
#define OPTIMIZE_LEVEL( _LEVEL ) __attribute__((optimize( STRING_LITERAL(O##_LEVEL) )))
#define STRING_LITERAL( x ) (#x)

#ifdef MONTICELLO_COMPILER_VCE2013
    #define NORETURN __declspec(noreturn)
#else
    #define NORETURN __attribute__((noreturn))
#endif // MONTICELLO_COMPILER_VCE2013

#ifdef DELNULL
    #error "macro DELNULL was already defined"
#endif //DELNULL

#ifdef DELNULLARRAY
    #error "macro DELNULLARRAY was already defined"
#endif //DELNULLARRAY

#define DELNULL(obj) { delete (obj); (obj)=nullptr; }
#define DELNULLARRAY(obj) { delete [] (obj); (obj)=nullptr; }

#ifdef CALL_METHOD
    #error "macro CALL_METHOD was already defined"
#endif //CALL_METHOD

#define CALL_METHOD(object,ptrToMethod)  ((object).*(ptrToMethod))

#define SETTER_GETTER( NAME, TYPE, MEMBER ) \
private: \
    TYPE MEMBER; \
public: \
    void Set##NAME( const TYPE& val ) { MEMBER = val; } \
    TYPE Get##NAME( void ) const { return MEMBER; }

#define SETTER_GETTER_BYVAL( NAME, TYPE, MEMBER ) \
private: \
    TYPE MEMBER; \
public: \
    void Set##NAME( const TYPE val ) { MEMBER = val; } \
    TYPE Get##NAME( void ) const { return MEMBER; }

#endif // SRC_CLIB_MACROS_H_
