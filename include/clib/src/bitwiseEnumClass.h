// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#ifndef SRC_CLIB_BITWISE_ENUM_CLASS_H_
#define SRC_CLIB_BITWISE_ENUM_CLASS_H_

#include "./warnings.h"
#include "./macros.h"
#include "./types.h"
#include "./std.h"
#include "./strings.h"

#define BITWISE_OPERATORS_FOR_ENUM_CLASS( TYPE, BASETYPE ) \
    inline constexpr TYPE operator&( TYPE x, TYPE y ) { \
        return static_cast<TYPE>(static_cast<BASETYPE>(x) & static_cast<BASETYPE>(y ) ); \
    } \
    inline constexpr TYPE operator|( TYPE x, TYPE y ) { \
        return static_cast<TYPE>(static_cast<BASETYPE>(x) | static_cast<BASETYPE>(y ) ); \
    } \
    inline constexpr TYPE operator^( TYPE x, TYPE y ) { \
        return static_cast<TYPE>(static_cast<BASETYPE>(x) ^ static_cast<BASETYPE>(y ) ); \
    } \
    inline constexpr TYPE operator~( TYPE x ) { \
        return static_cast<TYPE>(~static_cast<BASETYPE>(x) ); \
    } \
    inline TYPE& operator&=( TYPE & x, TYPE y ) { \
        return x = x & y; \
    } \
    inline TYPE& operator|=( TYPE & x, TYPE y ) { \
        return x = x | y; \
    } \
    inline TYPE& operator^=( TYPE & x, TYPE y ) { \
        return x = x ^ y; \
    } \
    inline constexpr bool BitwiseAnd( TYPE x, TYPE y ) { \
        return static_cast<URegister>( x & y ) != 0; \
    } \
    inline constexpr TYPE BitwiseOr( TYPE x, TYPE y ) { \
        return static_cast<TYPE>( x | y ); \
    } \
    inline constexpr TYPE BitwiseRemove( TYPE x, TYPE y ) { \
        return static_cast<TYPE>( x & ~y ); \
    } \
    inline constexpr TYPE OperatorXor( TYPE x, TYPE y ) { \
        return static_cast<TYPE>( x ^ y ); \
    }

#endif  // SRC_CLIB_BITWISE_ENUM_CLASS
