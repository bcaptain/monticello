// Copyright 2010-2019 Brandon Captain. You may not copy this work.

#include "./warnings.h"
#include "./clib.h"

// **
//
// Portability Checking
// not exactly a unit test, but this is a good place to put it
//
// **

#ifdef CLIB_DEBUG // stuck this in a CPP since we only need to check this one time per system.
    static_assert( sizeof( int8 ) == 1, "Portability Problem" );
    static_assert( sizeof( Uint8 ) == 1, "Portability Problem" );

    static_assert( sizeof( short int ) == 2, "Portability Problem" );
    static_assert( sizeof( int16 ) == 2, "Portability Problem" );
    static_assert( sizeof( Uint16 ) == 2, "Portability Problem" );

    static_assert( sizeof( int ) == 4, "Portability Problem" );
    static_assert( sizeof( long int ) == 4, "Portability Problem" );
    static_assert( sizeof( int ) == 4, "Portability Problem" );
    static_assert( sizeof( uint ) == 4, "Portability Problem" );

    static_assert( sizeof( long long int ) == 8, "Portability Problem" );

    static_assert( sizeof( float ) == 4, "Portability Problem" );

    static_assert( sizeof( double ) == 8, "Portability Problem" );

    static_assert( sizeof( long double ) == 12, "Portability Problem" );

    static_assert( sizeof( std::size_t ) == 4, "Portability Problem" );

    static_assert( std::is_same<std::size_t, unsigned int>::value, "size_t is not uint!" );
#endif // CLIB_DEBUG

#ifdef CLIB_UNIT_TEST

void CLibUnitTest( void ) {
    Vec3f::UnitTest();
    Maths::UnitTest();
    Hashes::UnitTest();
    Mat3f::UnitTest();
    Mat4f::UnitTest();
    Quat::UnitTest();
    Dualquat::UnitTest();
    UnitTest_LinkList();
    UnitTest_HashList();
    Maths::UnitTest_Math();
}

#endif // CLIB_UNIT_TEST
