#!/bin/bash
vera++ $* 3>&1 2>&1 |
	grep -v "closing curly bracket" |
	grep -v whitespace |
	grep -v trailing |
	grep -v "expected in the control structure" |
	grep -v "line is longer" |
	grep -v "short form" |
	grep -v "consecutive empty lines" |
	grep -v "single space" |
	grep -v "semicolon is isolated from other tokens"


