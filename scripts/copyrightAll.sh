
#!/bin/bash
# intelligently prepend copyright to all files in the project
copyright='\/\/ Copyright 2010-2018 Brandon Captain.'

function prepend {
	head -n 1 "$*" | grep -i Copyright 1>/dev/null 2>/dev/null || {
	sed -i "1i $copyright" "$*"
	}
}

function findin {
	for file in `find "$@" -iname '*.cpp' -or -iname '*.h'` ; do
		prepend "$file"
	done
}

findin *.h *.cpp "$@"

