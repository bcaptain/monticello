// This program will create a compile_command.json database in the current directory of each file it is called with.
// This does not have to be run from a specific directory.
// You can also pass in files via stdin, as well as on the commandline
// usage: cat files.txt | program
// usage: program myFile.cpp another.cpp etc

#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <climits>
#include <iostream>

#include "../../include/clib/src/macros.h"
#include "../../include/clib/src/files/filesystem.h"
#include "../../include/clib/src/files/base/file.h"
#include "../../include/clib/src/strings.h"

void SpitEntry( const std::vector< const char* >& builds, std::string& dir, const std::string& file ) {
	std::string commandsFile(dir);
	Printf("generating entry for %s\n", file);
	commandsFile += "/compile_commands.json";

	const bool existed = File::GetExists(commandsFile.c_str() );
	std::string entry;
	if ( existed ) {
		entry+=',';
	} else {
		entry+="[\n";
	}

	for ( uint i=0; i<builds.size(); ++i ) {
		if ( i>0 )
			entry+=',';

		entry+="{\n";
		entry+="    \"directory\": \"";
			entry+=dir;
			entry+="\"\n";
		entry+="    \"command\": \"";
			entry += builds[i];
			entry += " ";
			entry += file;
			entry += "\"\n";
		entry+="    \"file\": \"";
			entry+=dir;
			entry+="/";
			entry+=file;
			entry+="\"\n";
		entry+="}\n";
	}

	entry+=']';

	FILE* outfile = fopen(commandsFile.c_str(), "a");
	fclose( outfile );
	outfile = fopen(commandsFile.c_str(), "r+");
	if ( ! outfile ) {
		Printf("could not open file %s\n", commandsFile);
		return;
	}
	if ( existed ) {
		auto x = fseek( outfile, -1, SEEK_END );
		assert( !x );
	}
	Printf("writing file %s\n", commandsFile.c_str());
	fwrite( entry.c_str(), 1, entry.size(), outfile );
	fclose( outfile );
}

void Go( const int argc, const char **argv ) {

    // for monticello
    /*
	const std::vector< const char* > builds {
		"g++ -pg -O3 -ansi -pedantic -fno-exceptions -finline-functions -std=c++17 -DNDEBUG=1 -DMONTICELLO_EDITOR=1 -D__linux=1 -DNDEBUG=1 -DMONTICELLO_COMPILER_GNU=1 -I. -I/home/antic/Documents/proj/monticello/include/lin -I/home/antic/Documents/proj/monticello/include/ -I/usr/include/i386-linux-gnu/"
	};
    /**/
    // for clib
    /**/
	const std::vector< const char* > builds {    
        "g++  -m32 -fno-exceptions -pg -g -O3 -ansi -pedantic -Wall -ggdb -std=c++17  -DNDEBUG=1 -DMONTICELLO_EDITOR=1 -D__linux=1 -DMONTICELLO_COMPILER_GNU=1 -I/home/antic/Documents/proj/monticello/include/clib/include/lin"
    };
	/**/
    
	for ( unsigned int i=0; i<argc; ++i ) {
		char actualpath [PATH_MAX+1];
		const char *ptr = realpath(argv[i], actualpath);
		std::string path = ptr ? (ptr) : "./";
		path = String::GetDirFromPath( path );

//path = String::Replace( path, "/src/", "/codelite/../src/" );

		Printf("Generating: %s.\n", path.c_str());
		SpitEntry( builds, path, argv[argc-1] );
	}
}

int main ( const int argc, const char *argv[] ) {
	if ( argc > 1 ) {
		Go( argc-1, argv+1 );
	} else {
		for (std::string line; std::getline(std::cin, line);  ) {
			const char* item = line.c_str();
			Go( 1, &item );
		}
	}


	return 0;
}

