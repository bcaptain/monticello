#!/usr/bin/perl -w -0777
# this is a script to add a copy of a function to a CPP file based on one that already exists in it.
# this version of the script is crafted for a paritcular method, change/copy it to suit your needs

$num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "\nUsage: replace.pl filename.cpp\n";
    exit;
}

local $/ = undef;

# load the whole file
$path=$ARGV[0];
open INFILE, $path or die "Could not open file. $!";
$file =  <INFILE>;
close INFILE;

$FUNCTION_TO_REPLACE='.*(bool *[a-zA-Z]+[:][:]Set[(] const NamedSetterT_BaseType setter[^}]*}[^}]*}).*';

# create the replacement function
$replacement_func="$file"; # start with the whole file
$replacement_func =~ s/\n/__NEW_LINE_REPLACED__/g; # replace newlines with something else, so we can treat the whole file as a single line
$replacement_func =~ s/$FUNCTION_TO_REPLACE/$1/g; # remove everything but the function we're replacing
$replacement_func =~ s/__NEW_LINE_REPLACED__/\n/g; # replace it back with normal newline

# line-by-line go through and hack up a new function
$replacement_func =~ s/bool ([a-zA-Z0-9]+)::Set\(/std::string $1::Get(/g;
$replacement_func =~ s/, const std::string& value//g;
$replacement_func =~ s/(case [a-zA-Z_0-9]+:) Set/$1 return Get/g;
$replacement_func =~ s/\( *value *\); *return +true/()/g;
$replacement_func =~ s/return (Get[a-zA-Z_0-9]+)\( *String::To[^)]+\)[^)]+\); *return *true;/return String::ToString($1());/g;
$replacement_func =~ s/::Set\( *setter, *value *\)/::Get( setter )/g;

# add the new function the the whole file contents
$file =~ s/$FUNCTION_TO_REPLACE/$1\n$replacement_func/g;

# save it to file
open(my $fout, '>', $path) or die "Could not open file '$path' $!";
print $fout "$file";
close $fout;




