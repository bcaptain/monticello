#!/usr/bin/perl -w -0777
# this is a script to add a copy of a function to a CPP file based on one that already exists in it.
# this version of the script is crafted for a paritcular method, change/copy it to suit your needs

$num_args = $#ARGV + 1;
if ($num_args != 1) {
    print "\nUsage: replace.pl filename.cpp\n";
    exit;
}

local $/ = undef;

# load the whole file
$path=$ARGV[0];
open INFILE, $path or die "Could not open file. $!";
$file =  <INFILE>;
close INFILE;

# create the replacement line
$file =~ s/\n/__NEW_LINE_REPLACED__/g; # replace newlines with something else, so we can treat the whole file as a single line
$LINE_TO_PULL='(void *)([a-zA-Z_]+)(::MapLoad[^{]*{\s*)([a-zA-Z_]+)(\s*)([a-zA-Z_]+)([:][:]MapLoad[^;]*;\s*__NEW_LINE_REPLACED__)';
# base = $6
# derived = $2
$file =~ s/$LINE_TO_PULL/$1$2$3$4$5$6$7    static_assert( is_parent_of<$2>( get_typeid<$6>() ) );__NEW_LINE_REPLACED__/g; # remove everything but the line we're relacing
$file =~ s/__NEW_LINE_REPLACED__/\n/g; # replace it back with normal newline

# save it to file
open(my $fout, '>', $path) or die "Could not open file '$path' $!";
print $fout "$file";
close $fout;




