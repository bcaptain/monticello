
#!/bin/bash
# intelligently prepend copyright to all files in the project
copyright='\/\/ Copyright 2010-2018 Brandon Captain.'

find ../include/clib/src/ -iname *.cpp -exec sed -i 's/^[/ ]*Copyright [0-9]*.*/\/\/ Copyright 2010-2019 Brandon Captain. You may not copy this work./g' "{}" \;
find ../src/ -iname *.cpp -exec sed -i 's/^[/ ]*Copyright [0-9]*.*/\/\/ Copyright 2010-2019 Brandon Captain. You may not copy this work./g' "{}" \;

