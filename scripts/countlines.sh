#!/bin/bash

function getNumbers {
	perl -nle '$sum += $_ } END { print $sum' numbers
}

function countRootLines {
	find ./ -maxdepth 1 -iname '*.cpp' -or -iname '*.h' > count.tmp
}

function countFoldersLines {
	find `cat dirs` -iname '*.cpp' -or -iname '*.h' >> count.tmp
}

function countClibLines {
	find clib -iname '*.cpp' -or -iname '*.h' > count.tmp
}

function countLines {
	for file in `cat count.tmp` ; do
			egrep \^ "$file" | # get all lines
			egrep -v '^[ \t]*$' | # exclude blank lines
			egrep -v '^[ \t]*\/\/.*$' | # exclude comment-only lines
			egrep -v '^[ \t]*\/\*.*$' | # exclude comment-only lines
			egrep -v '^[ \t]*{[ \t]*$' | # exclude open-curly-brace-only lines ( they dont really count!)
			egrep \$ -c
	done > numbers	
}	

countRootLines
countFoldersLines
countLines
getNumbers

countClibLines
countLines
getNumbers

rm count.tmp
