cd ..
files=`find src -iname '*.cpp' -or -iname '*.h'`
cpplint.py --filter=-readability/multiline_comment,-whitespace,-whitespace/parens,-runtime/references,-whitespace/labels,-whitespace/line_length $files

