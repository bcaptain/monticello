#!/bin/bash
filename="doxyclean.sh"

if [ -f "scripts/$filename" ] ; then
	echo "running from project directory"
	rm -rf ./doxy/gen
	rm -rf ./doxy_all/gen
	rm -rf ./include/clib/doxy/gen

elif [ -f "$filename" ] ; then 
	echo "running from scripts directory"
	rm -rf ../doxy/gen
	rm -rf ../doxy_all/gen
	rm -rf ../include/clib/doxy/gen

else
	echo "could not determine directory"
fi
