#!/bin/bash

# Scans for CMSG/ERR/WARN/LOG messages that arent \n terminated

echo " **** FINDING MALFORMED MESSAGES (no  \n at end):"
grep -nER '(DIE|WARN|ERR|LOG|MSG)\(\s*"[^\"]*"' .


