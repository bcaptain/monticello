#!/bin/bash

if (( $# < 2 )) ; then
	echo "usage: ./findSubClassesOf.sh Entity $(find ./ -iname '*.h')"
	exit 0	
fi

classname="$1"
shift

declare -a classes
declare -a sub_classes

function GrepClassLinesInheritingFrom {
	egrep 'class\s+[^ ]+\s*:\s*(public|private)\s*'"$1"'\s*{'
}

function PluckClassNameFromDecl {
	sed -r 's/class\s+([^ ]+)\s.*/\1/g'
}

function FindClasses {
	# echo "Finding classes which depend on $classname"
	filename="$1"
	sub_classes+=(`cat "$filename" | GrepClassLinesInheritingFrom $classname | PluckClassNameFromDecl`)
}

#echo "These classes derive from $classname:"

#for (( i=0; i<$#; i++ ))
#do
#	echo "${ $(expr $i) }"
#done

for file in "$@" ; do
	FindClasses $file
done

while [ ${#sub_classes[@]} -gt 0 ] ; do
	# move the last element of sub_classes to classes, and then recurse
	lastIndex="$(expr ${#sub_classes[@]} - 1)"
	classname="${sub_classes[ $lastIndex ]}"

	classes[ ${#classes[@]} ]="$classname" # assign
	unset "sub_classes[ $lastIndex ]" # erase

	for file in "$@" ; do
		FindClasses $file
	done
done

#FindClasses

for (( i=0; i<${#classes[@]}; i++ )) ; do
	echo "${classes[$i]}"
done