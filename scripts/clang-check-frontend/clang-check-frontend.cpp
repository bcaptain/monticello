// this program is designed to be run as a compiler for codelite. It will throw out any garbage paramters and pass the rest to clang-test
// you will need to turn off most compiler options in your project, including those in the global section.

#include <vector>
#include <string>
#include <cstdio>

#include "../../include/clib/src/strings.h"

int main ( int argc, char *argv[] ) {
	//printf("EXECUTING.\n");
	std::string command("/usr/local/clang_8.0.0/bin/clang-check ");
	std::string path;

	//printf("Args In:");
	//for ( uint i=1; i<argc; ++i )
	//	printf(" %s", argv[i]);
	//printf("\n");
	
	for ( uint i=1; i<argc; ++i ) {
		std::string check( argv[i] );

		if ( check == "-c" )
			continue;
		if ( check == "-MG" )
			continue;
		if ( check == "-MP" )
			continue;
		if ( check == "-MM" )
			continue;
		if ( check.substr(0,3) == "-MT" ) {
			++i;
			continue;
		}
		if ( check.substr(0,3) == "-MF" ) {
			++i;
			continue;
		}
		if ( check == "-o" ) {
			++i;
			continue;
		}

		path += argv[i];
		path += ' ';
	}

	//printf("-----------------------------------------------PATH WAS: %s\n", path.c_str());
	path = String::RemoveBackDirs( path );
	//printf("PATH BEC: %s\n", path.c_str());
	command += path;
	fflush(stdout);
	return system(command.c_str());
}

