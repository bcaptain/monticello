for file in `find ./ -name '*.cue' -or -iname '*.mtl' -or -iname '*.ent' -or -iname '*.dmg'`; do
	# if it does not end with a newline
	if [[ "`tail -c2 $file | wc -l`" != "2" ]] ; then
		echo "adding newline to file: $file"
		echo "" >> "$file"
	fi
done

