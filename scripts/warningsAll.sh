#!/bin/bash
#warnings='#include \".\/clib\/src\/warnings.h\"'
warnings='#include \".\/warnings.h\"'
copyright='\/\/ Copyright 2010-2019 Brandon Captain. You may not copy this work.'

find ../include/clib/src/ -iname '*.cpp' -exec sed -i 's/^[/][/] Copyright [0-9]*.*/'"$copyright\n\n$warnings"'/g' "{}" \;
find ../include/clib/src/ -iname '*.h' -exec sed -i 's/^[/][/] Copyright [0-9]*.*/'"$copyright\n\n$warnings"'/g' "{}" \;


