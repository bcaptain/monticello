#!/bin/bash

function inRoot {
	find ./ -maxdepth 1 -iname '*.cpp' -or -iname '*.h' > count.tmp
}

function inDirs {
	find `cat dirs` -iname '*.cpp' -or -iname '*.h' >> count.tmp
}

function inClib {
	find clib -iname '*.cpp' -or -iname '*.h' >> count.tmp
}

function doIt {
	for file in `cat count.tmp` ; do
			cat "$file" |
			egrep '([a-zA-Z_]*)::\1' 
	done 
}	

inRoot; doIt
inDirs; doIt
#inClib; doIt

rm count.tmp
