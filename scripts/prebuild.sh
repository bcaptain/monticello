#!/bin/bash

g++ --version

cd ..
if [[ -e "run/monticello" ]] ; then
	echo "relinking executable"
	rm -f "run/monticello"
fi
if [[ -e "run/monticello_debug" ]] ; then
	echo "relinking executable"
	rm -f "run/monticello_debug"
fi
exit 0

