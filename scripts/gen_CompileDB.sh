find ../src -name compile_database.json -exec rm "{}" \;
find ../include/clib/src -name compile_database.json -exec rm "{}" \;
find ../src -name compile_commands.json -exec rm "{}" \;
find ../include/clib/src -name compile_commands.json -exec rm "{}" \;

find ../src -iname '*.cpp' | ./makeCompileDB_monticello
find ../include/clib/src -iname '*.cpp' | ./makeCompileDB_clib

