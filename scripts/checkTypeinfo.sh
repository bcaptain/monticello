#!/bin/bash

if (( $# < 2 )) ; then
	echo 'Checks that any class deriving from Entity has a proper EXTENDED_TYPEINFO_ declaration'
	echo "usage: ./checkTypeInfo.sh Entity $(find ./ -iname '*.h')"
	exit 0	
fi

classname="$1"
shift

classes=($(./findSubClassesOf.sh $classname $@))

function FindExtendedTypeInfo {
	egrep '^\s*EXTENDED_TYPEINFO_(DERIVED|BASE)\(\s*'"$class"'\s*[,)]' "$file"
}

for class in ${classes[@]} ; do
	found=""
	for file in $@ ; do
		found="$(FindExtendedTypeInfo $class $file)"

		if (( "$?" == "0" )) ; then
			break;
		fi
	done
	if [ "$found" == "" ] ; then
		echo 'Did not find EXTENDED_TYPEINFO_* for class: '"$class"
	fi
done

#EXTENDED_TYPEINFO_
