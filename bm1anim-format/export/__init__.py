# Copyright 2012-2013 Brandon Captain.

# this script is for blender
# pack()'s format characters and endian-ness: http://docs.python.org/2/library/struct.html#format-characters
# this script exports multi-byte values as little-endian

#todo: frame rate

bma1_version = 3

bl_info = {
	"name": "BM1Anim",
	"description": "Export BM1 Animation (.bm1anim format)",
	"author": "Brandon Captain",
	"version": (1,0),
	"blender": (2, 6, 3),
	"location": "File > Export > BM1Anim",
	"warning": "",
	"wiki_url": "",
	"tracker_url": "",
	"category": "Import-Export"
}

import bpy
import warnings
from mathutils import *
from math import *
from bpy_extras.io_utils import ExportHelper
from struct import *
from bpy.types import Operator, AddonPreferences, PropertyGroup
from bpy.props import StringProperty, IntProperty, BoolProperty

def update_all_limb_export_flags( self, context ):
	bone = self
	active_export_limb_object = []
	bm1anim = bpy.context.scene.BM1Anim
	
	if bone.bm1anim_export_enabled:
		
		for ob in bpy.data.objects:
			if not ob.proxy:
				if ob.type == 'ARMATURE' and bone in list(ob.pose.bones):
					active_export_limb_object = ob
					break
				
		limb = []
		limb.append(bone)
		root_bone = bone
		for child in bone.children:
			add_children_to_limb( child, limb )
		while root_bone.parent:
			root_bone = root_bone.parent
			limb.append(root_bone)
		armature = bone.id_data.name
		#clear all marked bones in other limbs
		if bm1anim.maintain_limb_structure:
			for bone in bpy.context.scene.objects[armature].pose.bones:
				if not bone in limb:
					bone.bm1anim_export_enabled = False
				
			for ob in bpy.data.objects:
				if not ob.proxy:
					if ob.type == 'ARMATURE' and ob != active_export_limb_object:
						for bone in ob.pose.bones:
							bone.bm1anim_export_enabled = False

def update_export_legs_and_torso( self, context):
	if self.export_legs_and_torso:
		self.export_accessory=False

def update_export_accessory( self, context):
	if self.export_accessory:
		self.export_legs_and_torso=False
						
def add_children_to_limb( bone, limb ):
    limb.append(bone)
    for child in bone.children:
        add_children_to_limb( child, limb)
		
def define_valid_armatures():
	global armature_body_set, armature_accessory_set, all_armatures_set,armature_dict
	valid_body_armatures = ['Legs', 'Torso','legs','torso']
	valid_accessory_armatures = ['Accessory','accessory']
	armature_body_set = set(valid_body_armatures)
	armature_accessory_set = set(valid_accessory_armatures)
	all_armatures_set = set(valid_body_armatures + valid_accessory_armatures )
	armature_dict = dict(Legs = 0, legs = 0,Torso = 1, torso = 1, Accessory = 1, accessory = 1)
	
def define_valid_mesh_names():
	global valid_meshName
	valid_meshName = ['mesh','Mesh']
	
def check_armature_usage_count_in_scene( armature ):
	count = 0
	for ob in bpy.context.scene.objects:
		if not ob.proxy:
			if ob.type == 'MESH':
				for mod in ob.modifiers:
					if mod.type == 'ARMATURE' and mod.object == armature:
						count += 1
	if count > 1:
		raise RuntimeError("Armature: " + armature.name + "is being used as a modifier in too many objects, must be <= 1, consider joining objects (usually meshes)")
	
def get_mesh_user_of_armature( armature ):
	check_armature_usage_count_in_scene( armature )
	for ob in bpy.context.scene.objects:
		if not ob.proxy:
			if ob.type == 'MESH':
				for mod in ob.modifiers:
					if mod.type == 'ARMATURE' and mod.object == armature:
						return ob
					
	raise RuntimeError("Armature " + armature.name + " is not being used in the modifier of any mesh objects")
	
def BM1Anim_enableAttach_update(self, context):
	blah=1
	
#ExportBM1AnimPreferences
#Panels/Buttons/etc. in this definition will show up when you are viewing Blenders User Preferences
#File->User Preferences...->Add-ons->Import-Export: BM1Anim
class ExportBM1AnimPreferences(AddonPreferences, PropertyGroup):
	bl_idname = __name__
	
	blah = bpy.props.FloatProperty(name="test float preference", min=0.0, max=1.0,default=0.5)

	def draw(self, context):
		layout = self.layout
		layout.label(text="This is a preferences view for our addon")
		
class BM1Anim_PoseBone(bpy.types.PropertyGroup):
	bpy.types.PoseBone.bm1anim_export_enabled = bpy.props.BoolProperty(
		name="Export BM1 Anim Flag",
		description = "Marks limb for BM1Anim export",
		default = True,
		update=update_all_limb_export_flags
	)
		
class BM1Anim_Bone(bpy.types.PropertyGroup):
	attachBone_enabled = bpy.props.BoolProperty(
		name="BM1Anim Enable Bone Attachment",
		description= "Enable attachment for bone",
		default=False,
		update=BM1Anim_enableAttach_update
	)
		
class BM1Anim_Scene(bpy.types.PropertyGroup):
	export_legs_and_torso = bpy.props.BoolProperty(
		name = "Export Legs/Torso",
		description = "Export 'Leg' and 'Torso' armatures",
		default = True,
		update=update_export_legs_and_torso
	)
	export_accessory = bpy.props.BoolProperty(
		name = "Export Accessory",
		description = "Export 'Accessory' armature",
		default = False,
		update=update_export_accessory
	)
	maintain_limb_structure = bpy.props.BoolProperty(
		name = "Enforce Parent/Child Chain",
		description = "Checks for bone connections when exporting limbs",
		default = True,
	)
	export_mesh = bpy.props.BoolProperty(
		name = "Export Mesh",
		description = "mesh marked for export",
		default = False
	)
	uniformScale_enabled = bpy.props.BoolProperty(
		name="Uniform scaling",
		description = "If Marked, model will export with a uniform scale defined by exportBM1Anim_uniformScale",
		default = True
		)
	uniformScale= bpy.props.FloatProperty(
		name="Scale",
		description="On import into the game engine, the mesh vertices and bones will be scaled uniformly by this factor",
		default=True
	)
	scaleVec = bpy.props.FloatVectorProperty(
		name="Scale",
		description="On import into the game engine, the mesh vertices and bones will be scaled by these dimensions",
		default=(1.0,1.0,1.0),
		precision=4,
		subtype='XYZ',
		size=3
	)
		
def attach_to_armature_poll( self, object ):
	return object.type == 'ARMATURE' and object.data.name in armature_body_set
		
class BM1Anim_Attach_Bone_Panel( bpy.types.Panel ):
	bl_idname = "BM1Anim_attach_bone"
	bl_label = "BM1Anim Attach Bone"
	bl_space_type = 'PROPERTIES'
	bl_region_type = 'WINDOW'
	bl_context = "bone"
	bl_options = {'DEFAULT_CLOSED'}

	def draw_header(self, context):
		layout = self.layout
		bon = context.bone
		layout.prop(bon.BM1Anim, "attachBone_enabled", text="")
		
	def draw(self, context):
		scene = context.scene
		layout = self.layout
		
		bon = context.bone
		col = layout.column()
		if(bon.BM1Anim.attachBone_enabled):
			box = col.box()
			box.label(text="Bone Attachment")
			box.prop(bon, "attached_to_armature", text="Armature",icon='ARMATURE_DATA')
			arm = bon.attached_to_armature
			if arm is not None:
				box.prop_search(bon, "attached_to_bone", bon.attached_to_armature.pose , "bones", text="Attach to Bone")
				row = box.row()
				row.prop(bon, "attached_to_bone_head_tail", text="Head/Tail")
			else:
				row = box.row()
				row.label(text="Attach to Bone :")
				sub = row.split()
				row.enabled = False
				sub.label(text="Must first select armature above", icon='BONE_DATA')
		else:
			col.label(text="Bone Attachment Disabled")

#ExportBM1Anim
#Called when you click File->Export->BM1Anim Format(.bm1anim)
#This is where you need to put properties if you want them to show up in the panel when you click the "export" button
class ExportBM1Anim(Operator, ExportHelper): 
	bl_idname = "export_format.bm1anim"
	bl_label = "Export BM1Anim"
	bl_options = {'REGISTER', 'UNDO'}
	
	filename_ext  = ".bm1anim";
	define_valid_armatures()
	define_valid_mesh_names()
	
	def execute(self, context):
		user_preferences = context.user_preferences
		addon_prefs = user_preferences.addons[__name__].preferences

		FilePath = bpy.path.ensure_ext(self.filepath, ".bm1anim")
		
		try:
			write_bm1anim( self, FilePath)
		except:
			raise #passes exceptions raised during export to blender to print 
		
		scene = bpy.data.scenes[0]
		bm1anim = scene.BM1Anim
		if bm1anim.export_mesh:
			tmpFilePath = self.filepath.split('_')
			tmpFilePath.remove(tmpFilePath[len(tmpFilePath)-1]) #trims the _[animName] from the filepath
			bm1FilePath = '_'.join(tmpFilePath)
			try:
				self.report({'INFO'},"Trying bm1 export to " + bm1FilePath)
				if bm1anim.uniformScale_enabled:
					bm1anim.scaleVec.xyz = bm1anim.uniformScale
				limb_only = False
				accessory_mesh = False
				if not bm1anim.export_legs_and_torso and not bm1anim.export_accessory:
					limb_only = True
				if bm1anim.export_accessory and bm1anim.export_mesh:
					accessory_mesh = True
				bpy.ops.export_format.bm1(exporting_limb_only=limb_only, export_accessory_mesh_from_BM1Anim_export=accessory_mesh,filePath_from_BM1Anim_export=bm1FilePath, scaleVec_from_BM1Anim_export=bm1anim.scaleVec)
			except AttributeError as e:
				print(e)
				print("Export .bm1 may not be installed correctly!")
		return {'FINISHED'}
		
	def draw(self, context):
		scene = context.scene
		bm1anim = scene.BM1Anim
		layout = self.layout
		box = layout.box()
		row = box.row()
		row.label("Export Scale")
		col = box.column()
		col.prop( bm1anim, "uniformScale_enabled")
		row = col.row()
		if bm1anim.uniformScale_enabled:
			row.prop( bm1anim, "uniformScale")
		else:
			row.prop( bm1anim, "scaleVec")
		row = layout.row()
		row.prop( bm1anim, "export_mesh")
		row = layout.row()
		row.prop( bm1anim, "export_legs_and_torso")
		row = layout.row()
		row.prop( bm1anim, "export_accessory")
		if not bm1anim.export_legs_and_torso and not bm1anim.export_accessory:
			row = layout.row()
			box = layout.box()
			box.prop( bm1anim, "maintain_limb_structure")
			draw_bone_checkboxes( box, context )
			
def draw_bone_checkboxes(box, context ):
	objs = bpy.context.scene.objects
	for ob in objs:
		if not ob.proxy:
			if ob.type == 'ARMATURE' and ob.data.name in all_armatures_set:
				box = box.box()
				box.label(text=ob.data.name, icon='ARMATURE_DATA')
				parentlessBones = getParentlessBones( ob )
				for bone in parentlessBones:
					draw_bone_heirarchy( ob , bone, box )

def draw_bone_heirarchy( ob, bone, box ):
	scene = bpy.context.scene
	bm1anim = scene.BM1Anim
	if not bone.parent:
		if bm1anim.export_legs_and_torso:
			box.enabled = False
		
	box.prop(bone, "bm1anim_export_enabled", text = bone.name)

	if bone.children:
		split = box.split(percentage=0.01)
		col_left = split.column()
		col_right = split.column()
		box = col_right

	for child in bone.children:
		draw_bone_heirarchy( ob, child, box )


def write_vec3f( out_files , vec ):
	out_files['bm1anim'].write( pack('<fff', vec.x, vec.y, vec.z ) )

def write_quat4f( out_files , quat ):
	out_files['bm1anim'].write( pack('<ffff', quat.x, quat.y, quat.z, quat.w ) )  # X and Y axis swapped because of Blender Bone Orientation

def write_uchar( out_files , uchr ):
	out_files['bm1anim'].write( pack('<B', uchr ) )

def write_uint( out_files , uval ):
	out_files['bm1anim'].write( pack('<I', uval ) )

def write_float( out_files , flt ):
	out_files['bm1anim'].write( pack('<f', flt ) )

def write_ushort( out_files , uval ):
	out_files['bm1anim'].write( pack('<H', uval ) )

def write_short_string( out_files , str ):
	write_uchar( out_files, len( str ) )
	out_files['bm1anim'].write( bytes( str, 'ASCII') )

def write_int( out_files, val ):
	out_files['bm1anim'].write( pack('<i', val ) )

def write_skeleton_structure(object, out_files, export_whole_armature):
	
	for bone in object.pose.bones:
		if bone.bm1anim_export_enabled or export_whole_armature:
			out_files['exp_log'].write(bone.name + "[" + str(len(bone.children)) + "] :")
			for child in bone.children:
				out_files['exp_log'].write(child.name + " | ")
			else:
				out_files['exp_log'].write("\n")
						
def get_boneID_from_mesh( bone, mesh ):
	if not bone.name in mesh.vertex_groups.keys():
		out_files['exp_log'].write("BoneName : " + bone.name + " not in VertexGroup list \n")
		return -1
	else:
		return mesh.vertex_groups[bone.name].index
		
def get_valid_mesh_object( objects ):
	exportMeshObjects = []
	for ob in objects:
		if not ob.proxy:
			if ob.type == 'MESH' and ob.data.name in valid_meshName:
				exportMeshObjects.append(ob)
	if len(exportMeshObjects) < 1:
		raise ExportError("Valid Mesh Not Found")
	if len(exportMeshObjects) > 1:
		raise ExportError("Multiple mesh objects found with name 'mesh' or 'Mesh', only 1 allowed")
	
	return exportMeshObjects[0]
	
def write_boneID(out_files, bone, export_whole_armature) :
	boneID = None
	if export_whole_armature:
		armature = bone.id_data
		mesh = get_mesh_user_of_armature( armature )
		boneID = get_boneID_from_mesh( bone, mesh )
	else:
		boneID = getBoneIndexFromLimb( bone )
	write_uchar(out_files, boneID)
	out_files['exp_log'].write("BoneID : " + str(boneID) + "\n")
	
def get_bone_object_from_armatureData_using_bone_name( armature, boneName ):
	if boneName not in armature.bones:
		raise RuntimeError("Attach Bone Target : " + boneName + " not in Armature " + armature.data.name )
	else:
		return armature.bones[boneName]
		
def verify_proper_attachment( bone ):
	if bone.attached_to_armature is not None:
		if bone.attached_to_bone:
			return True
		else:
			raise RuntimeError("No bone target for attachment on bone : " + bone.name + ", select an armature and bone or disable the bone attachment")	
	
	return False
	
def write_bone_attachment( out_files, posebone ):
	bone = posebone.bone
	hasGoodAttachment = verify_proper_attachment( bone )
	outputBoneID = -1 
	
	if hasGoodAttachment:
		armatureOb = bone.attached_to_armature
		mesh = get_mesh_user_of_armature( armatureOb )
		attachedToBone = get_bone_object_from_armatureData_using_bone_name( armatureOb.data , bone.attached_to_bone )
		outputBoneID = get_boneID_from_mesh( attachedToBone, mesh )
	
	write_int( out_files, outputBoneID )
	
	log_boneAttachInfo = ""
	if outputBoneID < 0:
		log_boneAttachInfo = "none (-1)"
	else:
		write_float( out_files, bone.attached_to_bone_head_tail)
		log_boneAttachInfo = str(outputBoneID) + " at point " + str(bone.attached_to_bone_head_tail)
	out_files['exp_log'].write('\tAttached To Bone ID: ' + log_boneAttachInfo + "\n")
	

def getBoneIndexFromLimb( bone ):
	armature = bpy.context.scene.objects[bone.id_data.name]
	bone_idx = []
	for pBone in armature.pose.bones:
		if pBone.bm1anim_export_enabled:
			bone_idx.append( pBone.name )
	return bone_idx.index( bone.name )
	
def getFCurveIndexForBone( armature, bone ):
	for index in range(len(armature.animation_data.action.fcurves)):
		if armature.animation_data.action.fcurves[index].data_path.split('"')[1] == bone.name: #using root bone X location as the base for counting frames
			return index
			
def getRootPoseBone( armature ):
	for bone in armature.pose.bones:
		if not bone.parent:
			return bone
			
def getParentlessBones( armature ):
	parentlessBones  = []
	for bone in armature.pose.bones:
		if not bone.parent:
			parentlessBones.append(bone)
	return parentlessBones

def write_bind_and_inv_bind_pose(bone, object, out_files, keyframe_points): #object = bpy.context.scene.objects[x].pose.bones[x]
	bone.id_data.data.pose_position = 'REST'
	bpy.context.scene.frame_set(keyframe_points[0].co.x)
	armature_name = object.data.name
	bind_rot = bpy.data.objects[object.name].pose.bones[bone.name].matrix.to_quaternion()
	bind_trans = bpy.data.objects[object.name].pose.bones[bone.name].matrix.translation
	quat_bone_trans = Quaternion((0,bind_trans.x,bind_trans.y,bind_trans.z))
	write_quat4f(out_files, bind_rot)
	out_files['exp_log'].write('\tBind q_rot : [{0:.3f},{1:.3f},{2:.3f},{3:.3f}]'.format(bind_rot.x,bind_rot.y,bind_rot.z,bind_rot.w) + '\n')
	out_files['exp_log'].write('\tBind loc : [{0:.3f},{1:.3f},{2:.3f},{3:.3f}]'.format(quat_bone_trans.x,quat_bone_trans.y,quat_bone_trans.z,quat_bone_trans.w) + '\n')
	write_quat4f(out_files, quat_bone_trans)
	inv_bind_rot = bpy.data.objects[object.name].pose.bones[bone.name].matrix.to_quaternion().inverted()
	inv_bone_trans = -bpy.data.objects[object.name].pose.bones[bone.name].matrix.translation
	quat_inv_bone_trans = Quaternion((0,inv_bone_trans.x,inv_bone_trans.y,inv_bone_trans.z))
	write_quat4f(out_files, inv_bind_rot )
	out_files['exp_log'].write('\tInverse Bind q_rot : [{0:.3f},{1:.3f},{2:.3f},{3:.3f}]'.format(inv_bind_rot.x,inv_bind_rot.y,inv_bind_rot.z,inv_bind_rot.w) + '\n')
	out_files['exp_log'].write('\tInverse Bind loc : [{0:.3f},{1:.3f},{2:.3f},{3:.3f}]'.format(quat_inv_bone_trans.x,quat_inv_bone_trans.y,quat_inv_bone_trans.z,quat_inv_bone_trans.w) + '\n')
	write_quat4f(out_files, quat_inv_bone_trans)
	bone.id_data.data.pose_position = 'POSE'

def write_bone_in_T_pose( bone, object, out_files):
	trans_mat = object.matrix_world
	tmp_bone = bpy.data.armatures[object.data.name].bones[bone.name]
	write_vec3f( out_files, tmp_bone.head_local * trans_mat)
	out_files['exp_log'].write('\tT_Pose Head (Rest) : [{0:.3f},{1:.3f},{2:.3f}]'.format(tmp_bone.head_local.x, tmp_bone.head_local.y, tmp_bone.head_local.z) + '\n')
	write_vec3f( out_files, tmp_bone.tail_local * trans_mat)
	out_files['exp_log'].write('\tT_Pose Tail (Rest) : [{0:.3f},{1:.3f},{2:.3f}]'.format(tmp_bone.tail_local.x, tmp_bone.tail_local.y, tmp_bone.tail_local.z) + '\n')

def write_bm1anim( self, filepath):
	scene = bpy.context.scene
	bm1anim = scene.BM1Anim
	out=open(filepath,"w+b")
	if bm1anim.export_legs_and_torso:
		export_log = open(bpy.path.abspath("//") + bpy.path.basename(bpy.context.blend_data.filepath).split('.')[0] + "_export_log.txt", "w")
	elif bm1anim.export_accessory:
		export_log = open(bpy.path.abspath("//") + bpy.path.basename(filepath).split('.')[0] + "_accessory_export_log.txt", "w")
	else:
		export_log = open(bpy.path.abspath("//") + bpy.path.basename(filepath).split('.')[0] + "_limb_export_log.txt", "w")
	out_files = dict(bm1anim = out, exp_log = export_log)
	error_pre_check()
	write_header( out_files )
	write_export_scale( self, out_files )
	objects = scene.objects
	write_keyframes( objects, out_files )
	out.close()

def write_keyframe_count(object, out_files ):
	rootBone = getRootPoseBone(object)
	fCurveIndex = getFCurveIndexForBone(object, rootBone )
	num_keyframes = len(object.animation_data.action.fcurves[fCurveIndex].keyframe_points)
	write_ushort(out_files , num_keyframes)
	out_files['exp_log'].write("Number of Keyframes = " + str(num_keyframes) + "\n")

def write_header( out_files ):
	out_files['bm1anim'].write( bytes( "BMA1", 'ASCII') )
	write_uchar( out_files , bma1_version)
	write_uchar( out_files , 0 ) # unused bits, we can add things here later
	
def write_export_scale( self, out_files ):
	scene = bpy.context.scene
	bm1anim = scene.BM1Anim
	if bm1anim.uniformScale_enabled:
		uniform_val = bm1anim.uniformScale
		uniform_scalevec = Vector([uniform_val,uniform_val,uniform_val])
		write_vec3f( out_files, uniform_scalevec)
		out_files['exp_log'].write("Scale (uniform): {0.x},{0.y},{0.z}\n".format( uniform_scalevec ))
	else:
		export_precision = bpy.types.bm1anim.scaleVec[1]['precision']
		write_vec3f( out_files, bm1anim.scaleVec )
		out_files['exp_log'].write("Scale : {0.x:.{1}f},{0.y:.{1}f},{0.z:.{1}f}\n".format( bm1anim.scaleVec, export_precision ))
	
def write_channel( ob, out_files):
	channelName = ob.data.name
	if ( ob.data.name in ['Accessory','accessory'] ): #the channel for an accessory will match the channel of the armature it is attached to
		rootBone = getRootPoseBone(ob)
		hasGoodAttachment = verify_proper_attachment( rootBone.bone )
		if ( hasGoodAttachment ):
			channelName = rootBone.bone.attached_to_armature.name
	write_uint(out_files, armature_dict[channelName])
	out_files['exp_log'].write('Channel : {}'.format(armature_dict[channelName]) + '\n')

# we only cycle root bones here, but their children are handled as well
# the armature is traversed in the same order as the BM1 format ( for proper bone ids )
def write_keyframes( objects, out_files ): #objects = bpy.context.scene.objects
	exportedArmatureCount = 0
	write_skeleton_count( out_files )
	
	bm1anim = bpy.context.scene.BM1Anim
	export_whole_armature = bm1anim.export_legs_and_torso or bm1anim.export_accessory
	armature_set = []
	invalid_armatures = []
	
	if bm1anim.export_legs_and_torso:
		armature_set = armature_body_set
		
	if bm1anim.export_accessory:
		armature_set = armature_accessory_set
	
	for ob in objects:
		if not ob.proxy:
			if ob.type == 'ARMATURE':
				if ob.data.name in armature_set:
					if len(armature_set):
						exportedArmatureCount +=1
					else:
						for bone in ob.pose.bones:
							if bone.bm1anim_export_enabled:
								if not bone.parent or not bone.parent.bm1anim_export_enabled:
									exportedArmatureCount += 1
					out_files['exp_log'].write('Armature : {0}'.format(ob.data.name) + '\n')
					write_channel( ob, out_files)
					write_keyframe_count(ob, out_files )
					write_root_bones( ob, out_files, export_whole_armature )
					out_files['exp_log'].write("\nEnd of Armature : '{0}'\n\n".format(ob.data.name))
				else:
					invalid_armatures.append(ob.data.name)
				
	if exportedArmatureCount < 1:
		armature_name_error_msg = "No valid armature data names found for export, valid names are : "
		if len(armature_set):
			armature_name_error_msg += ' ,'.join(armature_set)
		raise RuntimeError(armature_name_error_msg)
		
	if len(invalid_armatures) > 0:
		warnings.warn("Armature(s) : '" + ','.join(invalid_armatures) + "' not in armature dictionary", UserWarning, stacklevel=2)
		
def write_skeleton_count( out_files ):
	scene = bpy.context.scene
	bm1anim = scene.BM1Anim
	armature_count = 0
	
	if bm1anim.export_legs_and_torso:
		for arm in bpy.data.armatures:
			if arm.name in armature_body_set:
				armature_count += 1
	else:
		armature_count = 1 #default for now

	write_uchar( out_files , armature_count)
	out_files['exp_log'].write("Number of Armatures = " + str(armature_count) + "\n\n")
	
def count_limb_bones( object ): #only called when exporting limbs, not entire armatures
	count = 0
	for bone in object.pose.bones:
		if bone.bm1anim_export_enabled:
			count += 1
	return count

def write_total_bone_count( object, out_files, export_whole_armature ):
	if export_whole_armature:
		bone_count = len(object.pose.bones)
	else:
		bone_count = count_limb_bones( object )
	write_uchar( out_files , bone_count )
	out_files['exp_log'].write("Total Bone Count in Armature = {0}".format(str(bone_count) + "\n"))

def write_root_bones( object, out_files, export_whole_armature ): #object = bpy.context.scene.objects[#]
	bm1anim = bpy.context.scene.BM1Anim
	write_total_bone_count( object, out_files, export_whole_armature ) 
	write_skeleton_structure( object, out_files, export_whole_armature) #log only
	if export_whole_armature:
		for bone in object.pose.bones:
			if bone.parent == None:
				armature = object
				mesh = get_mesh_user_of_armature( armature )
				if bone.name in mesh.vertex_groups.keys():
					out_files['exp_log'].write("Begin Frame Data \n\n")
					write_frame_data( bone, object, out_files, export_whole_armature )
				else:
					out_files['exp_log'].write("BoneName : " + bone.name + " not in VertexGroup list \n")
					raise RuntimeError("BoneName : " + bone.name + " not in VertexGroup list")
	else:
		for bone in object.pose.bones:
			if bone.bm1anim_export_enabled:
				if not bone.parent or not bone.parent.bm1anim_export_enabled:
					out_files['exp_log'].write("Begin Frame Data \n\n")
					write_frame_data( bone, object, out_files, export_whole_armature )

def write_frame_data( bone, object, out_files, export_whole_armature): #object = bpy.context.scene.objects[#] , bone = object.pose.bones[#]
	trans_mat = object.matrix_world #if origin is (0,0,0) this will be an identity matrix
	armature_name = object.data.name
	rootBone = getRootPoseBone(object)
	fcurveIndex = getFCurveIndexForBone( object, rootBone )
	keyframe_points = object.animation_data.action.fcurves[fcurveIndex].keyframe_points
	write_short_string( out_files , bone.name)
	out_files['exp_log'].write("Bone Name : " + bone.name + "\n")
	write_boneID(out_files, bone, export_whole_armature )
	write_bone_attachment( out_files, bone )
	write_bind_and_inv_bind_pose(bone, object, out_files, keyframe_points)
	write_bone_in_T_pose(bone, object, out_files)
	for keyFrame in keyframe_points:
		bpy.context.scene.frame_set(frame=keyFrame.co.x)
		write_ushort( out_files , bpy.context.scene.frame_current )
		out_files['exp_log'].write('\tFrame : ' + str(keyFrame.co.x) + '\n')
		write_vec3f( out_files , bone.head * trans_mat )
		bone_head = bone.head * trans_mat
		out_files['exp_log'].write('\t\tHead: [{0:.3f},{1:.3f},{2:.3f}]'.format(bone_head.x, bone_head.y, bone_head.z) + '\n')
		write_vec3f( out_files , bone.tail * trans_mat )
		bone_tail = bone.tail * trans_mat
		out_files['exp_log'].write('\t\tTail: [{0:.3f},{1:.3f},{2:.3f}]'.format(bone_tail.x, bone_tail.y, bone_tail.z) + '\n')
		if bone.parent:
			boneLoc =  bpy.data.objects[object.name].pose.bones[bone.name].location * bpy.data.armatures[armature_name].bones[bone.name].matrix
			out_files['exp_log'].write('\t\tLocation w/ Parent : {0:.3f},{1:.3f},{2:.3f}'.format(round(boneLoc.x,3),round(boneLoc.y,3),round(boneLoc.z,3)) + '\n')
		else:
			boneLoc = bpy.data.objects[object.name].pose.bones[bone.name].location
			out_files['exp_log'].write('\t\tLocation : {0:.3f},{1:.3f},{2:.3f}'.format(round(boneLoc.x,3),round(boneLoc.y,3),round(boneLoc.z,3)) + '\n')
		write_vec3f( out_files, boneLoc)
		write_quat4f( out_files , bone.rotation_quaternion )
		boneQuat = bone.rotation_quaternion
		out_files['exp_log'].write('\t\tQuaternion : {0:.3f},{1:.3f},{2:.3f},{3:.3f}'.format(round(boneQuat.x,3),round(boneQuat.y,3),round(boneQuat.z,3),round(boneQuat.w,3)) + '\n')

	child_count = 0
	if export_whole_armature:
		child_count = len(bone.children)
	else:
		for child in bone.children:
			if child.bm1anim_export_enabled:
				child_count += 1
	write_uchar( out_files, child_count )
	out_files['exp_log'].write("Num Children = {0}\n".format(child_count))
	for child in bone.children:
		if not export_whole_armature:
			if not child.export_whole_armature:
				continue
		write_frame_data( child, object, out_files, export_whole_armature)
		
def check_keyframe_errors():
	#All armatures should have the same number of keyframes
	armKeyCountDict = dict()
	keyCountList = []
	for ob in bpy.data.objects:
		if not ob.proxy:
			if ob.type == 'ARMATURE':
				if ob.data.name in armature_body_set:
					rootBone = getRootPoseBone(ob)
					fCurveIndex = getFCurveIndexForBone(ob, rootBone )
					num_keyframes = len(ob.animation_data.action.fcurves[fCurveIndex].keyframe_points)
					armKeyCountDict[ob.data.name] = num_keyframes
					keyCountList.append(num_keyframes)
	if len(set(keyCountList)) > 1:
		errMsg = "Keyframe count mismatch found\n"
		for key in list(armKeyCountDict.keys()):
			errMsg += key + ' :' + str(armKeyCountDict[key]) + '\n'
		raise RuntimeError(errMsg)
		return
			
		
def error_pre_check(): #this is for errors that cause problems in the engine
	#Vertex Groups should match exported bones
	bone_list = []
	mesh_ob = []
	for ob in bpy.data.objects:
		if not ob.proxy:
			if ob.type == 'ARMATURE':
				for bone in ob.pose.bones:
					bone_list.append(bone.name)
	mesh_ob = get_valid_mesh_object(bpy.data.objects)
	vertex_group_list = mesh_ob.vertex_groups
	for group in vertex_group_list:
		if group.name not in bone_list:
			raise RuntimeError('Vertex group "' + group.name + '" does not match any bones in export armatures')
			return
	check_keyframe_errors()


def menu_func(self, context):
	self.layout.operator(ExportBM1Anim.bl_idname, text="BM1Anim Format(.bm1anim)");

def register():
	bpy.utils.register_module(__name__)
	bpy.types.Scene.BM1Anim = bpy.props.PointerProperty(type = BM1Anim_Scene)
	bpy.types.Bone.BM1Anim = bpy.props.PointerProperty(type = BM1Anim_Bone)
	bpy.types.Bone.attached_to_armature = bpy.props.PointerProperty(
		name="attached_to_armature",
		description="Armature containing target bone for attachment",
		type=bpy.types.Object,
		poll=attach_to_armature_poll
	)
	bpy.types.Bone.attached_to_bone = bpy.props.StringProperty(
		name="attached_to_bone",
		description="Target bone for attachment"
	)
	bpy.types.Bone.attached_to_bone_head_tail = bpy.props.FloatProperty(
		name="attached_to_bone_head_tail",
		description="Point of attachment, [0..1], 0=Head 1=Tail",
		default =0.0,
		min=0.0,
		max=1.0,
		step=1,
		precision = 2	
	)
	bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
	bpy.utils.unregister_module(__name__)
	bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
	register()

